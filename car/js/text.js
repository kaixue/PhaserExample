function b(a) {
    throw a;
}
var k = void 0, p = !0, q = null, r = !1;
function aa() {
    return function (a) {
        return a
    }
}
function u() {
    return function () {
    }
}
function z(a) {
    return function (c) {
        this[a] = c
    }
}
function A(a) {
    return function () {
        return this[a]
    }
}
function D(a) {
    return function () {
        return a
    }
}
var F = F || {};
F.N = F.N || {};
F.i = {};
M = window;
M = Object.prototype;
delete window._p;
F.bc = function (a) {
    return document.createElement(a)
};
F.Sa = function (a, c, d, e) {
    a.addEventListener(c, d, e)
};
F.rN = "undefined" !== typeof require && require("fs");
F.QP = function (a, c) {
    if (a)if (a instanceof Array)for (var d = 0, e = a.length; d < e && c.call(k, a[d], d) !== r; d++); else for (d in a)if (c.call(k, a[d], d) === r)break
};
F.extend = function (a) {
    var c = 2 <= arguments.length ? Array.prototype.slice.call(arguments, 1) : [];
    F.QP(c, function (c) {
        for (var e in c)c.hasOwnProperty(e) && (a[e] = c[e])
    });
    return a
};
F.ac = function (a) {
    return "function" == typeof a
};
F.du = function (a) {
    return "number" == typeof a || "[object Number]" == Object.prototype.toString.call(a)
};
F.Dd = function (a) {
    return "string" == typeof a || "[object String]" == Object.prototype.toString.call(a)
};
F.isArray = function (a) {
    return "[object Array]" == Object.prototype.toString.call(a)
};
F.nk = function (a) {
    return "undefined" == typeof a
};
F.Gq = function (a) {
    var c = typeof a;
    return "function" == c || a && "object" == c
};
F.Ol = function (a) {
    if (!a)return F.log("invalid URL"), r;
    var c = a.indexOf("://");
    if (-1 == c)return r;
    c = a.indexOf("/", c + 3);
    return (-1 == c ? a : a.substring(0, c)) != location.origin
};
function ba(a, c, d, e, f) {
    var g = this;
    g.Xca = a;
    g.Ax = c;
    g.dt = [];
    g.xN = d;
    g.yN = f;
    g.Um = e;
    g.Hx = f;
    g.hO = a instanceof Array ? [] : {};
    g.qN = r;
    F.QP(a, function (a, c) {
        g.dt.push({index: c, value: a})
    });
    g.size = g.dt.length;
    g.sG = 0;
    g.EF = 0;
    g.Ax = g.Ax || g.size;
    g.pla = function (a, c) {
        g.xN = a;
        g.yN = c
    };
    g.ola = function (a, c) {
        g.Um = a;
        g.Hx = c
    };
    g.hN = function () {
        var a = this;
        if (0 != a.dt.length && !(a.EF >= a.Ax)) {
            var c = a.dt.shift(), d = c.value, e = c.index;
            a.EF++;
            a.xN.call(a.yN, d, e, function (c) {
                if (!a.qN)if (a.sG++, a.EF--, c)a.qN = p, a.Um && a.Um.call(a.Hx, c); else {
                    var d =
                        Array.prototype.slice.call(arguments, 1);
                    a.hO[this.index] = d[0];
                    a.sG == a.size ? a.Um && a.Um.call(a.Hx, q, a.hO) : a.hN()
                }
            }.bind(c), a)
        }
    };
    g.rq = function () {
        if (0 == this.dt.length)this.Um && this.Um.call(this.Hx, q, []); else for (var a = 0; a < this.Ax; a++)this.hN()
    }
}
F.async = {
    Wma: function (a, c, d) {
        a = new ba(a, 1, function (a, c, g) {
            a.call(d, g)
        }, c, d);
        a.rq();
        return a
    }, Nla: function (a, c, d) {
        a = new ba(a, 0, function (a, c, g) {
            a.call(d, g)
        }, c, d);
        a.rq();
        return a
    }, Qpa: function (a, c, d) {
        var e = [], f = [q], g = new ba(a, 1, function (c, g, n) {
            e.push(function (c) {
                e = Array.prototype.slice.call(arguments, 1);
                a.length - 1 == g && (f = f.concat(e));
                n.apply(q, arguments)
            });
            c.apply(d, e)
        }, function (a) {
            if (c) {
                if (a)return c.call(d, a);
                c.apply(d, f)
            }
        });
        g.rq();
        return g
    }, map: function (a, c, d, e) {
        var f = c;
        "object" == typeof c && (d = c.Bn,
            e = c.Wia, f = c.Via);
        a = new ba(a, 0, f, d, e);
        a.rq();
        return a
    }, Tka: function (a, c, d, e, f) {
        a = new ba(a, c, d, e, f);
        a.rq();
        return a
    }
};
F.path = {
    join: function () {
        for (var a = arguments.length, c = "", d = 0; d < a; d++)c = (c + ("" == c ? "" : "/") + arguments[d]).replace(/(\/|\\\\)$/, "");
        return c
    }, Hl: function (a) {
        return (a = /(\.[^\.\/\?\\]*)(\?.*)?$/.exec(a)) ? a[1] : q
    }, Ska: function (a) {
        if (a) {
            var c = a.lastIndexOf(".");
            if (-1 !== c)return a.substring(0, c)
        }
        return a
    }, x2: function (a, c) {
        var d = a.indexOf("?");
        0 < d && (a = a.substring(0, d));
        d = /(\/|\\\\)([^(\/|\\\\)]+)$/g.exec(a.replace(/(\/|\\\\)$/, ""));
        if (!d)return q;
        d = d[2];
        return c && a.substring(a.length - c.length).toLowerCase() ==
        c.toLowerCase() ? d.substring(0, d.length - c.length) : d
    }, dirname: function (a) {
        return a.replace(/((.*)(\/|\\|\\\\))?(.*?\..*$)?/, "$2")
    }, pP: function (a, c) {
        c = c || "";
        var d = a.indexOf("?"), e = "";
        0 < d && (e = a.substring(d), a = a.substring(0, d));
        d = a.lastIndexOf(".");
        return 0 > d ? a + c + e : a.substring(0, d) + c + e
    }, hq: function (a, c, d) {
        if (0 == c.indexOf("."))return this.pP(a, c);
        var e = a.indexOf("?"), f = "";
        d = d ? this.Hl(a) : "";
        0 < e && (f = a.substring(e), a = a.substring(0, e));
        e = a.lastIndexOf("/");
        return a.substring(0, 0 >= e ? 0 : e + 1) + c + d + f
    }
};
F.aa = {
    zN: {}, Sx: {}, C_: {}, xm: {}, Yz: "", u2: "", jg: {}, az: function () {
        return window.XMLHttpRequest ? new window.XMLHttpRequest : new ActiveXObject("MSXML2.XMLHTTP")
    }, WM: function (a) {
        var c = a[0], d = a[1], e = a[2], f = ["", q, q];
        1 === a.length ? f[1] = c instanceof Array ? c : [c] : 2 === a.length ? "function" == typeof d ? (f[1] = c instanceof Array ? c : [c], f[2] = d) : (f[0] = c || "", f[1] = d instanceof Array ? d : [d]) : 3 === a.length ? (f[0] = c || "", f[1] = d instanceof Array ? d : [d], f[2] = e) : b("arguments error to load js!");
        return f
    }, C6: function (a, c, d) {
        var e = this, f =
            e.zN, g = e.WM(arguments), h = g[0], m = g[1], g = g[2];
        -1 < navigator.userAgent.indexOf("Trident/5") ? e.EN(h, m, 0, g) : F.async.map(m, function (a, c, d) {
            a = F.path.join(h, a);
            if (f[a])return d(q);
            e.IL(a, r, d)
        }, g)
    }, oR: function (a, c, d) {
        var e = this.H_(), f = this.WM(arguments);
        this.C6(f[0], f[1], function (a) {
            a && b(a);
            e.parentNode.removeChild(e);
            if (f[2])f[2]()
        })
    }, IL: function (a, c, d) {
        var e = document, f = F.bc("script");
        f.async = c;
        f.src = a;
        this.zN[a] = p;
        F.Sa(f, "load", function () {
            f.parentNode.removeChild(f);
            this.removeEventListener("load", arguments.callee,
                r);
            d()
        }, r);
        F.Sa(f, "error", function () {
            f.parentNode.removeChild(f);
            d("Load " + a + " failed!")
        }, r);
        e.body.appendChild(f)
    }, EN: function (a, c, d, e) {
        if (d >= c.length)e && e(); else {
            var f = this;
            f.IL(F.path.join(a, c[d]), r, function (g) {
                if (g)return e(g);
                f.EN(a, c, d + 1, e)
            })
        }
    }, H_: function () {
        var a = document, c = a.getElementById("cocos2d_loadJsImg");
        if (!c) {
            c = F.bc("img");
            F.GN && (c.src = F.GN);
            a = a.getElementById(F.Qb.Re.id);
            a.style.backgroundColor = "black";
            a.parentNode.appendChild(c);
            var d = getComputedStyle ? getComputedStyle(a) : a.currentStyle;
            d || (d = {width: a.width, height: a.height});
            c.style.left = a.offsetLeft + (parseFloat(d.width) - c.width) / 2 + "px";
            c.style.top = a.offsetTop + (parseFloat(d.height) - c.height) / 2 + "px";
            c.style.position = "absolute"
        }
        return c
    }, Fz: function (a, c) {
        if (F.rN)require("fs").cma(a, function (a, d) {
            a ? c(a) : c(q, d.toString())
        }); else {
            var d = this.az(), e = "load " + a + " failed!";
            d.open("GET", a, p);
            /msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent) ? (d.setRequestHeader("Accept-Charset", "utf-8"), d.onreadystatechange = function () {
                4 ==
                d.readyState && (200 == d.status ? c(q, d.responseText) : c(e))
            }) : (d.overrideMimeType && d.overrideMimeType("text/plain; charset\x3dutf-8"), d.onload = function () {
                4 == d.readyState && (200 == d.status ? c(q, d.responseText) : c(e))
            });
            d.send(q)
        }
    }, FN: function (a) {
        if (F.rN)return require("fs").dma(a).toString();
        var c = this.az();
        c.open("GET", a, r);
        /msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent) ? c.setRequestHeader("Accept-Charset", "utf-8") : c.overrideMimeType && c.overrideMimeType("text/plain; charset\x3dutf-8");
        c.send(q);
        return 4 == !c.readyState || 200 != c.status ? q : c.responseText
    }, A6: function (a, c) {
        var d = new XMLHttpRequest;
        d.open("GET", a, p);
        d.responseType = "arraybuffer";
        d.onload = function () {
            var e = d.response;
            e && (window.Xka = e);
            4 == d.readyState && (200 == d.status ? c(q, d.response) : c("load " + a + " failed!"))
        };
        d.send(q)
    }, pR: function (a, c) {
        this.Fz(a, function (d, e) {
            try {
                d ? c(d) : c(q, JSON.parse(e))
            } catch (f) {
                b("load json [" + a + "] failed : " + f)
            }
        })
    }, CL: function (a) {
        return /(\.png)|(\.jpg)|(\.bmp)|(\.jpeg)|(\.gif)/.exec(a) != q
    }, Kq: function (a,
                     c, d) {
        function e() {
            this.removeEventListener("error", e, r);
            h.crossOrigin && "anonymous" == h.crossOrigin.toLowerCase() ? (g.Ol = r, F.aa.Kq(a, g, d)) : "function" == typeof d && d("load image failed")
        }

        function f() {
            this.removeEventListener("load", f, r);
            this.removeEventListener("error", e, r);
            d && d(q, h)
        }

        var g = {Ol: p};
        d !== k ? g.Ol = c.Ol == q ? g.Ol : c.Ol : c !== k && (d = c);
        var h = new Image;
        g.Ol && "file://" != location.origin && (h.crossOrigin = "Anonymous");
        F.Sa(h, "load", f);
        F.Sa(h, "error", e);
        h.src = a;
        return h
    }, I_: function (a, c, d) {
        var e = this, f = q;
        (c =
            a.type) ? (c = "." + c.toLowerCase(), f = a.src ? a.src : a.name + c) : (f = a, c = F.path.Hl(f));
        var g = e.jg[f];
        if (g)return d(q, g);
        g = q;
        c && (g = e.Sx[c.toLowerCase()]);
        if (!g)return F.error("loader for [" + c + "] not exists!"), d();
        c = e.c5(g.Ry ? g.Ry() : e.Yz, f);
        g.load(c, f, a, function (a, c) {
            a ? (F.log(a), e.jg[f] = q, delete e.jg[f], d()) : (e.jg[f] = c, d(q, c))
        })
    }, c5: function (a, c) {
        var d = this.C_, e = F.path;
        if (a !== k && c === k) {
            c = a;
            var f = e.Hl(c), f = f ? f.toLowerCase() : "";
            a = (f = this.Sx[f]) ? f.Ry ? f.Ry() : this.Yz : this.Yz
        }
        c = F.path.join(a || "", c);
        if (c.match(/[\/(\\\\)]lang[\/(\\\\)]/i)) {
            if (d[c])return d[c];
            e = e.Hl(c) || "";
            c = d[c] = c.substring(0, c.length - e.length) + "_" + F.ta.language + e
        }
        return c
    }, load: function (a, c, d) {
        var e = this, f = arguments.length;
        0 == f && b("arguments error!");
        3 == f ? "function" == typeof c && (c = "function" == typeof d ? {XS: c, Bn: d} : {
            Bn: c,
            I2: d
        }) : 2 == f ? "function" == typeof c && (c = {Bn: c}) : 1 == f && (c = {});
        a instanceof Array || (a = [a]);
        f = new ba(a, 0, function (a, d, f, n) {
            e.I_(a, d, function (a) {
                if (a)return f(a);
                var d = Array.prototype.slice.call(arguments, 1);
                c.XS && c.XS.call(c.Epa, d[0], n.size, n.sG);
                f(q, d[0])
            })
        }, c.Bn, c.I2);
        f.rq();
        return f
    }, gN: function (a, c) {
        var d = this.xm, e = [], f;
        for (f in a) {
            var g = a[f];
            d[f] = g;
            e.push(g)
        }
        this.load(e, c)
    }, Pka: function (a, c) {
        var d = this, e = d.ge(a);
        e ? d.gN(e.filenames, c) : d.load(a, function (a, e) {
            d.gN(e[0].filenames, c)
        })
    }, nj: function (a, c) {
        if (a && c) {
            if ("string" == typeof a)return this.Sx[a.trim().toLowerCase()] = c;
            for (var d = 0, e = a.length; d < e; d++)this.Sx["." + a[d].trim().toLowerCase()] = c
        }
    }, ge: function (a) {
        return this.jg[a] || this.jg[this.xm[a]]
    }, oj: function (a) {
        var c = this.jg, d = this.xm;
        delete c[a];
        delete c[d[a]];
        delete d[a]
    },
    nma: function () {
        var a = this.jg, c = this.xm, d;
        for (d in a)delete a[d];
        for (d in c)delete c[d]
    }
};
F.tG = function () {
    var a = arguments, c = a.length;
    if (1 > c)return "";
    var d = a[0], e = p;
    "object" == typeof d && (e = r);
    for (var f = 1; f < c; ++f) {
        var g = a[f];
        if (e)for (; ;) {
            var h = q;
            if ("number" == typeof g && (h = d.match(/(%d)|(%s)/))) {
                d = d.replace(/(%d)|(%s)/, g);
                break
            }
            d = (h = d.match(/%s/)) ? d.replace(/%s/, g) : d + ("    " + g);
            break
        } else d += "    " + g
    }
    return d
};
(function () {
    function a() {
        F.La && F.Qb.Gs && F.La.dispatchEvent(F.Qb.Gs);
        F.Qb.Lm && (window.cancelAnimationFrame(F.Qb.Lm), F.Qb.EE())
    }

    function c() {
        F.La && F.Qb.Fs && F.La.dispatchEvent(F.Qb.Fs)
    }

    var d = window, e, f;
    F.nk(document.hidden) ? F.nk(document.Vka) ? F.nk(document.Wka) ? F.nk(document.Tpa) || (e = "webkitHidden", f = "webkitvisibilitychange") : (e = "msHidden", f = "msvisibilitychange") : (e = "mozHidden", f = "mozvisibilitychange") : (e = "hidden", f = "visibilitychange");
    e ? F.Sa(document, f, function () {
        document[e] ? c() : a()
    }, r) : (F.Sa(d, "blur",
        c, r), F.Sa(d, "focus", a, r));
    -1 < navigator.userAgent.indexOf("MicroMessenger") && (d.onfocus = function () {
        a()
    });
    "onpageshow" in window && "onpagehide" in window && (F.Sa(d, "pagehide", c, r), F.Sa(d, "pageshow", a, r));
    f = d = q
})();
F.log = F.warn = F.error = F.assert = u();
F.zP = function (a, c) {
    for (var d = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"], e = q, f = 0; f < d.length; ++f) {
        try {
            e = a.getContext(d[f], c)
        } catch (g) {
        }
        if (e)break
    }
    return e
};
F.q_ = function (a, c) {
    F.Aa = 0;
    F.Y = 1;
    F.ta = {};
    var d = F.ta;
    d.bV = "en";
    d.Baa = "zh";
    d.Daa = "fr";
    d.Gaa = "it";
    d.Eaa = "de";
    d.Naa = "es";
    d.Caa = "du";
    d.Maa = "ru";
    d.Iaa = "ko";
    d.Haa = "ja";
    d.Faa = "hu";
    d.Laa = "pt";
    d.Aaa = "ar";
    d.Jaa = "no";
    d.Kaa = "pl";
    d.oJ = "Windows";
    d.zo = "iOS";
    d.rB = "OS X";
    d.HV = "UNIX";
    d.nJ = "Linux";
    d.mJ = "Android";
    d.IV = "Unknown";
    d.Tba = 0;
    d.Oaa = 1;
    d.Qaa = 2;
    d.p$ = 3;
    d.yaa = 4;
    d.xaa = 5;
    d.y$ = 6;
    d.Uaa = 7;
    d.iaa = 8;
    d.yba = 9;
    d.Uba = 10;
    d.Vba = 11;
    d.mV = 100;
    d.aU = 101;
    d.IA = "wechat";
    d.xT = "androidbrowser";
    d.FA = "ie";
    d.zT = "qqbrowser";
    d.BI = "mqqbrowser";
    d.CI = "ucbrowser";
    d.B$ = "360browser";
    d.yT = "baiduboxapp";
    d.AI = "baidubrowser";
    d.C$ = "maxthon";
    d.GA = "opera";
    d.E$ = "oupeng";
    d.D$ = "miuibrowser";
    d.pr = "firefox";
    d.HA = "safari";
    d.EA = "chrome";
    d.AT = "unknown";
    d.Gia = r;
    var e = [d.AI, d.GA, d.pr, d.EA, d.HA], f = [d.zo, d.oJ, d.rB, d.nJ], g = [d.AI, d.GA, d.pr, d.EA, d.yT, d.HA, d.CI, d.zT, d.BI, d.FA], h = window, m = h.navigator, n = document.documentElement, s = m.userAgent.toLowerCase();
    d.nh = -1 != s.indexOf("mobile") || -1 != s.indexOf("android");
    d.platform = d.nh ? d.mV : d.aU;
    var t = m.language, t = (t = t ? t : m.browserLanguage) ?
        t.split("-")[0] : d.bV;
    d.language = t;
    var t = d.AT, v = s.match(/micromessenger|qqbrowser|mqqbrowser|ucbrowser|360browser|baiduboxapp|baidubrowser|maxthon|trident|oupeng|opera|miuibrowser|firefox/i) || s.match(/chrome|safari/i);
    v && 0 < v.length && (t = v[0].toLowerCase(), "micromessenger" == t ? t = d.IA : "safari" === t && s.match(/android.*applewebkit/) ? t = d.xT : "trident" == t && (t = d.FA));
    d.$h = t;
    t = s.match(/(iPad|iPhone|iPod)/i) ? p : r;
    s = s.match(/android/i) || m.platform.match(/android/i) ? p : r;
    v = d.IV;
    -1 != m.appVersion.indexOf("Win") ? v = d.oJ :
        t ? v = d.zo : -1 != m.appVersion.indexOf("Mac") ? v = d.rB : -1 != m.appVersion.indexOf("X11") ? v = d.HV : s ? v = d.mJ : -1 != m.appVersion.indexOf("Linux") && (v = d.nJ);
    d.Tl = v;
    d.u1 = -1 < g.indexOf(d.$h);
    g = parseInt(a[c.Iu]);
    s = F.Y;
    t = F.bc("Canvas");
    F.iy = p;
    e = !window.WebGLRenderingContext || -1 == e.indexOf(d.$h) || -1 == f.indexOf(d.Tl);
    if (1 === g || 0 === g && e || "file://" == location.origin)s = F.Aa;
    d.LY = function () {
        var a = document.createElement("canvas");
        a.width = 1;
        a.height = 1;
        a = a.getContext("2d");
        a.fillStyle = "#000";
        a.fillRect(0, 0, 1, 1);
        a.globalCompositeOperation =
            "multiply";
        var c = document.createElement("canvas");
        c.width = 1;
        c.height = 1;
        var d = c.getContext("2d");
        d.fillStyle = "#fff";
        d.fillRect(0, 0, 1, 1);
        a.drawImage(c, 0, 0, 1, 1);
        return 0 === a.getImageData(0, 0, 1, 1).data[0]
    };
    d.hy = d.LY();
    if (s == F.Y && (!h.WebGLRenderingContext || !F.zP(t, {
            stencil: p,
            preserveDrawingBuffer: p
        })))0 == g ? s = F.Aa : F.iy = r;
    if (s == F.Aa)try {
        t.getContext("2d")
    } catch (w) {
        F.iy = r
    }
    F.B = s;
    try {
        d.jy = !!new (h.AudioContext || h.webkitAudioContext || h.mozAudioContext)
    } catch (x) {
        d.jy = r
    }
    try {
        var y = d.localStorage = h.localStorage;
        y.setItem("storage", "");
        y.removeItem("storage");
        y = q
    } catch (B) {
        ("SECURITY_ERR" === B.name || "QuotaExceededError" === B.name) && F.warn("Warning: localStorage isn't enabled. Please confirm browser cookie or privacy option"), d.localStorage = u()
    }
    y = d.UF = {canvas: p};
    F.B == F.Y && (y.opengl = p);
    if (n.ontouchstart !== k || m.msPointerEnabled)y.touches = p;
    n.onmouseup !== k && (y.mouse = p);
    n.onkeyup !== k && (y.keyboard = p);
    if (h.DeviceMotionEvent || h.DeviceOrientationEvent)y.accelerometer = p;
    d.yfa = u();
    d.Bea = u();
    d.Hma = u();
    d.zea = function () {
        var a;
        a = "" + ("isMobile : " + this.nh + "\r\n");
        a += "language : " + this.language + "\r\n";
        a += "browserType : " + this.$h + "\r\n";
        a += "capabilities : " + JSON.stringify(this.UF) + "\r\n";
        a += "os : " + this.Tl + "\r\n";
        a += "platform : " + this.platform + "\r\n";
        F.log(a)
    }
};
F.aba = 0;
F.bba = 1;
F.Zaa = 2;
F.$aa = 3;
F.Ge = q;
F.l = q;
F.lb = q;
F.wZ = q;
F.Tx = r;
F.yO = r;
F.j1 = function (a) {
    var c, d;
    if (!F.yO) {
        F.yO = p;
        var e = window, f = new Date, g = 1E3 / F.Qb.Re[F.Qb.Dg.Qt], h = function (a) {
            var c = (new Date).getTime(), d = Math.max(0, g - (c - f)), e = window.setTimeout(function () {
                a()
            }, d);
            f = c + d;
            return e
        }, m = function (a) {
            clearTimeout(a)
        };
        F.ta.Tl === F.ta.zo && F.ta.$h === F.ta.IA ? (e.Xz = h, e.cancelAnimationFrame = m) : 60 != F.Qb.Re[F.Qb.Dg.Qt] ? (e.Xz = h, e.cancelAnimationFrame = m) : (e.Xz = e.requestAnimationFrame || e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || e.oRequestAnimationFrame || e.msRequestAnimationFrame ||
            h, e.cancelAnimationFrame = window.cancelAnimationFrame || window.cancelRequestAnimationFrame || window.msCancelRequestAnimationFrame || window.mozCancelRequestAnimationFrame || window.oCancelRequestAnimationFrame || window.webkitCancelRequestAnimationFrame || window.msCancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame || window.oCancelAnimationFrame || m);
        m = F.Gb(a) || F.Gb("#" + a);
        "CANVAS" == m.tagName ? (c = c || m.width, d = d || m.height, h = F.Dn = F.bc("DIV"), a = F.lb = m, a.parentNode.insertBefore(h,
            a), a.fP(h), h.setAttribute("id", "Cocos2dGameContainer")) : ("DIV" != m.tagName && F.log("Warning: target element is not a DIV or CANVAS"), c = c || m.clientWidth, d = d || m.clientHeight, h = F.Dn = m, a = F.lb = F.Gb(F.bc("CANVAS")), m.appendChild(a));
        a.dP("gameCanvas");
        a.setAttribute("width", c || 480);
        a.setAttribute("height", d || 320);
        a.setAttribute("tabindex", 99);
        a.style.outline = "none";
        m = h.style;
        m.width = (c || 480) + "px";
        m.height = (d || 320) + "px";
        m.margin = "0 auto";
        m.position = "relative";
        m.overflow = "hidden";
        h.top = "100%";
        F.B == F.Y && (F.l =
            F.Spa = F.zP(a, {stencil: p, preserveDrawingBuffer: p, antialias: !F.ta.nh, alpha: r}));
        F.l ? (e.gl = F.l, F.Ge = new F.nU(F.l), F.Tx = p, F.Ra.t_(), F.le.Ps()) : (F.l = a.getContext("2d"), F.Eca = F.l, F.l.translate(0, a.height), F.Ge = F.QI ? new F.QI(F.l) : q);
        F.wZ = h;
        F.log(F.VA);
        F.K0();
        F.ta.nh && (c = F.bc("style"), c.type = "text/css", document.body.appendChild(c), c.textContent = "body,canvas,div{ -moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;-khtml-user-select: none;-webkit-tap-highlight-color:rgba(0,0,0,0);}");
        F.view =
            F.TA.mD();
        F.WQ.s7(F.lb);
        F.L = F.jb.mD();
        F.L.mA && F.L.mA(F.view);
        F.dv = F.L.Pa();
        F.Qma = new F.Wv;
        F.e7 = new F.VV
    }
};
F.RC = function () {
    F.B !== F.Y && b("This feature supports WebGL render mode only.")
};
F.pN = r;
F.K0 = function () {
    F.pN = r;
    F.lb.oncontextmenu = function () {
        if (!F.pN)return r
    }
};
F.Qb = {
    ZT: 0,
    XT: 1,
    R$: 2,
    II: 3,
    YT: 4,
    S$: 5,
    WT: 6,
    WA: "game_on_hide",
    nv: "game_on_show",
    Fs: q,
    Gs: q,
    Ica: [],
    Dg: {
        pG: "engineDir",
        kea: "dependencies",
        Mt: "debugMode",
        d9: "showFPS",
        Qt: "frameRate",
        id: "id",
        Iu: "renderMode",
        Q5: "jsList",
        K2: "classReleaseMode"
    },
    UN: r,
    Px: r,
    Gc: p,
    Lm: q,
    Re: q,
    uR: q,
    qla: q,
    Cna: function (a) {
        this.Re[this.Dg.Qt] = a;
        this.Lm && window.cancelAnimationFrame(this.Lm);
        this.Gc = p;
        this.EE()
    },
    EE: function () {
        var a = this, c, d = F.L;
        d.g8(a.Re[a.Dg.d9]);
        c = function () {
            a.Gc || (d.H6(), a.Lm && window.cancelAnimationFrame(a.Lm), a.Lm = window.Xz(c))
        };
        window.Xz(c);
        a.Gc = r
    },
    P7: function (a) {
        function c() {
            a && (d.Re[d.Dg.id] = a);
            d.UN || d.i7(function () {
                d.Px = p
            });
            F.iy && (d.MY = setInterval(function () {
                d.Px && (F.j1(d.Re[d.Dg.id]), d.EE(), d.Fs = d.Fs || new F.mm(d.WA), d.Fs.setUserData(d), d.Gs = d.Gs || new F.mm(d.nv), d.Gs.setUserData(d), d.uR(), clearInterval(d.MY))
            }, 10))
        }

        var d = this;
        document.body ? c() : F.Sa(window, "load", function () {
            this.removeEventListener("load", arguments.callee, r);
            c()
        }, r)
    },
    k_: function () {
        function a(a) {
            a[c.pG] = a[c.pG] || "frameworks/cocos2d-html5";
            a[c.Mt] == q && (a[c.Mt] =
                0);
            a[c.Qt] = a[c.Qt] || 60;
            a[c.Iu] == q && (a[c.Iu] = 1);
            return a
        }

        var c = this.Dg;
        if (document.ccConfig)this.Re = a(document.ccConfig); else try {
            for (var d = document.getElementsByTagName("script"), e = 0; e < d.length; e++) {
                var f = d[e].getAttribute("cocos");
                if ("" == f || f)break
            }
            var g, h, m;
            if (e < d.length) {
                if (g = d[e].src)m = /(.*)\//.exec(g)[0], F.aa.Yz = m, g = F.path.join(m, "project.json");
                h = F.aa.FN(g)
            }
            h || (h = F.aa.FN("project.json"));
            var n = JSON.parse(h);
            this.Re = a(n || {})
        } catch (s) {
            F.log("Failed to read or parse project.json"), this.Re = a({})
        }
        F.q_(this.Re,
            c)
    },
    A_: {},
    $M: function (a, c, d) {
        var e = this.A_;
        if (e[c])return q;
        d = d || "";
        var f = [], g = a[c];
        g || b("can not find module [" + c + "]");
        c = F.path;
        for (var h = 0, m = g.length; h < m; h++) {
            var n = g[h];
            if (!e[n]) {
                var s = c.Hl(n);
                s ? ".js" == s.toLowerCase() && f.push(c.join(d, n)) : (s = this.$M(a, n, d)) && (f = f.concat(s));
                e[n] = 1
            }
        }
        return f
    },
    i7: function (a) {
        var c = this, d = c.Re, e = c.Dg, f = d[e.pG], g = F.aa;
        F.iy || b("The renderer doesn't support the renderMode " + d[e.Iu]);
        c.UN = p;
        var h = d[e.Q5] || [];
        F.za ? g.oR("", h, function (d) {
            d && b(d);
            c.Px = p;
            a && a()
        }) : (e = F.path.join(f,
            "moduleConfig.json"), g.pR(e, function (e, g) {
            e && b(e);
            var s = d.modules || [], t = g.module, v = [];
            F.B == F.Y ? s.splice(0, 0, "shaders") : 0 > s.indexOf("core") && s.splice(0, 0, "core");
            for (var w = 0, x = s.length; w < x; w++) {
                var y = c.$M(t, s[w], f);
                y && (v = v.concat(y))
            }
            v = v.concat(h);
            F.aa.oR(v, function (d) {
                d && b(d);
                c.Px = p;
                a && a()
            })
        }))
    }
};
F.Qb.k_();
Function.prototype.bind = Function.prototype.bind || function (a) {
        function c() {
            return f.apply(this instanceof d && a ? this : a, e.concat(Array.prototype.slice.call(arguments)))
        }

        function d() {
        }

        F.ac(this) || b(new TypeError("Function.prototype.bind - what is trying to be bound is not callable"));
        var e = Array.prototype.slice.call(arguments, 1), f = this;
        d.prototype = this.prototype;
        c.prototype = new d;
        return c
    };
F.i = {
    sI: "cc.ActionManager.addAction(): action must be non-null",
    pT: "cocos2d: removeAction: Target not found",
    t$: "cc.ActionManager.removeActionByTag(): an invalid tag",
    u$: "cc.ActionManager.removeActionByTag(): target must be non-null",
    nT: "cc.ActionManager.getActionByTag(): an invalid tag",
    oT: "cocos2d : getActionByTag(tag \x3d %s): Action not found",
    O2: "cocos2d: **** WARNING **** CC_ENABLE_PROFILERS is defined. Disable it when you finish profiling (from ccConfig.js)",
    P2: "Expected 'data' dict, but not found. Config file: %s",
    Q2: "Please load the resource first : %s",
    gU: "cocos2d: Director: Error in gettimeofday",
    hU: "cocos2d: Director: unrecognized projection",
    haa: "cocos2d: Director: unrecognized projection",
    fU: "cocos2d: Director: Error in gettimeofday",
    eU: "running scene should not null",
    NI: "the scene should not null",
    Gt: "element type is wrong!",
    yW: "CCSheduler#scheduleCallback. Callback already scheduled. Updating interval from:%s to %s",
    zW: "cc.scheduler.scheduleCallbackForTarget(): callback_fn should be non-null.",
    AW: "cc.scheduler.scheduleCallbackForTarget(): target should be non-null.",
    wW: "cc.Scheduler.pauseTarget():target should be non-null",
    xW: "cc.Scheduler.resumeTarget():target should be non-null",
    vW: "cc.Scheduler.isTargetPaused():target should be non-null",
    uV: "getZOrder is deprecated. Please use getLocalZOrder instead.",
    EV: "setZOrder is deprecated. Please use setLocalZOrder instead.",
    sV: "RotationX !\x3d RotationY. Don't know which one to return",
    tV: "ScaleX !\x3d ScaleY. Don't know which one to return",
    Vaa: "An Node can't be added as a child of itself.",
    Waa: "child already added. It can't be added again",
    pV: "child must be non-null",
    yV: "removeFromParentAndCleanup is deprecated. Use removeFromParent instead",
    qV: "boundingBox is deprecated. Use getBoundingBox instead",
    wV: "argument tag is an invalid tag",
    xV: "cocos2d: removeChildByTag(tag \x3d %s): child not found!",
    Yaa: "removeAllChildrenWithCleanup is deprecated. Use removeAllChildren instead",
    FV: "cc.Node.stopActionBy(): argument tag an invalid tag",
    rV: "cc.Node.getActionByTag(): argument tag is an invalid tag",
    AV: "resumeSchedulerAndActions is deprecated, please use resume instead.",
    vV: "pauseSchedulerAndActions is deprecated, please use pause instead.",
    oV: "Unknown callback function",
    zV: "child must be non-null",
    BV: "cc.Node.runAction(): action must be non-null",
    CV: "callback function must be non-null",
    DV: "interval must be positive",
    Xaa: "cocos2d: Could not initialize cc.AtlasNode. Invalid Texture.",
    wT: "cc.AtlasNode.updateAtlasValues(): Shall be overridden in subclasses",
    x$: "",
    vI: "cocos2d: Could not initialize cc.AtlasNode. Invalid Texture.",
    hY: "cc._EventListenerKeyboard.checkAvailable(): Invalid EventListenerKeyboard!",
    jY: "cc._EventListenerTouchOneByOne.checkAvailable(): Invalid EventListenerTouchOneByOne!",
    iY: "cc._EventListenerTouchAllAtOnce.checkAvailable(): Invalid EventListenerTouchAllAtOnce!",
    gY: "cc._EventListenerAcceleration.checkAvailable(): _onAccelerationEvent must be non-nil",
    vU: "Invalid parameter.",
    ww: "Don't call this method if the event is for touch.",
    v3: "Invalid scene graph priority!",
    w3: "0 priority is forbidden for fixed priority since it's used for scene graph based priority.",
    A3: "Invalid listener type!",
    B3: "Can't set fixed priority with scene graph based listener.",
    x3: "Invalid parameters.",
    y3: "listener must be a cc.EventListener object when adding a fixed priority listener",
    z3: "The listener has been registered, please don't register it again.",
    fV: "parameters should not be ending with null in Javascript",
    gV: "Invalid index in MultiplexLayer switchTo message",
    hV: "Invalid index in MultiplexLayer switchTo message",
    eV: "cc.Layer.addLayer(): layer should be non-null",
    oU: "Resolution not valid",
    pU: "should set resolutionPolicy",
    F5: "The touches is more than MAX_TOUCHES, nUnusedIndex \x3d %s",
    OS: "cc.swap is being modified from original macro, please check usage",
    jq: "WebGL error %s",
    i2: "cocos2d: cc.AnimationCache: No animations were found in provided dictionary.",
    j2: "cc.AnimationCache. Invalid animation format",
    q2: "cc.AnimationCache.addAnimations(): File could not be found",
    k2: "cocos2d: cc.AnimationCache: Animation '%s' found in dictionary without any frames - cannot add to animation cache.",
    l2: "cocos2d: cc.AnimationCache: Animation '%s' refers to frame '%s' which is not currently in the cc.SpriteFrameCache. This frame will not be added to the animation.",
    m2: "cocos2d: cc.AnimationCache: None of the frames for animation '%s' were found in the cc.SpriteFrameCache. Animation is not being added to the Animation Cache.",
    n2: "cocos2d: cc.AnimationCache: An animation in your dictionary refers to a frame which is not in the cc.SpriteFrameCache. Some or all of the frames for the animation '%s' may be missing.",
    o2: "cocos2d: CCAnimationCache: Animation '%s' found in dictionary without any frames - cannot add to animation cache.",
    p2: "cocos2d: cc.AnimationCache: Animation '%s' refers to frame '%s' which is not currently in the cc.SpriteFrameCache. This frame will not be added to the animation.",
    r2: "cc.AnimationCache.addAnimations(): Invalid texture file name",
    $W: "cc.Sprite.reorderChild(): this child is not in children list",
    SW: "cc.Sprite.ignoreAnchorPointForPosition(): it is invalid in cc.Sprite when using SpriteBatchNode",
    cX: "cc.Sprite.setDisplayFrameWithAnimationName(): Frame not found",
    dX: "cc.Sprite.setDisplayFrameWithAnimationName(): Invalid frame index",
    bX: "setDisplayFrame is deprecated, please use setSpriteFrame instead.",
    LW: "cc.Sprite._updateBlendFunc(): _updateBlendFunc doesn't work when the sprite is rendered using a cc.CCSpriteBatchNode",
    UW: "cc.Sprite.initWithSpriteFrame(): spriteFrame should be non-null",
    VW: "cc.Sprite.initWithSpriteFrameName(): spriteFrameName should be non-null",
    WW: " is null, please check.",
    TW: "cc.Sprite.initWithFile(): filename should be non-null",
    eX: "cc.Sprite.setDisplayFrameWithAnimationName(): animationName must be non-null",
    aX: "cc.Sprite.reorderChild(): child should be non-null",
    MW: "cc.Sprite.addChild(): cc.Sprite only supports cc.Sprites as children when using cc.SpriteBatchNode",
    NW: "cc.Sprite.addChild(): cc.Sprite only supports a sprite using same texture as children when using cc.SpriteBatchNode",
    OW: "cc.Sprite.addChild(): child should be non-null",
    gX: "cc.Sprite.texture setter: Batched sprites should use the same texture as the batchnode",
    oba: "cc.SpriteBatchNode.updateQuadFromSprite(): cc.SpriteBatchNode only supports cc.Sprites as children",
    YW: "cc.SpriteBatchNode.insertQuadFromSprite(): cc.SpriteBatchNode only supports cc.Sprites as children",
    PW: "cc.SpriteBatchNode.addChild(): cc.SpriteBatchNode only supports cc.Sprites as children",
    QW: "cc.SpriteBatchNode.addChild(): cc.Sprite is not using the same texture",
    XW: "Sprite.initWithTexture(): Argument must be non-nil ",
    fX: "Invalid spriteFrameName",
    hX: "Invalid argument: cc.Sprite.texture setter expects a CCTexture2D.",
    pba: "cc.SpriteBatchNode.updateQuadFromSprite(): sprite should be non-null",
    ZW: "cc.SpriteBatchNode.insertQuadFromSprite(): sprite should be non-null",
    RW: "cc.SpriteBatchNode.addChild(): child should be non-null",
    EW: "cc.SpriteBatchNode.addQuadFromSprite(): SpriteBatchNode only supports cc.Sprites as children",
    GW: "cocos2d: CCSpriteBatchNode: resizing TextureAtlas capacity from %s to %s.",
    HW: "cocos2d: WARNING: Not enough memory to resize the atlas",
    JW: "cc.SpriteBatchNode.addChild(): Child doesn't belong to Sprite",
    IW: "cc.SpriteBatchNode.addChild(): sprite batch node should contain the child",
    FW: "cc.SpriteBatchNode.addQuadFromSprite(): child should be non-null",
    KW: "cc.SpriteBatchNode.addChild():child should be non-null",
    l9: "cocos2d: WARNING: originalWidth/Height not found on the cc.SpriteFrame. AnchorPoint won't work as expected. Regenrate the .plist",
    n9: "cocos2d: WARNING: an alias with name %s already exists",
    k9: "cocos2d: WARNING: Sprite frame: %s has already been added by another source, please fix name conflit",
    q9: "cocos2d: cc.SpriteFrameCahce: Frame %s not found",
    m9: "Please load the resource first : %s",
    o9: "cc.SpriteFrameCache.addSpriteFrames(): plist should be non-null",
    p9: "Argument must be non-nil",
    LA: "cc.SpriteBatchNode.updateQuadFromSprite(): cc.SpriteBatchNode only supports cc.Sprites as children",
    JT: "cc.SpriteBatchNode.insertQuadFromSprite(): cc.SpriteBatchNode only supports cc.Sprites as children",
    FT: "cc.SpriteBatchNode.addChild(): cc.SpriteBatchNode only supports cc.Sprites as children",
    IT: "Sprite.initWithTexture(): Argument must be non-nil ",
    GT: "cc.Sprite.addChild(): child should be non-null",
    LT: "Invalid spriteFrameName",
    MT: "Invalid argument: cc.Sprite texture setter expects a CCTexture2D.",
    NT: "cc.SpriteBatchNode.updateQuadFromSprite(): sprite should be non-null",
    KT: "cc.SpriteBatchNode.insertQuadFromSprite(): sprite should be non-null",
    HT: "cc.SpriteBatchNode.addChild(): child should be non-null",
    EX: "cocos2d: Could not open file: %s",
    pK: "cc.TextureAtlas.insertQuad(): invalid totalQuads",
    FX: "cc.TextureAtlas.initWithTexture():texture should be non-null",
    PX: "cc.TextureAtlas.updateQuad(): quad should be non-null",
    QX: "cc.TextureAtlas.updateQuad(): Invalid index",
    IX: "cc.TextureAtlas.insertQuad(): Invalid index",
    JX: "cc.TextureAtlas.insertQuad(): Invalid index + amount",
    GX: "cc.TextureAtlas.insertQuadFromIndex(): Invalid newIndex",
    HX: "cc.TextureAtlas.insertQuadFromIndex(): Invalid fromIndex",
    NX: "cc.TextureAtlas.removeQuadAtIndex(): Invalid index",
    OX: "cc.TextureAtlas.removeQuadsAtIndex(): index + amount out of bounds",
    KX: "cc.TextureAtlas.moveQuadsFromIndex(): move is out of bounds",
    LX: "cc.TextureAtlas.moveQuadsFromIndex(): Invalid newIndex",
    MX: "cc.TextureAtlas.moveQuadsFromIndex(): Invalid oldIndex",
    B9: "TextureCache:addPVRTCImage does not support on HTML5",
    z9: "TextureCache:addPVRTCImage does not support on HTML5",
    G9: "textureForKey is deprecated. Please use getTextureForKey instead.",
    A9: "addPVRImage does not support on HTML5",
    C9: "cocos2d: Couldn't add UIImage in TextureCache",
    E9: "cocos2d: '%s' id\x3d%s %s x %s",
    QS: "cocos2d: '%s' id\x3d HTMLCanvasElement %s x %s",
    F9: "cocos2d: TextureCache dumpDebugInfo: %s textures, HTMLCanvasElement for %s KB (%s MB)",
    D9: "cc.Texture.addUIImage(): image should be non-null",
    vX: "initWithETCFile does not support on HTML5",
    zX: "initWithPVRFile does not support on HTML5",
    BX: "initWithPVRTCData does not support on HTML5",
    rX: "cc.Texture.addImage(): path should be non-null",
    xX: "cocos2d: cc.Texture2D. Can't create Texture. UIImage is nil",
    yX: "cocos2d: WARNING: Image (%s x %s) is bigger than the supported %s x %s",
    DX: "initWithString isn't supported on cocos2d-html5",
    wX: "initWithETCFile does not support on HTML5",
    AX: "initWithPVRFile does not support on HTML5",
    CX: "initWithPVRTCData does not support on HTML5",
    tX: "bitsPerPixelForFormat: %s, cannot give useful result, it's a illegal pixel format",
    qX: "cocos2d: cc.Texture2D: Using RGB565 texture since image has no alpha",
    sX: "cc.Texture.addImage(): path should be non-null",
    uX: "NSInternalInconsistencyException",
    Hd: "Missing file: %s",
    HR: "cc.radiansToDegress() should be called cc.radiansToDegrees()",
    MB: "Rect width exceeds maximum margin: %s",
    KB: "Rect height exceeds maximum margin: %s",
    wU: "If program goes here, there should be event in dispatch.",
    xU: "_inDispatch should be 1 here."
};
F.K_ = function (a) {
    if (F.lb) {
        var c = F.J_, d = document;
        if (!c) {
            var e = d.createElement("Div"), c = e.style;
            e.setAttribute("id", "logInfoDiv");
            F.lb.parentNode.appendChild(e);
            e.setAttribute("width", "200");
            e.setAttribute("height", F.lb.height);
            c.zIndex = "99999";
            c.position = "absolute";
            c.top = "0";
            c.left = "0";
            c = F.J_ = d.createElement("textarea");
            d = c.style;
            c.setAttribute("rows", "20");
            c.setAttribute("cols", "30");
            c.setAttribute("disabled", p);
            e.appendChild(c);
            d.backgroundColor = "transparent";
            d.borderBottom = "1px solid #cccccc";
            d.borderRightWidth =
                "0px";
            d.borderLeftWidth = "0px";
            d.borderTopWidth = "0px";
            d.borderTopStyle = "none";
            d.borderRightStyle = "none";
            d.borderLeftStyle = "none";
            d.padding = "0px";
            d.margin = 0
        }
        c.value = c.value + a + "\r\n";
        c.scrollTop = c.scrollHeight
    }
};
F.RM = function (a) {
    if (F.Gq(a))try {
        return JSON.stringify(a)
    } catch (c) {
        return ""
    } else return a
};
F.l_ = function () {
    var a = F.Qb.Re[F.Qb.Dg.Mt], c = F.Qb;
    if (a != c.ZT) {
        var d;
        a > c.II ? (d = F.K_.bind(F), F.error = function () {
            d("ERROR :  " + F.tG.apply(F, arguments))
        }, F.assert = function (a, c) {
            if (!a && c) {
                for (var g = 2; g < arguments.length; g++)c = c.replace(/(%s)|(%d)/, F.RM(arguments[g]));
                d("Assert: " + c)
            }
        }, a != c.WT && (F.warn = function () {
            d("WARN :  " + F.tG.apply(F, arguments))
        }), a == c.YT && (F.log = function () {
            d(F.tG.apply(F, arguments))
        })) : console && (F.error = function () {
            return console.error.apply(console, arguments)
        }, F.assert = function (a, c) {
            if (!a &&
                c) {
                for (var d = 2; d < arguments.length; d++)c = c.replace(/(%s)|(%d)/, F.RM(arguments[d]));
                b(c)
            }
        }, a != c.II && (F.warn = function () {
            return console.warn.apply(console, arguments)
        }), a == c.XT && (F.log = function () {
            return console.log.apply(console, arguments)
        }))
    }
};
F.l_();
F.aa.z6 = function (a, c) {
    var d = this, e = this.az(), f = "load " + a + " failed!";
    e.open("GET", a, p);
    /msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent) ? (e.setRequestHeader("Accept-Charset", "x-user-defined"), e.onreadystatechange = function () {
        if (4 == e.readyState && 200 == e.status) {
            var a = F.FL(e.responseBody);
            c(q, d.fy(a))
        } else c(f)
    }) : (e.overrideMimeType && e.overrideMimeType("text/plain; charset\x3dx-user-defined"), e.onload = function () {
        4 == e.readyState && 200 == e.status ? c(q, d.fy(e.responseText)) : c(f)
    });
    e.send(q)
};
F.aa.fy = function (a) {
    if (!a)return q;
    for (var c = new Uint8Array(a.length), d = 0; d < a.length; d++)c[d] = a.charCodeAt(d) & 255;
    return c
};
F.aa.Qka = function (a) {
    var c = this.az(), d = "load " + a + " failed!";
    c.open("GET", a, r);
    a = q;
    if (/msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent)) {
        c.setRequestHeader("Accept-Charset", "x-user-defined");
        c.send(q);
        if (200 != c.status)return F.log(d), q;
        (c = F.FL(c.responseBody)) && (a = this.fy(c))
    } else {
        c.overrideMimeType && c.overrideMimeType("text/plain; charset\x3dx-user-defined");
        c.send(q);
        if (200 != c.status)return F.log(d), q;
        a = this.fy(c.responseText)
    }
    return a
};
var Uint8Array = Uint8Array || Array;
if (/msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent)) {
    var da = F.bc("script");
    da.type = "text/vbscript";
    da.textContent = '\x3c!-- IEBinaryToArray_ByteStr --\x3e\r\nFunction IEBinaryToArray_ByteStr(Binary)\r\n   IEBinaryToArray_ByteStr \x3d CStr(Binary)\r\nEnd Function\r\nFunction IEBinaryToArray_ByteStr_Last(Binary)\r\n   Dim lastIndex\r\n   lastIndex \x3d LenB(Binary)\r\n   if lastIndex mod 2 Then\r\n       IEBinaryToArray_ByteStr_Last \x3d Chr( AscB( MidB( Binary, lastIndex, 1 ) ) )\r\n   Else\r\n       IEBinaryToArray_ByteStr_Last \x3d ""\r\n   End If\r\nEnd Function\r\n';
    document.body.appendChild(da);
    F.FL = function (a) {
        for (var c = {}, d = 0; 256 > d; d++)for (var e = 0; 256 > e; e++)c[String.fromCharCode(d + 256 * e)] = String.fromCharCode(d) + String.fromCharCode(e);
        d = IEBinaryToArray_ByteStr(a);
        a = IEBinaryToArray_ByteStr_Last(a);
        return d.replace(/[\s\S]/g, function (a) {
                return c[a]
            }) + a
    }
}
;F = F || {};
F.GN = "data:image/gif;base64,R0lGODlhEAAQALMNAD8/P7+/vyoqKlVVVX9/fxUVFUBAQGBgYMDAwC8vL5CQkP///wAAAP///wAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFAAANACwAAAAAEAAQAAAEO5DJSau9OOvNex0IMnDIsiCkiW6g6BmKYlBFkhSUEgQKlQCARG6nEBwOgl+QApMdCIRD7YZ5RjlGpCUCACH5BAUAAA0ALAAAAgAOAA4AAAQ6kLGB0JA4M7QW0hrngRllkYyhKAYqKUGguAws0ypLS8JxCLQDgXAIDg+FRKIA6v0SAECCBpXSkstMBAAh+QQFAAANACwAAAAACgAQAAAEOJDJORAac6K1kDSKYmydpASBUl0mqmRfaGTCcQgwcxDEke+9XO2WkxQSiUIuAQAkls0n7JgsWq8RACH5BAUAAA0ALAAAAAAOAA4AAAQ6kMlplDIzTxWC0oxwHALnDQgySAdBHNWFLAvCukc215JIZihVIZEogDIJACBxnCSXTcmwGK1ar1hrBAAh+QQFAAANACwAAAAAEAAKAAAEN5DJKc4RM+tDyNFTkSQF5xmKYmQJACTVpQSBwrpJNteZSGYoFWjIGCAQA2IGsVgglBOmEyoxIiMAIfkEBQAADQAsAgAAAA4ADgAABDmQSVZSKjPPBEDSGucJxyGA1XUQxAFma/tOpDlnhqIYN6MEAUXvF+zldrMBAjHoIRYLhBMqvSmZkggAIfkEBQAADQAsBgAAAAoAEAAABDeQyUmrnSWlYhMASfeFVbZdjHAcgnUQxOHCcqWylKEohqUEAYVkgEAMfkEJYrFA6HhKJsJCNFoiACH5BAUAAA0ALAIAAgAOAA4AAAQ3kMlJq704611SKloCAEk4lln3DQgyUMJxCBKyLAh1EMRR3wiDQmHY9SQslyIQUMRmlmVTIyRaIgA7";
F.jp = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAAgCAYAAAD9qabkAAAKQ2lDQ1BJQ0MgcHJvZmlsZQAAeNqdU3dYk/cWPt/3ZQ9WQtjwsZdsgQAiI6wIyBBZohCSAGGEEBJAxYWIClYUFRGcSFXEgtUKSJ2I4qAouGdBiohai1VcOO4f3Ke1fXrv7e371/u855zn/M55zw+AERImkeaiagA5UoU8Otgfj09IxMm9gAIVSOAEIBDmy8JnBcUAAPADeXh+dLA//AGvbwACAHDVLiQSx+H/g7pQJlcAIJEA4CIS5wsBkFIAyC5UyBQAyBgAsFOzZAoAlAAAbHl8QiIAqg0A7PRJPgUA2KmT3BcA2KIcqQgAjQEAmShHJAJAuwBgVYFSLALAwgCgrEAiLgTArgGAWbYyRwKAvQUAdo5YkA9AYACAmUIszAAgOAIAQx4TzQMgTAOgMNK/4KlfcIW4SAEAwMuVzZdL0jMUuJXQGnfy8ODiIeLCbLFCYRcpEGYJ5CKcl5sjE0jnA0zODAAAGvnRwf44P5Dn5uTh5mbnbO/0xaL+a/BvIj4h8d/+vIwCBAAQTs/v2l/l5dYDcMcBsHW/a6lbANpWAGjf+V0z2wmgWgrQevmLeTj8QB6eoVDIPB0cCgsL7SViob0w44s+/zPhb+CLfvb8QB7+23rwAHGaQJmtwKOD/XFhbnauUo7nywRCMW735yP+x4V//Y4p0eI0sVwsFYrxWIm4UCJNx3m5UpFEIcmV4hLpfzLxH5b9CZN3DQCshk/ATrYHtctswH7uAQKLDljSdgBAfvMtjBoLkQAQZzQyefcAAJO/+Y9AKwEAzZek4wAAvOgYXKiUF0zGCAAARKCBKrBBBwzBFKzADpzBHbzAFwJhBkRADCTAPBBCBuSAHAqhGJZBGVTAOtgEtbADGqARmuEQtMExOA3n4BJcgetwFwZgGJ7CGLyGCQRByAgTYSE6iBFijtgizggXmY4EImFINJKApCDpiBRRIsXIcqQCqUJqkV1II/ItchQ5jVxA+pDbyCAyivyKvEcxlIGyUQPUAnVAuagfGorGoHPRdDQPXYCWomvRGrQePYC2oqfRS+h1dAB9io5jgNExDmaM2WFcjIdFYIlYGibHFmPlWDVWjzVjHVg3dhUbwJ5h7wgkAouAE+wIXoQQwmyCkJBHWExYQ6gl7CO0EroIVwmDhDHCJyKTqE+0JXoS+cR4YjqxkFhGrCbuIR4hniVeJw4TX5NIJA7JkuROCiElkDJJC0lrSNtILaRTpD7SEGmcTCbrkG3J3uQIsoCsIJeRt5APkE+S+8nD5LcUOsWI4kwJoiRSpJQSSjVlP+UEpZ8yQpmgqlHNqZ7UCKqIOp9aSW2gdlAvU4epEzR1miXNmxZDy6Qto9XQmmlnafdoL+l0ugndgx5Fl9CX0mvoB+nn6YP0dwwNhg2Dx0hiKBlrGXsZpxi3GS+ZTKYF05eZyFQw1zIbmWeYD5hvVVgq9ip8FZHKEpU6lVaVfpXnqlRVc1U/1XmqC1SrVQ+rXlZ9pkZVs1DjqQnUFqvVqR1Vu6k2rs5Sd1KPUM9RX6O+X/2C+mMNsoaFRqCGSKNUY7fGGY0hFsYyZfFYQtZyVgPrLGuYTWJbsvnsTHYF+xt2L3tMU0NzqmasZpFmneZxzQEOxrHg8DnZnErOIc4NznstAy0/LbHWaq1mrX6tN9p62r7aYu1y7Rbt69rvdXCdQJ0snfU6bTr3dQm6NrpRuoW623XP6j7TY+t56Qn1yvUO6d3RR/Vt9KP1F+rv1u/RHzcwNAg2kBlsMThj8MyQY+hrmGm40fCE4agRy2i6kcRoo9FJoye4Ju6HZ+M1eBc+ZqxvHGKsNN5l3Gs8YWJpMtukxKTF5L4pzZRrmma60bTTdMzMyCzcrNisyeyOOdWca55hvtm82/yNhaVFnMVKizaLx5balnzLBZZNlvesmFY+VnlW9VbXrEnWXOss623WV2xQG1ebDJs6m8u2qK2brcR2m23fFOIUjynSKfVTbtox7PzsCuya7AbtOfZh9iX2bfbPHcwcEh3WO3Q7fHJ0dcx2bHC866ThNMOpxKnD6VdnG2ehc53zNRemS5DLEpd2lxdTbaeKp26fesuV5RruutK10/Wjm7ub3K3ZbdTdzD3Ffav7TS6bG8ldwz3vQfTw91jicczjnaebp8LzkOcvXnZeWV77vR5Ps5wmntYwbcjbxFvgvct7YDo+PWX6zukDPsY+Ap96n4e+pr4i3z2+I37Wfpl+B/ye+zv6y/2P+L/hefIW8U4FYAHBAeUBvYEagbMDawMfBJkEpQc1BY0FuwYvDD4VQgwJDVkfcpNvwBfyG/ljM9xnLJrRFcoInRVaG/owzCZMHtYRjobPCN8Qfm+m+UzpzLYIiOBHbIi4H2kZmRf5fRQpKjKqLupRtFN0cXT3LNas5Fn7Z72O8Y+pjLk722q2cnZnrGpsUmxj7Ju4gLiquIF4h/hF8ZcSdBMkCe2J5MTYxD2J43MC52yaM5zkmlSWdGOu5dyiuRfm6c7Lnnc8WTVZkHw4hZgSl7I/5YMgQlAvGE/lp25NHRPyhJuFT0W+oo2iUbG3uEo8kuadVpX2ON07fUP6aIZPRnXGMwlPUit5kRmSuSPzTVZE1t6sz9lx2S05lJyUnKNSDWmWtCvXMLcot09mKyuTDeR55m3KG5OHyvfkI/lz89sVbIVM0aO0Uq5QDhZML6greFsYW3i4SL1IWtQz32b+6vkjC4IWfL2QsFC4sLPYuHhZ8eAiv0W7FiOLUxd3LjFdUrpkeGnw0n3LaMuylv1Q4lhSVfJqedzyjlKD0qWlQyuCVzSVqZTJy26u9Fq5YxVhlWRV72qX1VtWfyoXlV+scKyorviwRrjm4ldOX9V89Xlt2treSrfK7etI66Trbqz3Wb+vSr1qQdXQhvANrRvxjeUbX21K3nShemr1js20zcrNAzVhNe1bzLas2/KhNqP2ep1/XctW/a2rt77ZJtrWv913e/MOgx0VO97vlOy8tSt4V2u9RX31btLugt2PGmIbur/mft24R3dPxZ6Pe6V7B/ZF7+tqdG9s3K+/v7IJbVI2jR5IOnDlm4Bv2pvtmne1cFoqDsJB5cEn36Z8e+NQ6KHOw9zDzd+Zf7f1COtIeSvSOr91rC2jbaA9ob3v6IyjnR1eHUe+t/9+7zHjY3XHNY9XnqCdKD3x+eSCk+OnZKeenU4/PdSZ3Hn3TPyZa11RXb1nQ8+ePxd07ky3X/fJ897nj13wvHD0Ivdi2yW3S609rj1HfnD94UivW2/rZffL7Vc8rnT0Tes70e/Tf/pqwNVz1/jXLl2feb3vxuwbt24m3Ry4Jbr1+Hb27Rd3Cu5M3F16j3iv/L7a/eoH+g/qf7T+sWXAbeD4YMBgz8NZD+8OCYee/pT/04fh0kfMR9UjRiONj50fHxsNGr3yZM6T4aeypxPPyn5W/3nrc6vn3/3i+0vPWPzY8Av5i8+/rnmp83Lvq6mvOscjxx+8znk98ab8rc7bfe+477rfx70fmSj8QP5Q89H6Y8en0E/3Pud8/vwv94Tz+4A5JREAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfcAgcQLxxUBNp/AAAQZ0lEQVR42u2be3QVVZbGv1N17829eRLyIKAEOiISEtPhJTJAYuyBDmhWjAEx4iAGBhxA4wABbVAMWUAeykMCM+HRTcBRWkNH2l5moS0LCCrQTkYeQWBQSCAIgYRXEpKbW/XNH5zS4noR7faPEeu31l0h4dSpvc+t/Z199jkFWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhY/H9D/MR9qfKnLj/00U71aqfJn9+HCkCR/Wk36ddsgyJ/1wF4fkDfqqm9/gPsUeTnVr6a2xlQfnxdI7zs0W7irzD17Ytb2WT7EeNv/r4ox1O3Quf2QP2pgt9utwfout4FQE8AVBSlnaRmfvAURQkg2RlAbwB9AThlW5L0GaiKojhJhgOIBqDa7XaPrusdPtr5kQwF0BVAAoBIABRCKDd5aFUhRDAAw57eAOwAhKIoupft3zoqhB1AqLwuHIBut9uFt02qqvqRDJR2dAEQJj/BAOjn56dqmma+xiaECAEQAWAggLsB6A6HQ2iaZggBhBAqgEAAnQB0kzaEmT4hAITT6VQ8Ho/HJAKKECJQtr8LwD1y/A1/vcdfEUIEyfZ9AcQbYvZ942Px88L2UwlJR0dH0EMPPbRj5syZPUeNGrXR7Xb/641xIwJ1XY9NSUlZm52dfW+XLl1w8uRJzJ8//+OGhoYJqqqe1TSt1Wsm9NN1PSIqKmr12rVrR5WUlHy1bdu2AQCumWc3IYRD1/UwVVXnFRQUTIuNjUVzczN2797dWFJSkq8oymZd15sAGAEnFEUJ1nX9nzIzM1dnZmZGh4SE4OTJk5g5c+Zf29vbp9pstrMej6fVOyhIhgAYU1hY+B+hoaGoqKg4XVlZea+XTULTNFdCQsLGiRMnPuR2u3UhBOV9eeDAAWXTpk095DUe6WsoyRE5OTlr0tLSAux2O/bs2cO5c+e+pijKUpIXSHaQVAGkvPLKK++6XK4OksJLCFlXV2cvKSlJBFAjhU+x2WwhHo9nUHp6+urMzMy7wsLCUF9fjxdffPHjxsbGiTab7WuPx9NiEutOuq4PyMjI+M+srKyYqKgoHD58GDNmzNjq8XhyVFU9b/q+LH7hBAEYu3PnTlZVVRFAGgCX6f/tAHoOHDjwa0p27txp/JO9e/f+QM7cipw9nfL3kQBKt2zZQpJ87rnn6mQmoHilw2EACs+cOUOSrK+vZ1NTE0nyo48+IoBpxswoBcMJ4Ndjx471kOTFixe5d+9ekqTH42H//v13A4jyzpAURfEH0H/OnDnthu1z5sw558MmFUCPWbNmnaMP3nrrLZoyDmP8Hl68eDFJ8siRI9/Yc+zYMQKYKdtAztrTrl27xptRXV1NAKMAOAyBBBA/Y8aMdpLs6Ojgxx9//E37+++//29yvFXppwvAwMcee8xjtDHsuXLlCqOjo//ia3wsfpkoALqFhoZuIckJEyackimm3dQmEMDUmpoakmRISMhhAHOHDx/eQJIbN24kgKEyMAHAFRMTs2XXrl1saWkhSZ0kp0+ffhrAr3wEW/S8efOukORLL72kA1gKYMPWrVtJkk899dRJAHeYrgsEsIQkjx8/TgDvAPjd448/3kaSb7zxBmUa7vC6z53BwcFbSHL9+vU6Sc6aNes8gF5ewWAH0PfVV18lSQL4DMBGIcQ6AKtcLleBFC2jXtFt8ODBe0iyoqKCAJYByC8qKmJDQwOzsrK+MAmqo1OnTveHhoa+GRkZ+XZkZOSWiIiIvzgcjk9mzpypkWRmZuZpmbYbGV4AgPnNzc1sa2sjgN0A5iQmJtaSZHl5OQHcb/K3s81mW0uSTU1NBFAFYFbfvn1Pk+Tbb79NAA8IIVzW42/hByA+Pz/fLR/2ZXIda05NI/z9/TeR5J49ewhgqlxTrtI0jY2NjQQw3zTLuWJiYjaUlJToS5Ys6fjkk080kwDEeAmADcA9GzZsIElGRUW9CyAWwLApU6Y0kOSKFSsog9QICGdERMTGsrIyZmVlEcC9AB4IDw/fTpLbtm0jgN94CUAnAJmVlZVcs2aNZ/LkyRdJcvbs2b4EwAkgZfPmzTxw4AABFAN4BkC6vFeUSewcAO5duXIlSTIhIaEawGMAxgKYAmAGgCS73e5vrKVk/yGythANYEhCQsIhkly+fDkBpKqqGmL6DgIALDKN/3yZpVWQZGVlJQE8aPI3KiMjo5okV61aRQAjAPQBMPfIkSN0u90EUCBtsPiFEwpgbn19PdetW2fM5N4zQ9ekpKQqkty0aRMBpMjiWM6JEydIkoqirJUFJ6iq6pAPVy8A6cZMehMBUACEuVyuFwG8HBwcPEIWx367ZMkSjSQXLVrUJouTRorrkAHdA8BdQogsAOsKCwtJkmPGjDkvMw2bDDo/ADEjRoz4XylyFbm5uY0mAbjLyyZ/AOOrq6tZVlbWsWDBgo69e/eyoqKCgwcPPg4gSQaoIRbp27dvN7KF+tLSUr28vJwFBQXtMpvpYRIM7+wrAkDeqVOnePbsWQIoNKfzpiXPg8uXLydJJicnNwF4f+nSpW6STEtLq5fjYwhk1wkTJtSQ5Ouvv04AqTKj+N2xY8dIkgEBAW/Ie1v8wncRegwZMmQvSfbr12+3Ua33WqPfOWbMmP0kWVpaSgCDZAqcfejQIWNZsEGKgvnh9gfQb9myZd8nAEJVVZtMkUNk8CcNHTq0liR1XWdYWNhmH1mJIme80OnTp18x1rp5eXkEsNJms92Fb7e/IgEsvHz5Mp999tkmAI/l5uZeMC0B7vEqqAYAyL106RJJsra2lpWVld+sucePH38ZQG+5NncBeOrgwYMkqbe3t/Po0aOsra011wAWyl0H7x0JJ4DE+fPnu0kyPT29DsDdUrBuyNKEEAkAdpw/f/6GeoEM8GUmfwEgPCIiopwkGxsbabPZPgOw6L777vvm4p49e26VGYjFLxUhhD+ApLKyMp44ccIoVnXybgbgzkcfffRzklyzZg0BDJYCMMmoCwQFBXkLgLGWvvcWAgBToSsKwNPTp09vMR7UuLi4rwH0lgU8c/Db5ezbeeTIkRWzZ8++aMxu+fn5BPCADBwHgP4LFy701NXVEUAJgAnPP/98kyxMNgHo53A4zH77BQQETMvPz7+Um5vbBuAlAFMSExPPmdbVL0qh8Acw8fDhw5SCchVAEYAVb775JknyhRdeaJYztHfxMwLAaqNwCGC2FArv8x0hAHKNLGPKlCme5OTk/Zs3bzb7O0wKiiG8KXl5ed8IxenTp0mSR48e1UmyW7duWywBuD2xyQcgFECgoih+8H1gyJgZV5Lkyy+/3CbTRIePtl2HDBmyw1QBHyGDdXZdXR1JUghRKkXBjOMHCoBdpr0L3nvvPZLkF198wejo6O0A4lVVDTb74HQ6AwD8Wq7Jh8rgGgDgQ13XjVR8qaxJuADMbmlpYXl5uV5UVNRWUFDgfv/993Vj/ZydnU1c37eHXML4S3viAcQqitJD2l104cIFY8lTKsXSBWBMVVWVcd9yed2A1NTUQ6Zl00CvLMMOoHdubm6zFIlWOf5+PsY/Kj09vdrU11QAwwGsv3jxIk21m2DZr10I0RXAuAcffPBgaWkpV69eTYfDcdiwUxY0w6xw+flX8L1xApjevXv3lREREaW6rofB93aPDUDQpEmTMgHgtddeqwBwEd/utZvpqK6uPgEAcXFxkA94NwB9unfvjrNnz4LklwDcf08iIqv66Zs2bXrl4YcfxooVKxAbG7uqrq5uAYA2TdOEqqpGYIi2tjbl6aeffu/YsWPv5uTk7JaC1wHg4Pnz542MwoVvTx+21dbWYvjw4WLixIl+2dnZ9lGjRgmSTE1NRUpKCkwFTGiaxtTU1OXTpk3707Bhw/6g67pDipnT4biuj7qut+Lbk3Vf1tTUXI9qu91Pjq1QFEUBgJaWFgBo8yGOQ8eNGxcAAOvXr/8QwBUfYygAKL169eoCABcuXACAWtn2hOGv0+kMNO1KiPDw8F4A4rZv3/7R1KlTR0+bNu1ht9u9r1+/fqitrQXJgwDarRC6/QjPzs4+QJIffPCB9/aQmSAA43ft2mW0e1QGoi8CAPyLsZccExNTC2BlRkbGRdOyYJCP2csBIN6UAZzCd7cBbQCijYp/dXU1ExMTz6SmptaMHj36f9LS0vYlJCRsl6mxIWSdu3fv/g5J7t+/nwC2AShMTk6+SJKff/45AWRLYbD7+fndAeDf5BJnLoCCyZMnt5JkdnZ2C4B/F0KEm1Pu+Pj4rST55ZdfEsBWAK+mpaVdMo3raDn7KwDuSEpK+m+S3LBhAwG8DuCtHTt2UBbpjgC408vvcFVV15HkuXPnjMp+p5uMf0RcXNyHJNnQ0EBVVfcCWBQXF3fG+Jv0yxABPwB5LS0tRmFxN4BlTzzxxGWSXLx4sS5F3GGFy+1Hp5SUlJq6ujoWFxdTpsZ2H+0iIyMj/0iSWVlZX5mr5jfJFroPGzasxlhTnjp1iiTZ3NxMl8tlrCd9pfa9SkpKSJI5OTmnZOageLUZZqxvfVFWVkZcPwdgNwnSCKPqb17jkmR8fPzfZMDZ5CRsFBmNI7h95s2b1yhT7/MAYmStwCx4vy0uLqa3v5qmEcCfvSr1QQAeXb16NY3Cm3HQ55133iGAp+SxZTNhKSkpfzUddkrFjYevzAQCeGjp0qXfsYckY2NjTwD4leGDLCL2HTdunNtoY+zWSHFcIHdsFCtcfuZ1vO9Eqs3m7/F47sb1k2qX/f3997W2tl7BjWfpBYDOzzzzzIVJkyZh0KBBCwEsB3AJvl9AETabLcDj8dwRFRW1ctasWb8JCgpSzp07d62wsPC/Wltb8xRFadR1/ZqPXYbgAQMGbI2Pjw/+6quv9ldVVT0r01ezuPRJSUn5Y9euXXVd11WzDaqq6kePHm3+7LPPRgO4KlNuxWazhXo8nuTk5OSXMjIyEl0uFxoaGtqKior+dPXq1VdUVT0jj7r68ieoT58+vx8yZMjdx48fP1JVVTVF9m20VW02WyfZf97YsWPjXS4X6urqWvPy8jYCWCyEuEDS8FdVFKWzruv//OSTTy5OTk7uqWkaPv3007qysrJ8RVH+LI8ym8/rB3Tu3HnRI488knLo0KG2ffv2ZQI4C98vP6mqqoZqmpaclpa2cOTIkX39/f3R0NDQUVxc/G5TU9PLqqrWa5rWLH1QVFUN0TStX1JSUvH48eP7BwYG4uDBg1cKCgpeBbBe2u+2Qug2EwD5N5sMPuNtMe8XP4TT6Qxoa2sbIGeXvUKIK7d4IISiKC5d1wPljOfA9bPwzYqiXNV13dd6Uqiq6qdpml2mpe02m63d4/G4vcTF5fF47LJf71nJA6BZVVW3pmntuPHlmAD5wk6Q9NnbHp9vHaqq6tA0zU/64PZhk1FfCZB9G/23ALiqKEqzD39tpvbGUqoFwFUhRLP3yzpCCDtJpxyXDulfG27+pqRR3DXsUWVd4Yq0x/taVQjhIhksC8L+ABpM9ljBf5sKwI8pIBr75L5E4vvu+UNeG/a+hv+AL7yFH8qPtOfHjtOP6V/Bja8D6z/B2Nys/1u9Xv33tLf4GfF/LC4GCJwByWIAAAAASUVORK5CYII\x3d";
F.ID = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAlAAD/4QMpaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjM4MDBEMDY2QTU1MjExRTFBQTAzQjEzMUNFNzMxRkQwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjM4MDBEMDY1QTU1MjExRTFBQTAzQjEzMUNFNzMxRkQwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkU2RTk0OEM4OERCNDExRTE5NEUyRkE3M0M3QkE1NTlEIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkU2RTk0OEM5OERCNDExRTE5NEUyRkE3M0M3QkE1NTlEIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQADQkJCQoJDQoKDRMMCwwTFhENDREWGhUVFhUVGhkUFhUVFhQZGR0fIB8dGScnKionJzk4ODg5QEBAQEBAQEBAQAEODAwOEA4RDw8RFA4RDhQVERISERUfFRUXFRUfKB0ZGRkZHSgjJiAgICYjLCwoKCwsNzc1NzdAQEBAQEBAQEBA/8AAEQgAyACgAwEiAAIRAQMRAf/EALAAAAEFAQEAAAAAAAAAAAAAAAQAAgMFBgcBAQEAAwEBAAAAAAAAAAAAAAAAAQMEAgUQAAIBAgIEBwoLBgQGAwAAAAECAwAEEQUhMRIGQVFxsTITFGGBwdEiQlKSMzWRoeFicqKyI1NzFYJjJDQWB9KjVCbxwkNkJWXik3QRAAIBAgMFBQcDBQEAAAAAAAABAhEDIRIEMUFRcTJhwVIUBZGhsSJyEzOB0ULhYpIjUxX/2gAMAwEAAhEDEQA/AMJSpUqAVKlXuFAeUq9wpUB5XuFe4V6ooDzZHDox0CnGMinzwl7Z8NajaHeoO3vmTBZBtp9YUIqTEV5ROxHKnWRnaU8VRMhFBUjpV7hSoSeUq9pUB5Sr2lhQHlKvcK8oBV7hSFSRrtaKAZs07YNPM1pG2xJIAw1jSeandry/8X4m8VCKkWwaWwam7Xl/4v1W8VLtmX/i/VbxUoKkWwakSM407tmX/i/VbxUmzGwjQsjdY41IARie/U0IbZO0kNtCXnOCkEBeFu4KI3Bs7DNb27ya+jDx3kJeEnpJJEcQVbWDsk17u5urd591ucZkWhym2Vnd9RkCDEpFxDRpbw0bunu5mlp2De2FMLYXOD2wB2xbOeraUcYGJ72mlSUiqzzdzMd3Z3mixltA2yzcK/NlHM1DQyRXce1HocdNOEfJXZ88y9ZojOqhiBszIRiHQ8Y4cK5TvHuzLljHNMqxNoDjLFraHHnjPxcNCGVbxEUzYNTx5jZSxhpW6qTzlwJ+DCvO2Zf+L9VvFSgqyHYNLYNTdssPxfibxUu15f8Ai/VPiqCakOwa82DU/a8v/F+JvFTDdWPBL8R8VKCvYRYV5UzoMAy6QdIIqI0B4KJtxiRQwou16QoGUkntH5Tz0RbZbmF2hktraSVBo2lUkY8tDye0flPPXTslVUyiyVRsjqUOA4yMT8dW2ram2m6UVTNq9S7EIyUVJydMTn/6DnP+im9Wl+g5z/opvVrpteEhQWY4AaSTwAVf5WPiZh/9S5/zj7zltzlmYWkfWXNvJDGTgGcYDHirR7i7mSbwXParsFMrgb7w6jKw/wCmnc9I14kF3vpvCljbMyWMOJL4aEiB8qU/ObUK7HYWVrl1pFZWiCOCBQqKOLjPGTrNZZqKbUXVHq2nNwTuJRk1VpbgXN8s7Rk5ym0UQQzhIG2NAjhxHWbI+gCBVjBBFbwxwQqEiiUJGg1BVGAFe7dV28WYLYZFmF2Th1UD7JGjymGyn1iK5OyzIBGB1HgrLZhamzumQAGJwSqnSCh1q3GOCodxt4cxurdcpzuN4cyhiWaF5Bg09udUmnWw1H/jV9nFuJ7Quo+8h8peThFA+047vduyMtk7fYqTl07YFdfUufMPzT5p71UdtlmYXaGS2t3mQHAsgxANdadYJopLe4QS2867EsZ4QfCNYrCFbjdDPmgkYyWFxgVf04ifJf6ScNdRUW1XBb6FU5TjF5EpSSrGu/s5lN+g5z/opvVpfoOc/wCim9WtdHnatvObJXDW7xLGhB8nrPaY9/HCr+tEdPCVaSeDoYLnqF63lzW4/PFSW3ecxbI84VSzWUwUaSdg0DXXK5nvAipnd6qgKvWnQO7pri9ZUEmm3Vl2j1kr8pRlFRyquBNZjGxQ/S56Y1S2fu9OVueon11Szahoou06QoQUXadIVCD2FJJ7R+U89dMydv8Axdn+TH9muZye0flPPXQstlK5Tbka1gUjlC1q0vVLkeb6r+O3Tx9xcY1nt8c0NrZCyiOE1108NYjGv1joo7Js1jzKyScYLIvkzL6LDwHXVJksH9Sb49dKNq0tj1jA6uriOCL+02FWX7iVtZX1/AzaHTyeoauKn2MX9W79zebiZCuR5MjSrhfXuEtwTrUeZH+yNfdrRNcxI6IzhXlJEak6WIGJ2Rw4ChWnChndtlVBLMdQA0k1gbXNMzzDfDLs6mjaPKppJbWwJ1bOwwxw43OnHh71YT3DpfWUJmFlb5jHHDdeXBHIsrRea5TSqvxqG04cNN62vetoCS4tre5mgnkGE9q+3DKOkuI2WX6LDQRRHWDh1UCtwj7QRg2wdl8Djgw1qe7XvW0BQ3kfZ7mSLgU+T9E6RVbnuVrnWVSWqj+Lt8ZbRuHEdKPkYVcZ2MJY5fSGyeVar45+rkWQHAqccalPE5km1htWK5nK4Wnt5FuUBUwOMG4nGkA/BXUrW4S6torlOjMgcd/xVn7rLo7zKs0uEjCNeSvdwoBhgsZxX1l2j36k3Lu+uyprdj5Vs5A+i/lD48a0aaVJOPi7jB6lbzWozpjB48pf1NDXNN4vfl7+Z4BXS65pvF78vfzPAK71XTHmZ/S/yT+jvJ7L3fHytz1E+upbL+Qj5W56jfXWRnsIYKLtekKEFGWvSFQgyjk9o/Keet3YthlMP/5x9msJJ7R+U89biyb/AMXEv7gD6tadL1T+kwepRrC39ZkLDMbiwMvUHRPG0bjlGg8ore/23sxBldxfMPLupNhT8yL/AORNZbdzJ484scytxgLqJY5LZj6Q2sV5G1Vud1mjjyG0ij0NEGSZToKyhjtqw4waztuiXA3qKTbSxltfGhbZlE95ZtZqxVbgiOZhrER9ph3Svk9+pJILZ4Y4DGBFCUMKjRsGPobPFhUfW0NJmljE2xJcIrcI2vFUEln1lRXd6lrazXT9GCNpD+yNqoI7mOVduNw6nzlOIoPOUa6yye1XXcbMR5GdQ3xY0BSbj31/FcTQZirJ+q431q7anbHCTZ72Bw7lbPrKBMcBWNNgbMBBh+bsjBdni0VJ1lARZs6yWiupxCuMDy6KpS2IwOo6DTr3Mre3e5tZZVUM4ZBjqOOJoWO4jkXajcOOMHGgDISvWIrdAkKR80+TzVl908bPPL3LzxOuHdifxVfiTAg92qI/w+/8gGgSyN/mR7XPVlp0lF/3L3mbVKtu5Hjbk/8AHE2Fc03i9+Xv5ngFdKNc13i9+Xv5ngFaNV0x5nn+l/kn9HeEWXu+PlbnqJ9dS2Xu9OVueon11kZ7CGCjLXpCgxRlr0hUIPYUcntH5Tz1s8vb+Bt1/dqPirGSe0flPPWusG/g4Py15q06XqlyMWvVYQ+ruI9xJOqzO9hOto/sP8tbGOFIrmWeM7IuMDMnAXXQJOUjQeOsJk0nY96ip0CYunrjaHx1t+srPJUbXBm2LrFPikwTOb+T+VhbZxGMrDXp83x1QSy2tucJpUjPETp+Cn5/ftaRvKvtp3Kx48HG3erHMzOxZiWZtLMdJNQSbbL71Vk6yynViOkqnEEfOWtPbXi3EQkGg6mXiNckjeSJxJGxR10qw0GtxuxmvbImD4CZMFlA4fRfv0BqesqqzTMZNMEDbIHtHH2QeCiZJSqMQdOGiue53mz3czQwsRbIcNHnkec3c4qAMuriz68gTIToxwOOnlp0MjxMJYW741Gs3RVldtbygE/dMcHX/moDaxTiWNZB53B3arb8/wC+4SOF4sf/AKxU9kcBsfOGHfoUHtG/RbzY5Die5HHhXdvavqiZ9Q8Jdlq4/gbKua7xe/L38zwCuhpf2Uk/Zo50kmwJKIdogDjw1VzzeL35e/meAVp1LTgqY4nn+mRauzqmqwrjzCLL3fHytz1E+upLL+Qj5W56jfXWRnroYKLtekKEFF2vSFQg9hSSe0flPPWosm/hIfoLzVl5PaPynnrRWb/w0X0F5q06XqlyM2sVYx5gmbFre/t71NY2T+0h8VbSO5SWNJUOKSAMp7jDGspmMPaLRlXS6eWve1/FRO7WYdbZm1Y/eW/R7qHxHRXGojlm3ulid6aVbaW+OALvgCLq2Hm9WxHKWqjhj6xsK1e8dm15l4niG1LZkswGsxtrPeOmsvayBJA1VItlWjptLuTdPMo7LtjRDq9naK4+WF9IrUW7BaHOljGqVHB7w2hzVoZt87d8vaNYSLl02CcRsDEbJbj71Uu7UBkvJ7/D7q2QoDxySaAO8MTXdxRVMpRp5XZOWdF/ms7R5XdyKfKWJsO/5PhrG5XlNxmEywW6bTnTxAAcJNbGSMXkM1pjgbiNo1PziPJ+Os7u7m/6ReM00ZOgxSpqYYHT3wRXMKN4ll9zUG4bQfNshu8sZVuEA2hirA4qe/VOwwrVbzbww5mI44UKRRYkbWG0S3JWctbd7u5WFfOOLHiUdJqmaipfLsIsObhWe001lMkMVvJNjhghIALMcBxCs7fxXQmkupx1bXDswGPlaTidVaEyKNXkoo4eBV+Sq7L7Vs9zcBgeyQ4GQ/MB1crmoim2orezqcowTuSeEY48jQ7oZX2PLzdyLhNd6RjrEY6I7+uspvH78vfzPAK6UAAAFGAGgAcArmu8Xvy9/M8ArTfio24RW5nnaG67uou3H/KPuqT2X8hHytz1G+upLL3enK3PUb66ys9RDBRdr0hQgou06QqEGUkntH5Tz1e238vF9BeaqKT2j8p56vbb+Xi+gvNWjTdUuRn1XTHmTh8KrJTJlt8t1CPIY44cGnpJVjTJYkmjaN9Ib4u7V923njTethRauZJV3PaW1rfLIiXEDYg6R4VYc9CXW7thfOZbKdbGZtLW8uPVY/u3GrkNUkM9zlcxUjbhfWOA90cRq4gv4LhdqN+VToNYWmnRm9NNVWNTyHc6VWBv8wt4YeHqm6xyPmroq1Z7WGFLSxTq7WLSuPSdjrkfumq5yHXDUeA92oO2SKpVumNAaoJLMXH3myp0rpJ4uKhc3tbDM5BMri1zAj79j7KTiY8TcdBpcsith0286o+sPCagEX9Pzg4zXUCp6QYse8oouCG3tk6m1BYv05W6T+IdyolxbHDAAa2OgDlNCz3ryN2WxBd5PJMg1t81eId2ukqnLlTBbfcuY+9uJLiRcvtPvHdsHK+cfRHcHDWsyawjyy0WBcDI3lTP6TeIcFV+S5OmXx9bJg1048o8Cj0V8Jq2DVu09nL80up7OxHi+oal3P8AXB/IsZS8T/YOV65zvCcc7vfzPAK3ivWCz445zeH954BXOr6I8yfSfyz+jvCLP3fHytz1G+upLP3fHytz1E+usbPaQ0UXadIUIKLtekKhB7Ckk9o/Keer22/l4/oLzVRSe0flPPV7b/y8X0F5q0abqlyM+q6Y8yQsBTDMor1o8aiaE1pbluMqS3sbLLHIhSRQyngqukhaJ9uBjo+H5aOa3ao2t34qouRlLajTalGP8v0IY8ylXQ+PKPFU/bYXOLPge6CKia0LaxTOxHu1Q7cuBd9yPEJ7TbjXKO8CajbMIF6CNIeNvJHjqIWJ7tSpYkalqVblwIdyG+RGXur0hXYJFxal+Dhq5y3slkv3Y2pD0pTr+QUClpJRUdo9XW4OLrTHtM16cZLLWkeC7y4jvlNEpcRtw1Ux27Ci448NZrTFy3nn3IQWxlgGrDZ3pza7/M8ArZo+ArF5171uvp+CqdV0R5l/psUrs2vB3hdl7vTlbnqJ9dS2Xu+PlbnqJ9dY2eshooq16QoQUXa9IVCD2FLJ7RuU89WNtmUSQqkgYMgw0accKrpPaPynnrZWG4Vi+VWmY5tnMWXG+XrIYnA0rhj0mdcTgdNdwnKDqjmduM1SRR/qlr8/4KX6pa8T/BVzDuLZXudRZblmbxXcPUNPc3KqCIwrbOzgrHEnHjoyD+3eSXkht7DeKG4umDGOJVUklfouThXfmbnZ7Cvy1vt9pmv1W1+d8FL9VteJvgq5yrcOGfLmzHN80iyyETPbptAEFo2ZG8pmUa1OFNn3Ky6W/sbDKM5hv5bx2WTZA+7RF2y52WOPJTzE+z2Dy1vt9pT/AKpacTerS/U7Tib1a04/t7kDXPY03jhN0W6sQ7K7W3q2dnrMccaDy/8At80kuZfqWYxWNtlcvUPPhiGYhWDeUy7IwYU8xPs9g8tb7faUn6pacTerTxm9oOBvVq3v9z927aynuId44LiWKNnjhAXF2UYhRg516qpsryjLr21665zFLSTaK9U2GOA87SwqY37knRU+BzOzags0s1Oyr+BKM6sxwP6tSDPLMen6vy0rvdm3Sxlu7K/S7WDDrFUDUTxgnTU826eXW7KlxmqQuwDBXUKcD+1Xee/wXuKX5XDGWLapSVcOyhEM/seJ/V+WnjeGx4pPV+Wkm6kKZlFay3Jlt7iFpYZY8ASVK6DjtDDA0f8A0Tl340/1f8Ndx8xJVWXB0KbktFFpNzdVXAC/qOwA0CQni2flrO3Vwbm5lnI2TKxbDirX/wBE5d+NcfV/wVR7xZPa5U9utvI8nWhmbbw0YEAYYAVxfhfy5rlKR4Fulu6X7mW1mzT8S4Yis/5CPlbnqJ9dSWfu9OVueon11mZvQ2i7XpChKKtekKhBlNJ7R+U89bDfGTb3a3ZX0Lcj6kdY+T2j8p560288m1kWQr6MJ+ylSAr+2cnV5renjs3H1loX+3j9XvbbtxLN9lqW4UnV5jdnjtXHxihtyZNjeSBu5J9k1BJe7xy7W5CJ/wCzuD/mTVTf2+fq97LJuLrPsNRueS7W6aJ/38x+vLVXuY+xvHaNxbf2GoCezf8A36j/APsSf8w1sLnqczTefJluYoLm5uo5F61sBshItP1cNFYe1f8A3ir/APfE/wCZUe9bB94r5jwuPsrQFhmG4l/Z2M17HdW90tuu3IkTHaCjWdIw0VVZdks9/C06yJFEp2dp+E1bbqybGTZ8vpQD7L1XRv8A7blT96Oda7tpNuuNE37Cq9KSisjyuUoxrStKllHbLlWTXsMs8chuSuwEPDqwoLe5y+YRE/gLzmqRekvKKtd4327yM/ulHxmrHJStySWVRyrjxKI2XC/CTlnlPPKTpTdFbP0L1bgrf5Lp0G3dPhQHwV0S1lzBsns3sESR8Crh9WAJGjSOKuU3E+zdZQ3oJh8IArdZXFDmOTpHa3i2+YrI2KtKy4ricBsBuHHgFXSo440+Wa2qqxjvM9uMoy+WvzWpLCWWWE28HxL6e43ojgkeSCBY1Ri5BGIUDT51cl3vm276BBqSEH4WbxV0tlkyXJcxTMb+OW6uY9mGHrCzDQwwAbTp2uKuTZ9N1uYsfRRR8WPhrm419mSSjRyiqxVK7y23B/ftuTm2oSdJyzNVw3BFn7vTlbnqF9dS2fu9OVueon11lZuQ2iLdsGFD05H2dNQGV0ntG5Tz1dWm9N1b2kVq8EVwsI2UaQaQOKhmitZGLOmk68DhSFvY+gfWNSAg7z3Qvo7yKCKIohiaNR5LKxx8qpxvjcqS0VpbxvwOAcRQPZ7D0G9Y0uz2HoH1jUCpLY7zXlpbm3eKO5QuzjrBqZji3x17PvNcyT288VvDBJbMWUovS2hslW7mFQ9nsPQPrGl2ew9A+saCod/WNxtbYsrfb17WBxx5ddD2281xC88klvDcSXEnWuzrqOGGC9zRUPZ7D0G9Y0uzWHoH1jQVCLreq6ntZbaO3it1mGy7RjTs1X2mYy20ZiCq8ZOODcdEdmsPQb1jS7PYegfWNdJuLqnQiSUlRqpFLmryxtH1Ma7Qw2gNNPOdSt0oI27p007s9h6B9Y0uz2HoH1jXX3Z+I4+1b8IJdX89xLHKQFMXQUahpxoiPN5P+onfU+A0/s9h6DesaXZ7D0D6xpG7OLbUtu0StW5JJx2bBsmbtiSiEk+cxoCWWSaVpZOk2vDVo0VYdnsPQb1jSNvZcCH1jSd2c+p1XAmFqEOmOPEfaH+BQd1ueo211IzrgFUYKNAAqI1WztCpUqVCRUqVKgFSpUqAVKlSoBUqVKgFSpUqAVKlSoBUqVKgFSpUqAVKlSoD/9k\x3d";
var F = F || {}, ea = {
    id: 0 | 998 * Math.random(), G5: 0 | 998 * Math.random(), sP: function (a, c, d) {
        a = a.toString();
        var e = a.substring(a.indexOf("(") + 1, a.indexOf(")")), e = e.trim();
        for (a = a.substring(a.indexOf("{") + 1, a.lastIndexOf("}")); -1 != a.indexOf("this._super");) {
            var f = a.indexOf("this._super"), g = a.indexOf("(", f), h = a.substring(g + 1, a.indexOf(")", g)), h = h.trim();
            a = a.substring(0, f) + "ClassManager[" + d + "]." + c + ".call(this" + (h ? "," : "") + a.substring(g + 1)
        }
        return Function(e, a)
    }, z4: function () {
        return this.id++
    }, A4: function () {
        return this.G5++
    }
};
ea.sP.Q$ = ea;
(function () {
    var a = /\b_super\b/, c = F.Qb.Re[F.Qb.Dg.K2];
    c && console.log("release Mode");
    F.za = u();
    F.za.extend = function (d) {
        function e() {
            this.ma = ea.A4();
            this.ctor && this.ctor.apply(this, arguments)
        }

        var f = this.prototype, g = Object.create(f), h = ea.z4();
        ea[h] = f;
        var m = {writable: p, enumerable: r, configurable: p};
        g.ma = q;
        e.id = h;
        m.value = h;
        Object.defineProperty(g, "__pid", m);
        e.prototype = g;
        m.value = e;
        Object.defineProperty(e.prototype, "constructor", m);
        this.zi && (e.zi = F.m(this.zi));
        this.Ai && (e.Ai = F.m(this.Ai));
        for (var n = 0, s = arguments.length; n <
        s; ++n) {
            var t = arguments[n], v;
            for (v in t) {
                var w = "function" === typeof t[v], x = "function" === typeof f[v], y = a.test(t[v]);
                c && w && x && y ? (m.value = ea.sP(t[v], v, h), Object.defineProperty(g, v, m)) : w && x && y ? (m.value = function (a, c) {
                    return function () {
                        var d = this._super;
                        this._super = f[a];
                        var e = c.apply(this, arguments);
                        this._super = d;
                        return e
                    }
                }(v, t[v]), Object.defineProperty(g, v, m)) : w ? (m.value = t[v], Object.defineProperty(g, v, m)) : g[v] = t[v];
                if (w) {
                    var B, I;
                    if (this.zi && this.zi[v]) {
                        var w = this.zi[v], E;
                        for (E in this.Ai)if (this.Ai[E] ==
                            w) {
                            I = E;
                            break
                        }
                        F.k(g, w, t[v], t[I] ? t[I] : g[I], v, I)
                    }
                    if (this.Ai && this.Ai[v]) {
                        w = this.Ai[v];
                        for (E in this.zi)if (this.zi[E] == w) {
                            B = E;
                            break
                        }
                        F.k(g, w, t[B] ? t[B] : g[B], t[v], B, v)
                    }
                }
            }
        }
        e.extend = F.za.extend;
        e.oia = function (a) {
            for (var c in a)g[c] = a[c]
        };
        return e
    }
})();
F.k = function (a, c, d, e, f, g) {
    if (a.__defineGetter__)d && a.__defineGetter__(c, d), e && a.__defineSetter__(c, e); else if (Object.defineProperty) {
        var h = {enumerable: r, configurable: p};
        d && (h.get = d);
        e && (h.set = e);
        Object.defineProperty(a, c, h)
    } else b(Error("browser does not support getters"));
    if (!f && !g)for (var h = d != q, m = e != k, n = Object.getOwnPropertyNames(a), s = 0; s < n.length; s++) {
        var t = n[s];
        if (!((a.__lookupGetter__ ? a.__lookupGetter__(t) : Object.getOwnPropertyDescriptor(a, t)) || "function" !== typeof a[t])) {
            var v = a[t];
            if (h && v ===
                d && (f = t, !m || g))break;
            if (m && v === e && (g = t, !h || f))break
        }
    }
    a = a.constructor;
    f && (a.zi || (a.zi = {}), a.zi[f] = c);
    g && (a.Ai || (a.Ai = {}), a.Ai[g] = c)
};
F.m = function (a) {
    var c = a.constructor ? new a.constructor : {}, d;
    for (d in a) {
        var e = a[d];
        c[d] = "object" == typeof e && e && !(e instanceof F.n) && !(e instanceof HTMLElement) ? F.m(e) : e
    }
    return c
};
F = F || {};
F.N = F.N || {};
F.Cda = u();
F.Kr = {
    iP: 8,
    x9: 9,
    RP: 13,
    shift: 16,
    fea: 17,
    alt: 18,
    pause: 19,
    Lda: 20,
    escape: 27,
    Mla: 33,
    Lla: 34,
    end: 35,
    home: 36,
    left: 37,
    Gpa: 38,
    right: 39,
    xea: 40,
    sia: 45,
    faa: 46,
    "0": 48,
    1: 49,
    2: 50,
    3: 51,
    4: 52,
    5: 53,
    6: 54,
    7: 55,
    8: 56,
    9: 57,
    a: 65,
    b: 66,
    s: 67,
    z: 68,
    ih: 69,
    ci: 70,
    g: 71,
    Dq: 72,
    Zt: 73,
    rz: 74,
    sz: 75,
    Cz: 76,
    Gz: 77,
    Rl: 78,
    Jz: 79,
    d: 80,
    Uz: 81,
    r: 82,
    HH: 83,
    iI: 84,
    pa: 85,
    qa: 86,
    M: 87,
    x: 88,
    y: 89,
    e: 90,
    $ka: 96,
    ala: 97,
    bla: 98,
    cla: 99,
    dla: 100,
    ela: 101,
    fla: 102,
    gla: 103,
    hla: 104,
    ila: 105,
    "*": 106,
    "+": 107,
    "-": 109,
    numdel: 110,
    "/": 111,
    gfa: 112,
    kfa: 113,
    lfa: 114,
    mfa: 115,
    nfa: 116,
    ofa: 117,
    pfa: 118,
    qfa: 119,
    rfa: 120,
    hfa: 121,
    ifa: 122,
    jfa: 123,
    lla: 144,
    Sma: 145,
    Vma: 186,
    ",": 186,
    efa: 187,
    "\x3d": 187,
    ";": 188,
    Vda: 188,
    iea: 189,
    ".": 190,
    Sla: 190,
    wfa: 191,
    lia: 192,
    "[": 219,
    vla: 219,
    "]": 221,
    Rda: 221,
    Dda: 220,
    quote: 222,
    ipa: 32
};
F.laa = 0;
F.$A = 1;
F.SI = 2;
F.maa = 3;
F.naa = 4;
F.AU = 5;
F.n4 = function (a) {
    return 8 < a.length && 137 == a[0] && 80 == a[1] && 78 == a[2] && 71 == a[3] && 13 == a[4] && 10 == a[5] && 26 == a[6] && 10 == a[7] ? F.$A : 2 < a.length && (73 == a[0] && 73 == a[1] || 77 == a[0] && 77 == a[1] || 255 == a[0] && 216 == a[1]) ? F.SI : F.AU
};
F.pia = function (a, c) {
    function d() {
    }

    d.prototype = c.prototype;
    a.uA = c.prototype;
    a.prototype = new d;
    a.prototype.constructor = a
};
F.Eda = function (a, c, d) {
    var e = arguments.callee.caller;
    if (e.uA)return ret = e.uA.constructor.apply(a, Array.prototype.slice.call(arguments, 1));
    for (var f = Array.prototype.slice.call(arguments, 2), g = r, h = a.constructor; h; h = h.uA && h.uA.constructor)if (h.prototype[c] === e)g = p; else if (g)return h.prototype[c].apply(a, f);
    if (a[c] === e)return a.constructor.prototype[c].apply(a, f);
    b(Error("cc.base called from a method of one name to a method of a different name"))
};
F.gba = function (a, c) {
    this.x = a || 0;
    this.y = c || 0
};
F.d = function (a, c) {
    return a == k ? {x: 0, y: 0} : c == k ? {x: a.x, y: a.y} : {x: a, y: c}
};
F.Tz = function (a, c) {
    return a && c && a.x === c.x && a.y === c.y
};
F.mba = function (a, c) {
    this.width = a || 0;
    this.height = c || 0
};
F.size = function (a, c) {
    return a === k ? {width: 0, height: 0} : c === k ? {width: a.width, height: a.height} : {width: a, height: c}
};
F.gpa = function (a, c) {
    return a && c && a.width == c.width && a.height == c.height
};
F.NJ = function (a, c, d, e) {
    this.x = a || 0;
    this.y = c || 0;
    this.width = d || 0;
    this.height = e || 0
};
F.rect = function (a, c, d, e) {
    return a === k ? {x: 0, y: 0, width: 0, height: 0} : c === k ? {
        x: a.x,
        y: a.y,
        width: a.width,
        height: a.height
    } : {x: a, y: c, width: d, height: e}
};
F.LR = function (a, c) {
    return a && c && a.x === c.x && a.y === c.y && a.width === c.width && a.height === c.height
};
F.sE = function (a) {
    return a && 0 === a.x && 0 === a.y && 0 === a.width && 0 === a.height
};
F.hma = function (a, c) {
    return !a || !c ? r : !(a.x >= c.x || a.y >= c.y || a.x + a.width <= c.x + c.width || a.y + a.height <= c.y + c.height)
};
F.Fu = function (a) {
    return a.x + a.width
};
F.ima = function (a) {
    return a.x + a.width / 2
};
F.Rq = function (a) {
    return a.x
};
F.Gu = function (a) {
    return a.y + a.height
};
F.jma = function (a) {
    return a.y + a.height / 2
};
F.Sq = function (a) {
    return a.y
};
F.KR = function (a, c) {
    return c.x >= F.Rq(a) && c.x <= F.Fu(a) && c.y >= F.Sq(a) && c.y <= F.Gu(a)
};
F.lma = function (a, c) {
    var d = a.y + a.height, e = c.x + c.width, f = c.y + c.height;
    return !(a.x + a.width < c.x || e < a.x || d < c.y || f < a.y)
};
F.mma = function (a, c) {
    return !(a.x + a.width < c.x || c.x + c.width < a.x || a.y + a.height < c.y || c.y + c.height < a.y)
};
F.Vz = function (a, c) {
    var d = F.rect(0, 0, 0, 0);
    d.x = Math.min(a.x, c.x);
    d.y = Math.min(a.y, c.y);
    d.width = Math.max(a.x + a.width, c.x + c.width) - d.x;
    d.height = Math.max(a.y + a.height, c.y + c.height) - d.y;
    return d
};
F.kma = function (a, c) {
    var d = F.rect(Math.max(F.Rq(a), F.Rq(c)), Math.max(F.Sq(a), F.Sq(c)), 0, 0);
    d.width = Math.min(F.Fu(a), F.Fu(c)) - F.Rq(d);
    d.height = Math.min(F.Gu(a), F.Gu(c)) - F.Sq(d);
    return d
};
F.Wv = F.za.extend({
    PN: q, DD: q, ctor: function () {
        window.DOMParser ? (this.DD = p, this.PN = new DOMParser) : this.DD = r
    }, parse: function (a) {
        return this.$D(a)
    }, $D: function (a) {
        var c;
        this.DD ? c = this.PN.parseFromString(a, "text/xml") : (c = new ActiveXObject("Microsoft.XMLDOM"), c.async = "false", c.loadXML(a));
        return c
    }
});
F.VV = F.Wv.extend({
    parse: function (a) {
        a = this.$D(a).documentElement;
        "plist" != a.tagName && b("Not a plist file!");
        for (var c = q, d = 0, e = a.childNodes.length; d < e && !(c = a.childNodes[d], 1 == c.nodeType); d++);
        return this.ZD(c)
    }, ZD: function (a) {
        var c = q, d = a.tagName;
        if ("dict" == d)c = this.d0(a); else if ("array" == d)c = this.c0(a); else if ("string" == d)if (1 == a.childNodes.length)c = a.firstChild.nodeValue; else {
            c = "";
            for (d = 0; d < a.childNodes.length; d++)c += a.childNodes[d].nodeValue
        } else"false" == d ? c = r : "true" == d ? c = p : "real" == d ? c = parseFloat(a.firstChild.nodeValue) :
        "integer" == d && (c = parseInt(a.firstChild.nodeValue, 10));
        return c
    }, c0: function (a) {
        for (var c = [], d = 0, e = a.childNodes.length; d < e; d++) {
            var f = a.childNodes[d];
            1 == f.nodeType && c.push(this.ZD(f))
        }
        return c
    }, d0: function (a) {
        for (var c = {}, d = q, e = 0, f = a.childNodes.length; e < f; e++) {
            var g = a.childNodes[e];
            1 == g.nodeType && ("key" == g.tagName ? d = g.firstChild.nodeValue : c[d] = this.ZD(g))
        }
        return c
    }
});
F.SO = {
    load: function (a, c, d, e) {
        F.aa.Fz(a, e)
    }
};
F.aa.nj(["txt", "xml", "vsh", "fsh", "atlas"], F.SO);
F.B_ = {
    load: function (a, c, d, e) {
        F.aa.pR(a, e)
    }
};
F.aa.nj(["json", "ExportJson"], F.B_);
F.i_ = {
    load: function (a, c, d, e) {
        F.aa.jg[c] = F.aa.Kq(a, function (a, d) {
            if (a)return e(a);
            F.Ra.Lb(c);
            e(q, d)
        })
    }
};
F.aa.nj("png jpg bmp jpeg gif ico".split(" "), F.i_);
F.B0 = {
    load: function (a, c, d, e) {
        F.aa.jg[c] = F.aa.Kq(d.src, function (a, d) {
            if (a)return e(a);
            F.Ra.Lb(c);
            e(q, d)
        })
    }
};
F.aa.nj(["serverImg"], F.B0);
F.i0 = {
    load: function (a, c, d, e) {
        F.aa.Fz(a, function (a, c) {
            if (a)return e(a);
            e(q, F.e7.parse(c))
        })
    }
};
F.aa.nj(["plist"], F.i0);
F.uZ = {
    nX: {".eot": "embedded-opentype", ".ttf": "truetype", ".woff": "woff", ".svg": "svg"}, DN: function (a, c, d) {
        var e = document, f = F.path, g = this.nX, h = F.bc("style");
        h.type = "text/css";
        e.body.appendChild(h);
        var m = "@font-face { font-family:" + a + "; src:";
        if (c instanceof Array)for (var n = 0, s = c.length; n < s; n++)d = f.Hl(c[n]).toLowerCase(), m += "url('" + c[n] + "') format('" + g[d] + "')", m += n == s - 1 ? ";" : ","; else m += "url('" + c + "') format('" + g[d] + "');";
        h.textContent += m + "};";
        c = F.bc("div");
        d = c.style;
        d.fontFamily = a;
        c.innerHTML = ".";
        d.position =
            "absolute";
        d.left = "-100px";
        d.top = "-100px";
        e.body.appendChild(c)
    }, load: function (a, c, d, e) {
        c = d.type;
        a = d.name;
        c = d.jpa;
        F.Dd(d) ? (c = F.path.Hl(d), a = F.path.x2(d, c), this.DN(a, d, c)) : this.DN(a, c);
        e(q, p)
    }
};
F.aa.nj(["font", "eot", "ttf", "woff", "svg"], F.uZ);
F.eca = {
    load: function (a, c, d, e) {
        F.aa.z6(a, e)
    }
};
F.dZ = {
    load: function (a, c, d, e) {
        F.aa.A6(a, e)
    }
};
F.aa.nj(["csb"], F.dZ);
window.CocosEngine = F.VA = "Cocos2d-JS v3.1";
F.ro = 0;
F.QA = F.d(0, 0);
F.bU = 0.5;
F.P$ = 1;
F.$v = 1;
F.GV = 0;
F.gK = 0;
F.TB = 0;
F.rba = 0;
F.iba = 1;
F.hba = "-hd";
F.dY = 1;
F.Bo = 0;
F.kba = 0;
F.aV = 0;
F.$U = 0;
F.gB = 1;
F.T$ = F.VA + "-canvas";
F.lv = 1;
F.zh = 1;
F.Gb = function (a) {
    var c = this == F ? document : this;
    if (a = a instanceof HTMLElement ? a : c.querySelector(a))a.find = a.find || F.Gb, a.PG = a.PG || function (a) {
            return this.className.match(RegExp("(\\s|^)" + a + "(\\s|$)"))
        }, a.dP = a.dP || function (a) {
            this.PG(a) || (this.className && (this.className += " "), this.className += a);
            return this
        }, a.x7 = a.x7 || function (a) {
            this.PG(a) && (this.className = this.className.replace(a, ""));
            return this
        }, a.remove = a.remove || function () {
            this.parentNode && this.parentNode.removeChild(this);
            return this
        }, a.fP = a.fP ||
        function (a) {
            a.appendChild(this);
            return this
        }, a.j7 = a.j7 || function (a) {
            a.childNodes[0] ? a.insertBefore(this, a.childNodes[0]) : a.appendChild(this);
            return this
        }, a.Xu = a.Xu || function () {
            this.style[F.Gb.M9] = F.Gb.translate(this.position) + F.Gb.rotate(this.rotation) + F.Gb.scale(this.scale) + F.Gb.br(this.br);
            return this
        }, a.position = a.position || {x: 0, y: 0}, a.rotation = a.rotation || 0, a.scale = a.scale || {
            x: 1,
            y: 1
        }, a.br = a.br || {x: 0, y: 0}, a.WS = function (a, c) {
        this.position.x = a;
        this.position.y = c;
        this.Xu()
    }, a.rotate = function (a) {
        this.rotation =
            a;
        this.Xu();
        return this
    }, a.Ju = function (a, c) {
        this.scale.x = a;
        this.scale.y = c;
        this.Xu();
        return this
    }, a.voa = function (a, c) {
        this.br.x = a;
        this.br.y = c;
        this.Xu();
        return this
    };
    return a
};
switch (F.ta.$h) {
    case F.ta.pr:
        F.Gb.zu = "Moz";
        F.Gb.Eq = p;
        break;
    case F.ta.EA:
    case F.ta.HA:
        F.Gb.zu = "webkit";
        F.Gb.Eq = p;
        break;
    case F.ta.GA:
        F.Gb.zu = "O";
        F.Gb.Eq = r;
        break;
    case F.ta.FA:
        F.Gb.zu = "ms";
        F.Gb.Eq = r;
        break;
    default:
        F.Gb.zu = "webkit", F.Gb.Eq = p
}
F.Gb.M9 = F.Gb.zu + "Transform";
F.Gb.translate = F.Gb.Eq ? function (a) {
    return "translate3d(" + a.x + "px, " + a.y + "px, 0) "
} : function (a) {
    return "translate(" + a.x + "px, " + a.y + "px) "
};
F.Gb.rotate = F.Gb.Eq ? function (a) {
    return "rotateZ(" + a + "deg) "
} : function (a) {
    return "rotate(" + a + "deg) "
};
F.Gb.scale = function (a) {
    return "scale(" + a.x + ", " + a.y + ") "
};
F.Gb.br = function (a) {
    return "skewX(" + -a.x + "deg) skewY(" + a.y + "deg)"
};
F.mT = function () {
    return F.Gb(document.createElement("input"))
};
F.Gb.ufa = function (a) {
    var c = 0, d = 0;
    do c += a.offsetLeft, d += a.offsetTop; while (a = a.offsetParent);
    return {x: c, y: d}
};
F.waa = -1;
F.PI = Math.PI;
F.zU = parseFloat("3.402823466e+38F");
F.kaa = parseFloat("1.175494351e-38F");
F.WV = F.PI / 180;
F.KI = 180 / F.PI;
F.ZK = 4294967295;
F.OS = function (a, c, d) {
    if (F.Gq(d) && !F.nk(d.x) && !F.nk(d.y)) {
        var e = d[a];
        d[a] = d[c];
        d[c] = e
    } else F.log(F.i.OS)
};
F.Lka = function (a, c, d) {
    return a + (c - a) * d
};
F.Rb = function () {
    return 16777215 * Math.random()
};
F.p7 = function () {
    return 2 * (Math.random() - 0.5)
};
F.ama = Math.random;
F.kf = function (a) {
    return a * F.WV
};
F.AH = function (a) {
    return a * F.KI
};
F.HR = function (a) {
    F.log(F.i.HR);
    return a * F.KI
};
F.Qr = Number.MAX_VALUE - 1;
F.Ac = F.GV ? 1 : 770;
F.zc = 771;
F.nu = function (a) {
    a.na && (a.na.xb(), a.na.qf())
};
F.dfa = u();
F.oea = u();
F.lh = function () {
    F.bd += 1
};
F.Ev = 1.192092896E-7;
F.Fb = F.gB ? function () {
    return F.L.Dh
} : D(1);
F.f7 = function (a) {
    var c = F.Fb();
    return F.d(a.x * c, a.y * c)
};
F.xH = function (a) {
    var c = F.Fb();
    return F.d(a.x / c, a.y / c)
};
F.dE = function (a, c) {
    var d = F.Fb();
    c.x = a.x / d;
    c.y = a.y / d
};
F.g9 = function (a) {
    var c = F.Fb();
    return F.size(a.width * c, a.height * c)
};
F.FS = function (a) {
    var c = F.Fb();
    return F.size(a.width / c, a.height / c)
};
F.EO = function (a, c) {
    var d = F.Fb();
    c.width = a.width / d;
    c.height = a.height / d
};
F.Xl = F.gB ? function (a) {
    var c = F.Fb();
    return F.rect(a.x / c, a.y / c, a.width / c, a.height / c)
} : aa();
F.Hu = F.gB ? function (a) {
    var c = F.Fb();
    return F.rect(a.x * c, a.y * c, a.width * c, a.height * c)
} : aa();
F.ONE = 1;
F.ZERO = 0;
F.SRC_ALPHA = 770;
F.SRC_ALPHA_SATURATE = 776;
F.SRC_COLOR = 768;
F.DST_ALPHA = 772;
F.DST_COLOR = 774;
F.ONE_MINUS_SRC_ALPHA = 771;
F.ONE_MINUS_SRC_COLOR = 769;
F.ONE_MINUS_DST_ALPHA = 773;
F.ONE_MINUS_DST_COLOR = 775;
F.ONE_MINUS_CONSTANT_ALPHA = 32772;
F.ONE_MINUS_CONSTANT_COLOR = 32770;
F.jq = function () {
    if (F.Iu == F.Y) {
        var a = F.l.getError();
        a && F.log(F.i.jq, a)
    }
};
F.$$ = 0;
F.Y$ = 1;
F.aaa = 2;
F.Z$ = 3;
F.X$ = 2;
F.Rba = 0;
F.Jd = 1;
F.Do = 2;
F.Eo = 4;
F.Bh = F.Jd | F.Do | F.Eo;
F.paa = 0;
F.kb = 0;
F.sd = 1;
F.hd = 2;
F.Sba = 3;
F.cs = 0;
F.bs = 1;
F.Co = 2;
F.sw = 3;
F.rw = 4;
F.qw = 5;
F.ds = 6;
F.lC = 7;
F.Oba = 8;
F.yj = "ShaderPositionTextureColor";
F.Yv = "ShaderPositionTextureColorAlphaTest";
F.Xv = "ShaderPositionColor";
F.Tr = "ShaderPositionTexture";
F.QB = "ShaderPositionTexture_uColor";
F.PB = "ShaderPositionTextureA8Color";
F.RB = "ShaderPosition_uColor";
F.TJ = "ShaderPositionLengthTextureColor";
F.YX = "CC_PMatrix";
F.WX = "CC_MVMatrix";
F.XX = "CC_MVPMatrix";
F.bY = "CC_Time";
F.aY = "CC_SinTime";
F.VX = "CC_CosTime";
F.ZX = "CC_Random01";
F.$X = "CC_Texture0";
F.$K = "CC_alpha_value";
F.or = "a_color";
F.hm = "a_position";
F.lo = "a_texCoord";
F.OU = 32;
F.DI = 3233828865;
F.tw = 3233828866;
F.nV = 8801;
F.aW = 8802;
F.cU = 8803;
F.Gt = function (a, c) {
    if (a && 0 < a.length)for (var d = 0; d < a.length; d++)if (!(a[d] instanceof c))return F.log("element type is wrong!"), r;
    return p
};
F.we = function (a, c) {
    for (var d = 0, e = a.length; d < e; d++)if (a[d] == c) {
        a.splice(d, 1);
        break
    }
};
F.Bda = function (a, c) {
    for (var d = 0, e = c.length; d < e; d++)F.we(a, c[d])
};
F.Ada = function (a, c, d) {
    a.splice.apply(a, [d, 0].concat(c));
    return a
};
F.vP = function (a) {
    var c, d = a.length, e = Array(d);
    for (c = 0; c < d; c += 1)e[c] = a[c];
    return e
};
F = F || {};
F.N = F.N || {};
F.N.pC = function () {
    F.color = function (a, d, e, f, g, h) {
        return a === k ? new F.Pf(0, 0, 0, 255, g, h) : F.Dd(a) ? (a = F.IQ(a), new F.Pf(a.r, a.g, a.b, a.a)) : F.Gq(a) ? new F.Pf(a.r, a.g, a.b, a.a, a.cq, a.offset) : new F.Pf(a, d, e, f, g, h)
    };
    F.Pf = function (a, d, e, f, g, h) {
        this.td = g || new ArrayBuffer(F.Pf.BYTES_PER_ELEMENT);
        this.Ia = h || 0;
        g = this.td;
        h = this.Ia;
        var m = Uint8Array.BYTES_PER_ELEMENT;
        this.qE = new Uint8Array(g, h, 1);
        this.iD = new Uint8Array(g, h + m, 1);
        this.GC = new Uint8Array(g, h + 2 * m, 1);
        this.yC = new Uint8Array(g, h + 3 * m, 1);
        this.qE[0] = a || 0;
        this.iD[0] =
            d || 0;
        this.GC[0] = e || 0;
        this.yC[0] = f == q ? 255 : f;
        f === k && (this.Q1 = p)
    };
    F.Pf.BYTES_PER_ELEMENT = 4;
    var a = F.Pf.prototype;
    a.XZ = function () {
        return this.qE[0]
    };
    a.R0 = function (a) {
        this.qE[0] = 0 > a ? 0 : a
    };
    a.LZ = function () {
        return this.iD[0]
    };
    a.M0 = function (a) {
        this.iD[0] = 0 > a ? 0 : a
    };
    a.DZ = function () {
        return this.GC[0]
    };
    a.D0 = function (a) {
        this.GC[0] = 0 > a ? 0 : a
    };
    a.zZ = function () {
        return this.yC[0]
    };
    a.C0 = function (a) {
        this.yC[0] = 0 > a ? 0 : a
    };
    F.k(a, "r", a.XZ, a.R0);
    F.k(a, "g", a.LZ, a.M0);
    F.k(a, "b", a.DZ, a.D0);
    F.k(a, "a", a.zZ, a.C0);
    N = function (a, d, e, f) {
        this.td =
            e || new ArrayBuffer(N.BYTES_PER_ELEMENT);
        this.Ia = f || 0;
        this.tn = new Float32Array(this.td, this.Ia, 1);
        this.un = new Float32Array(this.td, this.Ia + 4, 1);
        this.tn[0] = a || 0;
        this.un[0] = d || 0
    };
    N.BYTES_PER_ELEMENT = 8;
    Object.defineProperties(N.prototype, {
        x: {
            get: function () {
                return this.tn[0]
            }, set: function (a) {
                this.tn[0] = a
            }, enumerable: p
        }, y: {
            get: function () {
                return this.un[0]
            }, set: function (a) {
                this.un[0] = a
            }, enumerable: p
        }
    });
    U = function (a, d, e, f, g) {
        this.td = f || new ArrayBuffer(U.BYTES_PER_ELEMENT);
        this.Ia = g || 0;
        f = this.td;
        g = this.Ia;
        this.tn = new Float32Array(f, g, 1);
        this.tn[0] = a || 0;
        this.un = new Float32Array(f, g + Float32Array.BYTES_PER_ELEMENT, 1);
        this.un[0] = d || 0;
        this.GF = new Float32Array(f, g + 2 * Float32Array.BYTES_PER_ELEMENT, 1);
        this.GF[0] = e || 0
    };
    U.BYTES_PER_ELEMENT = 12;
    Object.defineProperties(U.prototype, {
        x: {
            get: function () {
                return this.tn[0]
            }, set: function (a) {
                this.tn[0] = a
            }, enumerable: p
        }, y: {
            get: function () {
                return this.un[0]
            }, set: function (a) {
                this.un[0] = a
            }, enumerable: p
        }, e: {
            get: function () {
                return this.GF[0]
            }, set: function (a) {
                this.GF[0] = a
            }, enumerable: p
        }
    });
    fa = function (a, d, e, f) {
        this.td = e || new ArrayBuffer(fa.BYTES_PER_ELEMENT);
        this.Ia = f || 0;
        this.oF = new Float32Array(this.td, this.Ia, 1);
        this.uF = new Float32Array(this.td, this.Ia + 4, 1);
        this.oF[0] = a || 0;
        this.uF[0] = d || 0
    };
    fa.BYTES_PER_ELEMENT = 8;
    Object.defineProperties(fa.prototype, {
        pa: {
            get: function () {
                return this.oF[0]
            }, set: function (a) {
                this.oF[0] = a
            }, enumerable: p
        }, qa: {
            get: function () {
                return this.uF[0]
            }, set: function (a) {
                this.uF[0] = a
            }, enumerable: p
        }
    });
    F.JB = function (a, d, e, f, g, h) {
        this.td = g || new ArrayBuffer(F.JB.BYTES_PER_ELEMENT);
        this.Ia = h || 0;
        g = this.td;
        h = N.BYTES_PER_ELEMENT;
        this.Np = a ? new N(a.x, a.y, g, 0) : new N(0, 0, g, 0);
        this.Qp = d ? new N(d.x, d.y, g, h) : new N(0, 0, g, h);
        this.Lo = e ? new N(e.x, e.y, g, 2 * h) : new N(0, 0, g, 2 * h);
        this.Po = f ? new N(f.x, f.y, g, 3 * h) : new N(0, 0, g, 3 * h)
    };
    F.JB.BYTES_PER_ELEMENT = 32;
    F.Rv = function (a, d, e, f) {
        this.K = a || new U(0, 0, 0);
        this.V = d || new U(0, 0, 0);
        this.U = e || new U(0, 0, 0);
        this.O = f || new U(0, 0, 0)
    };
    Object.defineProperties(F.JB.prototype, {
        U: {
            get: A("Np"), set: function (a) {
                this.Np.x = a.x;
                this.Np.y = a.y
            }, enumerable: p
        }, O: {
            get: A("Qp"),
            set: function (a) {
                this.Qp.x = a.x;
                this.Qp.y = a.y
            }, enumerable: p
        }, K: {
            get: A("Lo"), set: function (a) {
                this.Lo.x = a.x;
                this.Lo.y = a.y
            }, enumerable: p
        }, V: {
            get: A("Po"), set: function (a) {
                this.Po.x = a.x;
                this.Po.y = a.y
            }, enumerable: p
        }
    });
    F.Kg = function (a, d, e, f, g) {
        this.td = f || new ArrayBuffer(F.Kg.BYTES_PER_ELEMENT);
        this.Ia = g || 0;
        f = this.td;
        g = this.Ia;
        var h = U.BYTES_PER_ELEMENT;
        this.bb = a ? new U(a.x, a.y, a.e, f, g) : new U(0, 0, 0, f, g);
        this.ts = d ? F.color(d.r, d.g, d.b, d.a, f, g + h) : F.color(0, 0, 0, 0, f, g + h);
        this.Ff = e ? new fa(e.pa, e.qa, f, g + h + F.Pf.BYTES_PER_ELEMENT) :
            new fa(0, 0, f, g + h + F.Pf.BYTES_PER_ELEMENT)
    };
    F.Kg.BYTES_PER_ELEMENT = 24;
    Object.defineProperties(F.Kg.prototype, {
        j: {
            get: A("bb"), set: function (a) {
                var d = this.bb;
                d.x = a.x;
                d.y = a.y;
                d.e = a.e
            }, enumerable: p
        }, A: {
            get: A("ts"), set: function (a) {
                var d = this.ts;
                d.r = a.r;
                d.g = a.g;
                d.b = a.b;
                d.a = a.a
            }, enumerable: p
        }, p: {
            get: A("Ff"), set: function (a) {
                this.Ff.pa = a.pa;
                this.Ff.qa = a.qa
            }, enumerable: p
        }
    });
    F.Hb = function (a, d, e, f, g, h) {
        this.td = g || new ArrayBuffer(F.Hb.BYTES_PER_ELEMENT);
        this.Ia = h || 0;
        g = this.td;
        h = this.Ia;
        var m = F.Kg.BYTES_PER_ELEMENT;
        this.Np = a ? new F.Kg(a.j, a.A, a.p, g, h) : new F.Kg(q, q, q, g, h);
        this.Lo = d ? new F.Kg(d.j, d.A, d.p, g, h + m) : new F.Kg(q, q, q, g, h + m);
        this.Qp = e ? new F.Kg(e.j, e.A, e.p, g, h + 2 * m) : new F.Kg(q, q, q, g, h + 2 * m);
        this.Po = f ? new F.Kg(f.j, f.A, f.p, g, h + 3 * m) : new F.Kg(q, q, q, g, h + 3 * m)
    };
    F.Hb.BYTES_PER_ELEMENT = 96;
    Object.defineProperties(F.Hb.prototype, {
        U: {
            get: A("Np"), set: function (a) {
                var d = this.Np;
                d.j = a.j;
                d.A = a.A;
                d.p = a.p
            }, enumerable: p
        }, K: {
            get: A("Lo"), set: function (a) {
                var d = this.Lo;
                d.j = a.j;
                d.A = a.A;
                d.p = a.p
            }, enumerable: p
        }, O: {
            get: A("Qp"), set: function (a) {
                var d =
                    this.Qp;
                d.j = a.j;
                d.A = a.A;
                d.p = a.p
            }, enumerable: p
        }, V: {
            get: A("Po"), set: function (a) {
                var d = this.Po;
                d.j = a.j;
                d.A = a.A;
                d.p = a.p
            }, enumerable: p
        }, cq: {get: A("td"), enumerable: p}
    });
    F.bL = function () {
        return new F.Hb
    };
    F.aL = function (a) {
        if (!a)return F.bL();
        var d = a.U, e = a.K, f = a.O;
        a = a.V;
        return {
            U: {
                j: {x: d.j.x, y: d.j.y, e: d.j.e},
                A: {r: d.A.r, g: d.A.g, b: d.A.b, a: d.A.a},
                p: {pa: d.p.pa, qa: d.p.qa}
            },
            K: {
                j: {x: e.j.x, y: e.j.y, e: e.j.e},
                A: {r: e.A.r, g: e.A.g, b: e.A.b, a: e.A.a},
                p: {pa: e.p.pa, qa: e.p.qa}
            },
            O: {
                j: {x: f.j.x, y: f.j.y, e: f.j.e}, A: {
                    r: f.A.r, g: f.A.g,
                    b: f.A.b, a: f.A.a
                }, p: {pa: f.p.pa, qa: f.p.qa}
            },
            V: {
                j: {x: a.j.x, y: a.j.y, e: a.j.e},
                A: {r: a.A.r, g: a.A.g, b: a.A.b, a: a.A.a},
                p: {pa: a.p.pa, qa: a.p.qa}
            }
        }
    };
    F.Qba = function (a) {
        if (!a)return [];
        for (var d = [], e = 0; e < a.length; e++)d.push(F.aL(a[e]));
        return d
    };
    F.Id = function (a, d, e, f, g) {
        this.td = f || new ArrayBuffer(F.Id.BYTES_PER_ELEMENT);
        this.Ia = g || 0;
        f = this.td;
        g = this.Ia;
        var h = N.BYTES_PER_ELEMENT;
        this.bb = a ? new N(a.x, a.y, f, g) : new N(0, 0, f, g);
        this.ts = d ? F.color(d.r, d.g, d.b, d.a, f, g + h) : F.color(0, 0, 0, 0, f, g + h);
        this.Ff = e ? new fa(e.pa, e.qa, f,
            g + h + F.Pf.BYTES_PER_ELEMENT) : new fa(0, 0, f, g + h + F.Pf.BYTES_PER_ELEMENT)
    };
    F.Id.BYTES_PER_ELEMENT = 20;
    Object.defineProperties(F.Id.prototype, {
        j: {
            get: A("bb"), set: function (a) {
                this.bb.x = a.x;
                this.bb.y = a.y
            }, enumerable: p
        }, A: {
            get: A("ts"), set: function (a) {
                var d = this.ts;
                d.r = a.r;
                d.g = a.g;
                d.b = a.b;
                d.a = a.a
            }, enumerable: p
        }, p: {
            get: A("Ff"), set: function (a) {
                this.Ff.pa = a.pa;
                this.Ff.qa = a.qa
            }, enumerable: p
        }
    });
    F.nc = function (a, d, e, f, g) {
        this.td = f || new ArrayBuffer(F.nc.BYTES_PER_ELEMENT);
        this.Ia = g || 0;
        f = this.td;
        g = this.Ia;
        var h = F.Id.BYTES_PER_ELEMENT;
        this.lL = a ? new F.Id(a.j, a.A, a.p, f, g) : new F.Id(q, q, q, f, g);
        this.vL = d ? new F.Id(d.j, d.A, d.p, f, g + h) : new F.Id(q, q, q, f, g + h);
        this.BL = e ? new F.Id(e.j, e.A, e.p, f, g + 2 * h) : new F.Id(q, q, q, f, g + 2 * h)
    };
    F.nc.BYTES_PER_ELEMENT = 60;
    Object.defineProperties(F.nc.prototype, {
        a: {
            get: A("lL"), set: function (a) {
                var d = this.lL;
                d.j = a.j;
                d.A = a.A;
                d.p = a.p
            }, enumerable: p
        }, b: {
            get: A("vL"), set: function (a) {
                var d = this.vL;
                d.j = a.j;
                d.A = a.A;
                d.p = a.p
            }, enumerable: p
        }, s: {
            get: A("BL"), set: function (a) {
                var d = this.BL;
                d.j = a.j;
                d.A = a.A;
                d.p = a.p
            }, enumerable: p
        }
    })
};
F.N.CB = function () {
    var a = F.color;
    a.g_ = function () {
        return a(255, 255, 255)
    };
    a.h_ = function () {
        return a(255, 255, 0)
    };
    a.FZ = function () {
        return a(0, 0, 255)
    };
    a.NZ = function () {
        return a(0, 255, 0)
    };
    a.YZ = function () {
        return a(255, 0, 0)
    };
    a.SZ = function () {
        return a(255, 0, 255)
    };
    a.EZ = function () {
        return a(0, 0, 0)
    };
    a.WZ = function () {
        return a(255, 127, 0)
    };
    a.MZ = function () {
        return a(166, 166, 166)
    };
    F.k(a, "WHITE", a.g_);
    F.k(a, "YELLOW", a.h_);
    F.k(a, "BLUE", a.FZ);
    F.k(a, "GREEN", a.NZ);
    F.k(a, "RED", a.YZ);
    F.k(a, "MAGENTA", a.SZ);
    F.k(a, "BLACK", a.EZ);
    F.k(a,
        "ORANGE", a.WZ);
    F.k(a, "GRAY", a.MZ);
    F.dc.hZ = function () {
        return new F.dc(F.ONE, F.ZERO)
    };
    F.dc.vY = function () {
        return new F.dc(F.ONE, F.ONE_MINUS_SRC_ALPHA)
    };
    F.dc.uY = function () {
        return new F.dc(F.SRC_ALPHA, F.ONE_MINUS_SRC_ALPHA)
    };
    F.dc.tY = function () {
        return new F.dc(F.SRC_ALPHA, F.ONE)
    };
    F.k(F.dc, "DISABLE", F.dc.hZ);
    F.k(F.dc, "ALPHA_PREMULTIPLIED", F.dc.vY);
    F.k(F.dc, "ALPHA_NON_PREMULTIPLIED", F.dc.uY);
    F.k(F.dc, "ADDITIVE", F.dc.tY)
};
F.Pf = function (a, c, d, e) {
    this.r = a || 0;
    this.g = c || 0;
    this.b = d || 0;
    this.a = e == q ? 255 : e
};
F.color = function (a, c, d, e) {
    return a === k ? {r: 0, g: 0, b: 0, a: 255} : F.Dd(a) ? F.IQ(a) : F.Gq(a) ? {
        r: a.r,
        g: a.g,
        b: a.b,
        a: a.a == q ? 255 : a.a
    } : {r: a, g: c, b: d, a: e == q ? 255 : e}
};
F.Sda = function (a, c) {
    return a.r === c.r && a.g === c.g && a.b === c.b
};
function ga() {
    this.timestamp = this.e = this.y = this.x = 0
}
function N(a, c) {
    this.x = a || 0;
    this.y = c || 0
}
F.BA = function (a, c) {
    return new N(a, c)
};
function U(a, c, d) {
    this.x = a || 0;
    this.y = c || 0;
    this.e = d || 0
}
F.Opa = function (a, c, d) {
    return new U(a, c, d)
};
function fa(a, c) {
    this.pa = a || 0;
    this.qa = c || 0
}
F.vpa = function (a, c) {
    return new fa(a, c)
};
F.dc = function (a, c) {
    this.src = a;
    this.J = c
};
F.Gda = function () {
    return new F.dc(F.ONE, F.ZERO)
};
F.IQ = function (a) {
    a = a.replace(/^#?/, "0x");
    a = parseInt(a);
    return F.color(a >> 16, (a >> 8) % 256, a % 256)
};
F.Uda = function (a) {
    var c = a.r.toString(16), d = a.g.toString(16), e = a.b.toString(16);
    return "#" + (16 > a.r ? "0" + c : c) + (16 > a.g ? "0" + d : d) + (16 > a.b ? "0" + e : e)
};
F.ew = 0;
F.Wr = 1;
F.iK = 2;
F.es = 0;
F.eY = 1;
F.cL = 2;
F.Yba = F.za.extend({
    vp: q, At: q, xC: 0, ctor: function () {
        this.vp = {};
        this.At = {};
        this.xC = 2 << (0 | 10 * Math.random())
    }, oY: function () {
        this.xC++;
        return "key_" + this.xC
    }, $na: function (a, c) {
        if (c != q) {
            var d = this.oY();
            this.vp[d] = c;
            this.At[d] = a
        }
    }, N6: function (a) {
        if (a == q)return q;
        var c = this.vp, d;
        for (d in c)if (c[d] === a)return this.At[d];
        return q
    }, Npa: function (a) {
        return this.N6(a)
    }, z7: function (a) {
        if (a != q) {
            var c = this.vp, d;
            for (d in c)if (c[d] === a) {
                delete this.At[d];
                delete c[d];
                break
            }
        }
    }, Ama: function (a) {
        if (a != q)for (var c = 0; c < a.length; c++)this.z7(a[c])
    },
    g2: function () {
        var a = [], c = this.vp, d;
        for (d in c)a.push(c[d]);
        return a
    }, uma: function () {
        this.vp = {};
        this.At = {}
    }, count: function () {
        return this.g2().length
    }
});
function ha() {
    this.fontName = "Arial";
    this.fontSize = 12;
    this.textAlign = F.Wr;
    this.verticalAlign = F.es;
    this.fillStyle = F.color(255, 255, 255, 255);
    this.boundingHeight = this.boundingWidth = 0;
    this.hI = r;
    this.strokeStyle = F.color(255, 255, 255, 255);
    this.lineWidth = 1;
    this.ES = r;
    this.shadowBlur = this.shadowOffsetY = this.shadowOffsetX = 0;
    this.shadowOpacity = 1
}
F.B === F.Y && (F.assert(F.ac(F.N.pC), F.i.Hd, "CCTypesWebGL.js"), F.N.pC(), delete F.N.pC);
F.assert(F.ac(F.N.CB), F.i.Hd, "CCTypesPropertyDefine.js");
F.N.CB();
delete F.N.CB;
F.Lba = [];
F.Mba = {};
F.U$ = "device-dpi";
F.$T = "high-dpi";
F.W$ = "medium-dpi";
F.V$ = "low-dpi";
F.TA = F.za.extend({
    kca: q,
    Gi: q,
    Xk: q,
    Bp: q,
    Qe: q,
    Bl: q,
    BE: r,
    FC: p,
    Hm: 1,
    zF: "",
    yE: q,
    sa: 1,
    LN: 1,
    Ya: 1,
    MN: 1,
    op: 0,
    PD: 5,
    Ri: q,
    kO: q,
    oO: q,
    nO: q,
    lO: q,
    mO: q,
    Qs: r,
    fca: r,
    bda: q,
    eN: q,
    fN: q,
    Yca: r,
    TC: q,
    cl: q,
    hD: 1,
    yw: r,
    oN: p,
    ny: q,
    ctor: function () {
        var a = document, c = F.Ck, d = F.Eg;
        this.cl = F.Dn.parentNode === a.body ? a.documentElement : F.Dn.parentNode;
        this.Gi = F.size(0, 0);
        this.xD();
        var a = F.lb.width, e = F.lb.height;
        this.Xk = F.size(a, e);
        this.Bp = F.size(a, e);
        this.Qe = F.rect(0, 0, a, e);
        this.Bl = F.rect(0, 0, a, e);
        this.TC = {left: 0, top: 0};
        this.zF = "Cocos2dHTML5";
        a = F.ta;
        this.s3(a.Tl == a.zo || a.Tl == a.rB);
        F.io && F.io.oa(this.Bl);
        this.kO = new F.sf(c.mv, d.XA);
        this.oO = new F.sf(c.SV, d.Zv);
        this.nO = new F.sf(c.mv, d.qB);
        this.lO = new F.sf(c.mv, d.YA);
        this.mO = new F.sf(c.mv, d.ZA);
        this.eN = F.lb;
        this.fN = F.l;
        this.ny = F.$T
    },
    zE: function () {
        var a = this.Bp.width, c = this.Bp.height;
        this.yE && (this.xD(), this.yE.call());
        0 < a && this.OH(a, c, this.Ri)
    },
    Doa: function (a) {
        this.ny = a;
        this.xO()
    },
    Lha: A("ny"),
    H7: function (a) {
        a ? this.yw || (this.yw = p, a = this.zE.bind(this), F.Sa(window, "resize", a, r)) : this.yw && (this.yw =
            p, a = this.zE.bind(this), window.removeEventListener("resize", a, r))
    },
    qoa: function (a) {
        if (F.ac(a) || a == q)this.yE = a
    },
    xD: function () {
        var a = this.Gi;
        a.width = this.cl.clientWidth;
        a.height = this.cl.clientHeight
    },
    Zba: function () {
        var a = this.Bp.width, c = this.Bp.height;
        0 < a && this.OH(a, c, this.Ri)
    },
    xO: function () {
        if (this.oN) {
            var a = document.getElementById("cocosMetaElement");
            a && document.head.removeChild(a);
            var c, d = (a = document.getElementsByName("viewport")) ? a[0] : q, e, a = F.bc("meta");
            a.id = "cocosMetaElement";
            a.name = "viewport";
            a.content = "";
            c = F.ta.nh && F.ta.$h == F.ta.pr ? {
                width: "device-width",
                "initial-scale": "1.0"
            } : {width: "device-width", "user-scalable": "no", "maximum-scale": "1.0", "initial-scale": "1.0"};
            F.ta.nh && (c["target-densitydpi"] = this.ny);
            e = d ? d.content : "";
            for (var f in c)RegExp(f).test(e) || (e += "," + f + "\x3d" + c[f]);
            "" != e && (e = e.substr(1));
            a.content = e;
            d && (d.content = e);
            document.head.appendChild(a)
        }
    },
    Rca: function () {
        var a = F.Fb();
        this.Ya = this.sa = a
    },
    Oca: function () {
        this.sa = this.LN;
        this.Ya = this.MN
    },
    $ba: u(),
    D5: function () {
        this.Qs = p
    },
    Z1: z("oN"),
    s3: function (a) {
        this.BE = a ? p : r
    },
    Lia: A("BE"),
    cfa: function (a) {
        this.FC = a ? p : r
    },
    xia: A("FC"),
    end: u(),
    Iia: function () {
        return this.eN != q && this.fN != q
    },
    Ena: function (a) {
        this.hD = a;
        F.L.eo(F.L.Qn())
    },
    spa: u(),
    Kna: u(),
    pna: function (a, c) {
        this.TC = {left: a, top: c}
    },
    Sfa: A("TC"),
    sga: function () {
        return F.size(this.Gi.width, this.Gi.height)
    },
    Dna: function (a, c) {
        this.Gi.width = a;
        this.Gi.height = c;
        this.cl.style.width = a + "px";
        this.cl.style.height = c + "px";
        this.zE();
        F.L.eo(F.L.Qn())
    },
    Mda: u(),
    kh: function () {
        return F.size(this.Bl.width,
            this.Bl.height)
    },
    $y: function () {
        return F.d(this.Bl.x, this.Bl.y)
    },
    Jda: D(p),
    oha: A("Ri"),
    D8: function (a) {
        if (a instanceof F.sf)this.Ri = a; else {
            var c = F.sf;
            a === c.XA && (this.Ri = this.kO);
            a === c.Zv && (this.Ri = this.oO);
            a === c.qB && (this.Ri = this.nO);
            a === c.YA && (this.Ri = this.lO);
            a === c.ZA && (this.Ri = this.mO)
        }
    },
    OH: function (a, c, d) {
        isNaN(a) || 0 == a || isNaN(c) || 0 == c ? F.log(F.i.oU) : (this.D8(d), (d = this.Ri) ? (d.Yn(this), F.ta.nh && this.xO(), this.xD(), this.Xk = F.size(a, c), this.Bp = F.size(a, c), a = d.apply(this, this.Xk), a.scale && 2 == a.scale.length &&
        (this.sa = a.scale[0], this.Ya = a.scale[1]), a.viewport && (a = this.Qe = a.viewport, c = this.Bl, c.width = F.lb.width / this.sa, c.height = F.lb.height / this.Ya, c.x = -a.x / this.sa, c.y = -a.y / this.Ya), a = F.L, a.ic.width = this.Xk.width, a.ic.height = this.Xk.height, d.Xn(this), F.dv.width = a.ic.width, F.dv.height = a.ic.height, F.B == F.Y && (a.Vo(), a.kS()), this.LN = this.sa, this.MN = this.Ya, F.dU && F.dU.Nca(), F.io && F.io.oa(this.Bl)) : F.log(F.i.pU))
    },
    yG: function () {
        return F.size(this.Xk.width, this.Xk.height)
    },
    X8: function (a, c, d, e) {
        var f = this.hD, g =
            this.sa, h = this.Ya;
        F.l.viewport(a * g * f + this.Qe.x * f, c * h * f + this.Qe.y * f, d * g * f, e * h * f)
    },
    soa: function (a, c, d, e) {
        var f = this.hD, g = this.sa, h = this.Ya;
        F.l.scissor(a * g * f + this.Qe.x * f, c * h * f + this.Qe.y * f, d * g * f, e * h * f)
    },
    Nia: function () {
        var a = F.l;
        return a.isEnabled(a.SCISSOR_TEST)
    },
    tha: function () {
        var a = F.l, c = this.sa, d = this.Ya, a = a.getParameter(a.SCISSOR_BOX);
        return F.rect((a[0] - this.Qe.x) / c, (a[1] - this.Qe.y) / d, a[2] / c, a[3] / d)
    },
    dpa: function (a) {
        a != q && 0 < a.length && (this.zF = a)
    },
    dia: A("zF"),
    eia: A("Qe"),
    sQ: A("sa"),
    tQ: A("Ya"),
    $fa: A("Hm"),
    ZF: function (a, c, d) {
        return {x: this.Hm * (a - d.left), y: this.Hm * (d.top + d.height - c)}
    },
    $Y: function (a, c) {
        var d = this.Qe;
        a.x = (this.Hm * (a.x - c.left) - d.x) / this.sa;
        a.y = (this.Hm * (c.top + c.height - a.y) - d.y) / this.Ya
    },
    Rw: function (a) {
        for (var c = this.Qe, d = this.sa, e = this.Ya, f, g, h, m = 0; m < a.length; m++)f = a[m], g = f.yd, h = f.Ph, f.QE((g.x - c.x) / d, (g.y - c.y) / e), f.fn((h.x - c.x) / d, (h.y - c.y) / e)
    }
});
F.TA.mD = function () {
    this.qx || (this.qx = this.qx || new F.TA, this.qx.D5());
    return this.qx
};
F.Ck = F.za.extend({
    Yn: u(), apply: u(), Xn: u(), UE: function (a, c, d) {
        var e = a.cl;
        F.view.FC && (F.ta.nh && e == document.documentElement) && F.screen.w2(e);
        var e = F.lb, f = F.Dn;
        f.style.width = e.style.width = c + "px";
        f.style.height = e.style.height = d + "px";
        f = a.Hm = 1;
        a.BE && (f = a.Hm = window.devicePixelRatio || 1);
        e.width = c * f;
        e.height = d * f;
        a = document.body;
        var g;
        if (a && (g = a.style))g.paddingTop = g.paddingTop || "0px", g.paddingRight = g.paddingRight || "0px", g.paddingBottom = g.paddingBottom || "0px", g.paddingLeft = g.paddingLeft || "0px", g.borderTop = g.borderTop ||
            "0px", g.borderRight = g.borderRight || "0px", g.borderBottom = g.borderBottom || "0px", g.borderLeft = g.borderLeft || "0px", g.marginTop = g.marginTop || "0px", g.marginRight = g.marginRight || "0px", g.marginBottom = g.marginBottom || "0px", g.marginLeft = g.marginLeft || "0px"
    }, OM: function () {
        document.body.insertBefore(F.Dn, document.body.firstChild);
        var a = document.body.style;
        a.width = window.innerWidth + "px";
        a.height = window.innerHeight + "px";
        a.overflow = "hidden";
        a = F.Dn.style;
        a.position = "fixed";
        a.left = a.top = "0px";
        document.body.scrollTop =
            0
    }
});
F.Eg = F.za.extend({
    AE: {scale: [1, 1], viewport: q}, os: function (a, c, d, e, f, g) {
        2 > Math.abs(a - d) && (d = a);
        2 > Math.abs(c - e) && (e = c);
        a = F.rect(Math.round((a - d) / 2), Math.round((c - e) / 2), d, e);
        F.B == F.Aa && F.l.translate(a.x, a.y + e);
        this.AE.scale = [f, g];
        this.AE.viewport = a;
        return this.AE
    }, Yn: u(), apply: function () {
        return {scale: [1, 1]}
    }, Xn: u()
});
(function () {
    var a = F.Ck.extend({
        apply: function (a) {
            this.UE(a, a.Gi.width, a.Gi.height)
        }
    }), c = F.Ck.extend({
        apply: function (a, c) {
            var d = a.Gi.width, e = a.Gi.height, f = F.Dn.style, t = c.width, v = c.height, w = d / t, x = e / v, y, B;
            w < x ? (y = d, B = v * w) : (y = t * x, B = e);
            t = Math.round((d - y) / 2);
            B = Math.round((e - B) / 2);
            this.UE(a, d - 2 * t, e - 2 * B);
            f.marginLeft = t + "px";
            f.marginRight = t + "px";
            f.marginTop = B + "px";
            f.marginBottom = B + "px"
        }
    });
    a.extend({
        Yn: function (a) {
            this._super(a);
            a.cl = document.documentElement
        }, apply: function (a) {
            this._super(a);
            this.OM()
        }
    });
    c.extend({
        Yn: function (a) {
            this._super(a);
            a.cl = document.documentElement
        }, apply: function (a, c) {
            this._super(a, c);
            this.OM()
        }
    });
    var d = F.Ck.extend({
        apply: function (a) {
            this.UE(a, F.lb.width, F.lb.height)
        }
    });
    F.Ck.mv = new a;
    F.Ck.SV = new c;
    F.Ck.cba = new d;
    var a = F.Eg.extend({
        apply: function (a, c) {
            var d = F.lb.width, e = F.lb.height;
            return this.os(d, e, d, e, d / c.width, e / c.height)
        }
    }), c = F.Eg.extend({
        apply: function (a, c) {
            var d = F.lb.width, e = F.lb.height, f = c.width, t = c.height, v = d / f, w = e / t, x = 0, y, B;
            v < w ? (x = v, y = d, B = t * x) : (x = w, y = f * x, B = e);
            return this.os(d, e, y, B, x, x)
        }
    }), d = F.Eg.extend({
        apply: function (a,
                         c) {
            var d = F.lb.width, e = F.lb.height, f = c.width, t = c.height, v = d / f, w = e / t, x, y, B;
            v < w ? (x = w, y = f * x, B = e) : (x = v, y = d, B = t * x);
            return this.os(d, e, y, B, x, x)
        }
    }), e = F.Eg.extend({
        apply: function (a, c) {
            var d = F.lb.width, e = F.lb.height, f = e / c.height;
            return this.os(d, e, d, e, f, f)
        }, Xn: function (a) {
            F.L.ic = a.kh()
        }
    }), f = F.Eg.extend({
        apply: function (a, c) {
            var d = F.lb.width, e = F.lb.height, f = d / c.width;
            return this.os(d, e, d, e, f, f)
        }, Xn: function (a) {
            F.L.ic = a.kh()
        }
    });
    F.Eg.XA = new a;
    F.Eg.Zv = new c;
    F.Eg.qB = new d;
    F.Eg.YA = new e;
    F.Eg.ZA = new f
})();
F.sf = F.za.extend({
    Pw: q, Qw: q, ctor: function (a, c) {
        this.b8(a);
        this.c8(c)
    }, Yn: function (a) {
        this.Pw.Yn(a);
        this.Qw.Yn(a)
    }, apply: function (a, c) {
        this.Pw.apply(a, c);
        return this.Qw.apply(a, c)
    }, Xn: function (a) {
        this.Pw.Xn(a);
        this.Qw.Xn(a)
    }, b8: function (a) {
        a instanceof F.Ck && (this.Pw = a)
    }, c8: function (a) {
        a instanceof F.Eg && (this.Qw = a)
    }
});
F.sf.XA = 0;
F.sf.qB = 1;
F.sf.Zv = 2;
F.sf.YA = 3;
F.sf.ZA = 4;
F.sf.cY = 5;
F.screen = {
    ly: r,
    jE: q,
    mF: "",
    bl: q,
    sZ: [["requestFullscreen", "exitFullscreen", "fullscreenchange", "fullscreenEnabled", "fullscreenElement"], ["requestFullScreen", "exitFullScreen", "fullScreenchange", "fullScreenEnabled", "fullScreenElement"], ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitfullscreenchange", "webkitIsFullScreen", "webkitCurrentFullScreenElement"], ["mozRequestFullScreen", "mozCancelFullScreen", "mozfullscreenchange", "mozFullScreen", "mozFullScreenElement"], ["msRequestFullscreen", "msExitFullscreen",
        "MSFullscreenChange", "msFullscreenEnabled", "msFullscreenElement"]],
    oa: function () {
        this.bl = {};
        var a, c, d = this.sZ, e;
        a = 0;
        for (l = d.length; a < l; a++)if ((c = d[a]) && c[1] in document) {
            a = 0;
            for (e = c.length; a < e; a++)this.bl[d[0][a]] = c[a];
            break
        }
        this.ly = this.bl.requestFullscreen != k;
        this.mF = "ontouchstart" in window ? "touchstart" : "mousedown"
    },
    fullScreen: function () {
        return this.ly && document[this.bl.fullscreenEnabled]
    },
    RR: function (a, c) {
        if (this.ly) {
            a = a || document.documentElement;
            a[this.bl.requestFullscreen]();
            if (c) {
                var d = this.bl.xfa;
                this.jE && document.removeEventListener(d, this.jE);
                this.jE = c;
                F.Sa(document, d, c, r)
            }
            return a[this.bl.requestFullscreen]()
        }
    },
    ffa: function () {
        return this.ly ? document[this.bl.exitFullscreen]() : p
    },
    w2: function (a, c) {
        function d() {
            f.RR(a, c);
            e.removeEventListener(f.mF, d)
        }

        a = a || document.body;
        var e = F.lb || a, f = this;
        this.RR(a, c);
        F.Sa(e, this.mF, d)
    }
};
F.screen.oa();
F.io = {
    SS: F.d(0, 0),
    TS: F.d(0, 0),
    top: F.d(0, 0),
    NF: F.d(0, 0),
    lP: F.d(0, 0),
    bottom: F.d(0, 0),
    Gy: F.d(0, 0),
    left: F.d(0, 0),
    right: F.d(0, 0),
    width: 0,
    height: 0,
    oa: function (a) {
        var c = this.width = a.width, d = this.height = a.height, e = a.x;
        a = a.y;
        var f = a + d, g = e + c;
        this.SS.x = e;
        this.SS.y = f;
        this.TS.x = g;
        this.TS.y = f;
        this.top.x = e + c / 2;
        this.top.y = f;
        this.NF.x = e;
        this.NF.y = a;
        this.lP.x = g;
        this.lP.y = a;
        this.bottom.x = e + c / 2;
        this.bottom.y = a;
        this.Gy.x = e + c / 2;
        this.Gy.y = a + d / 2;
        this.left.x = e;
        this.left.y = a + d / 2;
        this.right.x = g;
        this.right.y = a + d / 2
    }
};
F.SX = -90;
F.TX = 90;
F.UX = 180;
F.Nba = 0;
F.WQ = {
    yp: r,
    tN: r,
    SN: F.d(0, 0),
    o0: F.d(0, 0),
    TN: [],
    kE: 0,
    zl: [],
    nF: {},
    op: 0,
    PD: 5,
    Aw: r,
    Bw: 1 / 30,
    oL: 1,
    vm: 0,
    zC: q,
    zw: q,
    e_: function () {
        for (var a = this.op, c = 0; c < this.PD; c++) {
            if (!(a & 1))return this.op |= 1 << c, c;
            a >>= 1
        }
        return -1
    },
    w0: function (a) {
        0 > a || a >= this.PD || (a = ~(1 << a), this.op &= a)
    },
    el: q,
    NG: function (a) {
        for (var c, d, e, f = [], g = this.nF, h = 0, m = a.length; h < m; h++)if (c = a[h], e = c.Sg, d = g[e], d == q) {
            var n = this.e_();
            -1 == n ? F.log(F.i.F5, n) : (d = this.zl[n] = new F.ow(c.yd.x, c.yd.y, c.Sg), d.fn(c.Ph), g[e] = n, f.push(d))
        }
        0 < f.length && (this.el.Rw(f),
            a = new F.Fg(f), a.$k = F.Fg.qo.DA, F.La.dispatchEvent(a))
    },
    OG: function (a) {
        for (var c, d, e = [], f = this.zl, g = 0, h = a.length; g < h; g++)c = a[g], d = c.Sg, d = this.nF[d], d != q && f[d] && (f[d].QE(c.yd), f[d].fn(c.Ph), e.push(f[d]));
        0 < e.length && (this.el.Rw(e), a = new F.Fg(e), a.$k = F.Fg.qo.lB, F.La.dispatchEvent(a))
    },
    ez: function (a) {
        a = this.uQ(a);
        0 < a.length && (this.el.Rw(a), a = new F.Fg(a), a.$k = F.Fg.qo.UA, F.La.dispatchEvent(a))
    },
    GQ: function (a) {
        a = this.uQ(a);
        0 < a.length && (this.el.Rw(a), a = new F.Fg(a), a.$k = F.Fg.qo.KA, F.La.dispatchEvent(a))
    },
    uQ: function (a) {
        for (var c,
                 d, e, f = [], g = this.zl, h = this.nF, m = 0, n = a.length; m < n; m++)c = a[m], e = c.Sg, d = h[e], d != q && g[d] && (g[d].QE(c.yd), g[d].fn(c.Ph), f.push(g[d]), this.w0(d), delete h[e]);
        return f
    },
    ei: function (a) {
        var c = document.documentElement, d = window, e = q, e = F.ac(a.getBoundingClientRect) ? a.getBoundingClientRect() : a instanceof HTMLCanvasElement ? {
            left: 0,
            top: 0,
            width: a.width,
            height: a.height
        } : {left: 0, top: 0, width: parseInt(a.style.width), height: parseInt(a.style.height)};
        return {
            left: e.left + d.pageXOffset - c.clientLeft, top: e.top + d.pageYOffset -
            c.clientTop, width: e.width, height: e.height
        }
    },
    I4: function (a) {
        for (var c = q, d = this.TN, e = a.Sg, f = d.length - 1; 0 <= f; f--)if (d[f].Sg == e) {
            c = d[f];
            break
        }
        c || (c = a);
        return c
    },
    B8: function (a) {
        for (var c = r, d = this.TN, e = a.Sg, f = d.length - 1; 0 <= f; f--)if (d[f].Sg == e) {
            d[f] = a;
            c = p;
            break
        }
        c || (50 >= d.length ? d.push(a) : (d[this.kE] = a, this.kE = (this.kE + 1) % 50))
    },
    Wt: function (a, c, d) {
        var e = this.SN;
        a = this.el.ZF(a, c, d);
        c = new F.ow(a.x, a.y);
        c.fn(e.x, e.y);
        e.x = a.x;
        e.y = a.y;
        return c
    },
    yq: function (a, c, d) {
        var e = this.o0;
        this.el.$Y(a, c);
        c = new F.gd(d);
        c.p8(a.x,
            a.y);
        c.Q0(e.x, e.y);
        e.x = a.x;
        e.y = a.y;
        return c
    },
    Aq: function (a, c) {
        if (a.pageX != q)return {x: a.pageX, y: a.pageY};
        c.left -= document.body.scrollLeft;
        c.top -= document.body.scrollTop;
        return {x: a.clientX, y: a.clientY}
    },
    Zy: function (a, c) {
        for (var d = [], e = this.el, f, g, h = this.SN, m = a.changedTouches.length, n = 0; n < m; n++)if (f = a.changedTouches[n]) {
            var s;
            s = F.ta.pr === F.ta.$h ? e.ZF(f.pageX, f.pageY, c) : e.ZF(f.clientX, f.clientY, c);
            f.identifier != q ? (f = new F.ow(s.x, s.y, f.identifier), g = this.I4(f).Tt(), f.fn(g.x, g.y), this.B8(f)) : (f = new F.ow(s.x,
                s.y), f.fn(h.x, h.y));
            h.x = s.x;
            h.y = s.y;
            d.push(f)
        }
        return d
    },
    s7: function (a) {
        if (!this.tN) {
            this.el = F.view;
            var c = this, d = "touches" in F.ta.UF;
            "mouse" in F.ta.UF && (F.Sa(window, "mousedown", function () {
                c.yp = p
            }, r), F.Sa(window, "mouseup", function (e) {
                var f = c.yp;
                c.yp = r;
                if (f) {
                    var f = c.ei(a), m = c.Aq(e, f);
                    F.KR(new F.NJ(f.left, f.top, f.width, f.height), m) || (d || c.ez([c.Wt(m.x, m.y, f)]), f = c.yq(m, f, F.gd.mC), f.bo(e.button), F.La.dispatchEvent(f))
                }
            }, r), F.Sa(a, "mousedown", function (e) {
                c.yp = p;
                var f = c.ei(a), m = c.Aq(e, f);
                d || c.NG([c.Wt(m.x,
                    m.y, f)]);
                f = c.yq(m, f, F.gd.LI);
                f.bo(e.button);
                F.La.dispatchEvent(f);
                e.stopPropagation();
                e.preventDefault();
                a.focus()
            }, r), F.Sa(a, "mouseup", function (e) {
                c.yp = r;
                var f = c.ei(a), m = c.Aq(e, f);
                d || c.ez([c.Wt(m.x, m.y, f)]);
                f = c.yq(m, f, F.gd.mC);
                f.bo(e.button);
                F.La.dispatchEvent(f);
                e.stopPropagation();
                e.preventDefault()
            }, r), F.Sa(a, "mousemove", function (e) {
                    var f = c.ei(a), m = c.Aq(e, f);
                    d || c.OG([c.Wt(m.x, m.y, f)]);
                    f = c.yq(m, f, F.gd.eJ);
                    c.yp ? f.bo(e.button) : f.bo(q);
                    F.La.dispatchEvent(f);
                    e.stopPropagation();
                    e.preventDefault()
                },
                r), F.Sa(a, "mousewheel", function (d) {
                var e = c.ei(a), f = c.Aq(d, e), e = c.yq(f, e, F.gd.OB);
                e.bo(d.button);
                e.zS(0, d.wheelDelta);
                F.La.dispatchEvent(e);
                d.stopPropagation();
                d.preventDefault()
            }, r), F.Sa(a, "DOMMouseScroll", function (d) {
                var e = c.ei(a), f = c.Aq(d, e), e = c.yq(f, e, F.gd.OB);
                e.bo(d.button);
                e.zS(0, -120 * d.detail);
                F.La.dispatchEvent(e);
                d.stopPropagation();
                d.preventDefault()
            }, r));
            if (window.navigator.msPointerEnabled) {
                var e = {MSPointerDown: c.NG, MSPointerMove: c.OG, MSPointerUp: c.ez, MSPointerCancel: c.GQ}, f;
                for (f in e)(function (d,
                                       e) {
                    F.Sa(a, d, function (d) {
                        var f = c.ei(a);
                        f.left -= document.documentElement.scrollLeft;
                        f.top -= document.documentElement.scrollTop;
                        e.call(c, [c.Wt(d.clientX, d.clientY, f)]);
                        d.stopPropagation()
                    }, r)
                })(f, e[f])
            }
            d && (F.Sa(a, "touchstart", function (d) {
                if (d.changedTouches) {
                    var e = c.ei(a);
                    e.left -= document.body.scrollLeft;
                    e.top -= document.body.scrollTop;
                    c.NG(c.Zy(d, e));
                    d.stopPropagation();
                    d.preventDefault();
                    a.focus()
                }
            }, r), F.Sa(a, "touchmove", function (d) {
                if (d.changedTouches) {
                    var e = c.ei(a);
                    e.left -= document.body.scrollLeft;
                    e.top -= document.body.scrollTop;
                    c.OG(c.Zy(d, e));
                    d.stopPropagation();
                    d.preventDefault()
                }
            }, r), F.Sa(a, "touchend", function (d) {
                if (d.changedTouches) {
                    var e = c.ei(a);
                    e.left -= document.body.scrollLeft;
                    e.top -= document.body.scrollTop;
                    c.ez(c.Zy(d, e));
                    d.stopPropagation();
                    d.preventDefault()
                }
            }, r), F.Sa(a, "touchcancel", function (d) {
                if (d.changedTouches) {
                    var e = c.ei(a);
                    e.left -= document.body.scrollLeft;
                    e.top -= document.body.scrollTop;
                    c.GQ(c.Zy(d, e));
                    d.stopPropagation();
                    d.preventDefault()
                }
            }, r));
            this.cO();
            this.bO();
            this.tN =
                p
        }
    },
    cO: u(),
    bO: u(),
    update: function (a) {
        this.vm > this.Bw && (this.vm -= this.Bw, F.La.dispatchEvent(new F.uU(this.zC)));
        this.vm += a
    }
};
var M = F.WQ;
M.Yma = function (a) {
    this.Aw !== a && (this.Aw = a, a = F.L.Jl(), this.Aw ? (this.vm = 0, a.JH(this)) : (this.vm = 0, a.$u(this)))
};
M.Zma = function (a) {
    this.Bw !== a && (this.Bw = a)
};
M.cO = function () {
    F.Sa(F.lb, "keydown", function (a) {
        F.La.dispatchEvent(new F.RI(a.keyCode, p));
        a.stopPropagation();
        a.preventDefault()
    }, r);
    F.Sa(F.lb, "keyup", function (a) {
        F.La.dispatchEvent(new F.RI(a.keyCode, r));
        a.stopPropagation();
        a.preventDefault()
    }, r)
};
M.bO = function () {
    var a = window;
    this.zC = new ga;
    this.zw = a.DeviceMotionEvent || a.DeviceOrientationEvent;
    F.ta.$h == F.ta.BI && (this.zw = window.DeviceOrientationEvent);
    var c = this.zw == a.DeviceMotionEvent ? "devicemotion" : "deviceorientation", d = navigator.userAgent;
    if (/Android/.test(d) || /Adr/.test(d) && F.ta.$h == F.CI)this.Hca = -1;
    F.Sa(a, c, this.W2.bind(this), r)
};
M.W2 = function (a) {
    var c = window;
    if (this.Aw) {
        var d = this.zC, e, f, g;
        this.zw == window.DeviceMotionEvent ? (g = a.accelerationIncludingGravity, e = 0.1 * this.oL * g.x, f = 0.1 * this.oL * g.y, g = 0.1 * g.e) : (e = 0.981 * (a.gamma / 90), f = 0.981 * -(a.beta / 90), g = 0.981 * (a.alpha / 90));
        F.ta.Tl === F.ta.mJ ? (d.x = -e, d.y = -f) : (d.x = e, d.y = f);
        d.e = g;
        d.timestamp = a.timeStamp || Date.now();
        a = d.x;
        c.orientation === F.TX ? (d.x = -d.y, d.y = a) : c.orientation === F.SX ? (d.x = d.y, d.y = -a) : c.orientation === F.UX && (d.x = -d.x, d.y = -d.y)
    }
};
delete M;
F.w$ = function (a, c, d, e, f, g) {
    this.a = a;
    this.b = c;
    this.s = d;
    this.z = e;
    this.P = f;
    this.Q = g
};
F.qda = function (a, c, d, e, f, g) {
    return {a: a, b: c, s: d, z: e, P: f, Q: g}
};
F.GR = function (a, c) {
    return {x: c.a * a.x + c.s * a.y + c.P, y: c.b * a.x + c.z * a.y + c.Q}
};
F.ml = function (a, c, d) {
    return {x: d.a * a + d.s * c + d.P, y: d.b * a + d.z * c + d.Q}
};
F.fpa = function (a, c) {
    return {width: c.a * a.width + c.s * a.height, height: c.b * a.width + c.z * a.height}
};
F.rda = function () {
    return {a: 1, b: 0, s: 0, z: 1, P: 0, Q: 0}
};
F.pda = function () {
    return {a: 1, b: 0, s: 0, z: 1, P: 0, Q: 0}
};
F.BH = function (a, c) {
    var d = F.Sq(a), e = F.Rq(a), f = F.Fu(a), g = F.Gu(a), h = F.ml(e, d, c), d = F.ml(f, d, c), e = F.ml(e, g, c), m = F.ml(f, g, c), f = Math.min(h.x, d.x, e.x, m.x), g = Math.max(h.x, d.x, e.x, m.x), n = Math.min(h.y, d.y, e.y, m.y), h = Math.max(h.y, d.y, e.y, m.y);
    return F.rect(f, n, g - f, h - n)
};
F.p0 = function (a, c) {
    var d = F.Sq(a), e = F.Rq(a), f = F.Fu(a), g = F.Gu(a), h = F.ml(e, d, c), d = F.ml(f, d, c), e = F.ml(e, g, c), m = F.ml(f, g, c), f = Math.min(h.x, d.x, e.x, m.x), g = Math.max(h.x, d.x, e.x, m.x), n = Math.min(h.y, d.y, e.y, m.y), h = Math.max(h.y, d.y, e.y, m.y);
    a.x = f;
    a.y = n;
    a.width = g - f;
    a.height = h - n;
    return a
};
F.a2 = function (a, c, d) {
    return {a: a.a, b: a.b, s: a.s, z: a.z, P: a.P + a.a * c + a.s * d, Q: a.Q + a.b * c + a.z * d}
};
F.tda = function (a, c, d) {
    return {a: a.a * c, b: a.b * c, s: a.s * d, z: a.z * d, P: a.P, Q: a.Q}
};
F.sda = function (a, c) {
    var d = Math.sin(c), e = Math.cos(c);
    return {a: a.a * e + a.s * d, b: a.b * e + a.z * d, s: a.s * e - a.a * d, z: a.z * e - a.b * d, P: a.P, Q: a.Q}
};
F.wn = function (a, c) {
    return {
        a: a.a * c.a + a.b * c.s,
        b: a.a * c.b + a.b * c.z,
        s: a.s * c.a + a.z * c.s,
        z: a.s * c.b + a.z * c.z,
        P: a.P * c.a + a.Q * c.s + c.P,
        Q: a.P * c.b + a.Q * c.z + c.Q
    }
};
F.oda = function (a, c) {
    return a.a === c.a && a.b === c.b && a.s === c.s && a.z === c.z && a.P === c.P && a.Q === c.Q
};
F.Zp = function (a) {
    var c = 1 / (a.a * a.z - a.b * a.s);
    return {
        a: c * a.z,
        b: -c * a.b,
        s: -c * a.s,
        z: c * a.a,
        P: c * (a.s * a.Q - a.z * a.P),
        Q: c * (a.b * a.P - a.a * a.Q)
    }
};
F.sJ = parseFloat("1.192092896e-07F");
F.tH = function (a) {
    return F.d(-a.x, -a.y)
};
F.sk = function (a, c) {
    return F.d(a.x + c.x, a.y + c.y)
};
F.ie = function (a, c) {
    return F.d(a.x - c.x, a.y - c.y)
};
F.lj = function (a, c) {
    return F.d(a.x * c, a.y * c)
};
F.BR = function (a, c) {
    return F.lj(F.sk(a, c), 0.5)
};
F.Oq = function (a, c) {
    return a.x * c.x + a.y * c.y
};
F.R6 = function (a, c) {
    return a.x * c.y - a.y * c.x
};
F.Qz = function (a) {
    return F.d(-a.y, a.x)
};
F.Gla = function (a) {
    return F.d(a.y, -a.x)
};
F.Fla = function (a, c) {
    return F.lj(c, F.Oq(a, c) / F.Oq(c, c))
};
F.Hla = function (a, c) {
    return F.d(a.x * c.x - a.y * c.y, a.x * c.y + a.y * c.x)
};
F.Kla = function (a, c) {
    return F.d(a.x * c.x + a.y * c.y, a.y * c.x - a.x * c.y)
};
F.zR = function (a) {
    return F.Oq(a, a)
};
F.yR = function (a, c) {
    return F.zR(F.ie(a, c))
};
F.Ul = function (a) {
    return Math.sqrt(F.zR(a))
};
F.Ala = function (a, c) {
    return F.Ul(F.ie(a, c))
};
F.oh = function (a) {
    return F.lj(a, 1 / F.Ul(a))
};
F.Bla = function (a) {
    return F.d(Math.cos(a), Math.sin(a))
};
F.U6 = function (a) {
    return Math.atan2(a.y, a.x)
};
F.od = function (a, c, d) {
    if (c > d) {
        var e = c;
        c = d;
        d = e
    }
    return a < c ? c : a < d ? a : d
};
F.xR = function (a) {
    var c = F.d(0, 0), d = F.d(1, 1);
    return F.d(F.od(a.x, c.x, d.x), F.od(a.y, c.y, d.y))
};
F.Cla = function (a) {
    return F.d(a.width, a.height)
};
F.zla = function (a, c) {
    return F.d(c(a.x), c(a.y))
};
F.AR = function (a, c, d) {
    return F.sk(F.lj(a, 1 - d), F.lj(c, d))
};
F.Dla = function (a, c, d) {
    return a.x - d <= c.x && c.x <= a.x + d && a.y - d <= c.y && c.y <= a.y + d ? p : r
};
F.yla = function (a, c) {
    return F.d(a.x * c.x, a.y * c.y)
};
F.xla = function (a, c) {
    var d = F.oh(a), e = F.oh(c), d = Math.atan2(d.x * e.y - d.y * e.x, F.Oq(d, e));
    return Math.abs(d) < F.sJ ? 0 : d
};
F.wla = function (a, c) {
    var d = Math.acos(F.Oq(F.oh(a), F.oh(c)));
    return Math.abs(d) < F.sJ ? 0 : d
};
F.T6 = function (a, c, d) {
    a = F.ie(a, c);
    var e = Math.cos(d);
    d = Math.sin(d);
    var f = a.x;
    a.x = f * e - a.y * d + c.x;
    a.y = f * d + a.y * e + c.y;
    return a
};
F.sH = function (a, c, d, e, f) {
    if (a.x == c.x && a.y == c.y || d.x == e.x && d.y == e.y)return r;
    var g = c.x - a.x;
    c = c.y - a.y;
    var h = e.x - d.x;
    e = e.y - d.y;
    var m = a.x - d.x;
    a = a.y - d.y;
    d = e * g - h * c;
    f.x = h * a - e * m;
    f.y = g * a - c * m;
    if (0 == d)return 0 == f.x || 0 == f.y ? p : r;
    f.x /= d;
    f.y /= d;
    return p
};
F.Jla = function (a, c, d, e) {
    var f = F.d(0, 0);
    return F.sH(a, c, d, e, f) && 0 <= f.x && 1 >= f.x && 0 <= f.y && 1 >= f.y ? p : r
};
F.Ela = function (a, c, d, e) {
    var f = F.d(0, 0);
    return F.sH(a, c, d, e, f) ? (d = F.d(0, 0), d.x = a.x + f.x * (c.x - a.x), d.y = a.y + f.x * (c.y - a.y), d) : F.d(0, 0)
};
F.Ila = function (a, c) {
    return a != q && c != q ? a.x == c.x && a.y == c.y : r
};
F.Rz = function (a) {
    a.x = 0;
    a.y = 0
};
F.tk = function (a, c) {
    a.x = c.x;
    a.y = c.y
};
F.Pq = function (a, c) {
    a.x *= c;
    a.y *= c
};
F.CR = function (a, c) {
    a.x -= c.x;
    a.y -= c.y
};
F.Pz = function (a, c) {
    a.x += c.x;
    a.y += c.y
};
F.S6 = function (a) {
    F.Pq(a, 1 / Math.sqrt(a.x * a.x + a.y * a.y))
};
F.mI = function (a, c, d, e, f) {
    f += e;
    if (!(1 >= f)) {
        c *= 0.5;
        for (var g, h = f - 1, m = e; m < f; m++) {
            g = 2 * m;
            var n = F.d(a[2 * m], a[2 * m + 1]), s;
            if (0 === m)s = F.Qz(F.oh(F.ie(n, F.d(a[2 * (m + 1)], a[2 * (m + 1) + 1])))); else if (m === h)s = F.Qz(F.oh(F.ie(F.d(a[2 * (m - 1)], a[2 * (m - 1) + 1]), n))); else {
                s = F.d(a[2 * (m - 1)], a[2 * (m - 1) + 1]);
                var t = F.d(a[2 * (m + 1)], a[2 * (m + 1) + 1]), v = F.oh(F.ie(t, n)), w = F.oh(F.ie(s, n)), x = Math.acos(F.Oq(v, w));
                s = x < F.kf(70) ? F.Qz(F.oh(F.BR(v, w))) : x < F.kf(170) ? F.oh(F.BR(v, w)) : F.Qz(F.oh(F.ie(t, s)))
            }
            s = F.lj(s, c);
            d[2 * g] = n.x + s.x;
            d[2 * g + 1] = n.y + s.y;
            d[2 *
            (g + 1)] = n.x - s.x;
            d[2 * (g + 1) + 1] = n.y - s.y
        }
        for (m = 0 == e ? 0 : e - 1; m < h; m++) {
            g = 2 * m;
            a = g + 2;
            c = F.BA(d[2 * g], d[2 * g + 1]);
            f = F.BA(d[2 * (g + 1)], d[2 * (g + 1) + 1]);
            g = F.BA(d[2 * a], d[2 * a]);
            e = F.BA(d[2 * (a + 1)], d[2 * (a + 1) + 1]);
            c = !F.Y9(c.x, c.y, e.x, e.y, f.x, f.y, g.x, g.y);
            if (!c.eu && (0 > c.value || 1 < c.value))c.eu = p;
            c.eu && (d[2 * a] = e.x, d[2 * a + 1] = e.y, d[2 * (a + 1)] = g.x, d[2 * (a + 1) + 1] = g.y)
        }
    }
};
F.Y9 = function (a, c, d, e, f, g, h, m) {
    if (a == d && c == e || f == h && g == m)return {eu: r, value: 0};
    d -= a;
    e -= c;
    f -= a;
    g -= c;
    h -= a;
    m -= c;
    a = Math.sqrt(d * d + e * e);
    d /= a;
    e /= a;
    c = f * d + g * e;
    g = g * d - f * e;
    f = c;
    c = h * d + m * e;
    m = m * d - h * e;
    h = c;
    return g == m ? {eu: r, value: 0} : {eu: p, value: (h + (f - h) * m / (m - g)) / a}
};
F.Ppa = function (a) {
    for (var c = 0, d = a.length; c < d; c++) {
        var e = a[(c + 1) % d], f = a[(c + 2) % d];
        if (0 < F.R6(F.ie(e, a[c]), F.ie(f, e)))return r
    }
    return p
};
F.O$ = function (a, c) {
    c[2] = c[3] = c[6] = c[7] = c[8] = c[9] = c[11] = c[14] = 0;
    c[10] = c[15] = 1;
    c[0] = a.a;
    c[4] = a.s;
    c[12] = a.P;
    c[1] = a.b;
    c[5] = a.z;
    c[13] = a.Q
};
F.oaa = function (a, c) {
    c.a = a[0];
    c.s = a[4];
    c.P = a[12];
    c.b = a[1];
    c.z = a[5];
    c.Q = a[13]
};
F.ow = F.za.extend({
    yd: q, Ph: q, Sg: 0, JO: r, Vi: q, ctor: function (a, c, d) {
        this.yd = F.d(a || 0, c || 0);
        this.Sg = d || 0
    }, Tt: function () {
        return {x: this.yd.x, y: this.yd.y}
    }, t4: function () {
        return this.yd.x
    }, u4: function () {
        return this.yd.y
    }, hha: function () {
        return {x: this.Ph.x, y: this.Ph.y}
    }, yha: function () {
        return {x: this.Vi.x, y: this.Vi.y}
    }, xG: function () {
        return F.ie(this.yd, this.Ph)
    }, s4: function () {
        return {x: this.yd.x, y: this.yd.y}
    }, iha: function () {
        return {x: this.Ph.x, y: this.Ph.y}
    }, zha: function () {
        return {x: this.Vi.x, y: this.Vi.y}
    }, xga: A("Sg"),
    yga: function () {
        F.log("getId is deprecated. Please use getID instead.");
        return this.Sg
    }, Moa: function (a, c, d) {
        this.Ph = this.yd;
        this.yd = F.d(c || 0, d || 0);
        this.Sg = a;
        this.JO || (this.Vi = F.d(this.yd), this.JO = p)
    }, QE: function (a, c) {
        c === k ? (this.yd.x = a.x, this.yd.y = a.y) : (this.yd.x = a, this.yd.y = c)
    }, fn: function (a, c) {
        this.Ph = c === k ? F.d(a.x, a.y) : F.d(a || 0, c || 0)
    }
});
F.Mc = F.za.extend({
    Wb: 0, Mm: r, be: q, JE: z("be"), ctor: z("Wb"), LG: A("Wb"), stopPropagation: function () {
        this.Mm = p
    }, Ria: A("Mm"), Ufa: A("be")
});
F.Mc.iw = 0;
F.Mc.uo = 1;
F.Mc.jo = 2;
F.Mc.xo = 3;
F.Mc.qr = 4;
F.mm = F.Mc.extend({
    dD: q, ZO: q, ctor: function (a) {
        F.Mc.prototype.ctor.call(this, F.Mc.qr);
        this.dD = a
    }, setUserData: z("ZO"), getUserData: A("ZO"), mga: A("dD")
});
F.gd = F.Mc.extend({
    KM: 0, AL: 0, Xh: 0, hf: 0, lE: 0, mE: 0, qO: 0, rO: 0, ctor: function (a) {
        F.Mc.prototype.ctor.call(this, F.Mc.xo);
        this.KM = a
    }, zS: function (a, c) {
        this.qO = a;
        this.rO = c
    }, uha: A("qO"), vha: A("rO"), p8: function (a, c) {
        this.Xh = a;
        this.hf = c
    }, Tt: function () {
        return {x: this.Xh, y: this.hf}
    }, s4: function () {
        return {x: this.Xh, y: F.view.Xk.height - this.hf}
    }, Q0: function (a, c) {
        this.lE = a;
        this.mE = c
    }, xG: function () {
        return {x: this.Xh - this.lE, y: this.hf - this.mE}
    }, Yfa: function () {
        return this.Xh - this.lE
    }, Zfa: function () {
        return this.hf - this.mE
    },
    bo: z("AL"), Gfa: A("AL"), t4: A("Xh"), u4: A("hf")
});
F.gd.NONE = 0;
F.gd.LI = 1;
F.gd.mC = 2;
F.gd.eJ = 3;
F.gd.OB = 4;
F.gd.K$ = 0;
F.gd.M$ = 2;
F.gd.L$ = 1;
F.gd.F$ = 3;
F.gd.G$ = 4;
F.gd.H$ = 5;
F.gd.I$ = 6;
F.gd.J$ = 7;
F.Fg = F.Mc.extend({
    $k: 0, zl: q, ctor: function (a) {
        F.Mc.prototype.ctor.call(this, F.Mc.iw);
        this.zl = a || []
    }, lga: A("$k"), Vha: A("zl"), Qca: z("$k"), Tca: z("zl")
});
F.Fg.Raa = 5;
F.Fg.qo = {DA: 0, lB: 1, UA: 2, KA: 3};
F.Oa = F.za.extend({
    TD: q,
    Wb: 0,
    Ji: q,
    Df: r,
    al: 0,
    T: q,
    Gc: p,
    sp: p,
    ctor: function (a, c, d) {
        this.TD = d;
        this.Wb = a || 0;
        this.Ji = c || ""
    },
    PE: z("Gc"),
    zca: A("Gc"),
    it: z("Df"),
    z_: A("Df"),
    xca: A("Wb"),
    uca: A("Ji"),
    KE: z("al"),
    tca: A("al"),
    jt: z("T"),
    vca: A("T"),
    Fl: function () {
        return this.TD != q
    },
    m: D(q),
    Wd: z("sp"),
    isEnabled: A("sp"),
    $z: u(),
    oj: u()
});
F.Oa.cY = 0;
F.Oa.jw = 1;
F.Oa.XB = 2;
F.Oa.uo = 3;
F.Oa.xo = 4;
F.Oa.jo = 5;
F.Oa.qr = 6;
F.fs = F.Oa.extend({
    Zs: q, ctor: function (a, c) {
        this.Zs = c;
        var d = this;
        F.Oa.prototype.ctor.call(this, F.Oa.qr, a, function (a) {
            d.Zs != q && d.Zs(a)
        })
    }, Fl: function () {
        return F.Oa.prototype.Fl.call(this) && this.Zs != q
    }, m: function () {
        return new F.fs(this.Ji, this.Zs)
    }
});
F.fs.create = function (a, c) {
    return new F.fs(a, c)
};
F.Qk = F.Oa.extend({
    Kz: q, Nz: q, Lz: q, Mz: q, ctor: function () {
        var a = this;
        F.Oa.prototype.ctor.call(this, F.Oa.xo, F.Qk.Nc, function (c) {
            var d = F.gd;
            switch (c.KM) {
                case d.LI:
                    a.Kz && a.Kz(c);
                    break;
                case d.mC:
                    a.Nz && a.Nz(c);
                    break;
                case d.eJ:
                    a.Lz && a.Lz(c);
                    break;
                case d.OB:
                    a.Mz && a.Mz(c)
            }
        })
    }, m: function () {
        var a = new F.Qk;
        a.Kz = this.Kz;
        a.Nz = this.Nz;
        a.Lz = this.Lz;
        a.Mz = this.Mz;
        return a
    }, Fl: D(p)
});
F.Qk.Nc = "__cc_mouse";
F.Qk.create = function () {
    return new F.Qk
};
F.yi = F.Oa.extend({
    So: q, Uu: r, Nq: q, su: q, ru: q, qu: q, ctor: function () {
        F.Oa.prototype.ctor.call(this, F.Oa.jw, F.yi.Nc, q);
        this.So = []
    }, Aoa: z("Uu"), m: function () {
        var a = new F.yi;
        a.Nq = this.Nq;
        a.su = this.su;
        a.ru = this.ru;
        a.qu = this.qu;
        a.Uu = this.Uu;
        return a
    }, Fl: function () {
        return !this.Nq ? (F.log(F.i.jY), r) : p
    }
});
F.yi.Nc = "__cc_touch_one_by_one";
F.yi.create = function () {
    return new F.yi
};
F.xi = F.Oa.extend({
    tu: q, wu: q, vu: q, uu: q, ctor: function () {
        F.Oa.prototype.ctor.call(this, F.Oa.XB, F.xi.Nc, q)
    }, m: function () {
        var a = new F.xi;
        a.tu = this.tu;
        a.wu = this.wu;
        a.vu = this.vu;
        a.uu = this.uu;
        return a
    }, Fl: function () {
        return this.tu == q && this.wu == q && this.vu == q && this.uu == q ? (F.log(F.i.iY), r) : p
    }
});
F.xi.Nc = "__cc_touch_all_at_once";
F.xi.create = function () {
    return new F.xi
};
F.Oa.create = function (a) {
    F.assert(a && a.event, F.i.vU);
    var c = a.event;
    delete a.event;
    var d = q;
    c === F.Oa.jw ? d = new F.yi : c === F.Oa.XB ? d = new F.xi : c === F.Oa.xo ? d = new F.Qk : c === F.Oa.qr ? (d = new F.fs(a.C3, a.gq), delete a.C3, delete a.gq) : c === F.Oa.uo ? d = new F.Pk : c === F.Oa.jo && (d = new F.Ok(a.gq), delete a.gq);
    for (var e in a)d[e] = a[e];
    return d
};
F.kY = F.za.extend({
    $f: q, fg: q, FQ: 0, ctor: function () {
        this.$f = [];
        this.fg = []
    }, size: function () {
        return this.$f.length + this.fg.length
    }, empty: function () {
        return 0 === this.$f.length && 0 === this.fg.length
    }, push: function (a) {
        0 == a.al ? this.fg.push(a) : this.$f.push(a)
    }, M2: function () {
        this.fg.length = 0
    }, L2: function () {
        this.$f.length = 0
    }, clear: function () {
        this.fg.length = 0;
        this.$f.length = 0
    }, oga: A("$f"), sha: A("fg")
});
F.ww = function (a) {
    var c = F.Mc, d = a.Wb;
    if (d === c.jo)return F.Ok.Nc;
    if (d === c.qr)return a.dD;
    if (d === c.uo)return F.Pk.Nc;
    if (d === c.xo)return F.Qk.Nc;
    d === c.iw && F.log(F.i.ww);
    return ""
};
F.La = {
    RA: 0,
    jv: 1,
    tr: 2,
    baa: 3,
    Tg: {},
    et: {},
    Li: {},
    Dx: {},
    cN: {},
    Op: [],
    zs: [],
    Km: 0,
    sp: r,
    IN: 0,
    y_: [F.Qb.WA, F.Qb.nv],
    Yx: function (a) {
        this.Li[a.ma] != q && this.zs.push(a);
        a = a.u;
        for (var c = 0, d = a.length; c < d; c++)this.Yx(a[c])
    },
    Qq: function (a, c) {
        var d = this.Li[a.ma], e, f;
        if (d) {
            e = 0;
            for (f = d.length; e < f; e++)d[e].PE(p)
        }
        if (c === p) {
            d = a.u;
            e = 0;
            for (f = d.length; e < f; e++)this.Qq(d[e], p)
        }
    },
    wk: function (a, c) {
        var d = this.Li[a.ma], e, f;
        if (d) {
            e = 0;
            for (f = d.length; e < f; e++)d[e].PE(r)
        }
        this.Yx(a);
        if (c === p) {
            d = a.u;
            e = 0;
            for (f = d.length; e < f; e++)this.wk(d[e],
                p)
        }
    },
    sY: function (a) {
        0 === this.Km ? this.QM(a) : this.Op.push(a)
    },
    QM: function (a) {
        var c = a.Ji, d = this.Tg[c];
        d || (d = new F.kY, this.Tg[c] = d);
        d.push(a);
        0 == a.al ? (this.rl(c, this.tr), c = a.T, c == q && F.log(F.i.v3), this.AY(c, a), c.Rh && this.wk(c)) : this.rl(c, this.jv)
    },
    pD: function (a) {
        return this.Tg[a]
    },
    G1: function () {
        if (0 != this.zs.length) {
            for (var a = this.zs, c, d, e = this.Li, f = 0, g = a.length; f < g; f++)if (c = e[a[f].ma])for (var h = 0, m = c.length; h < m; h++)(d = c[h]) && this.rl(d.Ji, this.tr);
            this.zs.length = 0
        }
    },
    dO: function (a) {
        if (a)for (var c, d = 0; d <
        a.length;)c = a[d], c.it(r), c.T != q && (this.$L(c.T, c), c.jt(q)), 0 === this.Km ? F.we(a, c) : ++d
    },
    an: function (a) {
        var c = this.Tg[a];
        if (c) {
            var d = c.$f;
            this.dO(c.fg);
            this.dO(d);
            delete this.et[a];
            this.Km || (c.clear(), delete this.Tg[a])
        }
        for (var d = this.Op, e, c = 0; c < d.length;)(e = d[c]) && e.Ji == a ? F.we(d, e) : ++c
    },
    WE: function (a) {
        var c = this.RA, d = this.et;
        d[a] && (c = d[a]);
        c != this.RA && (d[a] = this.RA, c & this.jv && this.q1(a), c & this.tr && ((c = F.L.tc) ? this.s1(a, c) : d[a] = this.tr))
    },
    s1: function (a, c) {
        var d = this.pD(a);
        if (d) {
            var e = d.fg;
            e && 0 !== e.length &&
            (this.IN = 0, this.Dx = {}, this.AF(c, p), d.fg.sort(this.p1))
        }
    },
    p1: function (a, c) {
        var d = F.La.Dx;
        return !a || !c || !a.T || !c.T ? -1 : d[c.T.ma] - d[a.T.ma]
    },
    q1: function (a) {
        if (a = this.Tg[a]) {
            var c = a.$f;
            if (c && 0 !== c.length) {
                c.sort(this.r1);
                for (var d = 0, e = c.length; d < e && !(0 <= c[d].al);)++d;
                a.FQ = d
            }
        }
    },
    r1: function (a, c) {
        return a.al - c.al
    },
    UD: function (a) {
        if (a = this.Tg[a]) {
            var c = a.$f, d = a.fg, e, f;
            if (d)for (e = 0; e < d.length;)f = d[e], f.Df ? ++e : F.we(d, f);
            if (c)for (e = 0; e < c.length;)f = c[e], f.Df ? ++e : F.we(c, f);
            d && 0 === d.length && a.M2();
            c && 0 === c.length &&
            a.L2()
        }
    },
    ty: function (a) {
        var c = this.Km;
        F.assert(0 < c, F.i.wU);
        a.Wb == F.Mc.iw ? (this.UD(F.yi.Nc), this.UD(F.xi.Nc)) : this.UD(F.ww(a));
        if (!(1 < c)) {
            F.assert(1 == c, F.i.xU);
            a = this.Tg;
            var c = this.et, d;
            for (d in a)a[d].empty() && (delete c[d], delete a[d]);
            d = this.Op;
            if (0 !== d.length) {
                a = 0;
                for (c = d.length; a < c; a++)this.QM(d[a]);
                this.Op.length = 0
            }
        }
    },
    Z_: function (a, c) {
        if (!a.z_)return r;
        var d = c.event, e = c.$R;
        d.JE(a.T);
        var f = r, g, h = d.$k, m = F.Fg.qo;
        if (h == m.DA)a.Nq && (f = a.Nq(e, d)) && a.Df && a.So.push(e); else if (0 < a.So.length && -1 != (g = a.So.indexOf(e)))f =
            p, h === m.lB && a.su ? a.su(e, d) : h === m.UA ? (a.ru && a.ru(e, d), a.Df && a.So.splice(g, 1)) : h === m.KA && (a.qu && a.qu(e, d), a.Df && a.So.splice(g, 1));
        return d.Mm ? (F.La.ty(d), p) : f && a.Df && a.Uu ? (c.K6 && c.touches.splice(e, 1), p) : r
    },
    kZ: function (a) {
        this.WE(F.yi.Nc);
        this.WE(F.xi.Nc);
        var c = this.pD(F.yi.Nc), d = this.pD(F.xi.Nc);
        if (!(q == c && q == d)) {
            var e = a.zl, f = F.vP(e), g = {event: a, K6: c && d, touches: f, $R: q};
            if (c)for (var h = 0; h < e.length; h++)if (g.$R = e[h], this.YC(c, this.Z_, g), a.Mm)return;
            if (d && 0 < f.length && (this.YC(d, this.a0, {event: a, touches: f}),
                    a.Mm))return;
            this.ty(a)
        }
    },
    a0: function (a, c) {
        if (!a.Df)return r;
        var d = F.Fg.qo, e = c.event, f = c.touches, g = e.$k;
        e.JE(a.T);
        g == d.DA && a.tu ? a.tu(f, e) : g == d.lB && a.wu ? a.wu(f, e) : g == d.UA && a.vu ? a.vu(f, e) : g == d.KA && a.uu && a.uu(f, e);
        return e.Mm ? (F.La.ty(e), p) : r
    },
    AY: function (a, c) {
        var d = this.Li[a.ma];
        d || (d = [], this.Li[a.ma] = d);
        d.push(c)
    },
    $L: function (a, c) {
        var d = this.Li[a.ma];
        d && (F.we(d, c), 0 === d.length && delete this.Li[a.ma])
    },
    YC: function (a, c, d) {
        var e = r, f = a.$f, g = a.fg, h = 0, m;
        if (f && 0 !== f.length)for (; h < a.FQ; ++h)if (m = f[h], m.isEnabled() && !m.Gc && m.Df && c(m, d)) {
            e = p;
            break
        }
        if (g && !e)for (a = 0; a < g.length; a++)if (m = g[a], m.isEnabled() && !m.Gc && m.Df && c(m, d)) {
            e = p;
            break
        }
        if (f && !e)for (; h < f.length && !(m = f[h], m.isEnabled() && !m.Gc && m.Df && c(m, d)); ++h);
    },
    rl: function (a, c) {
        var d = this.et;
        d[a] = d[a] == q ? c : c | d[a]
    },
    AF: function (a, c) {
        var d = a.u, e = 0, f = d.length, g = this.cN, h = this.Li;
        if (0 < f) {
            for (var m; e < f; e++)if ((m = d[e]) && 0 > m.Nb)this.AF(m, r); else break;
            h[a.ma] != q && (g[a.Gj] || (g[a.Gj] = []), g[a.Gj].push(a.ma));
            for (; e < f; e++)(m = d[e]) && this.AF(m, r)
        } else h[a.ma] != q && (g[a.Gj] ||
        (g[a.Gj] = []), g[a.Gj].push(a.ma));
        if (c) {
            var d = [], n;
            for (n in g)d.push(n);
            d.sort(this.t1);
            n = d.length;
            m = this.Dx;
            for (e = 0; e < n; e++) {
                f = g[d[e]];
                for (h = 0; h < f.length; h++)m[f[h]] = ++this.IN
            }
            this.cN = {}
        }
    },
    t1: function (a, c) {
        return a - c
    },
    addListener: function (a, c) {
        F.assert(a && c, F.i.x3);
        if (a instanceof F.Oa) {
            if (a.Df) {
                F.log(F.i.z3);
                return
            }
        } else F.assert(!F.du(c), F.i.y3), a = F.Oa.create(a);
        if (a.Fl()) {
            if (F.du(c)) {
                if (0 == c) {
                    F.log(F.i.w3);
                    return
                }
                a.jt(q);
                a.KE(c);
                a.it(p);
                a.PE(r)
            } else a.jt(c), a.KE(0), a.it(p);
            this.sY(a);
            return a
        }
    },
    HF: function (a, c) {
        var d = new F.fs(a, c);
        this.addListener(d, 1);
        return d
    },
    removeListener: function (a) {
        if (a != q) {
            var c, d = this.Tg, e;
            for (e in d) {
                var f = d[e], g = f.$f;
                (c = this.eO(f.fg, a)) ? this.rl(a.Ji, this.tr) : (c = this.eO(g, a)) && this.rl(a.Ji, this.jv);
                f.empty() && (delete this.et[a.Ji], delete d[e]);
                if (c)break
            }
            if (!c) {
                c = this.Op;
                d = 0;
                for (e = c.length; d < e; d++)if (f = c[d], f == a) {
                    F.we(c, f);
                    break
                }
            }
        }
    },
    eO: function (a, c) {
        if (a == q)return r;
        for (var d = 0, e = a.length; d < e; d++) {
            var f = a[d];
            if (f == c)return f.it(r), f.T != q && (this.$L(f.T, f), f.jt(q)),
            0 == this.Km && F.we(a, f), p
        }
        return r
    },
    OR: function (a, c) {
        if (a instanceof F.n) {
            delete this.Dx[a.ma];
            F.we(this.zs, a);
            var d = this.Li[a.ma];
            if (d) {
                for (var e = F.vP(d), d = 0; d < e.length; d++)this.removeListener(e[d]);
                e.length = 0
            }
            e = this.Op;
            for (d = 0; d < e.length;) {
                var f = e[d];
                f.T == a ? (f.jt(q), f.it(r), e.splice(d, 1)) : ++d
            }
            if (c === p) {
                e = a.u;
                d = 0;
                for (f = e.length; d < f; d++)this.OR(e[d], p)
            }
        } else a == F.Oa.jw ? this.an(F.yi.Nc) : a == F.Oa.XB ? this.an(F.xi.Nc) : a == F.Oa.xo ? this.an(F.Qk.Nc) : a == F.Oa.jo ? this.an(F.Ok.Nc) : a == F.Oa.uo ? this.an(F.Pk.Nc) : F.log(F.i.A3)
    },
    yma: function (a) {
        this.an(a)
    },
    tma: function () {
        var a = this.Tg, c = this.y_, d;
        for (d in a)-1 === c.indexOf(d) && this.an(d)
    },
    loa: function (a, c) {
        if (a != q) {
            var d = this.Tg, e;
            for (e in d) {
                var f = d[e].$f;
                if (f && -1 != f.indexOf(a)) {
                    a.T != q && F.log(F.i.B3);
                    a.al !== c && (a.KE(c), this.rl(a.Ji, this.jv));
                    break
                }
            }
        }
    },
    Wd: z("sp"),
    isEnabled: A("sp"),
    dispatchEvent: function (a) {
        if (this.sp) {
            this.G1();
            this.Km++;
            (!a || !a.LG) && b("event is undefined");
            if (a.Wb == F.Mc.iw)this.kZ(a); else {
                var c = F.ww(a);
                this.WE(c);
                c = this.Tg[c];
                c != q && this.YC(c, this.T_, a);
                this.ty(a)
            }
            this.Km--
        }
    },
    T_: function (a, c) {
        c.JE(a.T);
        a.TD(c);
        return c.Mm
    },
    rea: function (a, c) {
        var d = new F.mm(a);
        d.setUserData(c);
        this.dispatchEvent(d)
    }
};
F.Ah = u();
F.Ah.prototype = {
    constructor: F.Ah, apply: function (a) {
        a.addEventListener = F.Ah.prototype.addEventListener;
        a.QG = F.Ah.prototype.QG;
        a.removeEventListener = F.Ah.prototype.removeEventListener;
        a.dispatchEvent = F.Ah.prototype.dispatchEvent
    }, addEventListener: function (a, c, d) {
        this.kl === k && (this.kl = {});
        var e = this.kl;
        e[a] === k && (e[a] = []);
        this.QG(a, c, d) || e[a].push({gq: c, qG: d})
    }, QG: function (a, c, d) {
        if (this.kl === k)return r;
        var e = this.kl;
        if (e[a] !== k) {
            a = 0;
            for (var f = e.length; a < f; a++) {
                var g = e[a];
                if (g.gq == c && g.qG == d)return p
            }
        }
        return r
    },
    removeEventListener: function (a, c) {
        if (this.kl !== k) {
            var d = this.kl[a];
            if (d !== k)for (var e = 0; e < d.length;)d[e].qG == c ? d.splice(e, 1) : e++
        }
    }, dispatchEvent: function (a, c) {
        if (this.kl !== k) {
            c == q && (c = p);
            var d = this.kl[a];
            if (d !== k) {
                for (var e = [], f = d.length, g = 0; g < f; g++)e[g] = d[g];
                for (g = 0; g < f; g++)e[g].gq.call(e[g].qG, this);
                c && (d.length = 0)
            }
        }
    }
};
F.uU = F.Mc.extend({
    nL: q, ctor: function (a) {
        F.Mc.prototype.ctor.call(this, F.Mc.jo);
        this.nL = a
    }
});
F.RI = F.Mc.extend({
    GD: 0, sN: r, ctor: function (a, c) {
        F.Mc.prototype.ctor.call(this, F.Mc.uo);
        this.GD = a;
        this.sN = c
    }
});
F.Ok = F.Oa.extend({
    Gx: q, ctor: function (a) {
        this.Gx = a;
        var c = this;
        F.Oa.prototype.ctor.call(this, F.Oa.jo, F.Ok.Nc, function (a) {
            c.Gx(a.nL, a)
        })
    }, Fl: function () {
        F.assert(this.Gx, F.i.gY);
        return p
    }, m: function () {
        return new F.Ok(this.Gx)
    }
});
F.Ok.Nc = "__cc_acceleration";
F.Ok.create = function (a) {
    return new F.Ok(a)
};
F.Pk = F.Oa.extend({
    ou: q, pu: q, ctor: function () {
        var a = this;
        F.Oa.prototype.ctor.call(this, F.Oa.uo, F.Pk.Nc, function (c) {
            c.sN ? a.ou && a.ou(c.GD, c) : a.pu && a.pu(c.GD, c)
        })
    }, m: function () {
        var a = new F.Pk;
        a.ou = this.ou;
        a.pu = this.pu;
        return a
    }, Fl: function () {
        return this.ou == q && this.pu == q ? (F.log(F.i.hY), r) : p
    }
});
F.Pk.Nc = "__cc_keyboard";
F.Pk.create = function () {
    return new F.Pk
};
F.B === F.Aa && (F.E7 = {
    xe: p, bj: [], Sj: [], tx: r, qs: {}, ps: [], Dj: 0, cb: function (a) {
        var c = this.Sj, d, e = F.view.sa, f = F.view.Ya, g = a || F.l;
        a = 0;
        for (d = c.length; a < d; a++)c[a].cb(g, e, f)
    }, Ux: function (a, c) {
        a || F.log("The context of RenderTexture is invalid.");
        c = c || this.Dj;
        var d = this.qs[c], e, f;
        e = 0;
        for (f = d.length; e < f; e++)d[e].cb(a, 1, 1);
        d.length = 0;
        d = this.ps;
        delete this.qs[c];
        F.we(d, c);
        0 === d.length ? this.tx = r : this.Dj = d[d.length - 1]
    }, Rp: function (a) {
        this.tx = p;
        a = a || 0;
        this.qs[a] = [];
        this.ps.push(a);
        this.Dj = a
    }, B1: function () {
        this.tx =
            r
    }, TR: function () {
        this.xe = r;
        this.bj.length = 0
    }, transform: function () {
        var a = this.bj;
        a.sort(this.XE);
        for (var c = 0, d = a.length; c < d; c++)a[c].bn && a[c].ef();
        a.length = 0
    }, US: function () {
        return 0 < this.bj.length
    }, XE: function (a, c) {
        return a.Eh - c.Eh
    }, Bu: function (a) {
        this.bj.push(a)
    }, rP: function () {
        this.Sj.length = 0
    }, xc: function (a) {
        if (this.tx) {
            var c = this.qs[this.Dj];
            -1 === c.indexOf(a) && c.push(a)
        } else-1 === this.Sj.indexOf(a) && this.Sj.push(a)
    }
}, F.ka = F.E7, F.qK = function (a) {
    this.T = a;
    this.Yi = {
        Uq: 0, Vq: 0, x: 0, y: 0, width: 0, height: 0,
        gm: r
    }
}, F.qK.prototype.cb = function (a, c, d) {
    var e = this.T;
    a = a || F.l;
    var f = this.Yi;
    if (!e.G || !(0 === f.width || 0 === f.height))if (f.gm || 0 !== e.ab)if (!e.G || e.G.Jb) {
        var g = e.Ad, h = e.Ob.x, m = -e.Ob.y - e.Ba.height, n = e.Ba.width, s = e.Ba.height, t, v = "source" !== e.Mg;
        t = e.ab / 255;
        1 !== g.a || 0 !== g.b || 0 !== g.s || 1 !== g.z || e.pc || e.qc ? (a.save(), a.globalAlpha = t, a.transform(g.a, g.s, g.b, g.z, g.P * c, -g.Q * d), v && (a.globalCompositeOperation = e.Mg), e.pc && (h = -h - n, a.scale(-1, 1)), e.qc && (m = e.Ob.y, a.scale(1, -1)), e.G ? (t = e.G.Xa, e.ss ? a.drawImage(t, 0, 0, f.width,
            f.height, h * c, m * d, n * c, s * d) : a.drawImage(t, f.Uq, f.Vq, f.width, f.height, h * c, m * d, n * c, s * d)) : (n = e.S, f.gm && (e = e.wa, a.fillStyle = "rgba(" + e.r + "," + e.g + "," + e.b + ",1)", a.fillRect(h * c, m * d, n.width * c, n.height * d))), a.restore()) : (v && (a.save(), a.globalCompositeOperation = e.Mg), a.globalAlpha = t, e.G ? (t = e.G.Xa, e.ss ? a.drawImage(t, 0, 0, f.width, f.height, (g.P + h) * c, (-g.Q + m) * d, n * c, s * d) : a.drawImage(t, f.Uq, f.Vq, f.width, f.height, (g.P + h) * c, (-g.Q + m) * d, n * c, s * d)) : (n = e.S, f.gm && (e = e.wa, a.fillStyle = "rgba(" + e.r + "," + e.g + "," + e.b + ",1)", a.fillRect((g.P +
            h) * c, (-g.Q + m) * d, n.width * c, n.height * d))), v && a.restore());
        F.bd++
    }
}, F.OJ = z("T"), F.OJ.prototype.cb = function (a, c, d) {
    a = a || F.l;
    var e = this.T, f = e.Ad, g = e.wa, h = e.ab / 255, m = e.S.width, n = e.S.height;
    if (0 !== h) {
        var s = 1 !== f.a || 0 !== f.b || 0 !== f.s || 1 !== f.z, t = "source" !== e.Mg || s;
        t && (a.save(), a.globalCompositeOperation = e.Mg);
        a.globalAlpha = h;
        a.fillStyle = "rgba(" + (0 | g.r) + "," + (0 | g.g) + "," + (0 | g.b) + ", 1)";
        s ? (a.transform(f.a, f.s, f.b, f.z, f.P * c, -f.Q * d), a.fillRect(0, 0, m * c, -n * d)) : a.fillRect(f.P * c, -f.Q * d, m * c, -n * d);
        t && a.restore();
        F.bd++
    }
},
    F.YI = function (a) {
        this.T = a;
        this.Vi = F.d(0, 0);
        this.ax = F.d(0, 0);
        this.JM = this.KO = q
    }, F.YI.prototype.cb = function (a, c, d) {
    a = a || F.l;
    var e = this.T, f = e.ab / 255, g = e.Ad;
    if (0 !== f) {
        var h = 1 !== g.a || 0 !== g.b || 0 !== g.s || 1 !== g.z, m = "source" !== e.Mg || h;
        m && (a.save(), a.globalCompositeOperation = e.Mg);
        a.globalAlpha = f;
        var f = e.S.width, e = e.S.height, n = a.createLinearGradient(this.Vi.x, this.Vi.y, this.ax.x, this.ax.y);
        n.addColorStop(0, this.KO);
        n.addColorStop(1, this.JM);
        a.fillStyle = n;
        h ? (a.transform(g.a, g.s, g.b, g.z, g.P * c, -g.Q * d), a.fillRect(0,
            0, f * c, -e * d)) : a.fillRect(g.P * c, -g.Q * d, f * c, -e * d);
        m && a.restore();
        F.bd++
    }
}, F.EJ = z("T"), F.EJ.prototype.cb = function (a, c, d) {
    a = a || F.l;
    var e = this.T, f = e.Ad, g = e.Xm;
    a.save();
    a.transform(f.a, f.s, f.b, f.z, f.P * c, -f.Q * d);
    a.globalCompositeOperation = e.YQ() ? "lighter" : "source-over";
    var h;
    c = this.T.Ed;
    d = this.T.Pj;
    if (F.o.VJ == F.o.UB) {
        if (!e.G || !e.G.Jb) {
            a.restore();
            return
        }
        var m = e.G.Xa;
        if (!m.width || !m.height) {
            a.restore();
            return
        }
        for (var n = F.Ra, s = m, e = 0; e < c; e++)if (f = d[e], h = f.color.a / 255, 0 !== h) {
            a.globalAlpha = h;
            a.save();
            a.translate(0 |
                f.hk.x, -(0 | f.hk.y));
            var t = 4 * Math.floor(f.size / 4);
            h = g.width;
            var v = g.height;
            a.scale(Math.max(1 / h * t, 1E-6), Math.max(1 / v * t, 1E-6));
            f.rotation && a.rotate(F.kf(f.rotation));
            if (f.bH && (t = n.Vt(m)))t.Te || (t.Te = F.bc("canvas"), t.Te.width = m.width, t.Te.height = m.height), F.Il(m, t, f.color, this.Xm, t.Te), s = t.Te;
            a.drawImage(s, -(0 | h / 2), -(0 | v / 2));
            a.restore()
        }
    } else {
        m = F.Ge;
        for (e = 0; e < c; e++)f = d[e], g = 0 | 0.5 * f.size, h = f.color.a / 255, 0 !== h && (a.globalAlpha = h, a.save(), a.translate(0 | f.hk.x, -(0 | f.hk.y)), F.o.zI == F.o.WJ ? (f.rotation && a.rotate(F.kf(f.rotation)),
            m.PP(a, g, f.color)) : m.MP(a, g, f.color), a.restore())
    }
    a.restore();
    F.bd++
}, F.KJ = function (a) {
    this.iL = Math.PI / 180;
    this.T = a;
    this.Kb = q;
    this.Wb = F.Va.Jg;
    this.wL = F.rect(0, 0, 0, 0);
    this.XD = F.d(0, 0);
    this.Cf = 0;
    this.IM = this.HO = 270;
    this.HL = r
}, F.KJ.prototype.cb = function (a, c, d) {
    a = a || F.l;
    var e = this.T, f = this.Kb, g = f.Z.Yi, h = f.ab / 255;
    if (!(0 === g.width || 0 === g.height) && f.G && g.gm && 0 !== h) {
        e = e.Ad;
        a.save();
        a.transform(e.a, e.s, e.b, e.z, e.P * c, -e.Q * d);
        "source" != f.Mg && (a.globalCompositeOperation = f.Mg);
        a.globalAlpha = h;
        var m = f.Ba, n = f.Ob,
            h = f.bM, e = 0 | n.x, s = -n.y - m.height;
        h.width = m.width * c;
        h.height = m.height * d;
        f.pc && (e = -n.x - m.width, a.scale(-1, 1));
        f.qc && (s = n.y, a.scale(1, -1));
        e *= c;
        s *= d;
        this.Wb == F.Va.Jk ? (m = this.wL, a.beginPath(), a.rect(m.x * c, m.y * d, m.width * c, m.height * d), a.clip(), a.closePath()) : this.Wb == F.Va.Jg && (c *= this.XD.x, m = this.XD.y * d, a.beginPath(), a.arc(c, m, this.Cf * d, this.iL * this.HO, this.iL * this.IM, this.HL), a.lineTo(c, m), a.clip(), a.closePath());
        a.drawImage(f.G.Xa, g.Uq, g.Vq, g.width, g.height, e, s, h.width, h.height);
        a.restore();
        F.bd++
    }
}, F.uj =
    function (a) {
        this.T = a;
        this.q = this.Fe = this.Ga = q
    }, F.uj.prototype.cb = function (a, c, d) {
    var e = a || F.l, f = this.T, g = f.ab / 255;
    if (0 !== g) {
        e.globalAlpha = g;
        f = f.Ad;
        e.save();
        a.transform(f.a, f.s, f.b, f.z, f.P * c, -f.Q * d);
        this.q && (this.q.src == F.SRC_ALPHA && this.q.J == F.ONE) && (e.globalCompositeOperation = "lighter");
        a = this.Ga;
        f = 0;
        for (g = a.length; f < g; f++) {
            var h = a[f];
            switch (h.type) {
                case F.fd.YB:
                    this.Bs(e, h, c, d);
                    break;
                case F.fd.nK:
                    this.Ds(e, h, c, d);
                    break;
                case F.fd.Aj:
                    this.Cs(e, h, c, d)
            }
        }
        e.restore()
    }
}, F.uj.prototype.Bs = function (a, c,
                                 d, e) {
    var f = c.fillColor, g = c.Be[0];
    c = c.lineWidth;
    a.fillStyle = "rgba(" + (0 | f.r) + "," + (0 | f.g) + "," + (0 | f.b) + "," + f.a / 255 + ")";
    a.beginPath();
    a.arc(g.x * d, -g.y * e, c * d, 0, 2 * Math.PI, r);
    a.closePath();
    a.fill()
}, F.uj.prototype.Ds = function (a, c, d, e) {
    var f = c.li, g = c.Be[0], h = c.Be[1], m = c.lineWidth;
    c = c.lineCap;
    a.strokeStyle = "rgba(" + (0 | f.r) + "," + (0 | f.g) + "," + (0 | f.b) + "," + f.a / 255 + ")";
    a.lineWidth = m * d;
    a.beginPath();
    a.lineCap = c;
    a.moveTo(g.x * d, -g.y * e);
    a.lineTo(h.x * d, -h.y * e);
    a.stroke()
}, F.uj.prototype.Cs = function (a, c, d, e) {
    var f = c.Be,
        g = c.lineCap, h = c.fillColor, m = c.lineWidth, n = c.li, s = c.bu, t = c.cu;
    c = c.hj;
    if (f != q) {
        var v = f[0];
        a.lineCap = g;
        h && (a.fillStyle = "rgba(" + (0 | h.r) + "," + (0 | h.g) + "," + (0 | h.b) + "," + h.a / 255 + ")");
        m && (a.lineWidth = m * d);
        n && (a.strokeStyle = "rgba(" + (0 | n.r) + "," + (0 | n.g) + "," + (0 | n.b) + "," + n.a / 255 + ")");
        a.beginPath();
        a.moveTo(v.x * d, -v.y * e);
        g = 1;
        for (h = f.length; g < h; g++)a.lineTo(f[g].x * d, -f[g].y * e);
        s && a.closePath();
        t && a.fill();
        c && a.stroke()
    }
}, F.GI = z("T"), F.GI.prototype.cb = function (a, c, d) {
    var e = this.T;
    a = a || F.l;
    e.To ? (c = F.mc.aN(), d = a.canvas,
        c.width = d.width, c.height = d.height, c.getContext("2d").drawImage(d, 0, 0), a.save()) : (e.transform(), e = e.Ad, a.save(), a.save(), a.transform(e.a, e.s, e.b, e.z, e.P * c, -e.Q * d))
}, F.EI = z("T"), F.EI.prototype.cb = function (a, c, d) {
    var e = this.T;
    a = a || F.l;
    e.To ? (a.globalCompositeOperation = e.ii ? "destination-out" : "destination-in", e = e.Ad, a.transform(e.a, e.s, e.b, e.z, e.P * c, -e.Q * d)) : (a.restore(), e.ii && (c = a.canvas, a.save(), a.setTransform(1, 0, 0, 1, 0, 0), a.moveTo(0, 0), a.lineTo(0, c.height), a.lineTo(c.width, c.height), a.lineTo(c.width,
        0), a.lineTo(0, 0), a.restore()), a.clip())
}, F.FI = z("T"), F.FI.prototype.cb = function (a) {
    var c = this.T, d = F.mc.aN();
    a = a || F.l;
    c.To && (a.restore(), a.save(), a.setTransform(1, 0, 0, 1, 0, 0), a.globalCompositeOperation = "destination-over", a.drawImage(d, 0, 0));
    a.restore()
}, F.Qv = function (a) {
    this.T = a;
    this.Ga = a.Ga
}, F.Qv.prototype.cb = function (a, c, d) {
    var e = this.T;
    e.nt && (e.nt.j3(F.mU.bind(e)), e.nt.i3(F.jU.bind(e)), F.uj.prototype.cb.call(this, a, c, d), e.clear())
}, F.Qv.prototype.Bs = F.uj.prototype.Bs, F.Qv.prototype.Ds = F.uj.prototype.Ds,
    F.Qv.prototype.Cs = F.uj.prototype.Cs, F.fw = function (a) {
    this.T = a;
    this.DL = []
}, F.fw.prototype.bZ = function (a) {
    if (a)for (var c = this.DL, d = c.length = 0, e = a.length; d < e; d++)c[d] = a[d]
}, F.fw.prototype.x0 = function (a, c) {
    var d = this.T;
    if (d.pe) {
        var e = this.DL, f = d.oe, g = d.ae;
        f.save();
        f.clearRect(0, 0, g.width, -g.height);
        g = F.Zp(d.Ad);
        f.transform(g.a, g.s, g.b, g.z, g.P * a, -g.Q * c);
        for (var g = 0, h = e.length; g < h; g++)e[g].cb(f, a, c), e[g].T && (e[g].T.pe = r);
        f.restore();
        d.pe = r
    }
}, F.fw.prototype.cb = function (a, c, d) {
    var e = this.T, f = e.ab / 255;
    if (!(0 >=
        f)) {
        this.x0(c, d);
        a = a || F.l;
        a.globalAlpha = f;
        var f = 0 | -e.Ib.x, g = 0 | -e.Ib.y, h = e.ae, m = e.Ad;
        h && (0 !== h.width && 0 !== h.height) && (a.save(), a.transform(m.a, m.s, m.b, m.z, m.P * c, -m.Q * d), m = h.height * d, e.ij === F.Hk ? a.drawImage(h, 0, 0, h.width, h.height, f, -(g + m) + 0.5 * e.gc.height * d, h.width * c, m) : a.drawImage(h, 0, 0, h.width, h.height, f, -(g + m), h.width * c, m), a.restore());
        F.bd++
    }
}, F.HI = function (a, c) {
    this.T = a;
    this.Wc = c
}, F.HI.prototype.cb = function (a, c, d) {
    this.Wc && this.Wc.call(this.T, a, c, d)
}, F.BW = z("T"), F.BW.prototype.cb = function (a, c, d) {
    var e =
        this.T;
    a = a || F.l;
    if (e.VC || e.UC) {
        var f = e.Ad;
        a.save();
        a.transform(f.a, f.s, f.b, f.z, f.P * c, -f.Q * d);
        c = e.m1;
        var g, h, m = F.Ge;
        if (e.VC) {
            m.Nf(0, 0, 255, 255);
            m.co(1);
            var n = [];
            d = 0;
            for (f = c.GS.length; d < f; d++)h = c.NP[d], h.El && h.El.type == sp.pI.MJ && (g = h.El, sp.Kca(g, h, n), m.ye(n, 4, p))
        }
        if (e.UC) {
            m.co(2);
            m.Nf(255, 0, 0, 255);
            d = 0;
            for (f = c.zn.length; d < f; d++)e = c.zn[d], m.bi({x: e.ev, y: e.fv}, {
                x: e.data.length * e.E6 + e.ev,
                y: e.data.length * e.F6 + e.fv
            });
            m.TH(4);
            m.Nf(0, 0, 255, 255);
            d = 0;
            for (f = c.zn.length; d < f; d++)e = c.zn[d], m.kG({x: e.ev, y: e.fv}), 0 ===
            d && m.Nf(0, 255, 0, 255)
        }
        a.restore()
    }
});
F.B === F.Y && (F.F7 = {
    xe: p, bj: [], Sj: [], sx: r, Jw: {}, ps: [], Dj: 0, cb: function (a) {
        var c = this.Sj, d, e = a || F.l;
        a = 0;
        for (d = c.length; a < d; a++)c[a].cb(e)
    }, Rp: function (a) {
        this.sx = p;
        a = a || 0;
        this.Jw[a] = [];
        this.ps.push(a);
        this.Dj = a
    }, B1: function () {
        this.sx = r
    }, y0: function (a) {
        a = a || this.Dj;
        var c = this.Jw[a], d, e, f = F.l, g = this.ps;
        d = 0;
        for (e = c.length; d < e; d++)c[d].cb(f);
        c.length = 0;
        delete this.Jw[a];
        F.we(g, a);
        0 === g.length ? this.sx = r : this.Dj = g[g.length - 1]
    }, TR: function () {
        this.xe = r;
        this.bj.length = 0
    }, transform: function () {
        var a = this.bj;
        a.sort(this.XE);
        for (var c = 0, d = a.length; c < d; c++)a[c].bn && a[c].ef();
        a.length = 0
    }, US: function () {
        return 0 < this.bj.length
    }, XE: function (a, c) {
        return a.Eh - c.Eh
    }, Bu: function (a) {
        this.bj.push(a)
    }, rP: function () {
        this.Sj.length = 0
    }, xc: function (a) {
        if (this.sx) {
            var c = this.Jw[this.Dj];
            -1 === c.indexOf(a) && c.push(a)
        } else-1 === this.Sj.indexOf(a) && this.Sj.push(a)
    }
}, F.ka = F.F7, F.rK = z("T"), F.rK.prototype.cb = function (a) {
    var c = this.T;
    if (c.Ca && 0 !== c.ab) {
        a = a || F.l;
        var d = c.G;
        d ? d.Jb && (c.na.xb(), c.na.Th(c.Pb), F.pd(c.q.src, c.q.J), F.bz(0, d), F.Xb(F.Bh),
            a.bindBuffer(a.ARRAY_BUFFER, c.Cp), c.Od && (a.bufferData(a.ARRAY_BUFFER, c.Ub.cq, a.DYNAMIC_DRAW), c.Od = r), a.vertexAttribPointer(0, 3, a.FLOAT, r, 24, 0), a.vertexAttribPointer(1, 4, a.UNSIGNED_BYTE, p, 24, 12), a.vertexAttribPointer(2, 2, a.FLOAT, r, 24, 16), a.drawArrays(a.TRIANGLE_STRIP, 0, 4)) : (c.na.xb(), c.na.Th(c.Pb), F.pd(c.q.src, c.q.J), F.Cd(q), F.Xb(F.Jd | F.Do), a.bindBuffer(a.ARRAY_BUFFER, c.Cp), c.Od && (a.bufferData(a.ARRAY_BUFFER, c.Ub.cq, a.STATIC_DRAW), c.Od = r), a.vertexAttribPointer(F.kb, 3, a.FLOAT, r, 24, 0), a.vertexAttribPointer(F.sd,
            4, a.UNSIGNED_BYTE, p, 24, 12), a.drawArrays(a.TRIANGLE_STRIP, 0, 4));
        F.bd++
    }
}, F.LB = z("T"), F.LB.prototype.cb = function (a) {
    a = a || F.l;
    var c = this.T;
    c.na.xb();
    c.na.Th(c.Pb);
    F.Xb(F.Jd | F.Do);
    a.bindBuffer(a.ARRAY_BUFFER, c.zy);
    a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, 0, 0);
    a.bindBuffer(a.ARRAY_BUFFER, c.Nw);
    a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, 0, 0);
    F.pd(c.q.src, c.q.J);
    a.drawArrays(a.TRIANGLE_STRIP, 0, 4)
}, F.OI = z("T"), F.OI.prototype.cb = function () {
    var a = this.T;
    F.pd(a.q.src, a.q.J);
    a.na.xb();
    a.na.Th(a.Pb);
    a.gO()
},
    F.hJ = z("T"), F.hJ.prototype.cb = function (a) {
    var c = this.T;
    !(1 >= c.Mi) && (c.texture && c.texture.Jb) && (a = a || F.l, c.na.xb(), c.na.Th(c.Pb), F.Xb(F.Bh), F.pd(c.q.src, c.q.J), F.Cd(c.texture), a.bindBuffer(a.ARRAY_BUFFER, c.nd), a.bufferData(a.ARRAY_BUFFER, c.bb, a.DYNAMIC_DRAW), a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, 0, 0), a.bindBuffer(a.ARRAY_BUFFER, c.kn), a.bufferData(a.ARRAY_BUFFER, c.Ff, a.DYNAMIC_DRAW), a.vertexAttribPointer(F.hd, 2, a.FLOAT, r, 0, 0), a.bindBuffer(a.ARRAY_BUFFER, c.Fm), a.bufferData(a.ARRAY_BUFFER, c.Em, a.DYNAMIC_DRAW),
        a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, 0, 0), a.drawArrays(a.TRIANGLE_STRIP, 0, 2 * c.Mi), F.bd++)
}, F.LJ = z("T"), F.LJ.prototype.cb = function (a) {
    var c = this.T;
    a = a || F.l;
    if (c.Ic && c.Kb) {
        c.na.xb();
        c.na.Th(c.Pb);
        var d = c.Kb.Jf();
        F.pd(d.src, d.J);
        F.Xb(F.Bh);
        F.Cd(c.Kb.texture);
        a.bindBuffer(a.ARRAY_BUFFER, c.yy);
        c.rn && (a.bufferData(a.ARRAY_BUFFER, c.gf, a.DYNAMIC_DRAW), c.rn = r);
        d = F.Id.BYTES_PER_ELEMENT;
        a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, d, 0);
        a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, d, 8);
        a.vertexAttribPointer(F.hd,
            2, a.FLOAT, r, d, 12);
        c.Wb === F.Va.Jg ? a.drawArrays(a.TRIANGLE_FAN, 0, c.Qc) : c.Wb == F.Va.Jk && (c.Pd ? (a.drawArrays(a.TRIANGLE_STRIP, 0, c.Qc / 2), a.drawArrays(a.TRIANGLE_STRIP, 4, c.Qc / 2), F.bd++) : a.drawArrays(a.TRIANGLE_STRIP, 0, c.Qc));
        F.bd++
    }
}, F.FJ = z("T"), F.FJ.prototype.cb = function (a) {
    var c = this.T;
    c.G && (a = a || F.l, c.na.xb(), c.na.Th(c.Pb), F.Cd(c.G), F.cz(c.q.src, c.q.J), F.Xb(F.Bh), a.bindBuffer(a.ARRAY_BUFFER, c.oc[0]), a.vertexAttribPointer(F.kb, 3, a.FLOAT, r, 24, 0), a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, 24, 12), a.vertexAttribPointer(F.hd,
        2, a.FLOAT, r, 24, 16), a.bindBuffer(a.ELEMENT_ARRAY_BUFFER, c.oc[1]), a.drawElements(a.TRIANGLES, 6 * c.af, a.UNSIGNED_SHORT, 0))
}, F.wJ = z("T"), F.wJ.prototype.cb = function () {
    var a = this.T;
    0 != a.textureAtlas.totalQuads && (a.na.xb(), a.na.Th(a.Pb), F.cz(a.q.src, a.q.J), a.textureAtlas.Gl())
}, F.PJ = z("T"), F.PJ.prototype.cb = function (a) {
    var c = a || F.l;
    a = this.T;
    if (a.yn) {
        a.hh();
        var d = a.Jt;
        if (d) {
            var e = [0, 0, 0, 0], f = 0, g = 0;
            d & c.COLOR_BUFFER_BIT && (e = c.getParameter(c.COLOR_CLEAR_VALUE), c.clearColor(a.Vf.r / 255, a.Vf.g / 255, a.Vf.b / 255, a.Vf.a /
                255));
            d & c.DEPTH_BUFFER_BIT && (f = c.getParameter(c.DEPTH_CLEAR_VALUE), c.clearDepth(a.Hy));
            d & c.STENCIL_BUFFER_BIT && (g = c.getParameter(c.STENCIL_CLEAR_VALUE), c.clearStencil(a.Iy));
            c.clear(d);
            d & c.COLOR_BUFFER_BIT && c.clearColor(e[0], e[1], e[2], e[3]);
            d & c.DEPTH_BUFFER_BIT && c.clearDepth(f);
            d & c.STENCIL_BUFFER_BIT && c.clearStencil(g)
        }
        a.Tc();
        c = a.u;
        for (d = 0; d < c.length; d++)e = c[d], e != a.sprite && e.H();
        a.end()
    }
}, F.SB = z("T"), F.SB.prototype.cb = function () {
    var a = this.T;
    0 !== a.textureAtlas.totalQuads && (a.na.xb(), a.na.Th(a.Pb),
        a.Tf(a.u, F.n.vf.Ae), F.pd(a.q.src, a.q.J), a.textureAtlas.Gl())
}, F.uI = z("T"), F.uI.prototype.cb = function (a) {
    a = a || F.l;
    var c = this.T;
    c.na.xb();
    c.na.Th(c.Pb);
    F.pd(c.q.src, c.q.J);
    c.yt && c.Dm && (a.uniform4fv(c.yt, c.Dm), c.textureAtlas.jG(c.quadsToDraw))
}, F.mo = function (a, c) {
    this.T = a;
    this.Wc = c
}, F.mo.prototype.cb = function (a) {
    this.Wc && this.Wc.call(this.T, a)
}, F.jK = z("T"), F.jK.prototype.cb = F.SB.prototype.cb, F.UV = z("T"), F.UV.prototype.cb = function () {
    var a = this.T;
    a.nt && (a.nt.j3(F.mU.bind(a)), a.nt.i3(F.jU.bind(a)), F.fd.prototype.Ka.call(a),
        a.clear())
}, F.DW = z("T"), F.DW.prototype.cb = function () {
    var a = this.T;
    a.na.xb();
    a.na.Th(a.Pb);
    var c = a.mg(), d = a.m1;
    d.r = c.r / 255;
    d.g = c.g / 255;
    d.b = c.b / 255;
    d.a = a.jh() / 255;
    a.m0 && (d.r *= d.a, d.g *= d.a, d.b *= d.a);
    var e, f, g, h, m, n = new F.Hb, s = a.q, c = 0;
    for (m = d.GS.length; c < m; c++)if (h = d.NP[c], h.El && h.El.type == sp.pI.MJ) {
        g = h.El;
        var t = a.Yy(g);
        h.data.nda != e ? (f && (f.Gl(), f.Tq()), e = !e, F.pd(s.src, e ? F.ONE : s.J)) : t != f && f && (f.Gl(), f.Tq());
        f = t;
        t = f.Za;
        if (f.ud == t && (f.Gl(), f.Tq(), !f.Zz(2 * f.ud)))return;
        sp.q0(g, h, n, a.m0);
        f.bv(n, t)
    }
    f && (f.Gl(),
        f.Tq());
    if (a.UC || a.VC) {
        F.Se(F.Qf);
        F.tb.stack.push(F.tb.top);
        F.tb.top = a.Pb;
        e = F.Ge;
        if (a.VC) {
            e.Nf(0, 0, 255, 255);
            e.co(1);
            c = 0;
            for (m = d.GS.length; c < m; c++)h = d.NP[c], h.El && h.El.type == sp.pI.MJ && (g = h.El, n = new F.Hb, sp.q0(g, h, n), f = [], f.push(F.d(n.K.j.x, n.K.j.y)), f.push(F.d(n.V.j.x, n.V.j.y)), f.push(F.d(n.O.j.x, n.O.j.y)), f.push(F.d(n.U.j.x, n.U.j.y)), e.ye(f, 4, p))
        }
        if (a.UC) {
            e.co(2);
            e.Nf(255, 0, 0, 255);
            c = 0;
            for (m = d.zn.length; c < m; c++)a = d.zn[c], n = a.data.length * a.E6 + a.ev, f = a.data.length * a.F6 + a.fv, e.bi(F.d(a.ev, a.fv), F.d(n,
                f));
            e.TH(4);
            e.Nf(0, 0, 255, 255);
            c = 0;
            for (m = d.zn.length; c < m; c++)a = d.zn[c], e.kG(F.d(a.ev, a.fv)), 0 == c && e.Nf(0, 255, 0, 255)
        }
        F.ok()
    }
}, F.uT = z("T"), F.uT.prototype.cb = function (a) {
    var c = this.T;
    F.Se(F.Qf);
    F.Sn();
    F.c6(c.Pb);
    for (var d = c.u, e = F.dc.ALPHA_PREMULTIPLIED, f = F.dc.ALPHA_NON_PREMULTIPLIED, g = 0, h = d.length; g < h; g++) {
        var m = d[g];
        if (m && m.c4) {
            var n = m.c4();
            if (q != n)switch (n.Ou(c.na), m.bga()) {
                case ccs.daa:
                    if (n instanceof ccs.nba) {
                        n.Ae();
                        var s = m.Jf();
                        s.src != e.src || s.J != e.J ? n.Vd(m.Jf()) : c.q.src == e.src && c.q.J == e.J && !n.Ma().gi() ?
                            n.Vd(f) : n.Vd(c.q);
                        n.Ka(a)
                    }
                    break;
                case ccs.caa:
                    n.Ka(a);
                    break;
                default:
                    n.H(a)
            }
        } else m instanceof F.n && (m.Ou(c.na), m.H(a))
    }
    F.ok()
});
F.N.oC = function () {
    var a = F.n.prototype;
    a.qy = q;
    a.Pb = q;
    a.tD = q;
    a.Ng = q;
    a.ctor = function () {
        this.yD();
        var a = new F.ea;
        a.c[2] = a.c[3] = a.c[6] = a.c[7] = a.c[8] = a.c[9] = a.c[11] = a.c[14] = 0;
        a.c[10] = a.c[15] = 1;
        this.qy = a;
        this.tD = 0;
        this.Pb = new F.ea;
        this.de()
    };
    a.Qa = function () {
        this.dk === r && (this.Zg(), this.bn = this.dk = this.rx = p, F.ka.Bu(this))
    };
    a.H = function () {
        if (this.Jc) {
            this.Ja && (this.Eh = this.Ja.Eh + 1);
            var a, d = F.tb;
            d.stack.push(d.top);
            F.og(this.Pb, d.top);
            d.top = this.Pb;
            this.transform();
            var e = this.u;
            if (e && 0 < e.length) {
                var f = e.length;
                this.Tc();
                for (a = 0; a < f; a++)if (e[a] && 0 > e[a].Nb)e[a].H(); else break;
                for (this.Z && F.ka.xc(this.Z); a < f; a++)e[a] && e[a].H()
            } else this.Z && F.ka.xc(this.Z);
            d.top = d.stack.pop()
        }
    };
    a.ef = function (a) {
        var d = this.qy, e = this.Pb;
        a = a || (this.Ja ? this.Ja.Pb : F.tb.top);
        var f = this.Sl(), g = d.c;
        g[0] = f.a;
        g[4] = f.s;
        g[12] = f.P;
        g[1] = f.b;
        g[5] = f.z;
        g[13] = f.Q;
        g[14] = this.Xp;
        F.lf(e, a, d);
        this.Ng != q && !(this.grid != q && this.grid.mk()) && (d = this.Ib.x, a = this.Ib.y, 0 !== d || 0 !== a ? (F.$v || (d |= 0, a |= 0), f = new F.ea, F.uz(f, d, a, 0), F.lf(e, e, f), this.Ng.HN(e), F.uz(f,
            -d, -a, 0), F.lf(e, e, f)) : this.Ng.HN(e));
        this.bn = r;
        if (this.u && 0 !== this.u.length) {
            f = this.u;
            d = 0;
            for (a = f.length; d < a; d++)f[d].ef(e)
        }
    };
    a.transform = function () {
        var a = this.qy, d = F.tb.top, e = this.Sl(), f = a.c;
        f[0] = e.a;
        f[4] = e.s;
        f[12] = e.P;
        f[1] = e.b;
        f[5] = e.z;
        f[13] = e.Q;
        f[14] = this.Xp;
        F.lf(d, d, a);
        this.Ng != q && !(this.grid != q && this.grid.mk()) && (a = this.Ib.x, d = this.Ib.y, 0 !== a || 0 !== d ? (F.$v || (a |= 0, d |= 0), F.Iq(a, d), this.Ng.mu(), F.Iq(-a, -d)) : this.Ng.mu())
    };
    a.dj = a.TZ
};
F.N.BB = function () {
    var a = F.n.prototype;
    F.k(a, "x", a.$b, a.qS);
    F.k(a, "y", a.va, a.am);
    F.k(a, "width", a.Gh, a.Xj);
    F.k(a, "height", a.dl, a.Wj);
    F.k(a, "anchorX", a.BZ, a.HE);
    F.k(a, "anchorY", a.CZ, a.IE);
    F.k(a, "skewX", a.T4, a.I8);
    F.k(a, "skewY", a.U4, a.J8);
    F.k(a, "zIndex", a.r4, a.mS);
    F.k(a, "vertexZ", a.e5, a.V8);
    F.k(a, "rotation", a.M4, a.VH);
    F.k(a, "rotationX", a.O4, a.G8);
    F.k(a, "rotationY", a.P4, a.H8);
    F.k(a, "scale", a.Q4, a.$q);
    F.k(a, "scaleX", a.sQ, a.Mu);
    F.k(a, "scaleY", a.tQ, a.Nu);
    F.k(a, "children", a.T3);
    F.k(a, "childrenCount", a.U3);
    F.k(a, "parent", a.getParent, a.pS);
    F.k(a, "visible", a.P5, a.ke);
    F.k(a, "running", a.N5);
    F.k(a, "ignoreAnchor", a.K5, a.hz);
    F.k(a, "actionManager", a.Py, a.aS);
    F.k(a, "scheduler", a.Jl, a.yS);
    F.k(a, "shaderProgram", a.HG, a.Ou);
    F.k(a, "opacity", a.jh, a.wb);
    F.k(a, "opacityModifyRGB", a.ng);
    F.k(a, "cascadeOpacity", a.$Q, a.fS);
    F.k(a, "color", a.mg, a.rb);
    F.k(a, "cascadeColor", a.ZQ, a.eS)
};
F.lJ = -1;
F.IH = 1;
F.n = F.za.extend({
    Nb: 0,
    Gj: 0,
    Xp: 0,
    Uj: 0,
    dn: 0,
    sa: 1,
    Ya: 1,
    xa: q,
    Tm: q,
    xy: r,
    RD: r,
    $g: 0,
    ah: 0,
    u: q,
    Jc: p,
    jd: q,
    Ib: q,
    S: q,
    Rh: r,
    Ja: q,
    Jj: r,
    tag: F.lJ,
    userData: q,
    userObject: q,
    dk: p,
    rx: p,
    pe: r,
    Uk: q,
    $ca: q,
    ck: q,
    Ad: q,
    nN: q,
    ue: r,
    na: q,
    arrivalOrder: 0,
    Lg: q,
    gg: q,
    qca: q,
    lN: r,
    is: r,
    Cw: q,
    Og: q,
    ED: r,
    Ep: 0,
    Wx: 0,
    Mb: "Node",
    DO: r,
    zp: "",
    ab: 255,
    Xg: 255,
    wa: q,
    te: q,
    wf: r,
    Uf: r,
    yca: 0,
    Eh: -1,
    Z: q,
    Mca: r,
    yD: function () {
        this.jd = F.d(0, 0);
        this.Ib = F.d(0, 0);
        this.S = F.size(0, 0);
        this.xa = F.d(0, 0);
        this.Tm = F.d(0, 0);
        this.u = [];
        this.ck = {a: 1, b: 0, s: 0, z: 1, P: 0, Q: 0};
        this.Ad = {
            a: 1,
            b: 0, s: 0, z: 1, P: 0, Q: 0
        };
        var a = F.L;
        this.Lg = a.Py();
        this.gg = a.Jl();
        this.lN = p;
        this.Cw = {a: 1, b: 0, s: 0, z: 1, P: 0, Q: 0};
        F.UT && (this.Og = new F.UT(this));
        this.Xg = this.ab = 255;
        this.wa = F.color(255, 255, 255, 255);
        this.te = F.color(255, 255, 255, 255);
        this.Uf = this.wf = r
    },
    oa: function () {
        this.lN === r && this.yD();
        return p
    },
    Tf: function (a, c) {
        if (a && 0 !== a.length) {
            var d, e = a.length, f;
            d = F.n.vf;
            switch (c) {
                case d.ba:
                    for (d = 0; d < e; d++)(f = a[d]) && f.ba();
                    break;
                case d.Cb:
                    for (d = 0; d < e; d++)(f = a[d]) && f.Cb();
                    break;
                case d.mi:
                    for (d = 0; d < e; d++)(f = a[d]) && f.mi();
                    break;
                case d.jf:
                    for (d = 0; d < e; d++)(f = a[d]) && f.jf();
                    break;
                case d.Ae:
                    for (d = 0; d < e; d++)(f = a[d]) && f.Ae();
                    break;
                case d.pg:
                    for (d = 0; d < e; d++)(f = a[d]) && f.pg();
                    break;
                case d.Tc:
                    for (d = 0; d < e; d++)(f = a[d]) && f.Tc();
                    break;
                default:
                    F.assert(0, F.i.oV)
            }
        }
    },
    Qa: q,
    ja: function (a) {
        for (var c in a)this[c] = a[c]
    },
    T4: A("$g"),
    I8: function (a) {
        this.$g = a;
        this.Qa()
    },
    U4: A("ah"),
    J8: function (a) {
        this.ah = a;
        this.Qa()
    },
    mS: function (a) {
        this.Nb = a;
        this.Ja && this.Ja.Wq(this, a);
        F.La.Yx(this)
    },
    Zx: z("Nb"),
    r4: A("Nb"),
    fia: function () {
        F.log(F.i.uV);
        return this.Nb
    },
    epa: function (a) {
        F.log(F.i.EV);
        this.mS(a)
    },
    Hna: function (a) {
        this.Gj != a && (this.Gj = a, F.La.Yx(this))
    },
    vga: A("Gj"),
    e5: A("Xp"),
    V8: z("Xp"),
    M4: function () {
        this.Uj !== this.dn && F.log(F.i.sV);
        return this.Uj
    },
    VH: function (a) {
        this.Uj = this.dn = a;
        this.Ep = 0.017453292519943295 * this.Uj;
        this.Wx = 0.017453292519943295 * this.dn;
        this.Qa()
    },
    O4: A("Uj"),
    G8: function (a) {
        this.Uj = a;
        this.Ep = 0.017453292519943295 * this.Uj;
        this.Qa()
    },
    P4: A("dn"),
    H8: function (a) {
        this.dn = a;
        this.Wx = 0.017453292519943295 * this.dn;
        this.Qa()
    },
    Q4: function () {
        this.sa !==
        this.Ya && F.log(F.i.tV);
        return this.sa
    },
    $q: function (a, c) {
        this.sa = a;
        this.Ya = c || 0 === c ? c : a;
        this.Qa()
    },
    sQ: A("sa"),
    Mu: function (a) {
        this.sa = a;
        this.Qa()
    },
    tQ: A("Ya"),
    Nu: function (a) {
        this.Ya = a;
        this.Qa()
    },
    X: function (a, c) {
        var d = this.xa;
        c === k ? (d.x = a.x, d.y = a.y) : (d.x = a, d.y = c);
        this.Qa();
        this.xy = r
    },
    Yna: function (a, c) {
        var d = this.Tm;
        c === k ? (d.x = a.x, d.y = a.y) : (d.x = a, d.y = c);
        this.Qa();
        this.RD = this.xy = p
    },
    Uy: function () {
        return F.d(this.xa)
    },
    Sga: function () {
        return F.d(this.Tm)
    },
    $b: function () {
        return this.xa.x
    },
    qS: function (a) {
        this.xa.x =
            a;
        this.Qa()
    },
    va: function () {
        return this.xa.y
    },
    am: function (a) {
        this.xa.y = a;
        this.Qa()
    },
    U3: function () {
        return this.u.length
    },
    T3: A("u"),
    P5: A("Jc"),
    ke: function (a) {
        this.Jc != a && ((this.Jc = a) && this.Qa(), F.ka.xe = p)
    },
    Afa: function () {
        return F.d(this.jd)
    },
    Zl: function (a, c) {
        var d = this.jd;
        if (c === k) {
            if (a.x === d.x && a.y === d.y)return;
            d.x = a.x;
            d.y = a.y
        } else {
            if (a === d.x && c === d.y)return;
            d.x = a;
            d.y = c
        }
        var e = this.Ib, f = this.S;
        e.x = f.width * d.x;
        e.y = f.height * d.y;
        this.Qa()
    },
    sca: A("jd"),
    sO: function (a) {
        var c = a.x;
        a = a.y;
        this.jd.x !== c && (this.jd.x =
            c, this.Ib.x = this.S.width * c);
        this.jd.y !== a && (this.jd.y = a, this.Ib.y = this.S.height * a);
        this.Qa()
    },
    BZ: function () {
        return this.jd.x
    },
    HE: function (a) {
        this.jd.x !== a && (this.jd.x = a, this.Ib.x = this.S.width * a, this.Qa())
    },
    CZ: function () {
        return this.jd.y
    },
    IE: function (a) {
        this.jd.y !== a && (this.jd.y = a, this.Ib.y = this.S.height * a, this.Qa())
    },
    Qy: function () {
        return F.d(this.Ib)
    },
    Gh: function () {
        return this.S.width
    },
    Xj: function (a) {
        this.S.width = a;
        this.Ib.x = a * this.jd.x;
        this.Qa()
    },
    dl: function () {
        return this.S.height
    },
    Wj: function (a) {
        this.S.height =
            a;
        this.Ib.y = a * this.jd.y;
        this.Qa()
    },
    $: function () {
        return F.size(this.S)
    },
    ze: function (a, c) {
        var d = this.S;
        if (c === k) {
            if (a.width === d.width && a.height === d.height)return;
            d.width = a.width;
            d.height = a.height
        } else {
            if (a === d.width && c === d.height)return;
            d.width = a;
            d.height = c
        }
        var e = this.Ib, f = this.jd;
        e.x = d.width * f.x;
        e.y = d.height * f.y;
        this.Qa()
    },
    N5: A("Rh"),
    getParent: A("Ja"),
    pS: z("Ja"),
    K5: A("Jj"),
    hz: function (a) {
        a != this.Jj && (this.Jj = a, this.Qa())
    },
    JG: A("tag"),
    rA: z("tag"),
    t8: z("zp"),
    getName: A("zp"),
    getUserData: A("userData"),
    setUserData: z("userData"),
    aia: A("userObject"),
    bpa: function (a) {
        this.userObject != a && (this.userObject = a)
    },
    Xga: A("arrivalOrder"),
    w8: z("arrivalOrder"),
    Py: function () {
        this.Lg || (this.Lg = F.L.Py());
        return this.Lg
    },
    aS: function (a) {
        this.Lg != a && (this.eI(), this.Lg = a)
    },
    Jl: function () {
        this.gg || (this.gg = F.L.Jl());
        return this.gg
    },
    yS: function (a) {
        this.gg != a && (this.zA(), this.gg = a)
    },
    Ida: function () {
        F.log(F.i.qV);
        return this.Q3()
    },
    Q3: function () {
        var a = F.rect(0, 0, this.S.width, this.S.height);
        return F.p0(a, this.dj())
    },
    jf: function () {
        this.eI();
        this.zA();
        F.La.OR(this);
        this.Tf(this.u, F.n.vf.jf)
    },
    di: function (a) {
        var c = this.u;
        if (c != q)for (var d = 0; d < c.length; d++) {
            var e = c[d];
            if (e && e.tag == a)return e
        }
        return q
    },
    Mfa: function (a) {
        if (!a)return F.log("Invalid name"), q;
        for (var c = this.u, d = 0, e = c.length; d < e; d++)if (c[d].zp == a)return c[d];
        return q
    },
    F: function (a, c, d) {
        c = c === k ? a.Nb : c;
        var e, f = r;
        F.nk(d) ? (d = k, e = a.zp) : F.Dd(d) ? (e = d, d = k) : F.du(d) && (f = p, e = "");
        F.assert(a, F.i.pV);
        F.assert(a.Ja === q, "child already added. It can't be added again");
        this.BC(a, c, d, e, f)
    },
    BC: function (a,
                  c, d, e, f) {
        this.u || (this.u = []);
        this.u_(a, c);
        f ? a.rA(d) : a.t8(e);
        a.pS(this);
        a.w8(F.IH++);
        this.Rh && (a.ba(), this.ED && a.mi());
        this.wf && this.FM();
        this.Uf && this.GM()
    },
    ni: function (a) {
        this.Ja && (a == q && (a = p), this.Ja.removeChild(this, a))
    },
    zma: function (a) {
        F.log(F.i.yV);
        this.ni(a)
    },
    removeChild: function (a, c) {
        0 !== this.u.length && (c == q && (c = p), -1 < this.u.indexOf(a) && this.gZ(a, c), this.Qa(), F.ka.xe = p)
    },
    DH: function (a, c) {
        a === F.lJ && F.log(F.i.wV);
        var d = this.di(a);
        d == q ? F.log(F.i.xV, a) : this.removeChild(d, c)
    },
    sma: function (a) {
        this.Mf(a)
    },
    Mf: function (a) {
        var c = this.u;
        if (c != q) {
            a == q && (a = p);
            for (var d = 0; d < c.length; d++) {
                var e = c[d];
                e && (this.Rh && (e.pg(), e.Cb()), a && e.jf(), e.parent = q)
            }
            this.u.length = 0
        }
    },
    gZ: function (a, c) {
        this.Rh && (a.pg(), a.Cb());
        c && a.jf();
        a.parent = q;
        a.Uk = q;
        F.we(this.u, a)
    },
    u_: function (a, c) {
        F.ka.xe = this.ue = p;
        this.u.push(a);
        a.Zx(c)
    },
    Wq: function (a, c) {
        F.assert(a, F.i.zV);
        F.ka.xe = this.ue = p;
        a.arrivalOrder = F.IH;
        F.IH++;
        a.Zx(c);
        this.Qa()
    },
    Tc: function () {
        if (this.ue) {
            var a = this.u, c = a.length, d, e, f;
            for (d = 1; d < c; d++) {
                f = a[d];
                for (e = d - 1; 0 <= e;) {
                    if (f.Nb <
                        a[e].Nb)a[e + 1] = a[e]; else if (f.Nb === a[e].Nb && f.arrivalOrder < a[e].arrivalOrder)a[e + 1] = a[e]; else break;
                    e--
                }
                a[e + 1] = f
            }
            this.ue = r
        }
    },
    Ka: u(),
    N9: function () {
        this.Ja != q && (this.Ja.N9(), this.Ja.transform())
    },
    ba: function () {
        this.ED = r;
        this.Rh = p;
        this.Tf(this.u, F.n.vf.ba);
        this.Xq()
    },
    mi: function () {
        this.ED = p;
        this.Tf(this.u, F.n.vf.mi)
    },
    pg: function () {
        this.Tf(this.u, F.n.vf.pg)
    },
    Cb: function () {
        this.Rh = r;
        this.pause();
        this.Tf(this.u, F.n.vf.Cb);
        this.v7()
    },
    Da: function (a) {
        F.assert(a, F.i.BV);
        this.actionManager.U1(a, this, !this.Rh);
        return a
    },
    eI: function () {
        this.actionManager && this.actionManager.NR(this)
    },
    t9: function (a) {
        this.actionManager.MR(a)
    },
    u9: function (a) {
        a === F.ko ? F.log(F.i.FV) : this.actionManager.u7(a, this)
    },
    uG: function (a) {
        return a === F.ko ? (F.log(F.i.rV), q) : this.actionManager.uG(a, this)
    },
    Uga: function () {
        return this.actionManager.M6(this)
    },
    Yq: function () {
        this.ZR(0)
    },
    ZR: function (a) {
        this.scheduler.JH(this, a, !this.Rh)
    },
    lr: function () {
        this.scheduler.$u(this)
    },
    eA: function (a, c, d, e) {
        c = c || 0;
        F.assert(a, F.i.CV);
        F.assert(0 <= c, F.i.DV);
        d =
            d == q ? F.Qr : d;
        this.scheduler.T7(this, a, c, d, e || 0, !this.Rh)
    },
    Rma: function (a, c) {
        this.eA(a, 0, 0, c)
    },
    Zu: function (a) {
        a && this.scheduler.aT(this, a)
    },
    zA: function () {
        this.scheduler.$S(this)
    },
    Ima: function () {
        F.log(F.i.AV);
        this.Xq()
    },
    Xq: function () {
        this.scheduler.wk(this);
        this.actionManager && this.actionManager.wk(this);
        F.La.wk(this)
    },
    Rla: function () {
        F.log(F.i.vV);
        this.pause()
    },
    pause: function () {
        this.scheduler.Qq(this);
        this.actionManager && this.actionManager.Qq(this);
        F.La.Qq(this)
    },
    $ma: function (a) {
        this.Cw = a;
        this.is = this.dk =
            p
    },
    F4: function () {
        this.rx && (this.nN = F.Zp(this.dj()), this.rx = r);
        return this.nN
    },
    Ola: function () {
        return this.F4()
    },
    Ut: function () {
        for (var a = this.dj(), c = this.Ja; c != q; c = c.parent)a = F.wn(a, c.dj());
        return a
    },
    sR: function () {
        return this.Ut()
    },
    EQ: function () {
        return F.Zp(this.Ut())
    },
    Wpa: function () {
        return this.EQ()
    },
    $F: function (a) {
        return F.GR(a, this.EQ())
    },
    Jy: function (a) {
        a = a || F.d(0, 0);
        return F.GR(a, this.Ut())
    },
    R2: function (a) {
        return F.ie(this.$F(a), this.Ib)
    },
    Yda: function (a) {
        a = a || F.d(0, 0);
        a = F.sk(a, this.Ib);
        return this.Jy(a)
    },
    jca: function (a) {
        a = this.Jy(a);
        return F.L.uP(a)
    },
    Zda: function (a) {
        return this.$F(a.Tt())
    },
    $da: function (a) {
        a = a.Tt();
        a = F.L.tP(a);
        return this.R2(a)
    },
    update: function (a) {
        this.Og && !this.Og.Dia() && this.Og.H(a)
    },
    Ae: function () {
        this.Tf(this.u, F.n.vf.Ae)
    },
    $z: u(),
    oj: u(),
    W3: function (a) {
        return this.Og ? this.Og.W3(a) : q
    },
    dda: function (a) {
        this.Og && this.Og.add(a)
    },
    xma: function (a) {
        return this.Og ? this.Og.remove(a) : r
    },
    v7: function () {
        this.Og && this.Og.qma()
    },
    grid: q,
    ctor: q,
    H: q,
    transform: q,
    Sl: function () {
        return this.dj()
    },
    dj: q,
    Zg: function () {
        if (this.pe === r) {
            this.pe = p;
            var a = this.Uk;
            a && a != this && a.Zg()
        }
    },
    ht: function (a) {
        if (this.Uk != a) {
            this.Uk = a;
            for (var c = this.u, d = 0, e = c.length; d < e; d++)c[d].ht(a)
        }
    },
    On: function () {
        this.Ng || (this.Ng = new F.rr);
        return this.Ng
    },
    St: A("grid"),
    m8: z("grid"),
    HG: A("na"),
    Ou: z("na"),
    uga: A("tD"),
    Gna: z("tD"),
    R3: function () {
        var a = F.rect(0, 0, this.S.width, this.S.height), c = this.Ut(), a = F.BH(a, this.Ut());
        if (!this.u)return a;
        for (var d = this.u, e = 0; e < d.length; e++) {
            var f = d[e];
            f && f.Jc && (f = f.Js(c)) && (a = F.Vz(a, f))
        }
        return a
    },
    Js: function (a) {
        var c = F.rect(0, 0, this.S.width, this.S.height);
        a = a == q ? this.dj() : F.wn(this.dj(), a);
        c = F.BH(c, a);
        if (!this.u)return c;
        for (var d = this.u, e = 0; e < d.length; e++) {
            var f = d[e];
            f && f.Jc && (f = f.Js(a)) && (c = F.Vz(c, f))
        }
        return c
    },
    TZ: function () {
        if (this.xy && this.Ja) {
            var a = this.Ja.S;
            this.xa.x = this.Tm.x * a.width;
            this.xa.y = this.Tm.y * a.height;
            this.RD = r
        }
        if (this.dk) {
            var a = this.xa.x, c = this.xa.y, d = this.Ib.x, e = -d, f = this.Ib.y, g = -f, h = this.sa, m = this.Ya;
            this.Jj && (a += d, c += f);
            var n = 1, s = 0, t = 1, v = 0;
            if (0 !== this.Uj || 0 !== this.dn)n =
                Math.cos(-this.Ep), s = Math.sin(-this.Ep), t = Math.cos(-this.Wx), v = Math.sin(-this.Wx);
            var w = this.$g || this.ah;
            if (!w && (0 !== d || 0 !== f))a += t * e * h + -s * g * m, c += v * e * h + n * g * m;
            var x = this.ck;
            x.a = t * h;
            x.b = v * h;
            x.s = -s * m;
            x.z = n * m;
            x.P = a;
            x.Q = c;
            if (w && (x = F.wn({
                    a: 1,
                    b: Math.tan(F.kf(this.ah)),
                    s: Math.tan(F.kf(this.$g)),
                    z: 1,
                    P: 0,
                    Q: 0
                }, x), 0 !== d || 0 !== f))x = F.a2(x, e, g);
            this.is && (x = F.wn(x, this.Cw), this.is = r);
            this.ck = x;
            this.dk = r
        }
        return this.ck
    },
    vc: u(),
    jh: A("Xg"),
    d4: A("ab"),
    wb: function (a) {
        this.ab = this.Xg = a;
        var c = 255, d = this.Ja;
        d && d.cascadeOpacity &&
        (c = d.ab);
        this.Uc(c);
        this.wa.a = this.te.a = a
    },
    Uc: function (a) {
        this.ab = this.Xg * a / 255;
        this.Z && this.Z.Je !== k && (this.Z.Je = this.ab / 255);
        if (this.Uf) {
            a = this.u;
            for (var c = 0; c < a.length; c++) {
                var d = a[c];
                d && d.Uc(this.ab)
            }
        }
    },
    $Q: A("Uf"),
    fS: function (a) {
        this.Uf !== a && ((this.Uf = a) ? this.GM() : this.jZ())
    },
    GM: function () {
        var a = 255, c = this.Ja;
        c && c.cascadeOpacity && (a = c.ab);
        this.Uc(a)
    },
    jZ: function () {
        this.ab = this.Xg;
        for (var a = this.u, c = 0; c < a.length; c++) {
            var d = a[c];
            d && d.Uc(255)
        }
    },
    mg: function () {
        var a = this.te;
        return F.color(a.r, a.g,
            a.b, a.a)
    },
    Sy: function () {
        var a = this.wa;
        return F.color(a.r, a.g, a.b, a.a)
    },
    rb: function (a) {
        var c = this.wa, d = this.te;
        c.r = d.r = a.r;
        c.g = d.g = a.g;
        c.b = d.b = a.b;
        a = (a = this.Ja) && a.cascadeColor ? a.Sy() : F.color.WHITE;
        this.ed(a)
    },
    ed: function (a) {
        var c = this.wa, d = this.te;
        c.r = 0 | d.r * a.r / 255;
        c.g = 0 | d.g * a.g / 255;
        c.b = 0 | d.b * a.b / 255;
        if (this.wf) {
            a = this.u;
            for (d = 0; d < a.length; d++) {
                var e = a[d];
                e && e.ed(c)
            }
        }
    },
    ZQ: A("wf"),
    eS: function (a) {
        this.wf !== a && ((this.wf = a) ? this.FM() : this.iZ())
    },
    FM: function () {
        var a;
        a = (a = this.Ja) && a.cascadeColor ? a.Sy() :
            F.color.WHITE;
        this.ed(a)
    },
    iZ: function () {
        var a = this.wa, c = this.te;
        a.r = c.r;
        a.g = c.g;
        a.b = c.b;
        for (var a = this.u, c = F.color.WHITE, d = 0; d < a.length; d++) {
            var e = a[d];
            e && e.ed(c)
        }
    },
    Of: u(),
    ng: D(r),
    de: u(),
    ef: q
});
F.n.create = function () {
    return new F.n
};
F.n.vf = {ba: 1, Cb: 2, jf: 3, mi: 4, Ae: 5, pg: 6, Tc: 7};
F.B === F.Aa ? (M = F.n.prototype, M.ctor = function () {
    this.yD();
    this.de()
}, M.Qa = function () {
    this.dk === r && (this.Zg(), this.bn = this.dk = this.rx = p, F.ka.Bu(this))
}, M.H = function () {
    if (this.Jc) {
        this.Ja && (this.Eh = this.Ja.Eh + 1);
        var a, c = this.u, d;
        this.transform();
        var e = c.length;
        if (0 < e) {
            this.Tc();
            for (a = 0; a < e; a++)if (d = c[a], 0 > d.Nb)d.H(); else break;
            for (this.Z && F.ka.xc(this.Z); a < e; a++)c[a].H()
        } else this.Z && F.ka.xc(this.Z);
        this.pe = r
    }
}, M.ef = function () {
    var a = this.Sl(), c = this.Ad;
    if (this.Ja) {
        var d = this.Ja.Ad;
        c.a = a.a * d.a + a.b * d.s;
        c.b = a.a * d.b + a.b * d.z;
        c.s = a.s * d.a + a.z * d.s;
        c.z = a.s * d.b + a.z * d.z;
        if (!this.$g || this.ah) {
            var e = this.Ja.ck, f = -(e.b + e.s) * a.P;
            c.P = a.P * d.a + a.Q * d.s + d.P + -(e.b + e.s) * a.Q;
            c.Q = a.P * d.b + a.Q * d.z + d.Q + f
        } else c.P = a.P * d.a + a.Q * d.s + d.P, c.Q = a.P * d.b + a.Q * d.z + d.Q
    } else c.a = a.a, c.b = a.b, c.s = a.s, c.z = a.z, c.P = a.P, c.Q = a.Q;
    this.bn = r;
    if (this.u && 0 !== this.u.length) {
        d = this.u;
        a = 0;
        for (c = d.length; a < c; a++)d[a].ef()
    }
}, M.transform = function () {
    var a = this.dj(), c = this.Ad;
    if (this.Ja) {
        var d = this.Ja.Ad;
        c.a = a.a * d.a + a.b * d.s;
        c.b = a.a * d.b + a.b * d.z;
        c.s = a.s *
            d.a + a.z * d.s;
        c.z = a.s * d.b + a.z * d.z;
        var e = this.Ja.ck, f = -(e.b + e.s) * a.P;
        c.P = a.P * d.a + a.Q * d.s + d.P + -(e.b + e.s) * a.Q;
        c.Q = a.P * d.b + a.Q * d.z + d.Q + f
    } else c.a = a.a, c.b = a.b, c.s = a.s, c.z = a.z, c.P = a.P, c.Q = a.Q
}, M.dj = function () {
    if (this.xy && this.Ja) {
        var a = this.Ja.S;
        this.xa.x = this.Tm.x * a.width;
        this.xa.y = this.Tm.y * a.height;
        this.RD = r
    }
    if (this.dk) {
        a = this.ck;
        a.P = this.xa.x;
        a.Q = this.xa.y;
        var c = 1, d = 0;
        this.Uj && (c = Math.cos(this.Ep), d = Math.sin(this.Ep));
        a.a = a.z = c;
        a.b = -d;
        a.s = d;
        var e = this.sa, f = this.Ya, g = this.Ib.x, h = this.Ib.y, m = 1E-6 > e && -1E-6 <
        e ? 1E-6 : e, n = 1E-6 > f && -1E-6 < f ? 1E-6 : f;
        if (this.$g || this.ah) {
            var s = Math.tan(-this.$g * Math.PI / 180), t = Math.tan(-this.ah * Math.PI / 180);
            Infinity === s && (s = 99999999);
            Infinity === t && (t = 99999999);
            var v = h * s * m, w = g * t * n;
            a.a = c + -d * t;
            a.b = c * s + -d;
            a.s = d + c * t;
            a.z = d * s + c;
            a.P += c * v + -d * w;
            a.Q += d * v + c * w
        }
        if (1 !== e || 1 !== f)a.a *= m, a.s *= m, a.b *= n, a.z *= n;
        a.P += c * -g * m + -d * h * n;
        a.Q -= d * -g * m + c * h * n;
        this.Jj && (a.P += g, a.Q += h);
        this.is && (this.ck = F.wn(a, this.Cw), this.is = r);
        this.dk = r
    }
    return this.ck
}, M = q) : (F.assert(F.ac(F.N.oC), F.i.Hd, "BaseNodesWebGL.js"),
    F.N.oC(), delete F.N.oC);
F.assert(F.ac(F.N.BB), F.i.Hd, "BaseNodesPropertyDefine.js");
F.N.BB();
delete F.N.BB;
F.Bk = F.n.extend({
    textureAtlas: q,
    quadsToDraw: 0,
    up: 0,
    wN: 0,
    Ii: 0,
    Lh: 0,
    Cj: q,
    qb: r,
    q: q,
    ox: r,
    Mb: "AtlasNode",
    ctor: function (a, c, d, e) {
        F.n.prototype.ctor.call(this);
        this.Cj = F.color.WHITE;
        this.q = {src: F.Ac, J: F.zc};
        this.ox = r;
        e !== k && this.z5(a, c, d, e)
    },
    de: function () {
        F.B === F.Y && (this.Z = new F.uI(this))
    },
    mr: function () {
        F.log(F.i.wT)
    },
    mg: function () {
        return this.qb ? this.Cj : F.n.prototype.mg.call(this)
    },
    Of: function (a) {
        var c = this.color;
        this.qb = a;
        this.color = c
    },
    ng: A("qb"),
    Jf: A("q"),
    Vd: function (a, c) {
        this.q = c === k ? a : {src: a, J: c}
    },
    YH: z("textureAtlas"),
    Yy: A("textureAtlas"),
    kha: A("quadsToDraw"),
    moa: z("quadsToDraw"),
    Zi: q,
    Fc: q,
    yt: q,
    Dm: q,
    z5: function (a, c, d, e) {
        a || b("cc.AtlasNode.initWithTileFile(): title should not be null");
        a = F.Ra.ad(a);
        return this.Fa(a, c, d, e)
    },
    Fa: q,
    zD: function (a, c, d, e) {
        this.Ii = c;
        this.Lh = d;
        this.qb = p;
        this.Fc = a;
        if (!this.Fc)return F.log(F.i.vI), r;
        this.Zi = this.Fc;
        this.Kw();
        this.quadsToDraw = e;
        return p
    },
    AD: function (a, c, d, e) {
        this.Ii = c;
        this.Lh = d;
        this.Cj = F.color.WHITE;
        this.qb = p;
        this.q.src = F.Ac;
        this.q.J = F.zc;
        c = this.te;
        this.Dm =
            new Float32Array([c.r / 255, c.g / 255, c.b / 255, this.Xg / 255]);
        this.textureAtlas = new F.wi;
        this.textureAtlas.Fa(a, e);
        if (!this.textureAtlas)return F.log(F.i.vI), r;
        this.ig();
        this.YO();
        this.Kw();
        this.quadsToDraw = e;
        this.shaderProgram = F.le.Kc(F.QB);
        this.yt = F.l.getUniformLocation(this.shaderProgram.Pn(), "u_color");
        return p
    },
    Ka: q,
    Fj: function (a) {
        a = a || F.l;
        F.nu(this);
        F.pd(this.q.src, this.q.J);
        this.yt && this.Dm && (a.uniform4fv(this.yt, this.Dm), this.textureAtlas.jG(this.quadsToDraw))
    },
    rb: q,
    I0: function (a) {
        var c = this.te;
        c.r == a.r && c.g == a.g && c.b == a.b || (c = F.color(a.r, a.g, a.b), this.Cj = a, this.qb && (a = this.ab, c.r = c.r * a / 255, c.g = c.g * a / 255, c.b = c.b * a / 255), this.De())
    },
    De: function () {
        var a = this.Ma();
        if (a && this.Fc) {
            var c = this.Fc.Xa;
            if (c) {
                var d = a.Xa, a = F.rect(0, 0, c.width, c.height);
                d instanceof HTMLCanvasElement ? F.Mn(c, this.Cj, a, d) : (d = F.Mn(c, this.Cj, a), a = new F.ha, a.qd(d), a.Lb(), this.ib(a))
            }
        }
    },
    J0: function (a) {
        var c = F.color(a.r, a.g, a.b);
        this.Cj = a;
        var d = this.ab;
        this.qb && (c.r = c.r * d / 255, c.g = c.g * d / 255, c.b = c.b * d / 255);
        F.n.prototype.rb.call(this,
            a);
        a = this.wa;
        this.Dm = new Float32Array([a.r / 255, a.g / 255, a.b / 255, d / 255])
    },
    wb: u(),
    uO: function (a) {
        F.n.prototype.wb.call(this, a);
        this.qb && (this.color = this.Cj)
    },
    OE: function (a) {
        F.n.prototype.wb.call(this, a);
        this.qb ? this.color = this.Cj : (a = this.wa, this.Dm = new Float32Array([a.r / 255, a.g / 255, a.b / 255, this.ab / 255]))
    },
    Ma: q,
    Ks: A("Zi"),
    sD: function () {
        return this.textureAtlas.texture
    },
    ib: q,
    SE: z("Zi"),
    TE: function (a) {
        this.textureAtlas.texture = a;
        this.ig();
        this.YO()
    },
    Kw: q,
    JY: function () {
        var a = this.texture.$();
        this.wN = 0 | a.height /
            this.Lh;
        this.up = 0 | a.width / this.Ii
    },
    KY: function () {
        var a = this.texture, c = a.$();
        this.ox && (c = a.S);
        this.wN = 0 | c.height / this.Lh;
        this.up = 0 | c.width / this.Ii
    },
    ig: function () {
        this.textureAtlas.texture.gi() || (this.q.src = F.SRC_ALPHA, this.q.J = F.ONE_MINUS_SRC_ALPHA)
    },
    YO: function () {
        this.qb = this.textureAtlas.texture.gi()
    },
    LE: z("ox")
});
M = F.Bk.prototype;
F.B === F.Y ? (M.Fa = M.AD, M.Ka = M.Fj, M.rb = M.J0, M.wb = M.OE, M.Ma = M.sD, M.ib = M.TE, M.Kw = M.KY) : (M.Fa = M.zD, M.Ka = F.n.prototype.Ka, M.rb = M.I0, M.wb = M.uO, M.Ma = M.Ks, M.ib = M.SE, M.Kw = M.JY, F.ta.hy || (M.De = function () {
    var a, c = this.Ma();
    if (c && this.Fc && (a = c.Xa)) {
        var d = this.Fc.Xa;
        if (c = F.Ra.Vt(d))d = F.rect(0, 0, d.width, d.height), a instanceof HTMLCanvasElement ? F.Il(a, c, this.wa, d, a) : (a = F.Il(a, c, this.wa, d), c = new F.ha, c.qd(a), c.Lb(), this.ib(c))
    }
}));
F.k(M, "opacity", M.jh, M.wb);
F.k(M, "color", M.mg, M.rb);
F.k(M, "texture", M.Ma, M.ib);
F.Bk.create = function (a, c, d, e) {
    return new F.Bk(a, c, d, e)
};
F.N.uC = function () {
    F.ha = F.za.extend({
        Jca: p,
        Pi: q,
        Rj: 0,
        Qj: 0,
        zp: "",
        S: q,
        jj: 0,
        kj: 0,
        nx: r,
        np: r,
        shaderProgram: q,
        Jb: r,
        Xa: q,
        ve: q,
        url: q,
        ctor: function () {
            this.S = F.size(0, 0);
            this.Pi = F.ha.DP
        },
        CH: function () {
            this.ve && F.l.deleteTexture(this.ve);
            F.aa.oj(this.url)
        },
        lQ: A("Pi"),
        EG: A("Rj"),
        DG: A("Qj"),
        getName: A("ve"),
        $: function () {
            return F.size(this.S.width / F.Fb(), this.S.height / F.Fb())
        },
        Gh: function () {
            return this.S.width / F.Fb()
        },
        dl: function () {
            return this.S.height / F.Fb()
        },
        X3: A("S"),
        w4: A("jj"),
        q8: z("jj"),
        x4: A("kj"),
        r8: z("kj"),
        HG: A("shaderProgram"),
        Ou: z("shaderProgram"),
        gi: A("nx"),
        m5: A("np"),
        description: function () {
            return "\x3ccc.Texture2D | Name \x3d " + this.zp + " | Dimensions \x3d " + this.Rj + " x " + this.Qj + " | Coordinates \x3d (" + this.jj + ", " + this.kj + ")\x3e"
        },
        t7: u(),
        U5: aa(),
        au: function (a, c, d, e, f) {
            var g = F.ha, h = F.l, m = h.RGBA, n = h.UNSIGNED_BYTE, s = d * F.ha.gL[c] / 8;
            0 === s % 8 ? h.pixelStorei(h.UNPACK_ALIGNMENT, 8) : 0 === s % 4 ? h.pixelStorei(h.UNPACK_ALIGNMENT, 4) : 0 === s % 2 ? h.pixelStorei(h.UNPACK_ALIGNMENT, 2) : h.pixelStorei(h.UNPACK_ALIGNMENT, 1);
            this.ve =
                h.createTexture();
            F.Cd(this);
            h.texParameteri(h.TEXTURE_2D, h.TEXTURE_MIN_FILTER, h.LINEAR);
            h.texParameteri(h.TEXTURE_2D, h.TEXTURE_MAG_FILTER, h.LINEAR);
            h.texParameteri(h.TEXTURE_2D, h.TEXTURE_WRAP_S, h.CLAMP_TO_EDGE);
            h.texParameteri(h.TEXTURE_2D, h.TEXTURE_WRAP_T, h.CLAMP_TO_EDGE);
            switch (c) {
                case g.xj:
                    m = h.RGBA;
                    break;
                case g.Pr:
                    m = h.RGB;
                    break;
                case g.Ov:
                    n = h.UNSIGNED_SHORT_4_4_4_4;
                    break;
                case g.Nv:
                    n = h.UNSIGNED_SHORT_5_5_5_1;
                    break;
                case g.Or:
                    n = h.UNSIGNED_SHORT_5_6_5;
                    break;
                case g.tB:
                    m = h.LUMINANCE_ALPHA;
                    break;
                case g.Nr:
                    m =
                        h.ALPHA;
                    break;
                case g.uB:
                    m = h.LUMINANCE;
                    break;
                default:
                    F.assert(0, F.i.uX)
            }
            h.texImage2D(h.TEXTURE_2D, 0, m, d, e, 0, m, n, a);
            this.S.width = f.width;
            this.S.height = f.height;
            this.Rj = d;
            this.Qj = e;
            this.Pi = c;
            this.jj = f.width / d;
            this.kj = f.height / e;
            this.np = this.nx = r;
            this.shaderProgram = F.le.Kc(F.Tr);
            return this.Jb = p
        },
        b3: function (a) {
            var c = [0, this.kj, this.jj, this.kj, 0, 0, this.jj, 0], d = this.Rj * this.jj, e = this.Qj * this.kj;
            a = [a.x, a.y, 0, d + a.x, a.y, 0, a.x, e + a.y, 0, d + a.x, e + a.y, 0];
            F.Xb(F.Jd | F.Eo);
            this.na.xb();
            this.na.Qu();
            F.Cd(this);
            d = F.l;
            d.vertexAttribPointer(F.kb, 2, d.FLOAT, r, 0, a);
            d.vertexAttribPointer(F.hd, 2, d.FLOAT, r, 0, c);
            d.drawArrays(d.TRIANGLE_STRIP, 0, 4)
        },
        d3: function (a) {
            var c = [0, this.kj, this.jj, this.kj, 0, 0, this.jj, 0];
            a = [a.x, a.y, a.x + a.width, a.y, a.x, a.y + a.height, a.x + a.width, a.y + a.height];
            F.Xb(F.Jd | F.Eo);
            this.na.xb();
            this.na.Qu();
            F.Cd(this);
            var d = F.l;
            d.vertexAttribPointer(F.kb, 2, d.FLOAT, r, 0, a);
            d.vertexAttribPointer(F.hd, 2, d.FLOAT, r, 0, c);
            d.drawArrays(d.TRIANGLE_STRIP, 0, 4)
        },
        PQ: function (a) {
            if (a == q)return F.log(F.i.xX), r;
            var c = a.g5(),
                d = a.k4(), e = F.kq.Ws;
            if (c > e || d > e)return F.log(F.i.yX, c, d, e, e), r;
            this.Jb = p;
            return this.o_(a, c, d)
        },
        qd: function (a) {
            a && (this.ve = F.l.createTexture(), this.Xa = a)
        },
        m4: A("Xa"),
        L5: A("Jb"),
        Lb: function () {
            if (F.Tx) {
                if (!this.Xa) {
                    var a = F.aa.ge(this.url);
                    if (!a)return;
                    this.qd(a)
                }
                this.Xa.width && this.Xa.height && (this.Jb = p, a = F.l, F.Cd(this), a.pixelStorei(a.UNPACK_ALIGNMENT, 4), a.texImage2D(a.TEXTURE_2D, 0, a.RGBA, a.RGBA, a.UNSIGNED_BYTE, this.Xa), a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MIN_FILTER, a.LINEAR), a.texParameteri(a.TEXTURE_2D,
                    a.TEXTURE_MAG_FILTER, a.LINEAR), a.texParameteri(a.TEXTURE_2D, a.TEXTURE_WRAP_S, a.CLAMP_TO_EDGE), a.texParameteri(a.TEXTURE_2D, a.TEXTURE_WRAP_T, a.CLAMP_TO_EDGE), this.shaderProgram = F.le.Kc(F.Tr), F.Cd(q), a = this.Xa.height, this.Rj = this.S.width = this.Xa.width, this.Qj = this.S.height = a, this.Pi = F.ha.xj, this.kj = this.jj = 1, this.np = this.nx = r, this.dispatchEvent("load"))
            }
        },
        Td: function () {
            F.log(F.i.DX);
            return q
        },
        t5: function () {
            F.log(F.i.wX);
            return r
        },
        v5: function () {
            F.log(F.i.AX);
            return r
        },
        w5: function () {
            F.log(F.i.CX);
            return r
        },
        O8: function (a, c, d, e) {
            var f = F.l;
            c !== k && (a = {J6: a, G6: c, iT: d, jT: e});
            F.assert(this.Rj == F.Gk(this.Rj) && this.Qj == F.Gk(this.Qj) || a.iT == f.CLAMP_TO_EDGE && a.jT == f.CLAMP_TO_EDGE, "WebGLRenderingContext.CLAMP_TO_EDGE should be used in NPOT textures");
            F.Cd(this);
            f.texParameteri(f.TEXTURE_2D, f.TEXTURE_MIN_FILTER, a.J6);
            f.texParameteri(f.TEXTURE_2D, f.TEXTURE_MAG_FILTER, a.G6);
            f.texParameteri(f.TEXTURE_2D, f.TEXTURE_WRAP_S, a.iT);
            f.texParameteri(f.TEXTURE_2D, f.TEXTURE_WRAP_T, a.jT)
        },
        W7: function () {
            var a = F.l;
            F.Cd(this);
            this.np ?
                a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MIN_FILTER, a.LINEAR_MIPMAP_NEAREST) : a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MIN_FILTER, a.LINEAR);
            a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MAG_FILTER, a.LINEAR)
        },
        KH: function () {
            var a = F.l;
            F.Cd(this);
            this.np ? a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MIN_FILTER, a.NEAREST_MIPMAP_NEAREST) : a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MIN_FILTER, a.NEAREST);
            a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MAG_FILTER, a.NEAREST)
        },
        generateMipmap: function () {
            F.assert(this.Rj == F.Gk(this.Rj) && this.Qj ==
                F.Gk(this.Qj), "Mimpap texture only works in POT textures");
            F.Cd(this);
            F.l.generateMipmap(F.l.TEXTURE_2D);
            this.np = p
        },
        w9: function () {
            return F.ha.mY[this.Pi]
        },
        D2: function (a) {
            a = a || this.Pi;
            var c = F.ha.gL[a];
            if (c != q)return c;
            F.log(F.i.tX, a);
            return -1
        },
        o_: function (a, c, d) {
            var e = F.ha, f = a.getData(), g = q, g = q, h = a.nia(), m = F.size(a.g5(), a.k4()), n = e.DP, s = a.Ffa();
            h || (8 <= s ? n = e.Pr : (F.log(F.i.qX), n = e.Or));
            var t = c * d;
            if (n == e.Or)if (h) {
                f = new Uint16Array(c * d);
                g = a.getData();
                for (s = 0; s < t; ++s)f[s] = (g[s] >> 0 & 255) >> 3 << 11 | (g[s] >> 8 &
                    255) >> 2 << 5 | (g[s] >> 16 & 255) >> 3 << 0
            } else {
                f = new Uint16Array(c * d);
                g = a.getData();
                for (s = 0; s < t; ++s)f[s] = (g[s] & 255) >> 3 << 11 | (g[s] & 255) >> 2 << 5 | (g[s] & 255) >> 3 << 0
            } else if (n == e.Ov) {
                f = new Uint16Array(c * d);
                g = a.getData();
                for (s = 0; s < t; ++s)f[s] = (g[s] >> 0 & 255) >> 4 << 12 | (g[s] >> 8 & 255) >> 4 << 8 | (g[s] >> 16 & 255) >> 4 << 4 | (g[s] >> 24 & 255) >> 4 << 0
            } else if (n == e.Nv) {
                f = new Uint16Array(c * d);
                g = a.getData();
                for (s = 0; s < t; ++s)f[s] = (g[s] >> 0 & 255) >> 3 << 11 | (g[s] >> 8 & 255) >> 3 << 6 | (g[s] >> 16 & 255) >> 3 << 1 | (g[s] >> 24 & 255) >> 7 << 0
            } else if (n == e.Nr) {
                f = new Uint8Array(c * d);
                g = a.getData();
                for (s = 0; s < t; ++s)f[s] = g >> 24 & 255
            }
            if (h && n == e.Pr) {
                g = a.getData();
                f = new Uint8Array(3 * c * d);
                for (s = 0; s < t; ++s)f[3 * s] = g >> 0 & 255, f[3 * s + 1] = g >> 8 & 255, f[3 * s + 2] = g >> 16 & 255
            }
            this.au(f, n, c, d, m);
            a.getData();
            this.nx = a.Kia();
            return p
        },
        Ft: function (a, c) {
            this.addEventListener("load", a, c)
        },
        y7: function (a) {
            this.removeEventListener("load", a)
        }
    })
};
F.N.vC = function () {
    var a = F.wi.prototype;
    a.gn = function () {
        var a = F.l;
        this.oc[0] = a.createBuffer();
        this.oc[1] = a.createBuffer();
        this.Zm = a.createBuffer();
        this.Bx()
    };
    a.Bx = function () {
        var a = F.l;
        a.bindBuffer(a.ARRAY_BUFFER, this.Zm);
        a.bufferData(a.ARRAY_BUFFER, this.zd, a.DYNAMIC_DRAW);
        a.bindBuffer(a.ELEMENT_ARRAY_BUFFER, this.oc[1]);
        a.bufferData(a.ELEMENT_ARRAY_BUFFER, this.zb, a.STATIC_DRAW)
    };
    a.jG = function (a) {
        var d;
        d = 0;
        if (!(0 === a || !this.texture || !this.texture.Jb)) {
            var e = F.l;
            F.Cd(this.texture);
            F.Xb(F.Bh);
            e.bindBuffer(e.ARRAY_BUFFER,
                this.Zm);
            this.dirty && e.bufferData(e.ARRAY_BUFFER, this.zd, e.DYNAMIC_DRAW);
            e.vertexAttribPointer(F.kb, 3, e.FLOAT, r, 24, 0);
            e.vertexAttribPointer(F.sd, 4, e.UNSIGNED_BYTE, p, 24, 12);
            e.vertexAttribPointer(F.hd, 2, e.FLOAT, r, 24, 16);
            this.dirty && (this.dirty = r);
            e.bindBuffer(e.ELEMENT_ARRAY_BUFFER, this.oc[1]);
            F.gK ? e.drawElements(e.TRIANGLE_STRIP, 6 * a, e.UNSIGNED_SHORT, 6 * d * this.zb.BYTES_PER_ELEMENT) : e.drawElements(e.TRIANGLES, 6 * a, e.UNSIGNED_SHORT, 6 * d * this.zb.BYTES_PER_ELEMENT);
            F.bd++
        }
    }
};
F.N.wC = function () {
    var a = F.Ra;
    a.Lb = function (a) {
        var d = this.$c;
        F.Tx || (d = this.Vs);
        var e = d[a];
        e || (e = d[a] = new F.ha, e.url = a);
        e.Lb()
    };
    a.ad = function (a, d, e) {
        F.assert(a, F.i.sX);
        var f = this.$c;
        F.Tx || (f = this.Vs);
        var g = f[a] || f[F.aa.xm[a]];
        if (g)return d && d.call(e, g), g;
        F.aa.ge(a) || (F.aa.CL(a) ? F.aa.load(a, function () {
            d && d.call(e)
        }) : F.aa.Kq(a, function (f, m) {
            if (f)return d ? d(f) : f;
            F.aa.jg[a] = m;
            F.Ra.Lb(a);
            d && d.call(e, g)
        }));
        g = f[a] = new F.ha;
        g.url = a;
        return g
    };
    a = q
};
F.N.HB = function () {
    var a = F.ha;
    a.fba = function (a) {
        F.TV = a
    };
    a.xj = 2;
    a.Pr = 3;
    a.Or = 4;
    a.Nr = 5;
    a.uB = 6;
    a.tB = 7;
    a.Ov = 8;
    a.Nv = 7;
    a.rJ = 9;
    a.qJ = 10;
    a.KV = a.xj;
    var c = F.ha.mY = {};
    c[a.xj] = "RGBA8888";
    c[a.Pr] = "RGB888";
    c[a.Or] = "RGB565";
    c[a.Nr] = "A8";
    c[a.uB] = "I8";
    c[a.tB] = "AI88";
    c[a.Ov] = "RGBA4444";
    c[a.Nv] = "RGB5A1";
    c[a.rJ] = "PVRTC4";
    c[a.qJ] = "PVRTC2";
    c = F.ha.gL = {};
    c[a.xj] = 32;
    c[a.Pr] = 24;
    c[a.Or] = 16;
    c[a.Nr] = 8;
    c[a.uB] = 8;
    c[a.tB] = 16;
    c[a.Ov] = 16;
    c[a.Nv] = 16;
    c[a.rJ] = 4;
    c[a.qJ] = 3;
    c = F.ha.prototype;
    F.k(c, "name", c.getName);
    F.k(c, "pixelFormat", c.lQ);
    F.k(c, "pixelsWidth", c.EG);
    F.k(c, "pixelsHeight", c.DG);
    F.k(c, "width", c.Gh);
    F.k(c, "height", c.dl);
    a.DP = a.KV
};
F.N.IB = function () {
    var a = F.wi.prototype;
    F.k(a, "totalQuads", a.DQ);
    F.k(a, "capacity", a.dQ);
    F.k(a, "quads", a.nQ, a.tS)
};
F.j$ = 51;
F.m$ = 19;
F.o$ = 18;
F.l$ = 50;
F.i$ = 34;
F.g$ = 35;
F.h$ = 33;
F.k$ = 49;
F.n$ = 17;
F.TV = r;
F.B === F.Aa ? F.ha = F.za.extend({
    S: q,
    Jb: r,
    Xa: q,
    url: q,
    ctor: function () {
        this.S = F.size(0, 0);
        this.Jb = r;
        this.Xa = q
    },
    EG: function () {
        return this.S.width
    },
    DG: function () {
        return this.S.height
    },
    $: function () {
        var a = F.Fb();
        return F.size(this.S.width / a, this.S.height / a)
    },
    Gh: function () {
        return this.S.width / F.Fb()
    },
    dl: function () {
        return this.S.height / F.Fb()
    },
    X3: A("S"),
    qd: function (a) {
        a && (this.Xa = a)
    },
    m4: A("Xa"),
    L5: A("Jb"),
    Lb: function () {
        if (!this.Jb) {
            if (!this.Xa) {
                var a = F.aa.ge(this.url);
                if (!a)return;
                this.qd(a)
            }
            this.Jb = p;
            a = this.Xa;
            this.S.width = a.width;
            this.S.height = a.height;
            this.dispatchEvent("load")
        }
    },
    description: function () {
        return "\x3ccc.Texture2D | width \x3d " + this.S.width + " height " + this.S.height + "\x3e"
    },
    au: D(r),
    PQ: D(r),
    Td: D(r),
    CH: u(),
    getName: D(q),
    w4: D(1),
    q8: u(),
    x4: D(1),
    r8: u(),
    lQ: D(q),
    HG: D(q),
    Ou: u(),
    gi: D(r),
    m5: D(r),
    t7: u(),
    U5: aa(),
    b3: u(),
    d3: u(),
    t5: function () {
        F.log(F.i.vX);
        return r
    },
    v5: function () {
        F.log(F.i.zX);
        return r
    },
    w5: function () {
        F.log(F.i.BX);
        return r
    },
    O8: u(),
    W7: u(),
    KH: u(),
    generateMipmap: u(),
    w9: D(""),
    D2: D(-1),
    Ft: function (a,
                  c) {
        this.addEventListener("load", a, c)
    },
    y7: function (a) {
        this.removeEventListener("load", a)
    }
}) : (F.assert(F.ac(F.N.uC), F.i.Hd, "TexturesWebGL.js"), F.N.uC(), delete F.N.uC);
F.Ah.prototype.apply(F.ha.prototype);
F.assert(F.ac(F.N.HB), F.i.Hd, "TexturesPropertyDefine.js");
F.N.HB();
delete F.N.HB;
F.Ra = {
    $c: {}, ut: {}, lF: 0 | 1E3 * Math.random(), Vs: {}, t_: function () {
        var a, c = this.Vs, d = this.$c;
        for (a in c) {
            var e = c[a];
            e.Lb();
            d[a] = e
        }
        this.Vs = {}
    }, hda: function () {
        F.log(F.i.B9)
    }, eda: function () {
        F.log(F.i.z9)
    }, description: function () {
        return "\x3cTextureCache | Number of textures \x3d " + this.$c.length + "\x3e"
    }, wpa: function (a) {
        F.log(F.i.G9);
        return this.Ml(a)
    }, Ml: function (a) {
        return this.$c[a] || this.$c[F.aa.xm[a]]
    }, o4: function (a) {
        for (var c in this.$c)if (this.$c[c] == a)return c;
        return q
    }, yZ: function () {
        this.lF++;
        return "_textureKey_" +
            this.lF
    }, Vt: function (a) {
        var c = this.o4(a);
        c || (c = a instanceof HTMLImageElement ? a.src : this.yZ());
        this.ut[c] || (this.ut[c] = F.uq(a));
        return this.ut[c]
    }, gda: function () {
        F.log(F.i.A9)
    }, vma: function () {
        var a = this.$c, c;
        for (c in a)a[c] && a[c].CH();
        this.$c = {}
    }, Fma: function (a) {
        if (a) {
            var c = this.$c, d;
            for (d in c)c[d] == a && (c[d].CH(), delete c[d])
        }
    }, Gma: function (a) {
        a != q && this.$c[a] && delete this.$c[a]
    }, E2: function (a, c) {
        if (c instanceof F.ha)this.$c[a] = c; else {
            var d = new F.ha;
            d.qd(c);
            d.Lb();
            this.$c[a] = d
        }
    }, mda: function (a, c) {
        F.assert(a,
            F.i.D9);
        if (c && this.$c[c])return this.$c[c];
        var d = new F.ha;
        d.PQ(a);
        c != q && d != q ? this.$c[c] = d : F.log(F.i.C9);
        return d
    }, Aea: function () {
        var a = 0, c = 0, d = this.$c, e;
        for (e in d) {
            var f = d[e];
            a++;
            f.Xa instanceof HTMLImageElement ? F.log(F.i.E9, e, f.Xa.src, f.pixelsWidth, f.pixelsHeight) : F.log(F.i.QS, e, f.pixelsWidth, f.pixelsHeight);
            c += 4 * f.pixelsWidth * f.pixelsHeight
        }
        d = this.ut;
        for (e in d) {
            var f = d[e], g;
            for (g in f) {
                var h = f[g];
                a++;
                F.log(F.i.QS, e, h.width, h.height);
                c += 4 * h.width * h.height
            }
        }
        F.log(F.i.F9, a, c / 1024, (c / 1048576).toFixed(2))
    },
    Vk: function () {
        this.$c = {};
        this.ut = {};
        this.lF = 0 | 1E3 * Math.random();
        this.Vs = {}
    }
};
F.B === F.Aa ? (M = F.Ra, M.Lb = function (a) {
    var c = this.$c, d = c[a];
    d || (d = c[a] = new F.ha, d.url = a);
    d.Lb()
}, M.ad = function (a, c, d) {
    F.assert(a, F.i.rX);
    var e = this.$c, f = e[a] || e[F.aa.xm[a]];
    if (f)return c && c.call(d, f), f;
    f = e[a] = new F.ha;
    f.url = a;
    F.aa.ge(a) ? f.Lb() : F.aa.CL(a) ? F.aa.load(a, function () {
        c && c.call(d)
    }) : F.aa.Kq(a, function (e, h) {
        if (e)return c ? c(e) : e;
        F.aa.jg[a] = h;
        F.Ra.Lb(a);
        c && c.call(d, f)
    });
    return f
}, M = q) : (F.assert(F.ac(F.N.wC), F.i.Hd, "TexturesWebGL.js"), F.N.wC(), delete F.N.wC);
F.wi = F.za.extend({
    dirty: r,
    texture: q,
    zb: q,
    oc: q,
    ud: 0,
    Xc: q,
    zd: q,
    Zm: q,
    eg: q,
    ctor: function (a, c) {
        this.oc = [];
        F.Dd(a) ? this.Nl(a, c) : a instanceof F.ha && this.Fa(a, c)
    },
    DQ: A("Za"),
    dQ: A("ud"),
    Ma: A("texture"),
    ib: z("texture"),
    gS: z("dirty"),
    cH: A("dirty"),
    nQ: A("Xc"),
    tS: z("Xc"),
    aZ: function (a, c) {
        if (a)for (var d = 0; d < a.length; d++)this.$x(a[d], c + d)
    },
    $x: function (a, c) {
        var d = this.Xc;
        d[c] ? (d[c].K = a.K, d[c].V = a.V, d[c].U = a.U, d[c].O = a.O) : d[c] = new F.Hb(a.U, a.K, a.O, a.V, this.zd, c * F.Hb.BYTES_PER_ELEMENT)
    },
    description: function () {
        return "\x3ccc.TextureAtlas | totalQuads \x3d" +
            this.Za + "\x3e"
    },
    zO: function () {
        if (0 !== this.ud)for (var a = this.zb, c = this.ud, d = 0; d < c; d++)F.gK ? (a[6 * d + 0] = 4 * d + 0, a[6 * d + 1] = 4 * d + 0, a[6 * d + 2] = 4 * d + 2, a[6 * d + 3] = 4 * d + 1, a[6 * d + 4] = 4 * d + 3, a[6 * d + 5] = 4 * d + 3) : (a[6 * d + 0] = 4 * d + 0, a[6 * d + 1] = 4 * d + 1, a[6 * d + 2] = 4 * d + 2, a[6 * d + 3] = 4 * d + 3, a[6 * d + 4] = 4 * d + 2, a[6 * d + 5] = 4 * d + 1)
    },
    gn: function () {
        var a = F.l;
        this.oc[0] = a.createBuffer();
        this.oc[1] = a.createBuffer();
        this.Zm = a.createBuffer();
        this.Bx()
    },
    Bx: function () {
        var a = F.l;
        a.bindBuffer(a.ARRAY_BUFFER, this.Zm);
        a.bufferData(a.ARRAY_BUFFER, this.zd, a.DYNAMIC_DRAW);
        a.bindBuffer(a.ELEMENT_ARRAY_BUFFER, this.oc[1]);
        a.bufferData(a.ELEMENT_ARRAY_BUFFER, this.zb, a.STATIC_DRAW)
    },
    Nl: function (a, c) {
        var d = F.Ra.ad(a);
        if (d)return this.Fa(d, c);
        F.log(F.i.EX, a);
        return r
    },
    Fa: function (a, c) {
        F.assert(a, F.i.FX);
        this.ud = c |= 0;
        this.Za = 0;
        this.texture = a;
        this.Xc = [];
        this.zb = new Uint16Array(6 * c);
        var d = F.Hb.BYTES_PER_ELEMENT;
        this.zd = new ArrayBuffer(d * c);
        this.eg = new Uint8Array(this.zd);
        if ((!this.Xc || !this.zb) && 0 < c)return r;
        for (var e = this.Xc, f = 0; f < c; f++)e[f] = new F.Hb(q, q, q, q, this.zd, f * d);
        this.zO();
        this.gn();
        return this.dirty = p
    },
    bv: function (a, c) {
        F.assert(a, F.i.PX);
        F.assert(0 <= c && c < this.ud, F.i.QX);
        this.Za = Math.max(c + 1, this.Za);
        this.$x(a, c);
        this.dirty = p
    },
    aH: function (a, c) {
        F.assert(c < this.ud, F.i.IX);
        this.Za++;
        if (this.Za > this.ud)F.log(F.i.pK); else {
            var d = F.Hb.BYTES_PER_ELEMENT, e = c * d, f = (this.Za - 1 - c) * d;
            this.Xc[this.Za - 1] = new F.Hb(q, q, q, q, this.zd, (this.Za - 1) * d);
            this.eg.set(this.eg.subarray(e, e + f), e + d);
            this.$x(a, c);
            this.dirty = p
        }
    },
    uia: function (a, c, d) {
        d = d || a.length;
        F.assert(c + d <= this.ud, F.i.JX);
        var e = F.Hb.BYTES_PER_ELEMENT;
        this.Za += d;
        if (this.Za > this.ud)F.log(F.i.pK); else {
            var f = c * e, g = (this.Za - 1 - c - d) * e, h = this.Za - 1 - d, m;
            for (m = 0; m < d; m++)this.Xc[h + m] = new F.Hb(q, q, q, q, this.zd, (this.Za - 1) * e);
            this.eg.set(this.eg.subarray(f, f + g), f + e * d);
            for (m = 0; m < d; m++)this.$x(a[m], c + m);
            this.dirty = p
        }
    },
    tia: function (a, c) {
        if (a !== c) {
            F.assert(0 <= c || c < this.Za, F.i.GX);
            F.assert(0 <= a || a < this.Za, F.i.HX);
            var d = F.Hb.BYTES_PER_ELEMENT, e = this.eg, f = e.subarray(a * d, d), g;
            a > c ? (g = c * d, e.set(e.subarray(g, g + (a - c) * d), g + d), e.set(f, g)) : (g = (a + 1) * d, e.set(e.subarray(g, g +
                (c - a) * d), g - d), e.set(f, c * d));
            this.dirty = p
        }
    },
    PR: function (a) {
        F.assert(a < this.Za, F.i.NX);
        var c = F.Hb.BYTES_PER_ELEMENT;
        this.Za--;
        this.Xc.length = this.Za;
        if (a !== this.Za) {
            var d = (a + 1) * c;
            this.eg.set(this.eg.subarray(d, d + (this.Za - a) * c), d - c)
        }
        this.dirty = p
    },
    A7: function (a, c) {
        F.assert(a + c <= this.Za, F.i.OX);
        this.Za -= c;
        if (a !== this.Za) {
            var d = F.Hb.BYTES_PER_ELEMENT, e = (a + c) * d;
            this.eg.set(this.eg.subarray(e, e + (this.Za - a) * d), a * d)
        }
        this.dirty = p
    },
    Tq: function () {
        this.Za = this.Xc.length = 0
    },
    rl: z("dirty"),
    Zz: function (a) {
        if (a == this.ud)return p;
        var c = F.Hb.BYTES_PER_ELEMENT, d = this.ud;
        this.Za = Math.min(this.Za, a);
        var e = this.ud = 0 | a, f = this.Za;
        if (this.Xc == q) {
            this.Xc = [];
            this.zd = new ArrayBuffer(c * e);
            this.eg = new Uint8Array(this.zd);
            for (a = 0; a < e; a++)this.Xc = new F.Hb(q, q, q, q, this.zd, a * c)
        } else {
            var g, h, m = this.Xc;
            if (e > d) {
                g = [];
                h = new ArrayBuffer(c * e);
                for (a = 0; a < f; a++)g[a] = new F.Hb(m[a].U, m[a].K, m[a].O, m[a].V, h, a * c);
                for (; a < e; a++)g[a] = new F.Hb(q, q, q, q, h, a * c)
            } else {
                f = Math.max(f, e);
                g = [];
                h = new ArrayBuffer(c * e);
                for (a = 0; a < f; a++)g[a] = new F.Hb(m[a].U, m[a].K, m[a].O,
                    m[a].V, h, a * c)
            }
            this.eg = new Uint8Array(h);
            this.Xc = g;
            this.zd = h
        }
        this.zb == q ? this.zb = new Uint16Array(6 * e) : e > d ? (c = new Uint16Array(6 * e), c.set(this.zb, 0), this.zb = c) : this.zb = this.zb.subarray(0, 6 * e);
        this.zO();
        this.Bx();
        return this.dirty = p
    },
    JQ: function (a) {
        this.Za += a
    },
    qR: function (a, c, d) {
        if (d === k) {
            if (d = c, c = this.Za - a, F.assert(d + (this.Za - a) <= this.ud, F.i.KX), 0 === c)return
        } else if (F.assert(d + c <= this.Za, F.i.LX), F.assert(a < this.Za, F.i.MX), a == d)return;
        var e = F.Hb.BYTES_PER_ELEMENT, f = a * e, g = c * e, h = this.eg, m = h.subarray(f, f +
            g), n = d * e;
        d < a ? (c = d * e, h.set(h.subarray(c, c + (a - d) * e), c + g)) : (c = (a + c) * e, h.set(h.subarray(c, c + (d - a) * e), f));
        h.set(m, n);
        this.dirty = p
    },
    XP: function (a, c) {
        for (var d = c * F.Hb.BYTES_PER_ELEMENT, e = new Uint8Array(this.zd, a * F.Hb.BYTES_PER_ELEMENT, d), f = 0; f < d; f++)e[f] = 0
    },
    Gl: function () {
        this.jG(this.Za)
    },
    Lca: function () {
        var a = F.l;
        this.oc && (this.oc[0] && a.deleteBuffer(this.oc[0]), this.oc[1] && a.deleteBuffer(this.oc[1]));
        this.Zm && a.deleteBuffer(this.Zm)
    }
});
M = F.wi.prototype;
F.k(M, "totalQuads", M.DQ);
F.k(M, "capacity", M.dQ);
F.k(M, "quads", M.nQ, M.tS);
F.wi.create = function (a, c) {
    return new F.wi(a, c)
};
F.wi.aG = F.wi.create;
F.B === F.Y && (F.assert(F.ac(F.N.vC), F.i.Hd, "TexturesWebGL.js"), F.N.vC(), delete F.N.vC);
F.assert(F.ac(F.N.IB), F.i.Hd, "TexturesPropertyDefine.js");
F.N.IB();
delete F.N.IB;
F.rm = F.n.extend({
    Mb: "Scene", ctor: function () {
        F.n.prototype.ctor.call(this);
        this.Jj = p;
        this.Zl(0.5, 0.5);
        this.ze(F.L.Pa())
    }
});
F.rm.create = function () {
    return new F.rm
};
F.kB = F.rm.extend({
    hl: q, vd: q, Mb: "LoaderScene", oa: function () {
        var a = this, c = 200, d = a.GY = new F.Gd(F.color(32, 32, 32, 255));
        d.X(F.io.NF);
        a.F(d, 0);
        var e = 24, f = -c / 2 + 100;
        F.ID && (F.aa.Kq(F.ID, {Ol: r}, function (d, e) {
            c = e.height;
            a.p_(e, F.io.Gy)
        }), e = 14, f = -c / 2 - 10);
        e = a.vd = new F.R("Loading... 0%", "Arial", e);
        e.X(F.sk(F.io.Gy, F.d(0, f)));
        e.rb(F.color(180, 180, 180));
        d.F(this.vd, 10);
        return p
    }, p_: function (a, c) {
        var d = this.Zca = new F.ha;
        d.qd(a);
        d.Lb();
        d = this.Dca = new F.I(d);
        d.$q(F.Fb());
        d.x = c.x;
        d.y = c.y;
        this.GY.F(d, 10)
    }, ba: function () {
        F.n.prototype.ba.call(this);
        this.eA(this.IO, 0.3)
    }, Cb: function () {
        F.n.prototype.Cb.call(this);
        this.vd.yc("Loading... 0%")
    }, y5: function (a, c) {
        F.Dd(a) && (a = [a]);
        this.I7 = a || [];
        this.Bn = c
    }, IO: function () {
        var a = this;
        a.Zu(a.IO);
        F.aa.load(a.I7, function (c, d, e) {
            c = Math.min(100 * (e / d) | 0, 100);
            a.vd.yc("Loading... " + c + "%")
        }, function () {
            a.Bn && a.Bn()
        })
    }
});
F.kB.yH = function (a, c) {
    var d = F;
    d.lu || (d.lu = new F.kB, d.lu.oa());
    d.lu.y5(a, c);
    F.L.GH(d.lu);
    return d.lu
};
F.N.jB = function () {
    var a = F.ec.prototype;
    a.jP = u();
    a.ZS = u();
    a.H = F.n.prototype.H
};
F.N.rC = function () {
    var a = F.Gd.prototype;
    a.cy = q;
    a.aF = q;
    a.zy = q;
    a.Nw = q;
    a.cF = q;
    a.bF = q;
    a.ctor = function (a, d, e) {
        this.cF = new ArrayBuffer(32);
        this.bF = new ArrayBuffer(16);
        var f = this.cF, g = this.bF, h = N.BYTES_PER_ELEMENT, m = F.Pf.BYTES_PER_ELEMENT;
        this.cy = [new N(0, 0, f, 0), new N(0, 0, f, h), new N(0, 0, f, 2 * h), new N(0, 0, f, 3 * h)];
        this.aF = [F.color(0, 0, 0, 255, g, 0), F.color(0, 0, 0, 255, g, m), F.color(0, 0, 0, 255, g, 2 * m), F.color(0, 0, 0, 255, g, 3 * m)];
        this.zy = F.l.createBuffer();
        this.Nw = F.l.createBuffer();
        F.ec.prototype.ctor.call(this);
        this.q =
            new F.dc(F.Ac, F.zc);
        F.Gd.prototype.oa.call(this, a, d, e)
    };
    a.de = function () {
        this.Z = new F.LB(this)
    };
    a.ze = function (a, d) {
        var e = this.cy;
        d === k ? (e[1].x = a.width, e[2].y = a.height, e[3].x = a.width, e[3].y = a.height) : (e[1].x = a, e[2].y = d, e[3].x = a, e[3].y = d);
        this.JC();
        F.ec.prototype.ze.call(this, a, d)
    };
    a.Xj = function (a) {
        var d = this.cy;
        d[1].x = a;
        d[3].x = a;
        this.JC();
        F.ec.prototype.Xj.call(this, a)
    };
    a.Wj = function (a) {
        var d = this.cy;
        d[2].y = a;
        d[3].y = a;
        this.JC();
        F.ec.prototype.Wj.call(this, a)
    };
    a.vc = function () {
        for (var a = this.wa, d = this.ab,
                 e = this.aF, f = 0; 4 > f; f++)e[f].r = a.r, e[f].g = a.g, e[f].b = a.b, e[f].a = d;
        this.yL()
    };
    a.Ka = function (a) {
        a = a || F.l;
        F.nu(this);
        F.Xb(F.Jd | F.Do);
        a.bindBuffer(a.ARRAY_BUFFER, this.zy);
        a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, 0, 0);
        a.bindBuffer(a.ARRAY_BUFFER, this.Nw);
        a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, 0, 0);
        F.pd(this.q.src, this.q.J);
        a.drawArrays(a.TRIANGLE_STRIP, 0, 4)
    };
    a.JC = function () {
        var a = F.l;
        a.bindBuffer(a.ARRAY_BUFFER, this.zy);
        a.bufferData(a.ARRAY_BUFFER, this.cF, a.STATIC_DRAW)
    };
    a.yL = function () {
        var a = F.l;
        a.bindBuffer(a.ARRAY_BUFFER, this.Nw);
        a.bufferData(a.ARRAY_BUFFER, this.bF, a.STATIC_DRAW)
    }
};
F.N.sC = function () {
    var a = F.nm.prototype;
    a.de = function () {
        this.Z = new F.LB(this)
    };
    a.Ka = F.Gd.prototype.Ka;
    a.vc = function () {
        var a = this.Sk, d = F.Ul(a);
        if (0 !== d) {
            var e = Math.sqrt(2), a = F.d(a.x / d, a.y / d);
            this.Ow && (a = F.lj(a, 1 / (Math.abs(a.x) + Math.abs(a.y)) * e));
            var f = this.ab / 255, d = this.wa, g = this.Ze, d = {r: d.r, g: d.g, b: d.b, a: this.Jp * f}, f = {
                r: g.r,
                g: g.g,
                b: g.b,
                a: this.$o * f
            }, h = this.aF, g = h[0], m = h[1], n = h[2], h = h[3];
            g.r = f.r + (d.r - f.r) * ((e + a.x + a.y) / (2 * e));
            g.g = f.g + (d.g - f.g) * ((e + a.x + a.y) / (2 * e));
            g.b = f.b + (d.b - f.b) * ((e + a.x + a.y) / (2 * e));
            g.a = f.a + (d.a - f.a) * ((e + a.x + a.y) / (2 * e));
            m.r = f.r + (d.r - f.r) * ((e - a.x + a.y) / (2 * e));
            m.g = f.g + (d.g - f.g) * ((e - a.x + a.y) / (2 * e));
            m.b = f.b + (d.b - f.b) * ((e - a.x + a.y) / (2 * e));
            m.a = f.a + (d.a - f.a) * ((e - a.x + a.y) / (2 * e));
            n.r = f.r + (d.r - f.r) * ((e + a.x - a.y) / (2 * e));
            n.g = f.g + (d.g - f.g) * ((e + a.x - a.y) / (2 * e));
            n.b = f.b + (d.b - f.b) * ((e + a.x - a.y) / (2 * e));
            n.a = f.a + (d.a - f.a) * ((e + a.x - a.y) / (2 * e));
            h.r = f.r + (d.r - f.r) * ((e - a.x - a.y) / (2 * e));
            h.g = f.g + (d.g - f.g) * ((e - a.x - a.y) / (2 * e));
            h.b = f.b + (d.b - f.b) * ((e - a.x - a.y) / (2 * e));
            h.a = f.a + (d.a - f.a) * ((e - a.x - a.y) / (2 * e));
            this.yL()
        }
    }
};
F.N.EB = function () {
    var a = F.Gd.prototype;
    F.k(a, "width", a.Gh, a.Xj);
    F.k(a, "height", a.dl, a.Wj)
};
F.N.FB = function () {
    var a = F.nm.prototype;
    F.k(a, "startColor", a.Xy, a.of);
    F.k(a, "endColor", a.Ty, a.nf);
    F.k(a, "startOpacity", a.W4, a.L8);
    F.k(a, "endOpacity", a.e4, a.i8);
    F.k(a, "vector", a.d5, a.U8)
};
F.ec = F.n.extend({
    il: r, ym: q, Jo: q, Mb: "Layer", ctor: function () {
        var a = F.n.prototype;
        a.ctor.call(this);
        this.Jj = p;
        a.Zl.call(this, 0.5, 0.5);
        a.ze.call(this, F.dv)
    }, oa: function () {
        this.Jj = p;
        this.Zl(0.5, 0.5);
        this.ze(F.dv);
        this.cascadeColor = this.cascadeOpacity = r;
        return p
    }, jP: q, ZS: q, Gw: q, zia: A("il"), H: q
});
F.ec.create = function () {
    return new F.ec
};
if (F.B === F.Aa) {
    var ia = F.ec.prototype;
    ia.jP = function () {
        if (!this.il) {
            this.il = this.pe = F.ka.xe = p;
            !this.Jo && this.Gw && (this.Jo = new F.HI(this, this.Gw));
            this.Uk = this;
            for (var a = this.u, c = 0, d = a.length; c < d; c++)a[c].ht(this);
            this.ym || (this.ym = new F.BT, this.ym.Ja = this)
        }
    };
    ia.ZS = function () {
        if (this.il) {
            F.ka.xe = p;
            this.il = r;
            this.pe = p;
            this.Uk = q;
            for (var a = this.u, c = 0, d = a.length; c < d; c++)a[c].ht(q)
        }
    };
    ia.F = function (a, c, d) {
        F.n.prototype.F.call(this, a, c, d);
        a.Ja == this && this.il && a.ht(this)
    };
    ia.Gw = function () {
        if (this.pe) {
            var a =
                this.u, c = this.ym, d = this.jD();
            d.width |= 0;
            d.height |= 0;
            var e = c.oe;
            c.SR(d.width, d.height);
            e.translate(0 - d.x, d.height + d.y);
            var f = F.Zp(this.Ad);
            e.transform(f.a, f.s, f.b, f.z, f.P * F.view.sa, -f.Q * F.view.Ya);
            f = c.Qy();
            c.X(f.x + d.x, f.y + d.y);
            this.Tc();
            F.ka.Rp(this.ma);
            c = 0;
            for (d = a.length; c < d; c++)a[c].H(e);
            F.ka.Ux(e, this.ma);
            this.pe = r
        }
    };
    ia.H = function (a) {
        if (this.il) {
            a = a || F.l;
            var c = this.u.length;
            this.Jc && 0 !== c && (this.transform(a), this.Jo && F.ka.xc(this.Jo), this.ym.H(a))
        } else F.n.prototype.H.call(this, a)
    };
    ia.jD = function () {
        var a =
            q;
        if (!this.u || 0 === this.u.length)return F.rect(0, 0, 10, 10);
        for (var c = this.u, d = 0; d < c.length; d++) {
            var e = c[d];
            e && e.Jc && (a ? (e = e.Js()) && (a = F.Vz(a, e)) : a = e.Js())
        }
        return a
    };
    ia = q
} else F.assert(F.ac(F.N.jB), F.i.Hd, "CCLayerWebGL.js"), F.N.jB(), delete F.N.jB;
F.Gd = F.ec.extend({
    q: q, Mb: "LayerColor", Jf: A("q"), Pda: function (a, c) {
        this.width = a;
        this.height = c
    }, Oda: z("width"), Nda: z("height"), Of: u(), ng: D(r), rb: function (a) {
        F.ec.prototype.rb.call(this, a);
        this.vc()
    }, wb: function (a) {
        F.ec.prototype.wb.call(this, a);
        this.vc()
    }, Mg: "source", ctor: q, oa: function (a, c, d) {
        F.B !== F.Aa && (this.shaderProgram = F.le.Kc(F.Xv));
        var e = F.L.Pa();
        a = a || F.color(0, 0, 0, 255);
        c = c === k ? e.width : c;
        d = d === k ? e.height : d;
        e = this.wa;
        e.r = a.r;
        e.g = a.g;
        e.b = a.b;
        e = this.te;
        e.r = a.r;
        e.g = a.g;
        e.b = a.b;
        this.Xg = this.ab = a.a;
        a = F.Gd.prototype;
        a.ze.call(this, c, d);
        a.vc.call(this);
        return p
    }, Vd: function (a, c) {
        var d = this.q;
        c === k ? (d.src = a.src, d.J = a.J) : (d.src = a, d.J = c);
        F.B === F.Aa && (this.Mg = F.XM(d))
    }, Xj: q, Wj: q, vc: q, ed: function (a) {
        F.ec.prototype.ed.call(this, a);
        this.vc()
    }, Uc: function (a) {
        F.ec.prototype.Uc.call(this, a);
        this.vc()
    }, Ka: q
});
F.Gd.create = function (a, c, d) {
    return new F.Gd(a, c, d)
};
F.B === F.Aa ? (M = F.Gd.prototype, M.ctor = function (a, c, d) {
    F.ec.prototype.ctor.call(this);
    this.q = new F.dc(F.Ac, F.zc);
    F.Gd.prototype.oa.call(this, a, c, d)
}, M.de = function () {
    this.Z = new F.OJ(this)
}, M.Xj = function (a) {
    F.n.prototype.Xj.call(this, a)
}, M.Wj = function (a) {
    F.n.prototype.Wj.call(this, a)
}, M.vc = function () {
    var a = this.Z;
    if (a && a.qe) {
        var c = this.wa;
        a.qe.r = c.r;
        a.qe.g = c.g;
        a.qe.b = c.b;
        a.qe.a = this.ab / 255
    }
}, M.Ka = function (a) {
    a = a || F.l;
    var c = F.view, d = this.wa;
    a.fillStyle = "rgba(" + (0 | d.r) + "," + (0 | d.g) + "," + (0 | d.b) + "," + this.ab /
        255 + ")";
    a.fillRect(0, 0, this.width * c.sa, -this.height * c.Ya);
    F.bd++
}, M.Gw = function () {
    if (this.pe) {
        var a = this.ym, c = this.u, d = c.length, e = this.jD();
        e.width |= 0;
        e.height |= 0;
        var f = a.oe;
        a.SR(e.width, e.height);
        var g = a.Qy(), h = this.xa;
        if (this.Jj)f.translate(0 - e.x + h.x, e.height + e.y - h.y), a.X(g.x + e.x - h.x, g.y + e.y - h.y); else {
            var m = this.Qy(), n = h.x - m.x, h = h.y - m.y;
            f.translate(0 - e.x + n, e.height + e.y - h);
            a.X(g.x + e.x - n, g.y + e.y - h)
        }
        a = F.Zp(this.Ad);
        f.transform(a.a, a.s, a.b, a.z, a.P * F.view.sa, -a.Q * F.view.Ya);
        F.ka.Rp(this.ma);
        if (0 < d) {
            this.Tc();
            for (a = 0; a < d; a++)if (e = c[a], 0 > e.Nb)e.H(f); else break;
            for (this.Z && F.ka.xc(this.Z); a < d; a++)c[a].H(f)
        } else this.Z && F.ka.xc(this.Z);
        F.ka.Ux(f, this.ma);
        this.pe = r
    }
}, M.H = function (a) {
    this.il ? (a = a || F.l, this.Jc && (this.transform(a), this.Jo && F.ka.xc(this.Jo), this.ym.H(a))) : F.n.prototype.H.call(this, a)
}, M.jD = function () {
    var a = F.rect(0, 0, this.S.width, this.S.height), c = this.sR(), a = F.BH(a, this.sR());
    if (!this.u || 0 === this.u.length)return a;
    for (var d = this.u, e = 0; e < d.length; e++) {
        var f = d[e];
        f && f.Jc && (f = f.Js(c), a = F.Vz(a, f))
    }
    return a
},
    M = q) : (F.assert(F.ac(F.N.rC), F.i.Hd, "CCLayerWebGL.js"), F.N.rC(), delete F.N.rC);
F.assert(F.ac(F.N.EB), F.i.Hd, "CCLayerPropertyDefine.js");
F.N.EB();
delete F.N.EB;
F.nm = F.Gd.extend({
    Ze: q, Jp: 255, $o: 255, Sk: q, Ow: r, Mb: "LayerGradient", ctor: function (a, c, d) {
        F.Gd.prototype.ctor.call(this);
        this.Ze = F.color(0, 0, 0, 255);
        this.Sk = F.d(0, -1);
        this.$o = this.Jp = 255;
        F.nm.prototype.oa.call(this, a, c, d)
    }, de: function () {
        this.Z = new F.YI(this)
    }, oa: function (a, c, d) {
        a = a || F.color(0, 0, 0, 255);
        c = c || F.color(0, 0, 0, 255);
        d = d || F.d(0, -1);
        var e = this.Ze;
        this.Jp = a.a;
        e.r = c.r;
        e.g = c.g;
        e.b = c.b;
        this.$o = c.a;
        this.Sk = d;
        this.Ow = p;
        F.Gd.prototype.oa.call(this, F.color(a.r, a.g, a.b, 255));
        F.nm.prototype.vc.call(this);
        return p
    }, ze: function (a, c) {
        F.Gd.prototype.ze.call(this, a, c);
        this.vc()
    }, Xj: function (a) {
        F.Gd.prototype.Xj.call(this, a);
        this.vc()
    }, Wj: function (a) {
        F.Gd.prototype.Wj.call(this, a);
        this.vc()
    }, Xy: A("te"), of: z("color"), nf: function (a) {
        this.Ze = a;
        this.vc()
    }, Ty: A("Ze"), L8: function (a) {
        this.Jp = a;
        this.vc()
    }, W4: A("Jp"), i8: function (a) {
        this.$o = a;
        this.vc()
    }, e4: A("$o"), U8: function (a) {
        this.Sk.x = a.x;
        this.Sk.y = a.y;
        this.vc()
    }, d5: function () {
        return F.d(this.Sk.x, this.Sk.y)
    }, Bia: A("Ow"), mna: function (a) {
        this.Ow = a;
        this.vc()
    },
    nca: q, vc: q
});
F.nm.create = function (a, c, d) {
    return new F.nm(a, c, d)
};
F.B === F.Aa ? (M = F.nm.prototype, M.vc = function () {
    var a = this.Sk, c = 0.5 * this.width, d = 0.5 * this.height, e = this.Z;
    e.Vi.x = c * -a.x + c;
    e.Vi.y = d * a.y - d;
    e.ax.x = c * a.x + c;
    e.ax.y = d * -a.y - d;
    a = this.wa;
    c = this.Ze;
    d = this.$o;
    e.KO = "rgba(" + Math.round(a.r) + "," + Math.round(a.g) + "," + Math.round(a.b) + "," + this.Jp.toFixed(4) + ")";
    e.JM = "rgba(" + Math.round(c.r) + "," + Math.round(c.g) + "," + Math.round(c.b) + "," + d.toFixed(4) + ")"
}, M = q) : (F.assert(F.ac(F.N.sC), F.i.Hd, "CCLayerWebGL.js"), F.N.sC(), delete F.N.sC);
F.assert(F.ac(F.N.FB), F.i.Hd, "CCLayerPropertyDefine.js");
F.N.FB();
delete F.N.FB;
F.Mv = F.ec.extend({
    Jm: 0, Ie: q, Mb: "LayerMultiplex", ctor: function (a) {
        F.ec.prototype.ctor.call(this);
        a instanceof Array ? F.Mv.prototype.RQ.call(this, a) : F.Mv.prototype.RQ.call(this, Array.prototype.slice.call(arguments))
    }, RQ: function (a) {
        0 < a.length && a[a.length - 1] == q && F.log(F.i.fV);
        this.Ie = a;
        this.Jm = 0;
        this.F(this.Ie[this.Jm]);
        return p
    }, tpa: function (a) {
        a >= this.Ie.length ? F.log(F.i.gV) : (this.removeChild(this.Ie[this.Jm], p), this.Jm = a, this.F(this.Ie[a]))
    }, upa: function (a) {
        a >= this.Ie.length ? F.log(F.i.hV) : (this.removeChild(this.Ie[this.Jm],
            p), this.Ie[this.Jm] = q, this.Jm = a, this.F(this.Ie[a]))
    }, fda: function (a) {
        a ? this.Ie.push(a) : F.log(F.i.eV)
    }
});
F.Mv.create = function () {
    return new F.Mv(Array.prototype.slice.call(arguments))
};
F.N.tC = function () {
    var a = F.I.prototype;
    a.$E = function (a) {
        this.Qa(p);
        this.Db(a.fi(), a.Le, a.zq());
        this.dispatchEvent("load")
    };
    a.Of = function (a) {
        this.qb !== a && (this.qb = a, this.av())
    };
    a.Uc = function (a) {
        F.n.prototype.Uc.call(this, a);
        this.av()
    };
    a.ctor = function (a, d, e) {
        F.n.prototype.ctor.call(this);
        this.tl = r;
        this.Ob = F.d(0, 0);
        this.Wh = F.d(0, 0);
        this.q = {src: F.Ac, J: F.zc};
        this.Ba = F.rect(0, 0, 0, 0);
        this.Ub = new F.Hb;
        this.Cp = F.l.createBuffer();
        this.Ca = this.Od = p;
        this.FO(a, d, e)
    };
    a.de = function () {
        this.Z = new F.rK(this)
    };
    a.Vd =
        function (a, d) {
            var e = this.q;
            d === k ? (e.src = a.src, e.J = a.J) : (e.src = a, e.J = d)
        };
    a.oa = function () {
        if (0 < arguments.length)return this.Nl(arguments[0], arguments[1]);
        F.n.prototype.oa.call(this);
        this.dirty = this.Qh = r;
        this.q.src = F.Ac;
        this.q.J = F.zc;
        this.texture = q;
        this.Ca = p;
        this.pc = this.qc = r;
        this.anchorY = this.anchorX = 0.5;
        this.Ob.x = 0;
        this.Ob.y = 0;
        this.Hi = r;
        var a = {r: 255, g: 255, b: 255, a: 255};
        this.Ub.K.A = a;
        this.Ub.V.A = a;
        this.Ub.U.A = a;
        this.Ub.O.A = a;
        this.Od = p;
        this.Db(F.rect(0, 0, 0, 0), r, F.size(0, 0));
        return p
    };
    a.Fa = function (a,
                     d, e) {
        F.assert(0 != arguments.length, F.i.XW);
        e = e || r;
        if (!F.n.prototype.oa.call(this))return r;
        this.ca = q;
        this.dirty = this.Qh = r;
        this.qb = p;
        this.q.src = F.Ac;
        this.q.J = F.zc;
        this.pc = this.qc = r;
        this.anchorY = this.anchorX = 0.5;
        this.Ob.x = 0;
        this.Ob.y = 0;
        this.Hi = r;
        var f = F.color(255, 255, 255, 255), g = this.Ub;
        g.K.A = f;
        g.V.A = f;
        g.U.A = f;
        g.O.A = f;
        this.Ca = f = a.Jb;
        if (!f)return this.Yc = e || r, d && (f = this.Ba, f.x = d.x, f.y = d.y, f.width = d.width, f.height = d.height), a.addEventListener("load", this.oy, this), p;
        d || (d = F.rect(0, 0, a.width, a.height));
        a &&
        a.url && (e ? (f = d.x + d.height, g = d.y + d.width) : (f = d.x + d.width, g = d.y + d.height), f > a.width && F.error(F.i.MB, a.url), g > a.height && F.error(F.i.KB, a.url));
        this.texture = a;
        this.Db(d, e);
        this.batchNode = q;
        return this.Od = p
    };
    a.oy = function (a) {
        if (!this.Ca) {
            this.Ca = p;
            var d = this.Ba;
            d ? F.sE(d) && (d.width = a.width, d.height = a.height) : d = F.rect(0, 0, a.width, a.height);
            this.texture = a;
            this.Db(d, this.Yc);
            this.batchNode = this.ca;
            this.Od = p;
            this.dispatchEvent("load")
        }
    };
    a.Db = function (a, d, e) {
        this.Yc = d || r;
        this.ze(e || a);
        this.aI(a);
        this.wO(a);
        a = this.Wh;
        this.pc && (a.x = -a.x);
        this.qc && (a.y = -a.y);
        var f = this.Ba;
        this.Ob.x = a.x + (this.S.width - f.width) / 2;
        this.Ob.y = a.y + (this.S.height - f.height) / 2;
        if (this.ca)this.dirty = p; else {
            a = this.Ob.x;
            d = this.Ob.y;
            e = a + f.width;
            var f = d + f.height, g = this.Ub;
            g.K.j = {x: a, y: d, e: 0};
            g.V.j = {x: e, y: d, e: 0};
            g.U.j = {x: a, y: f, e: 0};
            g.O.j = {x: e, y: f, e: 0};
            this.Od = p
        }
    };
    a.Ae = function () {
        if (this.dirty) {
            var a = this.Ub, d = this.Ja;
            if (!this.Jc || d && d != this.ca && d.tl)a.V.j = a.U.j = a.O.j = a.K.j = {
                x: 0,
                y: 0,
                e: 0
            }, this.tl = p; else {
                this.tl = r;
                var e = this.pn = !d || d == this.ca ?
                    this.Sl() : F.wn(this.Sl(), d.pn), f = this.Ba, d = this.Ob.x, g = this.Ob.y, h = d + f.width, m = g + f.height, n = e.P, s = e.Q, t = e.a, v = e.b, w = e.z, x = -e.s, e = d * t - g * x + n, f = d * v + g * w + s, y = h * t - g * x + n, g = h * v + g * w + s, B = h * t - m * x + n, h = h * v + m * w + s, n = d * t - m * x + n, d = d * v + m * w + s, m = this.Xp;
                F.$v || (e |= 0, f |= 0, y |= 0, g |= 0, B |= 0, h |= 0, n |= 0, d |= 0);
                a.K.j = {x: e, y: f, e: m};
                a.V.j = {x: y, y: g, e: m};
                a.U.j = {x: n, y: d, e: m};
                a.O.j = {x: B, y: h, e: m}
            }
            this.textureAtlas.bv(a, this.atlasIndex);
            this.dirty = this.Qh = r
        }
        this.Hi && this.Tf(this.u, F.n.vf.Ae);
        F.Bo && (a = [F.d(this.Ub.K.j.x, this.Ub.K.j.y), F.d(this.Ub.V.j.x,
            this.Ub.V.j.y), F.d(this.Ub.O.j.x, this.Ub.O.j.y), F.d(this.Ub.U.j.x, this.Ub.U.j.y)], F.Ge.ye(a, 4, p))
    };
    a.F = function (a, d, e) {
        F.assert(a, F.i.OW);
        d == q && (d = a.Nb);
        e == q && (e = a.tag);
        if (this.ca) {
            if (!(a instanceof F.I)) {
                F.log(F.i.MW);
                return
            }
            a.texture.ve !== this.textureAtlas.texture.ve && F.log(F.i.NW);
            this.ca.appendChild(a);
            this.ue || this.RE()
        }
        F.n.prototype.F.call(this, a, d, e);
        this.Hi = p
    };
    a.wb = function (a) {
        F.n.prototype.wb.call(this, a);
        this.av()
    };
    a.rb = function (a) {
        F.n.prototype.rb.call(this, a);
        this.av()
    };
    a.ed = function (a) {
        F.n.prototype.ed.call(this,
            a);
        this.av()
    };
    a.bm = function (a) {
        var d = this;
        F.Dd(a) && (a = F.cm.jk(a), F.assert(a, F.i.fX));
        d.Qa(p);
        var e = a.ik();
        d.Wh.x = e.x;
        d.Wh.y = e.y;
        e = a.Ma();
        a.Ca || (d.Ca = r, a.addEventListener("load", function (a) {
            d.Ca = p;
            var c = a.Ma();
            c != d.G && (d.texture = c);
            d.Db(a.fi(), a.Le, a.zq());
            d.dispatchEvent("load")
        }, d));
        e != d.G && (d.texture = e);
        d.Yc = a.Le;
        d.Db(a.fi(), d.Yc, a.zq())
    };
    a.aR = function (a) {
        return F.LR(a.fi(), this.Ba) && a.Ma().getName() == this.G.getName() && F.Tz(a.ik(), this.Wh)
    };
    a.$l = function (a) {
        if (this.ca = a)this.pn = {
            a: 1, b: 0, s: 0, z: 1,
            P: 0, Q: 0
        }, this.textureAtlas = this.ca.textureAtlas; else {
            this.atlasIndex = F.I.fB;
            this.textureAtlas = q;
            this.dirty = this.Qh = r;
            a = this.Ob.x;
            var d = this.Ob.y, e = a + this.Ba.width, f = d + this.Ba.height, g = this.Ub;
            g.K.j = {x: a, y: d, e: 0};
            g.V.j = {x: e, y: d, e: 0};
            g.U.j = {x: a, y: f, e: 0};
            g.O.j = {x: e, y: f, e: 0};
            this.Od = p
        }
    };
    a.ib = function (a) {
        var d = this;
        if (a && F.Dd(a)) {
            a = F.Ra.ad(a);
            d.ib(a);
            var e = a.$();
            d.Db(F.rect(0, 0, e.width, e.height));
            a.Jb || a.addEventListener("load", function () {
                var e = a.$();
                d.Db(F.rect(0, 0, e.width, e.height))
            }, this)
        } else F.assert(!a ||
            a instanceof F.ha, F.i.hX), d.ca && d.ca.texture != a ? F.log(F.i.gX) : (d.shaderProgram = a ? F.le.Kc(F.yj) : F.le.Kc(F.Xv), !d.ca && d.G != a && (d.G = a, d.ig()))
    };
    a.Ka = function () {
        if (this.Ca) {
            var a = F.l, d = this.G;
            d ? d.Jb && (this.na.xb(), this.na.qf(), F.pd(this.q.src, this.q.J), F.bz(0, d), F.Xb(F.Bh), a.bindBuffer(a.ARRAY_BUFFER, this.Cp), this.Od && (a.bufferData(a.ARRAY_BUFFER, this.Ub.cq, a.DYNAMIC_DRAW), this.Od = r), a.vertexAttribPointer(0, 3, a.FLOAT, r, 24, 0), a.vertexAttribPointer(1, 4, a.UNSIGNED_BYTE, p, 24, 12), a.vertexAttribPointer(2, 2,
                a.FLOAT, r, 24, 16), a.drawArrays(a.TRIANGLE_STRIP, 0, 4)) : (this.na.xb(), this.na.qf(), F.pd(this.q.src, this.q.J), F.Cd(q), F.Xb(F.Jd | F.Do), a.bindBuffer(a.ARRAY_BUFFER, this.Cp), this.Od && (F.l.bufferData(F.l.ARRAY_BUFFER, this.Ub.cq, F.l.STATIC_DRAW), this.Od = r), a.vertexAttribPointer(F.kb, 3, a.FLOAT, r, 24, 0), a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, 24, 12), a.drawArrays(a.TRIANGLE_STRIP, 0, 4));
            F.bd++;
            if (0 !== F.Bo || this.DO)1 === F.Bo || this.DO ? (a = this.Ub, a = [F.d(a.U.j.x, a.U.j.y), F.d(a.K.j.x, a.K.j.y), F.d(a.V.j.x, a.V.j.y),
                F.d(a.O.j.x, a.O.j.y)], F.Ge.ye(a, 4, p)) : 2 === F.Bo && (a = this.CQ(), d = this.D4(), a = [F.d(d.x, d.y), F.d(d.x + a.width, d.y), F.d(d.x + a.width, d.y + a.height), F.d(d.x, d.y + a.height)], F.Ge.ye(a, 4, p))
        }
    };
    delete a
};
F.N.GB = function () {
    var a = F.I.prototype;
    F.k(a, "opacityModifyRGB", a.ng, a.Of);
    F.k(a, "opacity", a.jh, a.wb);
    F.k(a, "color", a.mg, a.rb);
    F.k(a, "flippedX", a.H5, a.gA);
    F.k(a, "flippedY", a.I5, a.RH);
    F.k(a, "offsetX", a.UZ);
    F.k(a, "offsetY", a.VZ);
    F.k(a, "texture", a.Ma, a.ib);
    F.k(a, "textureRectRotated", a.O5);
    F.k(a, "batchNode", a.vG, a.$l);
    F.k(a, "quad", a.K4)
};
F.Mn = function (a, c, d, e) {
    e = e || F.bc("canvas");
    d = d || F.rect(0, 0, a.width, a.height);
    var f = e.getContext("2d");
    e.width != d.width || e.height != d.height ? (e.width = d.width, e.height = d.height) : f.globalCompositeOperation = "source-over";
    f.fillStyle = "rgb(" + (0 | c.r) + "," + (0 | c.g) + "," + (0 | c.b) + ")";
    f.fillRect(0, 0, d.width, d.height);
    f.globalCompositeOperation = "multiply";
    f.drawImage(a, d.x, d.y, d.width, d.height, 0, 0, d.width, d.height);
    f.globalCompositeOperation = "destination-atop";
    f.drawImage(a, d.x, d.y, d.width, d.height, 0, 0, d.width, d.height);
    return e
};
F.Il = function (a, c, d, e, f) {
    e || (e = F.rect(0, 0, a.width, a.height));
    a = d.r / 255;
    var g = d.g / 255;
    d = d.b / 255;
    var h = Math.min(e.width, c[0].width), m = Math.min(e.height, c[0].height), n;
    f ? (n = f.getContext("2d"), n.clearRect(0, 0, h, m)) : (f = F.bc("canvas"), f.width = h, f.height = m, n = f.getContext("2d"));
    n.save();
    n.globalCompositeOperation = "lighter";
    var s = n.globalAlpha;
    0 < a && (n.globalAlpha = a * s, n.drawImage(c[0], e.x, e.y, h, m, 0, 0, h, m));
    0 < g && (n.globalAlpha = g * s, n.drawImage(c[1], e.x, e.y, h, m, 0, 0, h, m));
    0 < d && (n.globalAlpha = d * s, n.drawImage(c[2], e.x,
        e.y, h, m, 0, 0, h, m));
    1 > a + g + d && (n.globalAlpha = s, n.drawImage(c[3], e.x, e.y, h, m, 0, 0, h, m));
    n.restore();
    return f
};
F.uq = function (a) {
    function c() {
        var c = F.uq, e = a.width, h = a.height;
        d[0].width = e;
        d[0].height = h;
        d[1].width = e;
        d[1].height = h;
        d[2].width = e;
        d[2].height = h;
        d[3].width = e;
        d[3].height = h;
        c.canvas.width = e;
        c.canvas.height = h;
        var m = c.canvas.getContext("2d");
        m.drawImage(a, 0, 0);
        c.jI.width = e;
        c.jI.height = h;
        for (var m = m.getImageData(0, 0, e, h).data, n = 0; 4 > n; n++) {
            var s = d[n].getContext("2d");
            s.getImageData(0, 0, e, h).data;
            c.PS.drawImage(a, 0, 0);
            for (var t = c.PS.getImageData(0, 0, e, h), v = t.data, w = 0; w < m.length; w += 4)v[w] = 0 === n ? m[w] : 0, v[w +
            1] = 1 === n ? m[w + 1] : 0, v[w + 2] = 2 === n ? m[w + 2] : 0, v[w + 3] = m[w + 3];
            s.putImageData(t, 0, 0)
        }
        a.onload = q
    }

    if (a.qP)return a.qP;
    var d = [F.bc("canvas"), F.bc("canvas"), F.bc("canvas"), F.bc("canvas")];
    try {
        c()
    } catch (e) {
        a.onload = c
    }
    return a.qP = d
};
F.uq.canvas = F.bc("canvas");
F.uq.jI = F.bc("canvas");
F.uq.PS = F.uq.jI.getContext("2d");
F.cG = function (a, c) {
    if (!a)return q;
    if (!c)return a;
    var d = F.bc("canvas");
    d.width = c.width;
    d.height = c.height;
    var e = d.getContext("2d");
    e.translate(d.width / 2, d.height / 2);
    e.rotate(-1.5707963267948966);
    e.drawImage(a, c.x, c.y, c.height, c.width, -c.height / 2, -c.width / 2, c.height, c.width);
    return d
};
F.XM = function (a) {
    return a ? a.src == F.SRC_ALPHA && a.J == F.ONE || a.src == F.ONE && a.J == F.ONE ? "lighter" : a.src == F.ZERO && a.J == F.SRC_ALPHA ? "destination-in" : a.src == F.ZERO && a.J == F.ONE_MINUS_SRC_ALPHA ? "destination-out" : "source" : "source"
};
F.I = F.n.extend({
    dirty: r,
    atlasIndex: 0,
    textureAtlas: q,
    ca: q,
    Qh: q,
    Hi: q,
    tl: r,
    pn: q,
    q: q,
    G: q,
    Ba: q,
    Yc: r,
    Ob: q,
    Wh: q,
    qb: r,
    pc: r,
    qc: r,
    Ca: r,
    Cx: q,
    Mb: "Sprite",
    KN: F.color.WHITE,
    Vu: A("Ca"),
    Ft: function (a, c) {
        this.addEventListener("load", a, c)
    },
    cH: A("dirty"),
    gS: z("dirty"),
    O5: A("Yc"),
    wq: A("atlasIndex"),
    LH: z("atlasIndex"),
    CQ: function () {
        return F.rect(this.Ba)
    },
    Yy: A("textureAtlas"),
    YH: z("textureAtlas"),
    D4: function () {
        return F.d(this.Ob)
    },
    UZ: function () {
        return this.Ob.x
    },
    VZ: function () {
        return this.Ob.y
    },
    Jf: A("q"),
    fj: function (a) {
        F.assert(a,
            F.i.UW);
        a.Ca || (this.Ca = r, a.addEventListener("load", this.$E, this));
        var c = F.B === F.Aa ? r : a.Le, c = this.Fa(a.Ma(), a.fi(), c);
        this.bm(a);
        return c
    },
    $E: q,
    ria: function (a) {
        F.assert(a, F.i.VW);
        var c = F.cm.jk(a);
        F.assert(c, a + F.i.WW);
        return this.fj(c)
    },
    Kpa: function (a) {
        this.textureAtlas = a.textureAtlas;
        this.ca = a
    },
    aI: function (a) {
        var c = this.Ba;
        c.x = a.x;
        c.y = a.y;
        c.width = a.width;
        c.height = a.height
    },
    Tc: function () {
        if (this.ue) {
            var a = this.u, c = a.length, d, e, f;
            for (d = 1; d < c; d++) {
                f = a[d];
                for (e = d - 1; 0 <= e;) {
                    if (f.Nb < a[e].Nb)a[e + 1] = a[e]; else if (f.Nb ===
                        a[e].Nb && f.arrivalOrder < a[e].arrivalOrder)a[e + 1] = a[e]; else break;
                    e--
                }
                a[e + 1] = f
            }
            this.ca && this.Tf(a, F.n.vf.Tc);
            this.ue = r
        }
    },
    Wq: function (a, c) {
        F.assert(a, F.i.aX);
        -1 === this.u.indexOf(a) ? F.log(F.i.$W) : c !== a.zIndex && (this.ca && !this.ue && (this.RE(), this.ca.QR(p)), F.n.prototype.Wq.call(this, a, c))
    },
    removeChild: function (a, c) {
        this.ca && this.ca.Zn(a);
        F.n.prototype.removeChild.call(this, a, c)
    },
    ke: function (a) {
        F.n.prototype.ke.call(this, a);
        this.PH(p)
    },
    Mf: function (a) {
        var c = this.u, d = this.ca;
        if (d && c != q)for (var e = 0, f = c.length; e <
        f; e++)d.Zn(c[e]);
        F.n.prototype.Mf.call(this, a);
        this.Hi = r
    },
    PH: function (a) {
        this.dirty = this.Qh = a;
        a = this.u;
        for (var c, d = a ? a.length : 0, e = 0; e < d; e++)c = a[e], c instanceof F.I && c.PH(p)
    },
    Qa: function (a) {
        F.n.prototype.Qa.call(this);
        !a && (this.ca && !this.Qh) && (this.Hi ? this.PH(p) : this.dirty = this.Qh = p)
    },
    hz: function (a) {
        this.ca ? F.log(F.i.SW) : F.n.prototype.hz.call(this, a)
    },
    gA: function (a) {
        this.pc != a && (this.pc = a, this.Db(this.Ba, this.Yc, this.S), this.Qa(p))
    },
    RH: function (a) {
        this.qc != a && (this.qc = a, this.Db(this.Ba, this.Yc, this.S),
            this.Qa(p))
    },
    H5: A("pc"),
    I5: A("qc"),
    Of: q,
    ng: A("qb"),
    Uc: q,
    wna: function (a, c) {
        F.assert(a, F.i.eX);
        var d = F.By.bQ(a);
        d ? (d = d.se[c]) ? this.bm(d.jk()) : F.log(F.i.dX) : F.log(F.i.cX)
    },
    vG: A("ca"),
    RE: function () {
        if (!this.ue) {
            this.ue = p;
            for (var a = this.Ja; a && a != this.ca;)a.RE(), a = a.parent
        }
    },
    Ma: A("G"),
    Ub: q,
    Cp: q,
    Od: r,
    ss: r,
    Mg: "source",
    Fc: q,
    bM: q,
    ctor: q,
    FO: function (a, c, d) {
        if (a === k)F.I.prototype.oa.call(this); else if (F.Dd(a))"#" === a[0] ? (a = F.cm.jk(a.substr(1, a.length - 1)), this.fj(a)) : F.I.prototype.oa.call(this, a, c); else if (F.Gq(a))if (a instanceof F.ha)this.Fa(a, c, d); else if (a instanceof F.Ce)this.fj(a); else if (a instanceof HTMLImageElement || a instanceof HTMLCanvasElement)c = new F.ha, c.qd(a), c.Lb(), this.Fa(c)
    },
    K4: A("Ub"),
    Vd: q,
    oa: q,
    Nl: function (a, c) {
        F.assert(a, F.i.TW);
        var d = F.Ra.Ml(a);
        if (d) {
            if (!c) {
                var e = d.$();
                c = F.rect(0, 0, e.width, e.height)
            }
            return this.Fa(d, c)
        }
        d = F.Ra.ad(a);
        return this.Fa(d, c || F.rect(0, 0, d.S.width, d.S.height))
    },
    Fa: q,
    oy: q,
    Db: q,
    Ae: q,
    F: q,
    av: function () {
        var a = this.wa, c = this.ab, a = {r: a.r, g: a.g, b: a.b, a: c};
        this.qb && (a.r *= c / 255, a.g *= c / 255,
            a.b *= c / 255);
        c = this.Ub;
        c.K.A = a;
        c.V.A = a;
        c.U.A = a;
        c.O.A = a;
        this.ca && (this.atlasIndex != F.I.fB ? this.textureAtlas.bv(c, this.atlasIndex) : this.dirty = p);
        this.Od = p
    },
    wb: q,
    rb: q,
    ed: q,
    bm: q,
    f8: function (a) {
        F.log(F.i.bX);
        this.bm(a)
    },
    aR: q,
    a3: function () {
        return new F.Ce(this.G, F.Hu(this.Ba), this.Yc, F.f7(this.Wh), F.g9(this.S))
    },
    $l: q,
    ib: q,
    ig: function () {
        this.ca ? F.log(F.i.LW) : !this.G || !this.G.gi() ? (this.q.src = F.SRC_ALPHA, this.q.J = F.ONE_MINUS_SRC_ALPHA, this.opacityModifyRGB = r) : (this.q.src = F.Ac, this.q.J = F.zc, this.opacityModifyRGB =
            p)
    },
    De: function () {
        var a, c = this.G, d = this.Z.Yi;
        if (c && (d.gm && this.Fc) && (a = c.Xa))this.ss = p, a instanceof HTMLCanvasElement && !this.Yc && !this.Cx && this.Fc.Xa != a ? F.Mn(this.Fc.Xa, this.wa, d, a) : (a = F.Mn(this.Fc.Xa, this.wa, d), c = new F.ha, c.qd(a), c.Lb(), this.texture = c)
    },
    wO: function (a) {
        a = F.Hu(a);
        var c = this.ca ? this.textureAtlas.texture : this.G;
        if (c) {
            var d = c.pixelsWidth, e = c.pixelsHeight, f, g = this.Ub;
            this.Yc ? (F.ro ? (c = (2 * a.x + 1) / (2 * d), d = c + (2 * a.height - 2) / (2 * d), f = (2 * a.y + 1) / (2 * e), a = f + (2 * a.width - 2) / (2 * e)) : (c = a.x / d, d = (a.x + a.height) /
                d, f = a.y / e, a = (a.y + a.width) / e), this.pc && (e = f, f = a, a = e), this.qc && (e = c, c = d, d = e), g.K.p.pa = c, g.K.p.qa = f, g.V.p.pa = c, g.V.p.qa = a, g.U.p.pa = d, g.U.p.qa = f, g.O.p.pa = d, g.O.p.qa = a) : (F.ro ? (c = (2 * a.x + 1) / (2 * d), d = c + (2 * a.width - 2) / (2 * d), f = (2 * a.y + 1) / (2 * e), a = f + (2 * a.height - 2) / (2 * e)) : (c = a.x / d, d = (a.x + a.width) / d, f = a.y / e, a = (a.y + a.height) / e), this.pc && (e = c, c = d, d = e), this.qc && (e = f, f = a, a = e), g.K.p.pa = c, g.K.p.qa = a, g.V.p.pa = d, g.V.p.qa = a, g.U.p.pa = c, g.U.p.qa = f, g.O.p.pa = d, g.O.p.qa = f);
            this.Od = p
        }
    }
});
F.I.create = function (a, c, d) {
    return new F.I(a, c, d)
};
F.I.aG = F.I.create;
F.I.dea = F.I.create;
F.I.cea = F.I.create;
F.I.fB = -1;
F.B === F.Aa ? (M = F.I.prototype, M.$E = function (a) {
    this.Qa(p);
    this.Db(a.fi(), a.Le, a.zq());
    a = this.color;
    (255 !== a.r || 255 !== a.g || 255 !== a.b) && this.De();
    this.dispatchEvent("load")
}, M.Of = function (a) {
    this.qb !== a && (this.qb = a, this.Qa(p))
}, M.Uc = function (a) {
    F.n.prototype.Uc.call(this, a);
    this.Zg()
}, M.ctor = function (a, c, d) {
    F.n.prototype.ctor.call(this);
    this.tl = r;
    this.Ob = F.d(0, 0);
    this.Wh = F.d(0, 0);
    this.q = {src: F.Ac, J: F.zc};
    this.Ba = F.rect(0, 0, 0, 0);
    this.Cx = r;
    this.Ca = p;
    this.bM = F.size(0, 0);
    this.FO(a, c, d)
}, M.de = function () {
    this.Z =
        new F.qK(this)
}, M.Vd = function (a, c) {
    var d = this.q;
    c === k ? (d.src = a.src, d.J = a.J) : (d.src = a, d.J = c);
    F.B === F.Aa && (this.Mg = F.XM(d))
}, M.oa = function () {
    if (0 < arguments.length)return this.Nl(arguments[0], arguments[1]);
    F.n.prototype.oa.call(this);
    this.dirty = this.Qh = r;
    this.q.src = F.Ac;
    this.q.J = F.zc;
    this.texture = q;
    this.Ca = p;
    this.pc = this.qc = r;
    this.anchorY = this.anchorX = 0.5;
    this.Ob.x = 0;
    this.Ob.y = 0;
    this.Hi = r;
    this.Db(F.rect(0, 0, 0, 0), r, F.size(0, 0));
    return p
}, M.Fa = function (a, c, d) {
    F.assert(0 != arguments.length, F.i.IT);
    if ((d =
            d || r) && a.Jb) {
        var e = a.Xa, e = F.cG(e, c), f = new F.ha;
        f.qd(e);
        f.Lb();
        a = f;
        this.Ba = F.rect(0, 0, c.width, c.height)
    }
    if (!F.n.prototype.oa.call(this))return r;
    this.ca = q;
    this.dirty = this.Qh = r;
    this.qb = p;
    this.q.src = F.Ac;
    this.q.J = F.zc;
    this.pc = this.qc = r;
    this.anchorY = this.anchorX = 0.5;
    this.Ob.x = 0;
    this.Ob.y = 0;
    this.Hi = r;
    this.Ca = e = a.Jb;
    if (!e)return this.Yc = d, c && (this.Ba.x = c.x, this.Ba.y = c.y, this.Ba.width = c.width, this.Ba.height = c.height), this.texture && this.texture.removeEventListener("load", this), a.addEventListener("load", this.oy,
        this), this.texture = a, p;
    c || (c = F.rect(0, 0, a.width, a.height));
    a && a.url && (e = c.y + c.height, c.x + c.width > a.width && F.error(F.i.MB, a.url), e > a.height && F.error(F.i.KB, a.url));
    this.texture = this.Fc = a;
    this.Db(c, d);
    this.batchNode = q;
    return p
}, M.oy = function (a) {
    if (!this.Ca) {
        this.Ca = p;
        var c = this.Ba;
        c ? F.sE(c) && (c.width = a.width, c.height = a.height) : c = F.rect(0, 0, a.width, a.height);
        this.texture = this.Fc = a;
        this.Db(c, this.Yc);
        a = this.wa;
        (255 != a.r || 255 != a.g || 255 != a.b) && this.De();
        this.batchNode = this.ca;
        this.dispatchEvent("load")
    }
},
    M.Db = function (a, c, d) {
        this.Yc = c || r;
        this.ze(d || a);
        this.aI(a);
        c = this.Z.Yi;
        d = F.Fb();
        c.Uq = c.x = 0 | a.x * d;
        c.Vq = c.y = 0 | a.y * d;
        c.width = 0 | a.width * d;
        c.height = 0 | a.height * d;
        c.gm = !(0 === c.width || 0 === c.height || 0 > c.x || 0 > c.y);
        a = this.Wh;
        this.pc && (a.x = -a.x);
        this.qc && (a.y = -a.y);
        this.Ob.x = a.x + (this.S.width - this.Ba.width) / 2;
        this.Ob.y = a.y + (this.S.height - this.Ba.height) / 2;
        this.ca && (this.dirty = p)
    }, M.Ae = function () {
    if (this.dirty) {
        var a = this.Ja;
        !this.Jc || a && a != this.ca && a.tl ? this.tl = p : (this.tl = r, this.pn = !a || a == this.ca ? this.Sl() : F.wn(this.Sl(),
            a.pn));
        this.dirty = this.Qh = r
    }
    this.Hi && this.Tf(this.u, F.n.vf.Ae)
}, M.F = function (a, c, d) {
    F.assert(a, F.i.GT);
    c == q && (c = a.Nb);
    d == q && (d = a.tag);
    F.n.prototype.F.call(this, a, c, d);
    this.Hi = p
}, M.wb = function (a) {
    F.n.prototype.wb.call(this, a);
    this.Zg()
}, M.rb = function (a) {
    var c = this.color;
    this.KN = c;
    c.r === a.r && c.g === a.g && c.b === a.b || F.n.prototype.rb.call(this, a)
}, M.ed = function (a) {
    F.n.prototype.ed.call(this, a);
    a = this.KN;
    var c = this.wa;
    a.r === c.r && a.g === c.g && a.b === c.b || (this.De(), this.Zg())
}, M.bm = function (a) {
    var c = this;
    F.Dd(a) &&
    (a = F.cm.jk(a), F.assert(a, F.i.LT));
    c.Qa(p);
    var d = a.ik();
    c.Wh.x = d.x;
    c.Wh.y = d.y;
    c.Yc = a.Le;
    var d = a.Ma(), e = a.Ca;
    e || (c.Ca = r, a.addEventListener("load", function (a) {
        c.Ca = p;
        var d = a.Ma();
        d != c.G && (c.texture = d);
        c.Db(a.fi(), a.Le, a.zq());
        c.dispatchEvent("load")
    }, c));
    d != c.G && (c.texture = d);
    c.Yc && (c.Fc = d);
    c.Db(a.fi(), c.Yc, a.zq());
    c.ss = r;
    c.Z.Yi.Uq = c.Z.Yi.x;
    c.Z.Yi.Vq = c.Z.Yi.y;
    e && (a = c.color, (255 !== a.r || 255 !== a.g || 255 !== a.b) && c.De())
}, M.aR = function (a) {
    return a.Ma() != this.G ? r : F.LR(a.fi(), this.Ba)
}, M.$l = function (a) {
    (this.ca =
        a) ? (this.pn = {
        a: 1,
        b: 0,
        s: 0,
        z: 1,
        P: 0,
        Q: 0
    }, this.textureAtlas = this.ca.textureAtlas) : (this.atlasIndex = F.I.fB, this.textureAtlas = q, this.dirty = this.Qh = r)
}, M.ib = function (a) {
    var c = this;
    if (a && F.Dd(a)) {
        a = F.Ra.ad(a);
        c.ib(a);
        var d = a.$();
        c.Db(F.rect(0, 0, d.width, d.height));
        a.Jb || a.addEventListener("load", function () {
            var d = a.$();
            c.Db(F.rect(0, 0, d.width, d.height))
        }, this)
    } else F.assert(!a || a instanceof F.ha, F.i.MT), c.G != a && (a && a.Xa instanceof HTMLImageElement && (c.Fc = a), c.G = a)
}, F.ta.hy || (M.De = function () {
    var a, c = this.G,
        d = this.Z.Yi;
    if (c && (d.gm && this.Fc) && (a = c.Xa))if (c = F.Ra.Vt(this.Fc.Xa))this.ss = p, a instanceof HTMLCanvasElement && !this.Yc && !this.Cx ? F.Il(a, c, this.wa, d, a) : (a = F.Il(a, c, this.wa, d), c = new F.ha, c.qd(a), c.Lb(), this.texture = c)
}), M = q) : (F.assert(F.ac(F.N.tC), F.i.Hd, "SpritesWebGL.js"), F.N.tC(), delete F.N.tC);
F.Ah.prototype.apply(F.I.prototype);
F.assert(F.ac(F.N.GB), F.i.Hd, "SpritesPropertyDefine.js");
F.N.GB();
delete F.N.GB;
F.PA = 29;
F.We = F.n.extend({
    textureAtlas: q, q: q, ce: q, Mb: "SpriteBatchNode", X1: function (a, c, d) {
        F.assert(a, F.i.FW);
        if (!(a instanceof F.I))return F.log(F.i.EW), q;
        a.atlasIndex = c;
        var e = 0, f = this.ce;
        if (f && 0 < f.length)for (var g = 0; g < f.length; g++) {
            var h = f[g];
            h && h.atlasIndex >= c && ++e
        }
        f.splice(e, 0, a);
        F.n.prototype.F.call(this, a, c, d);
        this.QR(r);
        return this
    }, Yy: A("textureAtlas"), YH: function (a) {
        a != this.textureAtlas && (this.textureAtlas = a)
    }, Z3: A("ce"), Nl: function (a, c) {
        var d = F.Ra.Ml(a);
        d || (d = F.Ra.ad(a));
        return this.Fa(d, c)
    }, Zg: function () {
        this.pe =
            p
    }, oa: function (a, c) {
        var d = F.Ra.Ml(a);
        d || (d = F.Ra.ad(a));
        return this.Fa(d, c)
    }, kz: function () {
        var a = this.textureAtlas.capacity, c = Math.floor(4 * (a + 1) / 3);
        F.log(F.i.GW, a, c);
        this.textureAtlas.Zz(c) || F.log(F.i.HW)
    }, w7: function (a, c) {
        this.removeChild(this.u[a], c)
    }, JR: function (a, c) {
        var d = a.children;
        if (d && 0 < d.length)for (var e = 0; e < d.length; e++) {
            var f = d[e];
            f && 0 > f.zIndex && (c = this.JR(f, c))
        }
        !a == this && (a.atlasIndex = c, c++);
        if (d && 0 < d.length)for (e = 0; e < d.length; e++)(f = d[e]) && 0 <= f.zIndex && (c = this.JR(f, c));
        return c
    }, SG: function (a) {
        var c =
            a.children;
        return !c || 0 == c.length ? a.atlasIndex : this.SG(c[c.length - 1])
    }, D6: function (a) {
        var c = a.children;
        return !c || 0 == c.length ? a.atlasIndex : this.D6(c[c.length - 1])
    }, s2: function (a, c) {
        var d = a.parent, e = d.children, f = e.indexOf(a), g = q;
        0 < f && f < F.ZK && (g = e[f - 1]);
        return d == this ? 0 == f ? 0 : this.SG(g) + 1 : 0 == f ? 0 > c ? d.atlasIndex : d.atlasIndex + 1 : 0 > g.zIndex && 0 > c || 0 <= g.zIndex && 0 <= c ? this.SG(g) + 1 : d.atlasIndex + 1
    }, QR: z("ue"), Vd: function (a, c) {
        this.q = c === k ? a : {src: a, J: c}
    }, Jf: A("q"), Wq: function (a, c) {
        F.assert(a, F.i.KW);
        -1 === this.u.indexOf(a) ?
            F.log(F.i.JW) : c !== a.zIndex && (F.n.prototype.Wq.call(this, a, c), this.Qa())
    }, removeChild: function (a, c) {
        a != q && (-1 === this.u.indexOf(a) ? F.log(F.i.IW) : (this.Zn(a), F.n.prototype.removeChild.call(this, a, c)))
    }, Zi: q, ada: r, Fc: q, ctor: q, us: function (a, c) {
        F.n.prototype.ctor.call(this);
        var d;
        c = c || F.PA;
        F.Dd(a) ? (d = F.Ra.Ml(a)) || (d = F.Ra.ad(a)) : a instanceof F.ha && (d = a);
        d && this.Fa(d, c)
    }, vs: function (a, c) {
        F.n.prototype.ctor.call(this);
        var d;
        c = c || F.PA;
        F.Dd(a) ? (d = F.Ra.Ml(a)) || (d = F.Ra.ad(a)) : a instanceof F.ha && (d = a);
        d && this.Fa(d,
            c)
    }, de: function () {
        F.B === F.Y && (this.Z = new F.SB(this))
    }, dT: q, J1: function (a, c) {
        F.assert(a, F.i.NT);
        a instanceof F.I ? (a.batchNode = this, a.atlasIndex = c, a.dirty = p, a.Ae()) : F.log(F.i.LA)
    }, K1: function (a, c) {
        F.assert(a, F.i.LA);
        if (a instanceof F.I) {
            for (var d = this.textureAtlas.capacity; c >= d || d == this.textureAtlas.totalQuads;)this.kz();
            a.batchNode = this;
            a.atlasIndex = c;
            a.dirty = p;
            a.Ae()
        } else F.log(F.i.LA)
    }, xl: function (a, c) {
        var d = this.ce, e = this.textureAtlas, f = e.quads, g = d[a], h = F.aL(f[a]);
        d[c].atlasIndex = a;
        d[a] = d[c];
        e.bv(f[c],
            a);
        d[c] = g;
        e.bv(h, c)
    }, pz: q, v_: function (a, c) {
        F.assert(a, F.i.KT);
        a instanceof F.I ? (a.batchNode = this, a.atlasIndex = c, a.dirty = p, a.Ae(), a.ht(this), this.u.splice(c, 0, a)) : F.log(F.i.JT)
    }, w_: function (a, c) {
        F.assert(a, F.i.ZW);
        if (a instanceof F.I) {
            for (var d = this.textureAtlas; c >= d.capacity || d.capacity === d.totalQuads;)this.kz();
            a.batchNode = this;
            a.atlasIndex = c;
            d.aH(a.quad, c);
            a.dirty = p;
            a.Ae()
        } else F.log(F.i.YW)
    }, XO: function (a, c) {
        var d = 0, e = a.children;
        e && (d = e.length);
        var f = 0;
        if (0 === d)f = a.atlasIndex, a.atlasIndex = c, a.arrivalOrder =
            0, f != c && this.xl(f, c), c++; else {
            f = p;
            0 <= e[0].zIndex && (f = a.atlasIndex, a.atlasIndex = c, a.arrivalOrder = 0, f != c && this.xl(f, c), c++, f = r);
            for (d = 0; d < e.length; d++) {
                var g = e[d];
                f && 0 <= g.zIndex && (f = a.atlasIndex, a.atlasIndex = c, a.arrivalOrder = 0, f != c && this.xl(f, c), c++, f = r);
                c = this.XO(g, c)
            }
            f && (f = a.atlasIndex, a.atlasIndex = c, a.arrivalOrder = 0, f != c && this.xl(f, c), c++)
        }
        return c
    }, ig: function () {
        this.textureAtlas.texture.gi() || (this.q.src = F.SRC_ALPHA, this.q.J = F.ONE_MINUS_SRC_ALPHA)
    }, Fa: q, zD: function (a) {
        this.u = [];
        this.ce = [];
        this.q =
            new F.dc(F.Ac, F.zc);
        this.Zi = this.Fc = a;
        return p
    }, AD: function (a, c) {
        this.u = [];
        this.ce = [];
        this.q = new F.dc(F.Ac, F.zc);
        c = c || F.PA;
        this.textureAtlas = new F.wi;
        this.textureAtlas.Fa(a, c);
        this.ig();
        this.shaderProgram = F.le.Kc(F.yj);
        return p
    }, $G: function (a, c) {
        a.batchNode = this;
        a.atlasIndex = c;
        a.dirty = p;
        var d = this.textureAtlas;
        d.totalQuads >= d.capacity && this.kz();
        d.aH(a.quad, c);
        this.ce.splice(c, 0, a);
        var d = c + 1, e = this.ce;
        if (e && 0 < e.length)for (; d < e.length; d++)e[d].atlasIndex++;
        var e = a.children, f;
        if (e) {
            d = 0;
            for (l = e.length ||
                0; d < l; d++)(f = e[d]) && this.$G(f, this.s2(f, f.zIndex))
        }
    }, appendChild: q, wY: function (a) {
        this.ue = p;
        a.batchNode = this;
        a.dirty = p;
        this.ce.push(a);
        a.atlasIndex = this.ce.length - 1;
        a = a.children;
        for (var c = 0, d = a.length || 0; c < d; c++)this.appendChild(a[c])
    }, xY: function (a) {
        this.ue = p;
        a.batchNode = this;
        a.dirty = p;
        this.ce.push(a);
        var c = this.ce.length - 1;
        a.atlasIndex = c;
        var d = this.textureAtlas;
        d.totalQuads == d.capacity && this.kz();
        d.aH(a.quad, c);
        a = a.children;
        c = 0;
        for (d = a.length || 0; c < d; c++)this.appendChild(a[c])
    }, Zn: q, u0: function (a) {
        a.batchNode =
            q;
        var c = this.ce, d = c.indexOf(a);
        if (-1 != d) {
            c.splice(d, 1);
            for (var e = c.length; d < e; ++d)c[d].atlasIndex--
        }
        if (a = a.children) {
            c = 0;
            for (d = a.length || 0; c < d; c++)a[c] && this.Zn(a[c])
        }
    }, v0: function (a) {
        this.textureAtlas.PR(a.atlasIndex);
        a.batchNode = q;
        var c = this.ce, d = c.indexOf(a);
        if (-1 != d) {
            c.splice(d, 1);
            for (var e = c.length; d < e; ++d)c[d].atlasIndex--
        }
        if (a = a.children) {
            c = 0;
            for (d = a.length || 0; c < d; c++)a[c] && this.Zn(a[c])
        }
    }, Ma: q, Ks: A("Zi"), sD: function () {
        return this.textureAtlas.texture
    }, ib: q, SE: function (a) {
        this.Zi = a;
        for (var c =
            this.u, d = 0; d < c.length; d++)c[d].texture = a
    }, TE: function (a) {
        this.textureAtlas.texture = a;
        this.ig()
    }, H: q, sn: function (a) {
        var c = a || F.l;
        if (this.Jc) {
            c.save();
            this.transform(a);
            var d = this.u;
            if (d) {
                this.Tc();
                for (a = 0; a < d.length; a++)d[a] && d[a].H(c)
            }
            c.restore()
        }
    }, Bt: function (a) {
        a = a || F.l;
        if (this.Jc) {
            var c = F.tb;
            c.stack.push(c.top);
            F.og(this.Pb, c.top);
            c.top = this.Pb;
            this.Tc();
            this.transform(a);
            this.Z && F.ka.xc(this.Z);
            c.top = c.stack.pop()
        }
    }, F: q, AC: function (a, c, d) {
        F.assert(a != q, F.i.HT);
        a instanceof F.I ? (c = c == q ? a.zIndex :
            c, d = d == q ? a.tag : d, F.n.prototype.F.call(this, a, c, d), this.appendChild(a), this.Qa()) : F.log(F.i.FT)
    }, rY: function (a, c, d) {
        F.assert(a != q, F.i.RW);
        a instanceof F.I ? a.texture != this.textureAtlas.texture ? F.log(F.i.QW) : (c = c == q ? a.zIndex : c, d = d == q ? a.tag : d, F.n.prototype.F.call(this, a, c, d), this.appendChild(a), this.Qa()) : F.log(F.i.PW)
    }, Mf: q, s0: function (a) {
        var c = this.ce;
        if (c && 0 < c.length)for (var d = 0, e = c.length; d < e; d++)c[d] && (c[d].batchNode = q);
        F.n.prototype.Mf.call(this, a);
        this.ce.length = 0
    }, t0: function (a) {
        var c = this.ce;
        if (c && 0 < c.length)for (var d = 0, e = c.length; d < e; d++)c[d] && (c[d].batchNode = q);
        F.n.prototype.Mf.call(this, a);
        this.ce.length = 0;
        this.textureAtlas.Tq()
    }, Tc: q, n1: function () {
        if (this.ue) {
            var a, c = 0, d = this.u, e = d.length, f;
            for (a = 1; a < e; a++) {
                var g = d[a], c = a - 1;
                for (f = d[c]; 0 <= c && (g.Nb < f.Nb || g.Nb == f.Nb && g.arrivalOrder < f.arrivalOrder);)d[c + 1] = f, c -= 1, f = d[c];
                d[c + 1] = g
            }
            0 < d.length && this.Tf(d, F.n.vf.Tc);
            this.ue = r
        }
    }, o1: function () {
        if (this.ue) {
            var a = this.u, c, d = 0, e = a.length, f;
            for (c = 1; c < e; c++) {
                var g = a[c], d = c - 1;
                for (f = a[d]; 0 <= d && (g.Nb <
                f.Nb || g.Nb == f.Nb && g.arrivalOrder < f.arrivalOrder);)a[d + 1] = f, d -= 1, f = a[d];
                a[d + 1] = g
            }
            if (0 < a.length) {
                this.Tf(a, F.n.vf.Tc);
                for (c = d = 0; c < a.length; c++)d = this.XO(a[c], d)
            }
            this.ue = r
        }
    }, Ka: q, Fj: function () {
        0 !== this.textureAtlas.totalQuads && (this.na.xb(), this.na.qf(), this.Tf(this.u, F.n.vf.Ae), F.pd(this.q.src, this.q.J), this.textureAtlas.Gl())
    }
});
M = F.We.prototype;
F.B === F.Y ? (M.ctor = M.vs, M.dT = M.K1, M.pz = M.w_, M.Fa = M.AD, M.appendChild = M.xY, M.Zn = M.v0, M.Ma = M.sD, M.ib = M.TE, M.H = M.Bt, M.F = M.rY, M.Mf = M.t0, M.Tc = M.o1, M.Ka = M.Fj) : (M.ctor = M.us, M.dT = M.J1, M.pz = M.v_, M.Fa = M.zD, M.appendChild = M.wY, M.Zn = M.u0, M.Ma = M.Ks, M.ib = M.SE, M.H = M.sn, M.Mf = M.s0, M.F = M.AC, M.Tc = M.n1, M.Ka = F.n.prototype.Ka);
F.k(M, "texture", M.Ma, M.ib);
F.k(M, "descendants", M.Z3);
F.We.create = function (a, c) {
    return new F.We(a, c)
};
F.We.aG = F.We.create;
F.BT = F.I.extend({
    ae: q, oe: q, ctor: function () {
        F.I.prototype.ctor.call(this);
        var a = document.createElement("canvas");
        a.width = a.height = 10;
        this.ae = a;
        this.oe = a.getContext("2d");
        var c = new F.ha;
        c.qd(a);
        c.Lb();
        this.ib(c)
    }, Ifa: A("oe"), Hfa: A("ae"), SR: function (a, c) {
        c === k && (c = a.height, a = a.width);
        var d = this.ae;
        d.width = a;
        d.height = c;
        this.Ma().Lb();
        this.Db(F.rect(0, 0, a, c), r)
    }
});
F.Cg = F.za.extend({
    Hp: q, re: 0, Wp: q, ctor: function (a, c, d) {
        this.Hp = a || q;
        this.re = c || 0;
        this.Wp = d || q
    }, m: function () {
        var a = new F.Cg;
        a.fj(this.Hp.m(), this.re, this.Wp);
        return a
    }, Ly: function () {
        return F.m(this)
    }, copy: function () {
        var a = new F.Cg;
        a.fj(this.Hp.m(), this.re, this.Wp);
        return a
    }, fj: function (a, c, d) {
        this.Hp = a;
        this.re = c;
        this.Wp = d;
        return p
    }, jk: A("Hp"), bm: z("Hp"), Wfa: A("re"), tna: z("re"), $ha: A("Wp"), apa: z("Wp")
});
F.Cg.create = function (a, c, d) {
    return new F.Cg(a, c, d)
};
F.sj = F.za.extend({
    se: q, Lj: 0, cn: r, t: 0, re: 0, aj: 0, ctor: function (a, c, d) {
        this.se = [];
        if (a === k)this.VQ(q, 0); else {
            var e = a[0];
            e && (e instanceof F.Ce ? this.VQ(a, c, d) : e instanceof F.Cg && this.lz(a, c, d))
        }
    }, tga: A("se"), Fna: z("se"), IF: function (a) {
        var c = new F.Cg;
        c.fj(a, 1, q);
        this.se.push(c);
        this.aj++
    }, jda: function (a) {
        a = F.Ra.ad(a);
        var c = F.rect(0, 0, 0, 0);
        c.width = a.width;
        c.height = a.height;
        a = new F.Ce(a, c);
        this.IF(a)
    }, kda: function (a, c) {
        var d = new F.Ce(a, c);
        this.IF(d)
    }, lz: function (a, c, d) {
        F.Gt(a, F.Cg);
        this.re = c;
        this.Lj = d ===
        k ? 1 : d;
        this.aj = 0;
        c = this.se;
        for (d = c.length = 0; d < a.length; d++) {
            var e = a[d];
            c.push(e);
            this.aj += e.re
        }
        return p
    }, m: function () {
        var a = new F.sj;
        a.lz(this.GL(), this.re, this.Lj);
        a.oA(this.cn);
        return a
    }, Ly: function () {
        var a = new F.sj;
        a.lz(this.GL(), this.re, this.Lj);
        a.oA(this.cn);
        return a
    }, GL: function () {
        for (var a = [], c = 0; c < this.se.length; c++)a.push(this.se[c].m());
        return a
    }, copy: function () {
        return this.Ly(q)
    }, Kga: A("Lj"), Sna: z("Lj"), oA: z("cn"), pha: A("cn"), xq: function () {
        return this.aj * this.re
    }, Vfa: A("re"), sna: z("re"),
    Tha: A("aj"), VQ: function (a, c, d) {
        F.Gt(a, F.Ce);
        this.Lj = d === k ? 1 : d;
        this.re = c || 0;
        this.aj = 0;
        c = this.se;
        c.length = 0;
        if (a) {
            for (d = 0; d < a.length; d++) {
                var e = a[d], f = new F.Cg;
                f.fj(e, 1, q);
                c.push(f)
            }
            this.aj += a.length
        }
        return p
    }, $z: u(), oj: u()
});
F.sj.create = function (a, c, d) {
    return new F.sj(a, c, d)
};
F.sj.aea = F.sj.create;
F.By = {
    Ho: {}, cP: function (a, c) {
        this.Ho[c] = a
    }, wma: function (a) {
        a && this.Ho[a] && delete this.Ho[a]
    }, bQ: function (a) {
        return this.Ho[a] ? this.Ho[a] : q
    }, qY: function (a, c) {
        var d = a.animations;
        if (d) {
            var e = 1, f = a.properties;
            if (f)for (var e = f.format != q ? parseInt(f.format) : e, f = f.spritesheets, g = F.cm, h = F.path, m = 0; m < f.length; m++)g.W1(h.hq(c, f[m]));
            switch (e) {
                case 1:
                    this.g0(d);
                    break;
                case 2:
                    this.h0(d);
                    break;
                default:
                    F.log(F.i.j2)
            }
        } else F.log(F.i.i2)
    }, cda: function (a) {
        F.assert(a, F.i.r2);
        var c = F.aa.ge(a);
        c ? this.qY(c, a) : F.log(F.i.q2)
    },
    g0: function (a) {
        var c = F.cm, d;
        for (d in a) {
            var e = a[d], f = e.frames, e = parseFloat(e.delay) || 0, g = q;
            if (f) {
                for (var g = [], h = 0; h < f.length; h++) {
                    var m = c.jk(f[h]);
                    if (m) {
                        var n = new F.Cg;
                        n.fj(m, 1, q);
                        g.push(n)
                    } else F.log(F.i.l2, d, f[h])
                }
                0 === g.length ? F.log(F.i.m2, d) : (g.length != f.length && F.log(F.i.n2, d), g = new F.sj(g, e, 1), F.By.cP(g, d))
            } else F.log(F.i.k2, d)
        }
    }, h0: function (a) {
        var c = F.cm, d;
        for (d in a) {
            var e = a[d], f = parseInt(e.loops), f = e.loop ? F.Qr : isNaN(f) ? 1 : f, g = e.restoreOriginalFrame && e.restoreOriginalFrame == p ? p : r, h = e.frames;
            if (h) {
                for (var m = [], n = 0; n < h.length; n++) {
                    var s = h[n], t = s.spriteframe, v = c.jk(t);
                    if (v) {
                        var t = parseFloat(s.delayUnits) || 0, s = s.notification, w = new F.Cg;
                        w.fj(v, t, s);
                        m.push(w)
                    } else F.log(F.i.p2, d, t)
                }
                e = parseFloat(e.delayPerUnit) || 0;
                h = new F.sj;
                h.lz(m, e, f);
                h.oA(g);
                F.By.cP(h, d)
            } else F.log(F.i.o2, d)
        }
    }, Vk: function () {
        this.Ho = {}
    }
};
F.Ce = F.za.extend({
    Ia: q, ll: q, bf: q, Le: r, Ba: q, Vg: q, dg: q, G: q, Lp: "", Ca: r, ctor: function (a, c, d, e, f) {
        this.Ia = F.d(0, 0);
        this.Vg = F.d(0, 0);
        this.ll = F.size(0, 0);
        this.Le = r;
        this.dg = F.size(0, 0);
        this.Lp = "";
        this.G = q;
        this.Ca = r;
        a !== k && c !== k && (d === k || e === k || f === k ? this.Fa(a, c) : this.Fa(a, c, d, e, f))
    }, Vu: A("Ca"), Ft: function (a, c) {
        this.addEventListener("load", a, c)
    }, L4: function () {
        var a = this.bf;
        return F.rect(a.x, a.y, a.width, a.height)
    }, poa: function (a) {
        this.bf || (this.bf = F.rect(0, 0, 0, 0));
        this.bf.x = a.x;
        this.bf.y = a.y;
        this.bf.width =
            a.width;
        this.bf.height = a.height;
        this.Ba = F.Xl(a)
    }, Mia: A("Le"), roa: z("Le"), fi: function () {
        var a = this.Ba;
        return F.rect(a.x, a.y, a.width, a.height)
    }, uS: function (a) {
        this.Ba || (this.Ba = F.rect(0, 0, 0, 0));
        this.Ba.x = a.x;
        this.Ba.y = a.y;
        this.Ba.width = a.width;
        this.Ba.height = a.height;
        this.bf = F.Hu(this.Ba)
    }, C4: function () {
        return F.d(this.Vg)
    }, aoa: function (a) {
        this.Vg.x = a.x;
        this.Vg.y = a.y;
        F.dE(this.Vg, this.Ia)
    }, Zga: function () {
        return F.size(this.dg)
    }, doa: function (a) {
        this.dg.width = a.width;
        this.dg.height = a.height
    }, zq: function () {
        return F.size(this.ll)
    },
    coa: function (a) {
        this.ll.width = a.width;
        this.ll.height = a.height
    }, Ma: function () {
        if (this.G)return this.G;
        if ("" !== this.Lp) {
            var a = F.Ra.ad(this.Lp);
            a && (this.Ca = a.Jb);
            return a
        }
        return q
    }, ib: function (a) {
        if (this.G != a) {
            var c = a.Jb;
            this.Ca = c;
            this.G = a;
            c || a.addEventListener("load", function (a) {
                this.Ca = p;
                if (this.Le && F.B === F.Aa) {
                    var c = a.Xa, c = F.cG(c, this.fi()), f = new F.ha;
                    f.qd(c);
                    f.Lb();
                    this.ib(f);
                    c = this.fi();
                    this.uS(F.rect(0, 0, c.width, c.height))
                }
                c = this.Ba;
                0 === c.width && 0 === c.height && (c = a.width, a = a.height, this.Ba.width =
                    c, this.Ba.height = a, this.bf = F.Hu(this.Ba), this.dg.width = this.bf.width, this.dg.height = this.bf.height, this.ll.width = c, this.ll.height = a);
                this.dispatchEvent("load")
            }, this)
        }
    }, ik: function () {
        return F.d(this.Ia)
    }, lA: function (a) {
        this.Ia.x = a.x;
        this.Ia.y = a.y
    }, m: function () {
        var a = new F.Ce;
        a.Fa(this.Lp, this.bf, this.Le, this.Vg, this.dg);
        a.ib(this.G);
        return a
    }, Ly: function () {
        var a = new F.Ce;
        a.Fa(this.Lp, this.bf, this.Le, this.Vg, this.dg);
        a.ib(this.G);
        return a
    }, copy: function () {
        return this.Ly()
    }, Fa: function (a, c, d, e, f) {
        2 ===
        arguments.length && (c = F.Hu(c));
        e = e || F.d(0, 0);
        f = f || c;
        d = d || r;
        F.Dd(a) ? (this.G = q, this.Lp = a) : a instanceof F.ha && this.ib(a);
        a = this.Ma();
        this.bf = c;
        c = this.Ba = F.Xl(c);
        if (a && a.url && a.Jb) {
            var g, h;
            d ? (g = c.x + c.height, h = c.y + c.width) : (g = c.x + c.width, h = c.y + c.height);
            g > a.EG() && F.error(F.i.MB, a.url);
            h > a.DG() && F.error(F.i.KB, a.url)
        }
        this.Vg.x = e.x;
        this.Vg.y = e.y;
        F.dE(e, this.Ia);
        this.dg.width = f.width;
        this.dg.height = f.height;
        F.EO(f, this.ll);
        this.Le = d;
        return p
    }
});
F.Ah.prototype.apply(F.Ce.prototype);
F.Ce.create = function (a, c, d, e, f) {
    return new F.Ce(a, c, d, e, f)
};
F.Ce.aG = F.Ce.create;
F.Ce.rca = function (a, c, d, e, f) {
    var g = new F.Ce;
    g.G = a;
    g.bf = c;
    g.Ba = F.Xl(c);
    g.Vg.x = e.x;
    g.Vg.y = e.y;
    F.dE(g.Vg, g.Ia);
    g.dg.width = f.width;
    g.dg.height = f.height;
    F.EO(g.dg, g.ll);
    g.Le = d;
    return g
};
F.cm = {
    hL: /^\s*\{\s*([\-]?\d+[.]?\d*)\s*,\s*([\-]?\d+[.]?\d*)\s*\}\s*$/,
    fY: /^\s*\{\s*\{\s*([\-]?\d+[.]?\d*)\s*,\s*([\-]?\d+[.]?\d*)\s*\}\s*,\s*\{\s*([\-]?\d+[.]?\d*)\s*,\s*([\-]?\d+[.]?\d*)\s*\}\s*\}\s*$/,
    Uh: {},
    Zj: {},
    kp: {},
    aO: function (a) {
        a = this.fY.exec(a);
        return !a ? F.rect(0, 0, 0, 0) : F.rect(parseFloat(a[1]), parseFloat(a[2]), parseFloat(a[3]), parseFloat(a[4]))
    },
    RN: function (a) {
        a = this.hL.exec(a);
        return !a ? F.d(0, 0) : F.d(parseFloat(a[1]), parseFloat(a[2]))
    },
    VE: function (a) {
        a = this.hL.exec(a);
        return !a ? F.size(0, 0) :
            F.size(parseFloat(a[1]), parseFloat(a[2]))
    },
    KZ: function (a) {
        var c = F.aa.ge(a);
        F.assert(c, F.i.m9, a);
        F.aa.oj(a);
        if (c.pp)return this.kp[a] = c;
        var d = c.frames, e = c.metadata || c.meta, c = {}, f = {}, g = 0;
        e && (g = e.format, g = 1 >= g.length ? parseInt(g) : g, f.p5 = e.textureFileName || e.textureFileName || e.image);
        for (var h in d) {
            var m = d[h];
            if (m) {
                e = {};
                if (0 == g) {
                    e.rect = F.rect(m.x, m.y, m.width, m.height);
                    e.dA = r;
                    e.offset = F.d(m.offsetX, m.offsetY);
                    var n = m.originalWidth, m = m.originalHeight;
                    (!n || !m) && F.log(F.i.l9);
                    n = Math.abs(n);
                    m = Math.abs(m);
                    e.size = F.size(n, m)
                } else if (1 == g || 2 == g)e.rect = this.aO(m.frame), e.dA = m.rotated || r, e.offset = this.RN(m.offset), e.size = this.VE(m.sourceSize); else if (3 == g) {
                    var n = this.VE(m.spriteSize), s = this.aO(m.textureRect);
                    n && (s = F.rect(s.x, s.y, n.width, n.height));
                    e.rect = s;
                    e.dA = m.textureRotated || r;
                    e.offset = this.RN(m.spriteOffset);
                    e.size = this.VE(m.spriteSourceSize);
                    e.d2 = m.aliases
                } else n = m.frame, s = m.sourceSize, h = m.filename || h, e.rect = F.rect(n.x, n.y, n.w, n.h), e.dA = m.rotated || r, e.offset = F.d(0, 0), e.size = F.size(s.w, s.h);
                c[h] =
                    e
            }
        }
        return this.kp[a] = {pp: p, frames: c, I6: f}
    },
    W1: function (a, c) {
        F.assert(a, F.i.o9);
        var d = this.kp[a] || F.aa.ge(a);
        if (d && d.frames) {
            var e = this.kp[a] || this.KZ(a), d = e.frames, e = e.I6;
            c ? c instanceof F.ha || (F.Dd(c) ? c = F.Ra.ad(c) : F.assert(0, F.i.p9)) : c = F.Ra.ad(F.path.hq(a, e.p5 || ".png"));
            var e = this.Zj, f = this.Uh, g;
            for (g in d) {
                var h = d[g], m = f[g];
                if (!m) {
                    m = new F.Ce(c, h.rect, h.dA, h.offset, h.size);
                    if (h = h.d2)for (var n = 0, s = h.length; n < s; n++) {
                        var t = h[n];
                        e[t] && F.log(F.i.n9, t);
                        e[t] = g
                    }
                    F.B === F.Aa && m.Le && m.Ma().Jb && (h = m.Ma().Xa, h =
                        F.cG(h, m.L4()), n = new F.ha, n.qd(h), n.Lb(), m.ib(n), h = m.Ba, m.uS(F.rect(0, 0, h.width, h.height)));
                    f[g] = m
                }
            }
        }
    },
    gca: function (a) {
        a = a.frames;
        for (var c in a)this.Uh[c] && F.log(F.i.k9, c)
    },
    IF: function (a, c) {
        this.Uh[c] = a
    },
    Cma: function () {
        this.Uh = {};
        this.Zj = {}
    },
    Bma: function (a) {
        a && (this.Zj[a] && delete this.Zj[a], this.Uh[a] && delete this.Uh[a])
    },
    Dma: function (a) {
        var c = this.Uh, d = this.Zj;
        if (a = this.kp[a]) {
            a = a.frames;
            for (var e in a)if (c[e]) {
                delete c[e];
                for (var f in d)d[f] == e && delete d[f]
            }
        }
    },
    Ema: function (a) {
        var c = this.Uh, d = this.Zj,
            e;
        for (e in c) {
            var f = c[e];
            if (f && f.Ma() == a) {
                delete c[e];
                for (var g in d)d[g] == e && delete d[g]
            }
        }
    },
    jk: function (a) {
        var c = this.Uh[a];
        if (!c) {
            var d = this.Zj[a];
            d && ((c = this.Uh[d.toString()]) || delete this.Zj[a])
        }
        c || F.log(F.i.q9, a);
        return c
    },
    Vk: function () {
        this.Uh = {};
        this.Zj = {};
        this.kp = {}
    }
};
F.kq = {
    jaa: 0,
    lba: 1,
    INT: 2,
    eaa: 3,
    A$: 4,
    Ws: 0,
    M_: 0,
    jF: r,
    my: r,
    hF: r,
    iF: r,
    kF: r,
    Fca: 0,
    OD: 0,
    vw: "",
    zt: {},
    pp: r,
    Ps: function () {
        var a = this.zt;
        a["cocos2d.x.version"] = F.VA;
        a["cocos2d.x.compiled_with_profiler"] = r;
        a["cocos2d.x.compiled_with_gl_state_cache"] = F.zh;
        this.pp = p
    },
    Pga: A("Ws"),
    Oga: A("M_"),
    Qga: A("OD"),
    opa: A("my"),
    ppa: A("jF"),
    npa: D(r),
    qpa: D(r),
    kpa: D(r),
    lpa: A("hF"),
    mpa: A("iF"),
    rpa: A("kF"),
    iq: function (a) {
        return -1 < this.vw.indexOf(a)
    },
    bia: function (a, c) {
        this.pp || this.Ps();
        var d = this.zt;
        return d[a] ? d[a] : c
    },
    cpa: function (a,
                   c) {
        this.zt[a] = c
    },
    h3: function () {
        0 === F.zh && (F.log(""), F.log(F.i.O2), F.log(""))
    },
    N3: function () {
        if (F.B !== F.Aa) {
            this.pp || this.Ps();
            var a = F.l, c = this.zt;
            c["gl.vendor"] = a.getParameter(a.VENDOR);
            c["gl.renderer"] = a.getParameter(a.RENDERER);
            c["gl.version"] = a.getParameter(a.VERSION);
            this.vw = "";
            for (var d = a.getSupportedExtensions(), e = 0; e < d.length; e++)this.vw += d[e] + " ";
            this.Ws = a.getParameter(a.MAX_TEXTURE_SIZE);
            c["gl.max_texture_size"] = this.Ws;
            this.OD = a.getParameter(a.MAX_COMBINED_TEXTURE_IMAGE_UNITS);
            c["gl.max_texture_units"] =
                this.OD;
            this.jF = this.iq("GL_IMG_texture_compression_pvrtc");
            c["gl.supports_PVRTC"] = this.jF;
            this.my = r;
            c["gl.supports_NPOT"] = this.my;
            this.hF = this.iq("GL_IMG_texture_format_BGRA888");
            c["gl.supports_BGRA8888"] = this.hF;
            this.iF = this.iq("GL_EXT_discard_framebuffer");
            c["gl.supports_discard_framebuffer"] = this.iF;
            this.kF = this.iq("vertex_array_object");
            c["gl.supports_vertex_array_object"] = this.kF;
            F.jq()
        }
    },
    Rka: function (a) {
        this.pp || this.Ps();
        var c = F.aa.ge(a);
        c || b("Please load the resource first : " + a);
        F.assert(c,
            F.i.Q2, a);
        if (c = c.data)for (var d in c)this.zt[d] = c[d]; else F.log(F.i.P2, a)
    }
};
F.N.SA = function () {
    F.gaa = F.za.extend({Ipa: u()});
    var a = F.jb.prototype;
    a.eo = function (a) {
        var d = this.ic;
        this.bI();
        var e = this.Nh, f = e.Qe.x / e.sa, g = e.Qe.y / e.Ya;
        switch (a) {
            case F.jb.PV:
                F.Se(F.Gg);
                F.Pl();
                e = new F.ea;
                F.iH(e, 0, d.width, 0, d.height, -1024, 1024);
                F.Hq(e);
                F.Se(F.Qf);
                F.Pl();
                break;
            case F.jb.tJ:
                var h = this.Cq(), m = new F.ea, e = new F.ea;
                F.Se(F.Gg);
                F.Pl();
                F.h6(m, d.width / d.height, 2 * h);
                F.Hq(m);
                F.Se(F.Qf);
                F.Pl();
                h = F.pk(q, -f + d.width / 2, -g + d.height / 2, h);
                d = F.pk(q, -f + d.width / 2, -g + d.height / 2, 0);
                f = F.pk(q, 0, 1, 0);
                F.hH(e, h,
                    d, f);
                F.Hq(e);
                break;
            case F.jb.QV:
                break;
            default:
                F.log(F.i.hU)
        }
        this.ft = a;
        F.La.dispatchEvent(this.dx);
        F.sS();
        F.ka.xe = p
    };
    a.NH = function (a) {
        var d = F.l;
        a ? (d.clearDepth(1), d.enable(d.DEPTH_TEST), d.depthFunc(d.LEQUAL)) : d.disable(d.DEPTH_TEST)
    };
    a.mA = function (a) {
        this.ic.width = F.lb.width;
        this.ic.height = F.lb.height;
        this.Nh = a || F.view;
        a = F.kq;
        a.N3();
        a.h3();
        this.Vo();
        this.kS();
        F.La && F.La.Wd(p)
    };
    a.Vk = function () {
        var a = F.l;
        a.clear(a.COLOR_BUFFER_BIT | a.DEPTH_BUFFER_BIT)
    };
    a.HC = function () {
        F.Sn()
    };
    a.CC = function () {
        F.ok()
    };
    a.Vo = function () {
        if (F.ui) {
            if (!(F.jb.SM == q || F.jb.SM == r)) {
                var a = new F.ha;
                a.qd(F.jb.jp);
                a.Lb();
                var d = F.view.yG().height / 320;
                0 === d && (d = this.ic.height / 320);
                var e = new F.ui;
                e.LE(p);
                e.Td("00.0", a, 12, 32, ".");
                e.scale = d;
                this.Rf = e;
                e = new F.ui;
                e.LE(p);
                e.Td("0.000", a, 12, 32, ".");
                e.scale = d;
                this.Sf = e;
                e = new F.ui;
                e.LE(p);
                e.Td("000", a, 12, 32, ".");
                e.scale = d;
                this.Yf = e;
                a = F.QA;
                this.Yf.X(a.x, 34 * d + a.y);
                this.Sf.X(a.x, 17 * d + a.y);
                this.Rf.X(a)
            }
        } else this.cZ()
    };
    a.cZ = function () {
        var a = 0, a = this.ic.width > this.ic.height ? 0 | 24 * (this.ic.height /
        320) : 0 | 24 * (this.ic.width / 320);
        this.Rf = new F.R("000.0", "Arial", a);
        this.Sf = new F.R("0.000", "Arial", a);
        this.Yf = new F.R("0000", "Arial", a);
        a = F.QA;
        this.Yf.X(this.Yf.width / 2 + a.x, 5 * this.Yf.height / 2 + a.y);
        this.Sf.X(this.Sf.width / 2 + a.x, 3 * this.Sf.height / 2 + a.y);
        this.Rf.X(this.Rf.width / 2 + a.x, this.Rf.height / 2 + a.y)
    };
    a.tP = function (a) {
        var d = new F.ea;
        F.WI(d);
        var e = new F.ea;
        F.g6(e, d);
        var d = d.c[14] / d.c[15], f = this.Nh.yG(), g = new F.vb;
        F.lR(g, new F.vb(2 * a.x / f.width - 1, 1 - 2 * a.y / f.height, d), e);
        return F.d(g.x, g.y)
    };
    a.uP = function (a) {
        var d =
            new F.ea;
        F.WI(d);
        var e = new F.vb;
        F.lR(e, new F.vb(a.x, a.y, 0), d);
        a = this.Nh.yG();
        return F.d(a.width * (0.5 * e.x + 0.5), a.height * (0.5 * -e.y + 0.5))
    };
    a.kh = function () {
        return this.Nh.kh()
    };
    a.$y = function () {
        return this.Nh.$y()
    };
    a.Cq = function () {
        return this.ic.height / 1.1566
    };
    a.bI = function () {
        var a = this.Nh;
        if (a) {
            var d = this.ic;
            a.X8(-a.Qe.x / a.sa, -a.Qe.y / a.Ya, d.width, d.height)
        }
    };
    a.E4 = A("Nh");
    a.Qn = A("ft");
    a.bS = function (a) {
        a ? F.pd(F.Ac, F.zc) : F.pd(F.l.ONE, F.l.ZERO)
    };
    a.kS = function () {
        this.bS(p);
        this.NH(r);
        this.eo(this.ft);
        F.l.clearColor(0,
            0, 0, 1)
    }
};
F.bd = 0;
F.WI = function (a) {
    var c = new F.ea;
    F.hu(F.Gg, c);
    var d = new F.ea;
    F.hu(F.Qf, d);
    F.lf(a, c, d)
};
F.jb = F.za.extend({
    Bca: r,
    Xs: r,
    Gc: r,
    Qx: r,
    pl: r,
    Go: 0,
    SD: 0,
    ft: 0,
    Fo: 0,
    Dh: 1,
    As: r,
    Wk: 0,
    gD: 0,
    Rf: q,
    Sf: q,
    Yf: q,
    ic: q,
    jl: q,
    Ki: q,
    Ex: q,
    Nh: q,
    Ui: q,
    pE: q,
    tc: q,
    se: 0,
    xt: 0,
    FE: 0,
    mca: q,
    gg: q,
    Lg: q,
    dx: q,
    aD: q,
    cD: q,
    bD: q,
    ctor: function () {
        var a = this;
        a.jl = Date.now();
        F.La.HF(F.Qb.nv, function () {
            a.jl = Date.now()
        })
    },
    oa: function () {
        this.SD = this.Go = 1 / F.V2;
        this.Ui = [];
        this.ft = F.jb.RV;
        this.pE = q;
        this.gD = this.Fo = 0;
        this.As = r;
        this.xt = this.se = 0;
        this.jl = Date.now();
        this.Qx = this.Gc = r;
        this.ic = F.size(0, 0);
        this.Nh = q;
        this.Dh = 1;
        this.gg = new F.Vr;
        this.Lg =
            F.rI ? new F.rI : q;
        this.gg.JH(this.Lg, F.Vr.Pv, r);
        this.aD = new F.mm(F.jb.qU);
        this.aD.setUserData(this);
        this.cD = new F.mm(F.jb.sU);
        this.cD.setUserData(this);
        this.bD = new F.mm(F.jb.rU);
        this.bD.setUserData(this);
        this.dx = new F.mm(F.jb.tU);
        this.dx.setUserData(this);
        return p
    },
    F2: function () {
        var a = Date.now();
        this.Xs ? (this.Wk = 0, this.Xs = r) : this.Wk = (a - this.jl) / 1E3;
        0 < F.Qb.Re[F.Qb.Dg.Mt] && 0.2 < this.Wk && (this.Wk = 1 / 60);
        this.jl = a
    },
    tP: q,
    uP: q,
    g3: function () {
        var a = F.ka;
        this.F2();
        this.Gc || (this.gg.update(this.Wk), F.La.dispatchEvent(this.bD));
        this.Vk();
        this.Ki && this.u8();
        this.HC && this.HC();
        this.tc && (a.xe === p ? (F.ka.rP(), this.tc.Eh = 0, this.tc.H(), a.TR()) : a.US() === p && a.transform(), F.La.dispatchEvent(this.cD));
        this.Ex && this.Ex.H();
        this.As && this.l1();
        this.CC && this.CC();
        a.cb(F.l);
        F.La.dispatchEvent(this.aD);
        this.xt++;
        this.As && this.IY()
    },
    HC: q,
    CC: q,
    end: function () {
        this.Qx = p
    },
    Rfa: A("Dh"),
    Tga: A("Ex"),
    Pa: function () {
        return F.size(this.ic)
    },
    Xt: function () {
        return F.size(this.ic.width * this.Dh, this.ic.height * this.Dh)
    },
    kh: q,
    $y: q,
    Cq: q,
    pause: function () {
        this.Gc ||
        (this.SD = this.Go, this.cS(0.25), this.Gc = p)
    },
    Tla: function () {
        F.assert(this.tc, F.i.eU);
        this.Ui.pop();
        var a = this.Ui.length;
        0 == a ? this.end() : (this.pl = p, this.Ki = this.Ui[a - 1])
    },
    m7: function () {
        F.By.Vk();
        F.cm.Vk();
        F.Ra.Vk()
    },
    n7: function () {
        this.Jl().zA();
        F.La && F.La.Wd(r);
        this.tc && (this.tc.pg(), this.tc.Cb(), this.tc.jf());
        this.Ki = this.tc = q;
        this.Ui.length = 0;
        this.MS();
        this.m7();
        F.jq()
    },
    o7: function (a) {
        F.assert(a, F.i.NI);
        this.pl = r;
        this.Ui.push(a);
        this.Ki = a
    },
    GH: function (a) {
        F.assert(a, F.i.NI);
        if (this.tc) {
            var c = this.Ui.length;
            0 === c ? (this.pl = p, this.Ui[c] = a) : (this.pl = p, this.Ui[c - 1] = a);
            this.Ki = a
        } else this.o7(a), this.KS()
    },
    Xq: function () {
        this.Gc && (this.cS(this.SD), (this.jl = Date.now()) || F.log(F.i.gU), this.Gc = r, this.Wk = 0)
    },
    ona: function (a) {
        a != this.Dh && (this.Dh = a, this.Vo())
    },
    NH: q,
    rna: u(),
    Wna: z("Xs"),
    u8: function () {
        var a = r, c = r;
        F.ia && (a = this.tc ? this.tc instanceof F.ia : r, c = this.Ki ? this.Ki instanceof F.ia : r);
        if (!c) {
            if (c = this.tc)c.pg(), c.Cb();
            this.pl && c && c.jf()
        }
        this.tc = this.Ki;
        F.ka.xe = p;
        this.Ki = q;
        !a && this.tc != q && (this.tc.ba(), this.tc.mi())
    },
    Zna: z("Ex"),
    Y3: A("pE"),
    d8: z("pE"),
    mA: q,
    eo: q,
    bI: q,
    E4: q,
    Qn: q,
    bS: q,
    l1: function () {
        this.se++;
        this.Fo += this.Wk;
        this.Rf && this.Sf && this.Yf ? (this.Fo > F.bU && (this.Sf.string = this.FE.toFixed(3), this.gD = this.se / this.Fo, this.Fo = this.se = 0, this.Rf.string = this.gD.toFixed(1), this.Yf.string = (0 | F.bd).toString()), this.Rf.H(), this.Sf.H(), this.Yf.H()) : this.Vo();
        F.bd = 0
    },
    Pia: A("pl"),
    rha: A("tc"),
    Dfa: A("Go"),
    Cia: A("As"),
    g8: z("As"),
    wha: A("FE"),
    Hia: A("Xs"),
    Jia: A("Gc"),
    Uha: A("xt"),
    Ula: function () {
        this.g7(1)
    },
    g7: function (a) {
        F.assert(this.tc,
            F.i.fU);
        var c = this.Ui, d = c.length;
        if (0 == d)this.end(); else if (!(a > d)) {
            for (; d > a;) {
                var e = c.pop();
                e.running && (e.pg(), e.Cb());
                e.jf();
                d--
            }
            this.Ki = c[c.length - 1];
            this.pl = r
        }
    },
    Jl: A("gg"),
    yS: function (a) {
        this.gg != a && (this.gg = a)
    },
    Py: A("Lg"),
    aS: function (a) {
        this.Lg != a && (this.Lg = a)
    },
    Xfa: A("Wk"),
    Vo: q,
    IY: function () {
        this.FE = (Date.now() - this.jl) / 1E3
    }
});
F.jb.tU = "director_projection_changed";
F.jb.qU = "director_after_draw";
F.jb.sU = "director_after_visit";
F.jb.rU = "director_after_update";
F.iU = F.jb.extend({
    qz: r, KS: function () {
        this.Xs = p;
        this.qz = r
    }, H6: function () {
        this.Qx ? (this.Qx = r, this.n7()) : this.qz || this.g3()
    }, MS: function () {
        this.qz = p
    }, cS: function (a) {
        this.Go = a;
        this.qz || (this.MS(), this.KS())
    }
});
F.jb.cI = q;
F.jb.YP = p;
F.jb.mD = function () {
    F.jb.YP && (F.jb.YP = r, F.jb.cI = new F.iU, F.jb.cI.oa());
    return F.jb.cI
};
F.V2 = 60;
F.jb.PV = 0;
F.jb.tJ = 1;
F.jb.QV = 3;
F.jb.RV = F.jb.tJ;
F.B === F.Aa ? (M = F.jb.prototype, M.eo = function (a) {
    this.ft = a;
    F.La.dispatchEvent(this.dx)
}, M.NH = u(), M.mA = function (a) {
    this.ic.width = F.lb.width;
    this.ic.height = F.lb.height;
    this.Nh = a || F.view;
    F.La && F.La.Wd(p)
}, M.Vk = function () {
    var a = this.Nh.Qe;
    F.l.clearRect(-a.x, a.y, a.width, -a.height)
}, M.Vo = function () {
    var a = 0, a = this.ic.width > this.ic.height ? 0 | 24 * (this.ic.height / 320) : 0 | 24 * (this.ic.width / 320);
    this.Rf = new F.R("000.0", "Arial", a);
    this.Sf = new F.R("0.000", "Arial", a);
    this.Yf = new F.R("0000", "Arial", a);
    a = F.QA;
    this.Yf.X(this.Yf.width /
        2 + a.x, 5 * this.Yf.height / 2 + a.y);
    this.Sf.X(this.Sf.width / 2 + a.x, 3 * this.Sf.height / 2 + a.y);
    this.Rf.X(this.Rf.width / 2 + a.x, this.Rf.height / 2 + a.y)
}, M.kh = function () {
    return this.Pa()
}, M.$y = function () {
    return F.d(0, 0)
}) : (F.jb.jp = new Image, F.Sa(F.jb.jp, "load", function () {
    F.jb.SM = p
}), F.jp && (F.jb.jp.src = F.jp), F.assert(F.ac(F.N.SA), F.i.Hd, "CCDirectorWebGL.js"), F.N.SA(), delete F.N.SA);
F.rr = F.za.extend({
    ep: q, fp: q, gp: q, Am: q, Bm: q, Cm: q, Sp: q, Tp: q, Up: q, sb: q, wp: q, ctor: function () {
        this.wp = new F.ea;
        this.restore()
    }, description: function () {
        return "\x3cCCCamera | center \x3d(" + this.Am + "," + this.Bm + "," + this.Cm + ")\x3e"
    }, gS: z("sb"), cH: A("sb"), restore: function () {
        this.ep = this.fp = 0;
        this.gp = F.rr.Cq();
        this.Sp = this.Am = this.Bm = this.Cm = 0;
        this.Tp = 1;
        this.Up = 0;
        F.Tn(this.wp);
        this.sb = r
    }, mu: function () {
        if (this.sb) {
            var a = new F.vb, c = new F.vb, d = new F.vb;
            F.pk(a, this.ep, this.fp, this.gp);
            F.pk(c, this.Am, this.Bm, this.Cm);
            F.pk(d, this.Sp, this.Tp, this.Up);
            F.hH(this.wp, a, c, d);
            this.sb = r
        }
        F.Hq(this.wp)
    }, HN: function (a) {
        if (this.sb) {
            var c = new F.vb, d = new F.vb, e = new F.vb;
            F.pk(c, this.ep, this.fp, this.gp);
            F.pk(d, this.Am, this.Bm, this.Cm);
            F.pk(e, this.Sp, this.Tp, this.Up);
            F.hH(this.wp, c, d, e);
            this.sb = r
        }
        F.lf(a, a, this.wp)
    }, yna: function (a, c, d) {
        this.jS(a, c, d)
    }, jS: function (a, c, d) {
        this.ep = a;
        this.fp = c;
        this.gp = d;
        this.sb = p
    }, hna: function (a, c, d) {
        this.Z7(a, c, d)
    }, Z7: function (a, c, d) {
        this.Am = a;
        this.Bm = c;
        this.Cm = d;
        this.sb = p
    }, $oa: function (a, c, d) {
        this.T8(a,
            c, d)
    }, T8: function (a, c, d) {
        this.Sp = a;
        this.Tp = c;
        this.Up = d;
        this.sb = p
    }, nga: function () {
        return {x: this.ep, y: this.fp, e: this.gp}
    }, iQ: function () {
        return {x: this.ep, y: this.fp, e: this.gp}
    }, Kfa: function () {
        return {x: this.Am, y: this.Bm, e: this.Cm}
    }, eQ: function () {
        return {x: this.Am, y: this.Bm, e: this.Cm}
    }, Zha: function () {
        return {x: this.Sp, y: this.Tp, e: this.Up}
    }, b5: function () {
        return {x: this.Sp, y: this.Tp, e: this.Up}
    }, Xba: u()
});
F.rr.Cq = function () {
    return F.Ev
};
F.eba = F.Pv + 1;
F.dJ = function (a, c, d) {
    this.next = this.Vla = q;
    this.target = a;
    this.k7 = c;
    this.paused = d;
    this.Hz = r
};
F.ZI = function (a, c, d) {
    this.list = a;
    this.Jn = c;
    this.target = d;
    this.gz = q
};
F.IU = function (a, c) {
    this.rj = q;
    this.target = a;
    this.ir = 0;
    this.Lt = this.Kt = q;
    this.paused = c;
    this.gz = q
};
F.tK = F.za.extend({
    hl: 0,
    Wc: q,
    Ne: q,
    yb: 0,
    pO: r,
    wy: r,
    Mp: 0,
    xE: 0,
    Tw: 0,
    Aga: A("hl"),
    setInterval: z("hl"),
    Jfa: A("Wc"),
    ctor: function (a, c, d, e, f) {
        this.Ne = a;
        this.Wc = c;
        this.yb = -1;
        this.hl = d || 0;
        this.Tw = f || 0;
        this.wy = 0 < this.Tw;
        this.xE = e == q ? F.Qr : e;
        this.pO = this.xE == F.Qr
    },
    ZC: function () {
        if (F.Dd(this.Wc))this.Ne[this.Wc](this.yb); else this.Wc.call(this.Ne, this.yb)
    },
    update: function (a) {
        if (-1 == this.yb)this.Mp = this.yb = 0; else {
            var c = this.Ne, d = this.Wc;
            this.yb += a;
            this.pO && !this.wy ? this.yb >= this.hl && (c && d && this.ZC(), this.yb = 0) : (this.wy ?
            this.yb >= this.Tw && (c && d && this.ZC(), this.yb -= this.Tw, this.Mp += 1, this.wy = r) : this.yb >= this.hl && (c && d && this.ZC(), this.yb = 0, this.Mp += 1), this.Mp > this.xE && F.L.Jl().aT(c, d))
        }
    }
});
F.Vr = F.za.extend({
    wt: 1, qn: q, Ij: q, Fw: q, Hj: q, aca: q, be: q, Di: r, ry: r, ctor: function () {
        this.wt = 1;
        this.qn = [[], [], []];
        this.Ij = {};
        this.Fw = [];
        this.Hj = {};
        this.ls = [];
        this.be = q;
        this.ry = this.Di = r
    }, tE: function (a) {
        delete this.Hj[a.target.ma];
        F.we(this.ls, a);
        a.tK = q;
        a.target = q
    }, fO: function (a) {
        if (a = this.Ij[a.target.ma])F.we(a.list, a.Jn), delete this.Ij[a.target.ma], F.we(this.Fw, a), a.Jn = q, a.target = q
    }, VN: function (a, c, d, e) {
        e = new F.dJ(c, d, e);
        if (a) {
            for (var f = a.length - 1, g = 0; g <= f && !(d < a[g].k7); g++);
            a.splice(g, 0, e)
        } else a = [],
            a.push(e);
        d = new F.ZI(a, e, c);
        this.Fw.push(d);
        this.Ij[c.ma] = d;
        return a
    }, yY: function (a, c, d) {
        d = new F.dJ(c, 0, d);
        a.push(d);
        a = new F.ZI(a, d, c);
        this.Fw.push(a);
        this.Ij[c.ma] = a
    }, Loa: z("wt"), Sha: A("wt"), update: function (a) {
        var c = this.qn, d = this.ls, e, f, g;
        this.ry = p;
        1 != this.wt && (a *= this.wt);
        f = 0;
        for (g = c.length; f < g && 0 <= f; f++)for (var h = this.qn[f], m = 0, n = h.length; m < n; m++)e = h[m], !e.paused && !e.Hz && e.target.update(a);
        f = 0;
        for (g = d.length; f < g; f++) {
            e = d[f];
            if (!e)break;
            this.be = e;
            this.Di = r;
            if (!e.paused)for (e.ir = 0; e.ir < e.rj.length; e.ir++)e.Kt =
                e.rj[e.ir], e.Lt = r, e.Kt.update(a), e.Kt = q;
            this.Di && 0 == e.rj.length && (this.tE(e), f--)
        }
        f = 0;
        for (g = c.length; f < g; f++) {
            h = this.qn[f];
            m = 0;
            for (n = h.length; m < n;) {
                e = h[m];
                if (!e)break;
                e.Hz ? this.fO(e) : m++
            }
        }
        this.ry = r;
        this.be = q
    }, T7: function (a, c, d, e, f, g) {
        F.assert(c, F.i.zW);
        F.assert(a, F.i.AW);
        d = d || 0;
        e = e == q ? F.Qr : e;
        f = f || 0;
        g = g || r;
        var h = this.Hj[a.ma];
        h || (h = new F.IU(a, g), this.ls.push(h), this.Hj[a.ma] = h);
        if (h.rj == q)h.rj = []; else for (var m = 0; m < h.rj.length; m++)if (g = h.rj[m], c == g.Wc) {
            F.log(F.i.yW, g.hl.toFixed(4), d.toFixed(4));
            g.hl =
                d;
            return
        }
        g = new F.tK(a, c, d, e, f);
        h.rj.push(g)
    }, JH: function (a, c, d) {
        if (a !== q) {
            var e = this.qn, f = this.Ij[a.ma];
            f ? f.Jn.Hz = r : 0 == c ? this.yY(e[1], a, d) : 0 > c ? e[0] = this.VN(e[0], a, c, d) : e[2] = this.VN(e[2], a, c, d)
        }
    }, aT: function (a, c) {
        if (!(a == q || c == q)) {
            var d = this.Hj[a.ma];
            if (d)for (var e = d.rj, f = 0, g = e.length; f < g; f++) {
                var h = e[f];
                if (c == h.Wc) {
                    h == d.Kt && !d.Lt && (d.Lt = p);
                    e.splice(f, 1);
                    d.ir >= f && d.ir--;
                    0 == e.length && (this.be == d ? this.Di = p : this.tE(d));
                    break
                }
            }
        }
    }, $u: function (a) {
        a != q && (a = this.Ij[a.ma], a != q && (this.ry ? a.Jn.Hz = p : this.fO(a.Jn)))
    },
    $S: function (a) {
        if (a != q) {
            var c = this.Hj[a.ma];
            if (c) {
                var d = c.rj;
                !c.Lt && 0 <= d.indexOf(c.Kt) && (c.Lt = p);
                d.length = 0;
                this.be == c ? this.Di = p : this.tE(c)
            }
            this.$u(a)
        }
    }, zA: function () {
        this.R9(F.Vr.Pv)
    }, R9: function (a) {
        for (var c = this.ls, d = this.qn, e = 0, f = c.length; e < f; e++)this.$S(c[e].target);
        for (e = 2; 0 <= e; e--)if (!(1 == e && 0 < a || 0 == e && 0 <= a))for (var c = d[e], f = 0, g = c.length; f < g; f++)this.$u(c[f].target)
    }, Qla: function () {
        return this.Z6(F.Vr.Pv)
    }, Z6: function () {
        for (var a = [], c, d = this.ls, e = this.qn, f = 0, g = d.length; f < g; f++)if (c = d[f])c.paused =
            p, a.push(c.target);
        f = 0;
        for (g = e.length; f < g; f++)for (var d = e[f], h = 0, m = d.length; h < m; h++)if (c = d[h])c.paused = p, a.push(c.target);
        return a
    }, M7: function (a) {
        if (a)for (var c = 0; c < a.length; c++)this.wk(a[c])
    }, Qq: function (a) {
        F.assert(a, F.i.wW);
        var c = this.Hj[a.ma];
        c && (c.paused = p);
        (a = this.Ij[a.ma]) && (a.Jn.paused = p)
    }, wk: function (a) {
        F.assert(a, F.i.xW);
        var c = this.Hj[a.ma];
        c && (c.paused = r);
        (a = this.Ij[a.ma]) && (a.Jn.paused = r)
    }, Sia: function (a) {
        F.assert(a, F.i.vW);
        return (a = this.Hj[a.ma]) ? a.paused : r
    }
});
F.Vr.Pv = -2147483648;
F.pJ = 2 * Math.PI;
F.QI = F.za.extend({
    LC: [], l: q, ctor: z("l"), kG: function (a, c) {
        c || (c = 1);
        var d = F.view.sa, e = F.d(a.x * d, a.y * F.view.Ya);
        this.l.beginPath();
        this.l.arc(e.x, -e.y, c * d, 0, 2 * Math.PI, r);
        this.l.closePath();
        this.l.fill()
    }, e3: function (a, c, d) {
        if (a != q) {
            d || (d = 1);
            c = this.l;
            var e = F.view.sa, f = F.view.Ya;
            c.beginPath();
            for (var g = 0, h = a.length; g < h; g++)c.arc(a[g].x * e, -a[g].y * f, d * e, 0, 2 * Math.PI, r);
            c.closePath();
            c.fill()
        }
    }, bi: function (a, c) {
        var d = this.l, e = F.view.sa, f = F.view.Ya;
        d.beginPath();
        d.moveTo(a.x * e, -a.y * f);
        d.lineTo(c.x * e, -c.y * f);
        d.closePath();
        d.stroke()
    }, mG: function (a, c) {
        this.bi(F.d(a.x, a.y), F.d(c.x, a.y));
        this.bi(F.d(c.x, a.y), F.d(c.x, c.y));
        this.bi(F.d(c.x, c.y), F.d(a.x, c.y));
        this.bi(F.d(a.x, c.y), F.d(a.x, a.y))
    }, OP: function (a, c, d) {
        a = [a, F.d(c.x, a.y), c, F.d(a.x, c.y)];
        this.oG(a, 4, d)
    }, ye: function (a, c, d, e) {
        e = e || r;
        if (a != q) {
            3 > a.length && b(Error("Polygon's point must greater than 2"));
            var f = a[0];
            c = this.l;
            var g = F.view.sa, h = F.view.Ya;
            c.beginPath();
            c.moveTo(f.x * g, -f.y * h);
            for (var f = 1, m = a.length; f < m; f++)c.lineTo(a[f].x * g, -a[f].y * h);
            d && c.closePath();
            e ? c.fill() : c.stroke()
        }
    }, oG: function (a, c, d) {
        this.Nf(d.r, d.g, d.b, d.a);
        this.ye(a, c, p, p)
    }, gG: function (a, c, d, e, f) {
        f = f || r;
        e = this.l;
        var g = F.view.sa, h = F.view.Ya;
        e.beginPath();
        e.arc(0 | a.x * g, 0 | -(a.y * h), c * g, -d, -(d - 2 * Math.PI), r);
        f && e.lineTo(0 | a.x * g, 0 | -(a.y * h));
        e.stroke()
    }, lG: function (a, c, d, e) {
        for (var f = this.LC, g = f.length = 0, h = 0; h < e; h++)f.push(F.d(Math.pow(1 - g, 2) * a.x + 2 * (1 - g) * g * c.x + g * g * d.x, Math.pow(1 - g, 2) * a.y + 2 * (1 - g) * g * c.y + g * g * d.y)), g += 1 / e;
        f.push(F.d(d.x, d.y));
        this.ye(f, e + 1, r, r)
    }, hG: function (a, c, d, e, f) {
        for (var g =
            this.LC, h = g.length = 0, m = 0; m < f; m++)g.push(F.d(Math.pow(1 - h, 3) * a.x + 3 * Math.pow(1 - h, 2) * h * c.x + 3 * (1 - h) * h * h * d.x + h * h * h * e.x, Math.pow(1 - h, 3) * a.y + 3 * Math.pow(1 - h, 2) * h * c.y + 3 * (1 - h) * h * h * d.y + h * h * h * e.y)), h += 1 / f;
        g.push(F.d(e.x, e.y));
        this.ye(g, f + 1, r, r)
    }, fG: function (a, c) {
        this.Gn(a, 0.5, c)
    }, Gn: function (a, c, d) {
        F.l.strokeStyle = "rgba(255,255,255,1)";
        var e = this.LC;
        e.length = 0;
        for (var f, g, h = 1 / a.length, m = 0; m < d + 1; m++)g = m / d, 1 == g ? (f = a.length - 1, g = 1) : (f = 0 | g / h, g = (g - h * f) / h), f = F.TT(F.Bd(a, f - 1), F.Bd(a, f - 0), F.Bd(a, f + 1), F.Bd(a, f + 2), c, g),
            e.push(f);
        this.ye(e, d + 1, r, r)
    }, drawImage: function (a, c, d, e, f) {
        switch (arguments.length) {
            case 2:
                this.l.drawImage(a, c.x, -(c.y + a.height));
                break;
            case 3:
                this.l.drawImage(a, c.x, -(c.y + d.height), d.width, d.height);
                break;
            case 5:
                this.l.drawImage(a, c.x, c.y, d.width, d.height, e.x, -(e.y + f.height), f.width, f.height);
                break;
            default:
                b(Error("Argument must be non-nil"))
        }
    }, PP: function (a, c, d) {
        a = a || this.l;
        c *= F.view.sa;
        d = "rgba(" + (0 | d.r) + "," + (0 | d.g) + "," + (0 | d.b);
        a.fillStyle = d + ",1)";
        var e = c / 10;
        a.beginPath();
        a.moveTo(-c, c);
        a.lineTo(0,
            e);
        a.lineTo(c, c);
        a.lineTo(e, 0);
        a.lineTo(c, -c);
        a.lineTo(0, -e);
        a.lineTo(-c, -c);
        a.lineTo(-e, 0);
        a.lineTo(-c, c);
        a.closePath();
        a.fill();
        var f = a.createRadialGradient(0, 0, e, 0, 0, c);
        f.addColorStop(0, d + ", 1)");
        f.addColorStop(0.3, d + ", 0.8)");
        f.addColorStop(1, d + ", 0.0)");
        a.fillStyle = f;
        a.beginPath();
        a.arc(0, 0, c - e, 0, F.pJ, r);
        a.closePath();
        a.fill()
    }, MP: function (a, c, d) {
        a = a || this.l;
        c *= F.view.sa;
        d = "rgba(" + (0 | d.r) + "," + (0 | d.g) + "," + (0 | d.b);
        var e = a.createRadialGradient(0, 0, c / 10, 0, 0, c);
        e.addColorStop(0, d + ", 1)");
        e.addColorStop(0.3,
            d + ", 0.8)");
        e.addColorStop(0.6, d + ", 0.4)");
        e.addColorStop(1, d + ", 0.0)");
        a.fillStyle = e;
        a.beginPath();
        a.arc(0, 0, c, 0, F.pJ, r);
        a.closePath();
        a.fill()
    }, fillText: function (a, c, d) {
        this.l.fillText(a, c, -d)
    }, Nf: function (a, c, d, e) {
        this.l.fillStyle = "rgba(" + a + "," + c + "," + d + "," + e / 255 + ")";
        this.l.strokeStyle = "rgba(" + a + "," + c + "," + d + "," + e / 255 + ")"
    }, TH: u(), co: function (a) {
        this.l.lineWidth = a * F.view.sa
    }
});
F.nU = F.za.extend({
    l: q, Qs: r, Pc: q, Ci: -1, xf: q, fE: -1, eE: -1, ctor: function (a) {
        a == q && (a = F.l);
        !a instanceof WebGLRenderingContext && b("Can't initialise DrawingPrimitiveWebGL. context need is WebGLRenderingContext");
        this.l = a;
        this.xf = new Float32Array([1, 1, 1, 1])
    }, qk: function () {
        this.Qs || (this.Pc = F.le.Kc(F.RB), this.Ci = this.l.getUniformLocation(this.Pc.Pn(), "u_color"), this.fE = this.l.getUniformLocation(this.Pc.Pn(), "u_pointSize"), this.Qs = p)
    }, yea: function () {
        this.Qs = r
    }, kG: function (a) {
        this.qk();
        var c = this.l;
        this.Pc.xb();
        this.Pc.qf();
        F.Xb(F.Jd);
        c.uniform4fv(this.Ci, this.xf);
        this.Pc.Pu(this.fE, this.eE);
        var d = c.createBuffer();
        c.bindBuffer(c.ARRAY_BUFFER, d);
        c.bufferData(c.ARRAY_BUFFER, new Float32Array([a.x, a.y]), c.STATIC_DRAW);
        c.vertexAttribPointer(F.kb, 2, c.FLOAT, r, 0, 0);
        c.drawArrays(c.POINTS, 0, 1);
        c.deleteBuffer(d);
        F.lh()
    }, e3: function (a) {
        if (a && 0 != a.length) {
            this.qk();
            var c = this.l;
            this.Pc.xb();
            this.Pc.qf();
            F.Xb(F.Jd);
            c.uniform4fv(this.Ci, this.xf);
            this.Pc.Pu(this.fE, this.eE);
            var d = c.createBuffer();
            c.bindBuffer(c.ARRAY_BUFFER,
                d);
            c.bufferData(c.ARRAY_BUFFER, this.Nx(a), c.STATIC_DRAW);
            c.vertexAttribPointer(F.kb, 2, c.FLOAT, r, 0, 0);
            c.drawArrays(c.POINTS, 0, a.length);
            c.deleteBuffer(d);
            F.lh()
        }
    }, Nx: function (a) {
        for (var c = new Float32Array(2 * a.length), d = 0; d < a.length; d++)c[2 * d] = a[d].x, c[2 * d + 1] = a[d].y;
        return c
    }, bi: function (a, c) {
        this.qk();
        var d = this.l;
        this.Pc.xb();
        this.Pc.qf();
        F.Xb(F.Jd);
        d.uniform4fv(this.Ci, this.xf);
        var e = d.createBuffer();
        d.bindBuffer(d.ARRAY_BUFFER, e);
        d.bufferData(d.ARRAY_BUFFER, this.Nx([a, c]), d.STATIC_DRAW);
        d.vertexAttribPointer(F.kb,
            2, d.FLOAT, r, 0, 0);
        d.drawArrays(d.LINES, 0, 2);
        d.deleteBuffer(e);
        F.lh()
    }, mG: function (a, c) {
        this.bi(F.d(a.x, a.y), F.d(c.x, a.y));
        this.bi(F.d(c.x, a.y), F.d(c.x, c.y));
        this.bi(F.d(c.x, c.y), F.d(a.x, c.y));
        this.bi(F.d(a.x, c.y), F.d(a.x, a.y))
    }, OP: function (a, c, d) {
        a = [a, F.d(c.x, a.y), c, F.d(a.x, c.y)];
        this.oG(a, 4, d)
    }, ye: function (a, c, d) {
        this.qk();
        c = this.l;
        this.Pc.xb();
        this.Pc.qf();
        F.Xb(F.Jd);
        c.uniform4fv(this.Ci, this.xf);
        var e = c.createBuffer();
        c.bindBuffer(c.ARRAY_BUFFER, e);
        c.bufferData(c.ARRAY_BUFFER, this.Nx(a), c.STATIC_DRAW);
        c.vertexAttribPointer(F.kb, 2, c.FLOAT, r, 0, 0);
        d ? c.drawArrays(c.LINE_LOOP, 0, a.length) : c.drawArrays(c.LINE_STRIP, 0, a.length);
        c.deleteBuffer(e);
        F.lh()
    }, oG: function (a, c, d) {
        this.qk();
        d && this.Nf(d.r, d.g, d.b, d.a);
        c = this.l;
        this.Pc.xb();
        this.Pc.qf();
        F.Xb(F.Jd);
        c.uniform4fv(this.Ci, this.xf);
        d = c.createBuffer();
        c.bindBuffer(c.ARRAY_BUFFER, d);
        c.bufferData(c.ARRAY_BUFFER, this.Nx(a), c.STATIC_DRAW);
        c.vertexAttribPointer(F.kb, 2, c.FLOAT, r, 0, 0);
        c.drawArrays(c.TRIANGLE_FAN, 0, a.length);
        c.deleteBuffer(d);
        F.lh()
    }, gG: function (a,
                     c, d, e, f) {
        this.qk();
        var g = 1;
        f && g++;
        var h = 2 * Math.PI / e;
        if (f = new Float32Array(2 * (e + 2))) {
            for (var m = 0; m <= e; m++) {
                var n = m * h, s = c * Math.sin(n + d) + a.y;
                f[2 * m] = c * Math.cos(n + d) + a.x;
                f[2 * m + 1] = s
            }
            f[2 * (e + 1)] = a.x;
            f[2 * (e + 1) + 1] = a.y;
            a = this.l;
            this.Pc.xb();
            this.Pc.qf();
            F.Xb(F.Jd);
            a.uniform4fv(this.Ci, this.xf);
            c = a.createBuffer();
            a.bindBuffer(a.ARRAY_BUFFER, c);
            a.bufferData(a.ARRAY_BUFFER, f, a.STATIC_DRAW);
            a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, 0, 0);
            a.drawArrays(a.LINE_STRIP, 0, e + g);
            a.deleteBuffer(c);
            F.lh()
        }
    }, lG: function (a, c, d,
                     e) {
        this.qk();
        for (var f = new Float32Array(2 * (e + 1)), g = 0, h = 0; h < e; h++)f[2 * h] = Math.pow(1 - g, 2) * a.x + 2 * (1 - g) * g * c.x + g * g * d.x, f[2 * h + 1] = Math.pow(1 - g, 2) * a.y + 2 * (1 - g) * g * c.y + g * g * d.y, g += 1 / e;
        f[2 * e] = d.x;
        f[2 * e + 1] = d.y;
        a = this.l;
        this.Pc.xb();
        this.Pc.qf();
        F.Xb(F.Jd);
        a.uniform4fv(this.Ci, this.xf);
        c = a.createBuffer();
        a.bindBuffer(a.ARRAY_BUFFER, c);
        a.bufferData(a.ARRAY_BUFFER, f, a.STATIC_DRAW);
        a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, 0, 0);
        a.drawArrays(a.LINE_STRIP, 0, e + 1);
        a.deleteBuffer(c);
        F.lh()
    }, hG: function (a, c, d, e, f) {
        this.qk();
        for (var g = new Float32Array(2 * (f + 1)), h = 0, m = 0; m < f; m++)g[2 * m] = Math.pow(1 - h, 3) * a.x + 3 * Math.pow(1 - h, 2) * h * c.x + 3 * (1 - h) * h * h * d.x + h * h * h * e.x, g[2 * m + 1] = Math.pow(1 - h, 3) * a.y + 3 * Math.pow(1 - h, 2) * h * c.y + 3 * (1 - h) * h * h * d.y + h * h * h * e.y, h += 1 / f;
        g[2 * f] = e.x;
        g[2 * f + 1] = e.y;
        a = this.l;
        this.Pc.xb();
        this.Pc.qf();
        F.Xb(F.Jd);
        a.uniform4fv(this.Ci, this.xf);
        c = a.createBuffer();
        a.bindBuffer(a.ARRAY_BUFFER, c);
        a.bufferData(a.ARRAY_BUFFER, g, a.STATIC_DRAW);
        a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, 0, 0);
        a.drawArrays(a.LINE_STRIP, 0, f + 1);
        a.deleteBuffer(c);
        F.lh()
    }, fG: function (a, c) {
        this.Gn(a, 0.5, c)
    }, Gn: function (a, c, d) {
        this.qk();
        for (var e = new Float32Array(2 * (d + 1)), f, g, h = 1 / a.length, m = 0; m < d + 1; m++)g = m / d, 1 == g ? (f = a.length - 1, g = 1) : (f = 0 | g / h, g = (g - h * f) / h), f = F.TT(F.Bd(a, f - 1), F.Bd(a, f), F.Bd(a, f + 1), F.Bd(a, f + 2), c, g), e[2 * m] = f.x, e[2 * m + 1] = f.y;
        a = this.l;
        this.Pc.xb();
        this.Pc.qf();
        F.Xb(F.Jd);
        a.uniform4fv(this.Ci, this.xf);
        c = a.createBuffer();
        a.bindBuffer(a.ARRAY_BUFFER, c);
        a.bufferData(a.ARRAY_BUFFER, e, a.STATIC_DRAW);
        a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, 0, 0);
        a.drawArrays(a.LINE_STRIP,
            0, d + 1);
        a.deleteBuffer(c);
        F.lh()
    }, Nf: function (a, c, d, e) {
        this.xf[0] = a / 255;
        this.xf[1] = c / 255;
        this.xf[2] = d / 255;
        this.xf[3] = e / 255
    }, TH: function (a) {
        this.eE = a * F.Fb()
    }, co: function (a) {
        this.l.lineWidth && this.l.lineWidth(a)
    }
});
F.N.qC = function () {
    var a = F.R.prototype;
    a.rb = F.I.prototype.rb;
    a.ef = function () {
        this.cg && (this.cg = r, this.Vp());
        F.n.prototype.ef.call(this)
    };
    a.Ef = function () {
        this.cg = p;
        var a = this.jn, d = this.ln;
        this.by = "rgba(128,128,128," + this.Yj + ")";
        this.gx = "rgba(" + (0 | d.r) + "," + (0 | d.g) + "," + (0 | d.b) + ", 1)";
        this.gy = "rgba(" + (0 | a.r) + "," + (0 | a.g) + "," + (0 | a.b) + ", 1)"
    };
    a.ed = F.I.prototype.ed;
    a.wb = F.I.prototype.wb;
    a.Uc = F.I.prototype.Uc;
    a.WG = function (a, d) {
        if (!F.I.prototype.oa.call(this))return r;
        this.shaderProgram = F.le.Kc(F.R.nY);
        this.rF(d,
            r);
        this.string = a;
        return p
    };
    a.hA = function (a) {
        var d = this.ln;
        if (d.r != a.r || d.g != a.g || d.b != a.b)d.r = a.r, d.g = a.g, d.b = a.b, this.Ef(), this.cg = p
    };
    a.Ka = function (a) {
        if (this.Zb && "" != this.Zb) {
            a = a || F.l;
            var d = this.G;
            d && d.Jb && (this.na.xb(), this.na.qf(), F.pd(this.q.src, this.q.J), F.Cd(d), F.Xb(F.Bh), a.bindBuffer(a.ARRAY_BUFFER, this.Cp), this.Od && (a.bufferData(a.ARRAY_BUFFER, this.Ub.cq, a.STATIC_DRAW), this.Od = r), a.vertexAttribPointer(F.kb, 3, a.FLOAT, r, 24, 0), a.vertexAttribPointer(F.hd, 2, a.FLOAT, r, 24, 16), a.vertexAttribPointer(F.sd,
                4, a.UNSIGNED_BYTE, p, 24, 12), a.drawArrays(a.TRIANGLE_STRIP, 0, 4));
            if (1 === F.Bo)a = this.Ub, a = [F.d(a.U.j.x, a.U.j.y), F.d(a.K.j.x, a.K.j.y), F.d(a.V.j.x, a.V.j.y), F.d(a.O.j.x, a.O.j.y)], F.Ge.ye(a, 4, p); else if (2 === F.Bo) {
                a = this.CQ();
                var d = this.offsetX, e = this.offsetY;
                a = [F.d(d, e), F.d(d + a.width, e), F.d(d + a.width, e + a.height), F.d(d, e + a.height)];
                F.Ge.ye(a, 4, p)
            }
            F.bd++
        }
    };
    a.Db = F.I.prototype.Db
};
F.N.DB = function () {
    var a = F.R.prototype;
    F.k(a, "color", a.mg, a.rb);
    F.k(a, "opacity", a.jh, a.wb);
    F.k(a, "string", a.Ll, a.yc);
    F.k(a, "textAlign", a.l4, a.n8);
    F.k(a, "verticalAlign", a.f5, a.W8);
    F.k(a, "fontSize", a.AG, a.jA);
    F.k(a, "fontName", a.zG, a.iA);
    F.k(a, "font", a.JZ, a.L0);
    F.k(a, "boundingWidth", a.kD, a.F0);
    F.k(a, "boundingHeight", a.GZ, a.E0);
    F.k(a, "fillStyle", a.IZ, a.hA);
    F.k(a, "strokeStyle", a.d_, a.g1);
    F.k(a, "lineWidth", a.RZ, a.P0);
    F.k(a, "shadowOffsetX", a.$Z, a.X0);
    F.k(a, "shadowOffsetY", a.a_, a.Y0);
    F.k(a, "shadowOpacity",
        a.b_, a.Z0);
    F.k(a, "shadowBlur", a.ZZ, a.W0)
};
F.R = F.I.extend({
    Cc: q,
    fl: F.Wr,
    Al: F.es,
    rc: q,
    fc: 0,
    Zb: "",
    YD: q,
    tp: r,
    Fh: q,
    Qd: r,
    ee: q,
    Yj: 0,
    sl: 0,
    by: q,
    Fp: q,
    dh: r,
    jn: q,
    $j: 0,
    gy: q,
    ln: q,
    gx: q,
    eF: 0,
    fF: 0,
    cg: r,
    wx: q,
    xx: q,
    BN: q,
    Mb: "LabelTTF",
    Td: function (a, c, d, e, f, g) {
        a = a ? a + "" : "";
        d = d || 16;
        e = e || F.size(0, 0);
        f = f || F.ew;
        g = g || F.es;
        this.qb = r;
        this.Cc = F.size(e.width, e.height);
        this.rc = c || "Arial";
        this.fl = f;
        this.Al = g;
        this.fc = d;
        this.Fh = this.fc + "px '" + c + "'";
        this.Fi = F.R.gs(c, this.fc);
        this.string = a;
        this.Ef();
        this.Vp();
        this.Hc();
        return p
    },
    Hc: function () {
        this.bn = this.cg = p;
        F.ka.Bu(this)
    },
    ctor: function (a, c, d, e, f, g) {
        F.I.prototype.ctor.call(this);
        this.Cc = F.size(0, 0);
        this.fl = F.ew;
        this.Al = F.es;
        this.qb = r;
        this.Fh = "";
        this.rc = "Arial";
        this.Qd = this.tp = r;
        this.ee = F.d(0, 0);
        this.sl = this.Yj = 0;
        this.by = "rgba(128, 128, 128, 0.5)";
        this.dh = r;
        this.jn = F.color(255, 255, 255, 255);
        this.$j = 0;
        this.gy = "";
        this.ln = F.color(255, 255, 255, 255);
        this.gx = "rgba(255,255,255,1)";
        this.fF = this.eF = 0;
        this.cg = r;
        this.BN = [];
        this.Ef();
        c && c instanceof ha ? this.WG(a, c) : F.R.prototype.Td.call(this, a, c, d, e, f, g)
    },
    oa: function () {
        return this.Td(" ",
            this.rc, this.fc)
    },
    N_: function () {
        this.nD().font = this.Fh
    },
    xp: function (a) {
        return this.nD().measureText(a).width
    },
    description: function () {
        return "\x3ccc.LabelTTF | FontName \x3d" + this.rc + " FontSize \x3d " + this.fc.toFixed(1) + "\x3e"
    },
    rb: q,
    Ef: q,
    ed: q,
    wb: q,
    Uc: q,
    Hpa: function (a) {
        F.n.prototype.Uc.call(this, a);
        this.Ef()
    },
    Ll: A("Zb"),
    l4: A("fl"),
    f5: A("Al"),
    aga: function () {
        return F.size(this.Cc)
    },
    AG: A("fc"),
    zG: A("rc"),
    WG: q,
    Foa: function (a) {
        a && this.rF(a, p)
    },
    Mha: function () {
        return this.n0(r)
    },
    t3: function (a, c, d, e) {
        a.r != q &&
        a.g != q && a.b != q && a.a != q ? this.oZ(a, c, d) : this.HM(a, c, d, e)
    },
    HM: function (a, c, d, e) {
        d = d || 0.5;
        r === this.Qd && (this.Qd = p);
        var f = this.ee;
        if (f && f.x != a || f.hf != c)f.x = a, f.y = c;
        this.Yj != d && (this.Yj = d);
        this.Ef();
        this.sl != e && (this.sl = e);
        this.Hc()
    },
    oZ: function (a, c, d) {
        this.Fp || (this.Fp = F.color(255, 255, 255, 128));
        this.Fp.r = a.r;
        this.Fp.g = a.g;
        this.Fp.b = a.b;
        this.HM(c.width || c.x || 0, c.height || c.y || 0, a.a != q ? a.a / 255 : 0.5, d)
    },
    $Z: function () {
        return this.ee.x
    },
    X0: function (a) {
        r === this.Qd && (this.Qd = p);
        this.ee.x != a && (this.ee.x = a, this.Hc())
    },
    a_: function () {
        return this.ee.hf
    },
    Y0: function (a) {
        r === this.Qd && (this.Qd = p);
        this.ee.hf != a && (this.ee.hf = a, this.Hc())
    },
    wca: function () {
        return F.d(this.ee.x, this.ee.y)
    },
    Sca: function (a) {
        r === this.Qd && (this.Qd = p);
        if (this.ee.x != a.x || this.ee.y != a.y)this.ee.x = a.x, this.ee.y = a.y, this.Hc()
    },
    b_: A("Yj"),
    Z0: function (a) {
        r === this.Qd && (this.Qd = p);
        this.Yj != a && (this.Yj = a, this.Ef(), this.Hc())
    },
    ZZ: A("sl"),
    W0: function (a) {
        r === this.Qd && (this.Qd = p);
        this.sl != a && (this.sl = a, this.Hc())
    },
    pea: function () {
        this.Qd && (this.Qd = r, this.Hc())
    },
    u3: function (a, c) {
        this.dh === r && (this.dh = p);
        var d = this.jn;
        if (d.r !== a.r || d.g !== a.g || d.b !== a.b)d.r = a.r, d.g = a.g, d.b = a.b, this.Ef();
        this.$j !== c && (this.$j = c || 0);
        this.Hc()
    },
    d_: A("jn"),
    g1: function (a) {
        this.dh === r && (this.dh = p);
        var c = this.jn;
        if (c.r !== a.r || c.g !== a.g || c.b !== a.b)c.r = a.r, c.g = a.g, c.b = a.b, this.Ef(), this.Hc()
    },
    RZ: A("$j"),
    P0: function (a) {
        this.dh === r && (this.dh = p);
        this.$j !== a && (this.$j = a || 0, this.Hc())
    },
    qea: function () {
        this.dh && (this.dh = r, this.Hc())
    },
    hA: q,
    IZ: A("ln"),
    rF: function (a, c) {
        a.vfa ? (this.Cc.width = a.boundingWidth,
            this.Cc.height = a.boundingHeight) : (this.Cc.width = 0, this.Cc.height = 0);
        this.fl = a.textAlign;
        this.Al = a.verticalAlign;
        this.rc = a.fontName;
        this.fc = a.fontSize || 12;
        this.Fh = this.fc + "px '" + this.rc + "'";
        this.Fi = F.R.gs(this.rc, this.fc);
        a.ES && this.t3(a.shadowOffsetX, a.shadowOffsetY, a.shadowOpacity, a.shadowBlur);
        a.hI && this.u3(a.strokeStyle, a.lineWidth);
        this.hA(a.fillStyle);
        c && this.Vp()
    },
    n0: function (a) {
        var c = new ha;
        a ? (c.fontSize = this.fc, c.boundingWidth = F.Fb() * this.Cc.width, c.boundingHeight = F.Fb() * this.Cc.height) : (c.fontSize =
            this.fc, c.boundingWidth = this.Cc.width, c.boundingHeight = this.Cc.height);
        c.fontName = this.rc;
        c.textAlign = this.fl;
        c.verticalAlign = this.Al;
        if (this.dh) {
            c.hI = p;
            var d = this.jn;
            c.strokeStyle = F.color(d.r, d.g, d.b);
            c.lineWidth = this.$j
        } else c.hI = r;
        this.Qd ? (c.ES = p, c.shadowBlur = this.sl, c.shadowOpacity = this.Yj, c.shadowOffsetX = (a ? F.Fb() : 1) * this.ee.x, c.shadowOffsetY = (a ? F.Fb() : 1) * this.ee.y) : c.Qd = r;
        a = this.ln;
        c.fillStyle = F.color(a.r, a.g, a.b);
        return c
    },
    Fi: 18,
    yc: function (a) {
        a = String(a);
        this.YD != a && (this.YD = a + "", this.uy(),
            this.Hc())
    },
    uy: function () {
        this.Zb = this.YD
    },
    n8: function (a) {
        a !== this.fl && (this.fl = a, this.Hc())
    },
    W8: function (a) {
        a != this.Al && (this.Al = a, this.Hc())
    },
    una: function (a, c) {
        var d;
        c === k ? (d = a.width, c = a.height) : d = a;
        if (d != this.Cc.width || c != this.Cc.height)this.Cc.width = d, this.Cc.height = c, this.uy(), this.Hc()
    },
    kD: function () {
        return this.Cc.width
    },
    F0: function (a) {
        a != this.Cc.width && (this.Cc.width = a, this.uy(), this.Hc())
    },
    GZ: function () {
        return this.Cc.height
    },
    E0: function (a) {
        a != this.Cc.height && (this.Cc.height = a, this.uy(),
            this.Hc())
    },
    jA: function (a) {
        this.fc !== a && (this.fc = a, this.Fh = a + "px '" + this.rc + "'", this.Fi = F.R.gs(this.rc, a), this.Hc())
    },
    iA: function (a) {
        this.rc && this.rc != a && (this.rc = a, this.Fh = this.fc + "px '" + a + "'", this.Fi = F.R.gs(a, this.fc), this.Hc())
    },
    JZ: A("Fh"),
    L0: function (a) {
        var c = F.R.vZ.exec(a);
        c && (this.fc = parseInt(c[1]), this.rc = c[2], this.Fh = a, this.Fi = F.R.gs(this.rc, this.fc), this.Hc())
    },
    nZ: function (a) {
        if (a) {
            var c = this.eF, d = this.fF, e = this.S.height - d, f = this.Al, g = this.fl, h = this.Fi, m = this.$j;
            a.setTransform(1, 0, 0, 1, 0 +
                0.5 * c, e + 0.5 * d);
            a.font != this.Fh && (a.font = this.Fh);
            a.fillStyle = this.gx;
            var n = d = 0, s = this.dh;
            s && (a.lineWidth = 2 * m, a.strokeStyle = this.gy);
            this.Qd && (m = this.ee, a.shadowColor = this.by, a.shadowOffsetX = m.x, a.shadowOffsetY = -m.y, a.shadowBlur = this.sl);
            a.textBaseline = F.R.y1[f];
            a.textAlign = F.R.x1[g];
            c = this.S.width - c;
            d = g === F.iK ? d + c : g === F.Wr ? d + c / 2 : d + 0;
            if (this.tp) {
                g = this.Xi.length;
                f === F.cL ? n = h + e - h * g : f === F.eY && (n = h / 2 + (e - h * g) / 2);
                for (f = 0; f < g; f++)c = this.Xi[f], m = -e + h * f + n, s && a.strokeText(c, d, m), a.fillText(c, d, m)
            } else f !==
            F.cL && (n = f === F.es ? n - e : n - 0.5 * e), s && a.strokeText(this.Zb, d, n), a.fillText(this.Zb, d, n)
        }
    },
    nD: function () {
        if (this.xx)return this.xx;
        if (!this.wx) {
            var a = F.bc("canvas"), c = new F.ha;
            c.qd(a);
            this.texture = c;
            this.wx = a
        }
        return this.xx = this.wx.getContext("2d")
    },
    OY: function (a, c, d) {
        var e = a[c], f = this.xp(e);
        if (f > d && 1 < e.length) {
            for (var g = e.length * (d / f) | 0, h = e.substr(g), m = f - this.xp(h), n, s = 0, t = 0; m > d && 100 > t++;)g *= d / m, g |= 0, h = e.substr(g), m = f - this.xp(h);
            for (t = 0; m < d && 100 > t++;)h && (s = (n = F.R.P1.exec(h)) ? n[0].length : 1, n = h), g += s, h =
                e.substr(g), m = f - this.xp(h);
            g -= s;
            d = e.substr(0, g);
            if (F.R.d$ && F.R.w1.test(n || h))f = F.R.E_.exec(d), g -= f ? f[0].length : 0, n = e.substr(g), d = e.substr(0, g);
            if (F.R.rZ.test(n) && (f = F.R.D_.exec(d)) && d !== f[0])g -= f[0].length, n = e.substr(g), d = e.substr(0, g);
            a[c] = n || h;
            a.splice(c, 0, d)
        }
    },
    vy: function () {
        var a = this.Cc.width, c, d, e = this.BN;
        e.length = 0;
        this.tp = r;
        this.N_();
        if (0 !== a) {
            this.Xi = this.Zb.split("\n");
            for (c = 0; c < this.Xi.length; c++)this.OY(this.Xi, c, a)
        } else {
            this.Xi = this.Zb.split("\n");
            c = 0;
            for (d = this.Xi.length; c < d; c++)e.push(this.xp(this.Xi[c]))
        }
        0 <
        this.Xi.length && (this.tp = p);
        d = c = 0;
        this.dh && (c = d = 2 * this.$j);
        if (this.Qd) {
            var f = this.ee;
            c += 2 * Math.abs(f.x);
            d += 2 * Math.abs(f.y)
        }
        a = 0 === a ? this.tp ? F.size(0 | Math.max.apply(Math, e) + c, 0 | this.Fi * this.Xi.length + d) : F.size(0 | this.xp(this.Zb) + c, 0 | this.Fi + d) : 0 === this.Cc.height ? this.tp ? F.size(0 | a + c, 0 | this.Fi * this.Xi.length + d) : F.size(0 | a + c, 0 | this.Fi + d) : F.size(0 | a + c, 0 | this.Cc.height + d);
        this.ze(a);
        this.eF = c;
        this.fF = d;
        e = this.jd;
        this.Ib.x = 0.5 * c + (a.width - c) * e.x;
        this.Ib.y = 0.5 * d + (a.height - d) * e.y
    },
    $: function () {
        this.cg && this.vy();
        return F.I.prototype.$.call(this)
    },
    Gh: function () {
        this.cg && this.vy();
        return F.I.prototype.Gh.call(this)
    },
    dl: function () {
        this.cg && this.vy();
        return F.I.prototype.dl.call(this)
    },
    Vp: function () {
        var a = this.nD(), c = this.wx, d = this.S;
        if (0 === this.Zb.length)return c.width = 1, c.height = d.height || 1, this.G && this.G.Lb(), this.Db(F.rect(0, 0, 1, d.height)), p;
        a.font = this.Fh;
        this.vy();
        var e = d.width, d = d.height, f = c.width == e && c.height == d;
        c.width = e;
        c.height = d;
        f && a.clearRect(0, 0, e, d);
        this.nZ(a);
        this.G && this.G.Lb();
        this.Db(F.rect(0,
            0, e, d));
        return p
    },
    H: function (a) {
        this.Zb && "" != this.Zb && (this.cg && (this.cg = r, this.Vp()), F.I.prototype.H.call(this, a || F.l))
    },
    Ka: q,
    wO: function (a) {
        var c = this.ca ? this.textureAtlas.texture : this.G;
        if (c) {
            var d = c.pixelsWidth, e = c.pixelsHeight, f, g = this.Ub;
            this.Yc ? (F.ro ? (c = (2 * a.x + 1) / (2 * d), d = c + (2 * a.height - 2) / (2 * d), f = (2 * a.y + 1) / (2 * e), a = f + (2 * a.width - 2) / (2 * e)) : (c = a.x / d, d = (a.x + a.height) / d, f = a.y / e, a = (a.y + a.width) / e), this.pc && (e = f, f = a, a = e), this.qc && (e = c, c = d, d = e), g.K.p.pa = c, g.K.p.qa = f, g.V.p.pa = c, g.V.p.qa = a, g.U.p.pa = d,
                g.U.p.qa = f, g.O.p.pa = d, g.O.p.qa = a) : (F.ro ? (c = (2 * a.x + 1) / (2 * d), d = c + (2 * a.width - 2) / (2 * d), f = (2 * a.y + 1) / (2 * e), a = f + (2 * a.height - 2) / (2 * e)) : (c = a.x / d, d = (a.x + a.width) / d, f = a.y / e, a = (a.y + a.height) / e), this.pc && (e = c, c = d, d = e), this.qc && (e = f, f = a, a = e), g.K.p.pa = c, g.K.p.qa = a, g.V.p.pa = d, g.V.p.qa = a, g.U.p.pa = c, g.U.p.qa = f, g.O.p.pa = d, g.O.p.qa = f);
            this.Od = p
        }
    }
});
F.B === F.Aa ? (M = F.R.prototype, M.rb = function (a) {
    F.n.prototype.rb.call(this, a);
    this.Ef()
}, M.ef = function () {
    this.cg && (this.cg = r, this.Vp());
    F.n.prototype.ef.call(this)
}, M.Ef = function () {
    this.Hc();
    var a = this.wa, c = this.ab, d = this.Fp || this.wa, e = this.jn, f = this.ln;
    this.by = "rgba(" + (0 | 0.5 * d.r) + "," + (0 | 0.5 * d.g) + "," + (0 | 0.5 * d.b) + "," + this.Yj + ")";
    this.gx = "rgba(" + (0 | a.r / 255 * f.r) + "," + (0 | a.g / 255 * f.g) + "," + (0 | a.b / 255 * f.b) + ", " + c / 255 + ")";
    this.gy = "rgba(" + (0 | a.r / 255 * e.r) + "," + (0 | a.g / 255 * e.g) + "," + (0 | a.b / 255 * e.b) + ", " + c / 255 + ")"
},
    M.ed = function (a) {
        F.n.prototype.ed.call(this, a);
        this.Ef()
    }, M.wb = function (a) {
    this.Je !== a && (F.I.prototype.wb.call(this, a), this.Ef(), this.Hc())
}, M.Uc = F.I.prototype.Uc, M.WG = function (a, c) {
    this.rF(c, r);
    this.string = a;
    return p
}, M.hA = function (a) {
    var c = this.ln;
    if (c.r != a.r || c.g != a.g || c.b != a.b)c.r = a.r, c.g = a.g, c.b = a.b, this.Ef(), this.Hc()
}, M.Ka = F.I.prototype.Ka, M.Db = function (a, c, d) {
    this.Yc = c || r;
    this.ze(d || a);
    this.aI(a);
    c = this.Z.Yi;
    c.x = a.x;
    c.y = a.y;
    c.Uq = a.x;
    c.Vq = a.y;
    c.width = a.width;
    c.height = a.height;
    c.gm = !(0 === c.width ||
    0 === c.height || 0 > c.x || 0 > c.y);
    a = this.Wh;
    this.pc && (a.x = -a.x);
    this.qc && (a.y = -a.y);
    this.Ob.x = a.x + (this.S.width - this.Ba.width) / 2;
    this.Ob.y = a.y + (this.S.height - this.Ba.height) / 2;
    this.ca && (this.dirty = p)
}, M = q) : (F.assert(F.ac(F.N.qC), F.i.Hd, "LabelTTFWebGL.js"), F.N.qC(), delete F.N.qC);
F.assert(F.ac(F.N.DB), F.i.Hd, "LabelTTFPropertyDefine.js");
F.N.DB();
delete F.N.DB;
F.R.x1 = ["left", "center", "right"];
F.R.y1 = ["top", "middle", "bottom"];
F.R.d$ = p;
F.R.P1 = /([a-zA-Z0-9\u00c4\u00d6\u00dc\u00e4\u00f6\u00fc\u00df\u00e9\u00e8\u00e7\u00e0\u00f9\u00ea\u00e2\u00ee\u00f4\u00fb]+|\S)/;
F.R.w1 = /^[!,.:;}\]%\?>\u3001\u2018\u201c\u300b\uff1f\u3002\uff0c\uff01]/;
F.R.E_ = /([a-zA-Z0-9\u00c4\u00d6\u00dc\u00e4\u00f6\u00fc\u00df\u00e9\u00e8\u00e7\u00e0\u00f9\u00ea\u00e2\u00ee\u00f4\u00fb]+|\S)$/;
F.R.D_ = /[a-zA-Z0-9\u00c4\u00d6\u00dc\u00e4\u00f6\u00fc\u00df\u00e9\u00e8\u00e7\u00e0\u00f9\u00ea\u00e2\u00ee\u00f4\u00fb]+$/;
F.R.rZ = /^[a-zA-Z0-9\u00c4\u00d6\u00dc\u00e4\u00f6\u00fc\u00df\u00e9\u00e8\u00e7\u00e0\u00f9\u00ea\u00e2\u00ee\u00f4\u00fb]/;
F.R.vZ = /^(\d+)px\s+['"]?([\w\s\d]+)['"]?$/;
F.R.create = function (a, c, d, e, f, g) {
    return new F.R(a, c, d, e, f, g)
};
F.R.bea = F.R.create;
F.R.nY = F.dY ? F.yj : F.PB;
F.R.Rk = F.bc("div");
F.R.Rk.style.fontFamily = "Arial";
F.R.Rk.style.position = "absolute";
F.R.Rk.style.left = "-100px";
F.R.Rk.style.top = "-100px";
F.R.Rk.style.lineHeight = "normal";
document.body ? document.body.appendChild(F.R.Rk) : F.Sa(window, "load", function () {
    this.removeEventListener("load", arguments.callee, r);
    document.body.appendChild(F.R.Rk)
}, r);
F.R.gs = function (a, c) {
    var d = F.R.jL[a + "." + c];
    if (0 < d)return d;
    var e = F.R.Rk;
    e.innerHTML = "ajghl~!";
    e.style.fontFamily = a;
    e.style.fontSize = c + "px";
    d = e.clientHeight;
    F.R.jL[a + "." + c] = d;
    e.innerHTML = "";
    return d
};
F.R.jL = {};
F.HU = F.za.extend({
    Rc: q, target: q, vn: 0, If: q, En: r, paused: r, gz: q, ctor: function () {
        this.Rc = [];
        this.target = q;
        this.vn = 0;
        this.If = q;
        this.paused = this.En = r;
        this.gz = q
    }
});
F.rI = F.za.extend({
    Ih: q, Io: q, be: q, Di: r, Pca: function (a, c) {
        for (var d = 0; d < a.length; d++)if (c == a[d].target)return a[d];
        return q
    }, ctor: function () {
        this.Ih = {};
        this.Io = [];
        this.be = q;
        this.Di = r
    }, U1: function (a, c, d) {
        a || b("cc.ActionManager.addAction(): action must be non-null");
        c || b("cc.ActionManager.addAction(): action must be non-null");
        var e = this.Ih[c.ma];
        e || (e = new F.HU, e.paused = d, e.target = c, this.Ih[c.ma] = e, this.Io.push(e));
        this.pY(e);
        e.Rc.push(a);
        a.D(c)
    }, rma: function () {
        for (var a = this.Io, c = 0; c < a.length; c++) {
            var d =
                a[c];
            d && this.NR(d.target, p)
        }
    }, NR: function (a, c) {
        if (a != q) {
            var d = this.Ih[a.ma];
            d && (-1 !== d.Rc.indexOf(d.If) && !d.En && (d.En = p), d.Rc.length = 0, this.be == d && !c ? this.Di = p : this.WC(d))
        }
    }, MR: function (a) {
        if (a != q) {
            var c = this.Ih[a.originalTarget.ma];
            if (c)for (var d = 0; d < c.Rc.length; d++) {
                if (c.Rc[d] == a) {
                    c.Rc.splice(d, 1);
                    break
                }
            } else F.log(F.i.pT)
        }
    }, u7: function (a, c) {
        a == F.ko && F.log(F.i.sI);
        F.assert(c, F.i.sI);
        var d = this.Ih[c.ma];
        if (d)for (var e = d.Rc.length, f = 0; f < e; ++f) {
            var g = d.Rc[f];
            if (g && g.JG() === a && g.originalTarget == c) {
                this.r0(f,
                    d);
                break
            }
        }
    }, uG: function (a, c) {
        a == F.ko && F.log(F.i.nT);
        var d = this.Ih[c.ma];
        if (d) {
            if (d.Rc != q)for (var e = 0; e < d.Rc.length; ++e) {
                var f = d.Rc[e];
                if (f && f.JG() === a)return f
            }
            F.log(F.i.oT, a)
        }
        return q
    }, M6: function (a) {
        return (a = this.Ih[a.ma]) ? a.Rc ? a.Rc.length : 0 : 0
    }, Qq: function (a) {
        (a = this.Ih[a.ma]) && (a.paused = p)
    }, wk: function (a) {
        (a = this.Ih[a.ma]) && (a.paused = r)
    }, Pla: function () {
        for (var a = [], c = this.Io, d = 0; d < c.length; d++) {
            var e = c[d];
            e && !e.paused && (e.paused = p, a.push(e.target))
        }
        return a
    }, M7: function (a) {
        if (a)for (var c = 0; c < a.length; c++)a[c] &&
        this.wk(a[c])
    }, $la: function () {
        F.L.Jl().$u(this)
    }, r0: function (a, c) {
        c.Rc[a] == c.If && !c.En && (c.En = p);
        c.Rc.splice(a, 1);
        c.vn >= a && c.vn--;
        0 == c.Rc.length && (this.be == c ? this.Di = p : this.WC(c))
    }, WC: function (a) {
        a && (delete this.Ih[a.target.ma], F.we(this.Io, a), a.Rc = q, a.target = q)
    }, pY: function (a) {
        a.Rc == q && (a.Rc = [])
    }, update: function (a) {
        for (var c = this.Io, d, e = 0; e < c.length; e++) {
            d = this.be = c[e];
            if (!d.paused)for (d.vn = 0; d.vn < d.Rc.length; d.vn++)if (d.If = d.Rc[d.vn], d.If) {
                d.En = r;
                d.If.step(a * (d.If.ot ? d.If.Me : 1));
                if (d.En)d.If = q;
                else if (d.If.mh()) {
                    d.If.stop();
                    var f = d.If;
                    d.If = q;
                    this.MR(f)
                }
                d.If = q
            }
            this.Di && 0 === d.Rc.length && this.WC(d)
        }
    }
});
F.kka = Number;
F.bja = Number;
F.cja = Number;
F.vo = 0;
F.wo = 1;
F.jH = 3.141592;
F.k6 = 0.017453;
F.l6 = 57.295779;
F.Na = 0.015625;
F.cd = function (a) {
    return a * a
};
F.gu = function (a) {
    return a * F.k6
};
F.fka = function (a) {
    return a * F.l6
};
F.wz = function (a, c) {
    return a < c ? a : c
};
F.vz = function (a, c) {
    return a > c ? a : c
};
F.aja = function (a, c) {
    return a + F.Na > c && a - F.Na < c
};
F.ji = function () {
    this.y = this.x = 0
};
F.oka = function (a, c, d) {
    a.x = c;
    a.y = d;
    return a
};
F.yz = function (a) {
    return Math.sqrt(F.cd(a.x) + F.cd(a.y))
};
F.pka = function (a) {
    return F.cd(a.x) + F.cd(a.y)
};
F.r6 = function (a, c) {
    var d = 1 / F.yz(c), e = new F.ji;
    e.x = c.x * d;
    e.y = c.y * d;
    a.x = e.x;
    a.y = e.y
};
F.lka = function (a, c, d) {
    a.x = c.x + d.x;
    a.y = c.y + d.y;
    return a
};
F.nka = function (a, c) {
    return a.x * c.x + a.y * c.y
};
F.zz = function (a, c, d) {
    a.x = c.x - d.x;
    a.y = c.y - d.y;
    return a
};
F.rka = function (a, c, d) {
    var e = new F.ji;
    e.x = c.x * d.c[0] + c.y * d.c[3] + d.c[6];
    e.y = c.x * d.c[1] + c.y * d.c[4] + d.c[7];
    a.x = e.x;
    a.y = e.y;
    return a
};
F.ska = D(q);
F.qka = function (a, c, d) {
    a.x = c.x * d;
    a.y = c.y * d;
    return a
};
F.mka = function (a, c) {
    return a.x < c.x + F.Na && a.x > c.x - F.Na && a.y < c.y + F.Na && a.y > c.y - F.Na
};
F.vb = function (a, c, d) {
    this.x = a || 0;
    this.y = c || 0;
    this.e = d || 0
};
F.pk = function (a, c, d, e) {
    if (!a)return new F.vb(c, d, e);
    a.x = c;
    a.y = d;
    a.e = e;
    return a
};
F.jR = function (a) {
    return Math.sqrt(F.cd(a.x) + F.cd(a.y) + F.cd(a.e))
};
F.kR = function (a) {
    return F.cd(a.x) + F.cd(a.y) + F.cd(a.e)
};
F.Lf = function (a, c) {
    var d = 1 / F.jR(c);
    a.x = c.x * d;
    a.y = c.y * d;
    a.e = c.e * d
};
F.Ql = function (a, c, d) {
    a.x = c.y * d.e - c.e * d.y;
    a.y = c.e * d.x - c.x * d.e;
    a.e = c.x * d.y - c.y * d.x
};
F.lH = function (a, c) {
    return a.x * c.x + a.y * c.y + a.e * c.e
};
F.iR = function (a, c, d) {
    a.x = c.x + d.x;
    a.y = c.y + d.y;
    a.e = c.e + d.e
};
F.nH = function (a, c, d) {
    a.x = c.x - d.x;
    a.y = c.y - d.y;
    a.e = c.e - d.e
};
F.xka = function (a, c, d) {
    a.x = c.x * d.c[0] + c.y * d.c[4] + c.e * d.c[8] + d.c[12];
    a.y = c.x * d.c[1] + c.y * d.c[5] + c.e * d.c[9] + d.c[13];
    a.e = c.x * d.c[2] + c.y * d.c[6] + c.e * d.c[10] + d.c[14];
    return a
};
F.yka = function (a, c, d) {
    a.x = c.x * d.c[0] + c.y * d.c[4] + c.e * d.c[8];
    a.y = c.x * d.c[1] + c.y * d.c[5] + c.e * d.c[9];
    a.e = c.x * d.c[2] + c.y * d.c[6] + c.e * d.c[10];
    return a
};
F.lR = function (a, c, d) {
    var e = new F.mR, f = new F.mR;
    F.s6(f, c.x, c.y, c.e);
    F.nR(e, f, d);
    a.x = e.x / e.M;
    a.y = e.y / e.M;
    a.e = e.e / e.M
};
F.mH = function (a, c, d) {
    a.x = c.x * d;
    a.y = c.y * d;
    a.e = c.e * d;
    return a
};
F.tka = function (a, c) {
    return a.x < c.x + F.Na && a.x > c.x - F.Na && a.y < c.y + F.Na && a.y > c.y - F.Na && a.e < c.e + F.Na && a.e > c.e - F.Na ? 1 : 0
};
F.uka = function (a, c, d) {
    c = new F.vb(c.x - d.c[12], c.y - d.c[13], c.e - d.c[14]);
    a.x = c.x * d.c[0] + c.y * d.c[1] + c.e * d.c[2];
    a.y = c.x * d.c[4] + c.y * d.c[5] + c.e * d.c[6];
    a.e = c.x * d.c[8] + c.y * d.c[9] + c.e * d.c[10];
    return a
};
F.vka = function (a, c, d) {
    a.x = c.x * d.c[0] + c.y * d.c[1] + c.e * d.c[2];
    a.y = c.x * d.c[4] + c.y * d.c[5] + c.e * d.c[6];
    a.e = c.x * d.c[8] + c.y * d.c[9] + c.e * d.c[10];
    return a
};
F.iu = function (a, c) {
    a != c && (a.x = c.x, a.y = c.y, a.e = c.e)
};
F.zka = function (a) {
    a.x = 0;
    a.y = 0;
    a.e = 0;
    return a
};
F.wka = function (a) {
    if (!a)return q;
    var c = new Float32Array(3);
    c[0] = a.x;
    c[1] = a.y;
    c[2] = a.e;
    return c
};
F.mR = function () {
    this.M = this.e = this.y = this.x = 0
};
F.s6 = function (a, c, d, e) {
    a.x = c;
    a.y = d;
    a.e = e;
    a.M = 1
};
F.Aka = function (a, c, d) {
    a.x = c.x + d.x;
    a.y = c.y + d.y;
    a.e = c.e + d.e;
    a.M = c.M + d.M;
    return a
};
F.Dka = function (a, c) {
    return a.x * c.x + a.y * c.y + a.e * c.e + a.M * c.M
};
F.t6 = function (a) {
    return Math.sqrt(F.cd(a.x) + F.cd(a.y) + F.cd(a.e) + F.cd(a.M))
};
F.Eka = function (a) {
    return F.cd(a.x) + F.cd(a.y) + F.cd(a.e) + F.cd(a.M)
};
F.Fka = aa();
F.u6 = function (a, c) {
    var d = 1 / F.t6(c);
    a.x *= d;
    a.y *= d;
    a.e *= d;
    a.M *= d
};
F.Gka = function (a, c, d) {
    F.u6(a, c);
    a.x *= d;
    a.y *= d;
    a.e *= d;
    a.M *= d;
    return a
};
F.Hka = function (a, c, d) {
    a.x = c.x - d.x;
    a.y = c.y - d.y;
    a.e = c.e - d.e;
    a.M = c.M - d.M;
    return a
};
F.nR = function (a, c, d) {
    a.x = c.x * d.c[0] + c.y * d.c[4] + c.e * d.c[8] + c.M * d.c[12];
    a.y = c.x * d.c[1] + c.y * d.c[5] + c.e * d.c[9] + c.M * d.c[13];
    a.e = c.x * d.c[2] + c.y * d.c[6] + c.e * d.c[10] + c.M * d.c[14];
    a.M = c.x * d.c[3] + c.y * d.c[7] + c.e * d.c[11] + c.M * d.c[15]
};
F.Jka = function (a, c, d, e, f, g) {
    for (var h = 0; h < g;)F.nR(a + h * c, d + h * e, f), ++h;
    return a
};
F.Bka = function (a, c) {
    return a.x < c.x + F.Na && a.x > c.x - F.Na && a.y < c.y + F.Na && a.y > c.y - F.Na && a.e < c.e + F.Na && a.e > c.e - F.Na && a.M < c.M + F.Na && a.M > c.M - F.Na
};
F.Cka = function (a, c) {
    if (a == c)return F.log("destVec and srcVec are same object"), a;
    a.x = c.x;
    a.y = c.y;
    a.e = c.e;
    a.M = c.M;
    return a
};
F.Ika = function (a) {
    if (!a)return q;
    var c = new Float32Array(4);
    c[0] = a.x;
    c[1] = a.y;
    c[2] = a.e;
    c[3] = a.M;
    return c
};
F.gka = function (a) {
    this.start = a || new F.ji;
    this.start = a || new F.ji
};
F.hka = function (a, c, d, e, f) {
    a.start.x = c;
    a.start.y = d;
    a.dir.x = e;
    a.dir.y = f
};
F.kH = function (a, c, d, e) {
    var f = a.start.x, g = a.start.y, h = a.start.x + a.dir.x;
    a = a.start.y + a.dir.y;
    var m = c.x, n = c.y, s = d.x, t = d.y, v = (t - n) * (h - f) - (s - m) * (a - g);
    if (v > -F.Na && v < F.Na)return F.vo;
    n = ((s - m) * (g - n) - (t - n) * (f - m)) / v;
    m = f + n * (h - f);
    n = g + n * (a - g);
    if (m < F.wz(c.x, d.x) - F.Na || m > F.vz(c.x, d.x) + F.Na || n < F.wz(c.y, d.y) - F.Na || n > F.vz(c.y, d.y) + F.Na || m < F.wz(f, h) - F.Na || m > F.vz(f, h) + F.Na || n < F.wz(g, a) - F.Na || n > F.vz(g, a) + F.Na)return F.vo;
    e.x = m;
    e.y = n;
    return F.wo
};
F.RF = function (a, c, d) {
    var e = new F.ji;
    F.zz(e, c, a);
    d.x = -e.y;
    d.y = e.x;
    F.r6(d, d)
};
F.jka = function (a, c, d, e, f, g) {
    var h = new F.ji, m = new F.ji, n = new F.ji, s = 1E4, t = F.vo, v;
    F.kH(a, c, d, h) && (v = new F.ji, t = F.wo, v = F.yz(F.zz(v, h, a.start)), v < s && (m.x = h.x, m.y = h.y, s = v, F.RF(c, d, n)));
    F.kH(a, d, e, h) && (v = new F.ji, t = F.wo, v = F.yz(F.zz(v, h, a.start)), v < s && (m.x = h.x, m.y = h.y, s = v, F.RF(d, e, n)));
    F.kH(a, e, c, h) && (v = new F.ji, t = F.wo, v = F.yz(F.zz(v, h, a.start)), v < s && (m.x = h.x, m.y = h.y, F.RF(e, c, n)));
    t && (f.x = m.x, f.y = m.y, g && (g.x = n.x, g.y = n.y));
    return t
};
F.ika = function () {
    F.log("cc.kmRay2IntersectCircle() has not been implemented.")
};
var Float32Array = Float32Array || Array;
F.tz = function () {
    this.c = new Float32Array([0, 0, 0, 0, 0, 0, 0, 0, 0])
};
F.jja = function (a, c) {
    for (var d = 0; 9 > d; d++)a.c[d] = c;
    return a
};
F.d6 = function (a, c) {
    a.c[0] = c.c[4] * c.c[8] - c.c[5] * c.c[7];
    a.c[1] = c.c[2] * c.c[7] - c.c[1] * c.c[8];
    a.c[2] = c.c[1] * c.c[5] - c.c[2] * c.c[4];
    a.c[3] = c.c[5] * c.c[6] - c.c[3] * c.c[8];
    a.c[4] = c.c[0] * c.c[8] - c.c[2] * c.c[6];
    a.c[5] = c.c[2] * c.c[3] - c.c[0] * c.c[5];
    a.c[6] = c.c[3] * c.c[7] - c.c[4] * c.c[6];
    a.c[8] = c.c[0] * c.c[4] - c.c[1] * c.c[3]
};
F.cR = function (a) {
    a.c[1] = a.c[2] = a.c[3] = a.c[5] = a.c[6] = a.c[7] = 0;
    a.c[0] = a.c[4] = a.c[8] = 1
};
F.kja = function (a, c, d) {
    var e = new F.tz;
    if (0 === c)return q;
    c = 1 / c;
    F.d6(e, d);
    F.e6(a, e, c);
    return a
};
F.tz.wD = new Float32Array([1, 0, 0, 0, 1, 0, 0, 0, 1]);
F.lja = function (a) {
    for (var c = 0; 9 > c; c++)if (F.tz.wD[c] !== a.c[c])return r;
    return p
};
F.wja = function (a, c) {
    var d, e;
    for (d = 0; 3 > d; ++d)for (e = 0; 3 > e; ++e)a.c[3 * d + e] = c.c[3 * e + d];
    return a
};
F.ija = function (a) {
    var c;
    c = a.c[0] * a.c[4] * a.c[8] + a.c[1] * a.c[5] * a.c[6] + a.c[2] * a.c[3] * a.c[7];
    return c -= a.c[2] * a.c[4] * a.c[6] + a.c[0] * a.c[5] * a.c[7] + a.c[1] * a.c[3] * a.c[8]
};
F.mja = function (a, c, d) {
    c = c.c;
    d = d.c;
    a.c[0] = c[0] * d[0] + c[3] * d[1] + c[6] * d[2];
    a.c[1] = c[1] * d[0] + c[4] * d[1] + c[7] * d[2];
    a.c[2] = c[2] * d[0] + c[5] * d[1] + c[8] * d[2];
    a.c[3] = c[0] * d[3] + c[3] * d[4] + c[6] * d[5];
    a.c[4] = c[1] * d[3] + c[4] * d[4] + c[7] * d[5];
    a.c[5] = c[2] * d[3] + c[5] * d[4] + c[8] * d[5];
    a.c[6] = c[0] * d[6] + c[3] * d[7] + c[6] * d[8];
    a.c[7] = c[1] * d[6] + c[4] * d[7] + c[7] * d[8];
    a.c[8] = c[2] * d[6] + c[5] * d[7] + c[8] * d[8];
    return a
};
F.e6 = function (a, c, d) {
    for (var e = 0; 9 > e; e++)a.c[e] = c.c[e] * d
};
F.oja = function (a, c, d) {
    var e = Math.cos(d);
    d = Math.sin(d);
    a.c[0] = e + c.x * c.x * (1 - e);
    a.c[1] = c.e * d + c.y * c.x * (1 - e);
    a.c[2] = -c.y * d + c.e * c.x * (1 - e);
    a.c[3] = -c.e * d + c.x * c.y * (1 - e);
    a.c[4] = e + c.y * c.y * (1 - e);
    a.c[5] = c.x * d + c.e * c.y * (1 - e);
    a.c[6] = c.y * d + c.x * c.e * (1 - e);
    a.c[7] = -c.x * d + c.y * c.e * (1 - e);
    a.c[8] = e + c.e * c.e * (1 - e);
    return a
};
F.hja = function (a, c) {
    if (a == c)return F.log("cc.kmMat3Assign(): pOut equals pIn"), a;
    for (var d = 0; 9 > d; d++)a.c[d] = c.c[d];
    return a
};
F.gja = function (a, c) {
    if (a == c)return p;
    for (var d = 0; 9 > d; ++d)if (!(a.c[d] + F.Na > c.c[d] && a.c[d] - F.Na < c.c[d]))return r;
    return p
};
F.rja = function (a, c) {
    a.c[0] = 1;
    a.c[1] = 0;
    a.c[2] = 0;
    a.c[3] = 0;
    a.c[4] = Math.cos(c);
    a.c[5] = Math.sin(c);
    a.c[6] = 0;
    a.c[7] = -Math.sin(c);
    a.c[8] = Math.cos(c);
    return a
};
F.sja = function (a, c) {
    a.c[0] = Math.cos(c);
    a.c[1] = 0;
    a.c[2] = -Math.sin(c);
    a.c[3] = 0;
    a.c[4] = 1;
    a.c[5] = 0;
    a.c[6] = Math.sin(c);
    a.c[7] = 0;
    a.c[8] = Math.cos(c);
    return a
};
F.tja = function (a, c) {
    a.c[0] = Math.cos(c);
    a.c[1] = -Math.sin(c);
    a.c[2] = 0;
    a.c[3] = Math.sin(c);
    a.c[4] = Math.cos(c);
    a.c[5] = 0;
    a.c[6] = 0;
    a.c[7] = 0;
    a.c[8] = 1;
    return a
};
F.nja = function (a, c) {
    a.c[0] = Math.cos(c);
    a.c[1] = Math.sin(c);
    a.c[2] = 0;
    a.c[3] = -Math.sin(c);
    a.c[4] = Math.cos(c);
    a.c[5] = 0;
    a.c[6] = 0;
    a.c[7] = 0;
    a.c[8] = 1;
    return a
};
F.uja = function (a, c, d) {
    F.cR(a);
    a.c[0] = c;
    a.c[4] = d;
    return a
};
F.vja = function (a, c, d) {
    F.cR(a);
    a.c[6] = c;
    a.c[7] = d;
    return a
};
F.pja = function (a, c) {
    if (!c || !a)return q;
    a.c[0] = 1 - 2 * (c.y * c.y + c.e * c.e);
    a.c[1] = 2 * (c.x * c.y - c.M * c.e);
    a.c[2] = 2 * (c.x * c.e + c.M * c.y);
    a.c[3] = 2 * (c.x * c.y + c.M * c.e);
    a.c[4] = 1 - 2 * (c.x * c.x + c.e * c.e);
    a.c[5] = 2 * (c.y * c.e - c.M * c.x);
    a.c[6] = 2 * (c.x * c.e - c.M * c.y);
    a.c[7] = 2 * (c.y * c.e + c.M * c.x);
    a.c[8] = 1 - 2 * (c.x * c.x + c.y * c.y);
    return a
};
F.qja = function (a, c, d) {
    F.gR(k, d);
    F.hR(k, a);
    return a
};
F.ea = function () {
    this.c = new Float32Array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
};
F.zja = function (a, c) {
    a.c[0] = a.c[1] = a.c[2] = a.c[3] = a.c[4] = a.c[5] = a.c[6] = a.c[7] = a.c[8] = a.c[9] = a.c[10] = a.c[11] = a.c[12] = a.c[13] = a.c[14] = a.c[15] = c
};
F.Tn = function (a) {
    a.c[1] = a.c[2] = a.c[3] = a.c[4] = a.c[6] = a.c[7] = a.c[8] = a.c[9] = a.c[11] = a.c[12] = a.c[13] = a.c[14] = 0;
    a.c[0] = a.c[5] = a.c[10] = a.c[15] = 1
};
F.ea.ag = function (a, c, d) {
    return a.c[c + 4 * d]
};
F.ea.ql = function (a, c, d, e) {
    a.c[c + 4 * d] = e
};
F.ea.xl = function (a, c, d, e, f) {
    var g = F.ea.ag(a, c, d);
    F.ea.ql(a, c, d, F.ea.ag(a, e, f));
    F.ea.ql(a, e, f, g)
};
F.ea.xZ = function (a, c) {
    var d, e = 0, f = 0, g, h, m, n = [0, 0, 0, 0], s = [0, 0, 0, 0], t = [0, 0, 0, 0];
    for (d = 0; 4 > d; d++) {
        for (g = m = 0; 4 > g; g++)if (1 != t[g])for (h = 0; 4 > h; h++)0 == t[h] && Math.abs(F.ea.ag(a, g, h)) >= m && (m = Math.abs(F.ea.ag(a, g, h)), f = g, e = h);
        ++t[e];
        if (f != e) {
            for (g = 0; 4 > g; g++)F.ea.xl(a, f, g, e, g);
            for (g = 0; 4 > g; g++)F.ea.xl(c, f, g, e, g)
        }
        s[d] = f;
        n[d] = e;
        if (0 == F.ea.ag(a, e, e))return F.vo;
        h = 1 / F.ea.ag(a, e, e);
        F.ea.ql(a, e, e, 1);
        for (g = 0; 4 > g; g++)F.ea.ql(a, e, g, F.ea.ag(a, e, g) * h);
        for (g = 0; 4 > g; g++)F.ea.ql(c, e, g, F.ea.ag(c, e, g) * h);
        for (h = 0; 4 > h; h++)if (h !=
            e) {
            m = F.ea.ag(a, h, e);
            F.ea.ql(a, h, e, 0);
            for (g = 0; 4 > g; g++)F.ea.ql(a, h, g, F.ea.ag(a, h, g) - F.ea.ag(a, e, g) * m);
            for (g = 0; 4 > g; g++)F.ea.ql(c, h, g, F.ea.ag(a, h, g) - F.ea.ag(c, e, g) * m)
        }
    }
    for (g = 3; 0 <= g; g--)if (s[g] != n[g])for (h = 0; 4 > h; h++)F.ea.xl(a, h, s[g], h, n[g]);
    return F.wo
};
F.ea.wD = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
F.g6 = function (a, c) {
    var d = new F.ea, e = new F.ea;
    F.og(d, c);
    F.Tn(e);
    F.ea.xZ(d, e) != F.vo && F.og(a, d)
};
F.Dja = function (a) {
    for (var c = 0; 16 > c; c++)if (F.ea.wD[c] != a.c[c])return r;
    return p
};
F.Lja = function (a, c) {
    var d, e, f = a.c, g = c.c;
    for (e = 0; 4 > e; ++e)for (d = 0; 4 > d; ++d)f[4 * e + d] = g[4 * d + e];
    return a
};
F.lf = function (a, c, d) {
    a = a.c;
    var e = c.c[0], f = c.c[1], g = c.c[2], h = c.c[3], m = c.c[4], n = c.c[5], s = c.c[6], t = c.c[7], v = c.c[8], w = c.c[9], x = c.c[10], y = c.c[11], B = c.c[12], I = c.c[13], E = c.c[14];
    c = c.c[15];
    var L = d.c[0], P = d.c[1], K = d.c[2], G = d.c[3], J = d.c[4], C = d.c[5], H = d.c[6], Q = d.c[7], S = d.c[8], R = d.c[9], $ = d.c[10], Y = d.c[11], Z = d.c[12], ca = d.c[13], T = d.c[14];
    d = d.c[15];
    a[0] = L * e + P * m + K * v + G * B;
    a[1] = L * f + P * n + K * w + G * I;
    a[2] = L * g + P * s + K * x + G * E;
    a[3] = L * h + P * t + K * y + G * c;
    a[4] = J * e + C * m + H * v + Q * B;
    a[5] = J * f + C * n + H * w + Q * I;
    a[6] = J * g + C * s + H * x + Q * E;
    a[7] = J * h + C * t + H * y + Q *
        c;
    a[8] = S * e + R * m + $ * v + Y * B;
    a[9] = S * f + R * n + $ * w + Y * I;
    a[10] = S * g + R * s + $ * x + Y * E;
    a[11] = S * h + R * t + $ * y + Y * c;
    a[12] = Z * e + ca * m + T * v + d * B;
    a[13] = Z * f + ca * n + T * w + d * I;
    a[14] = Z * g + ca * s + T * x + d * E;
    a[15] = Z * h + ca * t + T * y + d * c
};
F.v4 = function () {
    var a = F.uk.top.c, c = F.rk.top.c, d = new Float32Array(16);
    d[0] = a[0] * c[0] + a[4] * c[1] + a[8] * c[2] + a[12] * c[3];
    d[1] = a[1] * c[0] + a[5] * c[1] + a[9] * c[2] + a[13] * c[3];
    d[2] = a[2] * c[0] + a[6] * c[1] + a[10] * c[2] + a[14] * c[3];
    d[3] = a[3] * c[0] + a[7] * c[1] + a[11] * c[2] + a[15] * c[3];
    d[4] = a[0] * c[4] + a[4] * c[5] + a[8] * c[6] + a[12] * c[7];
    d[5] = a[1] * c[4] + a[5] * c[5] + a[9] * c[6] + a[13] * c[7];
    d[6] = a[2] * c[4] + a[6] * c[5] + a[10] * c[6] + a[14] * c[7];
    d[7] = a[3] * c[4] + a[7] * c[5] + a[11] * c[6] + a[15] * c[7];
    d[8] = a[0] * c[8] + a[4] * c[9] + a[8] * c[10] + a[12] * c[11];
    d[9] = a[1] * c[8] +
        a[5] * c[9] + a[9] * c[10] + a[13] * c[11];
    d[10] = a[2] * c[8] + a[6] * c[9] + a[10] * c[10] + a[14] * c[11];
    d[11] = a[3] * c[8] + a[7] * c[9] + a[11] * c[10] + a[15] * c[11];
    d[12] = a[0] * c[12] + a[4] * c[13] + a[8] * c[14] + a[12] * c[15];
    d[13] = a[1] * c[12] + a[5] * c[13] + a[9] * c[14] + a[13] * c[15];
    d[14] = a[2] * c[12] + a[6] * c[13] + a[10] * c[14] + a[14] * c[15];
    d[15] = a[3] * c[12] + a[7] * c[13] + a[11] * c[14] + a[15] * c[15];
    return d
};
F.Nga = function (a, c, d) {
    a = a.c;
    c = c.c;
    var e = d.c;
    e[0] = a[0] * c[0] + a[4] * c[1] + a[8] * c[2] + a[12] * c[3];
    e[1] = a[1] * c[0] + a[5] * c[1] + a[9] * c[2] + a[13] * c[3];
    e[2] = a[2] * c[0] + a[6] * c[1] + a[10] * c[2] + a[14] * c[3];
    e[3] = a[3] * c[0] + a[7] * c[1] + a[11] * c[2] + a[15] * c[3];
    e[4] = a[0] * c[4] + a[4] * c[5] + a[8] * c[6] + a[12] * c[7];
    e[5] = a[1] * c[4] + a[5] * c[5] + a[9] * c[6] + a[13] * c[7];
    e[6] = a[2] * c[4] + a[6] * c[5] + a[10] * c[6] + a[14] * c[7];
    e[7] = a[3] * c[4] + a[7] * c[5] + a[11] * c[6] + a[15] * c[7];
    e[8] = a[0] * c[8] + a[4] * c[9] + a[8] * c[10] + a[12] * c[11];
    e[9] = a[1] * c[8] + a[5] * c[9] + a[9] * c[10] + a[13] *
        c[11];
    e[10] = a[2] * c[8] + a[6] * c[9] + a[10] * c[10] + a[14] * c[11];
    e[11] = a[3] * c[8] + a[7] * c[9] + a[11] * c[10] + a[15] * c[11];
    e[12] = a[0] * c[12] + a[4] * c[13] + a[8] * c[14] + a[12] * c[15];
    e[13] = a[1] * c[12] + a[5] * c[13] + a[9] * c[14] + a[13] * c[15];
    e[14] = a[2] * c[12] + a[6] * c[13] + a[10] * c[14] + a[14] * c[15];
    e[15] = a[3] * c[12] + a[7] * c[13] + a[11] * c[14] + a[15] * c[15];
    return d.c
};
F.og = function (a, c) {
    if (a == c)F.log("cc.kmMat4Assign(): pOut equals pIn"); else {
        var d = a.c, e = c.c;
        d[0] = e[0];
        d[1] = e[1];
        d[2] = e[2];
        d[3] = e[3];
        d[4] = e[4];
        d[5] = e[5];
        d[6] = e[6];
        d[7] = e[7];
        d[8] = e[8];
        d[9] = e[9];
        d[10] = e[10];
        d[11] = e[11];
        d[12] = e[12];
        d[13] = e[13];
        d[14] = e[14];
        d[15] = e[15]
    }
};
F.xja = function (a, c) {
    if (a == c)return F.log("cc.kmMat4AreEqual(): pMat1 and pMat2 are same object."), p;
    for (var d = 0; 16 > d; d++)if (!(a.c[d] + F.Na > c.c[d] && a.c[d] - F.Na < c.c[d]))return r;
    return p
};
F.Ija = function (a, c) {
    a.c[0] = 1;
    a.c[1] = 0;
    a.c[2] = 0;
    a.c[3] = 0;
    a.c[4] = 0;
    a.c[5] = Math.cos(c);
    a.c[6] = Math.sin(c);
    a.c[7] = 0;
    a.c[8] = 0;
    a.c[9] = -Math.sin(c);
    a.c[10] = Math.cos(c);
    a.c[11] = 0;
    a.c[12] = 0;
    a.c[13] = 0;
    a.c[14] = 0;
    a.c[15] = 1;
    return a
};
F.Jja = function (a, c) {
    a.c[0] = Math.cos(c);
    a.c[1] = 0;
    a.c[2] = -Math.sin(c);
    a.c[3] = 0;
    a.c[4] = 0;
    a.c[5] = 1;
    a.c[6] = 0;
    a.c[7] = 0;
    a.c[8] = Math.sin(c);
    a.c[9] = 0;
    a.c[10] = Math.cos(c);
    a.c[11] = 0;
    a.c[12] = 0;
    a.c[13] = 0;
    a.c[14] = 0;
    a.c[15] = 1;
    return a
};
F.Kja = function (a, c) {
    a.c[0] = Math.cos(c);
    a.c[1] = Math.sin(c);
    a.c[2] = 0;
    a.c[3] = 0;
    a.c[4] = -Math.sin(c);
    a.c[5] = Math.cos(c);
    a.c[6] = 0;
    a.c[7] = 0;
    a.c[8] = 0;
    a.c[9] = 0;
    a.c[10] = 1;
    a.c[11] = 0;
    a.c[12] = 0;
    a.c[13] = 0;
    a.c[14] = 0;
    a.c[15] = 1;
    return a
};
F.Eja = function (a, c, d, e) {
    var f = Math.cos(c);
    c = Math.sin(c);
    var g = Math.cos(d);
    d = Math.sin(d);
    var h = Math.cos(e);
    e = Math.sin(e);
    var m = c * d, n = f * d;
    a.c[0] = g * h;
    a.c[4] = g * e;
    a.c[8] = -d;
    a.c[1] = m * h - f * e;
    a.c[5] = m * e + f * h;
    a.c[9] = c * g;
    a.c[2] = n * h + c * e;
    a.c[6] = n * e - c * h;
    a.c[10] = f * g;
    a.c[3] = a.c[7] = a.c[11] = 0;
    a.c[15] = 1;
    return a
};
F.Fja = function (a, c) {
    a.c[0] = 1 - 2 * (c.y * c.y + c.e * c.e);
    a.c[1] = 2 * (c.x * c.y + c.e * c.M);
    a.c[2] = 2 * (c.x * c.e - c.y * c.M);
    a.c[3] = 0;
    a.c[4] = 2 * (c.x * c.y - c.e * c.M);
    a.c[5] = 1 - 2 * (c.x * c.x + c.e * c.e);
    a.c[6] = 2 * (c.e * c.y + c.x * c.M);
    a.c[7] = 0;
    a.c[8] = 2 * (c.x * c.e + c.y * c.M);
    a.c[9] = 2 * (c.y * c.e - c.x * c.M);
    a.c[10] = 1 - 2 * (c.x * c.x + c.y * c.y);
    a.c[11] = 0;
    a.c[12] = 0;
    a.c[13] = 0;
    a.c[14] = 0;
    a.c[15] = 1;
    return a
};
F.Hja = function (a, c, d) {
    a.c[0] = c.c[0];
    a.c[1] = c.c[1];
    a.c[2] = c.c[2];
    a.c[3] = 0;
    a.c[4] = c.c[3];
    a.c[5] = c.c[4];
    a.c[6] = c.c[5];
    a.c[7] = 0;
    a.c[8] = c.c[6];
    a.c[9] = c.c[7];
    a.c[10] = c.c[8];
    a.c[11] = 0;
    a.c[12] = d.x;
    a.c[13] = d.y;
    a.c[14] = d.e;
    a.c[15] = 1;
    return a
};
F.j6 = function (a, c, d, e) {
    a.c[0] = c;
    a.c[5] = d;
    a.c[10] = e;
    a.c[15] = 1;
    a.c[1] = a.c[2] = a.c[3] = a.c[4] = a.c[6] = a.c[7] = a.c[8] = a.c[9] = a.c[11] = a.c[12] = a.c[13] = a.c[14] = 0
};
F.uz = function (a, c, d, e) {
    a.c[0] = a.c[5] = a.c[10] = a.c[15] = 1;
    a.c[1] = a.c[2] = a.c[3] = a.c[4] = a.c[6] = a.c[7] = a.c[8] = a.c[9] = a.c[11] = 0;
    a.c[12] = c;
    a.c[13] = d;
    a.c[14] = e
};
F.Cja = function (a, c) {
    a.x = c.c[4];
    a.y = c.c[5];
    a.e = c.c[6];
    F.Lf(a, a);
    return a
};
F.Bja = function (a, c) {
    a.x = c.c[0];
    a.y = c.c[1];
    a.e = c.c[2];
    F.Lf(a, a);
    return a
};
F.Aja = function (a, c) {
    a.x = c.c[8];
    a.y = c.c[9];
    a.e = c.c[10];
    F.Lf(a, a);
    return a
};
F.h6 = function (a, c, d) {
    var e = F.gu(30), f = d - 0.1, g = Math.sin(e);
    0 == f || (0 == g || 0 == c) || (e = Math.cos(e) / g, F.Tn(a), a.c[0] = e / c, a.c[5] = e, a.c[10] = -(d + 0.1) / f, a.c[11] = -1, a.c[14] = -0.2 * d / f, a.c[15] = 0)
};
F.iH = function (a, c, d, e, f, g, h) {
    F.Tn(a);
    a.c[0] = 2 / (d - c);
    a.c[5] = 2 / (f - e);
    a.c[10] = -2 / (h - g);
    a.c[12] = -((d + c) / (d - c));
    a.c[13] = -((f + e) / (f - e));
    a.c[14] = -((h + g) / (h - g))
};
F.hH = function (a, c, d, e) {
    var f = new F.vb, g = new F.vb, h = new F.vb, m = new F.vb, n = new F.ea;
    F.nH(f, d, c);
    F.Lf(f, f);
    F.iu(g, e);
    F.Lf(g, g);
    F.Ql(h, f, g);
    F.Lf(h, h);
    F.Ql(m, h, f);
    F.Lf(h, h);
    F.Tn(a);
    a.c[0] = h.x;
    a.c[4] = h.y;
    a.c[8] = h.e;
    a.c[1] = m.x;
    a.c[5] = m.y;
    a.c[9] = m.e;
    a.c[2] = -f.x;
    a.c[6] = -f.y;
    a.c[10] = -f.e;
    F.uz(n, -c.x, -c.y, -c.e);
    F.lf(a, a, n)
};
F.i6 = function (a, c, d) {
    var e = Math.cos(d);
    d = Math.sin(d);
    var f = new F.vb;
    F.Lf(f, c);
    a.c[0] = e + f.x * f.x * (1 - e);
    a.c[1] = f.e * d + f.y * f.x * (1 - e);
    a.c[2] = -f.y * d + f.e * f.x * (1 - e);
    a.c[3] = 0;
    a.c[4] = -f.e * d + f.x * f.y * (1 - e);
    a.c[5] = e + f.y * f.y * (1 - e);
    a.c[6] = f.x * d + f.e * f.y * (1 - e);
    a.c[7] = 0;
    a.c[8] = f.y * d + f.x * f.e * (1 - e);
    a.c[9] = -f.x * d + f.y * f.e * (1 - e);
    a.c[10] = e + f.e * f.e * (1 - e);
    a.c[11] = 0;
    a.c[12] = 0;
    a.c[13] = 0;
    a.c[14] = 0;
    a.c[15] = 1
};
F.f6 = function (a, c) {
    a.c[0] = c.c[0];
    a.c[1] = c.c[1];
    a.c[2] = c.c[2];
    a.c[3] = c.c[4];
    a.c[4] = c.c[5];
    a.c[5] = c.c[6];
    a.c[6] = c.c[8];
    a.c[7] = c.c[9];
    a.c[8] = c.c[10]
};
F.yja = function (a, c, d) {
    switch (d) {
        case F.XU:
            a.a = c.c[3] - c.c[0];
            a.b = c.c[7] - c.c[4];
            a.s = c.c[11] - c.c[8];
            a.z = c.c[15] - c.c[12];
            break;
        case F.VU:
            a.a = c.c[3] + c.c[0];
            a.b = c.c[7] + c.c[4];
            a.s = c.c[11] + c.c[8];
            a.z = c.c[15] + c.c[12];
            break;
        case F.TU:
            a.a = c.c[3] + c.c[1];
            a.b = c.c[7] + c.c[5];
            a.s = c.c[11] + c.c[9];
            a.z = c.c[15] + c.c[13];
            break;
        case F.YU:
            a.a = c.c[3] - c.c[1];
            a.b = c.c[7] - c.c[5];
            a.s = c.c[11] - c.c[9];
            a.z = c.c[15] - c.c[13];
            break;
        case F.UU:
            a.a = c.c[3] - c.c[2];
            a.b = c.c[7] - c.c[6];
            a.s = c.c[11] - c.c[10];
            a.z = c.c[15] - c.c[14];
            break;
        case F.WU:
            a.a =
                c.c[3] + c.c[2];
            a.b = c.c[7] + c.c[6];
            a.s = c.c[11] + c.c[10];
            a.z = c.c[15] + c.c[14];
            break;
        default:
            F.log("cc.kmMat4ExtractPlane(): Invalid plane index")
    }
    c = Math.sqrt(a.a * a.a + a.b * a.b + a.s * a.s);
    a.a /= c;
    a.b /= c;
    a.s /= c;
    a.z /= c;
    return a
};
F.Gja = function (a, c, d) {
    c = new ja;
    var e = new F.tz;
    F.f6(e, d);
    F.gR(c, e);
    F.hR(c, a);
    return a
};
F.VU = 0;
F.XU = 1;
F.TU = 2;
F.YU = 3;
F.WU = 4;
F.UU = 5;
F.Mja = function (a, c, d, e) {
    this.a = a || 0;
    this.b = c || 0;
    this.s = d || 0;
    this.z = e || 0
};
F.NV = 0;
F.MV = 1;
F.OV = 2;
F.Oja = function (a, c) {
    return a.a * c.x + a.b * c.y + a.s * c.e + a.z * c.M
};
F.Pja = function (a, c) {
    return a.a * c.x + a.b * c.y + a.s * c.e + a.z
};
F.Qja = function (a, c) {
    return a.a * c.x + a.b * c.y + a.s * c.e
};
F.Rja = function (a, c, d) {
    a.a = d.x;
    a.b = d.y;
    a.s = d.e;
    a.z = -F.lH(d, c);
    return a
};
F.Sja = function (a, c, d, e) {
    var f = new F.vb, g = new F.vb, h = new F.vb;
    F.nH(g, d, c);
    F.nH(h, e, c);
    F.Ql(f, g, h);
    F.Lf(f, f);
    a.a = f.x;
    a.b = f.y;
    a.s = f.e;
    a.z = F.lH(F.mH(f, f, -1), c);
    return a
};
F.Tja = function () {
    b("cc.kmPlaneIntersectLine() hasn't been implemented.")
};
F.Uja = function (a, c) {
    var d = new F.vb;
    d.x = c.a;
    d.y = c.b;
    d.e = c.s;
    var e = 1 / F.jR(d);
    F.Lf(d, d);
    a.a = d.x;
    a.b = d.y;
    a.s = d.e;
    a.z = c.z * e;
    return a
};
F.Vja = function () {
    F.log("cc.kmPlaneScale() has not been implemented.")
};
F.Nja = function (a, c) {
    var d = a.a * c.x + a.b * c.y + a.s * c.e + a.z;
    return 0.0010 < d ? F.NV : -0.0010 > d ? F.MV : F.OV
};
function ja() {
    this.M = this.e = this.y = this.x = 0
}
F.n6 = function (a, c) {
    a.x = -c.x;
    a.y = -c.y;
    a.e = -c.e;
    a.M = c.M;
    return a
};
F.o6 = function (a, c) {
    return a.M * c.M + a.x * c.x + a.y * c.y + a.e * c.e
};
F.Xja = aa();
F.p6 = function (a) {
    a.x = 0;
    a.y = 0;
    a.e = 0;
    a.M = 1
};
F.Yja = function (a, c) {
    var d = F.dR(c), e = new ja;
    if (Math.abs(d) > F.Na)return a.x = 0, a.y = 0, a.e = 0, a.M = 0, a;
    F.xz(a, F.n6(e, c), 1 / d);
    return a
};
F.Zja = function (a) {
    return 0 == a.x && 0 == a.y && 0 == a.e && 1 == a.M
};
F.dR = function (a) {
    return Math.sqrt(F.q6(a))
};
F.q6 = function (a) {
    return a.x * a.x + a.y * a.y + a.e * a.e + a.M * a.M
};
F.$ja = aa();
F.aka = function (a, c, d) {
    a.M = c.M * d.M - c.x * d.x - c.y * d.y - c.e * d.e;
    a.x = c.M * d.x + c.x * d.M + c.y * d.e - c.e * d.y;
    a.y = c.M * d.y + c.y * d.M + c.e * d.x - c.x * d.e;
    a.e = c.M * d.e + c.e * d.M + c.x * d.y - c.y * d.x;
    return a
};
F.eR = function (a, c) {
    var d = F.dR(c);
    Math.abs(d) <= F.Na && b("cc.kmQuaternionNormalize(): pIn is an invalid value");
    F.xz(a, c, 1 / d)
};
F.fR = function (a, c) {
    var d = 0.5 * F.jH, e = Math.sin(d);
    a.M = Math.cos(d);
    a.x = c.x * e;
    a.y = c.y * e;
    a.e = c.e * e
};
F.gR = function (a, c) {
    var d, e, f, g;
    d = [];
    e = g = 0;
    if (c) {
        d[0] = c.c[0];
        d[1] = c.c[3];
        d[2] = c.c[6];
        d[4] = c.c[1];
        d[5] = c.c[4];
        d[6] = c.c[7];
        d[8] = c.c[2];
        d[9] = c.c[5];
        d[10] = c.c[8];
        d[15] = 1;
        var h = d[0];
        e = h[0] + h[5] + h[10] + 1;
        e > F.Na ? (g = 2 * Math.sqrt(e), d = (h[9] - h[6]) / g, e = (h[2] - h[8]) / g, f = (h[4] - h[1]) / g, g *= 0.25) : h[0] > h[5] && h[0] > h[10] ? (g = 2 * Math.sqrt(1 + h[0] - h[5] - h[10]), d = 0.25 * g, e = (h[4] + h[1]) / g, f = (h[2] + h[8]) / g, g = (h[9] - h[6]) / g) : h[5] > h[10] ? (g = 2 * Math.sqrt(1 + h[5] - h[0] - h[10]), d = (h[4] + h[1]) / g, e = 0.25 * g, f = (h[9] + h[6]) / g, g = (h[2] - h[8]) / g) : (g = 2 * Math.sqrt(1 +
                h[10] - h[0] - h[5]), d = (h[2] + h[8]) / g, e = (h[9] + h[6]) / g, f = 0.25 * g, g = (h[4] - h[1]) / g);
        a.x = d;
        a.y = e;
        a.e = f;
        a.M = g
    }
};
F.dka = function (a, c, d, e) {
    var f, g, h, m, n;
    f = F.gu(d) / 2;
    g = F.gu(c) / 2;
    h = F.gu(e) / 2;
    e = Math.cos(f);
    c = Math.cos(g);
    d = Math.cos(h);
    f = Math.sin(f);
    g = Math.sin(g);
    h = Math.sin(h);
    m = c * d;
    n = g * h;
    a.M = e * m + f * n;
    a.x = f * m - e * n;
    a.y = e * g * d + f * c * h;
    a.e = e * c * h - f * g * d;
    F.eR(a, a);
    return a
};
F.eka = function (a, c, d, e) {
    if (c.x == d.x && c.y == d.y && c.e == d.e && c.M == d.M)return a.x = c.x, a.y = c.y, a.e = c.e, a.M = c.M, a;
    var f = F.o6(c, d), g = Math.acos(f), f = Math.sqrt(1 - F.cd(f)), h = Math.sin(e * g) / f, m = new ja, n = new ja;
    F.xz(m, c, Math.sin((1 - e) * g) / f);
    F.xz(n, d, h);
    F.m6(a, m, n);
    return a
};
F.hR = function (a, c) {
    var d;
    d = Math.sqrt(F.cd(a.x) + F.cd(a.y) + F.cd(a.e));
    d > -F.Na && d < F.Na || d < 2 * F.jH + F.Na && d > 2 * F.jH - F.Na ? (c.x = 0, c.y = 0, c.e = 1) : (c.x = a.x / d, c.y = a.y / d, c.e = a.e / d, F.Lf(c, c))
};
F.xz = function (a, c, d) {
    a.x = c.x * d;
    a.y = c.y * d;
    a.e = c.e * d;
    a.M = c.M * d
};
F.Wja = function (a, c) {
    a.x = c.x;
    a.y = c.y;
    a.e = c.e;
    a.M = c.M;
    return a
};
F.m6 = function (a, c, d) {
    a.x = c.x + d.x;
    a.y = c.y + d.y;
    a.e = c.e + d.e;
    a.M = c.M + d.M
};
F.cka = function (a, c, d, e) {
    var f = new F.vb, g = new F.vb;
    F.iu(f, c);
    F.iu(g, d);
    F.Lf(f, f);
    F.Lf(g, g);
    d = F.lH(f, g);
    if (1 <= d)return F.p6(a), a;
    -0.999999 > d ? Math.abs(F.kR(e)) < F.Na ? F.fR(a, e) : (f = new F.vb, g = new F.vb, g.x = 1, g.y = 0, g.e = 0, F.Ql(f, g, c), Math.abs(F.kR(f)) < F.Na && (g = new F.vb, g.x = 0, g.y = 1, g.e = 0, F.Ql(f, g, c)), F.Lf(f, f), F.fR(a, f)) : (c = Math.sqrt(2 * (1 + d)), e = 1 / c, d = new F.vb, F.Ql(d, f, g), a.x = d.x * e, a.y = d.y * e, a.e = d.e * e, a.M = 0.5 * c, F.eR(a, a));
    return a
};
F.bka = function (a, c, d) {
    var e = new F.vb, f = new F.vb, g = new F.vb;
    g.x = c.x;
    g.y = c.y;
    g.e = c.e;
    F.Ql(e, g, d);
    F.Ql(f, g, e);
    F.mH(e, e, 2 * c.M);
    F.mH(f, f, 2);
    F.iR(a, d, e);
    F.iR(a, a, f);
    return a
};
F.Xia = function (a, c) {
    this.min = a || new F.vb;
    this.max = c || new F.vb
};
F.Zia = function (a, c) {
    return a.x >= c.min.x && a.x <= c.max.x && a.y >= c.min.y && a.y <= c.max.y && a.e >= c.min.e && a.e <= c.max.e ? F.wo : F.vo
};
F.Yia = function (a, c) {
    F.iu(a.min, c.min);
    F.iu(a.max, c.max);
    return a
};
F.$ia = function () {
    F.log("cc.kmAABBScale hasn't been supported.")
};
F.Az = function (a, c, d, e) {
    this.top = d;
    this.stack = e
};
F.Az.vaa = 30;
F.oH = function (a) {
    a.stack = [];
    a.top = q
};
F.Bz = function (a, c) {
    a.stack.push(a.top);
    a.top = new F.ea;
    F.og(a.top, c)
};
F.Kka = function (a) {
    a.top = a.stack.pop()
};
F.pH = function (a) {
    a.stack = q;
    a.top = q
};
F.Qf = 5888;
F.Gg = 5889;
F.aJ = 5890;
F.rk = new F.Az;
F.uk = new F.Az;
F.Wu = new F.Az;
F.tb = q;
F.ZG = r;
F.w6 = function () {
    if (!F.ZG) {
        var a = new F.ea;
        F.oH(F.rk);
        F.oH(F.uk);
        F.oH(F.Wu);
        F.tb = F.rk;
        F.ZG = p;
        F.Tn(a);
        F.Bz(F.rk, a);
        F.Bz(F.uk, a);
        F.Bz(F.Wu, a)
    }
};
F.w6();
F.b6 = function () {
    F.pH(F.rk);
    F.pH(F.uk);
    F.pH(F.Wu);
    F.ZG = r;
    F.tb = q
};
F.Sn = function () {
    F.Bz(F.tb, F.tb.top)
};
F.dja = function (a) {
    F.tb.stack.push(F.tb.top);
    F.og(a, F.tb.top);
    F.tb.top = a
};
F.ok = function () {
    F.tb.top = F.tb.stack.pop()
};
F.Se = function (a) {
    switch (a) {
        case F.Qf:
            F.tb = F.rk;
            break;
        case F.Gg:
            F.tb = F.uk;
            break;
        case F.aJ:
            F.tb = F.Wu;
            break;
        default:
            b("Invalid matrix mode specified")
    }
};
F.Pl = function () {
    F.Tn(F.tb.top)
};
F.c6 = function (a) {
    F.og(F.tb.top, a)
};
F.Hq = function (a) {
    F.lf(F.tb.top, F.tb.top, a)
};
F.Iq = function (a, c) {
    var d = new F.ea;
    F.uz(d, a, c, 0);
    F.lf(F.tb.top, F.tb.top, d)
};
F.eja = function (a, c, d, e) {
    c = new F.vb(c, d, e);
    d = new F.ea;
    F.i6(d, c, F.gu(a));
    F.lf(F.tb.top, F.tb.top, d)
};
F.fja = function (a, c, d) {
    var e = new F.ea;
    F.j6(e, a, c, d);
    F.lf(F.tb.top, F.tb.top, e)
};
F.hu = function (a, c) {
    switch (a) {
        case F.Qf:
            F.og(c, F.rk.top);
            break;
        case F.Gg:
            F.og(c, F.uk.top);
            break;
        case F.aJ:
            F.og(c, F.Wu.top);
            break;
        default:
            b("Invalid matrix mode specified")
    }
};
F.nW = "precision lowp float;\nvarying vec4 v_fragmentColor;\nvoid main()                              \n{ \n    gl_FragColor \x3d v_fragmentColor;      \n}\n";
F.oW = "attribute vec4 a_position;\nuniform    vec4 u_color;\nuniform float u_pointSize;\nvarying lowp vec4 v_fragmentColor; \nvoid main(void)   \n{\n    gl_Position \x3d (CC_PMatrix * CC_MVMatrix) * a_position;  \n    gl_PointSize \x3d u_pointSize;          \n    v_fragmentColor \x3d u_color;           \n}";
F.bW = "precision lowp float; \nvarying vec4 v_fragmentColor; \nvoid main() \n{ \n     gl_FragColor \x3d v_fragmentColor; \n} ";
F.eW = "attribute vec4 a_position;\nattribute vec4 a_color;\nvarying lowp vec4 v_fragmentColor;\nvoid main()\n{\n    gl_Position \x3d (CC_PMatrix * CC_MVMatrix) * a_position;  \n    v_fragmentColor \x3d a_color;             \n}";
F.cW = "// #extension GL_OES_standard_derivatives : enable\nvarying mediump vec4 v_color;\nvarying mediump vec2 v_texcoord;\nvoid main()\t\n{ \n// #if defined GL_OES_standard_derivatives\t\n// gl_FragColor \x3d v_color*smoothstep(0.0, length(fwidth(v_texcoord)), 1.0 - length(v_texcoord)); \n// #else\t\ngl_FragColor \x3d v_color * step(0.0, 1.0 - length(v_texcoord)); \n// #endif \n}";
F.dW = "attribute mediump vec4 a_position; \nattribute mediump vec2 a_texcoord; \nattribute mediump vec4 a_color;\t\nvarying mediump vec4 v_color; \nvarying mediump vec2 v_texcoord;\t\nvoid main() \n{ \n     v_color \x3d a_color;//vec4(a_color.rgb * a_color.a, a_color.a); \n     v_texcoord \x3d a_texcoord; \n    gl_Position \x3d (CC_PMatrix * CC_MVMatrix) * a_position;  \n}";
F.jW = "precision lowp float;   \nvarying vec2 v_texCoord;  \nvoid main() \n{  \n    gl_FragColor \x3d  texture2D(CC_Texture0, v_texCoord);   \n}";
F.mW = "attribute vec4 a_position; \nattribute vec2 a_texCoord; \nvarying mediump vec2 v_texCoord; \nvoid main() \n{ \n    gl_Position \x3d (CC_PMatrix * CC_MVMatrix) * a_position;  \n    v_texCoord \x3d a_texCoord;               \n}";
F.kW = "precision lowp float;  \nuniform vec4 u_color; \nvarying vec2 v_texCoord; \nvoid main() \n{  \n    gl_FragColor \x3d  texture2D(CC_Texture0, v_texCoord) * u_color;    \n}";
F.lW = "attribute vec4 a_position;\nattribute vec2 a_texCoord; \nvarying mediump vec2 v_texCoord; \nvoid main() \n{ \n    gl_Position \x3d (CC_PMatrix * CC_MVMatrix) * a_position;  \n    v_texCoord \x3d a_texCoord;                 \n}";
F.fW = "precision lowp float;  \nvarying vec4 v_fragmentColor; \nvarying vec2 v_texCoord; \nvoid main() \n{ \n    gl_FragColor \x3d vec4( v_fragmentColor.rgb,         \n        v_fragmentColor.a * texture2D(CC_Texture0, v_texCoord).a   \n    ); \n}";
F.gW = "attribute vec4 a_position; \nattribute vec2 a_texCoord; \nattribute vec4 a_color;  \nvarying lowp vec4 v_fragmentColor; \nvarying mediump vec2 v_texCoord; \nvoid main() \n{ \n    gl_Position \x3d (CC_PMatrix * CC_MVMatrix) * a_position;  \n    v_fragmentColor \x3d a_color; \n    v_texCoord \x3d a_texCoord; \n}";
F.iW = "precision lowp float;\nvarying vec4 v_fragmentColor; \nvarying vec2 v_texCoord; \nvoid main() \n{ \n    gl_FragColor \x3d v_fragmentColor * texture2D(CC_Texture0, v_texCoord); \n}";
F.UJ = "attribute vec4 a_position; \nattribute vec2 a_texCoord; \nattribute vec4 a_color;  \nvarying lowp vec4 v_fragmentColor; \nvarying mediump vec2 v_texCoord; \nvoid main() \n{ \n    gl_Position \x3d (CC_PMatrix * CC_MVMatrix) * a_position;  \n    v_fragmentColor \x3d a_color; \n    v_texCoord \x3d a_texCoord; \n}";
F.hW = "precision lowp float;   \nvarying vec4 v_fragmentColor; \nvarying vec2 v_texCoord;   \nuniform float CC_alpha_value; \nvoid main() \n{  \n    vec4 texColor \x3d texture2D(CC_Texture0, v_texCoord);  \n    if ( texColor.a \x3c\x3d CC_alpha_value )          \n        discard; \n    gl_FragColor \x3d texColor * v_fragmentColor;  \n}";
F.jba = "precision lowp float; \nvarying vec4 v_fragmentColor; \nvarying vec2 v_texCoord; \nuniform sampler2D u_texture;  \nuniform sampler2D   u_mask;   \nvoid main()  \n{  \n    vec4 texColor   \x3d texture2D(u_texture, v_texCoord);  \n    vec4 maskColor  \x3d texture2D(u_mask, v_texCoord); \n    vec4 finalColor \x3d vec4(texColor.r, texColor.g, texColor.b, maskColor.a * texColor.a);        \n    gl_FragColor    \x3d v_fragmentColor * finalColor; \n}";
F.le = {
    aC: 0, bC: 1, ZB: 2, $B: 3, dC: 4, cC: 5, eC: 6, mK: 7, Hba: 8, Nd: {}, Ps: function () {
        this.B6();
        return p
    }, $e: function (a, c) {
        switch (c) {
            case this.aC:
                a.gj(F.UJ, F.iW);
                a.Sd(F.hm, F.kb);
                a.Sd(F.or, F.sd);
                a.Sd(F.lo, F.hd);
                break;
            case this.bC:
                a.gj(F.UJ, F.hW);
                a.Sd(F.hm, F.kb);
                a.Sd(F.or, F.sd);
                a.Sd(F.lo, F.hd);
                break;
            case this.ZB:
                a.gj(F.eW, F.bW);
                a.Sd(F.hm, F.kb);
                a.Sd(F.or, F.sd);
                break;
            case this.$B:
                a.gj(F.mW, F.jW);
                a.Sd(F.hm, F.kb);
                a.Sd(F.lo, F.hd);
                break;
            case this.dC:
                a.gj(F.lW, F.kW);
                a.Sd(F.hm, F.kb);
                a.Sd(F.lo, F.hd);
                break;
            case this.cC:
                a.gj(F.gW,
                    F.fW);
                a.Sd(F.hm, F.kb);
                a.Sd(F.or, F.sd);
                a.Sd(F.lo, F.hd);
                break;
            case this.eC:
                a.gj(F.oW, F.nW);
                a.Sd("aVertex", F.kb);
                break;
            case this.mK:
                a.gj(F.dW, F.cW);
                a.Sd(F.hm, F.kb);
                a.Sd(F.lo, F.hd);
                a.Sd(F.or, F.sd);
                break;
            default:
                F.log("cocos2d: cc.shaderCache._loadDefaultShader, error shader type");
                return
        }
        a.link();
        a.W9()
    }, B6: function () {
        var a = new F.ti;
        this.$e(a, this.aC);
        this.Nd[F.yj] = a;
        this.Nd.ShaderPositionTextureColor = a;
        a = new F.ti;
        this.$e(a, this.bC);
        this.Nd[F.Yv] = a;
        this.Nd.ShaderPositionTextureColorAlphaTest = a;
        a = new F.ti;
        this.$e(a, this.ZB);
        this.Nd[F.Xv] = a;
        this.Nd.ShaderPositionColor = a;
        a = new F.ti;
        this.$e(a, this.$B);
        this.Nd[F.Tr] = a;
        this.Nd.ShaderPositionTexture = a;
        a = new F.ti;
        this.$e(a, this.dC);
        this.Nd[F.QB] = a;
        this.Nd.ShaderPositionTextureUColor = a;
        a = new F.ti;
        this.$e(a, this.cC);
        this.Nd[F.PB] = a;
        this.Nd.ShaderPositionTextureA8Color = a;
        a = new F.ti;
        this.$e(a, this.eC);
        this.Nd[F.RB] = a;
        this.Nd.ShaderPositionUColor = a;
        a = new F.ti;
        this.$e(a, this.mK);
        this.Nd[F.TJ] = a;
        this.Nd.ShaderPositionLengthTextureColor = a
    }, pma: function () {
        var a = this.Kc(F.yj);
        a.reset();
        this.$e(a, this.aC);
        a = this.Kc(F.Yv);
        a.reset();
        this.$e(a, this.bC);
        a = this.Kc(F.Xv);
        a.reset();
        this.$e(a, this.ZB);
        a = this.Kc(F.Tr);
        a.reset();
        this.$e(a, this.$B);
        a = this.Kc(F.QB);
        a.reset();
        this.$e(a, this.dC);
        a = this.Kc(F.PB);
        a.reset();
        this.$e(a, this.cC);
        a = this.Kc(F.RB);
        a.reset();
        this.$e(a, this.eC)
    }, Kc: function (a) {
        return this.Nd[a]
    }, Pn: function (a) {
        return this.Nd[a]
    }, ida: function (a, c) {
        this.Nd[c] = a
    }
};
F.JU = function () {
    this.location = this.value = k;
    this.gz = {}
};
F.ti = F.za.extend({
    la: q, Tb: q, Gf: q, Rg: q, $a: q, Hh: q, tF: r, ff: function (a, c) {
        if (a == q)return r;
        for (var d = p, e = q, f = 0; f < this.Hh.length; f++)this.Hh[f].location == a && (e = this.Hh[f]);
        e ? e.value == c ? d = r : e.value = c : (e = new F.JU, e.location = a, e.value = c, this.Hh.push(e));
        return d
    }, lca: function () {
        return "\x3cCCGLProgram \x3d " + this.toString() + " | Program \x3d " + this.Tb.toString() + ", VertexShader \x3d " + this.Gf.toString() + ", FragmentShader \x3d " + this.Rg.toString() + "\x3e"
    }, EL: function (a, c, d) {
        if (!d || !a)return r;
        this.la.shaderSource(a,
            "precision highp float;        \nuniform mat4 CC_PMatrix;         \nuniform mat4 CC_MVMatrix;        \nuniform mat4 CC_MVPMatrix;       \nuniform vec4 CC_Time;            \nuniform vec4 CC_SinTime;         \nuniform vec4 CC_CosTime;         \nuniform vec4 CC_Random01;        \nuniform sampler2D CC_Texture0;   \n//CC INCLUDES END                \n" + d);
        this.la.compileShader(a);
        d = this.la.getShaderParameter(a, this.la.COMPILE_STATUS);
        d || (F.log("cocos2d: ERROR: Failed to compile shader:\n" + this.la.getShaderSource(a)),
            c == this.la.VERTEX_SHADER ? F.log("cocos2d: \n" + this.Z9()) : F.log("cocos2d: \n" + this.M3()));
        return 1 == d
    }, ctor: function (a, c, d) {
        this.$a = [];
        this.Hh = [];
        this.la = d || F.l;
        a && c && this.oa(a, c)
    }, nea: function () {
        this.Hh = this.$a = this.Rg = this.Gf = q;
        this.la.deleteProgram(this.Tb)
    }, gj: function (a, c) {
        var d = this.la;
        this.Tb = d.createProgram();
        this.Rg = this.Gf = q;
        a && (this.Gf = d.createShader(d.VERTEX_SHADER), this.EL(this.Gf, d.VERTEX_SHADER, a) || F.log("cocos2d: ERROR: Failed to compile vertex shader"));
        c && (this.Rg = d.createShader(d.FRAGMENT_SHADER),
        this.EL(this.Rg, d.FRAGMENT_SHADER, c) || F.log("cocos2d: ERROR: Failed to compile fragment shader"));
        this.Gf && d.attachShader(this.Tb, this.Gf);
        F.jq();
        this.Rg && d.attachShader(this.Tb, this.Rg);
        this.Hh.length = 0;
        F.jq();
        return p
    }, Td: function (a, c) {
        return this.gj(a, c)
    }, C5: function (a, c) {
        var d = F.aa.ge(a);
        d || b("Please load the resource firset : " + a);
        var e = F.aa.ge(c);
        e || b("Please load the resource firset : " + c);
        return this.gj(d, e)
    }, oa: function (a, c) {
        return this.C5(a, c)
    }, Sd: function (a, c) {
        this.la.bindAttribLocation(this.Tb,
            c, a)
    }, link: function () {
        if (!this.Tb)return F.log("cc.GLProgram.link(): Cannot link invalid program"), r;
        this.la.linkProgram(this.Tb);
        this.Gf && this.la.deleteShader(this.Gf);
        this.Rg && this.la.deleteShader(this.Rg);
        this.Rg = this.Gf = q;
        return F.Qb.Re[F.Qb.Dg.Mt] && !this.la.getProgramParameter(this.Tb, this.la.LINK_STATUS) ? (F.log("cocos2d: ERROR: Failed to link program: " + this.la.getProgramInfoLog(this.Tb)), F.h5(this.Tb), this.Tb = q, r) : p
    }, xb: function () {
        F.MG(this.Tb)
    }, W9: function () {
        this.$a[F.cs] = this.la.getUniformLocation(this.Tb,
            F.YX);
        this.$a[F.bs] = this.la.getUniformLocation(this.Tb, F.WX);
        this.$a[F.Co] = this.la.getUniformLocation(this.Tb, F.XX);
        this.$a[F.sw] = this.la.getUniformLocation(this.Tb, F.bY);
        this.$a[F.rw] = this.la.getUniformLocation(this.Tb, F.aY);
        this.$a[F.qw] = this.la.getUniformLocation(this.Tb, F.VX);
        this.tF = this.$a[F.sw] != q || this.$a[F.rw] != q || this.$a[F.qw] != q;
        this.$a[F.ds] = this.la.getUniformLocation(this.Tb, F.ZX);
        this.$a[F.lC] = this.la.getUniformLocation(this.Tb, F.$X);
        this.xb();
        this.DS(this.$a[F.lC], 0)
    }, Wha: function (a) {
        a ||
        b("cc.GLProgram.getUniformLocationForName(): uniform name should be non-null");
        this.Tb || b("cc.GLProgram.getUniformLocationForName(): Invalid operation. Cannot get uniform location when program is not initialized");
        return this.la.getUniformLocation(this.Tb, a)
    }, Xha: function () {
        return this.$a[F.Co]
    }, Yha: function () {
        return this.$a[F.lC]
    }, DS: function (a, c) {
        this.ff(a, c) && this.la.uniform1i(a, c)
    }, Soa: function (a, c, d) {
        this.ff(a, [c, d]) && this.la.uniform2i(a, c, d)
    }, Voa: function (a, c, d, e) {
        this.ff(a, [c, d, e]) && this.la.uniform3i(a,
            c, d, e)
    }, Yoa: function (a, c, d, e, f) {
        this.ff(a, [c, d, e, f]) && this.la.uniform4i(a, c, d, e, f)
    }, Toa: function (a, c) {
        this.ff(a, c) && this.la.uniform2iv(a, c)
    }, Woa: function (a, c) {
        this.ff(a, c) && this.la.uniform3iv(a, c)
    }, Zoa: function (a, c) {
        this.ff(a, c) && this.la.uniform4iv(a, c)
    }, Qoa: function (a, c) {
        this.DS(a, c)
    }, Pu: function (a, c) {
        this.ff(a, c) && this.la.uniform1f(a, c)
    }, R8: function (a, c, d) {
        this.ff(a, [c, d]) && this.la.uniform2f(a, c, d)
    }, S8: function (a, c, d, e) {
        this.ff(a, [c, d, e]) && this.la.uniform3f(a, c, d, e)
    }, zk: function (a, c, d, e, f) {
        this.ff(a,
            [c, d, e, f]) && this.la.uniform4f(a, c, d, e, f)
    }, Roa: function (a, c) {
        this.ff(a, c) && this.la.uniform2fv(a, c)
    }, Uoa: function (a, c) {
        this.ff(a, c) && this.la.uniform3fv(a, c)
    }, Xoa: function (a, c) {
        this.ff(a, c) && this.la.uniform4fv(a, c)
    }, ar: function (a, c) {
        this.ff(a, c) && this.la.uniformMatrix4fv(a, r, c)
    }, Poa: function () {
        if (!(2 > arguments.length))switch (arguments.length) {
            case 2:
                this.Pu(arguments[0], arguments[1]);
                break;
            case 3:
                this.R8(arguments[0], arguments[1], arguments[2]);
                break;
            case 4:
                this.S8(arguments[0], arguments[1], arguments[2],
                    arguments[3]);
                break;
            case 5:
                this.zk(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4])
        }
    }, Qu: function () {
        var a = new F.ea, c = new F.ea, d = new F.ea;
        F.hu(F.Gg, a);
        F.hu(F.Qf, c);
        F.lf(d, a, c);
        this.ar(this.$a[F.cs], a.c, 1);
        this.ar(this.$a[F.bs], c.c, 1);
        this.ar(this.$a[F.Co], d.c, 1);
        this.tF && (a = F.L, a = a.xt * a.Go, this.zk(this.$a[F.sw], a / 10, a, 2 * a, 4 * a), this.zk(this.$a[F.rw], a / 8, a / 4, a / 2, Math.sin(a)), this.zk(this.$a[F.qw], a / 8, a / 4, a / 2, Math.cos(a)));
        -1 != this.$a[F.ds] && this.zk(this.$a[F.ds], Math.random(), Math.random(),
            Math.random(), Math.random())
    }, Uca: function (a) {
        var c = new F.ea, d = new F.ea;
        F.hu(F.Gg, c);
        F.lf(d, c, a.Pb);
        this.ar(this.$a[F.cs], c.c, 1);
        this.ar(this.$a[F.bs], a.Pb.c, 1);
        this.ar(this.$a[F.Co], d.c, 1);
        this.tF && (a = F.L, a = a.xt * a.Go, this.zk(this.$a[F.sw], a / 10, a, 2 * a, 4 * a), this.zk(this.$a[F.rw], a / 8, a / 4, a / 2, Math.sin(a)), this.zk(this.$a[F.qw], a / 8, a / 4, a / 2, Math.cos(a)));
        -1 != this.$a[F.ds] && this.zk(this.$a[F.ds], Math.random(), Math.random(), Math.random(), Math.random())
    }, Noa: function () {
        this.la.uniformMatrix4fv(this.$a[F.Co],
            r, F.v4())
    }, Ooa: function (a) {
        F.lf(a, F.uk.top, F.rk.top);
        this.la.uniformMatrix4fv(this.$a[F.Co], r, a.c)
    }, qf: function () {
        this.la.uniformMatrix4fv(this.$a[F.bs], r, F.rk.top.c);
        this.la.uniformMatrix4fv(this.$a[F.cs], r, F.uk.top.c)
    }, Th: function (a) {
        a || b("modelView matrix is undefined.");
        this.la.uniformMatrix4fv(this.$a[F.bs], r, a.c);
        this.la.uniformMatrix4fv(this.$a[F.cs], r, F.uk.top.c)
    }, Z9: function () {
        return this.la.getShaderInfoLog(this.Gf)
    }, cia: function () {
        return this.la.getShaderInfoLog(this.Gf)
    }, rga: function () {
        return this.la.getShaderInfoLog(this.Gf)
    },
    M3: function () {
        return this.la.getShaderInfoLog(this.Rg)
    }, Xla: function () {
        return this.la.getProgramInfoLog(this.Tb)
    }, jha: function () {
        return this.la.getProgramInfoLog(this.Tb)
    }, reset: function () {
        this.Rg = this.Gf = q;
        this.$a.length = 0;
        this.la.deleteProgram(this.Tb);
        this.Tb = q;
        for (var a = 0; a < this.Hh.length; a++)this.Hh[a].value = q, this.Hh[a] = q;
        this.Hh.length = 0
    }, Pn: A("Tb"), $z: u(), oj: u()
});
F.ti.create = function (a, c) {
    return new F.ti(a, c)
};
F.rS = function (a, c) {
    a.shaderProgram = c;
    var d = a.children;
    if (d)for (var e = 0; e < d.length; e++)F.rS(d[e], c)
};
F.LL = -1;
F.wF = r;
F.vF = r;
F.xF = r;
F.zh && (F.lV = 16, F.ys = -1, F.xs = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1], F.No = -1, F.Mo = -1, F.lY = 0, F.TB && (F.TO = 0));
F.kia = function () {
    F.b6();
    F.LL = -1;
    F.wF = r;
    F.vF = r;
    F.xF = r;
    if (F.zh) {
        F.ys = -1;
        for (var a = 0; a < F.lV; a++)F.xs[a] = -1;
        F.No = -1;
        F.Mo = -1;
        F.lY = 0
    }
};
F.MG = function (a) {
    a !== F.ys && (F.ys = a, F.l.useProgram(a))
};
F.zh || (F.MG = function (a) {
    F.l.useProgram(a)
});
F.h5 = function (a) {
    F.zh && a === F.ys && (F.ys = -1);
    gl.deleteProgram(a)
};
F.pd = function (a, c) {
    if (a !== F.No || c !== F.Mo)F.No = a, F.Mo = c, F.fA(a, c)
};
F.fA = function (a, c) {
    var d = F.l;
    a === d.ONE && c === d.ZERO ? d.disable(d.BLEND) : (d.enable(d.BLEND), F.l.blendFunc(a, c))
};
F.cz = function (a, c) {
    if (a !== F.No || c !== F.Mo) {
        F.No = a;
        F.Mo = c;
        var d = F.l;
        a === d.ONE && c === d.ZERO ? d.disable(d.BLEND) : (d.enable(d.BLEND), d.blendFuncSeparate(d.SRC_ALPHA, c, a, c))
    }
};
F.zh || (F.pd = F.fA);
F.hia = function () {
    var a = F.l;
    a.blendEquation(a.FUNC_ADD);
    F.zh ? F.fA(F.No, F.Mo) : F.fA(a.Ac, a.zc)
};
F.sS = function () {
    F.LL = -1
};
F.Xb = function (a) {
    var c = F.l, d = a & F.Jd;
    d !== F.wF && (d ? c.enableVertexAttribArray(F.kb) : c.disableVertexAttribArray(F.kb), F.wF = d);
    d = a & F.Do;
    d !== F.vF && (d ? c.enableVertexAttribArray(F.sd) : c.disableVertexAttribArray(F.sd), F.vF = d);
    a &= F.Eo;
    a !== F.xF && (a ? c.enableVertexAttribArray(F.hd) : c.disableVertexAttribArray(F.hd), F.xF = a)
};
F.Cd = function (a) {
    F.bz(0, a)
};
F.bz = function (a, c) {
    if (F.xs[a] != c) {
        F.xs[a] = c;
        var d = F.l;
        d.activeTexture(d.TEXTURE0 + a);
        c ? d.bindTexture(d.TEXTURE_2D, c.ve) : d.bindTexture(d.TEXTURE_2D, q)
    }
};
F.zh || (F.bz = function (a, c) {
    var d = F.l;
    d.activeTexture(d.TEXTURE0 + a);
    c ? d.bindTexture(d.TEXTURE_2D, c.ve) : d.bindTexture(d.TEXTURE_2D, q)
});
F.iia = function (a) {
    F.i5(a)
};
F.i5 = function (a) {
    F.zh && a == F.xs[0] && (F.xs[0] = -1);
    F.l.deleteTexture(a)
};
F.gia = function (a) {
    F.TB && (F.zh && F.TO != a) && (F.TO = a)
};
F.jia = u();
F.qaa = 0;
F.raa = 1;
F.saa = 9;
F.Gk = function (a) {
    a -= 1;
    a |= a >> 1;
    a |= a >> 2;
    a |= a >> 4;
    a |= a >> 8;
    return (a | a >> 16) + 1
};
F.Ao = F.n.extend({
    sprite: q,
    Jt: 0,
    Hy: 0,
    yn: r,
    ae: q,
    oe: q,
    fx: 0,
    Gm: 0,
    Ni: 0,
    G: q,
    vt: q,
    C1: q,
    Pi: F.ha.xj,
    Vf: q,
    Iy: 0,
    SC: q,
    Mb: "RenderTexture",
    dca: q,
    ica: q,
    hca: q,
    cca: q,
    pca: q,
    ctor: q,
    us: function (a, c, d, e) {
        F.n.prototype.ctor.call(this);
        this.Uf = this.wf = p;
        this.Vf = F.color(255, 255, 255, 255);
        this.SC = "rgba(255,255,255,1)";
        this.ae = F.bc("canvas");
        this.oe = this.ae.getContext("2d");
        this.anchorY = this.anchorX = 0;
        a !== k && c !== k && (d = d || F.ha.xj, this.oz(a, c, d, e || 0))
    },
    de: function () {
        F.B === F.Y && (this.Z = new F.PJ(this))
    },
    vs: function (a, c, d, e) {
        F.n.prototype.ctor.call(this);
        this.Uf = this.wf = p;
        this.Vf = F.color(0, 0, 0, 0);
        a !== k && c !== k && (d = d || F.ha.xj, this.oz(a, c, d, e || 0))
    },
    jf: q,
    PY: function () {
        F.n.prototype.Cb.call(this);
        this.ae = this.oe = q
    },
    QY: function () {
        F.n.prototype.Cb.call(this);
        this.vt = q;
        var a = F.l;
        a.deleteFramebuffer(this.fx);
        this.Gm && a.deleteRenderbuffer(this.Gm);
        this.C1 = q
    },
    wQ: A("sprite"),
    qA: z("sprite"),
    oz: q,
    r_: function (a, c) {
        var d = this.ae, e = F.Fb();
        d.width = 0 | a * e;
        d.height = 0 | c * e;
        this.oe.translate(0, d.height);
        e = new F.ha;
        e.qd(d);
        e.Lb();
        d = this.sprite = new F.I(e);
        d.Vd(F.ONE, F.ONE_MINUS_SRC_ALPHA);
        this.yn = r;
        this.F(d);
        return p
    },
    s_: function (a, c, d, e) {
        d == F.ha.Nr && F.log("cc.RenderTexture._initWithWidthAndHeightForWebGL() : only RGB and RGBA formats are valid for a render texture;");
        var f = F.l, g = F.Fb();
        a = 0 | a * g;
        c = 0 | c * g;
        this.Ni = f.getParameter(f.FRAMEBUFFER_BINDING);
        var h;
        F.kq.my ? (g = a, h = c) : (g = F.Gk(a), h = F.Gk(c));
        for (var m = new Uint8Array(4 * g * h), n = 0; n < 4 * g * h; n++)m[n] = 0;
        this.Pi = d;
        this.G = new F.ha;
        if (!this.G)return r;
        n = this.G;
        n.au(m, this.Pi, g, h, F.size(a, c));
        d = f.getParameter(f.RENDERBUFFER_BINDING);
        if (F.kq.iq("GL_QCOM")) {
            this.vt =
                new F.ha;
            if (!this.vt)return r;
            this.vt.au(m, this.Pi, g, h, F.size(a, c))
        }
        this.fx = f.createFramebuffer();
        f.bindFramebuffer(f.FRAMEBUFFER, this.fx);
        f.framebufferTexture2D(f.FRAMEBUFFER, f.COLOR_ATTACHMENT0, f.TEXTURE_2D, n.ve, 0);
        0 != e && (this.Gm = f.createRenderbuffer(), f.bindRenderbuffer(f.RENDERBUFFER, this.Gm), f.renderbufferStorage(f.RENDERBUFFER, e, g, h), e == f.DEPTH_STENCIL ? f.framebufferRenderbuffer(f.FRAMEBUFFER, f.DEPTH_STENCIL_ATTACHMENT, f.RENDERBUFFER, this.Gm) : e == f.STENCIL_INDEX || e == f.STENCIL_INDEX8 ? f.framebufferRenderbuffer(f.FRAMEBUFFER,
            f.STENCIL_ATTACHMENT, f.RENDERBUFFER, this.Gm) : e == f.DEPTH_COMPONENT16 && f.framebufferRenderbuffer(f.FRAMEBUFFER, f.DEPTH_ATTACHMENT, f.RENDERBUFFER, this.Gm));
        f.checkFramebufferStatus(f.FRAMEBUFFER) !== f.FRAMEBUFFER_COMPLETE && F.log("Could not attach texture to the framebuffer");
        n.KH();
        a = this.sprite = new F.I(n);
        a.scaleY = -1;
        a.Vd(f.ONE, f.ONE_MINUS_SRC_ALPHA);
        f.bindRenderbuffer(f.RENDERBUFFER, d);
        f.bindFramebuffer(f.FRAMEBUFFER, this.Ni);
        this.yn = r;
        this.F(a);
        return p
    },
    hh: q,
    CY: function () {
        F.ka.Rp(this.ma)
    },
    DY: function () {
        F.ka.Rp(this.ma);
        F.Se(F.Gg);
        F.Sn();
        F.Se(F.Qf);
        F.Sn();
        var a = F.L;
        a.eo(a.Qn());
        var c = this.G.S, d = F.L.Xt(), a = d.width / c.width, d = d.height / c.height, e = F.l;
        e.viewport(0, 0, c.width, c.height);
        c = new F.ea;
        F.iH(c, -1 / a, 1 / a, -1 / d, 1 / d, -1, 1);
        F.Hq(c);
        this.Ni = e.getParameter(e.FRAMEBUFFER_BINDING);
        e.bindFramebuffer(e.FRAMEBUFFER, this.fx);
        F.kq.iq("GL_QCOM") && (e.framebufferTexture2D(e.FRAMEBUFFER, e.COLOR_ATTACHMENT0, e.TEXTURE_2D, this.vt.ve, 0), e.clear(e.COLOR_BUFFER_BIT | e.DEPTH_BUFFER_BIT), e.framebufferTexture2D(e.FRAMEBUFFER, e.COLOR_ATTACHMENT0,
            e.TEXTURE_2D, this.G.ve, 0))
    },
    A2: function (a, c, d, e, f, g) {
        var h = F.l;
        f = f || h.COLOR_BUFFER_BIT;
        g = g || h.COLOR_BUFFER_BIT | h.DEPTH_BUFFER_BIT;
        this.IC(a, c, d, e, f, g, h.COLOR_BUFFER_BIT | h.DEPTH_BUFFER_BIT | h.STENCIL_BUFFER_BIT)
    },
    IC: q,
    EY: function (a, c, d, e) {
        this.hh();
        a = a || 0;
        c = c || 0;
        d = d || 0;
        e = isNaN(e) ? 1 : e;
        var f = this.oe, g = this.ae;
        f.save();
        f.fillStyle = "rgba(" + (0 | a) + "," + (0 | c) + "," + (0 | d) + "," + e / 255 + ")";
        f.clearRect(0, 0, g.width, -g.height);
        f.fillRect(0, 0, g.width, -g.height);
        f.restore()
    },
    FY: function (a, c, d, e, f, g, h) {
        a /= 255;
        c /= 255;
        d /=
            255;
        e /= 255;
        this.hh();
        var m = F.l, n = [0, 0, 0, 0], s = 0, t = 0;
        h & m.COLOR_BUFFER_BIT && (n = m.getParameter(m.COLOR_CLEAR_VALUE), m.clearColor(a, c, d, e));
        h & m.DEPTH_BUFFER_BIT && (s = m.getParameter(m.DEPTH_CLEAR_VALUE), m.clearDepth(f));
        h & m.STENCIL_BUFFER_BIT && (t = m.getParameter(m.STENCIL_CLEAR_VALUE), m.clearStencil(g));
        m.clear(h);
        h & m.COLOR_BUFFER_BIT && m.clearColor(n[0], n[1], n[2], n[3]);
        h & m.DEPTH_BUFFER_BIT && m.clearDepth(s);
        h & m.STENCIL_BUFFER_BIT && m.clearStencil(t)
    },
    end: q,
    pZ: function () {
        F.ka.Ux(this.oe, this.ma)
    },
    qZ: function () {
        F.ka.y0(this.ma);
        var a = F.l, c = F.L;
        a.bindFramebuffer(a.FRAMEBUFFER, this.Ni);
        c.bI();
        F.Se(F.Gg);
        F.ok();
        F.Se(F.Qf);
        F.ok()
    },
    clear: function (a, c, d, e) {
        this.A2(a, c, d, e);
        this.end()
    },
    clearRect: q,
    TY: function (a, c, d, e) {
        this.oe.clearRect(a, c, d, -e)
    },
    UY: u(),
    clearDepth: q,
    RY: function () {
        F.log("clearDepth isn't supported on Cocos2d-Html5")
    },
    SY: function (a) {
        this.hh();
        var c = F.l, d = c.getParameter(c.DEPTH_CLEAR_VALUE);
        c.clearDepth(a);
        c.clear(c.DEPTH_BUFFER_BIT);
        c.clearDepth(d);
        this.end()
    },
    clearStencil: q,
    VY: function () {
        F.log("clearDepth isn't supported on Cocos2d-Html5")
    },
    WY: function (a) {
        var c = F.l, d = c.getParameter(c.STENCIL_CLEAR_VALUE);
        c.clearStencil(a);
        c.clear(c.STENCIL_BUFFER_BIT);
        c.clearStencil(d)
    },
    H: q,
    sn: function (a) {
        this.Jc && (a = a || F.l, this.transform(a), this.sprite.H(a))
    },
    Bt: function (a) {
        this.Jc && (F.Sn(), this.transform(a), this.sprite.H(), this.Z && F.ka.xc(this.Z), F.ok())
    },
    Ka: q,
    Zo: function (a) {
        a = a || F.l;
        if (this.yn) {
            this.hh();
            if (this.Jt) {
                var c = this.ae;
                a.save();
                a.fillStyle = this.SC;
                a.clearRect(0, 0, c.width, -c.height);
                a.fillRect(0, 0, c.width, -c.height);
                a.restore()
            }
            this.Tc();
            a = this.u;
            for (var c = a.length, d = this.sprite, e = 0; e < c; e++) {
                var f = a[e];
                f != d && f.H()
            }
            this.end()
        }
    },
    Fj: function () {
        var a = F.l;
        if (this.yn) {
            this.hh();
            var c = this.Jt;
            if (c) {
                var d = [0, 0, 0, 0], e = 0, f = 0;
                c & a.COLOR_BUFFER_BIT && (d = a.getParameter(a.COLOR_CLEAR_VALUE), a.clearColor(this.Vf.r / 255, this.Vf.g / 255, this.Vf.b / 255, this.Vf.a / 255));
                c & a.DEPTH_BUFFER_BIT && (e = a.getParameter(a.DEPTH_CLEAR_VALUE), a.clearDepth(this.Hy));
                c & a.STENCIL_BUFFER_BIT && (f = a.getParameter(a.STENCIL_CLEAR_VALUE), a.clearStencil(this.Iy));
                a.clear(c);
                c & a.COLOR_BUFFER_BIT &&
                a.clearColor(d[0], d[1], d[2], d[3]);
                c & a.DEPTH_BUFFER_BIT && a.clearDepth(e);
                c & a.STENCIL_BUFFER_BIT && a.clearStencil(f)
            }
            this.Tc();
            a = this.u;
            for (c = 0; c < a.length; c++)d = a[c], d != this.sprite && d.H();
            this.end()
        }
    },
    Yka: function () {
        F.log("saveToFile isn't supported on cocos2d-html5");
        return q
    },
    Gca: function (a, c, d, e, f) {
        for (var g = 0; g < f; g++)a[c + g] = d[e + g]
    },
    Pma: function () {
        F.log("saveToFile isn't supported on Cocos2d-Html5")
    },
    Nka: function () {
        F.log("listenToBackground isn't supported on Cocos2d-Html5")
    },
    Oka: function () {
        F.log("listenToForeground isn't supported on Cocos2d-Html5")
    },
    Ofa: A("Jt"),
    jna: z("Jt"),
    V3: A("Vf"),
    MH: q,
    G0: function (a) {
        var c = this.Vf;
        c.r = a.r;
        c.g = a.g;
        c.b = a.b;
        c.a = a.a;
        this.SC = "rgba(" + (0 | a.r) + "," + (0 | a.g) + "," + (0 | a.b) + "," + a.a / 255 + ")"
    },
    H0: function (a) {
        var c = this.Vf;
        c.r = a.r;
        c.g = a.g;
        c.b = a.b;
        c.a = a.a
    },
    Nfa: A("Hy"),
    ina: z("Hy"),
    Pfa: A("Iy"),
    kna: z("Iy"),
    wia: A("yn"),
    bna: z("yn")
});
M = F.Ao.prototype;
F.B == F.Y ? (M.ctor = M.vs, M.jf = M.QY, M.oz = M.s_, M.hh = M.DY, M.IC = M.FY, M.end = M.qZ, M.clearRect = M.UY, M.clearDepth = M.SY, M.clearStencil = M.WY, M.H = M.Bt, M.Ka = M.Fj, M.MH = M.H0) : (M.ctor = M.us, M.jf = M.PY, M.oz = M.r_, M.hh = M.CY, M.IC = M.EY, M.end = M.pZ, M.clearRect = M.TY, M.clearDepth = M.RY, M.clearStencil = M.VY, M.H = M.sn, M.Ka = M.Zo, M.MH = M.G0);
F.k(M, "clearColorVal", M.V3, M.MH);
F.Ao.create = function (a, c, d, e) {
    return new F.Ao(a, c, d, e)
};
F.ui = F.Bk.extend({
    Zb: q, KD: q, Ca: r, Mb: "LabelAtlas", ctor: function (a, c, d, e, f) {
        F.Bk.prototype.ctor.call(this);
        this.wf = this.Uf = p;
        c && F.ui.prototype.Td.call(this, a, c, d, e, f)
    }, Vu: A("Ca"), Ft: function (a, c) {
        this.addEventListener("load", a, c)
    }, Td: function (a, c, d, e, f) {
        var g = a + "", h, m;
        if (d === k) {
            d = F.aa.ge(c);
            if (1 !== parseInt(d.version, 10))return F.log("cc.LabelAtlas.initWithString(): Unsupported version. Upgrade cocos2d version"), r;
            c = F.path.hq(c, d.textureFilename);
            e = F.Fb();
            h = parseInt(d.itemWidth, 10) / e;
            m = parseInt(d.itemHeight,
                    10) / e;
            d = String.fromCharCode(parseInt(d.firstChar, 10))
        } else h = d || 0, m = e || 0, d = f || " ";
        var n = q, n = c instanceof F.ha ? c : F.Ra.ad(c);
        (this.Ca = c = n.Jb) || n.addEventListener("load", function () {
            this.Fa(n, h, m, g.length);
            this.string = g;
            this.dispatchEvent("load")
        }, this);
        return this.Fa(n, h, m, g.length) ? (this.KD = d, this.string = g, p) : r
    }, rb: function (a) {
        F.Bk.prototype.rb.call(this, a);
        this.mr()
    }, Ll: A("Zb"), Ka: function (a) {
        F.Bk.prototype.Ka.call(this, a);
        F.$U && (a = this.size, a = [F.d(0, 0), F.d(a.width, 0), F.d(a.width, a.height), F.d(0, a.height)],
            F.Ge.ye(a, 4, p))
    }, AC: function (a, c, d) {
        a.F_ = p;
        F.n.prototype.F.call(this, a, c, d)
    }, mr: q, D1: function () {
        for (var a = this.Zb || "", c = a.length, d = this.texture, e = this.Ii, f = this.Lh, g = 0; g < c; g++) {
            var h = a.charCodeAt(g) - this.KD.charCodeAt(0), h = F.rect(parseInt(h % this.up, 10) * e, parseInt(h / this.up, 10) * f, e, f), m = a.charCodeAt(g), n = this.di(g);
            n ? 32 == m ? (n.oa(), n.Db(F.rect(0, 0, 10, 10), r, F.size(0, 0))) : (n.Fa(d, h), n.visible = p) : (n = new F.I, 32 == m ? (n.oa(), n.Db(F.rect(0, 0, 10, 10), r, F.size(0, 0))) : n.Fa(d, h), F.n.prototype.F.call(this, n, 0, g));
            n.X(g * e + e / 2, f / 2)
        }
    }, E1: function () {
        var a = this.Zb, c = a.length, d = this.textureAtlas, e = d.texture, f = e.pixelsWidth, e = e.pixelsHeight, g = this.Ii, h = this.Lh;
        this.ox || (g = this.Ii * F.Fb(), h = this.Lh * F.Fb());
        c > d.ud && F.log("cc.LabelAtlas._updateAtlasValues(): Invalid String length");
        for (var m = d.quads, n = this.wa, n = {r: n.r, g: n.g, b: n.b, a: this.ab}, s = this.Ii, t = 0; t < c; t++) {
            var v = a.charCodeAt(t) - this.KD.charCodeAt(0), w = v % this.up, x = 0 | v / this.up, y;
            F.ro ? (w = (2 * w * g + 1) / (2 * f), v = w + (2 * g - 2) / (2 * f), x = (2 * x * h + 1) / (2 * e), y = x + (2 * h - 2) / (2 * e)) : (w = w *
                g / f, v = w + g / f, x = x * h / e, y = x + h / e);
            var B = m[t], I = B.U, E = B.O, L = B.K, B = B.V;
            I.p.pa = w;
            I.p.qa = x;
            E.p.pa = v;
            E.p.qa = x;
            L.p.pa = w;
            L.p.qa = y;
            B.p.pa = v;
            B.p.qa = y;
            L.j.x = t * s;
            L.j.y = 0;
            L.j.e = 0;
            B.j.x = t * s + s;
            B.j.y = 0;
            B.j.e = 0;
            I.j.x = t * s;
            I.j.y = this.Lh;
            I.j.e = 0;
            E.j.x = t * s + s;
            E.j.y = this.Lh;
            E.j.e = 0;
            I.A = n;
            E.A = n;
            L.A = n;
            B.A = n
        }
        0 < c && (d.dirty = p, a = d.totalQuads, c > a && d.JQ(c - a))
    }, yc: q, d1: function (a) {
        a = String(a);
        var c = a.length;
        this.Zb = a;
        this.width = c * this.Ii;
        this.height = this.Lh;
        if (this.u) {
            a = this.u;
            for (var c = a.length, d = 0; d < c; d++) {
                var e = a[d];
                e && !e.F_ && (e.visible =
                    r)
            }
        }
        this.mr();
        this.quadsToDraw = c
    }, f1: function (a) {
        a = String(a);
        var c = a.length;
        c > this.textureAtlas.totalQuads && this.textureAtlas.Zz(c);
        this.Zb = a;
        this.width = c * this.Ii;
        this.height = this.Lh;
        this.mr();
        this.quadsToDraw = c
    }, wb: q, OE: function (a) {
        this.Je !== a && F.Bk.prototype.wb.call(this, a)
    }
});
M = F.ui.prototype;
F.Ah.prototype.apply(M);
F.B === F.Y ? (M.mr = M.E1, M.yc = M.f1, M.wb = M.OE) : (M.mr = M.D1, M.yc = M.d1, M.wb = M.uO, M.F = M.AC);
F.k(M, "opacity", M.jh, M.wb);
F.k(M, "color", M.mg, M.rb);
F.k(M, "string", M.Ll, M.yc);
F.ui.create = function (a, c, d, e, f) {
    return new F.ui(a, c, d, e, f)
};
F.zaa = -1;
F.iB = F.We.extend({
    qb: r,
    Zb: "",
    Wf: q,
    ix: "",
    qp: "",
    js: F.Wr,
    Ct: -1,
    AN: r,
    px: q,
    jO: q,
    ab: 255,
    Xg: 255,
    wa: q,
    te: q,
    wf: p,
    Uf: p,
    Ca: r,
    Mb: "LabelBMFont",
    vO: function (a, c) {
        c ? this.qp = a : this.Zb = a;
        var d = this.u;
        if (d)for (var e = 0; e < d.length; e++) {
            var f = d[e];
            f && f.ke(r)
        }
        this.Ca && (this.My(), c && this.Ag())
    },
    ctor: function (a, c, d, e, f) {
        F.We.prototype.ctor.call(this);
        this.px = F.d(0, 0);
        this.wa = F.color(255, 255, 255, 255);
        this.te = F.color(255, 255, 255, 255);
        this.jO = [];
        this.Td(a, c, d, e, f)
    },
    Vu: A("Ca"),
    Ft: function (a, c) {
        this.addEventListener("load", a,
            c)
    },
    Ka: function (a) {
        F.We.prototype.Ka.call(this, a);
        if (F.aV) {
            a = this.$();
            var c = F.d(0 | -this.Ib.x, 0 | -this.Ib.y);
            a = [F.d(c.x, c.y), F.d(c.x + a.width, c.y), F.d(c.x + a.width, c.y + a.height), F.d(c.x, c.y + a.height)];
            F.Ge.Nf(0, 255, 0, 255);
            F.Ge.ye(a, 4, p)
        }
    },
    rb: function (a) {
        var c = this.wa, d = this.te;
        d.r == a.r && d.g == a.g && d.b == a.b && d.a == a.a || (c.r = d.r = a.r, c.g = d.g = a.g, c.b = d.b = a.b, this.Ca && this.wf && (a = F.color.WHITE, (c = this.Ja) && c.cascadeColor && (a = c.Sy()), this.ed(a)))
    },
    ng: A("qb"),
    Of: function (a) {
        this.qb = a;
        if (a = this.u)for (var c = 0; c <
        a.length; c++) {
            var d = a[c];
            d && (d.opacityModifyRGB = this.qb)
        }
    },
    jh: A("Xg"),
    d4: A("ab"),
    wb: function (a) {
        this.ab = this.Xg = a;
        if (this.Uf) {
            var c = 255, d = this.Ja;
            d && d.cascadeOpacity && (c = d.ab);
            this.Uc(c)
        }
        this.wa.a = this.te.a = a
    },
    Uc: function (a) {
        this.ab = this.Xg * a / 255;
        a = this.u;
        for (var c = 0; c < a.length; c++) {
            var d = a[c];
            F.B == F.Y ? d.Uc(this.ab) : (F.n.prototype.Uc.call(d, this.ab), d.Qa())
        }
        this.De()
    },
    $Q: D(r),
    fS: z("Uf"),
    mg: function () {
        var a = this.te;
        return F.color(a.r, a.g, a.b, a.a)
    },
    Sy: function () {
        var a = this.wa;
        return F.color(a.r, a.g,
            a.b, a.a)
    },
    ed: function (a) {
        var c = this.wa, d = this.te;
        c.r = d.r * a.r / 255;
        c.g = d.g * a.g / 255;
        c.b = d.b * a.b / 255;
        a = this.u;
        for (c = 0; c < a.length; c++)d = a[c], F.B == F.Y ? d.ed(this.wa) : (F.n.prototype.ed.call(d, this.wa), d.Qa());
        this.De()
    },
    De: function () {
        if (F.B != F.Y) {
            var a = this.Ma();
            if (a && 0 < a.$().width) {
                var c = this.Fc.Xa;
                if (c) {
                    var d = a.Xa, e = F.rect(0, 0, c.width, c.height);
                    d instanceof HTMLCanvasElement && !this.Yc ? F.Mn(c, this.wa, e, d) : (d = F.Mn(c, this.wa, e), a = new F.ha, a.qd(d), a.Lb());
                    this.ib(a)
                }
            }
        }
    },
    ZQ: D(r),
    eS: z("wf"),
    oa: function () {
        return this.Td(q,
            q, q, q, q)
    },
    Td: function (a, c, d, e, f) {
        a = a || "";
        this.Wf && F.log("cc.LabelBMFont.initWithString(): re-init is no longer supported");
        if (c) {
            var g = F.aa.ge(c);
            if (!g)return F.log("cc.LabelBMFont.initWithString(): Impossible to create font. Please check file"), r;
            this.Wf = g;
            this.ix = c;
            c = F.Ra.ad(g.gP);
            (this.Ca = g = c.Jb) || c.addEventListener("load", function (a) {
                this.Ca = p;
                this.Fa(a, this.qp.length);
                this.yc(this.qp, p);
                this.dispatchEvent("load")
            }, this)
        } else c = new F.ha, g = new Image, c.qd(g), this.Ca = r;
        return this.Fa(c, a.length) ?
            (this.js = e || F.ew, this.px = f || F.d(0, 0), this.Ct = d == q ? -1 : d, this.ab = this.Xg = 255, this.wa = F.color(255, 255, 255, 255), this.te = F.color(255, 255, 255, 255), this.wf = this.Uf = p, this.S.width = 0, this.S.height = 0, this.Zl(0.5, 0.5), F.B === F.Y && (d = this.textureAtlas.texture, this.qb = d.gi(), e = this.jO = new F.I, e.Fa(d, F.rect(0, 0, 0, 0), r), e.batchNode = this), this.yc(a, p), p) : r
    },
    My: function () {
        var a = F.B, c = a === F.Aa ? this.texture : this.textureAtlas.texture, d = 0, e = F.size(0, 0), f = 0, g = 1, h = this.Zb, m = h ? h.length : 0;
        if (0 !== m) {
            var n, s = this.Wf, t = s.V5,
                v = s.XF, w = s.L3;
            for (n = 0; n < m - 1; n++)10 == h.charCodeAt(n) && g++;
            var x = v * g, g = -(v - v * g), y = -1;
            for (n = 0; n < m; n++)if (v = h.charCodeAt(n), 0 != v)if (10 === v)d = 0, g -= s.XF; else {
                var B = t[y << 16 | v & 65535] || 0, I = w[v];
                if (I) {
                    var E = F.rect(I.rect.x, I.rect.y, I.rect.width, I.rect.height), E = F.Xl(E);
                    E.x += this.px.x;
                    E.y += this.px.y;
                    (y = this.di(n)) ? 32 === v && a === F.Aa ? y.Db(E, r, F.size(0, 0)) : (y.Db(E, r), y.visible = p) : (y = new F.I, 32 === v && a === F.Aa && (E = F.rect(0, 0, 0, 0)), y.Fa(c, E, r), y.Cx = p, this.F(y, 0, n));
                    y.opacityModifyRGB = this.qb;
                    F.B == F.Y ? (y.ed(this.wa),
                        y.Uc(this.ab)) : (F.n.prototype.ed.call(y, this.wa), F.n.prototype.Uc.call(y, this.ab), y.Qa());
                    E = F.d(d + I.kT + 0.5 * I.rect.width + B, g + (s.XF - I.lT) - 0.5 * E.height * F.Fb());
                    y.X(F.xH(E));
                    d += I.nI + B;
                    y = v;
                    f < d && (f = d)
                } else F.log("cocos2d: LabelBMFont: character not found " + h[n])
            }
            e.width = I && I.nI < I.rect.width ? f - I.nI + I.rect.width : f;
            e.height = x;
            this.ze(F.FS(e))
        }
    },
    Jpa: function (a) {
        var c = this.u;
        if (c)for (var d = 0, e = c.length; d < e; d++) {
            var f = c[d];
            f && (f.visible = r)
        }
        this.Wf && this.My();
        a || this.Ag()
    },
    Ll: A("qp"),
    yc: function (a, c) {
        a = String(a);
        c == q && (c = p);
        if (a == q || !F.Dd(a))a += "";
        this.qp = a;
        this.vO(a, c)
    },
    e1: function (a) {
        this.yc(a, r)
    },
    fna: function (a) {
        this.yc(a, p)
    },
    Ag: function () {
        this.string = this.qp;
        if (0 < this.Ct) {
            for (var a = this.Zb.length, c = [], d = [], e = 1, f = 0, g = r, h = r, m = -1, n = -1, s = 0, t, v = 0, w = this.u.length; v < w; v++) {
                for (var x = 0; !(t = this.di(v + s + x));)x++;
                s += x;
                if (f >= a)break;
                var y = this.Zb[f];
                h || (n = this.oD(t), h = p);
                g || (m = n, g = p);
                if (10 == y.charCodeAt(0)) {
                    d.push("\n");
                    c = c.concat(d);
                    d.length = 0;
                    g = h = r;
                    m = n = -1;
                    v--;
                    s -= x;
                    e++;
                    if (f >= a)break;
                    n || (n = this.oD(t), h = p);
                    m || (m =
                        n, g = p);
                    f++
                } else if (this.FD(y))d.push(y), c = c.concat(d), d.length = 0, h = r, n = -1, f++; else if (this.QZ(t) - m > this.Ct)if (this.AN) {
                    this.$O(d);
                    d.push("\n");
                    c = c.concat(d);
                    d.length = 0;
                    g = h = r;
                    m = n = -1;
                    e++;
                    if (f >= a)break;
                    n || (n = this.oD(t), h = p);
                    m || (m = n, g = p);
                    v--
                } else d.push(y), -1 != c.lastIndexOf(" ") ? this.$O(c) : c = [], 0 < c.length && c.push("\n"), e++, g = r, m = -1, f++; else d.push(y), f++
            }
            c = c.concat(d);
            v = c.length;
            t = "";
            for (f = 0; f < v; ++f)t += c[f];
            t += String.fromCharCode(0);
            this.vO(t, r)
        }
        if (this.js != F.ew) {
            c = f = 0;
            a = this.Zb.length;
            d = [];
            for (e = 0; e <
            a; e++)if (10 == this.Zb[e].charCodeAt(0) || 0 == this.Zb[e].charCodeAt(0))if (v = 0, g = d.length, 0 == g)c++; else {
                if (t = f + g - 1 + c, !(0 > t) && (v = this.di(t), v != q)) {
                    v = v.$b() + v.Gh() / 2;
                    h = 0;
                    switch (this.js) {
                        case F.Wr:
                            h = this.width / 2 - v / 2;
                            break;
                        case F.iK:
                            h = this.width - v
                    }
                    if (0 != h)for (v = 0; v < g; v++)if (t = f + v + c, !(0 > t) && (t = this.di(t)))t.x += h;
                    f += g;
                    c++;
                    d.length = 0
                }
            } else d.push(this.Zb[f])
        }
    },
    U7: function (a) {
        this.js = a;
        this.Ag()
    },
    AZ: A("js"),
    Y7: function (a) {
        this.Ct = a;
        this.Ag()
    },
    kD: A("Ct"),
    Rna: function (a) {
        this.AN = a;
        this.Ag()
    },
    $q: function (a, c) {
        F.n.prototype.$q.call(this,
            a, c);
        this.Ag()
    },
    Mu: function (a) {
        F.n.prototype.Mu.call(this, a);
        this.Ag()
    },
    Nu: function (a) {
        F.n.prototype.Nu.call(this, a);
        this.Ag()
    },
    Ana: function (a) {
        if (a != q && a != this.ix) {
            var c = F.aa.ge(a);
            c ? (this.ix = a, this.Wf = c, a = F.Ra.ad(c.gP), this.Ca = c = a.Jb, this.texture = a, F.B === F.Aa && (this.Fc = this.texture), c ? this.My() : a.addEventListener("load", function (a) {
                this.Ca = p;
                this.texture = a;
                this.My();
                this.De();
                this.Ag();
                this.dispatchEvent("load")
            }, this)) : F.log("cc.LabelBMFont.setFntFile() : Impossible to create font. Please check file")
        }
    },
    pga: A("ix"),
    Zl: function (a, c) {
        F.n.prototype.Zl.call(this, a, c);
        this.Ag()
    },
    sO: function (a) {
        F.n.prototype.sO.call(this, a);
        this.Ag()
    },
    HE: function (a) {
        F.n.prototype.HE.call(this, a);
        this.Ag()
    },
    IE: function (a) {
        F.n.prototype.IE.call(this, a);
        this.Ag()
    },
    bca: u(),
    Aca: function (a, c) {
        var d = 0;
        if (this.ZY.W5) {
            var e = this.ZY.W5[(a << 16 | c & 65535).toString()];
            e && (d = e.zda)
        }
        return d
    },
    oD: function (a) {
        return a.$b() * this.sa - a.Gh() * this.sa * a.jd.x
    },
    QZ: function (a) {
        return a.$b() * this.sa + a.Gh() * this.sa * a.jd.x
    },
    FD: function (a) {
        a = a.charCodeAt(0);
        return 9 <= a && 13 >= a || 32 == a || 133 == a || 160 == a || 5760 == a || 8192 <= a && 8202 >= a || 8232 == a || 8233 == a || 8239 == a || 8287 == a || 12288 == a
    },
    $O: function (a) {
        var c = a.length;
        if (!(0 >= c) && (c -= 1, this.FD(a[c]))) {
            for (var d = c - 1; 0 <= d; --d)if (this.FD(a[d]))c = d; else break;
            this.N1(a, c)
        }
    },
    N1: function (a, c) {
        var d = a.length;
        c >= d || 0 > c || a.splice(c, d)
    }
});
M = F.iB.prototype;
F.Ah.prototype.apply(M);
F.B === F.Aa && (F.ta.hy || (M.De = function () {
    if (F.B != F.Y) {
        var a, c = this.Ma();
        if (c && 0 < c.$().width && (a = c.Xa)) {
            var d = F.Ra.Vt(this.Fc.Xa);
            d && (a instanceof HTMLCanvasElement && !this.Yc ? F.Il(a, d, this.wa, q, a) : (a = F.Il(a, d, this.wa), c = new F.ha, c.qd(a), c.Lb()), this.ib(c))
        }
    }
}), M.ib = function (a) {
    for (var c = this.u, d = this.wa, e = 0; e < c.length; e++) {
        var f = c[e], g = f.wa;
        this.Zi != f.G && (g.r !== d.r || g.g !== d.g || g.b !== d.b) || (f.texture = a)
    }
    this.Zi = a
});
F.k(M, "string", M.Ll, M.e1);
F.k(M, "boundingWidth", M.kD, M.Y7);
F.k(M, "textAlign", M.AZ, M.U7);
F.iB.create = function (a, c, d, e, f) {
    return new F.iB(a, c, d, e, f)
};
F.tZ = {
    LU: /info [^\n]*(\n|$)/gi,
    PT: /common [^\n]*(\n|$)/gi,
    JV: /page [^\n]*(\n|$)/gi,
    OT: /char [^\n]*(\n|$)/gi,
    SU: /kerning [^\n]*(\n|$)/gi,
    NU: /\w+=[^ \r\n]+/gi,
    MU: /^[\-]?\d+$/,
    at: function (a) {
        a = a.match(this.NU);
        var c = {};
        if (a)for (var d = 0, e = a.length; d < e; d++) {
            var f = a[d], g = f.indexOf("\x3d"), h = f.substring(0, g), f = f.substring(g + 1);
            f.match(this.MU) ? f = parseInt(f) : '"' == f[0] && (f = f.substring(1, f.length - 1));
            c[h] = f
        }
        return c
    },
    V6: function (a, c) {
        var d = {};
        this.at(a.match(this.LU)[0]);
        var e = this.at(a.match(this.PT)[0]);
        d.XF = e.lineHeight;
        if (F.B === F.Y) {
            var f = F.kq.Ws;
            (e.scaleW > f.width || e.scaleH > f.height) && F.log("cc.LabelBMFont._parseCommonArguments(): page can't be larger than supported")
        }
        1 !== e.pages && F.log("cc.LabelBMFont._parseCommonArguments(): only supports 1 page");
        e = this.at(a.match(this.JV)[0]);
        0 !== e.id && F.log("cc.LabelBMFont._parseImageFileName() : file could not be found");
        d.gP = F.path.hq(c, e.file);
        for (var g = a.match(this.OT), h = d.L3 = {}, e = 0, f = g.length; e < f; e++) {
            var m = this.at(g[e]);
            h[m.id] = {
                rect: {
                    x: m.x, y: m.y, width: m.width,
                    height: m.height
                }, kT: m.xoffset, lT: m.yoffset, nI: m.xadvance
            }
        }
        g = d.V5 = {};
        if (h = a.match(this.SU)) {
            e = 0;
            for (f = h.length; e < f; e++)m = this.at(h[e]), g[m.first << 16 | m.second & 65535] = m.amount
        }
        return d
    },
    load: function (a, c, d, e) {
        var f = this;
        F.aa.Fz(a, function (a, d) {
            if (a)return e(a);
            e(q, f.V6(d, c))
        })
    }
};
F.aa.nj(["fnt"], F.tZ);
F.gJ = F.n.extend({
    texture: q,
    qq: r,
    qj: r,
    q: q,
    st: 0,
    eD: 0,
    Rm: 0,
    ND: 0,
    Mi: 0,
    nE: 0,
    hE: q,
    gE: q,
    bb: q,
    Em: q,
    Ff: q,
    nd: q,
    Fm: q,
    kn: q,
    Mb: "MotionStreak",
    ctor: function (a, c, d, e, f) {
        F.n.prototype.ctor.call(this);
        this.Wg = F.d(0, 0);
        this.q = new F.dc(F.SRC_ALPHA, F.ONE_MINUS_SRC_ALPHA);
        this.yy = F.l.createBuffer();
        this.qj = this.qq = r;
        this.texture = q;
        this.nE = this.Mi = this.ND = this.Rm = this.eD = this.st = 0;
        this.kn = this.Fm = this.nd = this.Ff = this.Em = this.bb = this.gE = this.hE = q;
        f !== k && this.u5(a, c, d, e, f)
    },
    de: function () {
        this.Z = new F.hJ(this)
    },
    Ma: A("texture"),
    ib: function (a) {
        this.texture != a && (this.texture = a)
    },
    Jf: A("q"),
    Vd: function (a, c) {
        c === k ? this.q = a : (this.q.src = a, this.q.J = c)
    },
    jh: function () {
        F.log("cc.MotionStreak.getOpacity has not been supported.");
        return 0
    },
    wb: function () {
        F.log("cc.MotionStreak.setOpacity has not been supported.")
    },
    Of: u(),
    ng: D(r),
    Cb: function () {
        F.n.prototype.Cb.call(this);
        this.nd && F.l.deleteBuffer(this.nd);
        this.kn && F.l.deleteBuffer(this.kn);
        this.Fm && F.l.deleteBuffer(this.Fm)
    },
    Eia: A("qq"),
    zna: z("qq"),
    Qia: A("qj"),
    woa: z("qj"),
    u5: function (a,
                  c, d, e, f) {
        f || b("cc.MotionStreak.initWithFade(): Invalid filename or texture");
        F.Dd(f) && (f = F.Ra.ad(f));
        F.n.prototype.X.call(this, F.d(0, 0));
        this.anchorY = this.anchorX = 0;
        this.ignoreAnchor = p;
        this.qj = r;
        this.qq = p;
        this.Rm = -1 == c ? d / 5 : c;
        this.Rm *= this.Rm;
        this.st = d;
        this.eD = 1 / a;
        a = (0 | 60 * a) + 2;
        this.Mi = 0;
        this.gE = new Float32Array(a);
        this.hE = new Float32Array(2 * a);
        this.bb = new Float32Array(4 * a);
        this.Ff = new Float32Array(4 * a);
        this.Em = new Uint8Array(8 * a);
        this.ND = a;
        a = F.l;
        this.nd = a.createBuffer();
        this.kn = a.createBuffer();
        this.Fm =
            a.createBuffer();
        this.q.src = a.SRC_ALPHA;
        this.q.J = a.ONE_MINUS_SRC_ALPHA;
        this.shaderProgram = F.le.Kc(F.yj);
        this.texture = f;
        this.color = e;
        this.Yq();
        a.bindBuffer(a.ARRAY_BUFFER, this.nd);
        a.bufferData(a.ARRAY_BUFFER, this.bb, a.DYNAMIC_DRAW);
        a.bindBuffer(a.ARRAY_BUFFER, this.kn);
        a.bufferData(a.ARRAY_BUFFER, this.Ff, a.DYNAMIC_DRAW);
        a.bindBuffer(a.ARRAY_BUFFER, this.Fm);
        a.bufferData(a.ARRAY_BUFFER, this.Em, a.DYNAMIC_DRAW);
        return p
    },
    Dpa: function (a) {
        this.color = a;
        for (var c = this.Em, d = 0, e = 2 * this.Mi; d < e; d++)c[4 * d] = a.r,
            c[4 * d + 1] = a.g, c[4 * d + 2] = a.b
    },
    reset: function () {
        this.Mi = 0
    },
    X: function (a, c) {
        this.qj = p;
        c === k ? (this.Wg.x = a.x, this.Wg.y = a.y) : (this.Wg.x = a, this.Wg.y = c)
    },
    $b: function () {
        return this.Wg.x
    },
    qS: function (a) {
        this.Wg.x = a;
        this.qj || (this.qj = p)
    },
    va: function () {
        return this.Wg.y
    },
    am: function (a) {
        this.Wg.y = a;
        this.qj || (this.qj = p)
    },
    Ka: function (a) {
        !(1 >= this.Mi) && (this.texture && this.texture.Jb) && (a = a || F.l, F.nu(this), F.Xb(F.Bh), F.pd(this.q.src, this.q.J), F.Cd(this.texture), a.bindBuffer(a.ARRAY_BUFFER, this.nd), a.bufferData(a.ARRAY_BUFFER,
            this.bb, a.DYNAMIC_DRAW), a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, 0, 0), a.bindBuffer(a.ARRAY_BUFFER, this.kn), a.bufferData(a.ARRAY_BUFFER, this.Ff, a.DYNAMIC_DRAW), a.vertexAttribPointer(F.hd, 2, a.FLOAT, r, 0, 0), a.bindBuffer(a.ARRAY_BUFFER, this.Fm), a.bufferData(a.ARRAY_BUFFER, this.Em, a.DYNAMIC_DRAW), a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, 0, 0), a.drawArrays(a.TRIANGLE_STRIP, 0, 2 * this.Mi), F.bd++)
    },
    update: function (a) {
        if (this.qj) {
            a *= this.eD;
            var c, d, e, f, g = 0, h = this.Mi, m = this.gE, n = this.hE, s = this.bb, t = this.Em;
            for (e = 0; e < h; e++)m[e] -= a, 0 >= m[e] ? g++ : (c = e - g, 0 < g ? (m[c] = m[e], n[2 * c] = n[2 * e], n[2 * c + 1] = n[2 * e + 1], f = 2 * e, d = 2 * c, s[2 * d] = s[2 * f], s[2 * d + 1] = s[2 * f + 1], s[2 * (d + 1)] = s[2 * (f + 1)], s[2 * (d + 1) + 1] = s[2 * (f + 1) + 1], f *= 4, d *= 4, t[d + 0] = t[f + 0], t[d + 1] = t[f + 1], t[d + 2] = t[f + 2], t[d + 4] = t[f + 4], t[d + 5] = t[f + 5], t[d + 6] = t[f + 6]) : d = 8 * c, c = 255 * m[c], t[d + 3] = c, t[d + 7] = c);
            h -= g;
            e = p;
            if (h >= this.ND)e = r; else if (0 < h && (a = F.yR(F.d(n[2 * (h - 1)], n[2 * (h - 1) + 1]), this.Wg) < this.Rm, d = 1 == h ? r : F.yR(F.d(n[2 * (h - 2)], n[2 * (h - 2) + 1]), this.Wg) < 2 * this.Rm, a || d))e = r;
            e && (n[2 * h] = this.Wg.x, n[2 *
            h + 1] = this.Wg.y, m[h] = 1, m = 8 * h, e = this.wa, t[m] = e.r, t[m + 1] = e.g, t[m + 2] = e.b, t[m + 4] = e.r, t[m + 5] = e.g, t[m + 6] = e.b, t[m + 3] = 255, t[m + 7] = 255, 0 < h && this.qq && (1 < h ? F.mI(n, this.st, this.bb, h, 1) : F.mI(n, this.st, this.bb, 0, 2)), h++);
            this.qq || F.mI(n, this.st, this.bb, 0, h);
            if (h && this.nE != h) {
                n = 1 / h;
                t = this.Ff;
                for (e = 0; e < h; e++)t[4 * e] = 0, t[4 * e + 1] = n * e, t[2 * (2 * e + 1)] = 1, t[2 * (2 * e + 1) + 1] = n * e;
                this.nE = h
            }
            this.Mi = h
        }
    }
});
F.gJ.create = function (a, c, d, e, f) {
    return new F.gJ(a, c, d, e, f)
};
F.qm = F.n.extend({
    grid: q, Ne: q, uD: q, vD: q, ctor: function () {
        F.n.prototype.ctor.call(this);
        F.B === F.Y && (this.uD = new F.mo(this, this.O6), this.vD = new F.mo(this, this.P6))
    }, St: A("grid"), m8: z("grid"), pj: z("Ne"), O6: function () {
        var a = this.grid;
        F.B == F.Y && (a && a.wm) && a.y2()
    }, P6: function () {
        var a = this.grid;
        F.B == F.Y && (a && a.wm) && a.b2(this.Ne)
    }, H: function () {
        if (this.Jc) {
            var a = F.B == F.Y, c = this.grid;
            if (a) {
                var d = F.tb;
                d.stack.push(d.top);
                F.og(this.Pb, d.top);
                d.top = this.Pb
            }
            this.transform();
            a && (c && c.wm && F.L.Qn(), this.uD && F.ka.xc(this.uD),
            this.Ne && this.Ne.H());
            if ((c = this.u) && 0 < c.length) {
                var e = c.length;
                this.Tc();
                for (var f = 0; f < e; f++) {
                    var g = c[f];
                    g && g.H()
                }
            }
            a && (this.vD && F.ka.xc(this.vD), d.top = d.stack.pop())
        }
    }, A1: function () {
        var a = this.qy, c = F.tb.top, d = this.Sl(), e = a.c;
        e[0] = d.a;
        e[4] = d.s;
        e[12] = d.P;
        e[1] = d.b;
        e[5] = d.z;
        e[13] = d.Q;
        e[14] = this.Xp;
        F.lf(c, c, a);
        if (this.Ng != q && (!this.grid || !this.grid.mk()))a = this.Ib.x, c = this.Ib.y, 0 !== a || 0 !== c ? (F.$v || (a |= 0, c |= 0), F.Iq(a, c), this.Ng.mu(), F.Iq(-a, -c)) : this.Ng.mu()
    }
});
M = F.qm.prototype;
F.B === F.Y && (M.transform = M.A1);
F.k(M, "target", q, M.pj);
F.qm.create = function () {
    return new F.qm
};
F.Mpa = function () {
    return {x: 0, y: 0}
};
F.fm = function (a, c) {
    return {x: a, y: c}
};
F.Ue = function (a, c) {
    return F.fm(a.x + c.x, a.y + c.y)
};
F.Lc = function (a, c) {
    return F.fm(a.x - c.x, a.y - c.y)
};
F.Zd = function (a, c) {
    return F.fm(a.x * c, a.y * c)
};
F.nr = function (a) {
    return F.fm(-a.y, a.x)
};
F.Bg = function (a) {
    return F.fm(-a.x, -a.y)
};
F.eT = function (a, c) {
    return a.x * c.x + a.y * c.y
};
F.Lpa = function (a) {
    return F.fm(Math.cos(a), Math.sin(a))
};
F.cv = function (a) {
    a = F.oh(F.d(a.x, a.y));
    return F.fm(a.x, a.y)
};
F.Xe = function (a) {
    return F.fm(a.x, a.y)
};
F.Wa = function (a) {
    return {pa: a.x, qa: a.y}
};
F.kU = F.n.extend({
    Ga: q, q: q, wd: 1, Fe: q, Mb: "DrawNodeCanvas", ctor: function () {
        F.n.prototype.ctor.call(this);
        var a = this.Z;
        a.Ga = this.Ga = [];
        a.Fe = this.Fe = F.color(255, 255, 255, 255);
        a.q = this.q = new F.dc(F.Ac, F.zc);
        this.oa()
    }, de: function () {
        this.Z = new F.uj(this)
    }, Jf: A("q"), Vd: function (a, c) {
        c === k ? (this.q.src = a.src, this.q.J = a.J) : (this.q.src = a, this.q.J = c)
    }, co: z("wd"), q4: A("wd"), Nf: function (a) {
        var c = this.Fe;
        c.r = a.r;
        c.g = a.g;
        c.b = a.b;
        c.a = a.a == q ? 255 : a.a
    }, fe: function () {
        return F.color(this.Fe.r, this.Fe.g, this.Fe.b, this.Fe.a)
    },
    mG: function (a, c, d, e, f) {
        e = e || this.wd;
        f = f || this.fe();
        f.a == q && (f.a = 255);
        a = [a, F.d(c.x, a.y), c, F.d(a.x, c.y)];
        c = new F.Nk(F.fd.Aj);
        c.Be = a;
        c.lineWidth = e;
        c.li = f;
        c.bu = p;
        c.hj = p;
        c.lineCap = "butt";
        if (c.fillColor = d)d.a == q && (d.a = 255), c.cu = p;
        this.Ga.push(c)
    }, gG: function (a, c, d, e, f, g, h) {
        g = g || this.wd;
        h = h || this.fe();
        h.a == q && (h.a = 255);
        for (var m = 2 * Math.PI / e, n = [], s = 0; s <= e; s++) {
            var t = s * m;
            n.push(F.d(c * Math.cos(t + d) + a.x, c * Math.sin(t + d) + a.y))
        }
        f && n.push(F.d(a.x, a.y));
        a = new F.Nk(F.fd.Aj);
        a.Be = n;
        a.lineWidth = g;
        a.li = h;
        a.bu = p;
        a.hj =
            p;
        this.Ga.push(a)
    }, lG: function (a, c, d, e, f, g) {
        f = f || this.wd;
        g = g || this.fe();
        g.a == q && (g.a = 255);
        for (var h = [], m = 0, n = 0; n < e; n++)h.push(F.d(Math.pow(1 - m, 2) * a.x + 2 * (1 - m) * m * c.x + m * m * d.x, Math.pow(1 - m, 2) * a.y + 2 * (1 - m) * m * c.y + m * m * d.y)), m += 1 / e;
        h.push(F.d(d.x, d.y));
        a = new F.Nk(F.fd.Aj);
        a.Be = h;
        a.lineWidth = f;
        a.li = g;
        a.hj = p;
        a.lineCap = "round";
        this.Ga.push(a)
    }, hG: function (a, c, d, e, f, g, h) {
        g = g || this.wd;
        h = h || this.fe();
        h.a == q && (h.a = 255);
        for (var m = [], n = 0, s = 0; s < f; s++)m.push(F.d(Math.pow(1 - n, 3) * a.x + 3 * Math.pow(1 - n, 2) * n * c.x + 3 * (1 - n) * n *
            n * d.x + n * n * n * e.x, Math.pow(1 - n, 3) * a.y + 3 * Math.pow(1 - n, 2) * n * c.y + 3 * (1 - n) * n * n * d.y + n * n * n * e.y)), n += 1 / f;
        m.push(F.d(e.x, e.y));
        a = new F.Nk(F.fd.Aj);
        a.Be = m;
        a.lineWidth = g;
        a.li = h;
        a.hj = p;
        a.lineCap = "round";
        this.Ga.push(a)
    }, fG: function (a, c, d, e) {
        this.Gn(a, 0.5, c, d, e)
    }, Gn: function (a, c, d, e, f) {
        e = e || this.wd;
        f = f || this.fe();
        f.a == q && (f.a = 255);
        for (var g = [], h, m, n = 1 / a.length, s = 0; s < d + 1; s++)m = s / d, 1 == m ? (h = a.length - 1, m = 1) : (h = 0 | m / n, m = (m - n * h) / n), h = F.VF(F.Bd(a, h - 1), F.Bd(a, h - 0), F.Bd(a, h + 1), F.Bd(a, h + 2), c, m), g.push(h);
        a = new F.Nk(F.fd.Aj);
        a.Be = g;
        a.lineWidth = e;
        a.li = f;
        a.hj = p;
        a.lineCap = "round";
        this.Ga.push(a)
    }, iG: function (a, c, d) {
        d = d || this.fe();
        d.a == q && (d.a = 255);
        var e = new F.Nk(F.fd.YB);
        e.Be = [a];
        e.lineWidth = c;
        e.fillColor = d;
        this.Ga.push(e)
    }, c3: function (a, c, d) {
        if (a && 0 != a.length) {
            d = d || this.fe();
            d.a == q && (d.a = 255);
            for (var e = 0, f = a.length; e < f; e++)this.iG(a[e], c, d)
        }
    }, nG: function (a, c, d, e) {
        d = d || this.wd;
        e = e || this.fe();
        e.a == q && (e.a = 255);
        var f = new F.Nk(F.fd.Aj);
        f.Be = [a, c];
        f.lineWidth = 2 * d;
        f.li = e;
        f.hj = p;
        f.lineCap = "round";
        this.Ga.push(f)
    }, f3: function (a,
                     c, d, e) {
        d = d || this.wd;
        e = e || this.fe();
        e.a == q && (e.a = 255);
        var f = new F.Nk(F.fd.Aj);
        f.Be = a;
        f.fillColor = c;
        f.lineWidth = d;
        f.li = e;
        f.bu = p;
        f.hj = p;
        f.lineCap = "round";
        c && (f.cu = p);
        this.Ga.push(f)
    }, ye: function (a, c, d, e) {
        for (var f = [], g = 0; g < a.length; g++)f.push(F.d(a[g].x, a[g].y));
        return this.f3(f, c, d, e)
    }, Ka: function (a) {
        a = a || F.l;
        this.q && (this.q.src == F.SRC_ALPHA && this.q.J == F.ONE) && (a.globalCompositeOperation = "lighter");
        for (var c = 0; c < this.Ga.length; c++) {
            var d = this.Ga[c];
            switch (d.type) {
                case F.fd.YB:
                    this.Bs(a, d);
                    break;
                case F.fd.nK:
                    this.Ds(a,
                        d);
                    break;
                case F.fd.Aj:
                    this.Cs(a, d)
            }
        }
    }, Bs: function (a, c) {
        var d = c.fillColor, e = c.Be[0], f = c.lineWidth, g = F.view.sa, h = F.view.Ya;
        a.fillStyle = "rgba(" + (0 | d.r) + "," + (0 | d.g) + "," + (0 | d.b) + "," + d.a / 255 + ")";
        a.beginPath();
        a.arc(e.x * g, -e.y * h, f * g, 0, 2 * Math.PI, r);
        a.closePath();
        a.fill()
    }, Ds: function (a, c) {
        var d = c.li, e = c.Be[0], f = c.Be[1], g = c.lineWidth, h = c.lineCap, m = F.view.sa, n = F.view.Ya;
        a.strokeStyle = "rgba(" + (0 | d.r) + "," + (0 | d.g) + "," + (0 | d.b) + "," + d.a / 255 + ")";
        a.lineWidth = g * m;
        a.beginPath();
        a.lineCap = h;
        a.moveTo(e.x * m, -e.y * n);
        a.lineTo(f.x *
            m, -f.y * n);
        a.stroke()
    }, Cs: function (a, c) {
        var d = c.Be, e = c.lineCap, f = c.fillColor, g = c.lineWidth, h = c.li, m = c.bu, n = c.cu, s = c.hj;
        if (d != q) {
            var t = d[0], v = F.view.sa, w = F.view.Ya;
            a.lineCap = e;
            f && (a.fillStyle = "rgba(" + (0 | f.r) + "," + (0 | f.g) + "," + (0 | f.b) + "," + f.a / 255 + ")");
            g && (a.lineWidth = g * v);
            h && (a.strokeStyle = "rgba(" + (0 | h.r) + "," + (0 | h.g) + "," + (0 | h.b) + "," + h.a / 255 + ")");
            a.beginPath();
            a.moveTo(t.x * v, -t.y * w);
            e = 1;
            for (f = d.length; e < f; e++)a.lineTo(d[e].x * v, -d[e].y * w);
            m && a.closePath();
            n && a.fill();
            s && a.stroke()
        }
    }, clear: function () {
        this.Ga.length =
            0
    }
});
F.lU = F.n.extend({
    Qo: 0, Ga: q, ek: q, QO: q, PO: q, wd: 1, Fe: q, q: q, sb: r, Mb: "DrawNodeWebGL", Jf: A("q"), Vd: function (a, c) {
        c === k ? (this.q.src = a.src, this.q.J = a.J) : (this.q.src = a, this.q.J = c)
    }, ctor: function () {
        F.n.prototype.ctor.call(this);
        this.Ga = [];
        this.q = new F.dc(F.Ac, F.zc);
        this.Fe = F.color(255, 255, 255, 255);
        this.oa()
    }, de: function () {
        this.Z = new F.OI(this)
    }, oa: function () {
        return F.n.prototype.oa.call(this) ? (this.shaderProgram = F.le.Kc(F.TJ), this.Es(64), this.QO = F.l.createBuffer(), this.sb = p) : r
    }, co: z("wd"), q4: A("wd"), Nf: function (a) {
        var c = this.Fe;
        c.r = a.r;
        c.g = a.g;
        c.b = a.b;
        c.a = a.a
    }, fe: function () {
        return F.color(this.Fe.r, this.Fe.g, this.Fe.b, this.Fe.a)
    }, mG: function (a, c, d, e, f) {
        e = e || this.wd;
        f = f || this.fe();
        f.a == q && (f.a = 255);
        a = [a, F.d(c.x, a.y), c, F.d(a.x, c.y)];
        d == q ? this.Xw(a, e, f, p) : this.ye(a, d, e, f)
    }, gG: function (a, c, d, e, f, g, h) {
        g = g || this.wd;
        h = h || this.fe();
        h.a == q && (h.a = 255);
        var m = 2 * Math.PI / e, n = [], s;
        for (s = 0; s <= e; s++) {
            var t = s * m;
            n.push(F.d(c * Math.cos(t + d) + a.x, c * Math.sin(t + d) + a.y))
        }
        f && n.push(F.d(a.x, a.y));
        g *= 0.5;
        s = 0;
        for (a = n.length; s < a - 1; s++)this.nG(n[s], n[s +
        1], g, h)
    }, lG: function (a, c, d, e, f, g) {
        f = f || this.wd;
        g = g || this.fe();
        g.a == q && (g.a = 255);
        for (var h = [], m = 0, n = 0; n < e; n++)h.push(F.d(Math.pow(1 - m, 2) * a.x + 2 * (1 - m) * m * c.x + m * m * d.x, Math.pow(1 - m, 2) * a.y + 2 * (1 - m) * m * c.y + m * m * d.y)), m += 1 / e;
        h.push(F.d(d.x, d.y));
        this.Xw(h, f, g, r)
    }, hG: function (a, c, d, e, f, g, h) {
        g = g || this.wd;
        h = h || this.fe();
        h.a == q && (h.a = 255);
        for (var m = [], n = 0, s = 0; s < f; s++)m.push(F.d(Math.pow(1 - n, 3) * a.x + 3 * Math.pow(1 - n, 2) * n * c.x + 3 * (1 - n) * n * n * d.x + n * n * n * e.x, Math.pow(1 - n, 3) * a.y + 3 * Math.pow(1 - n, 2) * n * c.y + 3 * (1 - n) * n * n * d.y + n * n * n *
            e.y)), n += 1 / f;
        m.push(F.d(e.x, e.y));
        this.Xw(m, g, h, r)
    }, fG: function (a, c, d, e) {
        this.Gn(a, 0.5, c, d, e)
    }, Gn: function (a, c, d, e, f) {
        e = e || this.wd;
        f = f || this.fe();
        f.a == q && (f.a = 255);
        for (var g = [], h, m, n = 1 / a.length, s = 0; s < d + 1; s++)m = s / d, 1 == m ? (h = a.length - 1, m = 1) : (h = 0 | m / n, m = (m - n * h) / n), h = F.VF(F.Bd(a, h - 1), F.Bd(a, h - 0), F.Bd(a, h + 1), F.Bd(a, h + 2), c, m), g.push(h);
        e *= 0.5;
        a = 0;
        for (c = g.length; a < c - 1; a++)this.nG(g[a], g[a + 1], e, f)
    }, gO: function () {
        var a = F.l;
        F.Xb(F.Bh);
        a.bindBuffer(a.ARRAY_BUFFER, this.QO);
        this.sb && (a.bufferData(a.ARRAY_BUFFER,
            this.ek, a.STREAM_DRAW), this.sb = r);
        var c = F.Id.BYTES_PER_ELEMENT;
        a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, c, 0);
        a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, c, 8);
        a.vertexAttribPointer(F.hd, 2, a.FLOAT, r, c, 12);
        a.drawArrays(a.TRIANGLES, 0, 3 * this.Ga.length);
        F.lh()
    }, Es: function (a) {
        var c = this.Ga;
        if (c.length + a > this.Qo) {
            var d = F.nc.BYTES_PER_ELEMENT;
            this.Qo += Math.max(this.Qo, a);
            if (c == q || 0 === c.length)this.Ga = [], this.ek = new ArrayBuffer(d * this.Qo), this.PO = new Uint8Array(this.ek); else {
                a = [];
                for (var e = new ArrayBuffer(d *
                    this.Qo), f = 0; f < c.length; f++)a[f] = new F.nc(c[f].a, c[f].b, c[f].s, e, f * d);
                this.PO = new Uint8Array(e);
                this.ek = e;
                this.Ga = a
            }
        }
    }, Ka: function () {
        F.pd(this.q.src, this.q.J);
        this.na.xb();
        this.na.Qu();
        this.gO()
    }, iG: function (a, c, d) {
        d = d || this.fe();
        d.a == q && (d.a = 255);
        var e = {r: 0 | d.r, g: 0 | d.g, b: 0 | d.b, a: 0 | d.a};
        d = {j: {x: a.x - c, y: a.y - c}, A: e, p: {pa: -1, qa: -1}};
        var f = {j: {x: a.x - c, y: a.y + c}, A: e, p: {pa: -1, qa: 1}}, g = {
            j: {x: a.x + c, y: a.y + c},
            A: e,
            p: {pa: 1, qa: 1}
        };
        a = {j: {x: a.x + c, y: a.y - c}, A: e, p: {pa: 1, qa: -1}};
        this.Es(6);
        this.Ga.push(new F.nc(d, f,
            g, this.ek, this.Ga.length * F.nc.BYTES_PER_ELEMENT));
        this.Ga.push(new F.nc(d, g, a, this.ek, this.Ga.length * F.nc.BYTES_PER_ELEMENT));
        this.sb = p
    }, c3: function (a, c, d) {
        if (a && 0 != a.length) {
            d = d || this.fe();
            d.a == q && (d.a = 255);
            for (var e = 0, f = a.length; e < f; e++)this.iG(a[e], c, d)
        }
    }, nG: function (a, c, d, e) {
        e = e || this.fe();
        e.a == q && (e.a = 255);
        d = d || 0.5 * this.wd;
        this.Es(18);
        e = {r: 0 | e.r, g: 0 | e.g, b: 0 | e.b, a: 0 | e.a};
        var f = F.Xe(a);
        c = F.Xe(c);
        a = F.cv(F.nr(F.Lc(c, f)));
        var g = F.nr(a), h = F.Zd(a, d);
        d = F.Zd(g, d);
        var m = F.Ue(c, F.Lc(h, d)), n = F.Lc(c, h), s =
            F.Ue(c, h), t = F.Lc(f, h), v = F.Ue(f, h), w = F.Lc(f, F.Lc(h, d)), f = F.Ue(f, F.Ue(h, d)), x = F.nc.BYTES_PER_ELEMENT, y = this.ek, B = this.Ga;
        B.push(new F.nc({j: F.Lc(c, F.Ue(h, d)), A: e, p: F.Wa(F.Bg(F.Ue(a, g)))}, {
            j: m,
            A: e,
            p: F.Wa(F.Lc(a, g))
        }, {j: n, A: e, p: F.Wa(F.Bg(a))}, y, B.length * x));
        B.push(new F.nc({j: s, A: e, p: F.Wa(a)}, {j: m, A: e, p: F.Wa(F.Lc(a, g))}, {
            j: n,
            A: e,
            p: F.Wa(F.Bg(a))
        }, y, B.length * x));
        B.push(new F.nc({j: s, A: e, p: F.Wa(a)}, {j: t, A: e, p: F.Wa(F.Bg(a))}, {
            j: n,
            A: e,
            p: F.Wa(F.Bg(a))
        }, y, B.length * x));
        B.push(new F.nc({j: s, A: e, p: F.Wa(a)}, {
            j: t,
            A: e, p: F.Wa(F.Bg(a))
        }, {j: v, A: e, p: F.Wa(a)}, y, B.length * x));
        B.push(new F.nc({j: w, A: e, p: F.Wa(F.Lc(g, a))}, {j: t, A: e, p: F.Wa(F.Bg(a))}, {
            j: v,
            A: e,
            p: F.Wa(a)
        }, y, B.length * x));
        B.push(new F.nc({j: w, A: e, p: F.Wa(F.Lc(g, a))}, {j: f, A: e, p: F.Wa(F.Ue(a, g))}, {
            j: v,
            A: e,
            p: F.Wa(a)
        }, y, B.length * x));
        this.sb = p
    }, ye: function (a, c, d, e) {
        if (c == q)this.Xw(a, d, e, p); else {
            c.a == q && (c.a = 255);
            e.a == q && (e.a = 255);
            d = d || this.wd;
            d *= 0.5;
            c = {r: 0 | c.r, g: 0 | c.g, b: 0 | c.b, a: 0 | c.a};
            e = {r: 0 | e.r, g: 0 | e.g, b: 0 | e.b, a: 0 | e.a};
            var f = [], g, h, m, n, s = a.length;
            for (g = 0; g < s; g++) {
                h =
                    F.Xe(a[(g - 1 + s) % s]);
                m = F.Xe(a[g]);
                n = F.Xe(a[(g + 1) % s]);
                var t = F.cv(F.nr(F.Lc(m, h)));
                m = F.cv(F.nr(F.Lc(n, m)));
                f[g] = {offset: F.Zd(F.Ue(t, m), 1 / (F.eT(t, m) + 1)), Rl: m}
            }
            t = 0 < d;
            this.Es(3 * (3 * s - 2));
            var v = F.nc.BYTES_PER_ELEMENT, w = this.ek, x = this.Ga, y = t == r ? 0.5 : 0;
            for (g = 0; g < s - 2; g++)h = F.Lc(F.Xe(a[0]), F.Zd(f[0].offset, y)), m = F.Lc(F.Xe(a[g + 1]), F.Zd(f[g + 1].offset, y)), n = F.Lc(F.Xe(a[g + 2]), F.Zd(f[g + 2].offset, y)), x.push(new F.nc({
                j: h,
                A: c,
                p: F.Wa({x: 0, y: 0})
            }, {j: m, A: c, p: F.Wa({x: 0, y: 0})}, {j: n, A: c, p: F.Wa({x: 0, y: 0})}, w, x.length * v));
            for (g =
                     0; g < s; g++) {
                y = (g + 1) % s;
                h = F.Xe(a[g]);
                m = F.Xe(a[y]);
                n = f[g].Rl;
                var B = f[g].offset, I = f[y].offset, y = t ? F.Lc(h, F.Zd(B, d)) : F.Lc(h, F.Zd(B, 0.5)), E = t ? F.Lc(m, F.Zd(I, d)) : F.Lc(m, F.Zd(I, 0.5));
                h = t ? F.Ue(h, F.Zd(B, d)) : F.Ue(h, F.Zd(B, 0.5));
                m = t ? F.Ue(m, F.Zd(I, d)) : F.Ue(m, F.Zd(I, 0.5));
                t ? (x.push(new F.nc({j: y, A: e, p: F.Wa(F.Bg(n))}, {j: E, A: e, p: F.Wa(F.Bg(n))}, {
                    j: m,
                    A: e,
                    p: F.Wa(n)
                }, w, x.length * v)), x.push(new F.nc({j: y, A: e, p: F.Wa(F.Bg(n))}, {j: h, A: e, p: F.Wa(n)}, {
                    j: m,
                    A: e,
                    p: F.Wa(n)
                }, w, x.length * v))) : (x.push(new F.nc({j: y, A: c, p: F.Wa({x: 0, y: 0})},
                    {j: E, A: c, p: F.Wa({x: 0, y: 0})}, {
                        j: m,
                        A: c,
                        p: F.Wa(n)
                    }, w, x.length * v)), x.push(new F.nc({j: y, A: c, p: F.Wa({x: 0, y: 0})}, {
                    j: h,
                    A: c,
                    p: F.Wa(n)
                }, {j: m, A: c, p: F.Wa(n)}, w, x.length * v)))
            }
            this.sb = p
        }
    }, Xw: function (a, c, d, e) {
        c = c || this.wd;
        d = d || this.Fe;
        d.a == q && (d.a = 255);
        c *= 0.5;
        if (!(0 >= c)) {
            d = {r: 0 | d.r, g: 0 | d.g, b: 0 | d.b, a: 0 | d.a};
            var f = [], g, h, m, n, s = a.length;
            for (g = 0; g < s; g++) {
                h = F.Xe(a[(g - 1 + s) % s]);
                m = F.Xe(a[g]);
                n = F.Xe(a[(g + 1) % s]);
                var t = F.cv(F.nr(F.Lc(m, h)));
                m = F.cv(F.nr(F.Lc(n, m)));
                f[g] = {offset: F.Zd(F.Ue(t, m), 1 / (F.eT(t, m) + 1)), Rl: m}
            }
            this.Es(3 *
                (3 * s - 2));
            n = F.nc.BYTES_PER_ELEMENT;
            var t = this.ek, v = this.Ga;
            e = e ? s : s - 1;
            for (g = 0; g < e; g++) {
                var w = (g + 1) % s;
                h = F.Xe(a[g]);
                m = F.Xe(a[w]);
                var x = f[g].Rl, y = f[g].offset, w = f[w].offset, B = F.Lc(h, F.Zd(y, c));
                h = F.Ue(h, F.Zd(y, c));
                y = F.Ue(m, F.Zd(w, c));
                v.push(new F.nc({j: B, A: d, p: F.Wa(F.Bg(x))}, {j: F.Lc(m, F.Zd(w, c)), A: d, p: F.Wa(F.Bg(x))}, {
                    j: y,
                    A: d,
                    p: F.Wa(x)
                }, t, v.length * n));
                v.push(new F.nc({j: B, A: d, p: F.Wa(F.Bg(x))}, {j: h, A: d, p: F.Wa(x)}, {
                    j: y,
                    A: d,
                    p: F.Wa(x)
                }, t, v.length * n))
            }
            this.sb = p
        }
    }, clear: function () {
        this.Ga.length = 0;
        this.sb = p
    }
});
F.fd = F.B == F.Y ? F.lU : F.kU;
F.fd.create = function () {
    return new F.fd
};
F.Nk = function (a) {
    this.type = a;
    this.fillColor = this.Be = q;
    this.lineWidth = 0;
    this.li = q;
    this.lineCap = "butt";
    this.hj = this.cu = this.bu = r
};
F.fd.YB = 0;
F.fd.nK = 1;
F.fd.Aj = 2;
F.Tu = -1;
F.mc = F.n.extend({
    xn: 0,
    ii: r,
    wE: q,
    uE: q,
    vE: q,
    xL: q,
    rL: q,
    sL: q,
    hc: q,
    dN: r,
    To: q,
    OL: q,
    RL: q,
    SL: q,
    NL: q,
    PL: q,
    QL: q,
    TL: q,
    ML: q,
    JL: q,
    LD: q,
    ctor: function (a) {
        F.n.prototype.ctor.call(this);
        this.hc = q;
        this.xn = 0;
        this.ii = r;
        F.mc.prototype.oa.call(this, a || q)
    },
    de: function () {
        F.B === F.Aa ? (this.wE = new F.GI(this), this.uE = new F.EI(this), this.vE = new F.FI(this)) : (this.xL = new F.mo(this, this.Q_), this.rL = new F.mo(this, this.O_), this.sL = new F.mo(this, this.P_))
    },
    oa: q,
    Mb: "ClippingNode",
    n_: function (a) {
        this.hc = a;
        this.xn = 1;
        this.ii = r;
        F.mc.BD =
            p;
        F.mc.BD && (F.Tu = F.l.getParameter(F.l.STENCIL_BITS), 0 >= F.Tu && F.log("Stencil buffer is not enabled."), F.mc.BD = r);
        return p
    },
    m_: function (a) {
        this.hc = a;
        this.xn = 1;
        this.ii = r
    },
    ba: function () {
        F.n.prototype.ba.call(this);
        this.hc.ba()
    },
    mi: function () {
        F.n.prototype.mi.call(this);
        this.hc.mi()
    },
    pg: function () {
        this.hc.pg();
        F.n.prototype.pg.call(this)
    },
    Cb: function () {
        this.hc.Cb();
        F.n.prototype.Cb.call(this)
    },
    H: q,
    Bt: function (a) {
        if (1 > F.Tu)F.n.prototype.H.call(this, a); else if (!this.hc || !this.hc.visible)this.ii && F.n.prototype.H.call(this,
            a); else if (F.mc.yx + 1 == F.Tu)F.mc.BF = p, F.mc.BF && (F.log("Nesting more than " + F.Tu + "stencils is not supported. Everything will be drawn without stencil for this node and its childs."), F.mc.BF = r), F.n.prototype.H.call(this, a); else {
            F.ka.xc(this.xL);
            a = F.tb;
            a.stack.push(a.top);
            F.og(this.Pb, a.top);
            a.top = this.Pb;
            this.transform();
            this.hc.H();
            F.ka.xc(this.rL);
            var c = this.u;
            if (c && 0 < c.length) {
                var d = c.length;
                this.Tc();
                for (var e = 0; e < d; e++)if (c[e] && 0 > c[e].Nb)c[e].H(); else break;
                for (this.Z && F.ka.xc(this.Z); e < d; e++)c[e] &&
                c[e].H()
            } else this.Z && F.ka.xc(this.Z);
            F.ka.xc(this.sL);
            a.top = a.stack.pop()
        }
    },
    Q_: function (a) {
        a = a || F.l;
        F.mc.yx++;
        var c = 1 << F.mc.yx;
        this.LD = c | c - 1;
        this.ML = a.isEnabled(a.STENCIL_TEST);
        this.TL = a.getParameter(a.STENCIL_WRITEMASK);
        this.OL = a.getParameter(a.STENCIL_FUNC);
        this.RL = a.getParameter(a.STENCIL_REF);
        this.SL = a.getParameter(a.STENCIL_VALUE_MASK);
        this.NL = a.getParameter(a.STENCIL_FAIL);
        this.PL = a.getParameter(a.STENCIL_PASS_DEPTH_FAIL);
        this.QL = a.getParameter(a.STENCIL_PASS_DEPTH_PASS);
        a.enable(a.STENCIL_TEST);
        a.stencilMask(c);
        this.JL = a.getParameter(a.DEPTH_WRITEMASK);
        a.depthMask(r);
        a.stencilFunc(a.NEVER, c, c);
        a.stencilOp(!this.ii ? a.ZERO : a.REPLACE, a.KEEP, a.KEEP);
        this.mZ();
        a.stencilFunc(a.NEVER, c, c);
        a.stencilOp(!this.ii ? a.REPLACE : a.ZERO, a.KEEP, a.KEEP);
        1 > this.xn && (c = F.le.Kc(F.Yv), a = a.getUniformLocation(c.Pn(), F.$K), F.MG(c.Pn()), c.Pu(a, this.xn), F.rS(this.hc, c))
    },
    mZ: function () {
        F.Se(F.Gg);
        F.Sn();
        F.Pl();
        F.Se(F.Qf);
        F.Sn();
        F.Pl();
        F.Ge.OP(F.d(-1, -1), F.d(1, 1), F.color(255, 255, 255, 255));
        F.Se(F.Gg);
        F.ok();
        F.Se(F.Qf);
        F.ok()
    },
    O_: function (a) {
        a = a || F.l;
        a.depthMask(this.JL);
        a.stencilFunc(a.EQUAL, this.LD, this.LD);
        a.stencilOp(a.KEEP, a.KEEP, a.KEEP)
    },
    P_: function (a) {
        a = a || F.l;
        a.stencilFunc(this.OL, this.RL, this.SL);
        a.stencilOp(this.NL, this.PL, this.QL);
        a.stencilMask(this.TL);
        this.ML || a.disable(a.STENCIL_TEST);
        F.mc.yx--
    },
    sn: function (a) {
        this.To = this.NC() || this.hc instanceof F.I;
        var c = a || F.l, d = this.u, e;
        if (!this.hc || !this.hc.visible)this.ii && F.n.prototype.H.call(this, a); else {
            this.wE && F.ka.xc(this.wE);
            this.To ? F.n.prototype.H.call(this,
                c) : this.hc.H(c);
            this.uE && F.ka.xc(this.uE);
            this.transform();
            if (this.To)this.hc.H(); else {
                this.NC(p);
                var f = d.length;
                if (0 < f) {
                    this.Tc();
                    for (a = 0; a < f; a++)if (e = d[a], 0 > e.Nb)e.H(c); else break;
                    for (this.Z && F.ka.xc(this.Z); a < f; a++)d[a].H(c)
                } else this.Z && F.ka.xc(this.Z);
                this.NC(r)
            }
            this.vE && F.ka.xc(this.vE)
        }
    },
    X4: A("hc"),
    XH: q,
    c1: function (a) {
        if (this.hc != a && (this.hc && (this.hc.Ja = q), this.hc = a))this.hc.Ja = this
    },
    b1: function (a) {
        this.hc = a;
        if (a.Ga)for (var c = 0; c < a.Ga.length; c++)a.Ga[c].cu = r, a.Ga[c].hj = r;
        a instanceof F.fd &&
        (a.Z.cb = function (c, e, f) {
            e = e || F.view.sa;
            f = f || F.view.Ya;
            c = c || F.l;
            var g = this.T.Ad;
            c.save();
            c.transform(g.a, g.b, g.s, g.z, g.P * e, -g.Q * f);
            c.beginPath();
            for (g = 0; g < a.Ga.length; g++) {
                var h = a.Ga[g].Be, m = h[0];
                c.moveTo(m.x * e, -m.y * f);
                for (var m = 1, n = h.length; m < n; m++)c.lineTo(h[m].x * e, -h[m].y * f)
            }
            c.restore()
        })
    },
    zfa: A("xn"),
    ana: z("xn"),
    Fia: A("ii"),
    Lna: z("ii"),
    NC: function (a) {
        if (a === p || a === r)F.mc.prototype.dN = a;
        return F.mc.prototype.dN
    },
    ef: function (a) {
        F.n.prototype.ef.call(this, a);
        this.hc && this.hc.ef(this.Pb)
    }
});
M = F.mc.prototype;
F.B === F.Y ? (M.oa = M.n_, M.H = M.Bt, M.XH = M.c1) : (M.oa = M.m_, M.H = M.sn, M.XH = M.b1);
F.k(M, "stencil", M.X4, M.XH);
F.mc.BD = q;
F.mc.BF = q;
F.mc.yx = -1;
F.mc.BO = q;
F.mc.aN = function () {
    return F.mc.BO || (F.mc.BO = document.createElement("canvas"))
};
F.mc.create = function (a) {
    return new F.mc(a)
};
F.so = F.za.extend({
    wm: r, Si: 0, da: q, G: q, bh: q, mp: q, Nm: r, na: q, ZL: 0, sb: r, ctor: function (a, c, d) {
        F.RC();
        this.wm = r;
        this.Si = 0;
        this.G = this.da = q;
        this.bh = F.d(0, 0);
        this.mp = q;
        this.Nm = r;
        this.na = q;
        this.ZL = 0;
        this.sb = r;
        a !== k && this.nz(a, c, d)
    }, mk: A("wm"), setActive: function (a) {
        this.wm = a;
        if (!a) {
            a = F.L;
            var c = a.Qn();
            a.eo(c)
        }
    }, qha: A("Si"), E8: z("Si"), j4: function () {
        return F.size(this.da.width, this.da.height)
    }, Ina: function (a) {
        this.da.width = parseInt(a.width);
        this.da.height = parseInt(a.height)
    }, IG: function () {
        return F.d(this.bh.x,
            this.bh.y)
    }, xoa: function (a) {
        this.bh.x = a.x;
        this.bh.y = a.y
    }, Tia: A("Nm"), Goa: function (a) {
        this.Nm != a && (this.Nm = a, this.Fy())
    }, nz: function (a, c, d) {
        if (!c) {
            var e = F.L.Xt(), f = F.Gk(e.width), g = F.Gk(e.height), h = new Uint8Array(4 * f * g);
            if (!h)return F.log("cocos2d: CCGrid: not enough memory."), r;
            c = new F.ha;
            c.au(h, F.ha.xj, f, g, e);
            if (!c)return F.log("cocos2d: CCGrid: error creating texture"), r
        }
        this.wm = r;
        this.Si = 0;
        this.da = a;
        this.G = c;
        this.Nm = d || r;
        this.bh.x = this.G.width / a.width;
        this.bh.y = this.G.height / a.height;
        this.mp = new F.FU;
        if (!this.mp)return r;
        this.mp.j5(this.G);
        this.na = F.le.Kc(F.Tr);
        this.Fy();
        return p
    }, y2: function () {
        this.ZL = F.L.Qn();
        this.mp.z2(this.G)
    }, b2: function (a) {
        this.mp.c2(this.G);
        if (a && a.On().cH()) {
            var c = a.Qy();
            F.Iq(c.x, c.y);
            a.On().mu();
            F.Iq(-c.x, -c.y)
        }
        F.Cd(this.G);
        this.MF()
    }, MF: function () {
        F.log("cc.GridBase.blit(): Shall be overridden in subclass.")
    }, FH: function () {
        F.log("cc.GridBase.reuse(): Shall be overridden in subclass.")
    }, Fy: function () {
        F.log("cc.GridBase.calculateVertexPoints(): Shall be overridden in subclass.")
    },
    Xma: function () {
        var a = F.L.Xt();
        F.l.viewport(0, 0, a.width, a.height);
        F.Se(F.Gg);
        F.Pl();
        var c = new F.ea;
        F.iH(c, 0, a.width, 0, a.height, -1, 1);
        F.Hq(c);
        F.Se(F.Qf);
        F.Pl();
        F.sS()
    }
});
F.so.create = function (a, c, d) {
    return new F.so(a, c, d)
};
F.cB = F.so.extend({
    eh: q, bb: q, Nj: q, zb: q, hg: q, nd: q, bg: q, ctor: function (a, c, d) {
        F.so.prototype.ctor.call(this);
        this.bg = this.nd = this.hg = this.zb = this.Nj = this.bb = this.eh = q;
        a !== k && this.nz(a, c, d)
    }, gT: function (a) {
        (a.x !== (0 | a.x) || a.y !== (0 | a.y)) && F.log("cc.Grid3D.vertex() : Numbers must be integers");
        a = 0 | 3 * (a.x * (this.da.height + 1) + a.y);
        var c = this.bb;
        return new U(c[a], c[a + 1], c[a + 2])
    }, dd: function (a) {
        (a.x !== (0 | a.x) || a.y !== (0 | a.y)) && F.log("cc.Grid3D.originalVertex() : Numbers must be integers");
        a = 0 | 3 * (a.x * (this.da.height +
            1) + a.y);
        var c = this.Nj;
        return new U(c[a], c[a + 1], c[a + 2])
    }, je: function (a, c) {
        (a.x !== (0 | a.x) || a.y !== (0 | a.y)) && F.log("cc.Grid3D.setVertex() : Numbers must be integers");
        var d = 0 | 3 * (a.x * (this.da.height + 1) + a.y), e = this.bb;
        e[d] = c.x;
        e[d + 1] = c.y;
        e[d + 2] = c.e;
        this.sb = p
    }, MF: function () {
        var a = this.da.width * this.da.height;
        F.Xb(F.Jd | F.Eo);
        this.na.xb();
        this.na.Qu();
        var c = F.l, d = this.sb;
        c.bindBuffer(c.ARRAY_BUFFER, this.nd);
        d && c.bufferData(c.ARRAY_BUFFER, this.bb, c.DYNAMIC_DRAW);
        c.vertexAttribPointer(F.kb, 3, c.FLOAT, r, 0, 0);
        c.bindBuffer(c.ARRAY_BUFFER, this.hg);
        d && c.bufferData(c.ARRAY_BUFFER, this.eh, c.DYNAMIC_DRAW);
        c.vertexAttribPointer(F.hd, 2, c.FLOAT, r, 0, 0);
        c.bindBuffer(c.ELEMENT_ARRAY_BUFFER, this.bg);
        d && c.bufferData(c.ELEMENT_ARRAY_BUFFER, this.zb, c.STATIC_DRAW);
        c.drawElements(c.TRIANGLES, 6 * a, c.UNSIGNED_SHORT, 0);
        d && (this.sb = r);
        F.lh()
    }, FH: function () {
        if (0 < this.Si) {
            for (var a = this.Nj, c = this.bb, d = 0, e = this.bb.length; d < e; d++)a[d] = c[d];
            --this.Si
        }
    }, Fy: function () {
        var a = F.l, c = this.G.pixelsWidth, d = this.G.pixelsHeight, e = this.G.S.height,
            f = this.da, g = (f.width + 1) * (f.height + 1);
        this.bb = new Float32Array(3 * g);
        this.eh = new Float32Array(2 * g);
        this.zb = new Uint16Array(6 * f.width * f.height);
        this.nd && a.deleteBuffer(this.nd);
        this.nd = a.createBuffer();
        this.hg && a.deleteBuffer(this.hg);
        this.hg = a.createBuffer();
        this.bg && a.deleteBuffer(this.bg);
        this.bg = a.createBuffer();
        for (var h, m, n = this.zb, s = this.eh, t = this.Nm, v = this.bb, g = 0; g < f.width; ++g)for (h = 0; h < f.height; ++h) {
            var w = h * f.width + g;
            m = g * this.bh.x;
            var x = m + this.bh.x, y = h * this.bh.y, B = y + this.bh.y, I = g * (f.height +
                1) + h, E = (g + 1) * (f.height + 1) + h, L = (g + 1) * (f.height + 1) + (h + 1), P = g * (f.height + 1) + (h + 1);
            n[6 * w] = I;
            n[6 * w + 1] = E;
            n[6 * w + 2] = P;
            n[6 * w + 3] = E;
            n[6 * w + 4] = L;
            n[6 * w + 5] = P;
            var w = [3 * I, 3 * E, 3 * L, 3 * P], K = [{x: m, y: y, e: 0}, {x: x, y: y, e: 0}, {
                x: x,
                y: B,
                e: 0
            }, {x: m, y: B, e: 0}], I = [2 * I, 2 * E, 2 * L, 2 * P], x = [F.d(m, y), F.d(x, y), F.d(x, B), F.d(m, B)];
            for (m = 0; 4 > m; ++m)v[w[m]] = K[m].x, v[w[m] + 1] = K[m].y, v[w[m] + 2] = K[m].e, s[I[m]] = x[m].x / c, s[I[m] + 1] = t ? (e - x[m].y) / d : x[m].y / d
        }
        this.Nj = new Float32Array(this.bb);
        a.bindBuffer(a.ARRAY_BUFFER, this.nd);
        a.bufferData(a.ARRAY_BUFFER,
            this.bb, a.DYNAMIC_DRAW);
        a.bindBuffer(a.ARRAY_BUFFER, this.hg);
        a.bufferData(a.ARRAY_BUFFER, this.eh, a.DYNAMIC_DRAW);
        a.bindBuffer(a.ELEMENT_ARRAY_BUFFER, this.bg);
        a.bufferData(a.ELEMENT_ARRAY_BUFFER, this.zb, a.STATIC_DRAW);
        this.sb = p
    }
});
F.cB.create = function (a, c, d) {
    return new F.cB(a, c, d)
};
F.iC = F.so.extend({
    eh: q, bb: q, Nj: q, zb: q, hg: q, nd: q, bg: q, ctor: function (a, c, d) {
        F.so.prototype.ctor.call(this);
        this.bg = this.nd = this.hg = this.zb = this.Nj = this.bb = this.eh = q;
        a !== k && this.nz(a, c, d)
    }, RS: function (a) {
        (a.x !== (0 | a.x) || a.y !== (0 | a.y)) && F.log("cc.TiledGrid3D.tile() : Numbers must be integers");
        a = 12 * (this.da.height * a.x + a.y);
        var c = this.bb;
        return new F.Rv(new U(c[a], c[a + 1], c[a + 2]), new U(c[a + 3], c[a + 4], c[a + 5]), new U(c[a + 6], c[a + 7], c[a + 8]), new U(c[a + 9], c[a + 10], c[a + 11]))
    }, rg: function (a) {
        (a.x !== (0 | a.x) || a.y !==
        (0 | a.y)) && F.log("cc.TiledGrid3D.originalTile() : Numbers must be integers");
        a = 12 * (this.da.height * a.x + a.y);
        var c = this.Nj;
        return new F.Rv(new U(c[a], c[a + 1], c[a + 2]), new U(c[a + 3], c[a + 4], c[a + 5]), new U(c[a + 6], c[a + 7], c[a + 8]), new U(c[a + 9], c[a + 10], c[a + 11]))
    }, pf: function (a, c) {
        (a.x !== (0 | a.x) || a.y !== (0 | a.y)) && F.log("cc.TiledGrid3D.setTile() : Numbers must be integers");
        var d = 12 * (this.da.height * a.x + a.y), e = this.bb;
        e[d] = c.K.x;
        e[d + 1] = c.K.y;
        e[d + 2] = c.K.e;
        e[d + 3] = c.V.x;
        e[d + 4] = c.V.y;
        e[d + 5] = c.V.e;
        e[d + 6] = c.U.x;
        e[d + 7] = c.U.y;
        e[d + 8] = c.U.e;
        e[d + 9] = c.O.x;
        e[d + 10] = c.O.y;
        e[d + 11] = c.O.e;
        this.sb = p
    }, MF: function () {
        var a = this.da.width * this.da.height;
        this.na.xb();
        this.na.Qu();
        var c = F.l, d = this.sb;
        F.Xb(F.Jd | F.Eo);
        c.bindBuffer(c.ARRAY_BUFFER, this.nd);
        d && c.bufferData(c.ARRAY_BUFFER, this.bb, c.DYNAMIC_DRAW);
        c.vertexAttribPointer(F.kb, 3, c.FLOAT, r, 0, this.bb);
        c.bindBuffer(c.ARRAY_BUFFER, this.hg);
        d && c.bufferData(c.ARRAY_BUFFER, this.eh, c.DYNAMIC_DRAW);
        c.vertexAttribPointer(F.hd, 2, c.FLOAT, r, 0, this.eh);
        c.bindBuffer(c.ELEMENT_ARRAY_BUFFER, this.bg);
        d && c.bufferData(c.ELEMENT_ARRAY_BUFFER, this.zb, c.STATIC_DRAW);
        c.drawElements(c.TRIANGLES, 6 * a, c.UNSIGNED_SHORT, 0);
        d && (this.sb = r);
        F.lh()
    }, FH: function () {
        if (0 < this.Si) {
            for (var a = this.bb, c = this.Nj, d = 0; d < a.length; d++)c[d] = a[d];
            --this.Si
        }
    }, Fy: function () {
        var a = this.G.pixelsWidth, c = this.G.pixelsHeight, d = this.G.S.height, e = this.da, f = e.width * e.height;
        this.bb = new Float32Array(12 * f);
        this.eh = new Float32Array(8 * f);
        this.zb = new Uint16Array(6 * f);
        var g = F.l;
        this.nd && g.deleteBuffer(this.nd);
        this.nd = g.createBuffer();
        this.hg &&
        g.deleteBuffer(this.hg);
        this.hg = g.createBuffer();
        this.bg && g.deleteBuffer(this.bg);
        this.bg = g.createBuffer();
        var h, m, n = 0, s = this.bh, t = this.bb, v = this.eh, w = this.Nm;
        for (h = 0; h < e.width; h++)for (m = 0; m < e.height; m++) {
            var x = h * s.x, y = x + s.x, B = m * s.y, I = B + s.y;
            t[12 * n] = x;
            t[12 * n + 1] = B;
            t[12 * n + 2] = 0;
            t[12 * n + 3] = y;
            t[12 * n + 4] = B;
            t[12 * n + 5] = 0;
            t[12 * n + 6] = x;
            t[12 * n + 7] = I;
            t[12 * n + 8] = 0;
            t[12 * n + 9] = y;
            t[12 * n + 10] = I;
            t[12 * n + 11] = 0;
            var E = B, L = I;
            w && (E = d - B, L = d - I);
            v[8 * n] = x / a;
            v[8 * n + 1] = E / c;
            v[8 * n + 2] = y / a;
            v[8 * n + 3] = E / c;
            v[8 * n + 4] = x / a;
            v[8 * n + 5] = L / c;
            v[8 * n + 6] = y /
                a;
            v[8 * n + 7] = L / c;
            n++
        }
        a = this.zb;
        for (h = 0; h < f; h++)a[6 * h + 0] = 4 * h + 0, a[6 * h + 1] = 4 * h + 1, a[6 * h + 2] = 4 * h + 2, a[6 * h + 3] = 4 * h + 1, a[6 * h + 4] = 4 * h + 2, a[6 * h + 5] = 4 * h + 3;
        this.Nj = new Float32Array(this.bb);
        g.bindBuffer(g.ARRAY_BUFFER, this.nd);
        g.bufferData(g.ARRAY_BUFFER, this.bb, g.DYNAMIC_DRAW);
        g.bindBuffer(g.ARRAY_BUFFER, this.hg);
        g.bufferData(g.ARRAY_BUFFER, this.eh, g.DYNAMIC_DRAW);
        g.bindBuffer(g.ELEMENT_ARRAY_BUFFER, this.bg);
        g.bufferData(g.ELEMENT_ARRAY_BUFFER, this.zb, g.DYNAMIC_DRAW);
        this.sb = p
    }
});
F.iC.create = function (a, c, d) {
    return new F.iC(a, c, d)
};
F.FU = F.za.extend({
    uw: q, Ni: q, JN: q, lp: q, ctor: function () {
        F.RC();
        this.lp = F.l;
        this.JN = [0, 0, 0, 0];
        this.Ni = q;
        this.uw = this.lp.createFramebuffer()
    }, j5: function (a) {
        var c = this.lp;
        this.Ni = c.getParameter(c.FRAMEBUFFER_BINDING);
        c.bindFramebuffer(c.FRAMEBUFFER, this.uw);
        c.framebufferTexture2D(c.FRAMEBUFFER, c.COLOR_ATTACHMENT0, c.TEXTURE_2D, a.ve, 0);
        c.checkFramebufferStatus(c.FRAMEBUFFER) != c.FRAMEBUFFER_COMPLETE && F.log("Frame Grabber: could not attach texture to frmaebuffer");
        c.bindFramebuffer(c.FRAMEBUFFER, this.Ni)
    },
    z2: function () {
        var a = this.lp;
        this.Ni = a.getParameter(a.FRAMEBUFFER_BINDING);
        a.bindFramebuffer(a.FRAMEBUFFER, this.uw);
        this.JN = a.getParameter(a.COLOR_CLEAR_VALUE);
        a.clearColor(0, 0, 0, 0);
        a.clear(a.COLOR_BUFFER_BIT | a.DEPTH_BUFFER_BIT)
    }, c2: function () {
        var a = this.lp;
        a.bindFramebuffer(a.FRAMEBUFFER, this.Ni);
        a.colorMask(p, p, p, p)
    }, lea: function () {
        this.lp.deleteFramebuffer(this.uw)
    }
});
F.ko = -1;
F.$d = F.za.extend({
    originalTarget: q, target: q, tag: F.ko, ctor: function () {
        this.target = this.originalTarget = q;
        this.tag = F.ko
    }, copy: function () {
        F.log("copy is deprecated. Please use clone instead.");
        return this.m()
    }, m: function () {
        var a = new F.$d;
        a.originalTarget = q;
        a.target = q;
        a.tag = this.tag;
        return a
    }, mh: D(p), D: function (a) {
        this.target = this.originalTarget = a
    }, stop: function () {
        this.target = q
    }, step: function () {
        F.log("[Action step]. override me")
    }, update: function () {
        F.log("[Action update]. override me")
    }, Jha: A("target"), pj: z("target"),
    $ga: A("originalTarget"), eoa: z("originalTarget"), JG: A("tag"), rA: z("tag"), $z: u(), oj: u()
});
F.action = function () {
    return new F.$d
};
F.$d.create = F.action;
F.vj = F.$d.extend({
    t: 0, ctor: function () {
        F.$d.prototype.ctor.call(this);
        this.t = 0
    }, xq: function () {
        return this.t * (this.uc || 1)
    }, sg: z("t"), reverse: function () {
        F.log("cocos2d: FiniteTimeAction#reverse: Implement me");
        return q
    }, m: function () {
        return new F.vj
    }
});
F.dw = F.$d.extend({
    Me: 0, Ab: q, ctor: function (a, c) {
        F.$d.prototype.ctor.call(this);
        this.Me = 0;
        this.Ab = q;
        a && this.ga(a, c)
    }, Kl: A("Me"), Xd: z("Me"), ga: function (a, c) {
        a || b("cc.Speed.initWithAction(): action must be non nil");
        this.Ab = a;
        this.Me = c;
        return p
    }, m: function () {
        var a = new F.dw;
        a.ga(this.Ab.m(), this.Me);
        return a
    }, D: function (a) {
        F.$d.prototype.D.call(this, a);
        this.Ab.D(a)
    }, stop: function () {
        this.Ab.stop();
        F.$d.prototype.stop.call(this)
    }, step: function (a) {
        this.Ab.step(a * this.Me)
    }, mh: function () {
        return this.Ab.mh()
    },
    reverse: function () {
        return new F.dw(this.Ab.reverse(), this.Me)
    }, lS: function (a) {
        this.Ab != a && (this.Ab = a)
    }, BG: A("Ab")
});
F.speed = function (a, c) {
    return new F.dw(a, c)
};
F.dw.create = F.speed;
F.bB = F.$d.extend({
    hp: q, Oo: r, Iw: r, mx: q, Hs: q, FF: q, Un: 0, $n: 0, fo: 0, An: 0, ctor: function (a, c) {
        F.$d.prototype.ctor.call(this);
        this.hp = q;
        this.Iw = this.Oo = r;
        this.Hs = this.mx = q;
        this.An = this.fo = this.$n = this.Un = 0;
        this.FF = F.rect(0, 0, 0, 0);
        a && (c ? this.Fq(a, c) : this.Fq(a))
    }, m: function () {
        var a = new F.bB, c = this.FF;
        a.Fq(this.hp, new F.NJ(c.x, c.y, c.width, c.height));
        return a
    }, Aia: A("Oo"), ena: z("Oo"), Fq: function (a, c) {
        a || b("cc.Follow.initWithAction(): followedNode must be non nil");
        c = c || F.rect(0, 0, 0, 0);
        this.hp = a;
        this.FF = c;
        this.Oo = !F.sE(c);
        this.Iw = r;
        var d = F.L.Pa();
        this.Hs = F.d(d.width, d.height);
        this.mx = F.lj(this.Hs, 0.5);
        this.Oo && (this.Un = -(c.x + c.width - this.Hs.x), this.$n = -c.x, this.fo = -c.y, this.An = -(c.y + c.height - this.Hs.y), this.$n < this.Un && (this.$n = this.Un = (this.Un + this.$n) / 2), this.fo < this.An && (this.fo = this.An = (this.fo + this.An) / 2), this.fo == this.An && this.Un == this.$n && (this.Iw = p));
        return p
    }, step: function () {
        var a = this.hp.x, c = this.hp.y, a = this.mx.x - a, c = this.mx.y - c;
        this.Oo ? this.Iw || this.target.X(F.od(a, this.Un, this.$n), F.od(c, this.An,
            this.fo)) : this.target.X(a, c)
    }, mh: function () {
        return !this.hp.running
    }, stop: function () {
        this.target = q;
        F.$d.prototype.stop.call(this)
    }
});
F.K3 = function (a, c) {
    return new F.bB(a, c)
};
F.bB.create = F.K3;
F.C = F.vj.extend({
    yb: 0, hx: r, Qg: q, uc: 1, gt: r, Dp: r, Me: 1, ot: r, ctor: function (a) {
        this.uc = this.Me = 1;
        this.gt = r;
        this.MAX_VALUE = 2;
        this.ot = this.Dp = r;
        F.vj.prototype.ctor.call(this);
        a !== k && this.f(a)
    }, ega: A("yb"), f: function (a) {
        this.t = 0 === a ? F.Ev : a;
        this.yb = 0;
        return this.hx = p
    }, mh: function () {
        return this.yb >= this.t
    }, Ha: function (a) {
        a.gt = this.gt;
        a.Me = this.Me;
        a.uc = this.uc;
        a.Qg = this.Qg;
        a.ot = this.ot;
        a.Dp = this.Dp
    }, Ke: function (a) {
        if (this.Qg) {
            a.Qg = [];
            for (var c = 0; c < this.Qg.length; c++)a.Qg.push(this.Qg[c].reverse())
        }
    }, m: function () {
        var a =
            new F.C(this.t);
        this.Ha(a);
        return a
    }, fb: function (a) {
        this.Qg ? this.Qg.length = 0 : this.Qg = [];
        for (var c = 0; c < arguments.length; c++)this.Qg.push(arguments[c]);
        return this
    }, Kd: function (a) {
        var c = this.Qg;
        if (!c || 0 === c.length)return a;
        for (var d = 0, e = c.length; d < e; d++)a = c[d].fb(a);
        return a
    }, step: function (a) {
        this.hx ? (this.hx = r, this.yb = 0) : this.yb += a;
        a = this.yb / (1.192092896E-7 < this.t ? this.t : 1.192092896E-7);
        a = 1 > a ? a : 1;
        this.update(0 < a ? a : 0);
        this.Dp && (1 < this.uc && this.mh()) && (this.gt || this.uc--, this.D(this.target), this.step(this.yb -
            this.t))
    }, D: function (a) {
        F.$d.prototype.D.call(this, a);
        this.yb = 0;
        this.hx = p
    }, reverse: function () {
        F.log("cc.IntervalAction: reverse not implemented.");
        return q
    }, ao: function () {
        F.log("cc.ActionInterval.setAmplitudeRate(): it should be overridden in subclass.")
    }, Nn: function () {
        F.log("cc.ActionInterval.getAmplitudeRate(): it should be overridden in subclass.");
        return 0
    }, speed: function (a) {
        if (0 >= a)return F.log("The speed parameter error"), this;
        this.ot = p;
        this.Me *= a;
        return this
    }, Kl: A("Me"), Xd: function (a) {
        this.Me =
            a;
        return this
    }, repeat: function (a) {
        a = Math.round(a);
        if (isNaN(a) || 1 > a)return F.log("The repeat parameter error"), this;
        this.Dp = p;
        this.uc *= a;
        return this
    }, EH: function () {
        this.Dp = p;
        this.uc = this.MAX_VALUE;
        this.gt = p;
        return this
    }
});
F.S1 = function (a) {
    return new F.C(a)
};
F.C.create = F.S1;
F.zj = F.C.extend({
    Bi: q, GO: q, Ts: 0, ctor: function (a) {
        F.C.prototype.ctor.call(this);
        this.Bi = [];
        var c = a instanceof Array ? a : arguments, d = c.length - 1;
        0 <= d && c[d] == q && F.log("parameters should not be ending with null in Javascript");
        if (0 <= d) {
            for (var e = c[0], f = 1; f < d; f++)c[f] && (e = F.zj.Bj(e, c[f]));
            this.Rn(e, c[d])
        }
    }, Rn: function (a, c) {
        (!a || !c) && b("cc.Sequence.initWithTwoActions(): arguments must all be non nil");
        this.f(a.t + c.t);
        this.Bi[0] = a;
        this.Bi[1] = c;
        return p
    }, m: function () {
        var a = new F.zj;
        this.Ha(a);
        a.Rn(this.Bi[0].m(),
            this.Bi[1].m());
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.GO = this.Bi[0].t / this.t;
        this.Ts = -1
    }, stop: function () {
        -1 !== this.Ts && this.Bi[this.Ts].stop();
        F.$d.prototype.stop.call(this)
    }, update: function (a) {
        a = this.Kd(a);
        var c = 0, d = this.GO, e = this.Bi, f = this.Ts;
        a < d ? (a = 0 !== d ? a / d : 1, 0 === c && 1 === f && (e[1].update(0), e[1].stop())) : (c = 1, a = 1 === d ? 1 : (a - d) / (1 - d), -1 === f && (e[0].D(this.target), e[0].update(1), e[0].stop()), f || (e[0].update(1), e[0].stop()));
        f === c && e[c].mh() || (f !== c && e[c].D(this.target), e[c].update(a),
            this.Ts = c)
    }, reverse: function () {
        var a = F.zj.Bj(this.Bi[1].reverse(), this.Bi[0].reverse());
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.hb = function (a) {
    var c = a instanceof Array ? a : arguments;
    0 < c.length && c[c.length - 1] == q && F.log("parameters should not be ending with null in Javascript");
    for (var d = c[0], e = 1; e < c.length; e++)c[e] && (d = F.zj.Bj(d, c[e]));
    return d
};
F.zj.create = F.hb;
F.zj.Bj = function (a, c) {
    var d = new F.zj;
    d.Rn(a, c);
    return d
};
F.Tv = F.C.extend({
    uc: 0, yl: 0, QD: 0, qL: r, Ab: q, ctor: function (a, c) {
        F.C.prototype.ctor.call(this);
        c !== k && this.ga(a, c)
    }, ga: function (a, c) {
        return this.f(a.t * c) ? (this.uc = c, this.Ab = a, a instanceof F.Ve && (this.qL = p, this.uc -= 1), this.yl = 0, p) : r
    }, m: function () {
        var a = new F.Tv;
        this.Ha(a);
        a.ga(this.Ab.m(), this.uc);
        return a
    }, D: function (a) {
        this.yl = 0;
        this.QD = this.Ab.t / this.t;
        F.C.prototype.D.call(this, a);
        this.Ab.D(a)
    }, stop: function () {
        this.Ab.stop();
        F.$d.prototype.stop.call(this)
    }, update: function (a) {
        a = this.Kd(a);
        var c = this.Ab,
            d = this.t, e = this.uc, f = this.QD;
        if (a >= f) {
            for (; a > f && this.yl < e;)c.update(1), this.yl++, c.stop(), c.D(this.target), this.QD = f += c.t / d;
            1 <= a && this.yl < e && this.yl++;
            this.qL || (this.yl === e ? (c.update(1), c.stop()) : c.update(a - (f - c.t / d)))
        } else c.update(a * e % 1)
    }, mh: function () {
        return this.yl == this.uc
    }, reverse: function () {
        var a = new F.Tv(this.Ab.reverse(), this.uc);
        this.Ha(a);
        this.Ke(a);
        return a
    }, lS: function (a) {
        this.Ab != a && (this.Ab = a)
    }, BG: A("Ab")
});
F.repeat = function (a, c) {
    return new F.Tv(a, c)
};
F.Tv.create = F.repeat;
F.Uv = F.C.extend({
    Ab: q, ctor: function (a) {
        F.C.prototype.ctor.call(this);
        this.Ab = q;
        a && this.ga(a)
    }, ga: function (a) {
        a || b("cc.RepeatForever.initWithAction(): action must be non null");
        this.Ab = a;
        return p
    }, m: function () {
        var a = new F.Uv;
        this.Ha(a);
        a.ga(this.Ab.m());
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.Ab.D(a)
    }, step: function (a) {
        var c = this.Ab;
        c.step(a);
        c.mh() && (c.D(this.target), c.step(c.yb - c.t))
    }, mh: D(r), reverse: function () {
        var a = new F.Uv(this.Ab.reverse());
        this.Ha(a);
        this.Ke(a);
        return a
    }, lS: function (a) {
        this.Ab !=
        a && (this.Ab = a)
    }, BG: A("Ab")
});
F.EH = function (a) {
    return new F.Uv(a)
};
F.Uv.create = F.EH;
F.tm = F.C.extend({
    Mj: q, fk: q, ctor: function (a) {
        F.C.prototype.ctor.call(this);
        this.fk = this.Mj = q;
        var c = a instanceof Array ? a : arguments, d = c.length - 1;
        0 <= d && c[d] == q && F.log("parameters should not be ending with null in Javascript");
        if (0 <= d) {
            for (var e = c[0], f = 1; f < d; f++)c[f] && (e = F.tm.Bj(e, c[f]));
            this.Rn(e, c[d])
        }
    }, Rn: function (a, c) {
        (!a || !c) && b("cc.Spawn.initWithTwoActions(): arguments must all be non null");
        var d = r, e = a.t, f = c.t;
        this.f(Math.max(e, f)) && (this.Mj = a, this.fk = c, e > f ? this.fk = F.zj.Bj(c, F.jc(e - f)) : e < f && (this.Mj =
            F.zj.Bj(a, F.jc(f - e))), d = p);
        return d
    }, m: function () {
        var a = new F.tm;
        this.Ha(a);
        a.Rn(this.Mj.m(), this.fk.m());
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.Mj.D(a);
        this.fk.D(a)
    }, stop: function () {
        this.Mj.stop();
        this.fk.stop();
        F.$d.prototype.stop.call(this)
    }, update: function (a) {
        a = this.Kd(a);
        this.Mj && this.Mj.update(a);
        this.fk && this.fk.update(a)
    }, reverse: function () {
        var a = F.tm.Bj(this.Mj.reverse(), this.fk.reverse());
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.pi = function (a) {
    var c = a instanceof Array ? a : arguments;
    0 < c.length && c[c.length - 1] == q && F.log("parameters should not be ending with null in Javascript");
    for (var d = c[0], e = 1; e < c.length; e++)c[e] != q && (d = F.tm.Bj(d, c[e]));
    return d
};
F.tm.create = F.pi;
F.tm.Bj = function (a, c) {
    var d = new F.tm;
    d.Rn(a, c);
    return d
};
F.NB = F.C.extend({
    Yw: 0, pt: 0, XL: 0, $C: 0, Ip: 0, YL: 0, ctor: function (a, c, d) {
        F.C.prototype.ctor.call(this);
        c !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        return F.C.prototype.f.call(this, a) ? (this.Yw = c || 0, this.$C = d || this.Yw, p) : r
    }, m: function () {
        var a = new F.NB;
        this.Ha(a);
        a.f(this.t, this.Yw, this.$C);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        var c = a.rotationX % 360, d = this.Yw - c;
        180 < d && (d -= 360);
        -180 > d && (d += 360);
        this.pt = c;
        this.XL = d;
        this.Ip = a.rotationY % 360;
        a = this.$C - this.Ip;
        180 < a && (a -= 360);
        -180 > a && (a += 360);
        this.YL =
            a
    }, reverse: function () {
        F.log("cc.RotateTo.reverse(): it should be overridden in subclass.")
    }, update: function (a) {
        a = this.Kd(a);
        this.target && (this.target.rotationX = this.pt + this.XL * a, this.target.rotationY = this.Ip + this.YL * a)
    }
});
F.XR = function (a, c, d) {
    return new F.NB(a, c, d)
};
F.NB.create = F.XR;
F.Vv = F.C.extend({
    Ch: 0, pt: 0, Ew: 0, Ip: 0, ctor: function (a, c, d) {
        F.C.prototype.ctor.call(this);
        c !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        return F.C.prototype.f.call(this, a) ? (this.Ch = c || 0, this.Ew = d || this.Ch, p) : r
    }, m: function () {
        var a = new F.Vv;
        this.Ha(a);
        a.f(this.t, this.Ch, this.Ew);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.pt = a.rotationX;
        this.Ip = a.rotationY
    }, update: function (a) {
        a = this.Kd(a);
        this.target && (this.target.rotationX = this.pt + this.Ch * a, this.target.rotationY = this.Ip + this.Ew * a)
    }, reverse: function () {
        var a =
            new F.Vv(this.t, -this.Ch, -this.Ew);
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.WR = function (a, c, d) {
    return new F.Vv(a, c, d)
};
F.Vv.create = F.WR;
F.Fk = F.C.extend({
    Qi: q, Zc: q, ld: q, ctor: function (a, c, d) {
        F.C.prototype.ctor.call(this);
        this.Qi = F.d(0, 0);
        this.Zc = F.d(0, 0);
        this.ld = F.d(0, 0);
        c !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        return F.C.prototype.f.call(this, a) ? (c.x !== k && (d = c.y, c = c.x), this.Qi.x = c, this.Qi.y = d, p) : r
    }, m: function () {
        var a = new F.Fk;
        this.Ha(a);
        a.f(this.t, this.Qi);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        var c = a.$b();
        a = a.va();
        this.ld.x = c;
        this.ld.y = a;
        this.Zc.x = c;
        this.Zc.y = a
    }, update: function (a) {
        a = this.Kd(a);
        if (this.target) {
            var c =
                this.Qi.x * a;
            a *= this.Qi.y;
            var d = this.Zc;
            if (F.lv) {
                var e = this.target.va(), f = this.ld;
                d.x = d.x + this.target.$b() - f.x;
                d.y = d.y + e - f.y;
                c += d.x;
                a += d.y;
                f.x = c;
                f.y = a;
                this.target.X(c, a)
            } else this.target.X(d.x + c, d.y + a)
        }
    }, reverse: function () {
        var a = new F.Fk(this.t, F.d(-this.Qi.x, -this.Qi.y));
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.moveBy = function (a, c, d) {
    return new F.Fk(a, c, d)
};
F.Fk.create = F.moveBy;
F.pB = F.Fk.extend({
    Zf: q, ctor: function (a, c, d) {
        F.Fk.prototype.ctor.call(this);
        this.Zf = F.d(0, 0);
        c !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        return F.Fk.prototype.f.call(this, a, c, d) ? (c.x !== k && (d = c.y, c = c.x), this.Zf.x = c, this.Zf.y = d, p) : r
    }, m: function () {
        var a = new F.pB;
        this.Ha(a);
        a.f(this.t, this.Zf);
        return a
    }, D: function (a) {
        F.Fk.prototype.D.call(this, a);
        this.Qi.x = this.Zf.x - a.$b();
        this.Qi.y = this.Zf.y - a.va()
    }
});
F.moveTo = function (a, c, d) {
    return new F.pB(a, c, d)
};
F.pB.create = F.moveTo;
F.sm = F.C.extend({
    $g: 0, ah: 0, dy: 0, ey: 0, bx: 0, cx: 0, yf: 0, zf: 0, ctor: function (a, c, d) {
        F.C.prototype.ctor.call(this);
        d !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        var e = r;
        F.C.prototype.f.call(this, a) && (this.bx = c, this.cx = d, e = p);
        return e
    }, m: function () {
        var a = new F.sm;
        this.Ha(a);
        a.f(this.t, this.bx, this.cx);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.dy = a.skewX % 180;
        this.yf = this.bx - this.dy;
        180 < this.yf && (this.yf -= 360);
        -180 > this.yf && (this.yf += 360);
        this.ey = a.skewY % 360;
        this.zf = this.cx - this.ey;
        180 < this.zf && (this.zf -=
            360);
        -180 > this.zf && (this.zf += 360)
    }, update: function (a) {
        a = this.Kd(a);
        this.target.skewX = this.dy + this.yf * a;
        this.target.skewY = this.ey + this.zf * a
    }
});
F.i9 = function (a, c, d) {
    return new F.sm(a, c, d)
};
F.sm.create = F.i9;
F.cw = F.sm.extend({
    ctor: function (a, c, d) {
        F.sm.prototype.ctor.call(this);
        d !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        var e = r;
        F.sm.prototype.f.call(this, a, c, d) && (this.$g = c, this.ah = d, e = p);
        return e
    }, m: function () {
        var a = new F.cw;
        this.Ha(a);
        a.f(this.t, this.$g, this.ah);
        return a
    }, D: function (a) {
        F.sm.prototype.D.call(this, a);
        this.yf = this.$g;
        this.zf = this.ah;
        this.bx = this.dy + this.yf;
        this.cx = this.ey + this.zf
    }, reverse: function () {
        var a = new F.cw(this.t, -this.$g, -this.ah);
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.h9 = function (a, c, d) {
    return new F.cw(a, c, d)
};
F.cw.create = F.h9;
F.Ek = F.C.extend({
    Zc: q, Ei: q, Os: 0, Kj: 0, ld: q, ctor: function (a, c, d, e, f) {
        F.C.prototype.ctor.call(this);
        this.Zc = F.d(0, 0);
        this.ld = F.d(0, 0);
        this.Ei = F.d(0, 0);
        e !== k && this.f(a, c, d, e, f)
    }, f: function (a, c, d, e, f) {
        return F.C.prototype.f.call(this, a) ? (f === k && (f = e, e = d, d = c.y, c = c.x), this.Ei.x = c, this.Ei.y = d, this.Os = e, this.Kj = f, p) : r
    }, m: function () {
        var a = new F.Ek;
        this.Ha(a);
        a.f(this.t, this.Ei, this.Os, this.Kj);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        var c = a.$b();
        a = a.va();
        this.ld.x = c;
        this.ld.y = a;
        this.Zc.x = c;
        this.Zc.y =
            a
    }, update: function (a) {
        a = this.Kd(a);
        if (this.target) {
            var c = a * this.Kj % 1, c = 4 * this.Os * c * (1 - c), c = c + this.Ei.y * a;
            a *= this.Ei.x;
            var d = this.Zc;
            if (F.lv) {
                var e = this.target.va(), f = this.ld;
                d.x = d.x + this.target.$b() - f.x;
                d.y = d.y + e - f.y;
                a += d.x;
                c += d.y;
                f.x = a;
                f.y = c;
                this.target.X(a, c)
            } else this.target.X(d.x + a, d.y + c)
        }
    }, reverse: function () {
        var a = new F.Ek(this.t, F.d(-this.Ei.x, -this.Ei.y), this.Os, this.Kj);
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.bR = function (a, c, d, e, f) {
    return new F.Ek(a, c, d, e, f)
};
F.Ek.create = F.bR;
F.hB = F.Ek.extend({
    Zf: q, ctor: function (a, c, d, e, f) {
        F.Ek.prototype.ctor.call(this);
        this.Zf = F.d(0, 0);
        e !== k && this.f(a, c, d, e, f)
    }, f: function (a, c, d, e, f) {
        return F.Ek.prototype.f.call(this, a, c, d, e, f) ? (f === k && (d = c.y, c = c.x), this.Zf.x = c, this.Zf.y = d, p) : r
    }, D: function (a) {
        F.Ek.prototype.D.call(this, a);
        this.Ei.x = this.Zf.x - this.Zc.x;
        this.Ei.y = this.Zf.y - this.Zc.y
    }, m: function () {
        var a = new F.hB;
        this.Ha(a);
        a.f(this.t, this.Zf, this.Os, this.Kj);
        return a
    }
});
F.S5 = function (a, c, d, e, f) {
    return new F.hB(a, c, d, e, f)
};
F.hB.create = F.S5;
F.kP = function (a, c, d, e) {
    return 0 * Math.pow(1 - e, 3) + 3 * e * Math.pow(1 - e, 2) * a + 3 * Math.pow(e, 2) * (1 - e) * c + Math.pow(e, 3) * d
};
F.jm = F.C.extend({
    Wf: q, Zc: q, ld: q, ctor: function (a, c) {
        F.C.prototype.ctor.call(this);
        this.Wf = [];
        this.Zc = F.d(0, 0);
        this.ld = F.d(0, 0);
        c && this.f(a, c)
    }, f: function (a, c) {
        return F.C.prototype.f.call(this, a) ? (this.Wf = c, p) : r
    }, m: function () {
        var a = new F.jm;
        this.Ha(a);
        for (var c = [], d = 0; d < this.Wf.length; d++) {
            var e = this.Wf[d];
            c.push(F.d(e.x, e.y))
        }
        a.f(this.t, c);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        var c = a.$b();
        a = a.va();
        this.ld.x = c;
        this.ld.y = a;
        this.Zc.x = c;
        this.Zc.y = a
    }, update: function (a) {
        a = this.Kd(a);
        if (this.target) {
            var c =
                this.Wf, d = F.kP(c[0].x, c[1].x, c[2].x, a);
            a = F.kP(c[0].y, c[1].y, c[2].y, a);
            c = this.Zc;
            if (F.lv) {
                var e = this.target.va(), f = this.ld;
                c.x = c.x + this.target.$b() - f.x;
                c.y = c.y + e - f.y;
                d += c.x;
                a += c.y;
                f.x = d;
                f.y = a;
                this.target.X(d, a)
            } else this.target.X(c.x + d, c.y + a)
        }
    }, reverse: function () {
        var a = this.Wf, a = [F.sk(a[1], F.tH(a[2])), F.sk(a[0], F.tH(a[2])), F.tH(a[2])], a = new F.jm(this.t, a);
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.B2 = function (a, c) {
    return new F.jm(a, c)
};
F.jm.create = F.B2;
F.JA = F.jm.extend({
    py: q, ctor: function (a, c) {
        F.jm.prototype.ctor.call(this);
        this.py = [];
        c && this.f(a, c)
    }, f: function (a, c) {
        return F.C.prototype.f.call(this, a) ? (this.py = c, p) : r
    }, m: function () {
        var a = new F.JA;
        this.Ha(a);
        a.f(this.t, this.py);
        return a
    }, D: function (a) {
        F.jm.prototype.D.call(this, a);
        a = this.Zc;
        var c = this.py, d = this.Wf;
        d[0] = F.ie(c[0], a);
        d[1] = F.ie(c[1], a);
        d[2] = F.ie(c[2], a)
    }
});
F.C2 = function (a, c) {
    return new F.JA(a, c)
};
F.JA.create = F.C2;
F.Ur = F.C.extend({
    sa: 1, Ya: 1, qt: 1, rt: 1, ap: 0, bp: 0, yf: 0, zf: 0, ctor: function (a, c, d) {
        F.C.prototype.ctor.call(this);
        c !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        return F.C.prototype.f.call(this, a) ? (this.ap = c, this.bp = d != q ? d : c, p) : r
    }, m: function () {
        var a = new F.Ur;
        this.Ha(a);
        a.f(this.t, this.ap, this.bp);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.qt = a.scaleX;
        this.rt = a.scaleY;
        this.yf = this.ap - this.qt;
        this.zf = this.bp - this.rt
    }, update: function (a) {
        a = this.Kd(a);
        this.target && (this.target.scaleX = this.qt + this.yf *
            a, this.target.scaleY = this.rt + this.zf * a)
    }
});
F.Ud = function (a, c, d) {
    return new F.Ur(a, c, d)
};
F.Ur.create = F.Ud;
F.aw = F.Ur.extend({
    D: function (a) {
        F.Ur.prototype.D.call(this, a);
        this.yf = this.qt * this.ap - this.qt;
        this.zf = this.rt * this.bp - this.rt
    }, reverse: function () {
        var a = new F.aw(this.t, 1 / this.ap, 1 / this.bp);
        this.Ha(a);
        this.Ke(a);
        return a
    }, m: function () {
        var a = new F.aw;
        this.Ha(a);
        a.f(this.t, this.ap, this.bp);
        return a
    }
});
F.YR = function (a, c, d) {
    return new F.aw(a, c, d)
};
F.aw.create = F.YR;
F.iv = F.C.extend({
    uc: 0, NN: r, ctor: function (a, c) {
        F.C.prototype.ctor.call(this);
        c !== k && this.f(a, c)
    }, f: function (a, c) {
        return F.C.prototype.f.call(this, a) ? (this.uc = c, p) : r
    }, m: function () {
        var a = new F.iv;
        this.Ha(a);
        a.f(this.t, this.uc);
        return a
    }, update: function (a) {
        a = this.Kd(a);
        if (this.target && !this.mh()) {
            var c = 1 / this.uc;
            this.target.visible = a % c > c / 2
        }
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.NN = a.visible
    }, stop: function () {
        this.target.visible = this.NN;
        F.C.prototype.stop.call(this)
    }, reverse: function () {
        var a =
            new F.iv(this.t, this.uc);
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.blink = function (a, c) {
    return new F.iv(a, c)
};
F.iv.create = F.blink;
F.Dk = F.C.extend({
    Pp: 0, jx: 0, ctor: function (a, c) {
        F.C.prototype.ctor.call(this);
        c !== k && this.f(a, c)
    }, f: function (a, c) {
        return F.C.prototype.f.call(this, a) ? (this.Pp = c, p) : r
    }, m: function () {
        var a = new F.Dk;
        this.Ha(a);
        a.f(this.t, this.Pp);
        return a
    }, update: function (a) {
        a = this.Kd(a);
        var c = this.jx !== k ? this.jx : 255;
        this.target.opacity = c + (this.Pp - c) * a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.jx = a.opacity
    }
});
F.VP = function (a, c) {
    return new F.Dk(a, c)
};
F.Dk.create = F.VP;
F.Fv = F.Dk.extend({
    DE: q, ctor: function (a) {
        F.Dk.prototype.ctor.call(this);
        a && this.f(a, 255)
    }, reverse: function () {
        var a = new F.Gv;
        a.f(this.t, 0);
        this.Ha(a);
        this.Ke(a);
        return a
    }, m: function () {
        var a = new F.Fv;
        this.Ha(a);
        a.f(this.t, this.Pp);
        return a
    }, D: function (a) {
        this.DE && (this.Pp = this.DE.jx);
        F.Dk.prototype.D.call(this, a)
    }
});
F.rG = function (a) {
    return new F.Fv(a)
};
F.Fv.create = F.rG;
F.Gv = F.Dk.extend({
    ctor: function (a) {
        F.Dk.prototype.ctor.call(this);
        a && this.f(a, 0)
    }, reverse: function () {
        var a = new F.Fv;
        a.DE = this;
        a.f(this.t, 255);
        this.Ha(a);
        this.Ke(a);
        return a
    }, m: function () {
        var a = new F.Gv;
        this.Ha(a);
        a.f(this.t, this.Pp);
        return a
    }
});
F.Kn = function (a) {
    return new F.Gv(a)
};
F.Gv.create = F.Kn;
F.jC = F.C.extend({
    Rd: q, Ld: q, ctor: function (a, c, d, e) {
        F.C.prototype.ctor.call(this);
        this.Rd = F.color(0, 0, 0);
        this.Ld = F.color(0, 0, 0);
        e !== k && this.f(a, c, d, e)
    }, f: function (a, c, d, e) {
        return F.C.prototype.f.call(this, a) ? (this.Rd = F.color(c, d, e), p) : r
    }, m: function () {
        var a = new F.jC;
        this.Ha(a);
        var c = this.Rd;
        a.f(this.t, c.r, c.g, c.b);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.Ld = this.target.color
    }, update: function (a) {
        a = this.Kd(a);
        var c = this.Ld, d = this.Rd;
        c && (this.target.color = F.color(c.r + (d.r - c.r) * a, c.g + (d.g -
            c.g) * a, c.b + (d.b - c.b) * a))
    }
});
F.K9 = function (a, c, d, e) {
    return new F.jC(a, c, d, e)
};
F.jC.create = F.K9;
F.mw = F.C.extend({
    Ww: 0, Vw: 0, Uw: 0, VM: 0, UM: 0, TM: 0, ctor: function (a, c, d, e) {
        F.C.prototype.ctor.call(this);
        e !== k && this.f(a, c, d, e)
    }, f: function (a, c, d, e) {
        return F.C.prototype.f.call(this, a) ? (this.Ww = c, this.Vw = d, this.Uw = e, p) : r
    }, m: function () {
        var a = new F.mw;
        this.Ha(a);
        a.f(this.t, this.Ww, this.Vw, this.Uw);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        a = a.color;
        this.VM = a.r;
        this.UM = a.g;
        this.TM = a.b
    }, update: function (a) {
        a = this.Kd(a);
        this.target.color = F.color(this.VM + this.Ww * a, this.UM + this.Vw * a, this.TM + this.Uw *
            a)
    }, reverse: function () {
        var a = new F.mw(this.t, -this.Ww, -this.Vw, -this.Uw);
        this.Ha(a);
        this.Ke(a);
        return a
    }
});
F.J9 = function (a, c, d, e) {
    return new F.mw(a, c, d, e)
};
F.mw.create = F.J9;
F.kv = F.C.extend({
    update: u(), reverse: function () {
        var a = new F.kv(this.t);
        this.Ha(a);
        this.Ke(a);
        return a
    }, m: function () {
        var a = new F.kv;
        this.Ha(a);
        a.f(this.t);
        return a
    }
});
F.jc = function (a) {
    return new F.kv(a)
};
F.kv.create = F.jc;
F.Rr = F.C.extend({
    Oj: q, ctor: function (a) {
        F.C.prototype.ctor.call(this);
        this.Oj = q;
        a && this.ga(a)
    }, ga: function (a) {
        a || b("cc.ReverseTime.initWithAction(): action must be non null");
        a == this.Oj && b("cc.ReverseTime.initWithAction(): the action was already passed in.");
        return F.C.prototype.f.call(this, a.t) ? (this.Oj = a, p) : r
    }, m: function () {
        var a = new F.Rr;
        this.Ha(a);
        a.ga(this.Oj.m());
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.Oj.D(a)
    }, update: function (a) {
        a = this.Kd(a);
        this.Oj && this.Oj.update(1 - a)
    }, reverse: function () {
        return this.Oj.m()
    },
    stop: function () {
        this.Oj.stop();
        F.$d.prototype.stop.call(this)
    }
});
F.VR = function (a) {
    return new F.Rr(a)
};
F.Rr.create = F.VR;
F.hv = F.C.extend({
    Tk: q, Ys: 0, WD: q, ex: 0, ZE: q, ctor: function (a) {
        F.C.prototype.ctor.call(this);
        this.ZE = [];
        a && this.LQ(a)
    }, bQ: A("Tk"), V7: z("Tk"), LQ: function (a) {
        a || b("cc.Animate.initWithAnimation(): animation must be non-NULL");
        var c = a.xq();
        if (this.f(c * a.Lj)) {
            this.Ys = 0;
            this.V7(a);
            this.WD = q;
            this.ex = 0;
            var d = this.ZE, e = d.length = 0, f = c / a.aj;
            a = a.se;
            F.Gt(a, F.Cg);
            for (var g = 0; g < a.length; g++) {
                var h = e * f / c, e = e + a[g].re;
                d.push(h)
            }
            return p
        }
        return r
    }, m: function () {
        var a = new F.hv;
        this.Ha(a);
        a.LQ(this.Tk.m());
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this,
            a);
        this.Tk.cn && (this.WD = a.a3());
        this.ex = this.Ys = 0
    }, update: function (a) {
        a = this.Kd(a);
        1 > a && (a *= this.Tk.Lj, (0 | a) > this.ex && (this.Ys = 0, this.ex++), a %= 1);
        for (var c = this.Tk.se, d = c.length, e = this.ZE, f = this.Ys; f < d; f++)if (e[f] <= a)this.target.bm(c[f].jk()), this.Ys = f + 1; else break
    }, reverse: function () {
        var a = this.Tk, c = a.se, d = [];
        F.Gt(c, F.Cg);
        if (0 < c.length)for (var e = c.length - 1; 0 <= e; e--) {
            var f = c[e];
            if (!f)break;
            d.push(f.m())
        }
        c = new F.sj(d, a.re, a.Lj);
        c.oA(a.cn);
        a = new F.hv(c);
        this.Ha(a);
        this.Ke(a);
        return a
    }, stop: function () {
        this.Tk.cn &&
        this.target && this.target.bm(this.WD);
        F.$d.prototype.stop.call(this)
    }
});
F.h2 = function (a) {
    return new F.hv(a)
};
F.hv.create = F.h2;
F.gC = F.C.extend({
    hs: q, ip: q, ctor: function (a, c) {
        F.C.prototype.ctor.call(this);
        c && this.Fq(a, c)
    }, Fq: function (a, c) {
        return this.f(c.t) ? (this.ip = a, this.hs = c, p) : r
    }, m: function () {
        var a = new F.gC;
        this.Ha(a);
        a.Fq(this.ip, this.hs.m());
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.hs.D(this.ip)
    }, stop: function () {
        this.hs.stop()
    }, update: function (a) {
        a = this.Kd(a);
        this.hs.update(a)
    }, qga: A("ip"), Bna: function (a) {
        this.ip != a && (this.ip = a)
    }
});
F.y9 = function (a, c) {
    return new F.gC(a, c)
};
F.gC.create = F.y9;
F.Ve = F.vj.extend({
    mh: D(p), step: function () {
        this.update(1)
    }, update: u(), reverse: function () {
        return this.m()
    }, m: function () {
        return new F.Ve
    }
});
F.bw = F.Ve.extend({
    update: function () {
        this.target.visible = p
    }, reverse: function () {
        return new F.Lv
    }, m: function () {
        return new F.bw
    }
});
F.show = function () {
    return new F.bw
};
F.bw.create = F.show;
F.Lv = F.Ve.extend({
    update: function () {
        this.target.visible = r
    }, reverse: function () {
        return new F.bw
    }, m: function () {
        return new F.Lv
    }
});
F.hi = function () {
    return new F.Lv
};
F.Lv.create = F.hi;
F.nw = F.Ve.extend({
    update: function () {
        this.target.visible = !this.target.visible
    }, reverse: function () {
        return new F.nw
    }, m: function () {
        return new F.nw
    }
});
F.L9 = function () {
    return new F.nw
};
F.nw.create = F.L9;
F.Sv = F.Ve.extend({
    ux: p, ctor: function (a) {
        F.vj.prototype.ctor.call(this);
        a !== k && this.oa(a)
    }, update: function () {
        this.target.ni(this.ux)
    }, oa: function (a) {
        this.ux = a;
        return p
    }, reverse: function () {
        return new F.Sv(this.ux)
    }, m: function () {
        return new F.Sv(this.ux)
    }
});
F.B7 = function (a) {
    return new F.Sv(a)
};
F.Sv.create = F.B7;
F.Jv = F.Ve.extend({
    pc: r, ctor: function (a) {
        F.vj.prototype.ctor.call(this);
        this.pc = r;
        a !== k && this.NQ(a)
    }, NQ: function (a) {
        this.pc = a;
        return p
    }, update: function () {
        this.target.flippedX = this.pc
    }, reverse: function () {
        return new F.Jv(!this.pc)
    }, m: function () {
        var a = new F.Jv;
        a.NQ(this.pc);
        return a
    }
});
F.G3 = function (a) {
    return new F.Jv(a)
};
F.Jv.create = F.G3;
F.Kv = F.Ve.extend({
    qc: r, ctor: function (a) {
        F.vj.prototype.ctor.call(this);
        this.qc = r;
        a !== k && this.OQ(a)
    }, OQ: function (a) {
        this.qc = a;
        return p
    }, update: function () {
        this.target.flippedY = this.qc
    }, reverse: function () {
        return new F.Kv(!this.qc)
    }, m: function () {
        var a = new F.Kv;
        a.OQ(this.qc);
        return a
    }
});
F.I3 = function (a) {
    return new F.Kv(a)
};
F.Kv.create = F.I3;
F.wB = F.Ve.extend({
    Xh: 0, hf: 0, ctor: function (a, c) {
        F.vj.prototype.ctor.call(this);
        this.hf = this.Xh = 0;
        a !== k && (a.x !== k && (c = a.y, a = a.x), this.TQ(a, c))
    }, TQ: function (a, c) {
        this.Xh = a;
        this.hf = c;
        return p
    }, update: function () {
        this.target.X(this.Xh, this.hf)
    }, m: function () {
        var a = new F.wB;
        a.TQ(this.Xh, this.hf);
        return a
    }
});
F.c7 = function (a, c) {
    return new F.wB(a, c)
};
F.wB.create = F.c7;
F.MA = F.Ve.extend({
    Vj: q, Lw: q, Is: q, Xo: q, ctor: function (a, c, d) {
        F.vj.prototype.ctor.call(this);
        a !== k && (c === k ? this.mz(a) : this.mz(a, c, d))
    }, mz: function (a, c, d) {
        c ? (this.Xo = d, this.Lw = a, this.Vj = c) : a && (this.Is = a);
        return p
    }, execute: function () {
        this.Lw != q ? this.Lw.call(this.Vj, this.target, this.Xo) : this.Is && this.Is.call(q, this.target)
    }, update: function () {
        this.execute()
    }, Kha: A("Vj"), Coa: function (a) {
        a != this.Vj && (this.Vj && (this.Vj = q), this.Vj = a)
    }, m: function () {
        var a = new F.MA;
        this.Vj ? a.mz(this.Lw, this.Vj, this.Xo) : this.Is &&
        a.mz(this.Is);
        return a
    }
});
F.Eb = function (a, c, d) {
    return new F.MA(a, c, d)
};
F.MA.create = F.Eb;
F.CA = F.C.extend({
    OC: 0, PC: 0, QC: 0, LM: 0, MM: 0, NM: 0, UO: 0, VO: 0, WO: 0, ctor: function () {
        F.C.prototype.ctor.call(this);
        this.WO = this.VO = this.UO = this.NM = this.MM = this.LM = this.QC = this.PC = this.OC = 0
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        a = a.On();
        var c = a.eQ();
        this.OC = c.x;
        this.PC = c.y;
        this.QC = c.e;
        c = a.iQ();
        this.LM = c.x;
        this.MM = c.y;
        this.NM = c.e;
        a = a.b5();
        this.UO = a.x;
        this.VO = a.y;
        this.WO = a.e
    }, m: function () {
        return new F.CA
    }, reverse: function () {
        return new F.Rr(this)
    }
});
F.sB = F.CA.extend({
    Cf: 0, XC: 0, ks: 0, VL: 0, Ch: 0, UL: 0, ZN: 0, XN: 0, YN: 0, WN: 0, ctor: function (a, c, d, e, f, g, h) {
        F.CA.prototype.ctor.call(this);
        h !== k && this.f(a, c, d, e, f, g, h)
    }, f: function (a, c, d, e, f, g, h) {
        return F.C.prototype.f.call(this, a) ? (this.Cf = c, this.XC = d, this.ks = e, this.VL = f, this.Ch = g, this.UL = h, this.XN = F.kf(f), this.WN = F.kf(h), p) : r
    }, j9: function () {
        var a, c;
        c = this.target.On();
        var d = c.iQ();
        a = c.eQ();
        c = d.x - a.x;
        var e = d.y - a.y;
        a = d.e - a.e;
        var d = Math.sqrt(Math.pow(c, 2) + Math.pow(e, 2) + Math.pow(a, 2)), f = Math.sqrt(Math.pow(c, 2) +
            Math.pow(e, 2));
        0 === f && (f = F.Ev);
        0 === d && (d = F.Ev);
        a = Math.acos(a / d);
        c = 0 > c ? Math.PI - Math.asin(e / f) : Math.asin(e / f);
        return {L6: d / F.rr.Cq(), e$: a, azimuth: c}
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        a = this.j9();
        isNaN(this.Cf) && (this.Cf = a.L6);
        isNaN(this.ks) && (this.ks = F.AH(a.e$));
        isNaN(this.Ch) && (this.Ch = F.AH(a.azimuth));
        this.ZN = F.kf(this.ks);
        this.YN = F.kf(this.Ch)
    }, m: function () {
        var a = new F.sB;
        a.f(this.t, this.Cf, this.XC, this.ks, this.VL, this.Ch, this.UL);
        return a
    }, update: function (a) {
        a = this.Kd(a);
        var c = (this.Cf +
            this.XC * a) * F.rr.Cq(), d = this.ZN + this.XN * a, e = this.YN + this.WN * a;
        a = Math.sin(d) * Math.cos(e) * c + this.OC;
        e = Math.sin(d) * Math.sin(e) * c + this.PC;
        c = Math.cos(d) * c + this.QC;
        this.target.On().jS(a, e, c);
        this.target.Qa()
    }
});
F.qg = function (a, c, d, e, f, g, h) {
    return new F.sB(a, c, d, e, f, g, h)
};
F.sB.create = F.qg;
F.eb = F.C.extend({
    v: q, ctor: function (a) {
        F.C.prototype.ctor.call(this);
        a && this.ga(a)
    }, ga: function (a) {
        a || b("cc.ActionEase.initWithAction(): action must be non nil");
        return this.f(a.xq()) ? (this.v = a, p) : r
    }, m: function () {
        var a = new F.eb;
        a.ga(this.v.m());
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.v.D(this.target)
    }, stop: function () {
        this.v.stop();
        F.C.prototype.stop.call(this)
    }, update: function (a) {
        this.v.update(a)
    }, reverse: function () {
        return new F.eb(this.v.reverse())
    }, BG: A("v")
});
F.R1 = function (a) {
    return new F.eb(a)
};
F.eb.create = F.R1;
F.lm = F.eb.extend({
    sc: 0, ctor: function (a, c) {
        F.eb.prototype.ctor.call(this);
        c !== k && this.ga(a, c)
    }, noa: z("sc"), lha: A("sc"), ga: function (a, c) {
        return F.eb.prototype.ga.call(this, a) ? (this.sc = c, p) : r
    }, m: function () {
        var a = new F.lm;
        a.ga(this.v.m(), this.sc);
        return a
    }, reverse: function () {
        return new F.lm(this.v.reverse(), 1 / this.sc)
    }
});
F.r3 = function (a, c) {
    return new F.lm(a, c)
};
F.lm.create = F.r3;
F.Av = F.lm.extend({
    update: function (a) {
        this.v.update(Math.pow(a, this.sc))
    }, reverse: function () {
        return new F.Av(this.v.reverse(), 1 / this.sc)
    }, m: function () {
        var a = new F.Av;
        a.ga(this.v.m(), this.sc);
        return a
    }
});
F.Av.create = function (a, c) {
    return new F.Av(a, c)
};
F.o3 = function (a) {
    return {
        sc: a, fb: function (a) {
            return Math.pow(a, this.sc)
        }, reverse: function () {
            return F.o3(1 / this.sc)
        }
    }
};
F.po = F.lm.extend({
    update: function (a) {
        this.v.update(Math.pow(a, 1 / this.sc))
    }, reverse: function () {
        return new F.po(this.v.reverse(), 1 / this.sc)
    }, m: function () {
        var a = new F.po;
        a.ga(this.v.m(), this.sc);
        return a
    }
});
F.po.create = function (a, c) {
    return new F.po(a, c)
};
F.q3 = function (a) {
    return {
        sc: a, fb: function (a) {
            return Math.pow(a, 1 / this.sc)
        }, reverse: function () {
            return F.q3(1 / this.sc)
        }
    }
};
F.oo = F.lm.extend({
    update: function (a) {
        a *= 2;
        1 > a ? this.v.update(0.5 * Math.pow(a, this.sc)) : this.v.update(1 - 0.5 * Math.pow(2 - a, this.sc))
    }, m: function () {
        var a = new F.oo;
        a.ga(this.v.m(), this.sc);
        return a
    }, reverse: function () {
        return new F.oo(this.v.reverse(), this.sc)
    }
});
F.oo.create = function (a, c) {
    return new F.oo(a, c)
};
F.p3 = function (a) {
    return {
        sc: a, fb: function (a) {
            a *= 2;
            return 1 > a ? 0.5 * Math.pow(a, this.sc) : 1 - 0.5 * Math.pow(2 - a, this.sc)
        }, reverse: function () {
            return F.p3(this.sc)
        }
    }
};
F.xv = F.eb.extend({
    update: function (a) {
        this.v.update(0 === a ? 0 : Math.pow(2, 10 * (a - 1)))
    }, reverse: function () {
        return new F.zv(this.v.reverse())
    }, m: function () {
        var a = new F.xv;
        a.ga(this.v.m());
        return a
    }
});
F.xv.create = function (a) {
    return new F.xv(a)
};
F.qM = {
    fb: function (a) {
        return 0 === a ? 0 : Math.pow(2, 10 * (a - 1))
    }, reverse: function () {
        return F.sM
    }
};
F.Oea = function () {
    return F.qM
};
F.zv = F.eb.extend({
    update: function (a) {
        this.v.update(1 == a ? 1 : -Math.pow(2, -10 * a) + 1)
    }, reverse: function () {
        return new F.xv(this.v.reverse())
    }, m: function () {
        var a = new F.zv;
        a.ga(this.v.m());
        return a
    }
});
F.zv.create = function (a) {
    return new F.zv(a)
};
F.sM = {
    fb: function (a) {
        return 1 == a ? 1 : -Math.pow(2, -10 * a) + 1
    }, reverse: function () {
        return F.qM
    }
};
F.Qea = function () {
    return F.sM
};
F.yv = F.eb.extend({
    update: function (a) {
        1 != a && 0 !== a && (a *= 2, a = 1 > a ? 0.5 * Math.pow(2, 10 * (a - 1)) : 0.5 * (-Math.pow(2, -10 * (a - 1)) + 2));
        this.v.update(a)
    }, reverse: function () {
        return new F.yv(this.v.reverse())
    }, m: function () {
        var a = new F.yv;
        a.ga(this.v.m());
        return a
    }
});
F.yv.create = function (a) {
    return new F.yv(a)
};
F.rM = {
    fb: function (a) {
        return 1 !== a && 0 !== a ? (a *= 2, 1 > a ? 0.5 * Math.pow(2, 10 * (a - 1)) : 0.5 * (-Math.pow(2, -10 * (a - 1)) + 2)) : a
    }, reverse: function () {
        return F.rM
    }
};
F.Pea = function () {
    return F.rM
};
F.Bv = F.eb.extend({
    update: function (a) {
        a = 0 === a || 1 === a ? a : -1 * Math.cos(a * Math.PI / 2) + 1;
        this.v.update(a)
    }, reverse: function () {
        return new F.Dv(this.v.reverse())
    }, m: function () {
        var a = new F.Bv;
        a.ga(this.v.m());
        return a
    }
});
F.Bv.create = function (a) {
    return new F.Bv(a)
};
F.CM = {
    fb: function (a) {
        return 0 === a || 1 === a ? a : -1 * Math.cos(a * Math.PI / 2) + 1
    }, reverse: function () {
        return F.EM
    }
};
F.$ea = function () {
    return F.CM
};
F.Dv = F.eb.extend({
    update: function (a) {
        a = 0 === a || 1 === a ? a : Math.sin(a * Math.PI / 2);
        this.v.update(a)
    }, reverse: function () {
        return new F.Bv(this.v.reverse())
    }, m: function () {
        var a = new F.Dv;
        a.ga(this.v.m());
        return a
    }
});
F.Dv.create = function (a) {
    return new F.Dv(a)
};
F.EM = {
    fb: function (a) {
        return 0 === a || 1 == a ? a : Math.sin(a * Math.PI / 2)
    }, reverse: function () {
        return F.CM
    }
};
F.bfa = function () {
    return F.EM
};
F.Cv = F.eb.extend({
    update: function (a) {
        a = 0 === a || 1 === a ? a : -0.5 * (Math.cos(Math.PI * a) - 1);
        this.v.update(a)
    }, m: function () {
        var a = new F.Cv;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Cv(this.v.reverse())
    }
});
F.Cv.create = function (a) {
    return new F.Cv(a)
};
F.DM = {
    fb: function (a) {
        return 0 === a || 1 === a ? a : -0.5 * (Math.cos(Math.PI * a) - 1)
    }, reverse: function () {
        return F.DM
    }
};
F.afa = function () {
    return F.DM
};
F.no = F.eb.extend({
    Sb: 0.3, ctor: function (a, c) {
        F.eb.prototype.ctor.call(this);
        a && this.ga(a, c)
    }, eha: A("Sb"), joa: z("Sb"), ga: function (a, c) {
        F.eb.prototype.ga.call(this, a);
        this.Sb = c == q ? 0.3 : c;
        return p
    }, reverse: function () {
        F.log("cc.EaseElastic.reverse(): it should be overridden in subclass.");
        return q
    }, m: function () {
        var a = new F.no;
        a.ga(this.v.m(), this.Sb);
        return a
    }
});
F.no.create = function (a, c) {
    return new F.no(a, c)
};
F.uv = F.no.extend({
    update: function (a) {
        var c = 0;
        0 === a || 1 === a ? c = a : (c = this.Sb / 4, a -= 1, c = -Math.pow(2, 10 * a) * Math.sin(2 * (a - c) * Math.PI / this.Sb));
        this.v.update(c)
    }, reverse: function () {
        return new F.wv(this.v.reverse(), this.Sb)
    }, m: function () {
        var a = new F.uv;
        a.ga(this.v.m(), this.Sb);
        return a
    }
});
F.uv.create = function (a, c) {
    return new F.uv(a, c)
};
F.oM = {
    fb: function (a) {
        if (0 === a || 1 === a)return a;
        a -= 1;
        return -Math.pow(2, 10 * a) * Math.sin(2 * (a - 0.075) * Math.PI / 0.3)
    }, reverse: function () {
        return F.pM
    }
};
F.l3 = function (a) {
    return a && 0.3 !== a ? {
        Sb: a, fb: function (a) {
            if (0 === a || 1 === a)return a;
            a -= 1;
            return -Math.pow(2, 10 * a) * Math.sin(2 * (a - this.Sb / 4) * Math.PI / this.Sb)
        }, reverse: function () {
            return F.n3(this.Sb)
        }
    } : F.oM
};
F.wv = F.no.extend({
    update: function (a) {
        var c = 0, c = 0 === a || 1 == a ? a : Math.pow(2, -10 * a) * Math.sin(2 * (a - this.Sb / 4) * Math.PI / this.Sb) + 1;
        this.v.update(c)
    }, reverse: function () {
        return new F.uv(this.v.reverse(), this.Sb)
    }, m: function () {
        var a = new F.wv;
        a.ga(this.v.m(), this.Sb);
        return a
    }
});
F.wv.create = function (a, c) {
    return new F.wv(a, c)
};
F.pM = {
    fb: function (a) {
        return 0 === a || 1 === a ? a : Math.pow(2, -10 * a) * Math.sin(2 * (a - 0.075) * Math.PI / 0.3) + 1
    }, reverse: function () {
        return F.oM
    }
};
F.n3 = function (a) {
    return a && 0.3 !== a ? {
        Sb: a, fb: function (a) {
            return 0 === a || 1 === a ? a : Math.pow(2, -10 * a) * Math.sin(2 * (a - this.Sb / 4) * Math.PI / this.Sb) + 1
        }, reverse: function () {
            return F.l3(this.Sb)
        }
    } : F.pM
};
F.vv = F.no.extend({
    update: function (a) {
        var c = 0, c = this.Sb;
        if (0 === a || 1 == a)c = a; else {
            c || (c = this.Sb = 0.3 * 1.5);
            var d = c / 4;
            a = 2 * a - 1;
            c = 0 > a ? -0.5 * Math.pow(2, 10 * a) * Math.sin(2 * (a - d) * Math.PI / c) : 0.5 * Math.pow(2, -10 * a) * Math.sin(2 * (a - d) * Math.PI / c) + 1
        }
        this.v.update(c)
    }, reverse: function () {
        return new F.vv(this.v.reverse(), this.Sb)
    }, m: function () {
        var a = new F.vv;
        a.ga(this.v.m(), this.Sb);
        return a
    }
});
F.vv.create = function (a, c) {
    return new F.vv(a, c)
};
F.m3 = function (a) {
    return {
        Sb: a || 0.3, fb: function (a) {
            var d = 0, d = this.Sb;
            if (0 === a || 1 === a)d = a; else {
                d || (d = this.Sb = 0.3 * 1.5);
                var e = d / 4;
                a = 2 * a - 1;
                d = 0 > a ? -0.5 * Math.pow(2, 10 * a) * Math.sin(2 * (a - e) * Math.PI / d) : 0.5 * Math.pow(2, -10 * a) * Math.sin(2 * (a - e) * Math.PI / d) + 1
            }
            return d
        }, reverse: function () {
            return F.m3(this.Sb)
        }
    }
};
F.km = F.eb.extend({
    Ey: function (a) {
        if (a < 1 / 2.75)return 7.5625 * a * a;
        if (a < 2 / 2.75)return a -= 1.5 / 2.75, 7.5625 * a * a + 0.75;
        if (a < 2.5 / 2.75)return a -= 2.25 / 2.75, 7.5625 * a * a + 0.9375;
        a -= 2.625 / 2.75;
        return 7.5625 * a * a + 0.984375
    }, m: function () {
        var a = new F.km;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.km(this.v.reverse())
    }
});
F.km.create = function (a) {
    return new F.km(a)
};
F.rv = F.km.extend({
    update: function (a) {
        this.v.update(1 - this.Ey(1 - a))
    }, reverse: function () {
        return new F.tv(this.v.reverse())
    }, m: function () {
        var a = new F.rv;
        a.ga(this.v.m());
        return a
    }
});
F.rv.create = function (a) {
    return new F.rv(a)
};
F.Hw = function (a) {
    if (a < 1 / 2.75)return 7.5625 * a * a;
    if (a < 2 / 2.75)return a -= 1.5 / 2.75, 7.5625 * a * a + 0.75;
    if (a < 2.5 / 2.75)return a -= 2.25 / 2.75, 7.5625 * a * a + 0.9375;
    a -= 2.625 / 2.75;
    return 7.5625 * a * a + 0.984375
};
F.fM = {
    fb: function (a) {
        return 1 - F.Hw(1 - a)
    }, reverse: function () {
        return F.hM
    }
};
F.Fea = function () {
    return F.fM
};
F.tv = F.km.extend({
    update: function (a) {
        this.v.update(this.Ey(a))
    }, reverse: function () {
        return new F.rv(this.v.reverse())
    }, m: function () {
        var a = new F.tv;
        a.ga(this.v.m());
        return a
    }
});
F.tv.create = function (a) {
    return new F.tv(a)
};
F.hM = {
    fb: function (a) {
        return F.Hw(a)
    }, reverse: function () {
        return F.fM
    }
};
F.Hea = function () {
    return F.hM
};
F.sv = F.km.extend({
    update: function (a) {
        var c = 0, c = 0.5 > a ? 0.5 * (1 - this.Ey(1 - 2 * a)) : 0.5 * this.Ey(2 * a - 1) + 0.5;
        this.v.update(c)
    }, m: function () {
        var a = new F.sv;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.sv(this.v.reverse())
    }
});
F.sv.create = function (a) {
    return new F.sv(a)
};
F.gM = {
    fb: function (a) {
        return a = 0.5 > a ? 0.5 * (1 - F.Hw(1 - 2 * a)) : 0.5 * F.Hw(2 * a - 1) + 0.5
    }, reverse: function () {
        return F.gM
    }
};
F.Gea = function () {
    return F.gM
};
F.ov = F.eb.extend({
    update: function (a) {
        this.v.update(0 === a || 1 == a ? a : a * a * (2.70158 * a - 1.70158))
    }, reverse: function () {
        return new F.qv(this.v.reverse())
    }, m: function () {
        var a = new F.ov;
        a.ga(this.v.m());
        return a
    }
});
F.ov.create = function (a) {
    return new F.ov(a)
};
F.cM = {
    fb: function (a) {
        return 0 === a || 1 === a ? a : a * a * (2.70158 * a - 1.70158)
    }, reverse: function () {
        return F.eM
    }
};
F.Cea = function () {
    return F.cM
};
F.qv = F.eb.extend({
    update: function (a) {
        a -= 1;
        this.v.update(a * a * (2.70158 * a + 1.70158) + 1)
    }, reverse: function () {
        return new F.ov(this.v.reverse())
    }, m: function () {
        var a = new F.qv;
        a.ga(this.v.m());
        return a
    }
});
F.qv.create = function (a) {
    return new F.qv(a)
};
F.eM = {
    fb: function (a) {
        a -= 1;
        return a * a * (2.70158 * a + 1.70158) + 1
    }, reverse: function () {
        return F.cM
    }
};
F.Eea = function () {
    return F.eM
};
F.pv = F.eb.extend({
    update: function (a) {
        a *= 2;
        1 > a ? this.v.update(a * a * (3.5949095 * a - 2.5949095) / 2) : (a -= 2, this.v.update(a * a * (3.5949095 * a + 2.5949095) / 2 + 1))
    }, m: function () {
        var a = new F.pv;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.pv(this.v.reverse())
    }
});
F.pv.create = function (a) {
    return new F.pv(a)
};
F.dM = {
    fb: function (a) {
        a *= 2;
        if (1 > a)return a * a * (3.5949095 * a - 2.5949095) / 2;
        a -= 2;
        return a * a * (3.5949095 * a + 2.5949095) / 2 + 1
    }, reverse: function () {
        return F.dM
    }
};
F.Dea = function () {
    return F.dM
};
F.ur = F.eb.extend({
    Ix: q, Jx: q, Kx: q, Lx: q, ctor: function (a) {
        F.eb.prototype.ctor.call(this, a)
    }, ya: function (a, c, d, e, f) {
        return Math.pow(1 - f, 3) * a + 3 * f * Math.pow(1 - f, 2) * c + 3 * Math.pow(f, 2) * (1 - f) * d + Math.pow(f, 3) * e
    }, update: function (a) {
        this.v.update(this.ya(this.Ix, this.Jx, this.Kx, this.Lx, a))
    }, m: function () {
        var a = new F.ur;
        a.ga(this.v.m());
        a.dS(this.Ix, this.Jx, this.Kx, this.Lx);
        return a
    }, reverse: function () {
        var a = new F.ur(this.v.reverse());
        a.dS(this.Lx, this.Kx, this.Jx, this.Ix);
        return a
    }, dS: function (a, c, d, e) {
        this.Ix = a ||
            0;
        this.Jx = c || 0;
        this.Kx = d || 0;
        this.Lx = e || 0
    }
});
F.ur.create = function (a) {
    return new F.ur(a)
};
F.k3 = function (a, c, d, e) {
    return {
        fb: function (f) {
            return F.ur.prototype.ya(a, c, d, e, f)
        }, reverse: function () {
            return F.k3(e, d, c, a)
        }
    }
};
F.Br = F.eb.extend({
    ya: function (a) {
        return Math.pow(a, 2)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Br;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Br(this.v.reverse())
    }
});
F.Br.create = function (a) {
    return new F.Br(a)
};
F.tM = {
    fb: F.Br.prototype.ya, reverse: function () {
        return F.tM
    }
};
F.Rea = function () {
    return F.tM
};
F.Dr = F.eb.extend({
    ya: function (a) {
        return -a * (a - 2)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Dr;
        a.ga();
        return a
    }, reverse: function () {
        return new F.Dr(this.v.reverse())
    }
});
F.Dr.create = function (a) {
    return new F.Dr(a)
};
F.vM = {
    fb: F.Dr.prototype.ya, reverse: function () {
        return F.vM
    }
};
F.Tea = function () {
    return F.vM
};
F.Cr = F.eb.extend({
    ya: function (a) {
        var c = a;
        a *= 2;
        1 > a ? c = 0.5 * a * a : (--a, c = -0.5 * (a * (a - 2) - 1));
        return c
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Cr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Cr(this.v.reverse())
    }
});
F.Cr.create = function (a) {
    return new F.Cr(a)
};
F.uM = {
    fb: F.Cr.prototype.ya, reverse: function () {
        return F.uM
    }
};
F.Sea = function () {
    return F.uM
};
F.Er = F.eb.extend({
    ya: function (a) {
        return a * a * a * a
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Er;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Er(this.v.reverse())
    }
});
F.Er.create = function (a) {
    return new F.Er(a)
};
F.wM = {
    fb: F.Er.prototype.ya, reverse: function () {
        return F.wM
    }
};
F.Uea = function () {
    return F.wM
};
F.Gr = F.eb.extend({
    ya: function (a) {
        a -= 1;
        return -(a * a * a * a - 1)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Gr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Gr(this.v.reverse())
    }
});
F.Gr.create = function (a) {
    return new F.Gr(a)
};
F.yM = {
    fb: F.Gr.prototype.ya, reverse: function () {
        return F.yM
    }
};
F.Wea = function () {
    return F.yM
};
F.Fr = F.eb.extend({
    ya: function (a) {
        a *= 2;
        if (1 > a)return 0.5 * a * a * a * a;
        a -= 2;
        return -0.5 * (a * a * a * a - 2)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Fr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Fr(this.v.reverse())
    }
});
F.Fr.create = function (a) {
    return new F.Fr(a)
};
F.xM = {
    fb: F.Fr.prototype.ya, reverse: function () {
        return F.xM
    }
};
F.Vea = function () {
    return F.xM
};
F.Hr = F.eb.extend({
    ya: function (a) {
        return a * a * a * a * a
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Hr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Hr(this.v.reverse())
    }
});
F.Hr.create = function (a) {
    return new F.Hr(a)
};
F.zM = {
    fb: F.Hr.prototype.ya, reverse: function () {
        return F.zM
    }
};
F.Xea = function () {
    return F.zM
};
F.Jr = F.eb.extend({
    ya: function (a) {
        a -= 1;
        return a * a * a * a * a + 1
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Jr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Jr(this.v.reverse())
    }
});
F.Jr.create = function (a) {
    return new F.Jr(a)
};
F.BM = {
    fb: F.Jr.prototype.ya, reverse: function () {
        return F.BM
    }
};
F.Zea = function () {
    return F.BM
};
F.Ir = F.eb.extend({
    ya: function (a) {
        a *= 2;
        if (1 > a)return 0.5 * a * a * a * a * a;
        a -= 2;
        return 0.5 * (a * a * a * a * a + 2)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Ir;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Ir(this.v.reverse())
    }
});
F.Ir.create = function (a) {
    return new F.Ir(a)
};
F.AM = {
    fb: F.Ir.prototype.ya, reverse: function () {
        return F.AM
    }
};
F.Yea = function () {
    return F.AM
};
F.vr = F.eb.extend({
    ya: function (a) {
        return -1 * (Math.sqrt(1 - a * a) - 1)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.vr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.vr(this.v.reverse())
    }
});
F.vr.create = function (a) {
    return new F.vr(a)
};
F.iM = {
    fb: F.vr.prototype.ya, reverse: function () {
        return F.iM
    }
};
F.Iea = function () {
    return F.iM
};
F.xr = F.eb.extend({
    ya: function (a) {
        a -= 1;
        return Math.sqrt(1 - a * a)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.xr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.xr(this.v.reverse())
    }
});
F.xr.create = function (a) {
    return new F.xr(a)
};
F.kM = {
    fb: F.xr.prototype.ya, reverse: function () {
        return F.kM
    }
};
F.Kea = function () {
    return F.kM
};
F.wr = F.eb.extend({
    ya: function (a) {
        a *= 2;
        if (1 > a)return -0.5 * (Math.sqrt(1 - a * a) - 1);
        a -= 2;
        return 0.5 * (Math.sqrt(1 - a * a) + 1)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.wr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.wr(this.v.reverse())
    }
});
F.wr.create = function (a) {
    return new F.wr(a)
};
F.jM = {
    fb: F.wr.prototype.ya, reverse: function () {
        return F.jM
    }
};
F.Jea = function () {
    return F.jM
};
F.yr = F.eb.extend({
    ya: function (a) {
        return a * a * a
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.yr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.yr(this.v.reverse())
    }
});
F.yr.create = function (a) {
    return new F.yr(a)
};
F.lM = {
    fb: F.yr.prototype.ya, reverse: function () {
        return F.lM
    }
};
F.Lea = function () {
    return F.lM
};
F.Ar = F.eb.extend({
    ya: function (a) {
        a -= 1;
        return a * a * a + 1
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.Ar;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.Ar(this.v.reverse())
    }
});
F.Ar.create = function (a) {
    return new F.Ar(a)
};
F.nM = {
    fb: F.Ar.prototype.ya, reverse: function () {
        return F.nM
    }
};
F.Nea = function () {
    return F.nM
};
F.zr = F.eb.extend({
    ya: function (a) {
        a *= 2;
        if (1 > a)return 0.5 * a * a * a;
        a -= 2;
        return 0.5 * (a * a * a + 2)
    }, update: function (a) {
        this.v.update(this.ya(a))
    }, m: function () {
        var a = new F.zr;
        a.ga(this.v.m());
        return a
    }, reverse: function () {
        return new F.zr(this.v.reverse())
    }
});
F.zr.create = function (a) {
    return new F.zr(a)
};
F.mM = {
    fb: F.zr.prototype.ya, reverse: function () {
        return F.mM
    }
};
F.Mea = function () {
    return F.mM
};
F.VF = function (a, c, d, e, f, g) {
    var h = g * g, m = h * g, n = (1 - f) / 2;
    f = n * (-m + 2 * h - g);
    var s = n * (-m + h) + (2 * m - 3 * h + 1);
    g = n * (m - 2 * h + g) + (-2 * m + 3 * h);
    h = n * (m - h);
    return F.d(a.x * f + c.x * s + d.x * g + e.x * h, a.y * f + c.y * s + d.y * g + e.y * h)
};
F.UR = function (a) {
    for (var c = [], d = a.length - 1; 0 <= d; d--)c.push(F.d(a[d].x, a[d].y));
    return c
};
F.N2 = function (a) {
    for (var c = [], d = 0; d < a.length; d++)c.push(F.d(a[d].x, a[d].y));
    return c
};
F.Ky = F.N2;
F.Bd = function (a, c) {
    var d = Math.min(a.length - 1, Math.max(c, 0));
    return a[d]
};
F.Jma = function (a) {
    for (var c = a.length, d = 0 | c / 2, e = 0; e < d; ++e) {
        var f = a[e];
        a[e] = a[c - e - 1];
        a[c - e - 1] = f
    }
};
F.tj = F.C.extend({
    Oh: q, WL: 0, Kp: 0, ld: q, pL: q, ctor: function (a, c, d) {
        F.C.prototype.ctor.call(this);
        this.Oh = [];
        d !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        (!c || 0 == c.length) && b("Invalid configuration. It must at least have one control point");
        return F.C.prototype.f.call(this, a) ? (this.z8(c), this.Kp = d, p) : r
    }, m: function () {
        var a = new F.tj;
        a.f(this.t, F.Ky(this.Oh), this.Kp);
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.WL = 1 / (this.Oh.length - 1);
        this.ld = F.d(this.target.$b(), this.target.va());
        this.pL = F.d(0, 0)
    },
    update: function (a) {
        a = this.Kd(a);
        var c, d = this.Oh;
        if (1 == a)c = d.length - 1, a = 1; else {
            var e = this.WL;
            c = 0 | a / e;
            a = (a - e * c) / e
        }
        c = F.VF(F.Bd(d, c - 1), F.Bd(d, c - 0), F.Bd(d, c + 1), F.Bd(d, c + 2), this.Kp, a);
        if (F.lv && (d = this.target.$b() - this.ld.x, a = this.target.va() - this.ld.y, 0 != d || 0 != a))e = this.pL, d = e.x + d, a = e.y + a, e.x = d, e.y = a, c.x += d, c.y += a;
        this.cT(c)
    }, reverse: function () {
        var a = F.UR(this.Oh);
        return F.oP(this.t, a, this.Kp)
    }, cT: function (a) {
        this.target.X(a);
        this.ld = a
    }, fha: A("Oh"), z8: z("Oh")
});
F.oP = function (a, c, d) {
    return new F.tj(a, c, d)
};
F.tj.create = F.oP;
F.sr = F.tj.extend({
    Zc: q, ctor: function (a, c, d) {
        F.tj.prototype.ctor.call(this);
        this.Zc = F.d(0, 0);
        d !== k && this.f(a, c, d)
    }, D: function (a) {
        F.tj.prototype.D.call(this, a);
        this.Zc.x = a.$b();
        this.Zc.y = a.va()
    }, reverse: function () {
        for (var a = this.Oh.slice(), c, d = a[0], e = 1; e < a.length; ++e)c = a[e], a[e] = F.ie(c, d), d = c;
        a = F.UR(a);
        d = a[a.length - 1];
        a.pop();
        d.x = -d.x;
        d.y = -d.y;
        a.unshift(d);
        for (e = 1; e < a.length; ++e)c = a[e], c.x = -c.x, c.y = -c.y, c.x += d.x, c.y += d.y, d = a[e] = c;
        return F.nP(this.t, a, this.Kp)
    }, cT: function (a) {
        var c = this.Zc, d = a.x + c.x;
        a = a.y + c.y;
        this.ld.x = d;
        this.ld.y = a;
        this.target.X(d, a)
    }, m: function () {
        var a = new F.sr;
        a.f(this.t, F.Ky(this.Oh), this.Kp);
        return a
    }
});
F.nP = function (a, c, d) {
    return new F.sr(a, c, d)
};
F.sr.create = F.nP;
F.OA = F.tj.extend({
    ctor: function (a, c) {
        c && this.f(a, c)
    }, f: function (a, c) {
        return F.tj.prototype.f.call(this, a, c, 0.5)
    }, m: function () {
        var a = new F.OA;
        a.f(this.t, F.Ky(this.Oh));
        return a
    }
});
F.H2 = function (a, c) {
    return new F.OA(a, c)
};
F.OA.create = F.H2;
F.NA = F.sr.extend({
    ctor: function (a, c) {
        F.sr.prototype.ctor.call(this);
        c && this.f(a, c)
    }, f: function (a, c) {
        return F.tj.prototype.f.call(this, a, c, 0.5)
    }, m: function () {
        var a = new F.NA;
        a.f(this.t, F.Ky(this.Oh));
        return a
    }
});
F.G2 = function (a, c) {
    return new F.NA(a, c)
};
F.NA.create = F.G2;
F.v$ = F.za.extend({V9: u()});
F.gv = F.C.extend({
    key: "", Oy: 0, yA: 0, Nt: 0, ctor: function (a, c, d, e) {
        F.C.prototype.ctor.call(this);
        this.key = "";
        e !== k && this.f(a, c, d, e)
    }, f: function (a, c, d, e) {
        return F.C.prototype.f.call(this, a) ? (this.key = c, this.yA = e, this.Oy = d, p) : r
    }, D: function (a) {
        (!a || !a.V9) && b("cc.ActionTween.startWithTarget(): target must be non-null, and target must implement updateTweenAction function");
        F.C.prototype.D.call(this, a);
        this.Nt = this.yA - this.Oy
    }, update: u(), reverse: function () {
        return new F.gv(this.duration, this.key, this.yA, this.Oy)
    },
    m: function () {
        var a = new F.gv;
        a.f(this.t, this.key, this.Oy, this.yA);
        return a
    }
});
F.T1 = function (a, c, d, e) {
    return new F.gv(a, c, d, e)
};
F.gv.create = F.T1;
F.Bc = F.C.extend({
    da: q, ctor: function (a, c) {
        F.RC();
        F.C.prototype.ctor.call(this);
        this.da = F.size(0, 0);
        c && this.f(a, c)
    }, m: function () {
        var a = new F.Bc, c = this.da;
        a.f(this.t, F.size(c.width, c.height));
        return a
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        F.ka.xe = p;
        var c = this.St(), d = this.target;
        (a = d.grid) && 0 < a.Si ? (c = a.j4(), a.mk() && (c.width == this.da.width && c.height == this.da.height) && a.FH()) : (a && a.mk() && a.setActive(r), d.grid = c, d.grid.setActive(p))
    }, reverse: function () {
        return new F.Rr(this)
    }, f: function (a, c) {
        return F.C.prototype.f.call(this,
            a) ? (this.da.width = c.width, this.da.height = c.height, p) : r
    }, St: function () {
        F.log("cc.GridAction.getGrid(): it should be overridden in subclass.")
    }
});
F.l5 = function (a, c) {
    return new F.Bc(a, c)
};
F.Bc.create = F.l5;
F.rd = F.Bc.extend({
    St: function () {
        return new F.cB(this.da)
    }, gT: function (a) {
        return this.target.grid.gT(a)
    }, dd: function (a) {
        return this.target.grid.dd(a)
    }, je: function (a, c) {
        this.target.grid.je(a, c)
    }
});
F.k5 = function (a, c) {
    return new F.rd(a, c)
};
F.rd.create = F.k5;
F.Oc = F.Bc.extend({
    RS: function (a) {
        return this.target.grid.RS(a)
    }, rg: function (a) {
        return this.target.grid.rg(a)
    }, pf: function (a, c) {
        this.target.grid.pf(a, c)
    }, St: function () {
        return new F.iC(this.da)
    }
});
F.I9 = function (a, c) {
    return new F.Oc(a, c)
};
F.Oc.create = F.I9;
F.fK = F.Ve.extend({
    D: function (a) {
        F.Ve.prototype.D.call(this, a);
        F.ka.xe = p;
        (a = this.target.grid) && a.mk() && a.setActive(r)
    }
});
F.hr = function () {
    return new F.fK
};
F.fK.create = F.hr;
F.QJ = F.Ve.extend({
    uc: q, ctor: function (a) {
        F.Ve.prototype.ctor.call(this);
        a !== k && this.B5(a)
    }, B5: function (a) {
        this.uc = a;
        return p
    }, D: function (a) {
        F.Ve.prototype.D.call(this, a);
        F.ka.xe = p;
        this.target.grid && this.target.grid.mk() && this.target.grid.E8(this.target.grid.Si + this.uc)
    }
});
F.N7 = function (a) {
    return new F.QJ(a)
};
F.QJ.create = F.N7;
F.eL = F.rd.extend({
    Hf: 0, ob: 0, pb: 0, ctor: function (a, c, d, e) {
        F.Bc.prototype.ctor.call(this);
        e !== k && this.f(a, c, d, e)
    }, vq: A("ob"), Zq: z("ob"), Nn: A("pb"), ao: z("pb"), f: function (a, c, d, e) {
        return F.rd.prototype.f.call(this, a, c) ? (this.Hf = d, this.ob = e, this.pb = 1, p) : r
    }, update: function (a) {
        for (var c = this.da, d = this.ob, e = F.d(0, 0), f = this.pb, g = this.Hf, h = 0; h < c.width + 1; ++h)for (var m = 0; m < c.height + 1; ++m) {
            e.x = h;
            e.y = m;
            var n = this.dd(e);
            n.e += Math.sin(2 * Math.PI * a * g + 0.01 * (n.y + n.x)) * d * f;
            this.je(e, n)
        }
    }
});
F.b$ = function (a, c, d, e) {
    return new F.eL(a, c, d, e)
};
F.eL.create = F.b$;
F.aB = F.rd.extend({
    ctor: function (a) {
        a !== k ? F.Bc.prototype.ctor.call(this, a, F.size(1, 1)) : F.Bc.prototype.ctor.call(this)
    }, f: function (a) {
        return F.rd.prototype.f.call(this, a, F.size(1, 1))
    }, nz: function (a, c) {
        return 1 != a.width || 1 != a.height ? (F.log("Grid size must be (1,1)"), r) : F.rd.prototype.f.call(this, c, a)
    }, update: function (a) {
        var c = Math.PI * a;
        a = Math.sin(c);
        var d = Math.cos(c / 2), c = new U, e = F.d(0, 0);
        e.x = e.y = 1;
        var f = this.dd(e);
        e.x = e.y = 0;
        var e = this.dd(e), g = f.x, h = e.x, m, n;
        g > h ? (f = F.d(0, 0), e = F.d(0, 1), m = F.d(1, 0), n = F.d(1,
            1)) : (m = F.d(0, 0), n = F.d(0, 1), f = F.d(1, 0), e = F.d(1, 1), g = h);
        c.x = g - g * d;
        c.e = Math.abs(parseFloat(g * a / 4));
        a = this.dd(f);
        a.x = c.x;
        a.e += c.e;
        this.je(f, a);
        a = this.dd(e);
        a.x = c.x;
        a.e += c.e;
        this.je(e, a);
        a = this.dd(m);
        a.x -= c.x;
        a.e -= c.e;
        this.je(m, a);
        a = this.dd(n);
        a.x -= c.x;
        a.e -= c.e;
        this.je(n, a)
    }
});
F.H3 = function (a) {
    return new F.aB(a)
};
F.aB.create = F.H3;
F.VI = F.aB.extend({
    ctor: function (a) {
        a !== k ? F.Bc.prototype.ctor.call(this, a, F.size(1, 1)) : F.Bc.prototype.ctor.call(this)
    }, update: function (a) {
        var c = Math.PI * a;
        a = Math.sin(c);
        var d = Math.cos(c / 2), c = new U, e = F.d(0, 0);
        e.x = e.y = 1;
        var f = this.dd(e);
        e.x = e.y = 0;
        var e = this.dd(e), g = f.y, h = e.y, m, n;
        g > h ? (f = F.d(0, 0), e = F.d(0, 1), m = F.d(1, 0), n = F.d(1, 1)) : (e = F.d(0, 0), f = F.d(0, 1), n = F.d(1, 0), m = F.d(1, 1), g = h);
        c.y = g - g * d;
        c.e = Math.abs(parseFloat(g * a) / 4);
        a = this.dd(f);
        a.y = c.y;
        a.e += c.e;
        this.je(f, a);
        a = this.dd(e);
        a.y -= c.y;
        a.e -= c.e;
        this.je(e,
            a);
        a = this.dd(m);
        a.y = c.y;
        a.e += c.e;
        this.je(m, a);
        a = this.dd(n);
        a.y -= c.y;
        a.e -= c.e;
        this.je(n, a)
    }
});
F.J3 = function (a) {
    return new F.VI(a)
};
F.VI.create = F.J3;
F.bJ = F.rd.extend({
    xa: q, Cf: 0, zx: 0, YY: r, sb: r, ctor: function (a, c, d, e) {
        F.Bc.prototype.ctor.call(this);
        this.xa = F.d(0, 0);
        e !== k && this.f(a, c, d, e)
    }, Hga: A("zx"), Qna: z("zx"), nna: z("YY"), Uy: A("xa"), X: function (a) {
        F.Tz(a, this.xa) || (this.xa.x = a.x, this.xa.y = a.y, this.sb = p)
    }, f: function (a, c, d, e) {
        return F.rd.prototype.f.call(this, a, c) ? (this.X(d), this.Cf = e, this.zx = 0.7, this.sb = p) : r
    }, update: function () {
        if (this.sb) {
            for (var a = this.da.width, c = this.da.height, d = this.Cf, e = this.zx, f = F.d(0, 0), g = F.d(0, 0), h, m, n, s = 0; s < a + 1; ++s)for (var t =
                0; t < c + 1; ++t)f.x = s, f.y = t, h = this.dd(f), g.x = this.xa.x - h.x, g.y = this.xa.y - h.y, m = F.Ul(g), m < d && (m = d - m, m /= d, 0 == m && (m = 0.0010), m = Math.log(m) * e, n = Math.exp(m) * d, m = F.Ul(g), 0 < m && (g.x /= m, g.y /= m, g.x *= n, g.y *= n, h.e += F.Ul(g) * e)), this.je(f, h);
            this.sb = r
        }
    }
});
F.x6 = function (a, c, d, e) {
    return new F.bJ(a, c, d, e)
};
F.bJ.create = F.x6;
F.RJ = F.rd.extend({
    xa: q, Cf: 0, Hf: 0, ob: 0, pb: 0, ctor: function (a, c, d, e, f, g) {
        F.Bc.prototype.ctor.call(this);
        this.xa = F.d(0, 0);
        g !== k && this.f(a, c, d, e, f, g)
    }, Uy: A("xa"), X: function (a) {
        this.xa.x = a.x;
        this.xa.y = a.y
    }, vq: A("ob"), Zq: z("ob"), Nn: A("pb"), ao: z("pb"), f: function (a, c, d, e, f, g) {
        return F.rd.prototype.f.call(this, a, c) ? (this.X(d), this.Cf = e, this.Hf = f, this.ob = g, this.pb = 1, p) : r
    }, update: function (a) {
        for (var c = this.da.width, d = this.da.height, e = F.d(0, 0), f = this.Cf, g = this.Hf, h = this.ob, m = this.pb, n, s, t = F.d(0, 0), v = 0; v < c + 1; ++v)for (var w =
            0; w < d + 1; ++w)e.x = v, e.y = w, n = this.dd(e), t.x = this.xa.x - n.x, t.y = this.xa.y - n.y, s = F.Ul(t), s < f && (s = f - s, n.e += Math.sin(2 * a * Math.PI * g + 0.1 * s) * h * m * Math.pow(s / f, 2)), this.je(e, n)
    }
});
F.O7 = function (a, c, d, e, f, g) {
    return new F.RJ(a, c, d, e, f, g)
};
F.RJ.create = F.O7;
F.XJ = F.rd.extend({
    ol: 0, mt: r, ctor: function (a, c, d, e) {
        F.Bc.prototype.ctor.call(this);
        e !== k && this.f(a, c, d, e)
    }, f: function (a, c, d, e) {
        return F.rd.prototype.f.call(this, a, c) ? (this.ol = d, this.mt = e, p) : r
    }, update: function () {
        for (var a = this.da.width, c = this.da.height, d = this.ol, e = this.mt, f = F.d(0, 0), g, h = 0; h < a + 1; ++h)for (var m = 0; m < c + 1; ++m)f.x = h, f.y = m, g = this.dd(f), g.x += F.Rb() % (2 * d) - d, g.y += F.Rb() % (2 * d) - d, e && (g.e += F.Rb() % (2 * d) - d), this.je(f, g)
    }
});
F.Z8 = function (a, c, d, e) {
    return new F.XJ(a, c, d, e)
};
F.XJ.create = F.Z8;
F.cJ = F.rd.extend({
    Hf: 0, ob: 0, pb: 0, ctor: function (a, c, d, e) {
        F.Bc.prototype.ctor.call(this);
        e !== k && this.f(a, c, d, e)
    }, vq: A("ob"), Zq: z("ob"), Nn: A("pb"), ao: z("pb"), f: function (a, c, d, e) {
        return F.rd.prototype.f.call(this, a, c) ? (this.Hf = d, this.ob = e, this.pb = 1, p) : r
    }, update: function (a) {
        for (var c = this.da.width, d = this.da.height, e = F.d(0, 0), f = this.Hf, g = this.ob, h = this.pb, m, n = 1; n < c; ++n)for (var s = 1; s < d; ++s)e.x = n, e.y = s, m = this.dd(e), m.x += Math.sin(2 * a * Math.PI * f + 0.01 * m.x) * g * h, m.y += Math.sin(2 * a * Math.PI * f + 0.01 * m.y) * g * h, this.je(e,
            m)
    }
});
F.y6 = function (a, c, d, e) {
    return new F.cJ(a, c, d, e)
};
F.cJ.create = F.y6;
F.dL = F.rd.extend({
    Hf: 0, ob: 0, pb: 0, bP: r, iN: r, ctor: function (a, c, d, e, f, g) {
        F.Bc.prototype.ctor.call(this);
        g !== k && this.f(a, c, d, e, f, g)
    }, vq: A("ob"), Zq: z("ob"), Nn: A("pb"), ao: z("pb"), f: function (a, c, d, e, f, g) {
        return F.rd.prototype.f.call(this, a, c) ? (this.Hf = d, this.ob = e, this.pb = 1, this.iN = f, this.bP = g, p) : r
    }, update: function (a) {
        for (var c = this.da.width, d = this.da.height, e = F.d(0, 0), f = this.bP, g = this.iN, h = this.Hf, m = this.ob, n = this.pb, s, t = 0; t < c + 1; ++t)for (var v = 0; v < d + 1; ++v)e.x = t, e.y = v, s = this.dd(e), f && (s.x += Math.sin(2 * a * Math.PI *
                h + 0.01 * s.y) * m * n), g && (s.y += Math.sin(2 * a * Math.PI * h + 0.01 * s.x) * m * n), this.je(e, s)
    }
});
F.a$ = function (a, c, d, e, f, g) {
    return new F.dL(a, c, d, e, f, g)
};
F.dL.create = F.a$;
F.YK = F.rd.extend({
    xa: q, RO: 0, ob: 0, pb: 0, ctor: function (a, c, d, e, f) {
        F.Bc.prototype.ctor.call(this);
        this.xa = F.d(0, 0);
        f !== k && this.f(a, c, d, e, f)
    }, Uy: A("xa"), X: function (a) {
        this.xa.x = a.x;
        this.xa.y = a.y
    }, vq: A("ob"), Zq: z("ob"), Nn: A("pb"), ao: z("pb"), f: function (a, c, d, e, f) {
        return F.rd.prototype.f.call(this, a, c) ? (this.X(d), this.RO = e, this.ob = f, this.pb = 1, p) : r
    }, update: function (a) {
        for (var c = this.xa, d = this.da.width, e = this.da.height, f = F.d(0, 0), g = 0.1 * this.ob * this.pb, h = this.RO, m, n, s, t = F.d(0, 0), v = 0; v < d + 1; ++v)for (var w = 0; w <
        e + 1; ++w)f.x = v, f.y = w, m = this.dd(f), t.x = v - d / 2, t.y = w - e / 2, n = F.Ul(t) * Math.cos(Math.PI / 2 + 2 * a * Math.PI * h) * g, s = Math.sin(n) * (m.y - c.y) + Math.cos(n) * (m.x - c.x), n = Math.cos(n) * (m.y - c.y) - Math.sin(n) * (m.x - c.x), m.x = c.x + s, m.y = c.y + n, this.je(f, m)
    }
});
F.O9 = function (a, c, d, e, f) {
    return new F.YK(a, c, d, e, f)
};
F.YK.create = F.O9;
F.YJ = F.Oc.extend({
    ol: 0, mt: r, ctor: function (a, c, d, e) {
        F.Bc.prototype.ctor.call(this);
        e !== k && this.f(a, c, d, e)
    }, f: function (a, c, d, e) {
        return F.Oc.prototype.f.call(this, a, c) ? (this.ol = d, this.mt = e, p) : r
    }, update: function () {
        for (var a = this.da, c = this.ol, d = F.d(0, 0), e = 0; e < a.width; ++e)for (var f = 0; f < a.height; ++f) {
            d.x = e;
            d.y = f;
            var g = this.rg(d);
            g.K.x += F.Rb() % (2 * c) - c;
            g.V.x += F.Rb() % (2 * c) - c;
            g.U.x += F.Rb() % (2 * c) - c;
            g.O.x += F.Rb() % (2 * c) - c;
            g.K.y += F.Rb() % (2 * c) - c;
            g.V.y += F.Rb() % (2 * c) - c;
            g.U.y += F.Rb() % (2 * c) - c;
            g.O.y += F.Rb() % (2 * c) - c;
            this.mt &&
            (g.K.e += F.Rb() % (2 * c) - c, g.V.e += F.Rb() % (2 * c) - c, g.U.e += F.Rb() % (2 * c) - c, g.O.e += F.Rb() % (2 * c) - c);
            this.pf(d, g)
        }
    }
});
F.a9 = function (a, c, d, e) {
    return new F.YJ(a, c, d, e)
};
F.YJ.create = F.a9;
F.$J = F.Oc.extend({
    ol: 0, VD: r, CO: r, ctor: function (a, c, d, e) {
        F.Bc.prototype.ctor.call(this);
        e !== k && this.f(a, c, d, e)
    }, f: function (a, c, d, e) {
        return F.Oc.prototype.f.call(this, a, c) ? (this.VD = r, this.ol = d, this.CO = e, p) : r
    }, update: function () {
        if (this.VD === r) {
            for (var a = this.da, c = this.ol, d, e = F.d(0, 0), f = 0; f < a.width; ++f)for (var g = 0; g < a.height; ++g)e.x = f, e.y = g, d = this.rg(e), d.K.x += F.Rb() % (2 * c) - c, d.V.x += F.Rb() % (2 * c) - c, d.U.x += F.Rb() % (2 * c) - c, d.O.x += F.Rb() % (2 * c) - c, d.K.y += F.Rb() % (2 * c) - c, d.V.y += F.Rb() % (2 * c) - c, d.U.y += F.Rb() % (2 *
                c) - c, d.O.y += F.Rb() % (2 * c) - c, this.CO && (d.K.e += F.Rb() % (2 * c) - c, d.V.e += F.Rb() % (2 * c) - c, d.U.e += F.Rb() % (2 * c) - c, d.O.e += F.Rb() % (2 * c) - c), this.pf(e, d);
            this.VD = p
        }
    }
});
F.b9 = function (a, c, d, e) {
    return new F.$J(a, c, d, e)
};
F.$J.create = F.b9;
function la() {
    this.position = F.d(0, 0);
    this.s9 = F.d(0, 0);
    this.Nt = F.d(0, 0)
}
F.aK = F.Oc.extend({
    GE: 0, bk: 0, $i: q, df: q, ctor: function (a, c, d) {
        F.Bc.prototype.ctor.call(this);
        this.$i = [];
        this.df = [];
        d !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        return F.Oc.prototype.f.call(this, a, c) ? (this.GE = d, this.$i.length = 0, this.df.length = 0, p) : r
    }, dI: function (a, c) {
        for (var d = c - 1; 0 <= d; d--) {
            var e = 0 | F.Rb() % (d + 1), f = a[d];
            a[d] = a[e];
            a[e] = f
        }
    }, xG: function (a) {
        var c = this.da, d = a.width * c.height + a.height;
        return F.size(this.$i[d] / c.height - a.width, this.$i[d] % c.height - a.height)
    }, d7: function (a, c) {
        var d = this.rg(a), e = this.target.grid.IG(),
            f = c.position;
        d.K.x += f.x * e.x;
        d.K.y += f.y * e.y;
        d.V.x += f.x * e.x;
        d.V.y += f.y * e.y;
        d.U.x += f.x * e.x;
        d.U.y += f.y * e.y;
        d.O.x += f.x * e.x;
        d.O.y += f.y * e.y;
        this.pf(a, d)
    }, D: function (a) {
        F.Oc.prototype.D.call(this, a);
        a = this.da;
        this.bk = a.width * a.height;
        for (var c = this.$i, d = c.length = 0; d < this.bk; ++d)c[d] = d;
        this.dI(c, this.bk);
        for (var c = this.df, d = c.length = 0, e = F.size(0, 0), f = 0; f < a.width; ++f)for (var g = 0; g < a.height; ++g)c[d] = new la, c[d].position = F.d(f, g), c[d].s9 = F.d(f, g), e.width = f, e.height = g, c[d].Nt = this.xG(e), ++d
    }, update: function (a) {
        for (var c =
            0, d = this.da, e = this.df, f, g = F.d(0, 0), h = 0; h < d.width; ++h)for (var m = 0; m < d.height; ++m)g.x = h, g.y = m, f = e[c], f.position.x = f.Nt.width * a, f.position.y = f.Nt.height * a, this.d7(g, f), ++c
    }
});
F.f9 = function (a, c, d) {
    return new F.aK(a, c, d)
};
F.aK.create = F.f9;
F.Hv = F.Oc.extend({
    xA: function (a, c) {
        var d = this.da.width * c, e = this.da.height * c;
        return 0 == d + e ? 1 : Math.pow((a.width + a.height) / (d + e), 6)
    }, lI: function (a) {
        this.pf(a, this.rg(a))
    }, kI: function (a) {
        this.pf(a, new F.Rv)
    }, VS: function (a, c) {
        var d = this.rg(a), e = this.target.grid.IG();
        d.K.x += e.x / 2 * (1 - c);
        d.K.y += e.y / 2 * (1 - c);
        d.V.x -= e.x / 2 * (1 - c);
        d.V.y += e.y / 2 * (1 - c);
        d.U.x += e.x / 2 * (1 - c);
        d.U.y -= e.y / 2 * (1 - c);
        d.O.x -= e.x / 2 * (1 - c);
        d.O.y -= e.y / 2 * (1 - c);
        this.pf(a, d)
    }, update: function (a) {
        for (var c = this.da, d = F.d(0, 0), e = F.size(0, 0), f, g = 0; g < c.width; ++g)for (var h =
            0; h < c.height; ++h)d.x = g, d.y = h, e.width = g, e.height = h, f = this.xA(e, a), 0 == f ? this.kI(d) : 1 > f ? this.VS(d, f) : this.lI(d)
    }
});
F.UP = function (a, c) {
    return new F.Hv(a, c)
};
F.Hv.create = F.UP;
F.TI = F.Hv.extend({
    xA: function (a, c) {
        return 0 == a.width + a.height ? 1 : Math.pow((this.da.width * (1 - c) + this.da.height * (1 - c)) / (a.width + a.height), 6)
    }
});
F.SP = function (a, c) {
    return new F.TI(a, c)
};
F.TI.create = F.SP;
F.Iv = F.Hv.extend({
    xA: function (a, c) {
        var d = this.da.height * c;
        return 0 == d ? 1 : Math.pow(a.height / d, 6)
    }, VS: function (a, c) {
        var d = this.rg(a), e = this.target.grid.IG();
        d.K.y += e.y / 2 * (1 - c);
        d.V.y += e.y / 2 * (1 - c);
        d.U.y -= e.y / 2 * (1 - c);
        d.O.y -= e.y / 2 * (1 - c);
        this.pf(a, d)
    }
});
F.D3 = function (a, c) {
    return new F.Iv(a, c)
};
F.Iv.create = F.D3;
F.UI = F.Iv.extend({
    xA: function (a, c) {
        return 0 == a.height ? 1 : Math.pow(this.da.height * (1 - c) / a.height, 6)
    }
});
F.TP = function (a, c) {
    return new F.UI(a, c)
};
F.UI.create = F.TP;
F.XK = F.Oc.extend({
    GE: q, bk: 0, $i: q, ctor: function (a, c, d) {
        F.Bc.prototype.ctor.call(this);
        this.$i = [];
        c !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        return F.Oc.prototype.f.call(this, a, c) ? (this.GE = d || 0, this.$i.length = 0, p) : r
    }, dI: function (a, c) {
        for (var d = c - 1; 0 <= d; d--) {
            var e = 0 | F.Rb() % (d + 1), f = a[d];
            a[d] = a[e];
            a[e] = f
        }
    }, lI: function (a) {
        this.pf(a, this.rg(a))
    }, kI: function (a) {
        this.pf(a, new F.Rv)
    }, D: function (a) {
        F.Oc.prototype.D.call(this, a);
        this.bk = this.da.width * this.da.height;
        a = this.$i;
        for (var c = a.length = 0; c < this.bk; ++c)a[c] =
            c;
        this.dI(a, this.bk)
    }, update: function (a) {
        a = 0 | a * this.bk;
        for (var c = this.da, d, e = F.d(0, 0), f = this.$i, g = 0; g < this.bk; g++)d = f[g], e.x = 0 | d / c.height, e.y = d % (0 | c.height), g < a ? this.kI(e) : this.lI(e)
    }
});
F.YS = function (a, c, d) {
    return new F.XK(a, c, d)
};
F.XK.create = F.YS;
F.fL = F.Oc.extend({
    Hf: 0, ob: 0, pb: 0, ctor: function (a, c, d, e) {
        F.Bc.prototype.ctor.call(this);
        e !== k && this.f(a, c, d, e)
    }, vq: A("ob"), Zq: z("ob"), Nn: A("pb"), ao: z("pb"), f: function (a, c, d, e) {
        return F.Oc.prototype.f.call(this, a, c) ? (this.Hf = d, this.ob = e, this.pb = 1, p) : r
    }, update: function (a) {
        for (var c = this.da, d = this.Hf, e = this.ob, f = this.pb, g = F.d(0, 0), h, m = 0; m < c.width; m++)for (var n = 0; n < c.height; n++)g.x = m, g.y = n, h = this.rg(g), h.K.e = Math.sin(2 * a * Math.PI * d + 0.01 * (h.K.y + h.K.x)) * e * f, h.V.e = h.K.e, h.U.e = h.K.e, h.O.e = h.K.e, this.pf(g, h)
    }
});
F.c$ = function (a, c, d, e) {
    return new F.fL(a, c, d, e)
};
F.fL.create = F.c$;
F.$I = F.Oc.extend({
    Kj: 0, ob: 0, pb: 0, ctor: function (a, c, d, e) {
        F.Bc.prototype.ctor.call(this);
        e !== k && this.f(a, c, d, e)
    }, vq: A("ob"), Zq: z("ob"), Nn: A("pb"), ao: z("pb"), f: function (a, c, d, e) {
        return F.Oc.prototype.f.call(this, a, c) ? (this.Kj = d, this.ob = e, this.pb = 1, p) : r
    }, update: function (a) {
        var c = Math.sin(2 * Math.PI * a * this.Kj) * this.ob * this.pb;
        a = Math.sin(Math.PI * (2 * a * this.Kj + 1)) * this.ob * this.pb;
        for (var d = this.da, e = this.target.grid, f, g = F.d(0, 0), h = 0; h < d.width; h++)for (var m = 0; m < d.height; m++)g.x = h, g.y = m, f = e.rg(g), 0 == (h + m) % 2 ?
            (f.K.e += c, f.V.e += c, f.U.e += c, f.O.e += c) : (f.K.e += a, f.V.e += a, f.U.e += a, f.O.e += a), e.pf(g, f)
    }
});
F.R5 = function (a, c, d, e) {
    return new F.$I(a, c, d, e)
};
F.$I.create = F.R5;
F.cK = F.Oc.extend({
    z0: 0, Dt: q, ctor: function (a, c) {
        F.Bc.prototype.ctor.call(this);
        c !== k && this.f(a, c)
    }, f: function (a, c) {
        this.z0 = c;
        return F.Oc.prototype.f.call(this, a, F.size(1, c))
    }, update: function (a) {
        for (var c = this.da, d = this.Dt.width, e, f, g = F.d(0, 0), h = 0; h < c.height; ++h)g.y = h, e = this.rg(g), f = 1, 0 == h % 2 && (f = -1), e.K.x += f * d * a, e.V.x += f * d * a, e.U.x += f * d * a, e.O.x += f * d * a, this.pf(g, e)
    }, D: function (a) {
        F.Oc.prototype.D.call(this, a);
        this.Dt = F.L.Xt()
    }
});
F.JS = function (a, c) {
    return new F.cK(a, c)
};
F.cK.create = F.JS;
F.bK = F.Oc.extend({
    XY: 0, Dt: q, ctor: function (a, c) {
        F.Bc.prototype.ctor.call(this);
        c !== k && this.f(a, c)
    }, f: function (a, c) {
        this.XY = c;
        return F.Oc.prototype.f.call(this, a, F.size(c, 1))
    }, update: function (a) {
        for (var c = this.da.width, d = this.Dt.height, e, f, g = F.d(0, 0), h = 0; h < c; ++h)g.x = h, e = this.rg(g), f = 1, 0 == h % 2 && (f = -1), e.K.y += f * d * a, e.V.y += f * d * a, e.U.y += f * d * a, e.O.y += f * d * a, this.pf(g, e);
        F.ka.xe = p
    }, D: function (a) {
        F.Oc.prototype.D.call(this, a);
        this.Dt = F.L.Xt()
    }
});
F.IS = function (a, c) {
    return new F.bK(a, c)
};
F.bK.create = F.IS;
F.uJ = F.rd.extend({
    update: function (a) {
        var c = Math.max(0, a - 0.25), c = -100 - 500 * c * c, d = +Math.PI / 2 + -Math.PI / 2 * Math.sqrt(a);
        a = Math.sin(d);
        for (var d = Math.cos(d), e = this.da, f = F.d(0, 0), g = 0; g <= e.width; ++g)for (var h = 0; h <= e.height; ++h) {
            f.x = g;
            f.y = h;
            var m = this.dd(f), n = Math.sqrt(m.x * m.x + (m.y - c) * (m.y - c)), s = n * a, t = Math.asin(m.x / n) / a, v = Math.cos(t);
            m.x = t <= Math.PI ? s * Math.sin(t) : 0;
            m.y = n + c - s * (1 - v) * a;
            m.e = s * (1 - v) * d / 7;
            0.5 > m.e && (m.e = 0.5);
            this.je(f, m)
        }
    }
});
F.uH = function (a, c) {
    return new F.uJ(a, c)
};
F.uJ.create = F.uH;
F.Va = F.n.extend({
    Wb: q, Oi: 0, Kb: q, Mh: q, zm: q, Pd: r, Mb: "ProgressTimer", y4: function () {
        return F.d(this.Mh.x, this.Mh.y)
    }, s8: function (a) {
        this.Mh = F.xR(a)
    }, O3: function () {
        return F.d(this.zm.x, this.zm.y)
    }, X7: function (a) {
        this.zm = F.xR(a)
    }, LG: A("Wb"), G4: A("Oi"), wQ: A("Kb"), y8: function (a) {
        this.Oi != a && (this.Oi = F.od(a, 0, 100), this.qF())
    }, Of: u(), ng: D(r), M5: A("Pd"), KC: function (a) {
        if (a < F.Va.hK) {
            var c = F.Va.jX;
            return this.Pd ? F.d(c >> 7 - (a << 1) & 1, c >> 7 - ((a << 1) + 1) & 1) : F.d(c >> (a << 1) + 1 & 1, c >> (a << 1) & 1)
        }
        return F.d(0, 0)
    }, Qc: 0, Ic: q, gf: q,
    yy: q, rn: r, ctor: q, us: function (a) {
        F.n.prototype.ctor.call(this);
        this.Wb = F.Va.Jg;
        this.Oi = 0;
        this.Mh = F.d(0, 0);
        this.zm = F.d(0, 0);
        this.Pd = r;
        this.Kb = q;
        this.Z = new F.KJ(this);
        a && this.jN(a)
    }, vs: function (a) {
        F.n.prototype.ctor.call(this);
        this.Wb = F.Va.Jg;
        this.Oi = 0;
        this.Mh = F.d(0, 0);
        this.zm = F.d(0, 0);
        this.Pd = r;
        this.Kb = q;
        this.yy = F.l.createBuffer();
        this.Qc = 0;
        this.gf = this.Ic = q;
        this.rn = r;
        this.Z = new F.LJ(this);
        a && this.kN(a)
    }, rb: function (a) {
        this.Kb.color = a;
        this.vc()
    }, wb: function (a) {
        this.Kb.opacity = a;
        this.vc()
    }, mg: function () {
        return this.Kb.color
    },
    jh: function () {
        return this.Kb.opacity
    }, vS: q, U0: function (a) {
        this.Pd !== a && (this.Pd = a)
    }, V0: function (a) {
        this.Pd !== a && (this.Pd = a, this.gf = this.Ic = q, this.Qc = 0)
    }, qA: q, $0: function (a) {
        this.Kb != a && (this.Kb = a, this.Z.Kb = a, this.width = this.Kb.width, this.height = this.Kb.height)
    }, a1: function (a) {
        a && this.Kb != a && (this.Kb = a, this.width = a.width, this.height = a.height, this.Ic && (this.gf = this.Ic = q, this.Qc = 0))
    }, $H: q, h1: function (a) {
        a !== this.Wb && (this.Wb = a, this.Z.Wb = a)
    }, i1: function (a) {
        a !== this.Wb && (this.Ic && (this.gf = this.Ic = q, this.Qc =
            0), this.Wb = a)
    }, UH: q, S0: function (a) {
        this.Pd !== a && (this.Pd = a)
    }, T0: function (a) {
        this.Pd !== a && (this.Pd = a, this.gf = this.Ic = q, this.Qc = 0)
    }, Oe: function (a) {
        var c = this.Kb;
        if (!c)return {pa: 0, qa: 0};
        var d = c.quad, e = F.d(d.K.p.pa, d.K.p.qa), d = F.d(d.O.p.pa, d.O.p.qa);
        c.textureRectRotated && (c = a.x, a.x = a.y, a.y = c);
        return {pa: e.x * (1 - a.x) + d.x * a.x, qa: e.y * (1 - a.y) + d.y * a.y}
    }, Pe: function (a) {
        if (!this.Kb)return {x: 0, y: 0};
        var c = this.Kb.quad, d = F.d(c.K.j.x, c.K.j.y), c = F.d(c.O.j.x, c.O.j.y);
        return {x: d.x * (1 - a.x) + c.x * a.x, y: d.y * (1 - a.y) + c.y * a.y}
    },
    UQ: q, jN: function (a) {
        this.percentage = 0;
        this.anchorY = this.anchorX = 0.5;
        this.Wb = F.Va.Jg;
        this.Pd = r;
        this.midPoint = F.d(0.5, 0.5);
        this.barChangeRate = F.d(1, 1);
        this.sprite = a;
        return p
    }, kN: function (a) {
        this.percentage = 0;
        this.gf = this.Ic = q;
        this.Qc = 0;
        this.anchorY = this.anchorX = 0.5;
        this.Wb = F.Va.Jg;
        this.Pd = r;
        this.midPoint = F.d(0.5, 0.5);
        this.barChangeRate = F.d(1, 1);
        this.sprite = a;
        this.shaderProgram = F.le.Kc(F.yj);
        return p
    }, Ka: q, Fj: function (a) {
        a = a || F.l;
        if (this.Ic && this.Kb) {
            F.nu(this);
            var c = this.Kb.Jf();
            F.pd(c.src, c.J);
            F.Xb(F.Bh);
            F.Cd(this.Kb.texture);
            a.bindBuffer(a.ARRAY_BUFFER, this.yy);
            this.rn && (a.bufferData(a.ARRAY_BUFFER, this.gf, a.DYNAMIC_DRAW), this.rn = r);
            c = F.Id.BYTES_PER_ELEMENT;
            a.vertexAttribPointer(F.kb, 2, a.FLOAT, r, c, 0);
            a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, c, 8);
            a.vertexAttribPointer(F.hd, 2, a.FLOAT, r, c, 12);
            this.Wb === F.Va.Jg ? a.drawArrays(a.TRIANGLE_FAN, 0, this.Qc) : this.Wb == F.Va.Jk && (this.Pd ? (a.drawArrays(a.TRIANGLE_STRIP, 0, this.Qc / 2), a.drawArrays(a.TRIANGLE_STRIP, 4, this.Qc / 2), F.bd++) : a.drawArrays(a.TRIANGLE_STRIP,
                0, this.Qc));
            F.bd++
        }
    }, L1: function () {
        if (this.Kb) {
            var a, c = this.Mh;
            a = this.Oi / 100;
            var d = 2 * F.PI * (this.Pd ? a : 1 - a), e = F.d(c.x, 1), f = F.T6(e, c, d), d = 0;
            if (0 == a)f = e, d = 0; else if (1 == a)f = e, d = 4; else {
                var g = F.zU, h = F.Va.hK;
                for (a = 0; a <= h; ++a) {
                    var m = (a + (h - 1)) % h, n = this.KC(a % h), m = this.KC(m);
                    0 == a ? m = F.AR(n, m, 1 - c.x) : 4 == a && (n = F.AR(n, m, 1 - c.x));
                    var s = F.d(0, 0);
                    if (F.sH(n, m, c, f, s) && (!(0 == a || 4 == a) || 0 <= s.x && 1 >= s.x) && 0 <= s.y && s.y < g)g = s.y, d = a
                }
                f = F.sk(c, F.lj(F.ie(f, c), g))
            }
            g = p;
            this.Qc != d + 3 && (g = r, this.gf = this.Ic = q, this.Qc = 0);
            if (!this.Ic) {
                h = this.Qc =
                    d + 3;
                n = F.Id.BYTES_PER_ELEMENT;
                this.gf = new ArrayBuffer(h * n);
                m = [];
                for (a = 0; a < h; a++)m[a] = new F.Id(q, q, q, this.gf, a * n);
                this.Ic = m;
                if (!this.Ic) {
                    F.log("cc.ProgressTimer._updateRadial() : Not enough memory");
                    return
                }
            }
            this.vc();
            h = this.Ic;
            if (!g) {
                h[0].p = this.Oe(c);
                h[0].j = this.Pe(c);
                h[1].p = this.Oe(e);
                h[1].j = this.Pe(e);
                for (a = 0; a < d; a++)c = this.KC(a), h[a + 2].p = this.Oe(c), h[a + 2].j = this.Pe(c)
            }
            h[this.Qc - 1].p = this.Oe(f);
            h[this.Qc - 1].j = this.Pe(f)
        }
    }, F1: function () {
        if (this.Kb) {
            var a, c = this.Oi / 100, d = this.zm, d = F.lj(F.d(1 - d.x + c *
                d.x, 1 - d.y + c * d.y), 0.5), c = F.ie(this.Mh, d), d = F.sk(this.Mh, d);
            0 > c.x && (d.x += -c.x, c.x = 0);
            1 < d.x && (c.x -= d.x - 1, d.x = 1);
            0 > c.y && (d.y += -c.y, c.y = 0);
            1 < d.y && (c.y -= d.y - 1, d.y = 1);
            if (this.Pd) {
                if (!this.Ic) {
                    this.Qc = 8;
                    var e = F.Id.BYTES_PER_ELEMENT;
                    this.gf = new ArrayBuffer(8 * e);
                    var f = [];
                    for (a = 0; 8 > a; a++)f[a] = new F.Id(q, q, q, this.gf, a * e);
                    f[0].p = this.Oe(F.d(0, 1));
                    f[0].j = this.Pe(F.d(0, 1));
                    f[1].p = this.Oe(F.d(0, 0));
                    f[1].j = this.Pe(F.d(0, 0));
                    f[6].p = this.Oe(F.d(1, 1));
                    f[6].j = this.Pe(F.d(1, 1));
                    f[7].p = this.Oe(F.d(1, 0));
                    f[7].j = this.Pe(F.d(1,
                        0));
                    this.Ic = f
                }
                a = this.Ic;
                a[2].p = this.Oe(F.d(c.x, d.y));
                a[2].j = this.Pe(F.d(c.x, d.y));
                a[3].p = this.Oe(F.d(c.x, c.y));
                a[3].j = this.Pe(F.d(c.x, c.y));
                a[4].p = this.Oe(F.d(d.x, d.y));
                a[4].j = this.Pe(F.d(d.x, d.y));
                a[5].p = this.Oe(F.d(d.x, c.y));
                a[5].j = this.Pe(F.d(d.x, c.y))
            } else {
                if (!this.Ic) {
                    this.Qc = 4;
                    e = F.Id.BYTES_PER_ELEMENT;
                    this.gf = new ArrayBuffer(4 * e);
                    this.Ic = [];
                    for (a = 0; 4 > a; a++)this.Ic[a] = new F.Id(q, q, q, this.gf, a * e)
                }
                a = this.Ic;
                a[0].p = this.Oe(F.d(c.x, d.y));
                a[0].j = this.Pe(F.d(c.x, d.y));
                a[1].p = this.Oe(F.d(c.x, c.y));
                a[1].j = this.Pe(F.d(c.x, c.y));
                a[2].p = this.Oe(F.d(d.x, d.y));
                a[2].j = this.Pe(F.d(d.x, d.y));
                a[3].p = this.Oe(F.d(d.x, c.y));
                a[3].j = this.Pe(F.d(d.x, c.y))
            }
            this.vc()
        }
    }, vc: function () {
        if (this.Kb && this.Ic) {
            for (var a = this.Kb.quad.U.A, c = this.Ic, d = 0, e = this.Qc; d < e; ++d)c[d].A = a;
            this.rn = p
        }
    }, qF: q, H1: function () {
        var a = this.Kb, c = a.width, d = a.height, e = this.Mh, f = this.Z;
        if (this.Wb == F.Va.Jg) {
            f.Cf = Math.round(Math.sqrt(c * c + d * d));
            var g, h = r, m = f.XD;
            m.x = c * e.x;
            m.y = -d * e.y;
            this.Pd ? (g = 270, e = 270 - 3.6 * this.Oi) : (e = -90, g = -90 + 3.6 * this.Oi);
            a.pc &&
            (m.x -= c * 2 * this.Mh.x, e = -e - 180, g = -g - 180, h = !h);
            a.qc && (m.y += d * 2 * this.Mh.y, h = !h, e = -e, g = -g);
            f.HO = e;
            f.IM = g;
            f.HL = h
        } else m = this.zm, h = this.Oi / 100, f = f.wL, m = F.size(c * (1 - m.x), d * (1 - m.y)), h = F.size((c - m.width) * h, (d - m.height) * h), h = F.size(m.width + h.width, m.height + h.height), g = F.d(c * e.x, d * e.y), m = g.x - h.width / 2, 0.5 < e.x && h.width / 2 >= c - g.x && (m = c - h.width), c = g.y - h.height / 2, 0.5 < e.y && h.height / 2 >= d - g.y && (c = d - h.height), f.x = 0, d = 1, a.pc && (f.x -= h.width, d = -1), 0 < m && (f.x += m * d), f.y = 0, d = 1, a.qc && (f.y += h.height, d = -1), 0 < c && (f.y -= c * d), f.width =
            h.width, f.height = -h.height
    }, I1: function () {
        var a = this.Wb;
        a === F.Va.Jg ? this.L1() : a === F.Va.Jk && this.F1();
        this.rn = p
    }
});
M = F.Va.prototype;
F.B == F.Y ? (M.ctor = M.vs, M.vS = M.V0, M.qA = M.a1, M.$H = M.i1, M.UH = M.T0, M.UQ = M.kN, M.Ka = M.Fj, M.qF = M.I1) : (M.ctor = M.us, M.vS = M.U0, M.qA = M.$0, M.$H = M.h1, M.UH = M.S0, M.UQ = M.jN, M.Ka = M.Zo, M.qF = F.Va.prototype.H1);
F.k(M, "midPoint", M.y4, M.s8);
F.k(M, "barChangeRate", M.O3, M.X7);
F.k(M, "type", M.LG, M.$H);
F.k(M, "percentage", M.G4, M.y8);
F.k(M, "sprite", M.wQ, M.qA);
F.k(M, "reverseDir", M.M5, M.UH);
F.Va.create = function (a) {
    return new F.Va(a)
};
F.Va.hK = 4;
F.Va.jX = 75;
F.Va.Jg = 0;
F.Va.Jk = 1;
F.AB = F.C.extend({
    Rd: 0, Ld: 0, ctor: function (a, c) {
        F.C.prototype.ctor.call(this);
        this.Ld = this.Rd = 0;
        c !== k && this.f(a, c)
    }, f: function (a, c) {
        return F.C.prototype.f.call(this, a) ? (this.Rd = c, p) : r
    }, m: function () {
        var a = new F.AB;
        a.f(this.t, this.Rd);
        return a
    }, reverse: function () {
        F.log("cc.ProgressTo.reverse(): reverse hasn't been supported.");
        return q
    }, D: function (a) {
        F.C.prototype.D.call(this, a);
        this.Ld = a.percentage
    }, update: function (a) {
        this.target instanceof F.Va && (this.target.percentage = this.Ld + (this.Rd - this.Ld) * a)
    }
});
F.l7 = function (a, c) {
    return new F.AB(a, c)
};
F.AB.create = F.l7;
F.zB = F.C.extend({
    Rd: 0, Ld: 0, ctor: function (a, c, d) {
        F.C.prototype.ctor.call(this);
        this.Ld = this.Rd = 0;
        d !== k && this.f(a, c, d)
    }, f: function (a, c, d) {
        return F.C.prototype.f.call(this, a) ? (this.Rd = d, this.Ld = c, p) : r
    }, m: function () {
        var a = new F.zB;
        a.f(this.t, this.Ld, this.Rd);
        return a
    }, reverse: function () {
        return F.zH(this.t, this.Rd, this.Ld)
    }, D: function (a) {
        F.C.prototype.D.call(this, a)
    }, update: function (a) {
        this.target instanceof F.Va && (this.target.percentage = this.Ld + (this.Rd - this.Ld) * a)
    }
});
F.zH = function (a, c, d) {
    return new F.zB(a, c, d)
};
F.zB.create = F.zH;
F.Sr = 4208917214;
F.Fba = 0;
F.Ik = 1;
F.kw = 0;
F.Eba = 1;
F.ia = F.rm.extend({
    ra: q, mb: q, t: q, Kh: r, uN: r, Mb: "TransitionScene", ctor: function (a, c) {
        F.rm.prototype.ctor.call(this);
        a !== k && c !== k && this.f(a, c)
    }, tO: function () {
        this.Zu(this.tO);
        var a = F.L;
        this.uN = a.pl;
        a.GH(this.ra);
        F.La.Wd(p);
        this.mb.visible = p
    }, Ti: function () {
        this.Kh = p
    }, H: function () {
        this.Kh ? (this.mb.H(), this.ra.H()) : (this.ra.H(), this.mb.H());
        F.n.prototype.H.call(this)
    }, ba: function () {
        F.n.prototype.ba.call(this);
        F.La.Wd(r);
        this.mb.pg();
        this.ra.ba()
    }, Cb: function () {
        F.n.prototype.Cb.call(this);
        F.La.Wd(p);
        this.mb.Cb();
        this.ra.mi()
    }, jf: function () {
        F.n.prototype.jf.call(this);
        this.uN && this.mb.jf()
    }, f: function (a, c) {
        c || b("cc.TransitionScene.initWithDuration(): Argument scene must be non-nil");
        return this.oa() ? (this.t = a, this.ja({
            x: 0,
            y: 0,
            anchorX: 0,
            anchorY: 0
        }), this.ra = c, this.mb = F.L.tc, this.mb || (this.mb = new F.rm, this.mb.oa()), this.ra == this.mb && b("cc.TransitionScene.initWithDuration(): Incoming scene must be different from the outgoing scene"), this.Ti(), p) : r
    }, finish: function () {
        this.ra.ja({visible: p, x: 0, y: 0, scale: 1, rotation: 0});
        F.B === F.Y && this.ra.On().restore();
        this.mb.ja({visible: r, x: 0, y: 0, scale: 1, rotation: 0});
        F.B === F.Y && this.mb.On().restore();
        this.eA(this.tO, 0)
    }, RG: function () {
        this.ra.visible = p;
        this.mb.visible = r
    }
});
F.ia.create = function (a, c) {
    return new F.ia(a, c)
};
F.uf = F.ia.extend({
    Vm: 0, ctor: function (a, c, d) {
        F.ia.prototype.ctor.call(this);
        d != k && this.f(a, c, d)
    }, f: function (a, c, d) {
        F.ia.prototype.f.call(this, a, c) && (this.Vm = d);
        return p
    }
});
F.uf.create = function (a, c, d) {
    return new F.uf(a, c, d)
};
F.NK = F.ia.extend({
    ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        c && this.f(a, c)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        this.ra.ja({scale: 0.0010, anchorX: 0.5, anchorY: 0.5});
        this.mb.ja({scale: 1, anchorX: 0.5, anchorY: 0.5});
        var a = F.hb(F.pi(F.YR(this.t / 2, 0.0010), F.WR(this.t / 2, 720)), F.jc(this.t / 2));
        this.mb.Da(a);
        this.ra.Da(F.hb(a.reverse(), F.Eb(this.finish, this)))
    }
});
F.NK.create = function (a, c) {
    return new F.NK(a, c)
};
F.DK = F.ia.extend({
    ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        c && this.f(a, c)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a = F.L.Pa();
        this.ra.ja({scale: 0.5, x: a.width, y: 0, anchorX: 0.5, anchorY: 0.5});
        this.mb.anchorX = 0.5;
        this.mb.anchorY = 0.5;
        var c = F.bR(this.t / 4, F.d(-a.width, 0), a.width / 4, 2), d = F.Ud(this.t / 4, 1), a = F.Ud(this.t / 4, 0.5), a = F.hb(a, c), c = F.hb(c, d), d = F.jc(this.t / 2);
        this.mb.Da(a);
        this.ra.Da(F.hb(d, c, F.Eb(this.finish, this)))
    }
});
F.DK.create = function (a, c) {
    return new F.DK(a, c)
};
F.Lk = F.ia.extend({
    ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        c && this.f(a, c)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        this.lk();
        var a = this.action();
        this.ra.Da(F.hb(this.lg(a), F.Eb(this.finish, this)))
    }, lk: function () {
        this.ra.X(-F.L.Pa().width, 0)
    }, action: function () {
        return F.moveTo(this.t, F.d(0, 0))
    }, lg: function (a) {
        return new F.po(a, 2)
    }
});
F.Lk.create = function (a, c) {
    return new F.Lk(a, c)
};
F.FK = F.Lk.extend({
    ctor: function (a, c) {
        F.Lk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, lk: function () {
        this.ra.X(F.L.Pa().width, 0)
    }
});
F.FK.create = function (a, c) {
    return new F.FK(a, c)
};
F.GK = F.Lk.extend({
    ctor: function (a, c) {
        F.Lk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, lk: function () {
        this.ra.X(0, F.L.Pa().height)
    }
});
F.GK.create = function (a, c) {
    return new F.GK(a, c)
};
F.EK = F.Lk.extend({
    ctor: function (a, c) {
        F.Lk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, lk: function () {
        this.ra.X(0, -F.L.Pa().height)
    }
});
F.EK.create = function (a, c) {
    return new F.EK(a, c)
};
F.Ak = 0.5;
F.Mk = F.ia.extend({
    ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ti: function () {
        this.Kh = r
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        this.lk();
        var a = this.action(), c = this.action(), a = this.lg(a), c = F.hb(this.lg(c), F.Eb(this.finish, this));
        this.ra.Da(a);
        this.mb.Da(c)
    }, lk: function () {
        this.ra.X(-F.L.Pa().width + F.Ak, 0)
    }, action: function () {
        return F.moveBy(this.t, F.d(F.L.Pa().width - F.Ak, 0))
    }, lg: function (a) {
        return new F.oo(a, 2)
    }
});
F.Mk.create = function (a, c) {
    return new F.Mk(a, c)
};
F.QK = F.Mk.extend({
    ctor: function (a, c) {
        F.Mk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ti: function () {
        this.Kh = p
    }, lk: function () {
        this.ra.X(F.L.Pa().width - F.Ak, 0)
    }, action: function () {
        return F.moveBy(this.t, F.d(-(F.L.Pa().width - F.Ak), 0))
    }
});
F.QK.create = function (a, c) {
    return new F.QK(a, c)
};
F.PK = F.Mk.extend({
    ctor: function (a, c) {
        F.Mk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ti: function () {
        this.Kh = r
    }, lk: function () {
        this.ra.X(0, -(F.L.Pa().height - F.Ak))
    }, action: function () {
        return F.moveBy(this.t, F.d(0, F.L.Pa().height - F.Ak))
    }
});
F.PK.create = function (a, c) {
    return new F.PK(a, c)
};
F.RK = F.Mk.extend({
    ctor: function (a, c) {
        F.Mk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ti: function () {
        this.Kh = p
    }, lk: function () {
        this.ra.X(0, F.L.Pa().height - F.Ak)
    }, action: function () {
        return F.moveBy(this.t, F.d(0, -(F.L.Pa().height - F.Ak)))
    }
});
F.RK.create = function (a, c) {
    return new F.RK(a, c)
};
F.OK = F.ia.extend({
    ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        c && this.f(a, c)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        this.ra.ja({scale: 0.0010, anchorX: 2 / 3, anchorY: 0.5});
        this.mb.ja({scale: 1, anchorX: 1 / 3, anchorY: 0.5});
        var a = F.Ud(this.t, 0.01), c = F.Ud(this.t, 1);
        this.ra.Da(this.lg(c));
        this.mb.Da(F.hb(this.lg(a), F.Eb(this.finish, this)))
    }, lg: function (a) {
        return new F.po(a, 2)
    }
});
F.OK.create = function (a, c) {
    return new F.OK(a, c)
};
F.BK = F.uf.extend({
    ctor: function (a, c, d) {
        F.uf.prototype.ctor.call(this);
        d == q && (d = F.Ik);
        c && this.f(a, c, d)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a, c;
        this.ra.visible = r;
        var d;
        this.Vm === F.Ik ? (a = 90, d = 270, c = 90) : (a = -90, d = 90, c = -90);
        a = F.hb(F.jc(this.t / 2), F.show(), F.qg(this.t / 2, 1, 0, d, a, 0, 0), F.Eb(this.finish, this));
        c = F.hb(F.qg(this.t / 2, 1, 0, 0, c, 0, 0), F.hi(), F.jc(this.t / 2));
        this.ra.Da(a);
        this.mb.Da(c)
    }
});
F.BK.create = function (a, c, d) {
    return new F.BK(a, c, d)
};
F.CK = F.uf.extend({
    ctor: function (a, c, d) {
        F.uf.prototype.ctor.call(this);
        d == q && (d = F.kw);
        c && this.f(a, c, d)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a, c;
        this.ra.visible = r;
        var d;
        this.Vm == F.kw ? (a = 90, d = 270, c = 90) : (a = -90, d = 90, c = -90);
        a = F.hb(F.jc(this.t / 2), F.show(), F.qg(this.t / 2, 1, 0, d, a, 90, 0), F.Eb(this.finish, this));
        c = F.hb(F.qg(this.t / 2, 1, 0, 0, c, 90, 0), F.hi(), F.jc(this.t / 2));
        this.ra.Da(a);
        this.mb.Da(c)
    }
});
F.CK.create = function (a, c, d) {
    return new F.CK(a, c, d)
};
F.AK = F.uf.extend({
    ctor: function (a, c, d) {
        F.uf.prototype.ctor.call(this);
        d == q && (d = F.Ik);
        c && this.f(a, c, d)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a, c;
        this.ra.visible = r;
        var d;
        this.Vm === F.Ik ? (a = 90, d = 270, c = 90) : (a = -90, d = 90, c = -90);
        a = F.hb(F.jc(this.t / 2), F.show(), F.qg(this.t / 2, 1, 0, d, a, -45, 0), F.Eb(this.finish, this));
        c = F.hb(F.qg(this.t / 2, 1, 0, 0, c, 45, 0), F.hi(), F.jc(this.t / 2));
        this.ra.Da(a);
        this.mb.Da(c)
    }
});
F.AK.create = function (a, c, d) {
    return new F.AK(a, c, d)
};
F.VK = F.uf.extend({
    ctor: function (a, c, d) {
        F.uf.prototype.ctor.call(this);
        d == q && (d = F.Ik);
        c && this.f(a, c, d)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a, c;
        this.ra.visible = r;
        var d;
        this.Vm === F.Ik ? (a = 90, d = 270, c = 90) : (a = -90, d = 90, c = -90);
        a = F.hb(F.jc(this.t / 2), F.pi(F.qg(this.t / 2, 1, 0, d, a, 0, 0), F.Ud(this.t / 2, 1), F.show()), F.Eb(this.finish, this));
        c = F.hb(F.pi(F.qg(this.t / 2, 1, 0, 0, c, 0, 0), F.Ud(this.t / 2, 0.5)), F.hi(), F.jc(this.t / 2));
        this.ra.scale = 0.5;
        this.ra.Da(a);
        this.mb.Da(c)
    }
});
F.VK.create = function (a, c, d) {
    return new F.VK(a, c, d)
};
F.WK = F.uf.extend({
    ctor: function (a, c, d) {
        F.uf.prototype.ctor.call(this);
        d == q && (d = F.kw);
        c && this.f(a, c, d)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a, c;
        this.ra.visible = r;
        var d;
        this.Vm === F.kw ? (a = 90, d = 270, c = 90) : (a = -90, d = 90, c = -90);
        a = F.hb(F.jc(this.t / 2), F.pi(F.qg(this.t / 2, 1, 0, d, a, 90, 0), F.Ud(this.t / 2, 1), F.show()), F.Eb(this.finish, this));
        c = F.hb(F.pi(F.qg(this.t / 2, 1, 0, 0, c, 90, 0), F.Ud(this.t / 2, 0.5)), F.hi(), F.jc(this.t / 2));
        this.ra.scale = 0.5;
        this.ra.Da(a);
        this.mb.Da(c)
    }
});
F.WK.create = function (a, c, d) {
    return new F.WK(a, c, d)
};
F.UK = F.uf.extend({
    ctor: function (a, c, d) {
        F.uf.prototype.ctor.call(this);
        d == q && (d = F.Ik);
        c && this.f(a, c, d)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a, c;
        this.ra.visible = r;
        var d;
        this.Vm === F.Ik ? (a = 90, d = 270, c = 90) : (a = -90, d = 90, c = -90);
        a = F.hb(F.jc(this.t / 2), F.pi(F.qg(this.t / 2, 1, 0, d, a, -45, 0), F.Ud(this.t / 2, 1), F.show()), F.show(), F.Eb(this.finish, this));
        c = F.hb(F.pi(F.qg(this.t / 2, 1, 0, 0, c, 45, 0), F.Ud(this.t / 2, 0.5)), F.hi(), F.jc(this.t / 2));
        this.ra.scale = 0.5;
        this.ra.Da(a);
        this.mb.Da(c)
    }
});
F.UK.create = function (a, c, d) {
    return new F.UK(a, c, d)
};
F.wK = F.ia.extend({
    qe: q, ctor: function (a, c, d) {
        F.ia.prototype.ctor.call(this);
        this.qe = F.color();
        c && this.f(a, c, d)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a = new F.Gd(this.qe);
        this.ra.visible = r;
        this.F(a, 2, F.Sr);
        var a = this.di(F.Sr), c = F.hb(F.rG(this.t / 2), F.Eb(this.RG, this), F.Kn(this.t / 2), F.Eb(this.finish, this));
        a.Da(c)
    }, Cb: function () {
        F.ia.prototype.Cb.call(this);
        this.DH(F.Sr, r)
    }, f: function (a, c, d) {
        d = d || F.color.BLACK;
        F.ia.prototype.f.call(this, a, c) && (this.qe.r = d.r, this.qe.g = d.g, this.qe.b = d.b, this.qe.a =
            0);
        return p
    }
});
F.wK.create = function (a, c, d) {
    return new F.wK(a, c, d)
};
F.vK = F.ia.extend({
    ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        c && this.f(a, c)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a = F.color(0, 0, 0, 0), c = F.L.Pa(), a = new F.Gd(a), d = new F.Ao(c.width, c.height);
        if (q != d) {
            d.sprite.anchorX = 0.5;
            d.sprite.anchorY = 0.5;
            d.ja({x: c.width / 2, y: c.height / 2, anchorX: 0.5, anchorY: 0.5});
            d.hh();
            this.ra.H();
            d.end();
            var e = new F.Ao(c.width, c.height);
            e.X(c.width / 2, c.height / 2);
            e.sprite.anchorX = e.anchorX = 0.5;
            e.sprite.anchorY = e.anchorY = 0.5;
            e.hh();
            this.mb.H();
            e.end();
            d.sprite.Vd(F.ONE,
                F.ONE);
            e.sprite.Vd(F.SRC_ALPHA, F.ONE_MINUS_SRC_ALPHA);
            a.F(d);
            a.F(e);
            d.sprite.opacity = 255;
            e.sprite.opacity = 255;
            c = F.hb(F.VP(this.t, 0), F.Eb(this.RG, this), F.Eb(this.finish, this));
            e.sprite.Da(c);
            this.F(a, 2, F.Sr)
        }
    }, Cb: function () {
        this.DH(F.Sr, r);
        F.ia.prototype.Cb.call(this)
    }, H: function () {
        F.n.prototype.H.call(this)
    }, Ka: u()
});
F.vK.create = function (a, c) {
    return new F.vK(a, c)
};
F.TK = F.ia.extend({
    Ec: q, ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        this.Ec = new F.qm;
        c && this.f(a, c)
    }, Ti: function () {
        this.Kh = r
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        this.Ec.pj(this.mb);
        this.Ec.ba();
        var a = F.L.Pa(), a = F.YS(this.t, F.size(0 | 12 * (a.width / a.height), 12)), a = this.lg(a);
        this.Ec.Da(F.hb(a, F.Eb(this.finish, this), F.hr()))
    }, H: function () {
        this.ra.H();
        this.Ec.H()
    }, lg: aa()
});
F.TK.create = function (a, c) {
    return new F.TK(a, c)
};
F.pw = F.ia.extend({
    Ec: q, v1: function () {
        this.Ec.pj(this.ra)
    }, ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        this.Ec = new F.qm;
        c && this.f(a, c)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        this.Ec.pj(this.mb);
        this.Ec.ba();
        var a = this.action(), a = F.hb(a, F.Eb(this.v1, this), a.reverse());
        this.Ec.Da(F.hb(this.lg(a), F.Eb(this.finish, this), F.hr()))
    }, Cb: function () {
        this.Ec.pj(q);
        this.Ec.Cb();
        F.ia.prototype.Cb.call(this)
    }, H: function () {
        this.Ec.H()
    }, lg: function (a) {
        return new F.oo(a, 3)
    }, action: function () {
        return F.IS(this.t /
            2, 3)
    }
});
F.pw.create = function (a, c) {
    return new F.pw(a, c)
};
F.SK = F.pw.extend({
    ctor: function (a, c) {
        F.pw.prototype.ctor.call(this);
        c && this.f(a, c)
    }, action: function () {
        return F.JS(this.t / 2, 3)
    }
});
F.SK.create = function (a, c) {
    return new F.SK(a, c)
};
F.Kk = F.ia.extend({
    Ec: q, ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        this.Ec = new F.qm;
        c && this.f(a, c)
    }, Ti: function () {
        this.Kh = r
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        this.Ec.pj(this.mb);
        this.Ec.ba();
        var a = F.L.Pa(), a = this.Yp(F.size(0 | 12 * (a.width / a.height), 12));
        this.Ec.Da(F.hb(this.lg(a), F.Eb(this.finish, this), F.hr()))
    }, H: function () {
        this.ra.H();
        this.Ec.H()
    }, lg: aa(), Yp: function (a) {
        return F.UP(this.t, a)
    }
});
F.Kk.create = function (a, c) {
    return new F.Kk(a, c)
};
F.xK = F.Kk.extend({
    ctor: function (a, c) {
        F.Kk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Yp: function (a) {
        return F.SP(this.t, a)
    }
});
F.xK.create = function (a, c) {
    return new F.xK(a, c)
};
F.zK = F.Kk.extend({
    ctor: function (a, c) {
        F.Kk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Yp: function (a) {
        return new F.Iv(this.t, a)
    }
});
F.zK.create = function (a, c) {
    return new F.zK(a, c)
};
F.yK = F.Kk.extend({
    ctor: function (a, c) {
        F.Kk.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Yp: function (a) {
        return F.TP(this.t, a)
    }
});
F.yK.create = function (a, c) {
    return new F.yK(a, c)
};
F.SJ = 49153;
F.tf = F.ia.extend({
    Rd: 0, Ld: 0, Xx: q, Mb: "TransitionProgress", ctor: function (a, c) {
        F.ia.prototype.ctor.call(this);
        c && this.f(a, c)
    }, en: function (a, c, d) {
        a.ja({x: c, y: d, anchorX: 0.5, anchorY: 0.5})
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        this.AO();
        var a = F.L.Pa(), c = new F.Ao(a.width, a.height);
        c.sprite.anchorX = 0.5;
        c.sprite.anchorY = 0.5;
        this.en(c, a.width / 2, a.height / 2);
        c.clear(0, 0, 0, 1);
        c.hh();
        this.Xx.H();
        c.end();
        this.Xx == this.mb && this.RG();
        a = this.Ym(c);
        c = F.hb(F.zH(this.t, this.Ld, this.Rd), F.Eb(this.finish, this));
        a.Da(c);
        this.F(a, 2, F.SJ)
    }, Cb: function () {
        this.DH(F.SJ, p);
        F.ia.prototype.Cb.call(this)
    }, AO: function () {
        this.Xx = this.mb;
        this.Ld = 100;
        this.Rd = 0
    }, Ym: function () {
        F.log("cc.TransitionProgress._progressTimerNodeWithRenderTexture(): should be overridden in subclass");
        return q
    }, Ti: function () {
        this.Kh = r
    }
});
F.tf.create = function (a, c) {
    return new F.tf(a, c)
};
F.LK = F.tf.extend({
    ctor: function (a, c) {
        F.tf.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ym: function (a) {
        var c = F.L.Pa();
        a = new F.Va(a.sprite);
        F.B === F.Y && (a.sprite.flippedY = p);
        a.type = F.Va.Jg;
        a.reverseDir = r;
        a.percentage = 100;
        this.en(a, c.width / 2, c.height / 2);
        return a
    }
});
F.LK.create = function (a, c) {
    return new F.LK(a, c)
};
F.kC = F.tf.extend({
    ctor: function (a, c) {
        F.tf.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ym: function (a) {
        var c = F.L.Pa();
        a = new F.Va(a.sprite);
        F.B === F.Y && (a.sprite.flippedY = p);
        a.type = F.Va.Jg;
        a.reverseDir = p;
        a.percentage = 100;
        this.en(a, c.width / 2, c.height / 2);
        return a
    }
});
F.kC.create = function (a, c) {
    var d = new F.kC;
    return d != q && d.f(a, c) ? d : new F.kC(a, c)
};
F.IK = F.tf.extend({
    ctor: function (a, c) {
        F.tf.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ym: function (a) {
        var c = F.L.Pa();
        a = new F.Va(a.sprite);
        F.B === F.Y && (a.sprite.flippedY = p);
        a.type = F.Va.Jk;
        a.midPoint = F.d(1, 0);
        a.barChangeRate = F.d(1, 0);
        a.percentage = 100;
        this.en(a, c.width / 2, c.height / 2);
        return a
    }
});
F.IK.create = function (a, c) {
    return new F.IK(a, c)
};
F.MK = F.tf.extend({
    ctor: function (a, c) {
        F.tf.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ym: function (a) {
        var c = F.L.Pa();
        a = new F.Va(a.sprite);
        F.B === F.Y && (a.sprite.flippedY = p);
        a.type = F.Va.Jk;
        a.midPoint = F.d(0, 0);
        a.barChangeRate = F.d(0, 1);
        a.percentage = 100;
        this.en(a, c.width / 2, c.height / 2);
        return a
    }
});
F.MK.create = function (a, c) {
    return new F.MK(a, c)
};
F.JK = F.tf.extend({
    ctor: function (a, c) {
        F.tf.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ym: function (a) {
        var c = F.L.Pa();
        a = new F.Va(a.sprite);
        F.B === F.Y && (a.sprite.flippedY = p);
        a.type = F.Va.Jk;
        a.midPoint = F.d(0.5, 0.5);
        a.barChangeRate = F.d(1, 1);
        a.percentage = 0;
        this.en(a, c.width / 2, c.height / 2);
        return a
    }, Ti: function () {
        this.Kh = r
    }, AO: function () {
        this.Xx = this.ra;
        this.Ld = 0;
        this.Rd = 100
    }
});
F.JK.create = function (a, c) {
    return new F.JK(a, c)
};
F.KK = F.tf.extend({
    ctor: function (a, c) {
        F.tf.prototype.ctor.call(this);
        c && this.f(a, c)
    }, Ym: function (a) {
        var c = F.L.Pa();
        a = new F.Va(a.sprite);
        F.B === F.Y && (a.sprite.flippedY = p);
        a.type = F.Va.Jk;
        a.midPoint = F.d(0.5, 0.5);
        a.barChangeRate = F.d(1, 1);
        a.percentage = 100;
        this.en(a, c.width / 2, c.height / 2);
        return a
    }
});
F.KK.create = function (a, c) {
    return new F.KK(a, c)
};
F.HK = F.ia.extend({
    ctor: function (a, c, d) {
        F.ia.prototype.ctor.call(this);
        this.Ec = new F.qm;
        this.f(a, c, d)
    }, ns: p, Ec: q, Mb: "TransitionPageTurn", f: function (a, c, d) {
        this.ns = d;
        F.ia.prototype.f.call(this, a, c);
        return p
    }, Yp: function (a) {
        return this.ns ? F.VR(F.uH(this.t, a)) : F.uH(this.t, a)
    }, ba: function () {
        F.ia.prototype.ba.call(this);
        var a = F.L.Pa(), c;
        a.width > a.height ? (a = 16, c = 12) : (a = 12, c = 16);
        a = this.Yp(F.size(a, c));
        c = this.Ec;
        this.ns ? (c.pj(this.ra), c.ba(), this.ra.visible = r, c.Da(F.hb(a, F.Eb(this.finish, this), F.hr())), this.ra.Da(F.show())) :
            (c.pj(this.mb), c.ba(), c.Da(F.hb(a, F.Eb(this.finish, this), F.hr())))
    }, H: function () {
        this.ns ? this.mb.H() : this.ra.H();
        this.Ec.H()
    }, Ti: function () {
        this.Kh = this.ns
    }
});
F.HK.create = function (a, c, d) {
    return new F.HK(a, c, d)
};
F.ua = {name: "Jacob__Codec"};
F.S9 = function () {
    return F.ua.Ua.dz.apply(F.ua.Ua, arguments)
};
F.T9 = function () {
    var a = F.ua.im.AP.apply(F.ua.im, arguments);
    return F.ua.Ua.dz.apply(F.ua.Ua, [a])
};
F.bT = function (a, c) {
    c = c || 1;
    var d = this.T9(a), e = [], f, g, h;
    f = 0;
    for (h = d.length / c; f < h; f++) {
        e[f] = 0;
        for (g = c - 1; 0 <= g; --g)e[f] += d.charCodeAt(f * c + g) << 8 * g
    }
    return e
};
F.Fpa = function (a, c) {
    c = c || 1;
    var d = this.S9(a), e = [], f, g, h;
    f = 0;
    for (h = d.length / c; f < h; f++) {
        e[f] = 0;
        for (g = c - 1; 0 <= g; --g)e[f] += d.charCodeAt(f * c + g) << 8 * g
    }
    return e
};
F.qba = function (a) {
    a = a.split(",");
    var c = [], d;
    for (d = 0; d < a.length; d++)c.push(parseInt(a[d]));
    return c
};
F.ua.im = {name: "Jacob__Codec__Base64"};
F.ua.im.vx = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/\x3d";
F.ua.im.AP = function (a) {
    var c = [], d, e, f, g, h, m = 0;
    for (a = a.replace(/[^A-Za-z0-9\+\/\=]/g, ""); m < a.length;)d = this.vx.indexOf(a.charAt(m++)), e = this.vx.indexOf(a.charAt(m++)), g = this.vx.indexOf(a.charAt(m++)), h = this.vx.indexOf(a.charAt(m++)), d = d << 2 | e >> 4, e = (e & 15) << 4 | g >> 2, f = (g & 3) << 6 | h, c.push(String.fromCharCode(d)), 64 != g && c.push(String.fromCharCode(e)), 64 != h && c.push(String.fromCharCode(f));
    return c = c.join("")
};
F.ua.im.BP = function (a, c) {
    var d = this.AP(a), e = [], f, g, h;
    f = 0;
    for (h = d.length / c; f < h; f++) {
        e[f] = 0;
        for (g = c - 1; 0 <= g; --g)e[f] += d.charCodeAt(f * c + g) << 8 * g
    }
    return e
};
F.P9 = function (a) {
    if (0 != a.length % 4)return q;
    for (var c = a.length / 4, d = window.Pba ? new Uint32Array(c) : [], e = 0; e < c; e++) {
        var f = 4 * e;
        d[e] = a[f] + 256 * a[f + 1] + 65536 * a[f + 2] + 16777216 * a[f + 3]
    }
    return d
};
F.ua.Ua = function (a) {
    this.data = a;
    this.debug = r;
    this.kk = k;
    this.files = 0;
    this.AA = [];
    this.OF = Array(32768);
    this.Zh = 0;
    this.Iz = r;
    this.mP = 0;
    this.cj = 1;
    this.eq = 0;
    this.Lq = [];
    this.WP = k;
    this.ku = Array(F.ua.Ua.cV);
    this.Fn = Array(32);
    this.Yu = 0;
    this.xB = q;
    this.ki = 0;
    this.sq = Array(17);
    this.sq[0] = 0;
    this.aQ = this.ZP = k
};
F.ua.Ua.dz = function (a) {
    return (new F.ua.Ua(a)).dz()[0][0]
};
F.ua.Ua.dB = function () {
    this.Dy = this.Cy = 0;
    this.gH = q;
    this.T5 = -1
};
F.ua.Ua.cV = 288;
F.ua.Ua.kJ = 256;
F.ua.Ua.LF = [0, 128, 64, 192, 32, 160, 96, 224, 16, 144, 80, 208, 48, 176, 112, 240, 8, 136, 72, 200, 40, 168, 104, 232, 24, 152, 88, 216, 56, 184, 120, 248, 4, 132, 68, 196, 36, 164, 100, 228, 20, 148, 84, 212, 52, 180, 116, 244, 12, 140, 76, 204, 44, 172, 108, 236, 28, 156, 92, 220, 60, 188, 124, 252, 2, 130, 66, 194, 34, 162, 98, 226, 18, 146, 82, 210, 50, 178, 114, 242, 10, 138, 74, 202, 42, 170, 106, 234, 26, 154, 90, 218, 58, 186, 122, 250, 6, 134, 70, 198, 38, 166, 102, 230, 22, 150, 86, 214, 54, 182, 118, 246, 14, 142, 78, 206, 46, 174, 110, 238, 30, 158, 94, 222, 62, 190, 126, 254, 1, 129, 65, 193, 33, 161, 97, 225, 17, 145,
    81, 209, 49, 177, 113, 241, 9, 137, 73, 201, 41, 169, 105, 233, 25, 153, 89, 217, 57, 185, 121, 249, 5, 133, 69, 197, 37, 165, 101, 229, 21, 149, 85, 213, 53, 181, 117, 245, 13, 141, 77, 205, 45, 173, 109, 237, 29, 157, 93, 221, 61, 189, 125, 253, 3, 131, 67, 195, 35, 163, 99, 227, 19, 147, 83, 211, 51, 179, 115, 243, 11, 139, 75, 203, 43, 171, 107, 235, 27, 155, 91, 219, 59, 187, 123, 251, 7, 135, 71, 199, 39, 167, 103, 231, 23, 151, 87, 215, 55, 183, 119, 247, 15, 143, 79, 207, 47, 175, 111, 239, 31, 159, 95, 223, 63, 191, 127, 255];
F.ua.Ua.xP = [3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 0, 0];
F.ua.Ua.yP = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0, 99, 99];
F.ua.Ua.wP = [1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577];
F.ua.Ua.lq = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13];
F.ua.Ua.border = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];
F.ua.Ua.prototype.dz = function () {
    this.xu = [];
    pa(this);
    return this.AA
};
F.ua.Ua.prototype.W = function () {
    this.eq += 8;
    return this.mP < this.data.length ? this.data.charCodeAt(this.mP++) : -1
};
function qa(a) {
    var c;
    a.eq++;
    c = a.cj & 1;
    a.cj >>= 1;
    0 == a.cj && (a.cj = a.W(), c = a.cj & 1, a.cj = a.cj >> 1 | 128);
    return c
}
function V(a, c) {
    for (var d = 0, e = c; e--;)d = d << 1 | qa(a);
    c && (d = F.ua.Ua.LF[d] >> 8 - c);
    return d
}
function ra(a, c) {
    a.OF[a.Zh++] = c;
    a.xu.push(String.fromCharCode(c));
    32768 == a.Zh && (a.Zh = 0)
}
function sa(a) {
    for (; ;) {
        if (a.sq[a.ki] >= a.aQ)return -1;
        if (a.ZP[a.sq[a.ki]] == a.ki)return a.sq[a.ki]++;
        a.sq[a.ki]++
    }
}
function ta(a) {
    var c = a.xB[a.Yu], d;
    if (17 == a.ki)return -1;
    a.Yu++;
    a.ki++;
    d = sa(a);
    if (0 <= d)c.Cy = d; else if (c.Cy = 32768, ta(a))return -1;
    d = sa(a);
    if (0 <= d)c.Dy = d, c.gH = q; else if (c.Dy = 32768, c.gH = a.xB[a.Yu], c.T5 = a.Yu, ta(a))return -1;
    a.ki--;
    return 0
}
function ua(a, c, d, e) {
    a.xB = c;
    a.Yu = 0;
    a.ZP = e;
    a.aQ = d;
    for (c = 0; 17 > c; c++)a.sq[c] = 0;
    a.ki = 0;
    return ta(a) ? -1 : 0
}
function va(a, c) {
    for (var d, e, f = 0, g = c[f]; ;)if (d = qa(a)) {
        if (!(g.Dy & 32768))return g.Dy;
        g = g.gH;
        d = c.length;
        for (e = 0; e < d; e++)if (c[e] === g) {
            f = e;
            break
        }
    } else {
        if (!(g.Cy & 32768))return g.Cy;
        f++;
        g = c[f]
    }
    return -1
}
function xa(a) {
    var c, d, e, f, g;
    do if (c = qa(a), e = V(a, 2), 0 == e) {
        a.cj = 1;
        e = a.W();
        e |= a.W() << 8;
        d = a.W();
        d |= a.W() << 8;
        for ((e ^ ~d) & 65535 && document.write("BlockLen checksum mismatch\n"); e--;)d = a.W(), ra(a, d)
    } else if (1 == e)for (; ;)if (e = F.ua.Ua.LF[V(a, 7)] >> 1, 23 < e ? (e = e << 1 | qa(a), 199 < e ? (e -= 128, e = e << 1 | qa(a)) : (e -= 48, 143 < e && (e += 136))) : e += 256, 256 > e)ra(a, e); else if (256 == e)break; else {
        var h;
        e -= 257;
        g = V(a, F.ua.Ua.yP[e]) + F.ua.Ua.xP[e];
        e = F.ua.Ua.LF[V(a, 5)] >> 3;
        8 < F.ua.Ua.lq[e] ? (h = V(a, 8), h |= V(a, F.ua.Ua.lq[e] - 8) << 8) : h = V(a, F.ua.Ua.lq[e]);
        h += F.ua.Ua.wP[e];
        for (e = 0; e < g; e++)d = a.OF[a.Zh - h & 32767], ra(a, d)
    } else if (2 == e) {
        var m = Array(320);
        d = 257 + V(a, 5);
        h = 1 + V(a, 5);
        f = 4 + V(a, 4);
        for (e = 0; 19 > e; e++)m[e] = 0;
        for (e = 0; e < f; e++)m[F.ua.Ua.border[e]] = V(a, 3);
        g = a.Fn.length;
        for (f = 0; f < g; f++)a.Fn[f] = new F.ua.Ua.dB;
        if (ua(a, a.Fn, 19, m)) {
            a.Zh = 0;
            return
        }
        g = d + h;
        f = 0;
        for (var n = -1; f < g;)if (n++, e = va(a, a.Fn), 16 > e)m[f++] = e; else if (16 == e) {
            var s;
            e = 3 + V(a, 2);
            if (f + e > g) {
                a.Zh = 0;
                return
            }
            for (s = f ? m[f - 1] : 0; e--;)m[f++] = s
        } else {
            e = 17 == e ? 3 + V(a, 3) : 11 + V(a, 7);
            if (f + e > g) {
                a.Zh = 0;
                return
            }
            for (; e--;)m[f++] =
                0
        }
        g = a.ku.length;
        for (f = 0; f < g; f++)a.ku[f] = new F.ua.Ua.dB;
        if (ua(a, a.ku, d, m)) {
            a.Zh = 0;
            return
        }
        g = a.ku.length;
        for (f = 0; f < g; f++)a.Fn[f] = new F.ua.Ua.dB;
        e = [];
        for (f = d; f < m.length; f++)e[f - d] = m[f];
        if (ua(a, a.Fn, h, e)) {
            a.Zh = 0;
            return
        }
        for (; ;)if (e = va(a, a.ku), 256 <= e) {
            e -= 256;
            if (0 == e)break;
            e--;
            g = V(a, F.ua.Ua.yP[e]) + F.ua.Ua.xP[e];
            e = va(a, a.Fn);
            8 < F.ua.Ua.lq[e] ? (h = V(a, 8), h |= V(a, F.ua.Ua.lq[e] - 8) << 8) : h = V(a, F.ua.Ua.lq[e]);
            for (h += F.ua.Ua.wP[e]; g--;)d = a.OF[a.Zh - h & 32767], ra(a, d)
        } else ra(a, e)
    } while (!c);
    a.Zh = 0;
    a.cj = 1
}
function pa(a) {
    a.xu = [];
    a.Iz = r;
    var c = [];
    c[0] = a.W();
    c[1] = a.W();
    120 == c[0] && 218 == c[1] && (xa(a), a.AA[a.files] = [a.xu.join(""), "geonext.gxt"], a.files++);
    31 == c[0] && 139 == c[1] && (ya(a), a.AA[a.files] = [a.xu.join(""), "file"], a.files++);
    if (80 == c[0] && 75 == c[1] && (a.Iz = p, c[2] = a.W(), c[3] = a.W(), 3 == c[2] && 4 == c[3])) {
        c[0] = a.W();
        c[1] = a.W();
        a.kk = a.W();
        a.kk |= a.W() << 8;
        c = a.W();
        c |= a.W() << 8;
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        var d = a.W(), d = d | a.W() << 8, e = a.W(), e = e | a.W() << 8, f = 0;
        for (a.Lq = []; d--;) {
            var g =
                a.W();
            "/" == g | ":" == g ? f = 0 : f < F.ua.Ua.kJ - 1 && (a.Lq[f++] = String.fromCharCode(g))
        }
        a.WP || (a.WP = a.Lq);
        for (var f = 0; f < e;)a.W(), f++;
        8 == c && (xa(a), a.AA[a.files] = [a.xu.join(""), a.Lq.join("")], a.files++);
        ya(a)
    }
}
function ya(a) {
    var c = [], d;
    a.kk & 8 && (c[0] = a.W(), c[1] = a.W(), c[2] = a.W(), c[3] = a.W(), a.W(), a.W(), a.W(), a.W(), a.W(), a.W(), a.W(), a.W());
    a.Iz && pa(a);
    c[0] = a.W();
    if (8 == c[0]) {
        a.kk = a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        a.W();
        if (a.kk & 4) {
            c[0] = a.W();
            c[2] = a.W();
            a.ki = c[0] + 256 * c[1];
            for (c = 0; c < a.ki; c++)a.W()
        }
        if (a.kk & 8) {
            c = 0;
            for (a.Lq = []; d = a.W();) {
                if ("7" == d || ":" == d)c = 0;
                c < F.ua.Ua.kJ - 1 && (a.Lq[c++] = d)
            }
        }
        if (a.kk & 16)for (; a.W(););
        a.kk & 2 && (a.W(), a.W());
        xa(a);
        a.W();
        a.W();
        a.W();
        a.W();
        a.Iz && pa(a)
    }
};/*
 zlib.js 2012 - imaya [ https://github.com/imaya/zlib.js ] The MIT License */
(function () {
    function a(a) {
        b(a)
    }

    function c(a, c) {
        var d = a.split("."), e = K;
        !(d[0] in e) && e.execScript && e.execScript("var " + d[0]);
        for (var f; d.length && (f = d.shift());)!d.length && c !== L ? e[f] = c : e = e[f] ? e[f] : e[f] = {}
    }

    function d(a) {
        if ("string" === typeof a) {
            a = a.split("");
            var c, d;
            c = 0;
            for (d = a.length; c < d; c++)a[c] = (a[c].charCodeAt(0) & 255) >>> 0
        }
        c = 1;
        d = 0;
        for (var e = a.length, f, g = 0; 0 < e;) {
            f = 1024 < e ? 1024 : e;
            e -= f;
            do c += a[g++], d += c; while (--f);
            c %= 65521;
            d %= 65521
        }
        return (d << 16 | c) >>> 0
    }

    function e(c, d) {
        this.index = "number" === typeof d ? d : 0;
        this.Zt = 0;
        this.buffer = c instanceof (G ? Uint8Array : Array) ? c : new (G ? Uint8Array : Array)(32768);
        2 * this.buffer.length <= this.index && a(Error("invalid index"));
        this.buffer.length <= this.index && this.ci()
    }

    function f(a) {
        this.buffer = new (G ? Uint16Array : Array)(2 * a);
        this.length = 0
    }

    function g(a) {
        var c = a.length, d = 0, e = Number.POSITIVE_INFINITY, f, g, h, m, n, s, t, v, w;
        for (v = 0; v < c; ++v)a[v] > d && (d = a[v]), a[v] < e && (e = a[v]);
        f = 1 << d;
        g = new (G ? Uint32Array : Array)(f);
        h = 1;
        m = 0;
        for (n = 2; h <= d;) {
            for (v = 0; v < c; ++v)if (a[v] === h) {
                s = 0;
                t = m;
                for (w = 0; w < h; ++w)s =
                    s << 1 | t & 1, t >>= 1;
                for (w = s; w < f; w += n)g[w] = h << 16 | v;
                ++m
            }
            ++h;
            m <<= 1;
            n <<= 1
        }
        return [g, d, e]
    }

    function h(a, c) {
        this.Dq = $;
        this.M = 0;
        this.input = a;
        this.b = 0;
        c && (c.v6 && (this.M = c.v6), "number" === typeof c.YF && (this.Dq = c.YF), c.Oz && (this.a = G && c.Oz instanceof Array ? new Uint8Array(c.Oz) : c.Oz), "number" === typeof c.Q6 && (this.b = c.Q6));
        this.a || (this.a = new (G ? Uint8Array : Array)(32768))
    }

    function m(a, c) {
        this.length = a;
        this.BU = c
    }

    function n() {
        var c = Z;
        switch (P) {
            case 3 === c:
                return [257, c - 3, 0];
            case 4 === c:
                return [258, c - 4, 0];
            case 5 === c:
                return [259,
                    c - 5, 0];
            case 6 === c:
                return [260, c - 6, 0];
            case 7 === c:
                return [261, c - 7, 0];
            case 8 === c:
                return [262, c - 8, 0];
            case 9 === c:
                return [263, c - 9, 0];
            case 10 === c:
                return [264, c - 10, 0];
            case 12 >= c:
                return [265, c - 11, 1];
            case 14 >= c:
                return [266, c - 13, 1];
            case 16 >= c:
                return [267, c - 15, 1];
            case 18 >= c:
                return [268, c - 17, 1];
            case 22 >= c:
                return [269, c - 19, 2];
            case 26 >= c:
                return [270, c - 23, 2];
            case 30 >= c:
                return [271, c - 27, 2];
            case 34 >= c:
                return [272, c - 31, 2];
            case 42 >= c:
                return [273, c - 35, 3];
            case 50 >= c:
                return [274, c - 43, 3];
            case 58 >= c:
                return [275, c - 51, 3];
            case 66 >= c:
                return [276,
                    c - 59, 3];
            case 82 >= c:
                return [277, c - 67, 4];
            case 98 >= c:
                return [278, c - 83, 4];
            case 114 >= c:
                return [279, c - 99, 4];
            case 130 >= c:
                return [280, c - 115, 4];
            case 162 >= c:
                return [281, c - 131, 5];
            case 194 >= c:
                return [282, c - 163, 5];
            case 226 >= c:
                return [283, c - 195, 5];
            case 257 >= c:
                return [284, c - 227, 5];
            case 258 === c:
                return [285, c - 258, 0];
            default:
                a("invalid length: " + c)
        }
    }

    function s(c, d) {
        function e(c, d) {
            var f = c.BU, g = [], h = 0, m;
            m = ca[c.length];
            g[h++] = m & 65535;
            g[h++] = m >> 16 & 255;
            g[h++] = m >> 24;
            var n;
            switch (P) {
                case 1 === f:
                    n = [0, f - 1, 0];
                    break;
                case 2 === f:
                    n = [1, f -
                    2, 0];
                    break;
                case 3 === f:
                    n = [2, f - 3, 0];
                    break;
                case 4 === f:
                    n = [3, f - 4, 0];
                    break;
                case 6 >= f:
                    n = [4, f - 5, 1];
                    break;
                case 8 >= f:
                    n = [5, f - 7, 1];
                    break;
                case 12 >= f:
                    n = [6, f - 9, 2];
                    break;
                case 16 >= f:
                    n = [7, f - 13, 2];
                    break;
                case 24 >= f:
                    n = [8, f - 17, 3];
                    break;
                case 32 >= f:
                    n = [9, f - 25, 3];
                    break;
                case 48 >= f:
                    n = [10, f - 33, 4];
                    break;
                case 64 >= f:
                    n = [11, f - 49, 4];
                    break;
                case 96 >= f:
                    n = [12, f - 65, 5];
                    break;
                case 128 >= f:
                    n = [13, f - 97, 5];
                    break;
                case 192 >= f:
                    n = [14, f - 129, 6];
                    break;
                case 256 >= f:
                    n = [15, f - 193, 6];
                    break;
                case 384 >= f:
                    n = [16, f - 257, 7];
                    break;
                case 512 >= f:
                    n = [17, f - 385, 7];
                    break;
                case 768 >= f:
                    n = [18, f - 513, 8];
                    break;
                case 1024 >= f:
                    n = [19, f - 769, 8];
                    break;
                case 1536 >= f:
                    n = [20, f - 1025, 9];
                    break;
                case 2048 >= f:
                    n = [21, f - 1537, 9];
                    break;
                case 3072 >= f:
                    n = [22, f - 2049, 10];
                    break;
                case 4096 >= f:
                    n = [23, f - 3073, 10];
                    break;
                case 6144 >= f:
                    n = [24, f - 4097, 11];
                    break;
                case 8192 >= f:
                    n = [25, f - 6145, 11];
                    break;
                case 12288 >= f:
                    n = [26, f - 8193, 12];
                    break;
                case 16384 >= f:
                    n = [27, f - 12289, 12];
                    break;
                case 24576 >= f:
                    n = [28, f - 16385, 13];
                    break;
                case 32768 >= f:
                    n = [29, f - 24577, 13];
                    break;
                default:
                    a("invalid distance")
            }
            m = n;
            g[h++] = m[0];
            g[h++] = m[1];
            g[h++] = m[2];
            f = 0;
            for (h = g.length; f < h; ++f)w[x++] = g[f];
            B[g[0]]++;
            C[g[3]]++;
            y = c.length + d - 1;
            v = q
        }

        var f, g, h, n, s, t = {}, v, w = G ? new Uint16Array(2 * d.length) : [], x = 0, y = 0, B = new (G ? Uint32Array : Array)(286), C = new (G ? Uint32Array : Array)(30), I = c.M, E;
        if (!G) {
            for (h = 0; 285 >= h;)B[h++] = 0;
            for (h = 0; 29 >= h;)C[h++] = 0
        }
        B[256] = 1;
        f = 0;
        for (g = d.length; f < g; ++f) {
            h = s = 0;
            for (n = 3; h < n && f + h !== g; ++h)s = s << 8 | d[f + h];
            t[s] === L && (t[s] = []);
            h = t[s];
            if (!(0 < y--)) {
                for (; 0 < h.length && 32768 < f - h[0];)h.shift();
                if (f + 3 >= g) {
                    v && e(v, -1);
                    h = 0;
                    for (n = g - f; h < n; ++h)E = d[f + h], w[x++] = E, ++B[E];
                    break
                }
                if (0 < h.length) {
                    s = n = L;
                    var J = 0, H = L, K = L, T = H = L, Q = d.length, K = 0, T = h.length;
                    a:for (; K < T; K++) {
                        n = h[T - K - 1];
                        H = 3;
                        if (3 < J) {
                            for (H = J; 3 < H; H--)if (d[n + H - 1] !== d[f + H - 1])continue a;
                            H = J
                        }
                        for (; 258 > H && f + H < Q && d[n + H] === d[f + H];)++H;
                        H > J && (s = n, J = H);
                        if (258 === H)break
                    }
                    n = new m(J, f - s);
                    v ? v.length < n.length ? (E = d[f - 1], w[x++] = E, ++B[E], e(n, 0)) : e(v, -1) : n.length < I ? v = n : e(n, 0)
                } else v ? e(v, -1) : (E = d[f], w[x++] = E, ++B[E])
            }
            h.push(f)
        }
        w[x++] = 256;
        B[256]++;
        c.ZU = B;
        c.RU = C;
        return G ? w.subarray(0, x) : w
    }

    function t(a, c) {
        function d(a) {
            var c = w[a][x[a]];
            c ===
            t ? (d(a + 1), d(a + 1)) : --v[c];
            ++x[a]
        }

        var e = a.length, g = new f(572), h = new (G ? Uint8Array : Array)(e), m, n, s;
        if (!G)for (n = 0; n < e; n++)h[n] = 0;
        for (n = 0; n < e; ++n)0 < a[n] && g.push(n, a[n]);
        e = Array(g.length / 2);
        m = new (G ? Uint32Array : Array)(g.length / 2);
        if (1 === e.length)return h[g.pop().index] = 1, h;
        n = 0;
        for (s = g.length / 2; n < s; ++n)e[n] = g.pop(), m[n] = e[n].value;
        var t = m.length;
        n = new (G ? Uint16Array : Array)(c);
        var g = new (G ? Uint8Array : Array)(c), v = new (G ? Uint8Array : Array)(t);
        s = Array(c);
        var w = Array(c), x = Array(c), y = (1 << c) - t, B = 1 << c - 1, C, E, I;
        n[c -
        1] = t;
        for (C = 0; C < c; ++C)y < B ? g[C] = 0 : (g[C] = 1, y -= B), y <<= 1, n[c - 2 - C] = (n[c - 1 - C] / 2 | 0) + t;
        n[0] = g[0];
        s[0] = Array(n[0]);
        w[0] = Array(n[0]);
        for (C = 1; C < c; ++C)n[C] > 2 * n[C - 1] + g[C] && (n[C] = 2 * n[C - 1] + g[C]), s[C] = Array(n[C]), w[C] = Array(n[C]);
        for (y = 0; y < t; ++y)v[y] = c;
        for (B = 0; B < n[c - 1]; ++B)s[c - 1][B] = m[B], w[c - 1][B] = B;
        for (y = 0; y < c; ++y)x[y] = 0;
        1 === g[c - 1] && (--v[0], ++x[c - 1]);
        for (C = c - 2; 0 <= C; --C) {
            E = y = 0;
            I = x[C + 1];
            for (B = 0; B < n[C]; B++)E = s[C + 1][I] + s[C + 1][I + 1], E > m[y] ? (s[C][B] = E, w[C][B] = t, I += 2) : (s[C][B] = m[y], w[C][B] = y, ++y);
            x[C] = 0;
            1 === g[C] && d(C)
        }
        m =
            v;
        n = 0;
        for (s = e.length; n < s; ++n)h[e[n].index] = m[n];
        return h
    }

    function v(c) {
        var d = new (G ? Uint16Array : Array)(c.length), e = [], f = [], g = 0, h, m, n;
        h = 0;
        for (m = c.length; h < m; h++)e[c[h]] = (e[c[h]] | 0) + 1;
        h = 1;
        for (m = 16; h <= m; h++)f[h] = g, g += e[h] | 0, g > 1 << h && a("overcommitted"), g <<= 1;
        65536 > g && a("undercommitted");
        h = 0;
        for (m = c.length; h < m; h++) {
            g = f[c[h]];
            f[c[h]] += 1;
            e = d[h] = 0;
            for (n = c[h]; e < n; e++)d[h] = d[h] << 1 | g & 1, g >>>= 1
        }
        return d
    }

    function w(a, c) {
        this.input = a;
        this.a = new (G ? Uint8Array : Array)(32768);
        this.Dq = T.rz;
        var d = {}, e;
        if ((c || !(c = {})) &&
            "number" === typeof c.YF)this.Dq = c.YF;
        for (e in c)d[e] = c[e];
        d.Oz = this.a;
        this.e = new h(this.input, d)
    }

    function x(c, d) {
        this.sz = [];
        this.Cz = 32768;
        this.ih = this.g = this.s = this.Uz = 0;
        this.input = G ? new Uint8Array(c) : c;
        this.HH = r;
        this.Gz = ma;
        this.yI = r;
        if (d || !(d = {}))d.index && (this.s = d.index), d.PF && (this.Cz = d.PF), d.QF && (this.Gz = d.QF), d.Ju && (this.yI = d.Ju);
        switch (this.Gz) {
            case wa:
                this.b = 32768;
                this.a = new (G ? Uint8Array : Array)(32768 + this.Cz + 258);
                break;
            case ma:
                this.b = 0;
                this.a = new (G ? Uint8Array : Array)(this.Cz);
                this.ci = this.QU;
                this.iI = this.GU;
                this.Jz = this.KU;
                break;
            default:
                a(Error("invalid inflate mode"))
        }
    }

    function y(c, d) {
        for (var e = c.g, f = c.ih, g = c.input, h = c.s, m; f < d;)m = g[h++], m === L && a(Error("input buffer is broken")), e |= m << f, f += 8;
        c.g = e >>> d;
        c.ih = f - d;
        c.s = h;
        return e & (1 << d) - 1
    }

    function B(c, d) {
        for (var e = c.g, f = c.ih, g = c.input, h = c.s, m = d[0], n = d[1], s; f < n;)s = g[h++], s === L && a(Error("input buffer is broken")), e |= s << f, f += 8;
        g = m[e & (1 << n) - 1];
        m = g >>> 16;
        c.g = e >> m;
        c.ih = f - m;
        c.s = h;
        return g & 65535
    }

    function I(a) {
        function c(a, d, e) {
            var f, g, h, m;
            for (m = 0; m <
            a;)switch (f = B(this, d), f) {
                case 16:
                    for (h = 3 + y(this, 2); h--;)e[m++] = g;
                    break;
                case 17:
                    for (h = 3 + y(this, 3); h--;)e[m++] = 0;
                    g = 0;
                    break;
                case 18:
                    for (h = 11 + y(this, 7); h--;)e[m++] = 0;
                    g = 0;
                    break;
                default:
                    g = e[m++] = f
            }
            return e
        }

        var d = y(a, 5) + 257, e = y(a, 5) + 1, f = y(a, 4) + 4, h = new (G ? Uint8Array : Array)(Ea.length), m;
        for (m = 0; m < f; ++m)h[Ea[m]] = y(a, 3);
        f = g(h);
        h = new (G ? Uint8Array : Array)(d);
        m = new (G ? Uint8Array : Array)(e);
        a.Jz(g(c.call(a, d, f, h)), g(c.call(a, e, f, m)))
    }

    function E(c, d) {
        var e, f;
        this.input = c;
        this.s = 0;
        if (d || !(d = {}))d.index && (this.s = d.index),
        d.fT && (this.kV = d.fT);
        e = c[this.s++];
        f = c[this.s++];
        switch (e & 15) {
            case ka:
                this.method = ka;
                break;
            default:
                a(Error("unsupported compression method"))
        }
        0 !== ((e << 8) + f) % 31 && a(Error("invalid fcheck flag:" + ((e << 8) + f) % 31));
        f & 32 && a(Error("fdict flag is not supported"));
        this.oI = new x(c, {index: this.s, PF: d.PF, QF: d.QF, Ju: d.Ju})
    }

    var L = k, P = p, K = this, G = "undefined" !== typeof Uint8Array && "undefined" !== typeof Uint16Array && "undefined" !== typeof Uint32Array;
    e.prototype.ci = function () {
        var a = this.buffer, c, d = a.length, e = new (G ? Uint8Array :
            Array)(d << 1);
        if (G)e.set(a); else for (c = 0; c < d; ++c)e[c] = a[c];
        return this.buffer = e
    };
    e.prototype.z = function (a, c, d) {
        var e = this.buffer, f = this.index, g = this.Zt, h = e[f];
        d && 1 < c && (a = 8 < c ? (R[a & 255] << 24 | R[a >>> 8 & 255] << 16 | R[a >>> 16 & 255] << 8 | R[a >>> 24 & 255]) >> 32 - c : R[a] >> 8 - c);
        if (8 > c + g)h = h << c | a, g += c; else for (d = 0; d < c; ++d)h = h << 1 | a >> c - d - 1 & 1, 8 === ++g && (g = 0, e[f++] = R[h], h = 0, f === e.length && (e = this.ci()));
        e[f] = h;
        this.buffer = e;
        this.Zt = g;
        this.index = f
    };
    e.prototype.finish = function () {
        var a = this.buffer, c = this.index, d;
        0 < this.Zt && (a[c] <<=
            8 - this.Zt, a[c] = R[a[c]], c++);
        G ? d = a.subarray(0, c) : (a.length = c, d = a);
        return d
    };
    var J = new (G ? Uint8Array : Array)(256), C;
    for (C = 0; 256 > C; ++C) {
        for (var H = C, Q = H, S = 7, H = H >>> 1; H; H >>>= 1)Q <<= 1, Q |= H & 1, --S;
        J[C] = (Q << S & 255) >>> 0
    }
    var R = J, J = [0, 1996959894, 3993919788, 2567524794, 124634137, 1886057615, 3915621685, 2657392035, 249268274, 2044508324, 3772115230, 2547177864, 162941995, 2125561021, 3887607047, 2428444049, 498536548, 1789927666, 4089016648, 2227061214, 450548861, 1843258603, 4107580753, 2211677639, 325883990, 1684777152, 4251122042, 2321926636,
        335633487, 1661365465, 4195302755, 2366115317, 997073096, 1281953886, 3579855332, 2724688242, 1006888145, 1258607687, 3524101629, 2768942443, 901097722, 1119000684, 3686517206, 2898065728, 853044451, 1172266101, 3705015759, 2882616665, 651767980, 1373503546, 3369554304, 3218104598, 565507253, 1454621731, 3485111705, 3099436303, 671266974, 1594198024, 3322730930, 2970347812, 795835527, 1483230225, 3244367275, 3060149565, 1994146192, 31158534, 2563907772, 4023717930, 1907459465, 112637215, 2680153253, 3904427059, 2013776290, 251722036, 2517215374,
        3775830040, 2137656763, 141376813, 2439277719, 3865271297, 1802195444, 476864866, 2238001368, 4066508878, 1812370925, 453092731, 2181625025, 4111451223, 1706088902, 314042704, 2344532202, 4240017532, 1658658271, 366619977, 2362670323, 4224994405, 1303535960, 984961486, 2747007092, 3569037538, 1256170817, 1037604311, 2765210733, 3554079995, 1131014506, 879679996, 2909243462, 3663771856, 1141124467, 855842277, 2852801631, 3708648649, 1342533948, 654459306, 3188396048, 3373015174, 1466479909, 544179635, 3110523913, 3462522015, 1591671054, 702138776,
        2966460450, 3352799412, 1504918807, 783551873, 3082640443, 3233442989, 3988292384, 2596254646, 62317068, 1957810842, 3939845945, 2647816111, 81470997, 1943803523, 3814918930, 2489596804, 225274430, 2053790376, 3826175755, 2466906013, 167816743, 2097651377, 4027552580, 2265490386, 503444072, 1762050814, 4150417245, 2154129355, 426522225, 1852507879, 4275313526, 2312317920, 282753626, 1742555852, 4189708143, 2394877945, 397917763, 1622183637, 3604390888, 2714866558, 953729732, 1340076626, 3518719985, 2797360999, 1068828381, 1219638859, 3624741850,
        2936675148, 906185462, 1090812512, 3747672003, 2825379669, 829329135, 1181335161, 3412177804, 3160834842, 628085408, 1382605366, 3423369109, 3138078467, 570562233, 1426400815, 3317316542, 2998733608, 733239954, 1555261956, 3268935591, 3050360625, 752459403, 1541320221, 2607071920, 3965973030, 1969922972, 40735498, 2617837225, 3943577151, 1913087877, 83908371, 2512341634, 3803740692, 2075208622, 213261112, 2463272603, 3855990285, 2094854071, 198958881, 2262029012, 4057260610, 1759359992, 534414190, 2176718541, 4139329115, 1873836001, 414664567,
        2282248934, 4279200368, 1711684554, 285281116, 2405801727, 4167216745, 1634467795, 376229701, 2685067896, 3608007406, 1308918612, 956543938, 2808555105, 3495958263, 1231636301, 1047427035, 2932959818, 3654703836, 1088359270, 936918E3, 2847714899, 3736837829, 1202900863, 817233897, 3183342108, 3401237130, 1404277552, 615818150, 3134207493, 3453421203, 1423857449, 601450431, 3009837614, 3294710456, 1567103746, 711928724, 3020668471, 3272380065, 1510334235, 755167117];
    G && new Uint32Array(J);
    f.prototype.getParent = function (a) {
        return 2 * ((a - 2) /
            4 | 0)
    };
    f.prototype.push = function (a, c) {
        var d, e, f = this.buffer, g;
        d = this.length;
        f[this.length++] = c;
        for (f[this.length++] = a; 0 < d;)if (e = this.getParent(d), f[d] > f[e])g = f[d], f[d] = f[e], f[e] = g, g = f[d + 1], f[d + 1] = f[e + 1], f[e + 1] = g, d = e; else break;
        return this.length
    };
    f.prototype.pop = function () {
        var a, c, d = this.buffer, e, f, g;
        c = d[0];
        a = d[1];
        this.length -= 2;
        d[0] = d[this.length];
        d[1] = d[this.length + 1];
        for (g = 0; ;) {
            f = 2 * g + 2;
            if (f >= this.length)break;
            f + 2 < this.length && d[f + 2] > d[f] && (f += 2);
            if (d[f] > d[g])e = d[g], d[g] = d[f], d[f] = e, e = d[g + 1], d[g +
            1] = d[f + 1], d[f + 1] = e; else break;
            g = f
        }
        return {index: a, value: c, length: this.length}
    };
    var $ = 2, J = {NONE: 0, r: 1, rz: $, Taa: 3}, Y = [];
    for (C = 0; 288 > C; C++)switch (P) {
        case 143 >= C:
            Y.push([C + 48, 8]);
            break;
        case 255 >= C:
            Y.push([C - 144 + 400, 9]);
            break;
        case 279 >= C:
            Y.push([C - 256 + 0, 7]);
            break;
        case 287 >= C:
            Y.push([C - 280 + 192, 8]);
            break;
        default:
            a("invalid literal: " + C)
    }
    h.prototype.Rl = function () {
        var c, d, f, g, h = this.input;
        switch (this.Dq) {
            case 0:
                f = 0;
                for (g = h.length; f < g;) {
                    d = G ? h.subarray(f, f + 65535) : h.slice(f, f + 65535);
                    f += d.length;
                    var m = f === g, n =
                        L, w = n = L, w = n = L, x = this.a, y = this.b;
                    if (G) {
                        for (x = new Uint8Array(this.a.buffer); x.length <= y + d.length + 5;)x = new Uint8Array(x.length << 1);
                        x.set(this.a)
                    }
                    n = m ? 1 : 0;
                    x[y++] = n | 0;
                    n = d.length;
                    w = ~n + 65536 & 65535;
                    x[y++] = n & 255;
                    x[y++] = n >>> 8 & 255;
                    x[y++] = w & 255;
                    x[y++] = w >>> 8 & 255;
                    if (G)x.set(d, y), y += d.length, x = x.subarray(0, y); else {
                        n = 0;
                        for (w = d.length; n < w; ++n)x[y++] = d[n];
                        x.length = y
                    }
                    this.b = y;
                    this.a = x
                }
                break;
            case 1:
                f = new e(new Uint8Array(this.a.buffer), this.b);
                f.z(1, 1, P);
                f.z(1, 2, P);
                h = s(this, h);
                d = 0;
                for (m = h.length; d < m; d++)if (g = h[d], e.prototype.z.apply(f,
                        Y[g]), 256 < g)f.z(h[++d], h[++d], P), f.z(h[++d], 5), f.z(h[++d], h[++d], P); else if (256 === g)break;
                this.a = f.finish();
                this.b = this.a.length;
                break;
            case $:
                g = new e(new Uint8Array(this.a), this.b);
                var B, C, E, I = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15], J, H, n = Array(19), O, x = $;
                g.z(1, 1, P);
                g.z(x, 2, P);
                h = s(this, h);
                w = t(this.ZU, 15);
                J = v(w);
                x = t(this.RU, 7);
                y = v(x);
                for (B = 286; 257 < B && 0 === w[B - 1]; B--);
                for (C = 30; 1 < C && 0 === x[C - 1]; C--);
                var K = B, T = C;
                c = new (G ? Uint32Array : Array)(K + T);
                var Q = new (G ? Uint32Array : Array)(316), R, S;
                H = new (G ?
                    Uint8Array : Array)(19);
                for (O = E = 0; O < K; O++)c[E++] = w[O];
                for (O = 0; O < T; O++)c[E++] = x[O];
                if (!G) {
                    O = 0;
                    for (T = H.length; O < T; ++O)H[O] = 0
                }
                O = R = 0;
                for (T = c.length; O < T; O += E) {
                    for (E = 1; O + E < T && c[O + E] === c[O]; ++E);
                    K = E;
                    if (0 === c[O])if (3 > K)for (; 0 < K--;)Q[R++] = 0, H[0]++; else for (; 0 < K;)S = 138 > K ? K : 138, S > K - 3 && S < K && (S = K - 3), 10 >= S ? (Q[R++] = 17, Q[R++] = S - 3, H[17]++) : (Q[R++] = 18, Q[R++] = S - 11, H[18]++), K -= S; else if (Q[R++] = c[O], H[c[O]]++, K--, 3 > K)for (; 0 < K--;)Q[R++] = c[O], H[c[O]]++; else for (; 0 < K;)S = 6 > K ? K : 6, S > K - 3 && S < K && (S = K - 3), Q[R++] = 16, Q[R++] = S - 3, H[16]++,
                        K -= S
                }
                c = G ? Q.subarray(0, R) : Q.slice(0, R);
                H = t(H, 7);
                for (O = 0; 19 > O; O++)n[O] = H[I[O]];
                for (E = 19; 4 < E && 0 === n[E - 1]; E--);
                I = v(H);
                g.z(B - 257, 5, P);
                g.z(C - 1, 5, P);
                g.z(E - 4, 4, P);
                for (O = 0; O < E; O++)g.z(n[O], 3, P);
                O = 0;
                for (n = c.length; O < n; O++)if (d = c[O], g.z(I[d], H[d], P), 16 <= d) {
                    O++;
                    switch (d) {
                        case 16:
                            m = 2;
                            break;
                        case 17:
                            m = 3;
                            break;
                        case 18:
                            m = 7;
                            break;
                        default:
                            a("invalid code: " + d)
                    }
                    g.z(c[O], m, P)
                }
                m = [J, w];
                y = [y, x];
                d = m[0];
                m = m[1];
                x = y[0];
                J = y[1];
                y = 0;
                for (n = h.length; y < n; ++y)if (f = h[y], g.z(d[f], m[f], P), 256 < f)g.z(h[++y], h[++y], P), w = h[++y], g.z(x[w],
                    J[w], P), g.z(h[++y], h[++y], P); else if (256 === f)break;
                this.a = g.finish();
                this.b = this.a.length;
                break;
            default:
                a("invalid compression type")
        }
        return this.a
    };
    C = [];
    var Z;
    for (Z = 3; 258 >= Z; Z++)H = n(), C[Z] = H[2] << 24 | H[1] << 16 | H[0];
    var ca = G ? new Uint32Array(C) : C, T = J;
    w.prototype.Rl = function () {
        var c, e, f, g, h = 0;
        g = this.a;
        c = ka;
        switch (c) {
            case ka:
                e = Math.LOG2E * Math.log(32768) - 8;
                break;
            default:
                a(Error("invalid compression method"))
        }
        e = e << 4 | c;
        g[h++] = e;
        switch (c) {
            case ka:
                switch (this.Dq) {
                    case T.NONE:
                        f = 0;
                        break;
                    case T.r:
                        f = 1;
                        break;
                    case T.rz:
                        f =
                            2;
                        break;
                    default:
                        a(Error("unsupported compression type"))
                }
                break;
            default:
                a(Error("invalid compression method"))
        }
        c = f << 6 | 0;
        g[h++] = c | 31 - (256 * e + c) % 31;
        c = d(this.input);
        this.e.b = h;
        g = this.e.Rl();
        h = g.length;
        G && (g = new Uint8Array(g.buffer), g.length <= h + 4 && (this.a = new Uint8Array(g.length + 4), this.a.set(g), g = this.a), g = g.subarray(0, h + 4));
        g[h++] = c >> 24 & 255;
        g[h++] = c >> 16 & 255;
        g[h++] = c >> 8 & 255;
        g[h++] = c & 255;
        return g
    };
    c("Zlib.Deflate", w);
    c("Zlib.Deflate.compress", function (a, c) {
        return (new w(a, c)).Rl()
    });
    c("Zlib.Deflate.CompressionType",
        T);
    c("Zlib.Deflate.CompressionType.NONE", T.NONE);
    c("Zlib.Deflate.CompressionType.FIXED", T.r);
    c("Zlib.Deflate.CompressionType.DYNAMIC", T.rz);
    var wa = 0, ma = 1, J = {VT: wa, ET: ma};
    x.prototype.d = function () {
        for (; !this.HH;) {
            var c = y(this, 3);
            c & 1 && (this.HH = P);
            c >>>= 1;
            switch (c) {
                case 0:
                    var c = this.input, d = this.s, e = this.a, f = this.b, g = L, h = L, m = L, n = e.length, g = L;
                    this.ih = this.g = 0;
                    g = c[d++];
                    g === L && a(Error("invalid uncompressed block header: LEN (first byte)"));
                    h = g;
                    g = c[d++];
                    g === L && a(Error("invalid uncompressed block header: LEN (second byte)"));
                    h |= g << 8;
                    g = c[d++];
                    g === L && a(Error("invalid uncompressed block header: NLEN (first byte)"));
                    m = g;
                    g = c[d++];
                    g === L && a(Error("invalid uncompressed block header: NLEN (second byte)"));
                    m |= g << 8;
                    h === ~m && a(Error("invalid uncompressed block header: length verify"));
                    d + h > c.length && a(Error("input buffer is broken"));
                    switch (this.Gz) {
                        case wa:
                            for (; f + h > e.length;) {
                                g = n - f;
                                h -= g;
                                if (G)e.set(c.subarray(d, d + g), f), f += g, d += g; else for (; g--;)e[f++] = c[d++];
                                this.b = f;
                                e = this.ci();
                                f = this.b
                            }
                            break;
                        case ma:
                            for (; f + h > e.length;)e = this.ci({qa: 2});
                            break;
                        default:
                            a(Error("invalid inflate mode"))
                    }
                    if (G)e.set(c.subarray(d, d + h), f), f += h, d += h; else for (; h--;)e[f++] = c[d++];
                    this.s = d;
                    this.b = f;
                    this.a = e;
                    break;
                case 1:
                    this.Jz(La, Ma);
                    break;
                case 2:
                    I(this);
                    break;
                default:
                    a(Error("unknown BTYPE: " + c))
            }
        }
        return this.iI()
    };
    C = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];
    var Ea = G ? new Uint16Array(C) : C;
    C = [3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 258, 258];
    var Fa = G ? new Uint16Array(C) : C;
    C = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2,
        3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0, 0, 0];
    var na = G ? new Uint8Array(C) : C;
    C = [1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577];
    var Ga = G ? new Uint16Array(C) : C;
    C = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13];
    var oa = G ? new Uint8Array(C) : C;
    C = new (G ? Uint8Array : Array)(288);
    H = 0;
    for (Q = C.length; H < Q; ++H)C[H] = 143 >= H ? 8 : 255 >= H ? 9 : 279 >= H ? 7 : 8;
    var La = g(C);
    C = new (G ? Uint8Array : Array)(30);
    H = 0;
    for (Q = C.length; H < Q; ++H)C[H] = 5;
    var Ma = g(C);
    x.prototype.Jz =
        function (a, c) {
            var d = this.a, e = this.b;
            this.pa = a;
            for (var f = d.length - 258, g, h, m; 256 !== (g = B(this, a));)if (256 > g)e >= f && (this.b = e, d = this.ci(), e = this.b), d[e++] = g; else {
                g -= 257;
                m = Fa[g];
                0 < na[g] && (m += y(this, na[g]));
                g = B(this, c);
                h = Ga[g];
                0 < oa[g] && (h += y(this, oa[g]));
                for (e >= f && (this.b = e, d = this.ci(), e = this.b); m--;)d[e] = d[e++ - h]
            }
            for (; 8 <= this.ih;)this.ih -= 8, this.s--;
            this.b = e
        };
    x.prototype.KU = function (a, c) {
        var d = this.a, e = this.b;
        this.pa = a;
        for (var f = d.length, g, h, m; 256 !== (g = B(this, a));)if (256 > g)e >= f && (d = this.ci(), f = d.length),
            d[e++] = g; else {
            g -= 257;
            m = Fa[g];
            0 < na[g] && (m += y(this, na[g]));
            g = B(this, c);
            h = Ga[g];
            0 < oa[g] && (h += y(this, oa[g]));
            for (e + m > f && (d = this.ci(), f = d.length); m--;)d[e] = d[e++ - h]
        }
        for (; 8 <= this.ih;)this.ih -= 8, this.s--;
        this.b = e
    };
    x.prototype.ci = function () {
        var a = new (G ? Uint8Array : Array)(this.b - 32768), c = this.b - 32768, d, e, f = this.a;
        if (G)a.set(f.subarray(32768, a.length)); else {
            d = 0;
            for (e = a.length; d < e; ++d)a[d] = f[d + 32768]
        }
        this.sz.push(a);
        this.Uz += a.length;
        if (G)f.set(f.subarray(c, c + 32768)); else for (d = 0; 32768 > d; ++d)f[d] = f[c + d];
        this.b =
            32768;
        return f
    };
    x.prototype.QU = function (a) {
        var c, d = this.input.length / this.s + 1 | 0, e, f, g, h = this.input, m = this.a;
        a && ("number" === typeof a.qa && (d = a.qa), "number" === typeof a.yU && (d += a.yU));
        2 > d ? (e = (h.length - this.s) / this.pa[2], g = 258 * (e / 2) | 0, f = g < m.length ? m.length + g : m.length << 1) : f = m.length * d;
        G ? (c = new Uint8Array(f), c.set(m)) : c = m;
        return this.a = c
    };
    x.prototype.iI = function () {
        var a = 0, c = this.a, d = this.sz, e, f = new (G ? Uint8Array : Array)(this.Uz + (this.b - 32768)), g, h, m, n;
        if (0 === d.length)return G ? this.a.subarray(32768, this.b) :
            this.a.slice(32768, this.b);
        g = 0;
        for (h = d.length; g < h; ++g) {
            e = d[g];
            m = 0;
            for (n = e.length; m < n; ++m)f[a++] = e[m]
        }
        g = 32768;
        for (h = this.b; g < h; ++g)f[a++] = c[g];
        this.sz = [];
        return this.buffer = f
    };
    x.prototype.GU = function () {
        var a, c = this.b;
        G ? this.yI ? (a = new Uint8Array(c), a.set(this.a.subarray(0, c))) : a = this.a.subarray(0, c) : (this.a.length > c && (this.a.length = c), a = this.a);
        return this.buffer = a
    };
    E.prototype.d = function () {
        var c = this.input, e, f;
        e = this.oI.d();
        this.s = this.oI.s;
        this.kV && (f = (c[this.s++] << 24 | c[this.s++] << 16 | c[this.s++] <<
            8 | c[this.s++]) >>> 0, f !== d(e) && a(Error("invalid adler-32 checksum")));
        return e
    };
    c("Zlib.Inflate", E);
    c("Zlib.Inflate.BufferType", J);
    J.f$ = J.ET;
    J.z$ = J.VT;
    c("Zlib.Inflate.prototype.decompress", E.prototype.d);
    J = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];
    G && new Uint16Array(J);
    J = [3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 258, 258];
    G && new Uint16Array(J);
    J = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0, 0, 0];
    G && new Uint8Array(J);
    J = [1, 2, 3, 4, 5, 7, 9, 13,
        17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577];
    G && new Uint16Array(J);
    J = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13];
    G && new Uint8Array(J);
    J = new (G ? Uint8Array : Array)(288);
    C = 0;
    for (H = J.length; C < H; ++C)J[C] = 143 >= C ? 8 : 255 >= C ? 9 : 279 >= C ? 7 : 8;
    g(J);
    J = new (G ? Uint8Array : Array)(30);
    C = 0;
    for (H = J.length; C < H; ++C)J[C] = 5;
    g(J);
    var ka = 8
}).call(this);
M = window;
M = M.Wba = M.Zlib;
M.MI = M.Deflate;
M.MI.Wda = M.MI.compress;
M.to = M.Inflate;
M.to.N$ = M.to.BufferType;
M.to.prototype.CP = M.to.prototype.decompress;
F.LV = F.za.extend({
    ctor: function (a) {
        var c, d, e, f;
        this.data = a;
        this.nb = 8;
        this.vH = [];
        this.jz = [];
        this.ho = {};
        this.KF = q;
        this.text = {};
        for (e = q; ;) {
            c = this.vk();
            f = a = k;
            f = [];
            for (a = 0; 4 > a; ++a)f.push(String.fromCharCode(this.data[this.nb++]));
            a = f.join("");
            switch (a) {
                case "IHDR":
                    this.width = this.vk();
                    this.height = this.vk();
                    this.eq = this.data[this.nb++];
                    this.WF = this.data[this.nb++];
                    this.Xda = this.data[this.nb++];
                    this.sfa = this.data[this.nb++];
                    this.via = this.data[this.nb++];
                    break;
                case "acTL":
                    this.KF = {
                        jla: this.vk(), kla: this.vk() ||
                        Infinity, frames: []
                    };
                    break;
                case "PLTE":
                    this.vH = this.Du(c);
                    break;
                case "fcTL":
                    e && this.KF.frames.push(e);
                    this.nb += 4;
                    e = {width: this.vk(), height: this.vk(), kT: this.vk(), lT: this.vk()};
                    a = this.IR();
                    c = this.IR() || 100;
                    e.jea = 1E3 * a / c;
                    e.wea = this.data[this.nb++];
                    e.Hda = this.data[this.nb++];
                    e.data = [];
                    break;
                case "IDAT":
                case "fdAT":
                    "fdAT" === a && (this.nb += 4, c -= 4);
                    a = (e != q ? e.data : k) || this.jz;
                    for (f = 0; 0 <= c ? f < c : f > c; 0 <= c ? ++f : --f)a.push(this.data[this.nb++]);
                    break;
                case "tRNS":
                    this.ho = {};
                    switch (this.WF) {
                        case 3:
                            this.ho.TG = this.Du(c);
                            c = 255 - this.ho.TG.length;
                            if (0 < c)for (a = 0; 0 <= c ? a < c : a > c; 0 <= c ? ++a : --a)this.ho.TG.push(255);
                            break;
                        case 0:
                            this.ho.mia = this.Du(c)[0];
                            break;
                        case 2:
                            this.ho.Lma = this.Du(c)
                    }
                    break;
                case "tEXt":
                    f = this.Du(c);
                    c = f.indexOf(0);
                    a = String.fromCharCode.apply(String, f.slice(0, c));
                    this.text[a] = String.fromCharCode.apply(String, f.slice(c + 1));
                    break;
                case "IEND":
                    e && this.KF.frames.push(e);
                    a:{
                        switch (this.WF) {
                            case 0:
                            case 3:
                            case 4:
                                e = 1;
                                break a;
                            case 2:
                            case 6:
                                e = 3;
                                break a
                        }
                        e = k
                    }
                    this.A = e;
                    this.HQ = 4 === (d = this.WF) || 6 === d;
                    d = this.A + (this.HQ ? 1 :
                            0);
                    this.b7 = this.eq * d;
                    a:{
                        switch (this.A) {
                            case 1:
                                d = "DeviceGray";
                                break a;
                            case 3:
                                d = "DeviceRGB";
                                break a
                        }
                        d = k
                    }
                    this.Tda = d;
                    Uint8Array != Array && (this.jz = new Uint8Array(this.jz));
                    return;
                default:
                    this.nb += c
            }
            this.nb += 4;
            this.nb > this.data.length && b(Error("Incomplete or corrupt PNG file"))
        }
    }, Du: function (a) {
        var c, d;
        d = [];
        for (c = 0; 0 <= a ? c < a : c > a; 0 <= a ? ++c : --c)d.push(this.data[this.nb++]);
        return d
    }, vk: function () {
        var a, c, d, e;
        a = this.data[this.nb++] << 24;
        c = this.data[this.nb++] << 16;
        d = this.data[this.nb++] << 8;
        e = this.data[this.nb++];
        return a | c | d | e
    }, IR: function () {
        var a, c;
        a = this.data[this.nb++] << 8;
        c = this.data[this.nb++];
        return a | c
    }, U2: function (a) {
        var c, d, e, f, g, h, m, n, s, t, v, w, x, y, B;
        a == q && (a = this.jz);
        if (0 === a.length)return new Uint8Array(0);
        a = (new Zlib.to(a, {index: 0, fT: r})).CP();
        n = this.b7 / 8;
        w = n * this.width;
        s = new Uint8Array(w * this.height);
        h = a.length;
        for (d = t = v = 0; t < h;) {
            switch (a[t++]) {
                case 0:
                    for (c = 0; c < w; c += 1)s[d++] = a[t++];
                    break;
                case 1:
                    for (f = x = 0; x < w; f = x += 1)c = a[t++], g = f < n ? 0 : s[d - n], s[d++] = (c + g) % 256;
                    break;
                case 2:
                    for (f = g = 0; g < w; f = g += 1)c = a[t++],
                        e = (f - f % n) / n, x = v && s[(v - 1) * w + e * n + f % n], s[d++] = (x + c) % 256;
                    break;
                case 3:
                    for (f = B = 0; B < w; f = B += 1)c = a[t++], e = (f - f % n) / n, g = f < n ? 0 : s[d - n], x = v && s[(v - 1) * w + e * n + f % n], s[d++] = (c + Math.floor((g + x) / 2)) % 256;
                    break;
                case 4:
                    for (f = B = 0; B < w; f = B += 1)c = a[t++], e = (f - f % n) / n, g = f < n ? 0 : s[d - n], 0 === v ? x = y = 0 : (x = s[(v - 1) * w + e * n + f % n], y = e && s[(v - 1) * w + (e - 1) * n + f % n]), m = g + x - y, f = Math.abs(m - g), e = Math.abs(m - x), m = Math.abs(m - y), g = f <= e && f <= m ? g : e <= m ? x : y, s[d++] = (c + g) % 256;
                    break;
                default:
                    b(Error("Invalid filter algorithm: " + a[t - 1]))
            }
            v++
        }
        return s
    }, S2: function (a, c) {
        var d,
            e, f, g, h, m, n, s;
        e = this.A;
        s = q;
        d = this.HQ;
        this.vH.length && (s = (f = this.fZ) != q ? f : this.fZ = this.T2(), e = 4, d = p);
        f = a.data || a;
        n = f.length;
        h = s || c;
        g = m = 0;
        if (1 === e)for (; g < n;)e = s ? 4 * c[g / 4] : m, m = h[e++], f[g++] = m, f[g++] = m, f[g++] = m, f[g++] = d ? h[e++] : 255, m = e; else for (; g < n;)e = s ? 4 * c[g / 4] : m, f[g++] = h[e++], f[g++] = h[e++], f[g++] = h[e++], f[g++] = d ? h[e++] : 255, m = e
    }, T2: function () {
        var a, c, d, e, f, g, h, m, n;
        d = this.vH;
        g = this.ho.TG || [];
        f = new Uint8Array((g.length || 0) + d.length);
        c = h = a = e = 0;
        for (m = d.length; h < m; c = h += 3)f[e++] = d[c], f[e++] = d[c + 1], f[e++] = d[c +
        2], f[e++] = (n = g[a++]) != q ? n : 255;
        return f
    }, D7: function (a) {
        var c;
        a.width = this.width;
        a.height = this.height;
        a = a.getContext("2d");
        c = a.createImageData(this.width, this.height);
        this.S2(c, this.U2());
        return a.putImageData(c, 0, 0)
    }
});
F.H9 = {
    CN: r,
    mn: q,
    fD: [],
    getUint8: function (a) {
        return this.mn[a]
    },
    getUint16: function (a) {
        return this.CN ? this.mn[a + 1] << 8 | this.mn[a] : this.mn[a] << 8 | this.mn[a + 1]
    },
    getUint32: function (a) {
        var c = this.mn;
        return this.CN ? c[a + 3] << 24 | c[a + 2] << 16 | c[a + 1] << 8 | c[a] : c[a] << 24 | c[a + 1] << 16 | c[a + 2] << 8 | c[a + 3]
    },
    J2: function () {
        var a = this.getUint16(0);
        18761 === a ? this.qH = p : 19789 === a ? this.qH = r : (console.log(a), b(TypeError("Invalid byte order value.")));
        return this.qH
    },
    n5: function () {
        42 !== this.getUint16(2) && b(RangeError("You forgot your towel!"));
        return p
    },
    h4: function (a) {
        var c = this.F3;
        return a in c ? c[a] : q
    },
    f4: function (a) {
        var c = this.E3;
        if (a in c)return c[a];
        console.log("Unknown Field Tag:", a);
        return "Tag" + a
    },
    g4: function (a) {
        return -1 !== ["BYTE", "ASCII", "SBYTE", "UNDEFINED"].indexOf(a) ? 1 : -1 !== ["SHORT", "SSHORT"].indexOf(a) ? 2 : -1 !== ["LONG", "SLONG", "FLOAT"].indexOf(a) ? 4 : -1 !== ["RATIONAL", "SRATIONAL", "DOUBLE"].indexOf(a) ? 8 : q
    },
    i4: function (a, c, d, e) {
        a = [];
        var f = this.g4(c);
        if (4 >= f * d)this.qH === r ? a.push(e >>> 8 * (4 - f)) : a.push(e); else for (var g = 0; g < d; g++) {
            var h =
                f * g;
            8 <= f ? -1 !== ["RATIONAL", "SRATIONAL"].indexOf(c) ? (a.push(this.getUint32(e + h)), a.push(this.getUint32(e + h + 4))) : F.log("Can't handle this field type or size") : a.push(this.cQ(f, e + h))
        }
        "ASCII" === c && a.forEach(function (a, c, d) {
            d[c] = String.fromCharCode(a)
        });
        return a
    },
    cQ: function (a, c) {
        if (0 >= a)F.log("No bytes requested"); else {
            if (1 >= a)return this.getUint8(c);
            if (2 >= a)return this.getUint16(c);
            if (3 >= a)return this.getUint32(c) >>> 8;
            if (4 >= a)return this.getUint32(c);
            F.log("Too many bytes requested")
        }
    },
    P3: function (a,
                  c, d) {
        d = d || 0;
        c += Math.floor(d / 8);
        var e = d + a;
        a = 32 - a;
        var f, g;
        0 >= e ? console.log("No bits requested") : 8 >= e ? (f = 24 + d, g = this.getUint8(c)) : 16 >= e ? (f = 16 + d, g = this.getUint16(c)) : 32 >= e ? (f = d, g = this.getUint32(c)) : console.log("Too many bits requested");
        return {bits: g << f >>> a, byteOffset: c + Math.floor(e / 8), bitOffset: e % 8}
    },
    DR: function (a) {
        var c = this.getUint16(a), d = [];
        a += 2;
        for (var e = 0; e < c; a += 12, e++) {
            var f = this.getUint16(a), g = this.getUint16(a + 2), h = this.getUint32(a + 4), m = this.getUint32(a + 8), f = this.f4(f), g = this.h4(g), h = this.i4(f,
                g, h, m);
            d[f] = {type: g, ri: h}
        }
        this.fD.push(d);
        c = this.getUint32(a);
        0 !== c && this.DR(c)
    },
    Cn: function (a, c) {
        var d = Math.pow(2, 8 - c);
        return Math.floor(a * d + (d - 1))
    },
    W6: function (a, c) {
        c = c || F.bc("canvas");
        this.mn = a;
        this.canvas = c;
        this.J2();
        if (this.n5()) {
            var d = this.getUint32(4);
            this.fD.length = 0;
            this.DR(d);
            var e = this.fD[0], d = e.ImageWidth.ri[0], f = e.ImageLength.ri[0];
            this.canvas.width = d;
            this.canvas.height = f;
            var g = [], h = e.Compression ? e.Compression.ri[0] : 1, m = e.SamplesPerPixel.ri[0], n = [], s = 0, t = r;
            e.BitsPerSample.ri.forEach(function (a,
                                                 c) {
                n[c] = {fq: a, fz: r, It: k};
                0 === a % 8 && (n[c].fz = p, n[c].It = a / 8);
                s += a
            }, this);
            if (0 === s % 8)var t = p, v = s / 8;
            var w = e.StripOffsets.ri, x = w.length;
            if (e.StripByteCounts)var y = e.StripByteCounts.ri; else F.log("Missing StripByteCounts!"), 1 === x ? y = [Math.ceil(d * f * s / 8)] : b(Error("Cannot recover from missing StripByteCounts"));
            for (var B = 0; B < x; B++) {
                var I = w[B];
                g[B] = [];
                for (var E = y[B], L = 0, P = 0, K = 1, G = p, J = [], C = 0, H = 0, Q = 0; L < E; L += K)switch (h) {
                    case 1:
                        K = 0;
                        for (J = []; K < m; K++)n[K].fz ? J.push(this.cQ(n[K].It, I + L + n[K].It * K)) : (P = this.P3(n[K].fq,
                            I + L, P), J.push(P.eq), L = P.byteOffset - I, P = P.Fda, b(RangeError("Cannot handle sub-byte bits per sample")));
                        g[B].push(J);
                        t ? K = v : (K = 0, b(RangeError("Cannot handle sub-byte bits per pixel")));
                        break;
                    case 32773:
                        if (G) {
                            var G = r, S = 1, R = 1, K = this.getInt8(I + L);
                            0 <= K && 127 >= K ? S = K + 1 : -127 <= K && -1 >= K ? R = -K + 1 : G = p
                        } else {
                            for (var $ = this.getUint8(I + L), K = 0; K < R; K++)n[H].fz ? (Q = Q << 8 * C | $, C++, C === n[H].It && (J.push(Q), Q = C = 0, H++)) : b(RangeError("Cannot handle sub-byte bits per sample")), H === m && (g[B].push(J), J = [], H = 0);
                            S--;
                            0 === S && (G = p)
                        }
                        K = 1
                }
            }
            if (c.getContext) {
                v =
                    this.canvas.getContext("2d");
                v.fillStyle = "rgba(255, 255, 255, 0)";
                B = e.RowsPerStrip ? e.RowsPerStrip.ri[0] : f;
                y = g.length;
                f %= B;
                f = 0 === f ? B : f;
                S = B;
                t = 0;
                R = e.PhotometricInterpretation.ri[0];
                h = [];
                m = 0;
                e.ExtraSamples && (h = e.ExtraSamples.ri, m = h.length);
                if (e.ColorMap)var Y = e.ColorMap.ri, Z = Math.pow(2, n[0].fq);
                for (B = 0; B < y; B++) {
                    B + 1 === y && (S = f);
                    e = g[B].length;
                    t *= B;
                    for (x = w = 0; w < S, x < e; w++)for (I = 0; I < d; I++, x++) {
                        J = g[B][x];
                        C = L = E = 0;
                        G = 1;
                        if (0 < m)for (H = 0; H < m; H++)if (1 === h[H] || 2 === h[H]) {
                            G = J[3 + H] / 256;
                            break
                        }
                        switch (R) {
                            case 0:
                                if (n[0].fz)var ca =
                                    Math.pow(16, 2 * n[0].It);
                                J.forEach(function (a, c, d) {
                                    d[c] = ca - a
                                });
                            case 1:
                                E = L = C = this.Cn(J[0], n[0].fq);
                                break;
                            case 2:
                                E = this.Cn(J[0], n[0].fq);
                                L = this.Cn(J[1], n[1].fq);
                                C = this.Cn(J[2], n[2].fq);
                                break;
                            case 3:
                                Y === k && b(Error("Palette image missing color map"));
                                J = J[0];
                                E = this.Cn(Y[J], 16);
                                L = this.Cn(Y[Z + J], 16);
                                C = this.Cn(Y[2 * Z + J], 16);
                                break;
                            default:
                                b(RangeError("Unknown Photometric Interpretation:", R))
                        }
                        v.fillStyle = "rgba(" + E + ", " + L + ", " + C + ", " + G + ")";
                        v.fillRect(I, t + w, 1, 1)
                    }
                    t = S
                }
            }
            return this.canvas
        }
    },
    E3: {
        315: "Artist",
        258: "BitsPerSample",
        265: "CellLength",
        264: "CellWidth",
        320: "ColorMap",
        259: "Compression",
        33432: "Copyright",
        306: "DateTime",
        338: "ExtraSamples",
        266: "FillOrder",
        289: "FreeByteCounts",
        288: "FreeOffsets",
        291: "GrayResponseCurve",
        290: "GrayResponseUnit",
        316: "HostComputer",
        270: "ImageDescription",
        257: "ImageLength",
        256: "ImageWidth",
        271: "Make",
        281: "MaxSampleValue",
        280: "MinSampleValue",
        272: "Model",
        254: "NewSubfileType",
        274: "Orientation",
        262: "PhotometricInterpretation",
        284: "PlanarConfiguration",
        296: "ResolutionUnit",
        278: "RowsPerStrip",
        277: "SamplesPerPixel",
        305: "Software",
        279: "StripByteCounts",
        273: "StripOffsets",
        255: "SubfileType",
        263: "Threshholding",
        282: "XResolution",
        283: "YResolution",
        326: "BadFaxLines",
        327: "CleanFaxData",
        343: "ClipPath",
        328: "ConsecutiveBadFaxLines",
        433: "Decode",
        434: "DefaultImageColor",
        269: "DocumentName",
        336: "DotRange",
        321: "HalftoneHints",
        346: "Indexed",
        347: "JPEGTables",
        285: "PageName",
        297: "PageNumber",
        317: "Predictor",
        319: "PrimaryChromaticities",
        532: "ReferenceBlackWhite",
        339: "SampleFormat",
        559: "StripRowCounts",
        330: "SubIFDs",
        292: "T4Options",
        293: "T6Options",
        325: "TileByteCounts",
        323: "TileLength",
        324: "TileOffsets",
        322: "TileWidth",
        301: "TransferFunction",
        318: "WhitePoint",
        344: "XClipPathUnits",
        286: "XPosition",
        529: "YCbCrCoefficients",
        531: "YCbCrPositioning",
        530: "YCbCrSubSampling",
        345: "YClipPathUnits",
        287: "YPosition",
        37378: "ApertureValue",
        40961: "ColorSpace",
        36868: "DateTimeDigitized",
        36867: "DateTimeOriginal",
        34665: "Exif IFD",
        36864: "ExifVersion",
        33434: "ExposureTime",
        41728: "FileSource",
        37385: "Flash",
        40960: "FlashpixVersion",
        33437: "FNumber",
        42016: "ImageUniqueID",
        37384: "LightSource",
        37500: "MakerNote",
        37377: "ShutterSpeedValue",
        37510: "UserComment",
        33723: "IPTC",
        34675: "ICC Profile",
        700: "XMP",
        42112: "GDAL_METADATA",
        42113: "GDAL_NODATA",
        34377: "Photoshop"
    },
    F3: {
        1: "BYTE",
        2: "ASCII",
        3: "SHORT",
        4: "LONG",
        5: "RATIONAL",
        6: "SBYTE",
        7: "UNDEFINED",
        8: "SSHORT",
        9: "SLONG",
        10: "SRATIONAL",
        11: "FLOAT",
        12: "DOUBLE"
    }
};
F.Hg = function (a, c, d, e, f, g, h, m, n, s, t, v) {
    this.nb = a ? a : F.d(0, 0);
    this.tA = c ? c : F.d(0, 0);
    this.color = d ? d : {r: 0, g: 0, b: 0, a: 255};
    this.Ot = e ? e : {r: 0, g: 0, b: 0, a: 255};
    this.size = f || 0;
    this.dG = g || 0;
    this.rotation = h || 0;
    this.IP = m || 0;
    this.em = n || 0;
    this.atlasIndex = s || 0;
    this.lc = t ? t : new F.Hg.nB;
    this.he = v ? v : new F.Hg.oB;
    this.bH = r;
    this.hk = F.d(0, 0)
};
F.Hg.nB = function (a, c, d) {
    this.dir = a ? a : F.d(0, 0);
    this.radialAccel = c || 0;
    this.tangentialAccel = d || 0
};
F.Hg.oB = function (a, c, d, e) {
    this.gh = a || 0;
    this.EP = c || 0;
    this.Cu = d || 0;
    this.HP = e || 0
};
F.Hg.lw = [F.d(), F.d(), F.d(), F.d()];
F.o = F.n.extend({
    cE: "",
    yb: 0,
    aM: r,
    lc: q,
    he: q,
    Mb: "ParticleSystem",
    iE: F.d(0, 0),
    Pj: q,
    Im: 0,
    af: 0,
    ca: q,
    atlasIndex: 0,
    on: r,
    Dw: 0,
    Ny: q,
    sA: q,
    rp: r,
    Ed: 0,
    duration: 0,
    Gp: q,
    nl: q,
    mf: 0,
    Jq: 0,
    gh: 0,
    bq: 0,
    dr: 0,
    er: 0,
    In: 0,
    nq: 0,
    ul: q,
    vl: q,
    Ze: q,
    Zk: q,
    fr: 0,
    gr: 0,
    oq: 0,
    pq: 0,
    Hn: 0,
    Bb: 0,
    G: q,
    q: q,
    qb: r,
    mj: q,
    Ht: r,
    Ta: 0,
    Xc: q,
    zb: q,
    oc: q,
    Xm: q,
    Ca: q,
    zd: q,
    ctor: function (a) {
        F.n.prototype.ctor.call(this);
        this.Ta = F.o.ub;
        this.lc = new F.o.nB;
        this.he = new F.o.oB;
        this.q = {src: F.Ac, J: F.zc};
        this.Pj = [];
        this.Gp = F.d(0, 0);
        this.nl = F.d(0, 0);
        this.ul = F.color(255, 255, 255,
            255);
        this.vl = F.color(255, 255, 255, 255);
        this.Ze = F.color(255, 255, 255, 255);
        this.Zk = F.color(255, 255, 255, 255);
        this.cE = "";
        this.yb = 0;
        this.aM = r;
        this.iE = F.d(0, 0);
        this.af = this.Im = 0;
        this.ca = q;
        this.atlasIndex = 0;
        this.on = r;
        this.Dw = 0;
        this.Ny = F.o.VJ;
        this.sA = F.o.zI;
        this.rp = r;
        this.Bb = this.Hn = this.pq = this.oq = this.gr = this.fr = this.nq = this.In = this.er = this.dr = this.bq = this.gh = this.Jq = this.mf = this.duration = this.Ed = 0;
        this.G = q;
        this.qb = r;
        this.mj = F.o.as;
        this.Ht = r;
        this.oc = [0, 0];
        this.Xc = [];
        this.zb = [];
        this.Xm = F.rect(0, 0, 0, 0);
        this.Ca =
            p;
        F.B === F.Y && (this.zd = q);
        !a || F.du(a) ? (a = a || 100, this.h8(F.o.UB), this.kc(a)) : a && this.Nl(a)
    },
    de: function () {
        this.Z = F.B === F.Aa ? new F.EJ(this) : new F.FJ(this)
    },
    UG: function () {
        for (var a = this.zb, c = 0, d = this.Bb; c < d; ++c) {
            var e = 6 * c, f = 4 * c;
            a[e + 0] = f + 0;
            a[e + 1] = f + 1;
            a[e + 2] = f + 2;
            a[e + 5] = f + 1;
            a[e + 4] = f + 2;
            a[e + 3] = f + 3
        }
    },
    KQ: function (a) {
        var c = F.Fb(), d = F.rect(a.x * c, a.y * c, a.width * c, a.height * c), e = a.width, f = a.height;
        this.G && (e = this.G.pixelsWidth, f = this.G.pixelsHeight);
        if (F.B !== F.Aa) {
            F.ro ? (a = (2 * d.x + 1) / (2 * e), c = (2 * d.y + 1) / (2 * f), e = a + (2 *
                d.width - 2) / (2 * e), d = c + (2 * d.height - 2) / (2 * f)) : (a = d.x / e, c = d.y / f, e = a + d.width / e, d = c + d.height / f);
            var f = d, d = c, c = f, g = 0, h = 0;
            this.ca ? (f = this.ca.textureAtlas.quads, g = this.atlasIndex, h = this.atlasIndex + this.Bb) : (f = this.Xc, g = 0, h = this.Bb);
            for (; g < h; g++) {
                f[g] || (f[g] = F.bL());
                var m = f[g];
                m.K.p.pa = a;
                m.K.p.qa = c;
                m.V.p.pa = e;
                m.V.p.qa = c;
                m.U.p.pa = a;
                m.U.p.qa = d;
                m.O.p.pa = e;
                m.O.p.qa = d
            }
        }
    },
    vG: A("ca"),
    $l: function (a) {
        if (this.ca != a) {
            var c = this.ca;
            if (this.ca = a)for (var d = this.Pj, e = 0; e < this.Bb; e++)d[e].atlasIndex = e;
            a ? c || (this.ca.textureAtlas.aZ(this.Xc,
                this.atlasIndex), F.l.deleteBuffer(this.oc[1])) : (this.tL(), this.UG(), this.ib(c.Ma()), this.gn())
        }
    },
    wq: A("atlasIndex"),
    LH: z("atlasIndex"),
    cga: A("Ny"),
    h8: function (a) {
        this.Ny = a;
        this.Z && (this.Z.oca = a)
    },
    xha: A("sA"),
    uoa: function (a) {
        this.sA = a;
        this.Z && (this.Z.Wca = a)
    },
    mk: A("rp"),
    dha: A("Ed"),
    ioa: z("Ed"),
    xq: A("duration"),
    sg: z("duration"),
    V4: function () {
        return {x: this.Gp.x, y: this.Gp.y}
    },
    K8: z("Gp"),
    FG: function () {
        return {x: this.nl.x, y: this.nl.y}
    },
    wh: z("nl"),
    Iga: A("mf"),
    uh: z("mf"),
    Jga: A("Jq"),
    vh: z("Jq"),
    Bfa: A("gh"),
    ph: z("gh"),
    Cfa: A("bq"),
    qh: z("bq"),
    jQ: function () {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.getGravity() : Particle Mode should be Gravity");
        var a = this.lc.gravity;
        return F.d(a.x, a.y)
    },
    ug: function (a) {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.setGravity() : Particle Mode should be Gravity");
        this.lc.gravity = a
    },
    Kl: function () {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.getSpeed() : Particle Mode should be Gravity");
        return this.lc.speed
    },
    Xd: function (a) {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.setSpeed() : Particle Mode should be Gravity");
        this.lc.speed = a
    },
    vQ: function () {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.getSpeedVar() : Particle Mode should be Gravity");
        return this.lc.speedVar
    },
    xg: function (a) {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.setSpeedVar() : Particle Mode should be Gravity");
        this.lc.speedVar = a
    },
    AQ: function () {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.getTangentialAccel() : Particle Mode should be Gravity");
        return this.lc.tangentialAccel
    },
    xk: function (a) {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.setTangentialAccel() : Particle Mode should be Gravity");
        this.lc.tangentialAccel = a
    },
    BQ: function () {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.getTangentialAccelVar() : Particle Mode should be Gravity");
        return this.lc.tangentialAccelVar
    },
    yk: function (a) {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.setTangentialAccelVar() : Particle Mode should be Gravity");
        this.lc.tangentialAccelVar = a
    },
    oQ: function () {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.getRadialAccel() : Particle Mode should be Gravity");
        return this.lc.radialAccel
    },
    vg: function (a) {
        this.Ta !== F.o.ub &&
        F.log("cc.ParticleBatchNode.setRadialAccel() : Particle Mode should be Gravity");
        this.lc.radialAccel = a
    },
    pQ: function () {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.getRadialAccelVar() : Particle Mode should be Gravity");
        return this.lc.radialAccelVar
    },
    wg: function (a) {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.setRadialAccelVar() : Particle Mode should be Gravity");
        this.lc.radialAccelVar = a
    },
    N4: function () {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.getRotationIsDir() : Particle Mode should be Gravity");
        return this.lc.rotationIsDir
    },
    F8: function (a) {
        this.Ta !== F.o.ub && F.log("cc.ParticleBatchNode.setRotationIsDir() : Particle Mode should be Gravity");
        this.lc.rotationIsDir = a
    },
    yQ: function () {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.getStartRadius() : Particle Mode should be Radius");
        return this.he.startRadius
    },
    AS: function (a) {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.setStartRadius() : Particle Mode should be Radius");
        this.he.startRadius = a
    },
    zQ: function () {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.getStartRadiusVar() : Particle Mode should be Radius");
        return this.he.startRadiusVar
    },
    BS: function (a) {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.setStartRadiusVar() : Particle Mode should be Radius");
        this.he.startRadiusVar = a
    },
    gQ: function () {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.getEndRadius() : Particle Mode should be Radius");
        return this.he.endRadius
    },
    hS: function (a) {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.setEndRadius() : Particle Mode should be Radius");
        this.he.endRadius = a
    },
    hQ: function () {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.getEndRadiusVar() : Particle Mode should be Radius");
        return this.he.endRadiusVar
    },
    iS: function (a) {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.setEndRadiusVar() : Particle Mode should be Radius");
        this.he.endRadiusVar = a
    },
    qQ: function () {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.getRotatePerSecond() : Particle Mode should be Radius");
        return this.he.bA
    },
    wS: function (a) {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.setRotatePerSecond() : Particle Mode should be Radius");
        this.he.bA = a
    },
    rQ: function () {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.getRotatePerSecondVar() : Particle Mode should be Radius");
        return this.he.cA
    },
    xS: function (a) {
        this.Ta !== F.o.rf && F.log("cc.ParticleBatchNode.setRotatePerSecondVar() : Particle Mode should be Radius");
        this.he.cA = a
    },
    $q: function (a, c) {
        this.on = p;
        F.n.prototype.$q.call(this, a, c)
    },
    VH: function (a) {
        this.on = p;
        F.n.prototype.VH.call(this, a)
    },
    Mu: function (a) {
        this.on = p;
        F.n.prototype.Mu.call(this, a)
    },
    Nu: function (a) {
        this.on = p;
        F.n.prototype.Nu.call(this, a)
    },
    Aha: A("dr"),
    xh: z("dr"),
    Bha: A("er"),
    yh: z("er"),
    hga: A("In"),
    th: z("In"),
    iga: A("nq"),
    j8: z("nq"),
    Xy: function () {
        return F.color(this.ul.r,
            this.ul.g, this.ul.b, this.ul.a)
    },
    of: function (a) {
        this.ul = F.color(a)
    },
    xQ: function () {
        return F.color(this.vl.r, this.vl.g, this.vl.b, this.vl.a)
    },
    yg: function (a) {
        this.vl = F.color(a)
    },
    Ty: function () {
        return F.color(this.Ze.r, this.Ze.g, this.Ze.b, this.Ze.a)
    },
    nf: function (a) {
        this.Ze = F.color(a)
    },
    fQ: function () {
        return F.color(this.Zk.r, this.Zk.g, this.Zk.b, this.Zk.a)
    },
    tg: function (a) {
        this.Zk = F.color(a)
    },
    Cha: A("fr"),
    M8: z("fr"),
    Dha: A("gr"),
    N8: z("gr"),
    jga: A("oq"),
    k8: z("oq"),
    kga: A("pq"),
    l8: z("pq"),
    fga: A("Hn"),
    rh: z("Hn"),
    a5: A("Bb"),
    Q8: function (a) {
        if (F.B === F.Aa)this.Bb = 200 > a ? a : 200; else {
            if (a > this.Dw) {
                var c = F.Hb.BYTES_PER_ELEMENT;
                this.zb = new Uint16Array(6 * a);
                var d = new ArrayBuffer(a * c), e = this.Pj;
                e.length = 0;
                for (var f = this.Xc, g = f.length = 0; g < a; g++)e[g] = new F.Hg, f[g] = new F.Hb(q, q, q, q, d, g * c);
                this.Bb = this.Dw = a;
                if (this.ca)for (c = 0; c < a; c++)e[c].atlasIndex = c;
                this.zd = d;
                this.UG();
                this.gn();
                this.G && this.KQ(F.rect(0, 0, this.G.width, this.G.height))
            } else this.Bb = a;
            this.G7()
        }
    },
    Ma: A("G"),
    ib: function (a) {
        a.Jb ? this.ZH(a, F.rect(0, 0, a.width, a.height)) :
            (this.Ca = r, a.addEventListener("load", function (a) {
                this.Ca = p;
                this.ZH(a, F.rect(0, 0, a.width, a.height))
            }, this))
    },
    Jf: A("q"),
    Vd: function (a, c) {
        if (c === k)this.q != a && (this.q = a, this.ig()); else if (this.q.src != a || this.q.J != c)this.q = {
            src: a,
            J: c
        }, this.ig()
    },
    ng: A("qb"),
    Of: z("qb"),
    YQ: function () {
        return this.q.src == F.SRC_ALPHA && this.q.J == F.ONE || this.q.src == F.ONE && this.q.J == F.ONE
    },
    oi: function (a) {
        var c = this.q;
        a ? (c.src = F.SRC_ALPHA, c.J = F.ONE) : F.B === F.Y ? this.G && !this.G.gi() ? (c.src = F.SRC_ALPHA, c.J = F.ONE_MINUS_SRC_ALPHA) : (c.src =
            F.Ac, c.J = F.zc) : (c.src = F.Ac, c.J = F.zc)
    },
    gha: A("mj"),
    koa: z("mj"),
    yia: A("Ht"),
    cna: z("Ht"),
    gga: A("Ta"),
    sh: z("Ta"),
    oa: function () {
        return this.kc(150)
    },
    Nl: function (a) {
        this.cE = a;
        a = F.aa.ge(a);
        return !a ? (F.log("cc.ParticleSystem.initWithFile(): Particles: file not found"), r) : this.s5(a, "")
    },
    R3: function () {
        return F.rect(0, 0, F.lb.width, F.lb.height)
    },
    s5: function (a) {
        var c = r, d = q, d = this.O1, e = parseInt(d("maxParticles", a));
        if (this.kc(e)) {
            this.gh = parseFloat(d("angle", a));
            this.bq = parseFloat(d("angleVariance", a));
            this.duration =
                parseFloat(d("duration", a));
            this.q.src = parseInt(d("blendFuncSource", a));
            this.q.J = parseInt(d("blendFuncDestination", a));
            c = this.ul;
            c.r = 255 * parseFloat(d("startColorRed", a));
            c.g = 255 * parseFloat(d("startColorGreen", a));
            c.b = 255 * parseFloat(d("startColorBlue", a));
            c.a = 255 * parseFloat(d("startColorAlpha", a));
            c = this.vl;
            c.r = 255 * parseFloat(d("startColorVarianceRed", a));
            c.g = 255 * parseFloat(d("startColorVarianceGreen", a));
            c.b = 255 * parseFloat(d("startColorVarianceBlue", a));
            c.a = 255 * parseFloat(d("startColorVarianceAlpha",
                    a));
            c = this.Ze;
            c.r = 255 * parseFloat(d("finishColorRed", a));
            c.g = 255 * parseFloat(d("finishColorGreen", a));
            c.b = 255 * parseFloat(d("finishColorBlue", a));
            c.a = 255 * parseFloat(d("finishColorAlpha", a));
            c = this.Zk;
            c.r = 255 * parseFloat(d("finishColorVarianceRed", a));
            c.g = 255 * parseFloat(d("finishColorVarianceGreen", a));
            c.b = 255 * parseFloat(d("finishColorVarianceBlue", a));
            c.a = 255 * parseFloat(d("finishColorVarianceAlpha", a));
            this.dr = parseFloat(d("startParticleSize", a));
            this.er = parseFloat(d("startParticleSizeVariance", a));
            this.In =
                parseFloat(d("finishParticleSize", a));
            this.nq = parseFloat(d("finishParticleSizeVariance", a));
            this.X(parseFloat(d("sourcePositionx", a)), parseFloat(d("sourcePositiony", a)));
            this.nl.x = parseFloat(d("sourcePositionVariancex", a));
            this.nl.y = parseFloat(d("sourcePositionVariancey", a));
            this.fr = parseFloat(d("rotationStart", a));
            this.gr = parseFloat(d("rotationStartVariance", a));
            this.oq = parseFloat(d("rotationEnd", a));
            this.pq = parseFloat(d("rotationEndVariance", a));
            this.Ta = parseInt(d("emitterType", a));
            if (this.Ta ==
                F.o.ub)c = this.lc, c.gravity.x = parseFloat(d("gravityx", a)), c.gravity.y = parseFloat(d("gravityy", a)), c.speed = parseFloat(d("speed", a)), c.speedVar = parseFloat(d("speedVariance", a)), e = d("radialAcceleration", a), c.radialAccel = e ? parseFloat(e) : 0, e = d("radialAccelVariance", a), c.radialAccelVar = e ? parseFloat(e) : 0, e = d("tangentialAcceleration", a), c.tangentialAccel = e ? parseFloat(e) : 0, e = d("tangentialAccelVariance", a), c.tangentialAccelVar = e ? parseFloat(e) : 0, e = d("rotationIsDir", a).toLowerCase(), c.rotationIsDir = e != q && ("true" ===
                e || "1" === e); else if (this.Ta == F.o.rf)c = this.he, c.startRadius = parseFloat(d("maxRadius", a)), c.startRadiusVar = parseFloat(d("maxRadiusVariance", a)), c.endRadius = parseFloat(d("minRadius", a)), c.endRadiusVar = 0, c.bA = parseFloat(d("rotatePerSecond", a)), c.cA = parseFloat(d("rotatePerSecondVariance", a)); else return F.log("cc.ParticleSystem.initWithDictionary(): Invalid emitterType in config file"), r;
            this.mf = parseFloat(d("particleLifespan", a));
            this.Jq = parseFloat(d("particleLifespanVariance", a));
            this.Hn = this.Bb / this.mf;
            if (!this.ca)if (this.qb = r, c = d("textureFileName", a), c = F.path.hq(this.cE, c), e = F.Ra.Ml(c))this.ib(e); else if (a = d("textureImageData", a), !a || 0 === a.length) {
                e = F.Ra.ad(c);
                if (!e)return r;
                this.ib(e)
            } else {
                d = F.bT(a, 1);
                if (!d)return F.log("cc.ParticleSystem: error decoding or ungzipping textureImageData"), r;
                a = F.n4(d);
                if (a !== F.SI && a !== F.$A)return F.log("cc.ParticleSystem: unknown image format with Data"), r;
                e = F.bc("canvas");
                a === F.$A ? (new F.LV(d)).D7(e) : F.H9.W6(d, e);
                F.Ra.E2(c, e);
                (a = F.Ra.Ml(c)) || F.log("cc.ParticleSystem.initWithDictionary() : error loading the texture");
                this.ib(a)
            }
            c = p
        }
        return c
    },
    kc: function (a) {
        this.Bb = a;
        var c, d = this.Pj;
        for (c = d.length = 0; c < a; c++)d[c] = new F.Hg;
        if (!d)return F.log("Particle system: not enough memory"), r;
        this.Dw = a;
        if (this.ca)for (c = 0; c < this.Bb; c++)d[c].atlasIndex = c;
        this.rp = p;
        this.q.src = F.Ac;
        this.q.J = F.zc;
        this.mj = F.o.as;
        this.Ta = F.o.ub;
        this.on = this.Ht = r;
        this.ZR(1);
        if (F.B === F.Y) {
            if (!this.tL())return r;
            this.UG();
            this.gn();
            this.shaderProgram = F.le.Kc(F.yj)
        }
        return p
    },
    mea: function () {
        this.lr()
    },
    V1: function () {
        if (this.J5())return r;
        var a, c = this.Pj;
        F.B === F.Aa ? this.Ed < c.length ? a = c[this.Ed] : (a = new F.Hg, c.push(a)) : a = c[this.Ed];
        this.q5(a);
        ++this.Ed;
        return p
    },
    q5: function (a) {
        var c = F.p7;
        a.em = this.mf + this.Jq * c();
        a.em = Math.max(0, a.em);
        a.nb.x = this.Gp.x + this.nl.x * c();
        a.nb.y = this.Gp.y + this.nl.y * c();
        var d, e;
        d = this.ul;
        var f = this.vl, g = this.Ze;
        e = this.Zk;
        F.B === F.Aa ? (d = F.color(F.od(d.r + f.r * c(), 0, 255), F.od(d.g + f.g * c(), 0, 255), F.od(d.b + f.b * c(), 0, 255), F.od(d.a + f.a * c(), 0, 255)), e = F.color(F.od(g.r + e.r * c(), 0, 255), F.od(g.g + e.g * c(), 0, 255), F.od(g.b + e.b * c(), 0, 255), F.od(g.a +
            e.a * c(), 0, 255))) : (d = {
            r: F.od(d.r + f.r * c(), 0, 255),
            g: F.od(d.g + f.g * c(), 0, 255),
            b: F.od(d.b + f.b * c(), 0, 255),
            a: F.od(d.a + f.a * c(), 0, 255)
        }, e = {
            r: F.od(g.r + e.r * c(), 0, 255),
            g: F.od(g.g + e.g * c(), 0, 255),
            b: F.od(g.b + e.b * c(), 0, 255),
            a: F.od(g.a + e.a * c(), 0, 255)
        });
        a.color = d;
        f = a.Ot;
        g = a.em;
        f.r = (e.r - d.r) / g;
        f.g = (e.g - d.g) / g;
        f.b = (e.b - d.b) / g;
        f.a = (e.a - d.a) / g;
        d = this.dr + this.er * c();
        d = Math.max(0, d);
        a.size = d;
        this.In === F.o.Ig ? a.dG = 0 : (e = this.In + this.nq * c(), e = Math.max(0, e), a.dG = (e - d) / g);
        d = this.fr + this.gr * c();
        e = this.oq + this.pq * c();
        a.rotation =
            d;
        a.IP = (e - d) / g;
        this.mj == F.o.as ? a.tA = this.Jy(this.iE) : this.mj == F.o.fC && (a.tA.x = this.xa.x, a.tA.y = this.xa.y);
        d = F.kf(this.gh + this.bq * c());
        if (this.Ta === F.o.ub)g = this.lc, e = a.lc, f = g.speed + g.speedVar * c(), e.dir.x = Math.cos(d), e.dir.y = Math.sin(d), F.Pq(e.dir, f), e.radialAccel = g.radialAccel + g.radialAccelVar * c(), e.tangentialAccel = g.tangentialAccel + g.tangentialAccelVar * c(), g.rotationIsDir && (a.rotation = -F.AH(F.U6(e.dir))); else {
            e = this.he;
            a = a.he;
            var f = e.startRadius + e.startRadiusVar * c(), h = e.endRadius + e.endRadiusVar *
                c();
            a.Cu = f;
            a.HP = e.endRadius === F.o.pW ? 0 : (h - f) / g;
            a.gh = d;
            a.EP = F.kf(e.bA + e.cA * c())
        }
    },
    v9: function () {
        this.rp = r;
        this.yb = this.duration;
        this.Im = 0
    },
    G7: function () {
        this.rp = p;
        this.yb = 0;
        var a = this.Pj;
        for (this.af = 0; this.af < this.Ed; ++this.af)a[this.af].em = 0
    },
    J5: function () {
        return this.Ed >= this.Bb
    },
    U9: function (a, c) {
        var d = q;
        this.ca ? (d = this.ca.textureAtlas.quads[this.atlasIndex + a.atlasIndex], this.ca.textureAtlas.dirty = p) : d = this.Xc[this.af];
        var e, f, g, h;
        this.qb ? (e = 0 | a.color.r * a.color.a / 255, f = 0 | a.color.g * a.color.a / 255,
            g = 0 | a.color.b * a.color.a / 255) : (e = 0 | a.color.r, f = 0 | a.color.g, g = 0 | a.color.b);
        h = 0 | a.color.a;
        var m = d.K.A;
        m.r = e;
        m.g = f;
        m.b = g;
        m.a = h;
        m = d.V.A;
        m.r = e;
        m.g = f;
        m.b = g;
        m.a = h;
        m = d.U.A;
        m.r = e;
        m.g = f;
        m.b = g;
        m.a = h;
        m = d.O.A;
        m.r = e;
        m.g = f;
        m.b = g;
        m.a = h;
        e = a.size / 2;
        if (a.rotation) {
            f = -e;
            g = -e;
            h = c.x;
            var m = c.y, n = -F.kf(a.rotation), s = Math.cos(n), n = Math.sin(n);
            d.K.j.x = f * s - g * n + h;
            d.K.j.y = f * n + g * s + m;
            d.V.j.x = e * s - g * n + h;
            d.V.j.y = e * n + g * s + m;
            d.U.j.x = f * s - e * n + h;
            d.U.j.y = f * n + e * s + m;
            d.O.j.x = e * s - e * n + h;
            d.O.j.y = e * n + e * s + m
        } else d.K.j.x = c.x - e, d.K.j.y = c.y - e, d.V.j.x =
            c.x + e, d.V.j.y = c.y - e, d.U.j.x = c.x - e, d.U.j.y = c.y + e, d.O.j.x = c.x + e, d.O.j.y = c.y + e
    },
    h7: function () {
        if (F.B === F.Y) {
            var a = F.l;
            a.bindBuffer(a.ARRAY_BUFFER, this.oc[0]);
            a.bufferData(a.ARRAY_BUFFER, this.zd, a.DYNAMIC_DRAW)
        }
    },
    update: function (a) {
        if (this.rp && this.Hn) {
            var c = 1 / this.Hn;
            this.Ed < this.Bb && (this.Im += a);
            for (; this.Ed < this.Bb && this.Im > c;)this.V1(), this.Im -= c;
            this.yb += a;
            -1 != this.duration && this.duration < this.yb && this.v9()
        }
        this.af = 0;
        c = F.Hg.lw[0];
        this.mj == F.o.as ? F.tk(c, this.Jy(this.iE)) : this.mj == F.o.fC && (c.x = this.xa.x,
            c.y = this.xa.y);
        if (this.Jc) {
            for (var d = F.Hg.lw[1], e = F.Hg.lw[2], f = F.Hg.lw[3], g = this.Pj; this.af < this.Ed;) {
                F.Rz(d);
                F.Rz(e);
                F.Rz(f);
                var h = g[this.af];
                h.em -= a;
                if (0 < h.em) {
                    if (this.Ta == F.o.ub) {
                        var m = f, n = d, s = e;
                        h.nb.x || h.nb.y ? (F.tk(n, h.nb), F.S6(n)) : F.Rz(n);
                        F.tk(s, n);
                        F.Pq(n, h.lc.radialAccel);
                        var t = s.x;
                        s.x = -s.y;
                        s.y = t;
                        F.Pq(s, h.lc.tangentialAccel);
                        F.tk(m, n);
                        F.Pz(m, s);
                        F.Pz(m, this.lc.gravity);
                        F.Pq(m, a);
                        F.Pz(h.lc.dir, m);
                        F.tk(m, h.lc.dir);
                        F.Pq(m, a);
                        F.Pz(h.nb, m)
                    } else m = h.he, m.gh += m.EP * a, m.Cu += m.HP * a, h.nb.x = -Math.cos(m.gh) *
                        m.Cu, h.nb.y = -Math.sin(m.gh) * m.Cu;
                    if (!this.aM || F.B === F.Y)h.color.r += h.Ot.r * a, h.color.g += h.Ot.g * a, h.color.b += h.Ot.b * a, h.color.a += h.Ot.a * a, h.bH = p;
                    h.size += h.dG * a;
                    h.size = Math.max(0, h.size);
                    h.rotation += h.IP * a;
                    m = d;
                    this.mj == F.o.as || this.mj == F.o.fC ? (n = e, F.tk(n, c), F.CR(n, h.tA), F.tk(m, h.nb), F.CR(m, n)) : F.tk(m, h.nb);
                    this.ca && (m.x += this.xa.x, m.y += this.xa.y);
                    F.B == F.Y ? this.U9(h, m) : F.tk(h.hk, m);
                    ++this.af
                } else if (h = h.atlasIndex, this.af !== this.Ed - 1 && (m = g[this.af], g[this.af] = g[this.Ed - 1], g[this.Ed - 1] = m), this.ca && (this.ca.Z2(this.atlasIndex +
                        h), g[this.Ed - 1].atlasIndex = h), --this.Ed, 0 == this.Ed && this.Ht) {
                    this.lr();
                    this.Ja.removeChild(this, p);
                    return
                }
            }
            this.on = r
        }
        this.ca || this.h7()
    },
    X9: function () {
        this.update(0)
    },
    O1: function (a, c) {
        if (c) {
            var d = c[a];
            return d != q ? d : ""
        }
        return ""
    },
    ig: function () {
        if (this.ca)F.log("Can't change blending functions when the particle is being batched"); else {
            var a = this.G;
            if (a && a instanceof F.ha) {
                this.qb = r;
                var c = this.q;
                c.src == F.Ac && c.J == F.zc && (a.gi() ? this.qb = p : (c.src = F.SRC_ALPHA, c.J = F.ONE_MINUS_SRC_ALPHA))
            }
        }
    },
    m: function () {
        var a =
            new F.o;
        if (a.kc(this.Bb)) {
            a.ph(this.gh);
            a.qh(this.bq);
            a.sg(this.xq());
            var c = this.Jf();
            a.Vd(c.src, c.J);
            a.of(this.Xy());
            a.yg(this.xQ());
            a.nf(this.Ty());
            a.tg(this.fQ());
            a.xh(this.dr);
            a.yh(this.er);
            a.th(this.In);
            a.j8(this.nq);
            a.X(F.d(this.x, this.y));
            a.wh(F.d(this.FG().x, this.FG().y));
            a.M8(this.fr || 0);
            a.N8(this.gr || 0);
            a.k8(this.oq || 0);
            a.l8(this.pq || 0);
            a.sh(this.Ta);
            this.Ta == F.o.ub ? (c = this.jQ(), a.ug(F.d(c.x, c.y)), a.Xd(this.Kl()), a.xg(this.vQ()), a.vg(this.oQ()), a.wg(this.pQ()), a.xk(this.AQ()), a.yk(this.BQ())) :
            this.Ta == F.o.rf && (a.AS(this.yQ()), a.BS(this.zQ()), a.hS(this.gQ()), a.iS(this.hQ()), a.wS(this.qQ()), a.xS(this.rQ()));
            a.uh(this.mf);
            a.vh(this.Jq);
            a.rh(this.Hn);
            if (!this.ca && (a.Of(this.ng()), c = this.Ma())) {
                var d = c.$();
                a.ZH(c, F.rect(0, 0, d.width, d.height))
            }
        }
        return a
    },
    f8: function (a) {
        var c = a.C4();
        (0 != c.x || 0 != c.y) && F.log("cc.ParticleSystem.setDisplayFrame(): QuadParticle only supports SpriteFrames with no offsets");
        F.B === F.Y && (!this.G || a.Ma().ve != this.G.ve) && this.ib(a.Ma())
    },
    ZH: function (a, c) {
        var d = this.G;
        if (F.B ===
            F.Y) {
            if ((!d || a.ve != d.ve) && d != a)this.G = a, this.ig()
        } else if ((!d || a != d) && d != a)this.G = a, this.ig();
        this.Xm = c;
        this.KQ(c)
    },
    Ka: function (a) {
        this.Ca && !this.ca && (F.B === F.Aa ? this.Zo(a) : this.Fj(a), F.bd++)
    },
    Zo: function (a) {
        a = a || F.l;
        a.save();
        a.globalCompositeOperation = this.YQ() ? "lighter" : "source-over";
        for (var c = this.G.Xa, d = F.view.sa, e = F.view.Ya, f = 0; f < this.Ed; f++) {
            var g = this.Pj[f], h = 0 | 0.5 * g.size;
            if (this.Ny == F.o.UB) {
                if (c.width && c.height) {
                    a.save();
                    a.globalAlpha = g.color.a / 255;
                    a.translate(0 | g.hk.x, -(0 | g.hk.y));
                    var h =
                        4 * Math.floor(g.size / 4), m = this.Xm.width, n = this.Xm.height;
                    a.scale(Math.max(h * d / m, 1E-6), Math.max(h * e / n, 1E-6));
                    g.rotation && a.rotate(F.kf(g.rotation));
                    a.translate(-(0 | m / 2), -(0 | n / 2));
                    (g = g.bH ? this.De(c, g.color, this.Xm) : c) && a.drawImage(g, 0, 0);
                    a.restore()
                }
            } else a.save(), a.globalAlpha = g.color.a / 255, a.translate(0 | g.hk.x, -(0 | g.hk.y)), this.sA == F.o.WJ ? (g.rotation && a.rotate(F.kf(g.rotation)), F.Ge.PP(a, h, g.color)) : F.Ge.MP(a, h, g.color), a.restore()
        }
        a.restore()
    },
    De: function (a, c, d) {
        a.Te || (a.Te = document.createElement("canvas"),
            a.Te.width = a.width, a.Te.height = a.height);
        return F.Mn(a, c, d, a.Te)
    },
    Fj: function (a) {
        this.G && (a = a || F.l, this.na.xb(), this.na.qf(), F.Cd(this.G), F.cz(this.q.src, this.q.J), F.Xb(F.Bh), a.bindBuffer(a.ARRAY_BUFFER, this.oc[0]), a.vertexAttribPointer(F.kb, 3, a.FLOAT, r, 24, 0), a.vertexAttribPointer(F.sd, 4, a.UNSIGNED_BYTE, p, 24, 12), a.vertexAttribPointer(F.hd, 2, a.FLOAT, r, 24, 16), a.bindBuffer(a.ELEMENT_ARRAY_BUFFER, this.oc[1]), a.drawElements(a.TRIANGLES, 6 * this.af, a.UNSIGNED_SHORT, 0))
    },
    Mka: function () {
        F.TB || this.gn()
    },
    Vca: u(),
    gn: function () {
        if (F.B != F.Aa) {
            var a = F.l;
            this.oc[0] = a.createBuffer();
            a.bindBuffer(a.ARRAY_BUFFER, this.oc[0]);
            a.bufferData(a.ARRAY_BUFFER, this.zd, a.DYNAMIC_DRAW);
            this.oc[1] = a.createBuffer();
            a.bindBuffer(a.ELEMENT_ARRAY_BUFFER, this.oc[1]);
            a.bufferData(a.ELEMENT_ARRAY_BUFFER, this.zb, a.STATIC_DRAW)
        }
    },
    tL: function () {
        if (F.B === F.Aa)return p;
        if (this.ca)return F.log("cc.ParticleSystem._allocMemory(): Memory should not be allocated when not using batchNode"), r;
        var a = F.Hb.BYTES_PER_ELEMENT, c = this.Bb, d = this.Xc;
        d.length =
            0;
        this.zb = new Uint16Array(6 * c);
        for (var e = new ArrayBuffer(a * c), f = 0; f < c; f++)d[f] = new F.Hb(q, q, q, q, e, f * a);
        if (!d || !this.zb)return F.log("cocos2d: Particle system: not enough memory"), r;
        this.zd = e;
        return p
    }
});
M = F.o.prototype;
F.B === F.Aa && !F.ta.hy && (M.De = function (a, c, d) {
    var e = F.Ra.Vt(a);
    return e ? (e.Te || (e.Te = document.createElement("canvas"), e.Te.width = a.width, e.Te.height = a.height), F.Il(a, e, c, d, e.Te), e.Te) : q
});
F.k(M, "opacityModifyRGB", M.ng, M.Of);
F.k(M, "batchNode", M.vG, M.$l);
F.k(M, "active", M.mk);
F.k(M, "sourcePos", M.V4, M.K8);
F.k(M, "posVar", M.FG, M.wh);
F.k(M, "gravity", M.jQ, M.ug);
F.k(M, "speed", M.Kl, M.Xd);
F.k(M, "speedVar", M.vQ, M.xg);
F.k(M, "tangentialAccel", M.AQ, M.xk);
F.k(M, "tangentialAccelVar", M.BQ, M.yk);
F.k(M, "radialAccel", M.oQ, M.vg);
F.k(M, "radialAccelVar", M.pQ, M.wg);
F.k(M, "rotationIsDir", M.N4, M.F8);
F.k(M, "startRadius", M.yQ, M.AS);
F.k(M, "startRadiusVar", M.zQ, M.BS);
F.k(M, "endRadius", M.gQ, M.hS);
F.k(M, "endRadiusVar", M.hQ, M.iS);
F.k(M, "rotatePerS", M.qQ, M.wS);
F.k(M, "rotatePerSVar", M.rQ, M.xS);
F.k(M, "startColor", M.Xy, M.of);
F.k(M, "startColorVar", M.xQ, M.yg);
F.k(M, "endColor", M.Ty, M.nf);
F.k(M, "endColorVar", M.fQ, M.tg);
F.k(M, "totalParticles", M.a5, M.Q8);
F.k(M, "texture", M.Ma, M.ib);
F.o.create = function (a) {
    return new F.o(a)
};
F.o.eea = F.o.create;
F.o.nB = function (a, c, d, e, f, g, h, m) {
    this.gravity = a ? a : F.d(0, 0);
    this.speed = c || 0;
    this.speedVar = d || 0;
    this.tangentialAccel = e || 0;
    this.tangentialAccelVar = f || 0;
    this.radialAccel = g || 0;
    this.radialAccelVar = h || 0;
    this.rotationIsDir = m || r
};
F.o.oB = function (a, c, d, e, f, g) {
    this.startRadius = a || 0;
    this.startRadiusVar = c || 0;
    this.endRadius = d || 0;
    this.endRadiusVar = e || 0;
    this.bA = f || 0;
    this.cA = g || 0
};
F.o.VJ = 0;
F.o.UB = 1;
F.o.WJ = 0;
F.o.zI = 1;
F.o.si = -1;
F.o.Ig = -1;
F.o.pW = -1;
F.o.ub = 0;
F.o.rf = 1;
F.o.as = 0;
F.o.fC = 1;
F.o.Gba = 2;
F.yJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 300 : 150)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(0, 0)), this.vg(0), this.wg(0), this.Xd(60), this.xg(20), this.ph(90), this.qh(10), a = F.L.Pa(), this.X(a.width / 2, 60), this.wh(F.d(40, 20)), this.uh(3), this.vh(0.25), this.xh(54), this.yh(10), this.th(F.o.Ig), this.rh(this.Bb / this.mf), this.of(F.color(194, 64, 31, 255)), this.yg(F.color(0, 0, 0, 0)), this.nf(F.color(0, 0, 0, 255)), this.tg(F.color(0,
            0, 0, 0)), this.oi(p), p) : r
    }
});
F.yJ.create = function () {
    return new F.yJ
};
F.zJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 1500 : 150)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(0, -90)), this.vg(0), this.wg(0), this.Xd(180), this.xg(50), a = F.L.Pa(), this.X(a.width / 2, a.height / 2), this.ph(90), this.qh(20), this.uh(3.5), this.vh(1), this.rh(this.Bb / this.mf), this.of(F.color(128, 128, 128, 255)), this.yg(F.color(128, 128, 128, 255)), this.nf(F.color(26, 26, 26, 51)), this.tg(F.color(26, 26, 26, 51)), this.xh(8), this.yh(2), this.th(F.o.Ig),
            this.oi(r), p) : r
    }
});
F.zJ.create = function () {
    return new F.zJ
};
F.JJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 350 : 150)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.oi(p), this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(0, 0)), this.vg(0), this.wg(0), this.Xd(20), this.xg(5), this.ph(90), this.qh(360), a = F.L.Pa(), this.X(a.width / 2, a.height / 2), this.wh(F.d(0, 0)), this.uh(1), this.vh(0.5), this.xh(30), this.yh(10), this.th(F.o.Ig), this.rh(this.Bb / this.mf), this.of(F.color(194, 64, 31, 255)), this.yg(F.color(0, 0, 0, 0)), this.nf(F.color(0, 0, 0, 255)), this.tg(F.color(0,
            0, 0, 0)), p) : r
    }
});
F.JJ.create = function () {
    return new F.JJ
};
F.BJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 200 : 100)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(0, 0)), this.Xd(60), this.xg(10), this.vg(-80), this.wg(0), this.xk(80), this.yk(0), this.ph(90), this.qh(360), a = F.L.Pa(), this.X(a.width / 2, a.height / 2), this.wh(F.d(0, 0)), this.uh(4), this.vh(1), this.xh(37), this.yh(10), this.th(F.o.Ig), this.rh(this.Bb / this.mf), this.of(F.color(31, 64, 194, 255)), this.yg(F.color(0, 0, 0, 0)), this.nf(F.color(0,
            0, 0, 255)), this.tg(F.color(0, 0, 0, 0)), this.oi(p), p) : r
    }
});
F.BJ.create = function () {
    return new F.BJ
};
F.AJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 250 : 100)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(0, 0)), this.Xd(80), this.xg(10), this.vg(-60), this.wg(0), this.xk(15), this.yk(0), this.ph(90), this.qh(360), a = F.L.Pa(), this.X(a.width / 2, a.height / 2), this.wh(F.d(0, 0)), this.uh(4), this.vh(1), this.xh(30), this.yh(10), this.th(F.o.Ig), this.rh(this.Bb / this.mf), this.of(F.color(128, 128, 128, 255)), this.yg(F.color(128, 128, 128, 128)), this.nf(F.color(0,
            0, 0, 255)), this.tg(F.color(0, 0, 0, 0)), this.oi(p), p) : r
    }
});
F.AJ.create = function () {
    return new F.AJ
};
F.CJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 150 : 100)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(-200, 200)), this.Xd(15), this.xg(5), this.vg(0), this.wg(0), this.xk(0), this.yk(0), this.ph(90), this.qh(360), a = F.L.Pa(), this.X(a.width / 2, a.height / 2), this.wh(F.d(0, 0)), this.uh(2), this.vh(1), this.xh(60), this.yh(10), this.th(F.o.Ig), this.rh(this.Bb / this.mf), this.of(F.color(51, 102, 179)), this.yg(F.color(0, 0, 51, 26)), this.nf(F.color(0,
            0, 0, 255)), this.tg(F.color(0, 0, 0, 0)), this.oi(p), p) : r
    }
});
F.CJ.create = function () {
    return new F.CJ
};
F.IJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 500 : 100)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(0, 0)), this.Xd(150), this.xg(0), this.vg(-380), this.wg(0), this.xk(45), this.yk(0), this.ph(90), this.qh(0), a = F.L.Pa(), this.X(a.width / 2, a.height / 2), this.wh(F.d(0, 0)), this.uh(12), this.vh(0), this.xh(20), this.yh(0), this.th(F.o.Ig), this.rh(this.Bb / this.mf), this.of(F.color(128, 128, 128, 255)), this.yg(F.color(128, 128, 128, 0)), this.nf(F.color(128,
            128, 128, 255)), this.tg(F.color(128, 128, 128, 0)), this.oi(r), p) : r
    }
});
F.IJ.create = function () {
    return new F.IJ
};
F.xJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 700 : 300)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(0.1), this.sh(F.o.ub), this.ug(F.d(0, 0)), this.Xd(70), this.xg(40), this.vg(0), this.wg(0), this.xk(0), this.yk(0), this.ph(90), this.qh(360), a = F.L.Pa(), this.X(a.width / 2, a.height / 2), this.wh(F.d(0, 0)), this.uh(5), this.vh(2), this.xh(15), this.yh(10), this.th(F.o.Ig), this.rh(this.Bb / this.xq()), this.of(F.color(179, 26, 51, 255)), this.yg(F.color(128, 128, 128, 0)), this.nf(F.color(128,
            128, 128, 0)), this.tg(F.color(128, 128, 128, 0)), this.oi(r), p) : r
    }
});
F.xJ.create = function () {
    return new F.xJ
};
F.GJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 200 : 100)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(0, 0)), this.vg(0), this.wg(0), this.Xd(25), this.xg(10), this.ph(90), this.qh(5), a = F.L.Pa(), this.X(a.width / 2, 0), this.wh(F.d(20, 0)), this.uh(4), this.vh(1), this.xh(60), this.yh(10), this.th(F.o.Ig), this.rh(this.Bb / this.mf), this.of(F.color(204, 204, 204, 255)), this.yg(F.color(5, 5, 5, 0)), this.nf(F.color(0, 0, 0, 255)), this.tg(F.color(0, 0, 0,
            0)), this.oi(r), p) : r
    }
});
F.GJ.create = function () {
    return new F.GJ
};
F.HJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 700 : 250)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(0, -1)), this.Xd(5), this.xg(1), this.vg(0), this.wg(1), this.xk(0), this.yk(1), a = F.L.Pa(), this.X(a.width / 2, a.height + 10), this.wh(F.d(a.width / 2, 0)), this.ph(-90), this.qh(5), this.uh(45), this.vh(15), this.xh(10), this.yh(5), this.th(F.o.Ig), this.rh(10), this.of(F.color(255, 255, 255, 255)), this.yg(F.color(0, 0, 0, 0)), this.nf(F.color(255, 255,
            255, 0)), this.tg(F.color(0, 0, 0, 0)), this.oi(r), p) : r
    }
});
F.HJ.create = function () {
    return new F.HJ
};
F.DJ = F.o.extend({
    ctor: function () {
        F.o.prototype.ctor.call(this, F.B === F.Y ? 1E3 : 300)
    }, kc: function (a) {
        return F.o.prototype.kc.call(this, a) ? (this.sg(F.o.si), this.sh(F.o.ub), this.ug(F.d(10, -10)), this.vg(0), this.wg(1), this.xk(0), this.yk(1), this.Xd(130), this.xg(30), this.ph(-90), this.qh(5), a = F.L.Pa(), this.X(a.width / 2, a.height), this.wh(F.d(a.width / 2, 0)), this.uh(4.5), this.vh(0), this.xh(4), this.yh(2), this.th(F.o.Ig), this.rh(20), this.of(F.color(179, 204, 255, 255)), this.yg(F.color(0, 0, 0, 0)), this.nf(F.color(179, 204,
            255, 128)), this.tg(F.color(0, 0, 0, 0)), this.oi(r), p) : r
    }
});
F.DJ.create = function () {
    return new F.DJ
};
F.dba = 500;
F.vB = F.n.extend({
    textureAtlas: q, Kba: p, q: q, Mb: "ParticleBatchNode", ctor: function (a, c) {
        F.n.prototype.ctor.call(this);
        this.q = {src: F.Ac, J: F.zc};
        F.Dd(a) ? this.oa(a, c) : a instanceof F.ha && this.Fa(a, c)
    }, Fa: function (a, c) {
        this.textureAtlas = new F.wi;
        this.textureAtlas.Fa(a, c);
        this.u.length = 0;
        F.B === F.Y && (this.shaderProgram = F.le.Kc(F.yj));
        return p
    }, Nl: function (a, c) {
        var d = F.Ra.ad(a);
        return this.Fa(d, c)
    }, oa: function (a, c) {
        var d = F.Jba.zga().ad(a);
        return this.Fa(d, c)
    }, F: function (a, c, d) {
        a || b("cc.ParticleBatchNode.addChild() : child should be non-null");
        a instanceof F.o || b("cc.ParticleBatchNode.addChild() : only supports cc.ParticleSystem as children");
        c = c == q ? a.zIndex : c;
        d = d == q ? a.tag : d;
        a.Ma() != this.textureAtlas.texture && b("cc.ParticleSystem.addChild() : the child is not using the same texture id");
        var e = a.Jf();
        if (0 === this.u.length)this.Vd(e); else if (e.src != this.q.src || e.J != this.q.J) {
            F.log("cc.ParticleSystem.addChild() : Can't add a ParticleSystem that uses a different blending function");
            return
        }
        c = this.BC(a, c, d);
        d = 0;
        0 != c ? (c = this.u[c - 1], d = c.wq() + c.Bb) : d = 0;
        this.$G(a,
            d);
        a.$l(this)
    }, $G: function (a, c) {
        var d = a.Bb, e = this.textureAtlas, f = e.totalQuads;
        a.LH(c);
        f + d > e.ud && (this.j_(f + d), e.XP(e.ud - d, d));
        a.wq() + d != f && e.qR(c, c + d);
        e.JQ(d);
        this.pF()
    }, removeChild: function (a, c) {
        if (a != q)if (a instanceof F.o || b("cc.ParticleBatchNode.removeChild(): only supports cc.ParticleSystem as children"), -1 == this.u.indexOf(a))F.log("cc.ParticleBatchNode.removeChild(): doesn't contain the sprite. Can't remove it"); else {
            F.n.prototype.removeChild.call(this, a, c);
            var d = this.textureAtlas;
            d.A7(a.wq(),
                a.Bb);
            d.XP(d.totalQuads, a.Bb);
            a.$l(q);
            this.pF()
        }
    }, Wq: function (a, c) {
        a || b("cc.ParticleBatchNode.reorderChild(): child should be non-null");
        a instanceof F.o || b("cc.ParticleBatchNode.reorderChild(): only supports cc.QuadParticleSystems as children");
        if (-1 === this.u.indexOf(a))F.log("cc.ParticleBatchNode.reorderChild(): Child doesn't belong to batch"); else if (c != a.zIndex) {
            if (1 < this.u.length) {
                var d = this.HZ(a, c);
                if (d.tR != d.rR) {
                    this.u.splice(d.tR, 1);
                    this.u.splice(d.rR, 0, a);
                    d = a.wq();
                    this.pF();
                    for (var e = 0, f =
                        this.u, g = 0; g < f.length; g++)if (f[g] == a) {
                        e = a.wq();
                        break
                    }
                    this.textureAtlas.qR(d, a.Bb, e);
                    a.X9()
                }
            }
            a.Zx(c)
        }
    }, w7: function (a, c) {
        this.removeChild(this.u[za], c)
    }, Mf: function (a) {
        for (var c = this.u, d = 0; d < c.length; d++)c[d].$l(q);
        F.n.prototype.Mf.call(this, a);
        this.textureAtlas.Tq()
    }, Z2: function (a) {
        a = this.textureAtlas.quads[a];
        a.V.j.x = a.V.j.y = a.O.j.x = a.O.j.y = a.U.j.x = a.U.j.y = a.K.j.x = a.K.j.y = 0;
        this.textureAtlas.rl(p)
    }, Ka: function () {
        F.B !== F.Aa && 0 != this.textureAtlas.totalQuads && (F.nu(this), F.cz(this.q.src, this.q.J), this.textureAtlas.Gl())
    },
    Ma: function () {
        return this.textureAtlas.texture
    }, ib: function (a) {
        this.textureAtlas.texture = a;
        var c = this.q;
        a && (!a.gi() && c.src == F.Ac && c.J == F.zc) && (c.src = F.SRC_ALPHA, c.J = F.ONE_MINUS_SRC_ALPHA)
    }, Vd: function (a, c) {
        c === k ? (this.q.src = a.src, this.q.J = a.J) : (this.q.src = a, this.q.src = c)
    }, Jf: function () {
        return {src: this.q.src, J: this.q.J}
    }, H: function (a) {
        if (F.B !== F.Aa && this.Jc) {
            var c = F.tb;
            c.stack.push(c.top);
            F.og(this.Pb, c.top);
            c.top = this.Pb;
            this.transform(a);
            this.Z && F.ka.xc(this.Z);
            F.ok()
        }
    }, pF: function () {
        for (var a = 0,
                 c = this.u, d = 0; d < c.length; d++) {
            var e = c[d];
            e.LH(a);
            a += e.Bb
        }
    }, j_: function (a) {
        F.log("cocos2d: cc.ParticleBatchNode: resizing TextureAtlas capacity from [" + this.textureAtlas.ud + "] to [" + a + "].");
        this.textureAtlas.Zz(a) || F.log("cc.ParticleBatchNode._increaseAtlasCapacityTo() : WARNING: Not enough memory to resize the atlas")
    }, A0: function (a) {
        for (var c = this.u, d = c.length, e = 0; e < d; e++)if (c[e].zIndex > a)return e;
        return d
    }, HZ: function (a, c) {
        for (var d = r, e = r, f = 0, g = 0, h = 0, m = this.u, n = m.length, s = 0; s < n; s++) {
            var t = m[s];
            if (t.zIndex >
                c && !e && (f = s, e = p, d && e))break;
            if (a == t && (g = s, d = p, e || (h = -1), d && e))break
        }
        e || (f = n);
        return {rR: f + h, tR: g}
    }, BC: function (a, c, d) {
        a || b("cc.ParticleBatchNode._addChildHelper(): child should be non-null");
        if (a.parent)return F.log("cc.ParticleBatchNode._addChildHelper(): child already added. It can't be added again"), q;
        this.u || (this.u = []);
        var e = this.A0(c);
        this.u.splice(e, 0, a);
        a.tag = d;
        a.Zx(c);
        a.parent = this;
        this.Rh && (a.ba(), a.mi());
        return e
    }, ig: function () {
        this.textureAtlas.texture.gi() || (this.q.src = F.SRC_ALPHA, this.q.J =
            F.ONE_MINUS_SRC_ALPHA)
    }, Yy: A("textureAtlas"), YH: z("textureAtlas"), de: function () {
        F.B === F.Y && (this.Z = new F.wJ(this))
    }
});
M = F.vB.prototype;
F.k(M, "texture", M.Ma, M.ib);
F.vB.create = function (a, c) {
    return new F.vB(a, c)
};
F.uaa = function (a, c, d) {
    this.hh = a || F.rect(0, 0, 0, 0);
    this.end = c || F.rect(0, 0, 0, 0);
    this.duration = d || 0
};
F.taa = F.za.extend({
    ctor: function () {
        F.ej.eP(this)
    }, Wz: function () {
        F.ej.Wz(this)
    }, t2: function () {
        return F.ej.hP(this)
    }, KP: function () {
        return F.ej.JP(this)
    }, SF: D(r), X2: u(), TF: D(r), Y2: u(), XQ: u(), GP: u(), wG: D(""), a6: u(), Y5: u(), Z5: u(), X5: u()
});
F.eB = F.za.extend({
    Dc: q, gb: q, Ej: "", Om: q, ctor: function () {
        this.gb = new F.eB.PU;
        this.Om = F.d(0, 0)
    }, oa: function () {
        if (!F.ta.nh) {
            this.Dc = F.Gb("#imeDispatcherInput");
            this.Dc || (this.Dc = F.mT(), this.Dc.setAttribute("type", "text"), this.Dc.setAttribute("id", "imeDispatcherInput"), this.Dc.Ju(0, 0), this.Dc.WS(0, 0), this.Dc.style.opacity = "0", this.Dc.style.fontSize = "1px", this.Dc.setAttribute("tabindex", 2), this.Dc.style.position = "absolute", this.Dc.style.top = 0, this.Dc.style.left = 0, document.body.appendChild(this.Dc));
            var a =
                this;
            F.Sa(this.Dc, "input", function () {
                a.oE(a.Dc.value)
            }, r);
            F.Sa(this.Dc, "keydown", function (c) {
                c.keyCode === F.Kr.x9 ? (c.stopPropagation(), c.preventDefault()) : c.keyCode == F.Kr.RP && (a.Pt("\n", 1), c.stopPropagation(), c.preventDefault())
            }, r);
            /msie/i.test(navigator.userAgent) && F.Sa(this.Dc, "keyup", function (c) {
                c.keyCode == F.Kr.iP && a.oE(a.Dc.value)
            }, r);
            F.Sa(window, "mousedown", function (c) {
                var d = c.pageY || 0;
                a.Om.x = c.pageX || 0;
                a.Om.y = d
            }, r)
        }
    }, oE: function (a) {
        var c, d;
        c = this.Ej.length < a.length ? this.Ej.length : a.length;
        for (d =
                 0; d < c && a[d] === this.Ej[d]; d++);
        var e = this.Ej.length - d, f = a.length - d;
        for (c = 0; c < e; c++)this.LP();
        for (c = 0; c < f; c++)this.Pt(a[d + c], 1);
        this.Ej = a
    }, Pt: function (a, c) {
        this.gb && a && !(0 >= c) && this.gb.Ee && this.gb.Ee.XQ(a, c)
    }, LP: function () {
        this.gb && this.gb.Ee && this.gb.Ee.GP()
    }, wG: function () {
        if (this.gb && this.gb.Ee) {
            var a = this.gb.Ee.wG();
            return a ? a : ""
        }
        return ""
    }, vea: function () {
        if (this.gb)for (var a = 0; a < this.gb.Pg.length; a++);
    }, tea: function () {
        if (this.gb)for (var a = 0; a < this.gb.Pg.length; a++);
    }, uea: function () {
        if (this.gb)for (var a =
            0; a < this.gb.Pg.length; a++);
    }, sea: function () {
        if (this.gb)for (var a = 0; a < this.gb.Pg.length; a++);
    }, eP: function (a) {
        a && this.gb && (-1 < this.gb.Pg.indexOf(a) || this.gb.Pg.splice(0, 0, a))
    }, hP: function (a) {
        if (!this.gb || !a || -1 == this.gb.Pg.indexOf(a))return r;
        if (this.gb.Ee) {
            if (!this.gb.Ee.TF() || !a.SF())return r;
            this.gb.Ee = q;
            this.PM(a);
            return p
        }
        if (!a.SF())return r;
        this.PM(a);
        return p
    }, PM: function (a) {
        F.ta.nh ? (this.gb.Ee = a, this.Ej = a.string || "", a = prompt("please enter your word:", this.Ej), a != q && this.oE(a), this.Pt("\n",
            1)) : (this.gb.Ee = a, this.Ej = a.string || "", this.Dc.focus(), this.Dc.value = this.Ej, this.lZ())
    }, lZ: function () {
        /msie/i.test(navigator.userAgent) ? (this.Dc.style.left = this.Om.x + "px", this.Dc.style.top = this.Om.y + "px") : this.Dc.WS(this.Om.x, this.Om.y)
    }, JP: function (a) {
        if (!this.gb || !a || this.gb.Ee != a || !a.TF())return r;
        this.gb.Ee = q;
        F.lb.focus();
        return p
    }, Wz: function (a) {
        this.gb && a && -1 != this.gb.Pg.indexOf(a) && (this.gb.Ee && a == this.gb.Ee && (this.gb.Ee = q), F.we(this.gb.Pg, a))
    }, Wla: function (a) {
        32 > a ? a == F.Kr.iP ? this.LP() : a ==
        F.Kr.RP && this.Pt("\n", 1) : 255 > a && this.Pt(String.fromCharCode(a), 1)
    }
});
F.eB.PU = F.za.extend({
    Ee: q, Pg: q, ctor: function () {
        this.Pg = []
    }, tfa: function (a) {
        for (var c = 0; c < this.Pg.length; c++)if (this.Pg[c] == a)return c;
        return q
    }
});
F.ej = new F.eB;
document.body ? F.ej.oa() : F.Sa(window, "load", function () {
    F.ej.oa()
}, r);
F.Iba = F.za.extend({rla: D(r), tla: D(r), ula: D(r), sla: D(r), nla: D(r)});
F.hC = F.R.extend({
    FP: q, gk: q, Uo: q, Cca: q, Jh: "", Wm: "", Mw: 0, Mb: "TextFieldTTF", ctor: function (a, c, d, e, f) {
        this.gk = F.color(127, 127, 127);
        this.Uo = F.color(255, 255, 255, 255);
        F.ej.eP(this);
        F.R.prototype.ctor.call(this);
        f !== k ? (this.x5("", c, d, e, f), a && this.Lu(a)) : e === k && d !== k && (this.Td("", c, d), a && this.Lu(a))
    }, Y3: A("FP"), d8: z("FP"), S3: A("Mw"), Qfa: function () {
        return F.color(this.gk)
    }, lna: function (a) {
        this.gk.r = a.r;
        this.gk.g = a.g;
        this.gk.b = a.b;
        this.gk.a = F.nk(a.a) ? 255 : a.a
    }, Eoa: function (a) {
        this.Uo.r = a.r;
        this.Uo.g = a.g;
        this.Uo.b =
            a.b;
        this.Uo.a = F.nk(a.a) ? 255 : a.a
    }, x5: function (a, c, d, e, f) {
        switch (arguments.length) {
            case 5:
                return a && this.Lu(a), this.Td(this.Wm, e, f, c, d);
            case 3:
                return a && this.Lu(a), this.Td(this.Wm, arguments[1], arguments[2]);
            default:
                b("Argument must be non-nil ")
        }
    }, yc: function (a) {
        this.Jh = (a = String(a)) || "";
        this.Jh.length ? (F.R.prototype.yc.call(this, this.Jh), this.rb(this.Uo)) : (F.R.prototype.yc.call(this, this.Wm), this.rb(this.gk));
        F.B === F.Aa && this.Vp();
        this.Mw = this.Jh.length
    }, Ll: A("Jh"), Lu: function (a) {
        this.Wm = a || "";
        this.Jh.length ||
        (F.R.prototype.yc.call(this, this.Wm), this.rb(this.gk))
    }, H4: A("Wm"), Ka: function (a) {
        F.R.prototype.Ka.call(this, a || F.l)
    }, H: function (a) {
        this._super(a)
    }, t2: function () {
        return F.ej.hP(this)
    }, KP: function () {
        return F.ej.JP(this)
    }, SF: D(p), X2: u(), TF: D(p), Y2: u(), GP: function () {
        var a = this.Jh.length;
        0 != a && (1 >= a ? (this.Jh = "", this.Mw = 0, F.R.prototype.yc.call(this, this.Wm), this.rb(this.gk)) : this.string = this.Jh.substring(0, a - 1))
    }, Wz: function () {
        F.ej.Wz(this)
    }, XQ: function (a) {
        var c = a;
        a = c.indexOf("\n");
        -1 < a && (c = c.substring(0,
            a));
        0 < c.length && (c = this.Jh + c, this.Mw = c.length, this.string = c);
        -1 != a && this.KP()
    }, wG: A("Jh"), a6: u(), Y5: u(), Z5: u(), X5: u()
});
M = F.hC.prototype;
F.k(M, "charCount", M.S3);
F.k(M, "placeHolder", M.H4, M.Lu);
F.hC.create = function (a, c, d, e, f) {
    return new F.hC(a, c, d, e, f)
};
F.lx = F.OU;
F.Ns = "Arial";
F.bN = r;
F.Vc = F.n.extend({
    Af: r, Ne: q, Wc: q, Rs: r, Mb: "MenuItem", ctor: function (a, c) {
        var d = F.n.prototype;
        d.ctor.call(this);
        this.Wc = this.Ne = q;
        this.Af = this.Rs = r;
        d.Zl.call(this, 0.5, 0.5);
        this.Ne = c || q;
        if (this.Wc = a || q)this.Af = p
    }, Oia: A("Rs"), Of: u(), ng: D(r), pj: function (a, c) {
        this.Ne = c;
        this.Wc = a
    }, isEnabled: A("Af"), Wd: z("Af"), $t: function (a, c) {
        this.anchorY = this.anchorX = 0.5;
        this.Ne = c;
        this.Wc = a;
        this.Af = p;
        this.Rs = r;
        return p
    }, rect: function () {
        var a = this.xa, c = this.S, d = this.jd;
        return F.rect(a.x - c.width * d.x, a.y - c.height * d.y, c.width,
            c.height)
    }, selected: function () {
        this.Rs = p
    }, qi: function () {
        this.Rs = r
    }, gna: function (a, c) {
        this.Ne = c;
        this.Wc = a
    }, Et: function () {
        if (this.Af) {
            var a = this.Ne, c = this.Wc;
            if (c)if (a && F.Dd(c))a[c](this); else a && F.ac(c) ? c.call(a, this) : c(this)
        }
    }
});
M = F.Vc.prototype;
F.k(M, "enabled", M.isEnabled, M.Wd);
F.Vc.create = function (a, c) {
    return new F.Vc(a, c)
};
F.pm = F.Vc.extend({
    Yo: q, vd: q, b0: 0, rs: q, ctor: function (a, c, d) {
        F.Vc.prototype.ctor.call(this, c, d);
        this.vd = this.Yo = q;
        this.b0 = 0;
        this.rs = q;
        a && (this.$s = 1, this.rs = F.color.WHITE, this.Yo = F.color(126, 126, 126), this.kA(a), this.cascadeOpacity = this.cascadeColor = p)
    }, a4: A("Yo"), e8: z("Yo"), p4: A("vd"), kA: function (a) {
        a && (this.F(a), a.anchorX = 0, a.anchorY = 0, this.width = a.width, this.height = a.height);
        this.vd && this.removeChild(this.vd, p);
        this.vd = a
    }, Wd: function (a) {
        if (this.Af != a) {
            var c = this.vd;
            a ? c.color = this.rs : (this.rs = c.color,
                c.color = this.Yo)
        }
        F.Vc.prototype.Wd.call(this, a)
    }, wb: function (a) {
        this.vd.opacity = a
    }, jh: function () {
        return this.vd.opacity
    }, rb: function (a) {
        this.vd.color = a
    }, mg: function () {
        return this.vd.color
    }, QQ: function (a, c, d) {
        this.$t(c, d);
        this.$s = 1;
        this.rs = F.color.WHITE;
        this.Yo = F.color(126, 126, 126);
        this.kA(a);
        return this.cascadeOpacity = this.cascadeColor = p
    }, yc: function (a) {
        this.vd.string = a;
        this.width = this.vd.width;
        this.height = this.vd.height
    }, Ll: function () {
        return this.vd.string
    }, Et: function () {
        this.Af && (this.eI(), this.scale =
            this.$s, F.Vc.prototype.Et.call(this))
    }, selected: function () {
        if (this.Af) {
            F.Vc.prototype.selected.call(this);
            var a = this.uG(F.tw);
            a ? this.t9(a) : this.$s = this.scale;
            a = F.Ud(0.1, 1.2 * this.$s);
            a.rA(F.tw);
            this.Da(a)
        }
    }, qi: function () {
        if (this.Af) {
            F.Vc.prototype.qi.call(this);
            this.u9(F.tw);
            var a = F.Ud(0.1, this.$s);
            a.rA(F.tw);
            this.Da(a)
        }
    }
});
M = F.pm.prototype;
F.k(M, "string", M.Ll, M.yc);
F.k(M, "disabledColor", M.a4, M.e8);
F.k(M, "label", M.p4, M.kA);
F.pm.create = function (a, c, d) {
    return new F.pm(a, c, d)
};
F.fJ = F.pm.extend({
    ctor: function (a, c, d, e, f, g, h) {
        var m;
        a && 0 < a.length && (m = new F.ui(a, c, d, e, f));
        F.pm.prototype.ctor.call(this, m, g, h)
    }, Td: function (a, c, d, e, f, g, h) {
        (!a || 0 == a.length) && b("cc.MenuItemAtlasFont.initWithString(): value should be non-null and its length should be greater than 0");
        var m = new F.ui;
        m.Td(a, c, d, e, f);
        this.QQ(m, g, h);
        return p
    }
});
F.fJ.create = function (a, c, d, e, f, g, h) {
    return new F.fJ(a, c, d, e, f, g, h)
};
F.om = F.pm.extend({
    fc: q, rc: q, ctor: function (a, c, d) {
        var e;
        a && 0 < a.length ? (this.rc = F.Ns, this.fc = F.lx, e = new F.R(a, this.rc, this.fc)) : (this.fc = 0, this.rc = "");
        F.pm.prototype.ctor.call(this, e, c, d)
    }, Td: function (a, c, d) {
        (!a || 0 == a.length) && b("Value should be non-null and its length should be greater than 0");
        this.rc = F.Ns;
        this.fc = F.lx;
        a = new F.R(a, this.rc, this.fc);
        this.QQ(a, c, d);
        return p
    }, jA: function (a) {
        this.fc = a;
        this.$N()
    }, AG: A("fc"), iA: function (a) {
        this.rc = a;
        this.$N()
    }, zG: A("rc"), $N: function () {
        var a = new F.R(this.vd.string,
            this.rc, this.fc);
        this.kA(a)
    }
});
F.om.jA = function (a) {
    F.lx = a
};
F.om.fontSize = function () {
    return F.lx
};
F.om.iA = function (a) {
    F.bN && (F.Ns = "");
    F.Ns = a;
    F.bN = p
};
M = F.om.prototype;
F.k(M, "fontSize", M.AG, M.jA);
F.k(M, "fontName", M.zG, M.iA);
F.om.fontName = function () {
    return F.Ns
};
F.om.create = function (a, c, d) {
    return new F.om(a, c, d)
};
F.yo = F.Vc.extend({
    Md: q, cf: q, Ye: q, ctor: function (a, c, d, e, f) {
        F.Vc.prototype.ctor.call(this);
        this.Ye = this.cf = this.Md = q;
        if (c !== k) {
            var g, h, m;
            f !== k ? (g = d, m = e, h = f) : e !== k && F.ac(e) ? (g = d, m = e) : e !== k && F.ac(d) ? (h = e, m = d, g = new F.I(c)) : d === k && (g = new F.I(c));
            this.SQ(a, c, g, m, h)
        }
    }, B4: A("Md"), SH: function (a) {
        this.Md != a && (a && (this.F(a, 0, F.nV), a.anchorX = 0, a.anchorY = 0), this.Md && this.removeChild(this.Md, p), this.Md = a, this.width = this.Md.width, this.height = this.Md.height, this.sy(), a.Vu && !a.Ca && a.addEventListener("load", function (a) {
            this.width =
                a.width;
            this.height = a.height
        }, this))
    }, R4: A("cf"), WH: function (a) {
        this.cf != a && (a && (this.F(a, 0, F.aW), a.anchorX = 0, a.anchorY = 0), this.cf && this.removeChild(this.cf, p), this.cf = a, this.sy())
    }, b4: A("Ye"), QH: function (a) {
        this.Ye != a && (a && (this.F(a, 0, F.cU), a.anchorX = 0, a.anchorY = 0), this.Ye && this.removeChild(this.Ye, p), this.Ye = a, this.sy())
    }, SQ: function (a, c, d, e, f) {
        this.$t(e, f);
        this.SH(a);
        this.WH(c);
        this.QH(d);
        if (a = this.Md)this.width = a.width, this.height = a.height, a.Vu && !a.Ca && a.addEventListener("load", function (a) {
            this.width =
                a.width;
            this.height = a.height;
            this.cascadeOpacity = this.cascadeColor = p
        }, this);
        return this.cascadeOpacity = this.cascadeColor = p
    }, rb: function (a) {
        this.Md.color = a;
        this.cf && (this.cf.color = a);
        this.Ye && (this.Ye.color = a)
    }, mg: function () {
        return this.Md.color
    }, wb: function (a) {
        this.Md.opacity = a;
        this.cf && (this.cf.opacity = a);
        this.Ye && (this.Ye.opacity = a)
    }, jh: function () {
        return this.Md.opacity
    }, selected: function () {
        F.Vc.prototype.selected.call(this);
        this.Md && (this.Ye && (this.Ye.visible = r), this.cf ? (this.Md.visible = r, this.cf.visible =
            p) : this.Md.visible = p)
    }, qi: function () {
        F.Vc.prototype.qi.call(this);
        this.Md && (this.Md.visible = p, this.cf && (this.cf.visible = r), this.Ye && (this.Ye.visible = r))
    }, Wd: function (a) {
        this.Af != a && (F.Vc.prototype.Wd.call(this, a), this.sy())
    }, sy: function () {
        var a = this.Md, c = this.cf, d = this.Ye;
        this.Af ? (a && (a.visible = p), c && (c.visible = r), d && (d.visible = r)) : d ? (a && (a.visible = r), c && (c.visible = r), d && (d.visible = p)) : (a && (a.visible = p), c && (c.visible = r))
    }
});
M = F.yo.prototype;
F.k(M, "normalImage", M.B4, M.SH);
F.k(M, "selectedImage", M.R4, M.WH);
F.k(M, "disabledImage", M.b4, M.QH);
F.yo.create = function (a, c, d, e, f) {
    return new F.yo(a, c, d, e, f || k)
};
F.wj = F.yo.extend({
    ctor: function (a, c, d, e, f) {
        var g = q, h = q, m = q, n = q, s = q;
        a === k ? F.yo.prototype.ctor.call(this) : (g = new F.I(a), c && (h = new F.I(c)), e === k ? n = d : f === k ? (n = d, s = e) : f && (m = new F.I(d), n = e, s = f), F.yo.prototype.ctor.call(this, g, h, m, n, s))
    }, Xna: function (a) {
        this.SH(new F.I(a))
    }, toa: function (a) {
        this.WH(new F.I(a))
    }, vna: function (a) {
        this.QH(new F.I(a))
    }, qia: function (a, c, d, e, f) {
        var g = q, h = q, m = q;
        a && (g = new F.I(a));
        c && (h = new F.I(c));
        d && (m = new F.I(d));
        return this.SQ(g, h, m, e, f)
    }
});
F.wj.create = function (a, c, d, e, f) {
    return new F.wj(a, c, d, e, f)
};
F.mB = F.Vc.extend({
    Yd: q, Sh: 0, Je: q, qe: q, ctor: function () {
        F.Vc.prototype.ctor.call(this);
        this.Sh = 0;
        this.Yd = [];
        this.Je = 0;
        this.qe = F.color.WHITE;
        0 < arguments.length && this.VG(Array.prototype.slice.apply(arguments))
    }, jh: A("Je"), wb: function (a) {
        this.Je = a;
        if (this.Yd && 0 < this.Yd.length)for (var c = 0; c < this.Yd.length; c++)this.Yd[c].opacity = a;
        this.qe.a = a
    }, mg: function () {
        var a = this.qe;
        return F.color(a.r, a.g, a.b, a.a)
    }, rb: function (a) {
        var c = this.qe;
        c.r = a.r;
        c.g = a.g;
        c.b = a.b;
        if (this.Yd && 0 < this.Yd.length)for (c = 0; c < this.Yd.length; c++)this.Yd[c].rb(a);
        a.a !== k && !a.Q1 && this.wb(a.a)
    }, S4: A("Sh"), pA: function (a) {
        if (a != this.Sh) {
            this.Sh = a;
            (a = this.di(F.DI)) && a.ni(r);
            a = this.Yd[this.Sh];
            this.F(a, 0, F.DI);
            var c = a.width, d = a.height;
            this.width = c;
            this.height = d;
            a.X(c / 2, d / 2)
        }
    }, Hha: A("Yd"), zoa: z("Yd"), VG: function (a) {
        var c = a.length;
        F.ac(a[a.length - 2]) ? (this.$t(a[a.length - 2], a[a.length - 1]), c -= 2) : F.ac(a[a.length - 1]) ? (this.$t(a[a.length - 1], q), c -= 1) : this.$t(q, q);
        for (var d = this.Yd, e = d.length = 0; e < c; e++)a[e] && d.push(a[e]);
        this.Sh = F.ZK;
        this.pA(0);
        return this.cascadeOpacity =
            this.cascadeColor = p
    }, lda: function (a) {
        this.Yd.push(a)
    }, Et: function () {
        this.Af && this.pA((this.Sh + 1) % this.Yd.length);
        F.Vc.prototype.Et.call(this)
    }, selected: function () {
        F.Vc.prototype.selected.call(this);
        this.Yd[this.Sh].selected()
    }, qi: function () {
        F.Vc.prototype.qi.call(this);
        this.Yd[this.Sh].qi()
    }, Wd: function (a) {
        if (this.Af != a) {
            F.Vc.prototype.Wd.call(this, a);
            var c = this.Yd;
            if (c && 0 < c.length)for (var d = 0; d < c.length; d++)c[d].enabled = a
        }
    }, Uma: function () {
        return this.Yd[this.Sh]
    }, ba: function () {
        F.n.prototype.ba.call(this);
        this.pA(this.Sh)
    }
});
M = F.mB.prototype;
F.k(M, "selectedIndex", M.S4, M.pA);
F.mB.create = function () {
    0 < arguments.length && arguments[arguments.length - 1] == q && F.log("parameters should not be ending with null in Javascript");
    var a = new F.mB;
    a.VG(Array.prototype.slice.apply(arguments));
    return a
};
F.Mr = 0;
F.Lr = 1;
F.Saa = -128;
F.JI = 5;
F.vi = F.ec.extend({
    enabled: r, md: q, Vh: -1, OO: q, Mb: "Menu", ctor: function (a) {
        F.ec.prototype.ctor.call(this);
        this.qe = F.color.WHITE;
        this.enabled = r;
        this.Je = 255;
        this.md = q;
        this.Vh = -1;
        this.OO = F.Oa.create({event: F.Oa.jw, Uu: p, Nq: this.W_, su: this.$_, ru: this.Y_, qu: this.X_});
        0 < arguments.length && arguments[arguments.length - 1] == q && F.log("parameters should not be ending with null in Javascript");
        var c = arguments.length, d;
        if (0 == c)d = []; else if (1 == c)d = a instanceof Array ? a : [a]; else if (1 < c) {
            d = [];
            for (var e = 0; e < c; e++)arguments[e] &&
            d.push(arguments[e])
        }
        this.MQ(d)
    }, ba: function () {
        var a = this.OO;
        a.Df || F.La.addListener(a, this);
        F.n.prototype.ba.call(this)
    }, isEnabled: A("enabled"), Wd: z("enabled"), VG: function (a) {
        var c = [];
        if (a)for (var d = 0; d < a.length; d++)a[d] && c.push(a[d]);
        return this.MQ(c)
    }, MQ: function (a) {
        if (F.ec.prototype.oa.call(this)) {
            this.enabled = p;
            var c = F.dv;
            this.X(c.width / 2, c.height / 2);
            this.ze(c);
            this.Zl(0.5, 0.5);
            this.hz(p);
            if (a)for (c = 0; c < a.length; c++)this.F(a[c], c);
            this.md = q;
            this.Vh = F.Mr;
            return this.cascadeOpacity = this.cascadeColor =
                p
        }
        return r
    }, F: function (a, c, d) {
        a instanceof F.Vc || b("cc.Menu.addChild() : Menu only supports MenuItem objects as children");
        F.ec.prototype.F.call(this, a, c, d)
    }, xda: function () {
        this.f2(F.JI)
    }, f2: function (a) {
        var c = -a, d = this.u, e, f, g, h;
        if (d && 0 < d.length) {
            f = 0;
            for (e = d.length; f < e; f++)c += d[f].height * d[f].scaleY + a;
            var m = c / 2;
            f = 0;
            for (e = d.length; f < e; f++)h = d[f], g = h.height, c = h.scaleY, h.X(0, m - g * c / 2), m -= g * c + a
        }
    }, uda: function () {
        this.e2(F.JI)
    }, e2: function (a) {
        var c = -a, d = this.u, e, f, g, h;
        if (d && 0 < d.length) {
            e = 0;
            for (f = d.length; e <
            f; e++)c += d[e].width * d[e].scaleX + a;
            var m = -c / 2;
            e = 0;
            for (f = d.length; e < f; e++)h = d[e], c = h.scaleX, g = d[e].width, h.X(m + g * c / 2, 0), m += g * c + a
        }
    }, vda: function () {
        0 < arguments.length && arguments[arguments.length - 1] == q && F.log("parameters should not be ending with null in Javascript");
        for (var a = [], c = 0; c < arguments.length; c++)a.push(arguments[c]);
        var d = -5, e = 0, f = 0, g = 0, h, m, n, s = this.u;
        if (s && 0 < s.length) {
            c = 0;
            for (n = s.length; c < n; c++)if (!(e >= a.length) && (h = a[e]))m = s[c].height, f = f >= m || isNaN(m) ? f : m, ++g, g >= h && (d += f + 5, f = g = 0, ++e)
        }
        var t =
            F.L.Pa(), v = h = f = e = 0, w = 0, d = d / 2;
        if (s && 0 < s.length) {
            c = 0;
            for (n = s.length; c < n; c++) {
                var x = s[c];
                0 == h && (h = a[e], w = v = t.width / (1 + h));
                m = x.dl();
                f = f >= m || isNaN(m) ? f : m;
                x.X(w - t.width / 2, d - m / 2);
                w += v;
                ++g;
                g >= h && (d -= f + 5, f = h = g = 0, ++e)
            }
        }
    }, wda: function () {
        0 < arguments.length && arguments[arguments.length - 1] == q && F.log("parameters should not be ending with null in Javascript");
        var a = [], c;
        for (c = 0; c < arguments.length; c++)a.push(arguments[c]);
        var d = [], e = [], f = -10, g = -5, h = 0, m = 0, n = 0, s, t, v, w, x = this.u;
        if (x && 0 < x.length) {
            c = 0;
            for (v = x.length; c <
            v; c++)if (t = x[c], !(h >= a.length) && (s = a[h]))w = t.width, m = m >= w || isNaN(w) ? m : w, g += t.height + 5, ++n, n >= s && (d.push(m), e.push(g), f += m + 10, m = n = 0, g = -5, ++h)
        }
        g = F.L.Pa();
        s = m = h = 0;
        var f = -f / 2, y = 0;
        if (x && 0 < x.length) {
            c = 0;
            for (v = x.length; c < v; c++)t = x[c], 0 == s && (s = a[h], y = e[h]), w = t.Gh(), m = m >= w || isNaN(w) ? m : w, t.X(f + d[h] / 2, y - g.height / 2), y -= t.height + 10, ++n, n >= s && (f += m + 5, m = s = n = 0, ++h)
        }
    }, removeChild: function (a, c) {
        a != q && (a instanceof F.Vc ? (this.md == a && (this.md = q), F.n.prototype.removeChild.call(this, a, c)) : F.log("cc.Menu.removeChild():Menu only supports MenuItem objects as children"))
    },
    W_: function (a, c) {
        var d = c.be;
        if (d.Vh != F.Mr || !d.Jc || !d.enabled)return r;
        for (var e = d.parent; e != q; e = e.parent)if (!e.Jc)return r;
        d.md = d.vN(a);
        return d.md ? (d.Vh = F.Lr, d.md.selected(), p) : r
    }, Y_: function (a, c) {
        var d = c.be;
        d.Vh !== F.Lr ? F.log("cc.Menu.onTouchEnded(): invalid state") : (d.md && (d.md.qi(), d.md.Et()), d.Vh = F.Mr)
    }, X_: function (a, c) {
        var d = c.be;
        d.Vh !== F.Lr ? F.log("cc.Menu.onTouchCancelled(): invalid state") : (this.md && d.md.qi(), d.Vh = F.Mr)
    }, $_: function (a, c) {
        var d = c.be;
        if (d.Vh !== F.Lr)F.log("cc.Menu.onTouchMoved(): invalid state");
        else {
            var e = d.vN(a);
            e != d.md && (d.md && d.md.qi(), d.md = e, d.md && d.md.selected())
        }
    }, Cb: function () {
        this.Vh == F.Lr && (this.md && (this.md.qi(), this.md = q), this.Vh = F.Mr);
        F.n.prototype.Cb.call(this)
    }, Of: u(), ng: D(r), vN: function (a) {
        a = a.Tt();
        var c = this.u, d;
        if (c && 0 < c.length)for (var e = c.length - 1; 0 <= e; e--)if (d = c[e], d.Jc && d.isEnabled()) {
            var f = d.$F(a), g = d.rect();
            g.x = 0;
            g.y = 0;
            if (F.KR(g, f))return d
        }
        return q
    }
});
M = F.vi.prototype;
F.vi.create = function (a) {
    var c = arguments.length;
    0 < c && arguments[c - 1] == q && F.log("parameters should not be ending with null in Javascript");
    return 0 == c ? new F.vi : 1 == c ? new F.vi(a) : new F.vi(Array.prototype.slice.call(arguments, 0))
};
F.xba = 0;
F.tba = 1;
F.wba = 2;
F.uba = 3;
F.vba = 4;
F.sba = 5;
F.zpa = function (a, c, d) {
    var e = 2;
    if (e + 1 > c)return r;
    a = new F.DT(a);
    a.lA(e);
    d.type = a.W();
    e += 10;
    if (e + 4 + 1 > c)return r;
    a.lA(e);
    d.width = a.r7();
    d.height = a.q7();
    d.pixelDepth = a.W();
    if (e + 5 + 1 > c)return r;
    c = a.W();
    d.$P = 0;
    c & 32 && (d.$P = 1);
    return p
};
F.Apa = function (a, c, d) {
    var e, f;
    e = 0 | d.pixelDepth / 2;
    f = d.height * d.width * e;
    if (18 + f > c)return r;
    d.Kf = F.xw(a, 18, 18 + f);
    if (3 <= e)for (a = 0; a < f; a += e)c = d.Kf[a], d.Kf[a] = d.Kf[a + 2], d.Kf[a + 2] = c;
    return p
};
F.Cpa = function (a) {
    var c, d;
    if (8 !== a.pixelDepth) {
        var e = a.pixelDepth / 8, f = new Uint8Array(a.height * a.width);
        if (f !== q) {
            for (d = c = 0; d < a.width * a.height; c += e, d++)f[d] = 0.3 * a.Kf[c] + 0.59 * a.Kf[c + 1] + 0.11 * a.Kf[c + 2];
            a.pixelDepth = 8;
            a.type = 3;
            a.Kf = f
        }
    }
};
F.xpa = function (a) {
    a && (a.Kf = q)
};
F.Bpa = function (a, c, d) {
    var e, f, g, h = 0, m = 0, n = 0, s = [], t = 0, v = 18;
    e = d.pixelDepth / 8;
    f = d.height * d.width;
    for (g = 0; g < f; g++) {
        if (0 != t)t--, m = 0 != n; else {
            if (v + 1 > c)break;
            t = a[v];
            v += 1;
            (n = t & 128) && (t -= 128);
            m = 0
        }
        if (!m) {
            if (v + e > c)break;
            s = F.xw(a, v, v + e);
            v += e;
            3 <= e && (m = s[0], s[0] = s[2], s[2] = m)
        }
        for (m = 0; m < e; m++)d.Kf[h + m] = s[m];
        h += e
    }
    return p
};
F.ypa = function (a) {
    for (var c = a.width * (a.pixelDepth / 8), d = 0; d < a.height / 2; d++) {
        var e = F.xw(a.Kf, d * c, d * c + c);
        F.kL(F.xw(a.Kf, (a.height - (d + 1)) * c, c), a.Kf, d * c);
        F.kL(e, a.Kf, (a.height - (d + 1)) * c)
    }
    a.$P = 0
};
F.xw = function (a, c, d) {
    return a instanceof Array ? a.slice(c, d) : a.subarray(c, d)
};
F.kL = function (a, c, d) {
    for (var e = 0; e < a.length; e++)c[d + e] = a[e]
};
F.DT = F.za.extend({
    Ko: q, Ia: 0, ctor: z("Ko"), dna: function (a) {
        this.Ko = a;
        this.Ia = 0
    }, Efa: A("Ko"), NY: function (a) {
        this.Ia + Math.ceil(a / 8) < this.Xo.length || b(Error("Index out of bound"))
    }, eZ: function (a, c) {
        var d = a + c + 1, e = d >> 3;
        this.NY(d);
        var d = Math.pow(2, c - 1) - 1, f = this.rE(a + c, 1, e), g = this.rE(a, c, e), h = 0, m = 2, n = 0;
        do for (var s = this.Rx(++n, e), t = a % 8 || 8, v = 1 << t; v >>= 1;)s & v && (h += 1 / m), m *= 2; while (a -= t);
        this.Ia += e;
        return g == (d << 1) + 1 ? h ? NaN : f ? -Infinity : Infinity : (1 + -2 * f) * (g || h ? !g ? Math.pow(2, -d + 1) * h : Math.pow(2, g - d) * (1 + h) : 0)
    }, Rx: function (a,
                     c) {
        return this.Xo[this.Ia + c - a - 1]
    }, Sw: function (a, c) {
        var d = this.rE(0, a, a / 8), e = Math.pow(2, a);
        this.Ia += a / 8;
        return c && d >= e / 2 ? d - e : d
    }, k1: function (a, c) {
        for (++c; --c; a = 1073741824 == ((a %= 2147483648) & 1073741824) ? 2 * a : 2 * (a - 1073741824) + 2147483648);
        return a
    }, rE: function (a, c, d) {
        var e = (a + c) % 8, f = a % 8, g = d - (a >> 3) - 1;
        a = d + (-(a + c) >> 3);
        var h = g - a;
        c = this.Rx(g, d) >> f & (1 << (h ? 8 - f : c)) - 1;
        for (h && e && (c += (this.Rx(a++, d) & (1 << e) - 1) << (h-- << 3) - f); h;)c += this.k1(this.Rx(a++, d), (h-- << 3) - f);
        return c
    }, ema: function () {
        return this.Sw(32, p)
    }, q7: function () {
        return this.Sw(32,
            r)
    }, gma: function () {
        return this.eZ(23, 8)
    }, fma: function () {
        return this.Sw(16, p)
    }, r7: function () {
        return this.Sw(16, r)
    }, W: function () {
        var a = this.Xo[this.Ia];
        this.Ia += 1;
        return a
    }, bma: function (a, c) {
        return this.Ko instanceof Array ? this.Ko.slice(a, c) : this.Ko.subarray(a, c)
    }, lA: z("Ia"), ik: A("Ia")
});
F.hw = 0;
F.Hk = 1;
F.gw = 2;
F.VB = F.n.extend({
    Yb: q, rH: q, Mq: q, xd: q, Vb: q, ak: q, Mb: "TMXTiledMap", ctor: function (a, c) {
        F.n.prototype.ctor.call(this);
        this.xd = F.size(0, 0);
        this.Vb = F.size(0, 0);
        c !== k ? this.YG(a, c) : a !== k && this.XG(a)
    }, CG: function () {
        return F.size(this.xd.width, this.xd.height)
    }, nS: function (a) {
        this.xd.width = a.width;
        this.xd.height = a.height
    }, rD: function () {
        return this.xd.width
    }, NE: function (a) {
        this.xd.width = a
    }, qD: function () {
        return this.xd.height
    }, ME: function (a) {
        this.xd.height = a
    }, Bq: function () {
        return F.size(this.Vb.width, this.Vb.height)
    },
    CS: function (a) {
        this.Vb.width = a.width;
        this.Vb.height = a.height
    }, Ms: function () {
        return this.Vb.width
    }, lt: function (a) {
        this.Vb.width = a
    }, Ls: function () {
        return this.Vb.height
    }, kt: function (a) {
        this.Vb.height = a
    }, Lga: A("rH"), Tna: z("rH"), kQ: A("Mq"), oS: z("Mq"), Wy: A("Yb"), nA: z("Yb"), XG: function (a) {
        (!a || 0 == a.length) && b("cc.TMXTiledMap.initWithTMXFile(): tmxFile should be non-null or non-empty string.");
        this.height = this.width = 0;
        a = new F.Yr(a);
        if (!a)return r;
        var c = a.nn;
        (!c || 0 === c.length) && F.log("cc.TMXTiledMap.initWithTMXFile(): Map not found. Please check the filename.");
        this.zL(a);
        return p
    }, YG: function (a, c) {
        this.height = this.width = 0;
        var d = new F.Yr(a, c), e = d.nn;
        (!e || 0 === e.length) && F.log("cc.TMXTiledMap.initWithXML(): Map not found. Please check the filename.");
        this.zL(d);
        return p
    }, zL: function (a) {
        this.xd = a.CG();
        this.Vb = a.Bq();
        this.rH = a.orientation;
        this.Mq = a.kQ();
        this.Yb = a.Yb;
        this.ak = a.ak;
        var c = 0, d = a.Ie;
        if (d)for (var e = q, f = 0, g = d.length; f < g; f++)if ((e = d[f]) && e.visible)e = this.f0(e, a), this.F(e, c, c), this.width = Math.max(this.width, e.width), this.height = Math.max(this.height, e.height),
            c++
    }, yda: function () {
        for (var a = [], c = this.u, d = 0, e = c.length; d < e; d++) {
            var f = c[d];
            f && f instanceof F.Xr && a.push(f)
        }
        return a
    }, Bga: function (a) {
        (!a || 0 === a.length) && b("cc.TMXTiledMap.getLayer(): layerName should be non-null or non-empty string.");
        for (var c = this.u, d = 0; d < c.length; d++) {
            var e = c[d];
            if (e && e.Ez == a)return e
        }
        return q
    }, Vga: function (a) {
        (!a || 0 === a.length) && b("cc.TMXTiledMap.getObjectGroup(): groupName should be non-null or non-empty string.");
        if (this.Mq)for (var c = 0; c < this.Mq.length; c++) {
            var d = this.Mq[c];
            if (d && d.Yt == a)return d
        }
        return q
    }, GG: function (a) {
        return this.Yb[a.toString()]
    }, Yla: function (a) {
        F.log("propertiesForGID is deprecated. Please use getPropertiesForGID instead.");
        return this.J4[a]
    }, J4: function (a) {
        return this.ak[a]
    }, f0: function (a, c) {
        var d = this.z1(a, c), d = new F.Xr(d, a, c);
        a.wR = r;
        d.Y8();
        return d
    }, z1: function (a, c) {
        var d = a.Ea, e = c.nn;
        if (e)for (var f = e.length - 1; 0 <= f; f--) {
            var g = e[f];
            if (g)for (var h = 0; h < d.height; h++)for (var m = 0; m < d.width; m++) {
                var n = a.df[m + d.width * h];
                if (0 != n && (n & F.WB) >>> 0 >= g.Ln)return g
            }
        }
        F.log("cocos2d: Warning: TMX Layer " +
            a.name + " has no tiles");
        return q
    }
});
M = F.VB.prototype;
F.k(M, "mapWidth", M.rD, M.NE);
F.k(M, "mapHeight", M.qD, M.ME);
F.k(M, "tileWidth", M.Ms, M.lt);
F.k(M, "tileHeight", M.Ls, M.kt);
F.VB.create = function (a, c) {
    return new F.VB(a, c)
};
F.mX = 0;
F.Aba = 1;
F.zba = 2;
F.Cba = 3;
F.Bba = 4;
F.Dba = 5;
F.Zr = 2147483648;
F.$r = 1073741824;
F.kK = 536870912;
F.lK = (F.Zr | F.$r | F.kK) >>> 0;
F.WB = ~F.lK >>> 0;
F.um = F.za.extend({
    Yb: q,
    name: "",
    Ea: q,
    df: q,
    visible: q,
    Je: q,
    wR: p,
    Qm: 1E5,
    Pm: 0,
    offset: q,
    ctor: function () {
        this.Yb = [];
        this.name = "";
        this.Ea = q;
        this.df = [];
        this.visible = p;
        this.Je = 0;
        this.wR = p;
        this.Qm = 1E5;
        this.Pm = 0;
        this.offset = F.d(0, 0)
    },
    Wy: A("Yb"),
    nA: z("Yb")
});
F.lX = F.za.extend({
    name: "", Ln: 0, Vb: q, Ru: 0, margin: 0, HS: "", iz: q, ctor: function () {
        this.Vb = F.size(0, 0);
        this.iz = F.size(0, 0)
    }, Eu: function (a) {
        var c = F.rect(0, 0, 0, 0);
        c.width = this.Vb.width;
        c.height = this.Vb.height;
        a &= F.WB;
        a -= parseInt(this.Ln, 10);
        var d = parseInt((this.iz.width - 2 * this.margin + this.Ru) / (this.Vb.width + this.Ru), 10);
        c.x = parseInt(a % d * (this.Vb.width + this.Ru) + this.margin, 10);
        c.y = parseInt(parseInt(a / d, 10) * (this.Vb.height + this.Ru) + this.margin, 10);
        return c
    }
});
F.Yr = F.Wv.extend({
    Yb: q,
    orientation: q,
    parentElement: q,
    Sz: q,
    Dz: 0,
    gI: r,
    kr: q,
    bG: q,
    Fx: q,
    xd: q,
    Vb: q,
    Ie: q,
    nn: q,
    ak: q,
    Vx: "",
    KL: 0,
    ctor: function (a, c) {
        F.Wv.prototype.ctor.apply(this);
        this.xd = F.size(0, 0);
        this.Vb = F.size(0, 0);
        this.Ie = [];
        this.nn = [];
        this.Fx = [];
        this.Yb = [];
        this.ak = {};
        this.KL = 0;
        c !== k ? this.YG(a, c) : a !== k && this.XG(a)
    },
    Yga: A("orientation"),
    boa: z("orientation"),
    CG: function () {
        return F.size(this.xd.width, this.xd.height)
    },
    nS: function (a) {
        this.xd.width = a.width;
        this.xd.height = a.height
    },
    rD: function () {
        return this.xd.width
    },
    NE: function (a) {
        this.xd.width = a
    },
    qD: function () {
        return this.xd.height
    },
    ME: function (a) {
        this.xd.height = a
    },
    Bq: function () {
        return F.size(this.Vb.width, this.Vb.height)
    },
    CS: function (a) {
        this.Vb.width = a.width;
        this.Vb.height = a.height
    },
    Ms: function () {
        return this.Vb.width
    },
    lt: function (a) {
        this.Vb.width = a
    },
    Ls: function () {
        return this.Vb.height
    },
    kt: function (a) {
        this.Vb.height = a
    },
    Gga: A("Ie"),
    o8: function (a) {
        this.Ie.push(a)
    },
    Rha: A("nn"),
    P8: function (a) {
        this.nn.push(a)
    },
    kQ: A("Fx"),
    oS: function (a) {
        this.Fx.push(a)
    },
    bha: A("parentElement"),
    goa: z("parentElement"),
    cha: A("Sz"),
    hoa: z("Sz"),
    Cga: A("Dz"),
    Mna: z("Dz"),
    Gha: A("gI"),
    yoa: z("gI"),
    Wy: A("Yb"),
    nA: z("Yb"),
    XG: function (a) {
        this.mN(a, q);
        return this.wH(a)
    },
    YG: function (a, c) {
        this.mN(q, c);
        return this.X6(a)
    },
    wH: function (a, c) {
        var d = (c = c || r) ? a : F.aa.ge(a);
        d || b("Please load the resource first : " + a);
        var e, f, d = this.$D(d).documentElement;
        e = d.getAttribute("version");
        f = d.getAttribute("orientation");
        if ("map" == d.nodeName && ("1.0" != e && e !== q && F.log("cocos2d: TMXFormat: Unsupported TMX version:" + e), "orthogonal" ==
            f ? this.orientation = F.hw : "isometric" == f ? this.orientation = F.gw : "hexagonal" == f ? this.orientation = F.Hk : f !== q && F.log("cocos2d: TMXFomat: Unsupported orientation:" + f), e = F.size(0, 0), e.width = parseFloat(d.getAttribute("width")), e.height = parseFloat(d.getAttribute("height")), this.nS(e), e = F.size(0, 0), e.width = parseFloat(d.getAttribute("tilewidth")), e.height = parseFloat(d.getAttribute("tileheight")), this.CS(e), f = d.querySelectorAll("map \x3e properties \x3e  property"))) {
            var g = {};
            for (e = 0; e < f.length; e++)g[f[e].getAttribute("name")] =
                f[e].getAttribute("value");
            this.Yb = g
        }
        g = d.getElementsByTagName("tileset");
        "map" !== d.nodeName && (g = [], g.push(d));
        for (e = 0; e < g.length; e++) {
            f = g[e];
            var h = f.getAttribute("source");
            if (h)f = c ? F.path.join(this.Vx, h) : F.path.hq(a, h), this.wH(f); else {
                h = new F.lX;
                h.name = f.getAttribute("name") || "";
                h.Ln = parseInt(f.getAttribute("firstgid")) || 0;
                h.Ru = parseInt(f.getAttribute("spacing")) || 0;
                h.margin = parseInt(f.getAttribute("margin")) || 0;
                var m = F.size(0, 0);
                m.width = parseFloat(f.getAttribute("tilewidth"));
                m.height = parseFloat(f.getAttribute("tileheight"));
                h.Vb = m;
                var m = f.getElementsByTagName("image")[0].getAttribute("source"), n = -1;
                this.kr && (n = this.kr.lastIndexOf("/"));
                h.HS = -1 !== n ? this.kr.substr(0, n + 1) + m : this.Vx + (this.Vx ? "/" : "") + m;
                this.P8(h);
                if (m = f.getElementsByTagName("tile"))for (n = 0; n < m.length; n++) {
                    f = m[n];
                    this.Sz = parseInt(h.Ln) + parseInt(f.getAttribute("id") || 0);
                    var s = f.querySelectorAll("properties \x3e property");
                    if (s) {
                        var t = {};
                        for (f = 0; f < s.length; f++) {
                            var v = s[f].getAttribute("name");
                            t[v] = s[f].getAttribute("value")
                        }
                        this.ak[this.Sz] = t
                    }
                }
            }
        }
        if (g = d.getElementsByTagName("layer"))for (e =
                                                         0; e < g.length; e++) {
            m = g[e];
            n = m.getElementsByTagName("data")[0];
            h = new F.um;
            h.name = m.getAttribute("name");
            f = F.size(0, 0);
            f.width = parseFloat(m.getAttribute("width"));
            f.height = parseFloat(m.getAttribute("height"));
            h.Ea = f;
            f = m.getAttribute("visible");
            h.visible = "0" != f;
            f = m.getAttribute("opacity") || 1;
            h.Je = f ? parseInt(255 * parseFloat(f)) : 255;
            h.offset = F.d(parseFloat(m.getAttribute("x")) || 0, parseFloat(m.getAttribute("y")) || 0);
            s = "";
            for (f = 0; f < n.childNodes.length; f++)s += n.childNodes[f].nodeValue;
            s = s.trim();
            f = n.getAttribute("compression");
            t = n.getAttribute("encoding");
            if (f && "gzip" !== f && "zlib" !== f)return F.log("cc.TMXMapInfo.parseXMLFile(): unsupported compression method"), q;
            switch (f) {
                case "gzip":
                    h.df = F.bT(s, 4);
                    break;
                case "zlib":
                    f = new Zlib.to(F.ua.im.BP(s, 1));
                    h.df = F.P9(f.CP());
                    break;
                case q:
                case "":
                    if ("base64" == t)h.df = F.ua.im.BP(s, 4); else if ("csv" === t) {
                        h.df = [];
                        f = s.split(",");
                        for (n = 0; n < f.length; n++)h.df.push(parseInt(f[n]))
                    } else {
                        f = n.getElementsByTagName("tile");
                        h.df = [];
                        for (n = 0; n < f.length; n++)h.df.push(parseInt(f[n].getAttribute("gid")))
                    }
                    break;
                default:
                    this.Dz == F.um.qI && F.log("cc.TMXMapInfo.parseXMLFile(): Only base64 and/or gzip/zlib maps are supported")
            }
            if (m = m.querySelectorAll("properties \x3e property")) {
                n = {};
                for (f = 0; f < m.length; f++)n[m[f].getAttribute("name")] = m[f].getAttribute("value");
                h.Yb = n
            }
            this.o8(h)
        }
        if (g = d.getElementsByTagName("objectgroup"))for (e = 0; e < g.length; e++) {
            m = g[e];
            h = new F.kX;
            h.Yt = m.getAttribute("name");
            h.A8(F.d(parseFloat(m.getAttribute("x")) * this.Bq().width || 0, parseFloat(m.getAttribute("y")) * this.Bq().height || 0));
            if (n = m.querySelectorAll("objectgroup \x3e properties \x3e property"))for (f =
                                                                                             0; f < n.length; f++)s = {}, s[n[f].getAttribute("name")] = n[f].getAttribute("value"), h.Yb = s;
            if (m = m.querySelectorAll("object"))for (f = 0; f < m.length; f++) {
                s = m[f];
                n = {};
                n.name = s.getAttribute("name") || "";
                n.type = s.getAttribute("type") || "";
                n.x = parseInt(s.getAttribute("x") || 0) + h.mQ().x;
                t = parseInt(s.getAttribute("y") || 0) + h.mQ().y;
                n.width = parseInt(s.getAttribute("width")) || 0;
                n.height = parseInt(s.getAttribute("height")) || 0;
                n.y = parseInt(this.CG().height * this.Bq().height) - t - n.height;
                n.rotation = parseInt(s.getAttribute("rotation")) ||
                    0;
                if (t = s.querySelectorAll("properties \x3e property"))for (v = 0; v < t.length; v++)n[t[v].getAttribute("name")] = t[v].getAttribute("value");
                if ((t = s.querySelectorAll("polygon")) && 0 < t.length)(t = t[0].getAttribute("points")) && (n.polygonPoints = this.ON(t));
                if ((s = s.querySelectorAll("polyline")) && 0 < s.length)(s = s[0].getAttribute("points")) && (n.polylinePoints = this.ON(s));
                h.v8(n)
            }
            this.oS(h)
        }
        return d
    },
    ON: function (a) {
        if (!a)return q;
        var c = [];
        a = a.split(" ");
        for (var d = 0; d < a.length; d++) {
            var e = a[d].split(",");
            c.push({
                x: e[0],
                y: e[1]
            })
        }
        return c
    },
    X6: function (a) {
        return this.wH(a, p)
    },
    Oha: A("ak"),
    Ioa: function (a) {
        this.ak.push(a)
    },
    Tfa: A("bG"),
    qna: z("bG"),
    Iha: A("kr"),
    Boa: z("kr"),
    mN: function (a, c) {
        this.nn.length = 0;
        this.Ie.length = 0;
        this.kr = a;
        c && (this.Vx = c);
        this.Fx.length = 0;
        this.Yb.length = 0;
        this.ak.length = 0;
        this.bG = "";
        this.gI = r;
        this.Dz = F.um.qI;
        this.parentElement = F.mX;
        this.KL = 0
    }
});
M = F.Yr.prototype;
F.k(M, "mapWidth", M.rD, M.NE);
F.k(M, "mapHeight", M.qD, M.ME);
F.k(M, "tileWidth", M.Ms, M.lt);
F.k(M, "tileHeight", M.Ls, M.kt);
F.Yr.create = function (a, c) {
    return new F.Yr(a, c)
};
F.aa.nj(["tmx", "tsx"], F.SO);
F.um.qI = 1;
F.um.q$ = 2;
F.um.r$ = 4;
F.um.s$ = 8;
F.kX = F.za.extend({
    Yb: q, Yt: "", Ox: q, Ap: q, ctor: function () {
        this.Yt = "";
        this.Ox = F.d(0, 0);
        this.Yb = [];
        this.Ap = []
    }, mQ: function () {
        return F.d(this.Ox)
    }, A8: function (a) {
        this.Ox.x = a.x;
        this.Ox.y = a.y
    }, Wy: A("Yb"), nA: function (a) {
        this.Yb.push(a)
    }, wga: function () {
        return this.Yt.toString()
    }, Jna: z("Yt"), Zla: function (a) {
        return this.Yb[a]
    }, mla: function (a) {
        if (this.Ap && 0 < this.Ap.length)for (var c = this.Ap, d = 0, e = c.length; d < e; d++) {
            var f = c[d].name;
            if (f && f == a)return c[d]
        }
        return q
    }, Wga: A("Ap"), v8: function (a) {
        this.Ap.push(a)
    }
});
F.Xr = F.We.extend({
    me: q,
    zg: q,
    ij: q,
    Yb: q,
    Ez: "",
    Ea: q,
    gc: q,
    Je: 255,
    Qm: q,
    Pm: q,
    yF: q,
    sF: q,
    Yg: q,
    ne: q,
    Dh: q,
    ae: q,
    oe: q,
    MC: q,
    Mb: "TMXLayer",
    ctor: function (a, c, d) {
        F.We.prototype.ctor.call(this);
        this.ce = [];
        this.Ea = F.size(0, 0);
        this.gc = F.size(0, 0);
        if (F.B === F.Aa) {
            var e = F.lb, f = F.bc("canvas");
            f.width = e.width;
            f.height = e.height;
            this.ae = f;
            this.oe = this.ae.getContext("2d");
            var g = new F.ha;
            g.qd(f);
            g.Lb();
            this.MC = g;
            this.width = e.width;
            this.height = e.height;
            this.Uk = this
        }
        d !== k && this.A5(a, c, d)
    },
    de: function () {
        this.Z = F.B === F.Aa ? new F.fw(this) :
            new F.jK(this)
    },
    ze: function (a, c) {
        var d = this.S;
        F.n.prototype.ze.call(this, a, c);
        if (F.B === F.Aa) {
            var e = this.ae, f = F.Fb();
            e.width = 0 | 1.5 * d.width * f;
            e.height = 0 | 1.5 * d.height * f;
            this.ij === F.Hk ? this.oe.translate(0, e.height - 0.5 * this.gc.height) : this.oe.translate(0, e.height);
            d = this.MC.S;
            d.width = e.width;
            d.height = e.height;
            d = e.width * e.height;
            if (d > this.L_) {
                this.gF || (this.gF = []);
                this.MO || (this.MO = []);
                this.tt = Math.ceil(d / this.L_);
                d = this.gF;
                for (f = 0; f < this.tt; f++) {
                    d[f] || (d[f] = document.createElement("canvas"), this.MO[f] =
                        d[f].getContext("2d"));
                    var g = d[f];
                    g.width = this.NO = Math.round(e.width / this.tt);
                    g.height = e.height
                }
                for (f = this.tt; f < d.length; f++)g.width = 0, g.height = 0
            } else this.tt = 0
        }
    },
    Ma: q,
    Ks: A("MC"),
    H: q,
    sn: function () {
        var a, c, d = this.u;
        if (this.Jc && d && 0 !== d.length) {
            this.Ja && (this.Eh = this.Ja.Eh + 1);
            this.transform();
            if (this.pe) {
                var e = this.oe, f = this.ae, g = F.view, h = this.ma, m = F.ka;
                m.Rp(h);
                this.Tc();
                a = 0;
                for (c = d.length; a < c; a++)d[a] && (d[a].H(), d[a].pe = r);
                this.Z.bZ(m.qs[h]);
                e.save();
                e.clearRect(0, 0, f.width, -f.height);
                a = F.Zp(this.Ad);
                e.transform(a.a, a.s, a.b, a.z, a.P * g.sa, -a.Q * g.Ya);
                m.Ux(e, h);
                e.restore();
                this.pe = r
            }
            F.ka.xc(this.Z)
        }
    },
    Zg: function () {
        this.pe = p;
        -1 === F.ka.bj.indexOf(this) && F.ka.Bu(this);
        this.bn = p
    },
    Ka: q,
    Zo: function (a) {
        a = a || F.l;
        var c = 0 | -this.Ib.x, d = 0 | -this.Ib.y, e = F.view, f = this.ae;
        if (f) {
            var g = this.tt, h = f.height * e.Ya, m = 0.5 * this.gc.height * e.Ya;
            if (0 < g)for (var f = this.gF, n = 0; n < g; n++) {
                var s = f[n];
                this.ij === F.Hk ? a.drawImage(f[n], 0, 0, s.width, s.height, c + n * this.NO * e.sa, -(d + h) + m, s.width * e.sa, h) : a.drawImage(f[n], 0, 0, s.width, s.height,
                    c + n * this.NO * e.sa, -(d + h), s.width * e.sa, h)
            } else this.ij === F.Hk ? a.drawImage(f, 0, 0, f.width, f.height, c, -(d + h) + m, f.width * e.sa, h) : a.drawImage(f, 0, 0, f.width, f.height, c, -(d + h), f.width * e.sa, h)
        }
    },
    Fga: function () {
        return F.size(this.Ea.width, this.Ea.height)
    },
    Pna: function (a) {
        this.Ea.width = a.width;
        this.Ea.height = a.height
    },
    PZ: function () {
        return this.Ea.width
    },
    O0: function (a) {
        this.Ea.width = a
    },
    OZ: function () {
        return this.Ea.height
    },
    N0: function (a) {
        this.Ea.height = a
    },
    Mga: function () {
        return F.size(this.gc.width, this.gc.height)
    },
    Una: function (a) {
        this.gc.width = a.width;
        this.gc.height = a.height
    },
    Ms: function () {
        return this.gc.width
    },
    lt: function (a) {
        this.gc.width = a
    },
    Ls: function () {
        return this.gc.height
    },
    kt: function (a) {
        this.gc.height = a
    },
    Pha: A("me"),
    Joa: z("me"),
    Qha: A("zg"),
    Koa: z("zg"),
    Ega: A("ij"),
    Ona: z("ij"),
    Wy: A("Yb"),
    nA: z("Yb"),
    A5: function (a, c, d) {
        var e = c.Ea, f = 0.35 * parseInt(e.width * e.height) + 1, g;
        a && (g = F.Ra.ad(a.HS));
        return this.Fa(g, f) ? (this.Ez = c.name, this.Ea = e, this.me = c.df, this.Qm = c.Qm, this.Pm = c.Pm, this.Je = c.Je, this.Yb = c.Yb, this.Dh =
            F.L.Dh, this.zg = a, this.gc = d.Bq(), this.ij = d.orientation, a = this.HY(c.offset), this.X(F.xH(a)), this.ne = [], this.ze(F.FS(F.size(this.Ea.width * this.gc.width, this.Ea.height * this.gc.height))), this.sF = r, this.yF = 0, p) : r
    },
    oma: function () {
        this.me && (this.me = q);
        this.ne && (this.ne = q)
    },
    Nha: function (a, c) {
        a || b("cc.TMXLayer.getTileAt(): pos should be non-null");
        c !== k && (a = F.d(a, c));
        (a.x >= this.Ea.width || a.y >= this.Ea.height || 0 > a.x || 0 > a.y) && b("cc.TMXLayer.getTileAt(): invalid position");
        if (!this.me || !this.ne)return F.log("cc.TMXLayer.getTileAt(): TMXLayer: the tiles map has been released"),
            q;
        var d = q, e = this.KG(a);
        if (0 === e)return d;
        var f = 0 | a.x + a.y * this.Ea.width, d = this.di(f);
        d || (e = this.zg.Eu(e), e = F.Xl(e), d = new F.I, d.Fa(this.texture, e), d.batchNode = this, d.X(this.Vy(a)), d.vertexZ = this.aP(a), d.anchorX = 0, d.anchorY = 0, d.opacity = this.Je, e = this.DC(f), this.X1(d, e, f));
        return d
    },
    KG: function (a, c) {
        a || b("cc.TMXLayer.getTileGIDAt(): pos should be non-null");
        c !== k && (a = F.d(a, c));
        (a.x >= this.Ea.width || a.y >= this.Ea.height || 0 > a.x || 0 > a.y) && b("cc.TMXLayer.getTileGIDAt(): invalid position");
        return !this.me || !this.ne ?
            (F.log("cc.TMXLayer.getTileGIDAt(): TMXLayer: the tiles map has been released"), q) : (this.me[0 | a.x + a.y * this.Ea.width] & F.WB) >>> 0
    },
    Y4: function (a, c) {
        a || b("cc.TMXLayer.getTileFlagsAt(): pos should be non-null");
        c !== k && (a = F.d(a, c));
        (a.x >= this.Ea.width || a.y >= this.Ea.height || 0 > a.x || 0 > a.y) && b("cc.TMXLayer.getTileFlagsAt(): invalid position");
        return !this.me || !this.ne ? (F.log("cc.TMXLayer.getTileFlagsAt(): TMXLayer: the tiles map has been released"), q) : (this.me[0 | a.x + a.y * this.Ea.width] & F.lK) >>> 0
    },
    Hoa: function (a,
                   c, d, e) {
        c || b("cc.TMXLayer.setTileGID(): pos should be non-null");
        e !== k ? c = F.d(c, d) : e = d;
        (c.x >= this.Ea.width || c.y >= this.Ea.height || 0 > c.x || 0 > c.y) && b("cc.TMXLayer.setTileGID(): invalid position");
        if (!this.me || !this.ne)F.log("cc.TMXLayer.setTileGID(): TMXLayer: the tiles map has been released"); else if (0 !== a && a < this.zg.Ln)F.log("cc.TMXLayer.setTileGID(): invalid gid:" + a); else {
            e = e || 0;
            this.Zg();
            d = this.Y4(c);
            var f = this.KG(c);
            if (f != a || d != e)if (d = (a | e) >>> 0, 0 === a)this.C7(c); else if (0 === f)this.x_(d, c); else {
                var f =
                    c.x + c.y * this.Ea.width, g = this.di(f);
                g ? (a = this.zg.Eu(a), a = F.Xl(a), g.Db(a, r), e != q && this.ay(g, c, d), this.me[f] = d) : this.M1(d, c)
            }
        }
    },
    C7: function (a, c) {
        a || b("cc.TMXLayer.removeTileAt(): pos should be non-null");
        c !== k && (a = F.d(a, c));
        (a.x >= this.Ea.width || a.y >= this.Ea.height || 0 > a.x || 0 > a.y) && b("cc.TMXLayer.removeTileAt(): invalid position");
        if (!this.me || !this.ne)F.log("cc.TMXLayer.removeTileAt(): TMXLayer: the tiles map has been released"); else if (0 !== this.KG(a)) {
            F.B === F.Aa && this.Zg();
            var d = 0 | a.x + a.y * this.Ea.width,
                e = this.DC(d);
            this.me[d] = 0;
            this.ne.splice(e, 1);
            if (d = this.di(d))F.We.prototype.removeChild.call(this, d, p); else if (F.B === F.Y && this.textureAtlas.PR(e), this.u)for (var d = this.u, f = 0, g = d.length; f < g; f++) {
                var h = d[f];
                if (h) {
                    var m = h.atlasIndex;
                    m >= e && (h.atlasIndex = m - 1)
                }
            }
        }
    },
    Vy: function (a, c) {
        c !== k && (a = F.d(a, c));
        var d = F.d(0, 0);
        switch (this.ij) {
            case F.hw:
                d = this.l0(a);
                break;
            case F.gw:
                d = this.k0(a);
                break;
            case F.Hk:
                d = this.j0(a)
        }
        return F.xH(d)
    },
    GG: function (a) {
        return this.Yb[a]
    },
    Y8: function () {
        F.B === F.Aa ? this.zg.iz = this.Fc.S :
            (this.zg.iz = this.textureAtlas.texture.S, this.textureAtlas.texture.KH());
        this.e0();
        F.B === F.Aa && this.Zg();
        for (var a = this.Ea.height, c = this.Ea.width, d = 0; d < a; d++)for (var e = 0; e < c; e++) {
            var f = this.me[e + c * d];
            0 !== f && (this.zY(f, F.d(e, d)), this.Qm = Math.min(f, this.Qm), this.Pm = Math.max(f, this.Pm))
        }
        this.Pm >= this.zg.Ln && this.Qm >= this.zg.Ln || F.log("cocos2d:TMX: Only 1 tileset per layer is supported")
    },
    F: function () {
        F.log("addChild: is not supported on cc.TMXLayer. Instead use setTileGID or tileAt.")
    },
    removeChild: function (a,
                           c) {
        if (a)if (-1 === this.u.indexOf(a))F.log("cc.TMXLayer.removeChild(): Tile does not belong to TMXLayer"); else {
            F.B === F.Aa && this.Zg();
            var d = a.atlasIndex;
            this.me[this.ne[d]] = 0;
            this.ne.splice(d, 1);
            F.We.prototype.removeChild.call(this, a, c);
            F.ka.xe = p
        }
    },
    Dga: A("Ez"),
    Nna: z("Ez"),
    k0: function (a) {
        return F.d(this.gc.width / 2 * (this.Ea.width + a.x - a.y - 1), this.gc.height / 2 * (2 * this.Ea.height - a.x - a.y - 2))
    },
    l0: function (a) {
        return F.d(a.x * this.gc.width, (this.Ea.height - a.y - 1) * this.gc.height)
    },
    j0: function (a) {
        return F.d(3 * a.x *
            this.gc.width / 4, (this.Ea.height - a.y - 1) * this.gc.height + (1 == a.x % 2 ? -this.gc.height / 2 : 0))
    },
    HY: function (a) {
        var c = F.d(0, 0);
        switch (this.ij) {
            case F.hw:
                c = F.d(a.x * this.gc.width, -a.y * this.gc.height);
                break;
            case F.gw:
                c = F.d(this.gc.width / 2 * (a.x - a.y), this.gc.height / 2 * (-a.x - a.y));
                break;
            case F.Hk:
                (0 !== a.x || 0 !== a.y) && F.log("offset for hexagonal map not implemented yet")
        }
        return c
    },
    zY: function (a, c) {
        var d = this.zg.Eu(a), d = F.Xl(d), e = 0 | c.x + c.y * this.Ea.width, d = this.CE(d);
        this.ay(d, c, a);
        var f = this.ne.length;
        this.pz(d, f);
        this.ne.splice(f,
            0, e);
        return d
    },
    x_: function (a, c) {
        var d = this.zg.Eu(a), d = F.Xl(d), e = 0 | c.x + c.y * this.Ea.width, d = this.CE(d);
        this.ay(d, c, a);
        var f = this.BY(e);
        this.pz(d, f);
        this.ne.splice(f, 0, e);
        if (this.u)for (var g = this.u, h = 0, m = g.length; h < m; h++) {
            var n = g[h];
            if (n) {
                var s = n.atlasIndex;
                s >= f && (n.atlasIndex = s + 1)
            }
        }
        this.me[e] = a;
        return d
    },
    M1: function (a, c) {
        var d = this.zg.Eu(a), e = this.Dh, d = F.rect(d.x / e, d.y / e, d.width / e, d.height / e), e = c.x + c.y * this.Ea.width, d = this.CE(d);
        this.ay(d, c, a);
        d.atlasIndex = this.DC(e);
        d.dirty = p;
        d.Ae();
        this.me[e] = a;
        return d
    },
    e0: function () {
        var a = this.GG("cc_vertexz");
        if (a)if ("automatic" == a) {
            this.sF = p;
            var c = this.GG("cc_alpha_func"), a = 0;
            c && (a = parseFloat(c));
            F.B === F.Y && (this.shaderProgram = F.le.Kc(F.Yv), c = F.l.getUniformLocation(this.shaderProgram.Pn(), F.$K), this.shaderProgram.xb(), this.shaderProgram.Pu(c, a))
        } else this.yF = parseInt(a, 10)
    },
    ay: function (a, c, d) {
        var e = c.x + c.y * this.Ea.width;
        a.X(this.Vy(c));
        F.B === F.Y ? a.vertexZ = this.aP(c) : a.tag = e;
        a.anchorX = 0;
        a.anchorY = 0;
        a.opacity = this.Je;
        F.B === F.Y && (a.rotation = 0);
        a.gA(r);
        a.RH(r);
        (d &
        F.kK) >>> 0 ? (a.anchorX = 0.5, a.anchorY = 0.5, a.x = this.Vy(c).x + a.width / 2, a.y = this.Vy(c).y + a.height / 2, c = (d & (F.Zr | F.$r) >>> 0) >>> 0, c == F.Zr ? a.rotation = 90 : c == F.$r ? a.rotation = 270 : (a.rotation = c == (F.$r | F.Zr) >>> 0 ? 90 : 270, a.gA(p))) : ((d & F.Zr) >>> 0 && a.gA(p), (d & F.$r) >>> 0 && a.RH(p))
    },
    CE: function (a) {
        F.B === F.Y ? (this.Yg ? (this.Yg.batchNode = q, this.Yg.Db(a, r)) : (this.Yg = new F.I, this.Yg.Fa(this.texture, a, r)), this.Yg.batchNode = this) : (this.Yg = new F.I, this.Yg.Fa(this.Zi, a, r), this.Yg.batchNode = this, this.Yg.parent = this, this.Yg.Uk = this);
        return this.Yg
    },
    aP: function (a) {
        var c = 0, d = 0;
        if (this.sF)switch (this.ij) {
            case F.gw:
                d = this.Ea.width + this.Ea.height;
                c = -(d - (a.x + a.y));
                break;
            case F.hw:
                c = -(this.Ea.height - a.y);
                break;
            case F.Hk:
                F.log("TMX Hexa zOrder not supported");
                break;
            default:
                F.log("TMX invalid value")
        } else c = this.yF;
        return c
    },
    DC: function (a) {
        var c;
        if (this.ne)for (var d = this.ne, e = 0, f = d.length; e < f && !(c = d[e], c == a); e++);
        F.du(c) || F.log("cc.TMXLayer._atlasIndexForExistantZ(): TMX atlas index not found. Shall not happen");
        return e
    },
    BY: function (a) {
        for (var c =
            this.ne, d = 0, e = c.length; d < e && !(a < c[d]); d++);
        return d
    }
});
M = F.Xr.prototype;
F.B == F.Y ? (M.Ka = F.We.prototype.Ka, M.H = F.We.prototype.H, M.Ma = F.We.prototype.Ma) : (M.Ka = M.Zo, M.H = M.sn, M.Ma = M.Ks);
F.k(M, "texture", M.Ma, M.ib);
F.k(M, "layerWidth", M.PZ, M.O0);
F.k(M, "layerHeight", M.OZ, M.N0);
F.k(M, "tileWidth", M.Ms, M.lt);
F.k(M, "tileHeight", M.Ls, M.kt);
F.Xr.create = function (a, c, d) {
    return new F.Xr(a, c, d)
};
F.yB = F.za.extend({
    $m: q, Ia: q, Ro: q, ctor: function (a, c) {
        this.r5(a, c)
    }, mha: A("$m"), ooa: z("$m"), ik: A("Ia"), lA: z("Ia"), Lfa: A("Ro"), a8: z("Ro"), r5: function (a, c) {
        this.$m = a;
        this.Ia = c;
        this.Ro = q;
        return p
    }
});
F.yB.create = function (a, c) {
    return new F.yB(a, c)
};
F.vJ = F.n.extend({
    Vl: q, Us: q, Mb: "ParallaxNode", aha: A("Vl"), foa: z("Vl"), ctor: function () {
        F.n.prototype.ctor.call(this);
        this.Vl = [];
        this.Us = F.d(-100, -100)
    }, F: function (a, c, d, e) {
        if (3 === arguments.length)F.log("ParallaxNode: use addChild(child, z, ratio, offset) instead"); else {
            a || b("cc.ParallaxNode.addChild(): child should be non-null");
            var f = new F.yB(d, e);
            f.a8(a);
            this.Vl.push(f);
            a.X(this.xa.x * d.x + e.x, this.xa.y * d.y + e.y);
            F.n.prototype.F.call(this, a, c, a.tag)
        }
    }, removeChild: function (a, c) {
        for (var d = this.Vl, e = 0; e <
        d.length; e++)if (d[e].Ro == a) {
            d.splice(e, 1);
            break
        }
        F.n.prototype.removeChild.call(this, a, c)
    }, Mf: function (a) {
        this.Vl.length = 0;
        F.n.prototype.Mf.call(this, a)
    }, H: function () {
        var a = this.mL();
        if (!F.Tz(a, this.Us)) {
            for (var c = this.Vl, d = 0, e = c.length; d < e; d++) {
                var f = c[d];
                f.Ro.X(-a.x + a.x * f.$m.x + f.ik().x, -a.y + a.y * f.$m.y + f.ik().y)
            }
            this.Us = a
        }
        F.n.prototype.H.call(this)
    }, mL: function () {
        for (var a = this.xa, c = this; c.parent != q;)c = c.parent, a = F.sk(a, c.Uy());
        return a
    }, ef: function () {
        var a = this.mL();
        if (!F.Tz(a, this.Us)) {
            for (var c =
                this.Vl, d = 0, e = c.length; d < e; d++) {
                var f = c[d];
                f.Ro.X(-a.x + a.x * f.$m.x + f.ik().x, -a.y + a.y * f.$m.y + f.ik().y)
            }
            this.Us = a
        }
        F.n.prototype.ef.call(this)
    }
});
F.vJ.create = function () {
    return new F.vJ
};
if (F.ta.jy) {
    var Aa = F.Rpa = new (window.AudioContext || window.webkitAudioContext || window.mozAudioContext);
    F.nC = F.za.extend({
        cp: q,
        Ga: q,
        hn: q,
        DF: q,
        src: q,
        yH: q,
        autoplay: q,
        controls: q,
        Uka: q,
        currentTime: 0,
        startTime: 0,
        duration: 0,
        JD: q,
        CF: 1,
        Mx: 0,
        Gc: r,
        Wi: p,
        Ug: -1,
        ctor: function (a) {
            this.cp = {};
            this.src = a;
            this.DF = Aa.createGain ? Aa.createGain() : Aa.createGainNode();
            this.V_ = this.U_.bind(this);
            this.S_ = this.R_.bind(this)
        },
        aE: function (a) {
            var c = this.hn = Aa.createBufferSource(), d = this.DF;
            a = a || 0;
            c.buffer = this.Ga;
            d.gain.value = this.CF;
            c.connect(d);
            d.connect(Aa.destination);
            c.loop = this.JD;
            c.Wi = r;
            c.playbackState || (c.onended = function () {
                this.Wi = p
            });
            this.Wi = this.Gc = r;
            c.start ? c.start(0, a) : c.noteGrainOn ? (d = c.buffer.duration, this.loop ? c.noteGrainOn(0, a, d) : c.noteGrainOn(0, a, d - a)) : c.noteOn(0);
            this.Mx = 0
        },
        LO: function () {
            var a = this.hn;
            this.Wi || (a.stop ? a.stop(0) : a.Zka(0), this.Wi = p)
        },
        play: function () {
            if (-1 == this.Ug)this.Ug = 0; else if (1 == this.Ug) {
                var a = this.hn;
                if (this.Wi || !a || 2 != a.playbackState && a.Wi)this.startTime = Aa.currentTime, this.aE(0)
            }
        },
        pause: function () {
            this.Mx =
                Aa.currentTime;
            this.Gc = p;
            this.LO()
        },
        Xq: function () {
            this.Gc && this.aE(this.Ga ? (this.Mx - this.startTime) % this.Ga.duration : 0)
        },
        stop: function () {
            this.Mx = 0;
            this.Gc = r;
            this.LO()
        },
        load: function () {
            var a = this;
            if (1 != a.Ug) {
                a.Ug = -1;
                a.played = r;
                a.ended = p;
                var c = new XMLHttpRequest;
                c.open("GET", a.src, p);
                c.responseType = "arraybuffer";
                c.onload = function () {
                    Aa.decodeAudioData(c.response, a.V_, a.S_)
                };
                c.send()
            }
        },
        addEventListener: function (a, c) {
            this.cp[a] = c.bind(this)
        },
        removeEventListener: function (a) {
            delete this.cp[a]
        },
        Kda: function () {
            return F.ta.jy
        },
        U_: function (a) {
            this.Ga = a;
            a = this.cp.success;
            var c = this.cp.canplaythrough;
            a && a();
            c && c();
            (0 == this.Ug || "autoplay" == this.autoplay || this.autoplay == p) && this.aE();
            this.Ug = 1
        },
        R_: function () {
            var a = this.cp.error;
            a && a();
            this.Ug = -2
        },
        cloneNode: function () {
            var a = new F.nC(this.src);
            a.volume = this.volume;
            a.Ug = this.Ug;
            a.Ga = this.Ga;
            (0 == a.Ug || -1 == a.Ug) && a.load();
            return a
        }
    });
    M = F.nC.prototype;
    F.k(M, "loop", A("JD"), function (a) {
        this.JD = a;
        this.hn && (this.hn.loop = a)
    });
    F.k(M, "volume", A("CF"), function (a) {
        this.CF = a;
        this.DF.gain.value =
            a
    });
    F.k(M, "paused", A("Gc"));
    F.k(M, "ended", function () {
        var a = this.hn;
        return this.Gc ? r : this.Wi && !a ? p : a.playbackState == q ? a.Wi : 3 == a.playbackState
    });
    F.k(M, "played", function () {
        var a = this.hn;
        return a && (2 == a.playbackState || !a.Wi)
    })
}
F.wI = F.za.extend({
    YE: r, kd: q, ws: q, Bf: 0, uL: 0, He: {}, ms: {}, $w: 1, MD: 5, Yk: q, ct: [], ctor: function () {
        this.YE = 0 < F.EC.ky.length;
        this.Yk && (this.Yk = this.Yk.bind(this))
    }, Upa: D(r), dga: A("$w"), FR: function (a, c) {
        if (this.YE) {
            var d = this.kd;
            d && this.wl(d);
            F.ta.nh && F.ta.Tl == F.ta.zo ? (d = this.kx(a), this.kd = d.cloneNode(), this.ws = a) : a != this.ws && (this.kd = d = this.kx(a), this.ws = a);
            this.kd && (this.kd.loop = c || r, this.bE(this.kd))
        }
    }, kx: function (a) {
        var c = F.aa, d = c.ge(a);
        d || (c.load(a), d = c.ge(a));
        return d
    }, bE: function (a) {
        a.ended || (a.stop ?
            a.stop() : (a.pause(), 2 < a.readyState && (a.currentTime = 0)));
        this.Bf = 2;
        a.play()
    }, NS: function (a) {
        if (0 < this.Bf) {
            var c = this.kd;
            c && this.wl(c) && (a && F.aa.oj(this.ws), this.ws = this.kd = q, this.Bf = 0)
        }
    }, wl: function (a) {
        return a && !a.ended ? (a.stop ? a.stop() : (a.pause(), 2 < a.readyState && (a.duration && Infinity != a.duration) && (a.currentTime = a.duration)), p) : r
    }, ER: function () {
        2 == this.Bf && (this.kd.pause(), this.Bf = 1)
    }, L7: function () {
        1 == this.Bf && (this.Tj(this.kd), this.Bf = 2)
    }, Tj: function (a) {
        a && !a.ended && (a.Xq ? a.Xq() : a.play())
    }, Kma: function () {
        this.kd &&
        this.bE(this.kd)
    }, Rga: function () {
        return 0 == this.Bf ? 0 : this.kd.volume
    }, Vna: function (a) {
        0 < this.Bf && (this.kd.volume = Math.min(Math.max(a, 0), 1))
    }, dH: function () {
        return 2 == this.Bf && this.kd && !this.kd.ended
    }, lD: function (a) {
        var c = this.ms[a];
        c || (c = this.ms[a] = []);
        return c
    }, YM: function (a) {
        var c;
        if (!this.YE)return q;
        var d = this.lD(a);
        if (F.ta.nh && F.ta.Tl == F.ta.zo)c = this.ZM(d, a); else {
            for (var e = 0, f = d.length; e < f; e++) {
                var g = d[e];
                if (g.ended) {
                    c = g;
                    2 < c.readyState && (c.currentTime = 0);
                    window.Qda && c.load();
                    break
                }
            }
            c || (c = this.ZM(d,
                a)) && d.push(c)
        }
        return c
    }, ZM: function (a, c) {
        var d;
        if (a.length >= this.MD)return F.log("Error: " + c + " greater than " + this.MD), q;
        d = this.kx(c);
        if (!d)return q;
        d = d.cloneNode(p);
        this.Yk && F.Sa(d, "pause", this.Yk);
        d.volume = this.$w;
        return d
    }, Au: function (a, c) {
        var d = this.YM(a);
        if (!d)return q;
        d.loop = c || r;
        d.play();
        var e = this.uL++;
        this.He[e] = d;
        return e
    }, xna: function (a) {
        a = this.$w = Math.min(Math.max(a, 0), 1);
        var c = this.He, d;
        for (d in c)c[d].volume = a
    }, a7: function (a) {
        (a = this.He[a]) && !a.ended && a.pause()
    }, Y6: function () {
        var a =
            this.He, c;
        for (c in a) {
            var d = a[c];
            d.ended || d.pause()
        }
    }, K7: function (a) {
        this.Tj(this.He[a])
    }, J7: function () {
        var a = this.He, c;
        for (c in a)this.Tj(a[c])
    }, fI: function (a) {
        this.wl(this.He[a]);
        delete this.He[a]
    }, LS: function () {
        var a = this.He, c;
        for (c in a)this.wl(a[c]), delete a[c]
    }, Q9: function (a) {
        var c = F.aa, d = this.He, e = this.lD(a);
        c.oj(a);
        if (0 != e.length) {
            c = e[0].src;
            delete this.ms[a];
            for (var f in d)d[f].src == c && (this.wl(d[f]), delete d[f])
        }
    }, end: function () {
        this.NS();
        this.LS()
    }, QN: function () {
        var a = this.He, c, d;
        for (d in a)if ((c =
                a[d]) && !c.ended && !c.paused)this.ct.push(c), c.pause();
        this.dH() && (this.ct.push(this.kd), this.kd.pause())
    }, iO: function () {
        for (var a = this.ct, c = 0, d = a.length; c < d; c++)this.Tj(a[c]);
        a.length = 0
    }
});
!F.ta.jy && !F.ta.u1 && (F.xI = F.wI.extend({
    Cl: [], bt: [], Xf: q, MD: 2, Zw: {}, Sm: r, dp: 0, CD: r, bE: function (a) {
        this.dF();
        this._super(a)
    }, L7: function () {
        1 == this.Bf && (this.dF(), this.Sm = r, this.dp = 0, this._super())
    }, Au: function (a, c) {
        var d = this.Xf, e = c ? this.YM(a) : this.c_(a);
        if (!e)return q;
        e.loop = c || r;
        var f = this.uL++;
        this.He[f] = e;
        this.dH() && (this.ER(), this.Sm = p);
        d ? (d != e && this.Cl.push(this.Wo), this.Cl.push(f), d.pause()) : (this.Xf = e, this.Wo = f, e.play());
        return f
    }, a7: function () {
        F.log("pauseEffect not supported in single audio mode!")
    },
    Y6: function () {
        var a = this.Cl, c = this.bt, d = this.Xf;
        if (d) {
            for (var e = 0, f = a.length; e < f; e++)c.push(a[e]);
            a.length = 0;
            c.push(this.Wo);
            d.pause()
        }
    }, K7: function () {
        F.log("resumeEffect not supported in single audio mode!")
    }, J7: function () {
        var a = this.Cl, c = this.bt;
        this.dH() && (this.ER(), this.Sm = p);
        for (var d = 0, e = c.length; d < e; d++)a.push(c[d]);
        c.length = 0;
        if (!this.Xf && 0 <= a.length && (a = a.pop(), c = this.He[a]))this.Wo = a, this.Xf = c, this.Tj(c)
    }, fI: function (a) {
        var c = this.Xf, d = this.Cl, e = this.bt;
        c && this.Wo == a ? this.wl(c) : (c = d.indexOf(a),
            0 <= c ? d.splice(c, 1) : (c = e.indexOf(a), 0 <= c && e.splice(c, 1)))
    }, LS: function () {
        this.dF();
        !this.Xf && this.Sm && (this.Tj(this.kd), this.Bf = 2, this.Sm = r, this.dp = 0)
    }, Q9: function (a) {
        var c = F.aa, d = this.He, e = this.Zw, f = this.lD(a), g = this.Xf;
        c.oj(a);
        if (0 != f.length || e[a]) {
            c = 0 < f.length ? f[0].src : e[a].src;
            delete this.ms[a];
            delete e[a];
            for (var h in d)d[h].src == c && delete d[h];
            g && g.src == c && this.wl(g)
        }
    }, c_: function (a) {
        var c = this.Zw[a], d = this.Cl, e = this.bt, f = this.He;
        if (c)2 < c.readyState && (c.currentTime = 0); else {
            c = this.kx(a);
            if (!c)return q;
            c = c.cloneNode(p);
            this.Yk && F.Sa(c, "pause", this.Yk);
            c.volume = this.$w;
            this.Zw[a] = c
        }
        a = 0;
        for (var g = d.length; a < g;)f[d[a]] == c ? d.splice(a, 1) : a++;
        a = 0;
        for (g = e.length; a < g;)f[e[a]] == c ? e.splice(a, 1) : a++;
        c.Ss = p;
        return c
    }, dF: function () {
        var a = this.Xf, c = this.ms, d = this.Zw, e = this.Cl, f = this.bt;
        if (a || !(0 == e.length && 0 == f.length)) {
            for (var g in d) {
                var h = d[g];
                2 < h.readyState && (h.duration && Infinity != h.duration) && (h.currentTime = h.duration)
            }
            e.length = 0;
            f.length = 0;
            for (g in c) {
                d = c[g];
                e = 0;
                for (f = d.length; e < f; e++)h = d[e], h.loop = r, 2 <
                h.readyState && (h.duration && Infinity != h.duration) && (h.currentTime = h.duration)
            }
            a && this.wl(a)
        }
    }, Yk: function () {
        if (!this.CD) {
            var a = this.f_();
            if (a)a.Ss ? (delete a.Ss, a.play()) : this.Tj(a); else if (this.Sm) {
                a = this.kd;
                if (2 < a.readyState && a.duration && Infinity != a.duration) {
                    var c = a.currentTime + this.dp, c = c - a.duration * (c / a.duration | 0);
                    a.currentTime = c
                }
                this.dp = 0;
                this.Tj(a);
                this.Bf = 2;
                this.Sm = r
            }
        }
    }, f_: function () {
        var a = this.Cl, c = this.He, d = this.Xf, e = d ? d.currentTime - (d.startTime || 0) : 0;
        for (this.dp += e; 0 != a.length;) {
            var f = a.pop();
            if (d = c[f]) {
                if (d.Ss || d.loop || d.duration && d.currentTime + e < d.duration)return this.Wo = f, this.Xf = d, !d.Ss && (2 < d.readyState && d.duration && Infinity != d.duration) && (a = d.currentTime + e, a -= d.duration * (a / d.duration | 0), d.currentTime = a), d.Ss = r, d;
                2 < d.readyState && (d.duration && Infinity != d.duration) && (d.currentTime = d.duration)
            }
        }
        return this.Xf = this.Wo = q
    }, QN: function () {
        var a = this.Xf;
        this.CD = p;
        if (a = 2 == this.Bf ? this.kd : a)this.ct.push(a), a.pause()
    }, iO: function () {
        var a = this.ct;
        this.CD = r;
        0 < a.length && (this.Tj(a[0]), a.length = 0)
    }
}));
F.EC = {
    ky: q, Ry: function () {
        return F.aa.u2
    }, HD: function (a, c, d, e, f, g, h) {
        var m = this, n = F.aa, s = F.path, t = this.ky, v = "";
        if (0 == t.length)return h("can not support audio!");
        if (-1 == e)v = (s.Hl(a) || "").toLowerCase(), m.v2(v) || (v = t[0], e = 0); else if (e < t.length)v = t[e]; else return h("can not found the resource of audio! Last match url is : " + a);
        if (0 <= f.indexOf(v))return m.HD(a, c, d, e + 1, f, g, h);
        a = s.pP(a, v);
        f.push(v);
        g = m.G_(a, g, function (n) {
            if (n)return m.HD(a, c, d, e + 1, f, g, h);
            h(q, g)
        }, e == t.length - 1);
        n.jg[c] = g
    }, v2: function (a) {
        return !a ?
            r : 0 <= this.ky.indexOf(a.toLowerCase())
    }, G_: function (a, c, d, e) {
        var f;
        f = !F.Gq(window.cc) && "firefox" == F.ta.$h ? Audio : "file://" == location.origin ? Audio : F.nC || Audio;
        2 == arguments.length ? (d = c, c = new f) : 3 < arguments.length && !c && (c = new f);
        c.src = a;
        c.yH = "auto";
        f = navigator.userAgent;
        if (/Mobile/.test(f) && (/iPhone OS/.test(f) || /iPad/.test(f) || /Firefox/.test(f)) || /MSIE/.test(f))c.load(), d(q, c); else {
            F.Sa(c, "canplaythrough", function () {
                d(q, c);
                this.removeEventListener("canplaythrough", arguments.callee, r);
                this.removeEventListener("error",
                    arguments.callee, r)
            }, r);
            var g = function () {
                c.removeEventListener("emptied", g);
                c.removeEventListener("error", g);
                d("load " + a + " failed");
                e && (this.removeEventListener("canplaythrough", arguments.callee, r), this.removeEventListener("error", arguments.callee, r))
            };
            F.ta.$h === F.ta.IA && F.Sa(c, "emptied", g, r);
            F.Sa(c, "error", g, r);
            c.load()
        }
        return c
    }, load: function (a, c, d, e) {
        this.HD(a, c, d, -1, [], q, e)
    }
};
F.EC.ky = function () {
    var a = F.bc("audio"), c = [];
    if (a.canPlayType) {
        var d = function (c) {
            c = a.canPlayType(c);
            return "no" != c && "" != c
        };
        d('audio/ogg; codecs\x3d"vorbis"') && c.push(".ogg");
        d("audio/mpeg") && c.push(".mp3");
        d('audio/wav; codecs\x3d"1"') && c.push(".wav");
        d("audio/mp4") && c.push(".mp4");
        (d("audio/x-m4a") || d("audio/aac")) && c.push(".m4a")
    }
    return c
}();
F.aa.nj(["mp3", "ogg", "wav", "mp4", "m4a"], F.EC);
F.Yh = F.xI ? new F.xI : new F.wI;
F.La.HF(F.Qb.WA, function () {
    F.Yh.QN()
});
F.La.HF(F.Qb.nv, function () {
    F.Yh.iO()
});
var W = {
    dK: "res/startback.jpg",
    eK: "res/startbutton.png",
    iX: "res/startdesc.png",
    YV: "res/roadback.jpg",
    Paa: "res/light.png",
    oK: "res/tapbutton.png",
    oX: "res/tapicon_left.png",
    pX: "res/tapicon_right.png",
    tT: "res/airplane.png",
    rT: "res/airplane_shade.png",
    qT: "res/airplane_cloud.png",
    iV: "res/line_left.png",
    jV: "res/line_right.png",
    ST: "res/car.png",
    $V: "res/rotate.png",
    jJ: "res/moveback.jpg",
    iJ: "res/backline.png",
    XV: "res/ready.png",
    EU: "res/go.png",
    qW: "res/scene/1.jpg",
    rW: "res/scene/2.jpg",
    sW: "res/scene/3.jpg",
    tW: "res/scene/4.jpg",
    uW: "res/scene/5.jpg",
    DU: "res/gameoverback.jpg",
    ZJ: "res/share.png",
    tI: "res/again.png",
    CU: "res/gamelogo.png",
    vT: "res/arrow.png",
    ZV: "res/roadend.jpg",
    uK: "res/tourist.png",
    dV: "res/lamandoword.png",
    RX: "res/time.mp3",
    sK: "res/theremin.mp3",
    QT: "res/carfaster.mp3",
    RT: "res/carslower.mp3",
    sT: "res/airplan.mp3",
    XI: "res/gain.png",
    CT: "res/bestscoreback.png"
}, Ba = [], za;
for (za in W)Ba.push(W[za]);
var Ca = q, Da = q, Ha = q, X = 0, Ia = 0, Ja = 0, Na = F.ec.extend({
        fa: F.d(0, 0), Wn: q, Tma: q, Dl: q, Rt: q, tq: q, ctor: function () {
            this._super();
            F.Yh.FR(W.sK, p);
            this.fa = F.view.kh();
            X = parseInt(X);
            6E3 > X ? descContent = "\u6211\u5728\u51cc\u6e21\u98de\u884c\u4e2d\u98de\u4e86" + X + "\u7c73\uff0c\u4e0d\u670d\u5c31\u731b\u6233\u5c4f\u5e55\u6311\u6218" : 6E3 <= X && 8E3 > X ? descContent = "\u96be\u4ee5\u7f6e\u4fe1\uff01\u6211\u7adf\u7136\u5728\u51cc\u6e21\u98de\u884c\u4e2d\u98de\u4e86" + X + "\u7c73\u7684\u597d\u6210\u7ee9\u3002" : 8E3 <= X && 9500 > X ? descContent =
                "\u6211\u5728\u51cc\u6e21\u98de\u884c\u4e2d\u98de\u4e86" + X + "\u7c73\uff0c\u88c2\u5c4f\u624b\u901f\u4f60\u6562\u6311\u6218\u5417\uff1f" : 9500 <= X && (descContent = X + "\u7c73\u7684\u9006\u5929\u91cc\u7a0b\uff0c\u6211\u5c31\u662f\u51cc\u6e21\u98de\u884c\u7684\u738b\u724c\u98de\u884c\u5458\uff01");
            timelineTitle = descContent;
            imgUrl = overIconUrl;
            wx_initDataWithGameover();
            var a = new F.I(W.DU);
            a.ja({
                x: this.fa.width / 2,
                y: this.fa.height / 2,
                anchorX: 0.5,
                anchorY: 0.5,
                scaleX: this.fa.width / a.$().width,
                scaleY: this.fa.height /
                a.$().height
            });
            this.F(a, 0);
            a = F.Gd.create(F.color(0, 0, 0, 200), this.fa.width, this.fa.height);
            this.F(a);
            var c = new F.wj(W.tI, W.tI, function () {
                timelineTitle = descContent = "\u5b9e\u9a8c\u5ba4\u98de\u884c\u6311\u6218\u5168\u9762\u5f00\u542f\uff0c\u63ed\u5f00\u51cc\u6e21\u5f3a\u52b2\u626d\u529b\u4e4b\u8c1c\uff0c\u8fd8\u6709\u597d\u793c\u7b49\u4f60\u8d62\uff01";
                imgUrl = originIconUrl;
                wx_initDataWithGameover();
                Ha = q;
                F.L.tc.Mf(p);
                Da = new Ka;
                Da.start();
                F.L.tc.F(Da, 1)
            }, this);
            c.ja({
                x: this.fa.width / 2 - 30, y: 300, anchorX: 1,
                anchorY: 0
            });
            a = new F.wj(W.ZJ, W.ZJ, function () {
                this.c9()
            }, this);
            a.ja({x: this.fa.width / 2 + 30, y: 300, anchorX: 0, anchorY: 0});
            this.Rt = new F.vi(c, a);
            this.Rt.x = 0;
            this.Rt.y = 0;
            this.F(this.Rt, 1);
            c = new F.wj(W.XI, W.XI, function () {
                var a = "", a = "" == urlarg || urlarg == q ? redirUrl + "?s\x3d" + X : redirUrl + urlarg + "\x26s\x3d" + X;
                window.location.href = a
            }, this);
            c.ja({x: this.fa.width / 2, y: 300, anchorX: 0.5, anchorY: 0});
            this.tq = new F.vi(c);
            this.tq.x = 0;
            this.tq.y = 0;
            this.tq.ke(r);
            this.F(this.tq, 1);
            c = new F.I(W.CT);
            c.ja({
                x: this.fa.width / 2, y: 200,
                anchorX: 0.5, anchorY: 0
            });
            this.F(c, 1);
            c = new F.R("\u5feb\u53bb\u70ab\u8000\u4f60\u7684\u65e0\u654c\u6218\u7ee9\u5427\uff01", "Arial", 23);
            c.ja({x: this.fa.width / 2, y: a.va() + a.$().height + 50, anchorX: 0.5, anchorY: 0});
            this.F(c, 1);
            a = new F.I(W.dV);
            a.ja({x: this.fa.width / 2 - 5, y: c.va() + c.$().height + 5, anchorX: 0.5, anchorY: 0});
            this.F(a, 1);
            c = new F.R("\u5373\u53ef\u83b7\u5f97 ", "Arial", 23);
            c.ja({x: a.$b() - a.$().width / 2, y: a.va() + a.$().height / 2, anchorX: 1, anchorY: 0.5});
            this.F(c, 1);
            c = new F.R(" \u7ec4\u88c5\u90e8\u4ef6\uff0c",
                "Arial", 23);
            c.ja({x: a.$b() + a.$().width / 2, y: a.va() + a.$().height / 2, anchorX: 0, anchorY: 0.5});
            this.F(c, 1);
            c = new F.R(" \u5206\u4eab\u6e38\u620f\u7ed3\u679c\u5230\u5fae\u4fe1\u670b\u53cb\u5708", "Arial", 23);
            c.ja({x: this.fa.width / 2, y: a.va() + a.$().height + 5, anchorX: 0.5, anchorY: 0});
            this.F(c, 1);
            a = new F.R("\u7684\u73a9\u5bb6\uff0c\u5730\u7403\u4eba\u5df2\u7ecf\u65e0\u6cd5\u963b\u6b62\u4f60\u4e86\u3002", "Arial", 23);
            a.ja({x: this.fa.width / 2 + 70, y: c.va() + c.$().height + 5, anchorX: 0.5, anchorY: 0});
            this.F(a, 1);
            c = 100 * X / 1E4;
            c = parseInt(c) + "%";
            this.Wn = new F.R(c, "Arial", 23);
            this.Wn.rb(F.color(183, 122, 51));
            this.Wn.ja({x: a.$b() - a.$().width / 2, y: a.va(), anchorX: 1, anchorY: 0});
            this.F(this.Wn, 1);
            a = new F.R("\u60a8\u8d85\u8fc7\u4e86", "Arial", 23);
            a.ja({x: this.Wn.$b() - this.Wn.$().width, y: this.Wn.va(), anchorX: 1, anchorY: 0});
            this.F(a, 1);
            c = new F.R("\u672c\u8f6e\u6210\u7ee9", "Arial", 28);
            c.ja({x: this.fa.width / 2, y: a.va() + a.$().height + 30, anchorX: 0.5, anchorY: 0});
            this.F(c, 1);
            this.jr = new F.R("1000", "Arial", 80);
            this.jr.ja({
                x: this.fa.width / 2, y: c.va() +
                c.$().height + 5, anchorX: 0.5, anchorY: 0
            });
            this.F(this.jr, 1);
            this.jr.yc(parseInt(X));
            a = new F.R("\u7c73", "Arial", 32);
            a.ja({x: this.jr.$b() + this.jr.$().width / 2, y: this.jr.va() + 20, anchorX: 0, anchorY: 0});
            this.F(a, 1);
            a = new F.I(W.CU);
            a.ja({x: this.fa.width - 20, y: 20, anchorX: 1, anchorY: 0});
            this.F(a, 100);
            isShareed = r;
            this.Yq();
            return p
        }, update: function () {
            isShareed && (isShareed = r, this.o5())
        }, o5: function () {
            this.Dl && this.Dl.ke(r);
            this.e9()
        }, c9: function () {
            this.Dl ? this.Dl.ke(p) : (this.Dl = new F.I(W.vT), this.Dl.ja({
                x: this.fa.width -
                20, y: this.fa.height - 20, anchorX: 1, anchorY: 1
            }), this.F(this.Dl, 1E3), this.Dl.Da(F.EH(F.hb(F.moveBy(0.3, F.d(0, -25)), F.jc(0.1), F.moveBy(0.6, F.d(0, 50)), F.moveBy(0.3, F.d(0, -25))))))
        }, e9: function () {
            this.Rt.ke(r);
            this.tq.ke(p)
        }
    }), Oa = F.ec.extend({
        fa: F.size(0, 0), wc: [], dq: [], Su: 4, cr: 0, Sc: 0, fu: r, aA: q, Ku: q, ctor: function () {
            this._super();
            this.Su = 2;
            this.Sc = this.cr = 0;
            this.wc.splice(0, this.wc.length);
            this.dq.splice(0, this.dq.length);
            this.fa = F.view.kh();
            this.Ku = new F.I(W.YV);
            this.Ku.ja({
                x: this.fa.width / 2, y: 0, anchorX: 0.5,
                anchorY: 0
            });
            this.F(this.Ku, 100);
            for (var a = F.d(this.fa.width / 2, 0), c = q, d = 0; 2 > d; d++)c = new F.I(W.jJ), c.ja({
                x: a.x,
                y: a.y,
                anchorX: 0.5,
                anchorY: 0
            }), this.F(c), this.wc.push(c), backline = new F.I(W.iJ), backline.ja({
                x: a.x,
                y: a.y,
                anchorX: 0.5,
                anchorY: 0
            }), this.F(backline, 1E3), this.dq.push(backline), a.y += c.$().height;
            return p
        }, start: function () {
            this.Yq();
            this.eA(this.Ay, 0.16);
            var a = F.hb(F.Kn(1), F.Eb(function () {
                this.Ku.ni(p)
            }, this));
            this.Ku.Da(a)
        }, stop: function () {
            this.lr();
            this.Zu(this.Ay)
        }, Ay: function () {
            this.Sc += this.Su;
            60 <= this.Sc ? (260 < this.Sc && (this.Sc = 260), this.Zu(this.Ay)) : (this.cr += this.Su, 60 <= this.cr && this.Zu(this.Ay))
        }, Y1: function () {
            this.Sc += 6;
            260 < this.Sc && (this.Sc = 260)
        }, Kl: A("Sc"), Xd: z("Sc"), C8: function (a) {
            if ((this.fu = a) && this.aA == q)a = this.wc.length, 0 < a && (a = this.wc[a - 1], this.aA = new F.I(W.ZV), this.aA.ja({
                x: a.$().width / 2,
                y: a.$().height,
                anchorX: 0.5,
                anchorY: 0
            }), a.F(this.aA))
        }, nha: A("fu"), Z4: function () {
            var a = 0, c = this.wc.length;
            0 < c && (a = this.wc[c - 1], a = a.va() + a.$().height);
            return a
        }, update: function () {
            for (var a = this.wc.length,
                     c = 0.25 * this.Sc, d = a - 1; 0 <= d; d--)a = this.wc[d], a.am(a.va() - c), 0 > a.va() + a.$().height && (a.ni(p), this.wc.splice(d, 1)), a = this.dq[d], a.am(a.va() - c), 0 > a.va() + a.$().height && (a.ni(p), this.dq.splice(d, 1));
            this.fu || (a = this.wc.length, a = this.wc[a - 1], c = a.va() + a.$().height - 10, c < this.fa.height && (a = new F.I(W.jJ), a.ja({
                x: this.fa.width / 2,
                y: c,
                anchorX: 0.5,
                anchorY: 0
            }), this.F(a), this.wc.push(a), backline = new F.I(W.iJ), backline.ja({
                x: this.fa.width / 2,
                y: c,
                anchorX: 0.5,
                anchorY: 0
            }), this.F(backline, 1E3), this.dq.push(backline)))
        }
    }),
    Pa = F.ec.extend({
        fa: F.size(0, 0),
        wc: [],
        Su: 2,
        cr: 0,
        Sc: 0,
        S7: 5,
        R7: [W.qW, W.rW, W.sW, W.tW, W.uW],
        status: r,
        eH: r,
        Wl: 0,
        yu: q,
        aq: 0,
        gea: 0,
        vR: 0,
        hpa: 0,
        fH: r,
        ctor: function () {
            this._super();
            this.Su = 2;
            this.Sc = this.cr = 0;
            this.wc.splice(0, this.wc.length);
            this.fa = F.view.kh();
            this.yu = new F.R("", "Arial", 80);
            this.yu.ja({x: this.fa.width / 2, y: this.fa.height - 100, anchorX: 0.5, anchorY: 1});
            this.F(this.yu, 500);
            for (var a = F.d(this.fa.width / 2, this.fa.height), c = 0; c < this.S7; c++) {
                var d = new F.I(this.R7[c]);
                d.ja({x: a.x, y: a.y, anchorX: 0.5, anchorY: 0});
                d.ke(r);
                this.F(d);
                this.wc.push(d);
                a.y += d.$().height
            }
            return p
        },
        x8: function (a) {
            this.Wl = this.fa.height - a;
            this.yu.yc(this.Wl + "m");
            for (var c = this.wc.length, d = 0; d < c; d++) {
                var e = this.wc[d];
                e.ke(p);
                e.am(a);
                a += e.$().height
            }
            this.status = p;
            this.Yq()
        },
        Eha: A("status"),
        Fha: A("fH"),
        Xd: function (a) {
            this.Sc = this.vR = a;
            F.log("_speed:" + a);
            this.aq = 1E4 * (this.Sc / 260);
            F.log("this.airplancouldrundistance:" + this.aq)
        },
        update: function () {
            var a = 0.25 * this.Sc;
            this.Wl += a;
            1E4 < this.Wl && (this.Wl = 1E4);
            this.yu.yc(parseInt(this.Wl) + "m");
            for (var c = this.wc.length, c = c - 1; 0 <= c; c--) {
                var d = this.wc[c];
                d.am(d.va() - a);
                this.eH == r && 0 >= d.va() && (this.eH = p, this.aq -= this.fa.height - d.va(), F.log(this.aq));
                0 > d.va() + d.$().height && (d.ni(p), this.wc.splice(c, 1))
            }
            c = this.wc.length;
            if (0 < c && (d = this.wc[c - 1], d.va() + d.$().height < this.fa.height))for (var e = this.fa.height - d.$().height, c = c - 1; 0 <= c; c--)d = this.wc[c], d.am(e), e -= d.$().height;
            this.eH && (this.Sc -= this.vR * (a / this.aq), F.log("this.currentspeedy:" + this.Sc), 5 > this.Sc && (this.Sc = 0, this.lr(), Da = q, this.fH = p, this.Da(F.hb(F.jc(1),
                F.Eb(function () {
                    X = this.Wl;
                    F.L.tc.Mf(p);
                    Ha = new Na;
                    F.L.tc.F(Ha, 100)
                }, this)))))
        }
    }), Ka = F.ec.extend({
        fa: F.d(0, 0),
        Fd: q,
        Mma: q,
        Nma: 0,
        Oma: 5,
        ju: q,
        Vn: q,
        Uia: r,
        ai: q,
        fh: q,
        JF: q,
        $p: q,
        dm: q,
        vA: q,
        wA: q,
        hT: ["5", "4", "3", "2", "1"],
        Vpa: 0,
        kg: q,
        hea: q,
        mq: q,
        ctor: function () {
            this._super();
            this.fa = F.view.kh();
            this.E5();
            this.kg = new Oa;
            this.F(this.kg, 1);
            this.mq = new Pa;
            this.F(this.mq);
            return p
        },
        E5: function () {
            var a = new F.wj(W.oK, W.oK, function () {
                this.kg.Y1();
                var a = this.kg.Kl();
                this.Yl.yc(a)
            }, this);
            a.ja({
                x: this.fa.width / 2, y: 27, anchorX: 0.5,
                anchorY: 0
            });
            this.dm = new F.vi(a);
            this.dm.Wd(r);
            this.dm.x = 0;
            this.dm.y = 0;
            this.F(this.dm, 500);
            this.vA = new F.I(W.oX);
            this.vA.ja({x: a.$b() - a.$().width / 2, y: a.va() + a.$().height / 2, anchorX: 0.4, anchorY: 0.5});
            this.F(this.vA, 500);
            this.wA = new F.I(W.pX);
            this.wA.ja({x: a.$b() + a.$().width / 2, y: a.va() + a.$().height / 2, anchorX: 0.6, anchorY: 0.5});
            this.F(this.wA, 500);
            this.fh = new F.I(W.tT);
            this.fh.ja({
                x: this.fa.width / 2 + 2,
                y: a.va() + a.$().height + 50,
                anchorX: 0.5,
                anchorY: 0,
                scale: 494 / 547
            });
            this.F(this.fh, 1E3);
            this.JF = new F.I(W.rT);
            this.JF.ja({x: this.fh.$b(), y: this.fh.va() - 100, anchorX: 0.5, anchorY: 0});
            this.F(this.JF, 1E3);
            this.$p = new F.I(W.qT);
            this.$p.ja({x: this.fh.$().width / 2, y: 143, anchorX: 0.5, anchorY: 1, scaleX: 0.997});
            this.$p.Da(F.Kn(0.01));
            this.fh.F(this.$p, -1);
            this.ju = new F.I(W.iV);
            this.ju.ja({
                x: this.fa.width / 2 + 10,
                y: -this.ju.$().height / 2 + this.fh.va() + this.fh.$().height - 10,
                anchorX: 1,
                anchorY: 0
            });
            this.F(this.ju, 800);
            this.Vn = new F.I(W.jV);
            this.Vn.ja({
                x: this.fa.width / 2 - 0, y: -this.Vn.$().height / 2 + this.fh.va() + this.fh.$().height - 10,
                anchorX: 0, anchorY: 0
            });
            this.F(this.Vn, 800);
            this.ai = new F.I(W.ST);
            this.ai.ja({
                x: this.fa.width / 2,
                y: this.ai.$().height / 2 + this.Vn.va() + this.Vn.$().height - 12,
                anchorX: 0.5,
                anchorY: 0.5
            });
            this.F(this.ai, 500);
            this.Fd = new F.I(W.$V);
            this.Fd.ja({x: this.fa.width - 80, y: this.fa.height - 30, anchorX: 1, anchorY: 1});
            this.F(this.Fd, 500);
            this.Yl = new F.R("0", "Arial", 42);
            this.Yl.ja({x: this.Fd.$().width / 2, y: this.Fd.$().height / 2, anchorX: 0.5, anchorY: 0.3});
            this.Fd.F(this.Yl, 5);
            a = new F.R("km/h", "Arial", 22);
            a.ja({
                x: this.Yl.$b(), y: this.Yl.va() -
                0.3 * this.Yl.$().height, anchorX: 0.5, anchorY: 1
            });
            this.Fd.F(a, 5)
        },
        start: function () {
            var a = new F.wj(W.uK, W.uK, function () {
                F.Yh.NS();
                a.ni(p);
                this.r9()
            }, this);
            a.ja({x: this.fa.width / 2, y: 0, anchorX: 0.5, anchorY: 0});
            var c = new F.vi(a);
            c.x = 0;
            c.y = 0;
            this.F(c, 99999)
        },
        r9: function () {
            var a = new F.I(W.XV);
            a.ja({
                x: this.fa.width - this.Fd.$b() + this.Fd.$().width / 2,
                y: this.Fd.va() - this.Fd.$().height / 2,
                anchorX: 0.5,
                anchorY: 0.5,
                scaleX: 2,
                scaleY: 2
            });
            this.F(a, 5E3);
            a.ke(r);
            a.Da(F.hb(F.show(), F.Ud(0.2, 1, 1), F.jc(1), F.hi(), F.Eb(function () {
                    a.ni(p)
                },
                this)));
            var c = new F.I(W.EU);
            c.ja({
                x: this.fa.width - this.Fd.$b() + this.Fd.$().width / 2,
                y: this.Fd.va() - this.Fd.$().height / 2,
                anchorX: 0.5,
                anchorY: 0.5,
                scaleX: 2,
                scaleY: 2
            });
            this.F(c, 5E3);
            c.ke(r);
            c.Da(F.hb(F.jc(1.2), F.show(), F.Ud(0.2, 1, 1), F.jc(1), F.hi(), F.Eb(function () {
                this.dm.Wd(p);
                c.ni(p);
                this.Q7()
            }, this)));
            this.Yq()
        },
        Q7: function () {
            this.kg.start();
            F.Yh.Au(W.QT, r);
            F.Yh.Au(W.RX, r);
            for (var a = this.hT.length, c = 0; c < a; c++) {
                var d = new F.R(this.hT[c], "Arial", 56);
                d.ja({
                    x: this.fa.width - this.Fd.$b() + this.Fd.$().width /
                    2, y: this.Fd.va() - this.Fd.$().height / 2, anchorX: 0.5, anchorY: 0.5, scaleX: 3, scaleY: 3
                });
                d.ke(r);
                this.F(d, 500);
                var e = F.hi();
                c == a - 1 && (e = F.Eb(function () {
                    if (this.kg) {
                        var a = this.kg.Kl();
                        this.kg.Xd(a);
                        this.Yl.yc(a)
                    }
                    this.dm.Wd(r);
                    this.dm.ke(r);
                    this.vA.ke(r);
                    this.wA.ke(r);
                    a = F.hb(F.jc(1.2), F.Eb(function () {
                        Ja = F.Yh.Au(W.RT, r)
                    }), F.jc(0.3), F.Eb(function () {
                        this.ai.Da(F.XR(0.5, -110));
                        Ia = F.Yh.Au(W.sT, p);
                        this.fh.Da(F.pi(F.moveBy(2, F.d(0, 200)), F.Ud(2, 1)));
                        this.$p.Da(F.rG(2));
                        this.kg.C8(p);
                        this.ju.Da(F.pi(F.Kn(0.3), F.Ud(0.3,
                            1, 0)));
                        this.Vn.Da(F.pi(F.Kn(0.3), F.Ud(0.3, 1, 0)))
                    }, this));
                    this.Da(a)
                }, this));
                e = F.hb(F.jc(1 * c), F.Ud(0.01, 1, 1), F.show(), F.jc(1), F.hi(), F.Eb(u(), this), e);
                d.Da(e)
            }
        },
        update: function () {
            if (this.kg) {
                var a = this.kg.Kl();
                this.Yl.yc(a);
                if (this.kg.fu) {
                    var c = this.kg.Z4(), d = this.ai.va() + this.ai.$().height / 2 + 0;
                    c < this.fa.height && this.mq.status == r && (this.mq.x8(c), this.mq.Xd(a), this.Fd.ke(r));
                    c < d && (this.ai.am(c - (this.ai.$().height / 2 + 50)), 0 > this.ai.va() + this.ai.$().height / 2 && (F.Yh.fI(Ja), Ja = 0))
                }
            }
            this.mq.fH && (F.Yh.fI(Ia),
                Ia = 0, this.$p.Da(F.Kn(0.5)), this.lr())
        }
    }), Qa = F.ec.extend({
        fa: F.d(0, 0), eG: q, ctor: function () {
            this._super();
            this.fa = F.view.kh();
            var a = new F.wj(W.dK, W.dK, u(), this);
            a.ja({x: this.fa.width / 2, y: 0, anchorX: 0.5, anchorY: 0});
            var c = new F.wj(W.eK, W.eK, function () {
                Ca = q;
                this.ni(p)
            }, this);
            c.ja({x: this.fa.width / 2, y: 580, anchorX: 0.5, anchorY: 0.5});
            a = new F.vi(a, c);
            a.x = 0;
            a.y = 0;
            this.F(a, 1);
            this.eG = new F.I(W.iX);
            this.eG.ja({x: this.fa.width / 2, y: c.va() + c.$().height / 2 + 40, anchorX: 0.5, anchorY: 0});
            this.F(this.eG, 100);
            return p
        }
    }),
    Ra = F.ec.extend({
        fa: F.d(0, 0), ctor: function () {
            this._super();
            this.fa = F.view.kh();
            Da = new Ka;
            this.F(Da);
            Ca = new Qa;
            this.F(Ca);
            this.Yq();
            return p
        }, ba: function () {
            this._super();
            F.Yh.FR(W.sK, p)
        }, update: function () {
            Ca == q && (this.lr(), Da.start())
        }
    }), Sa = F.rm.extend({
        ba: function () {
            this._super();
            wx_Init();
            var a = new Ra;
            this.F(a)
        }
    });
F.Qb.uR = function () {
    F.view.Z1(p);
    F.view.OH(640, 1008, F.sf.Zv);
    F.view.H7(p);
    F.ID = q;
    F.kB.yH(Ba, function () {
        F.L.GH(new Sa)
    }, this)
};
F.Qb.P7();