var game = new Phaser.Game(640, 1085, Phaser.CANVAS);
game.States = {};
var score;
game.States.boot = function () {
    this.i = 1;
    this.preload = function () {
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        if (!game.device.desktop) {
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.forceLandscape = true;
        }
        this.scale.windowConstraints.bottom = "visual";
        this.scale.forceLandscape = true;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.refresh();

        game.load.image("sense_1", "res/sense/1.jpg");
        game.load.image("sense_2", "res/sense/2.jpg");
        game.load.image("sense_3", "res/sense/3.jpg");
        game.load.image("sense_4", "res/sense/4.jpg");
        game.load.image("sense_5", "res/sense/5.jpg");
        game.load.image("again", "res/again.png");
        game.load.image("airplane", "res/airplane.png");
        game.load.image("airplane_cloud", "res/airplane_cloud.png");
        game.load.image("airplane_shade", "res/airplane_shade.png");
        game.load.image("arrow", "res/arrow.png");
        game.load.image("backline", "res/backline.png");
        game.load.image("bestscoreback", "res/bestscoreback.png");
        game.load.image("car", "res/car.png");
        game.load.image("gain", "res/gain.png");
        game.load.image("gamelogo", "res/gamelogo.png");
        game.load.image("gameoverback", "res/gameoverback.jpg");
        game.load.image("go", "res/go.png");
        game.load.image("lamandoword", "res/lamandoword.png");
        game.load.image("light", "res/light.png");
        game.load.image("line_left", "res/line_left.png");
        game.load.image("line_right", "res/line_right.png");
        game.load.image("moveback", "res/moveback.jpg");
        game.load.image("ready", "res/ready.png");
        game.load.image("roadback", "res/roadback.jpg");
        game.load.image("roadend", "res/roadend.jpg");
        game.load.image("rotate", "res/rotate.png");
        game.load.image("share", "res/share.png");
        game.load.image("startback", "res/startback.jpg");
        game.load.image("startbutton", "res/startbutton.png");
        game.load.image("startdesc", "res/startdesc.png");
        game.load.image("tapbutton", "res/tapbutton.png");
        game.load.image("tapicon_left", "res/tapicon_left.png");
        game.load.image("tapicon_right", "res/tapicon_right.png");
        game.load.image("tourist", "res/tourist.png");
    }
    this.create = function () {
        game.stage.setBackgroundColor("#404040");
        // game.stage.setBackgroundColor("#00ffff");
        var style = {font: "48px '微软雅黑'", fill: "#ffffff"};
        this.text = game.add.text(game.width / 2, game.height / 2, "loading ... 100%", style);
        this.text.anchor.set(0.5, 0.5);
    }
    this.update = function () {
        if (this.i <= 10)
            this.text.setText("loading..." + this.i++ + "%");
        else
            game.state.start("prePlay");
    }
}


game.States.home = function () {
    this.create = function () {
        game.add.image(0, 0, "startback");
        game.add.image(game.width / 2, game.height / 5, "startdesc").anchor.setTo(0.5, 0.5);
        game.add.button(game.width / 2, game.height / 2, "startbutton", function () {
            game.state.start("prePlay");
        }).anchor.setTo(0.5, 1);

    }
}

game.States.prePlay = function () {
    this.create = function () {
        game.add.tileSprite(0, game.height, game.width, game.height, "roadback").anchor.set(0, 1);
        game.add.tileSprite(0, game.height, game.width, game.height, "backline").anchor.setTo(0, 1);
        game.add.sprite(game.width / 2, 470, "car").anchor.setTo(0.5, 1);
        var lineGroup = game.add.group();
        lineGroup.create(game.width / 2 + 10, 460, "line_left").anchor.setTo(1, 0);
        lineGroup.create(game.width / 2, 460, "line_right").anchor.setTo(0, 0);
        game.add.sprite(game.width / 2, 690, "airplane").anchor.setTo(0.5, 0.5);
        game.add.sprite(game.width * 3 / 4, 120, "rotate").anchor.setTo(0.5, 0.5);

        game.add.text(game.width * 3 / 4, 150, "km/h", {
                font: '26px Arial', fill: '#ffffff'
            }
        ).anchor.setTo(0.5, 0.5);

        game.add.text(game.width * 3 / 4, 120, 0, {
                font: '34px Arial', fill: '#ffffff'
            }
        ).anchor.setTo(0.5, 0.5);
        game.add.button(game.width / 2, game.height - 95, "tapbutton", function () {
            game.state.start("play");
        }).anchor.setTo(0.5, 0.5);
        game.add.sprite(game.width / 2, game.height, "tourist").anchor.setTo(0.5, 1);
        game.add.sprite(game.width / 2 - 80, game.height - 95, "tapicon_left").anchor.setTo(0.5, 0.5);
        game.add.sprite(game.width / 2 + 80, game.height - 95, "tapicon_right").anchor.setTo(0.5, 0.5);
    }
}


game.States.play = function () {
    var speed = 0;
    this.y = game.height;
    var countDown = 7;
    var countDownSpirt;

    var btnTap;
    var timer;
    var isAutoSpeedUp = false;
    var isSpeedCut = false;
    var speedSprite;
    var rotateSprite;
    var speedUnitSprite;
    var lineGroup;
    var plane;
    var car;
    var roadend;
    var roadBagHeight;
    var mileSprite;
    var planeAirSprite;
    var isFly = false;
    var isTiled = true;

    function tapBtn() {
        console.log("tap" + speed);
        speed += 0.5;
    }

    this.preload = function () {
        countDown = 7;
        speed = 0;
        isFly = false;
        isTiled = true;
        isAutoSpeedUp = false;
        isSpeedCut = false;
    }

    this.create = function () {
        this.roadBg = game.add.tileSprite(0, game.height, game.width, game.height, "roadback");
        this.roadBg.anchor.set(0, 1);
        roadBagHeight = this.roadBg.height;
        this.roadLine = game.add.tileSprite(0, game.height, game.width, game.height, "backline");
        this.roadLine.anchor.setTo(0, 1);
        roadend = game.add.sprite(game.width / 2, 0, "roadend");
        roadend.anchor.setTo(0.5, 1);
        this.bg1 = game.add.sprite(0, roadend.y - roadend.height, "sense_1");
        this.bg2 = game.add.sprite(0, this.roadBg.y - this.roadBg.height - 55, "sense_2");
        this.bg3 = game.add.sprite(0, this.bg2.y - this.bg2.height, "sense_3");
        this.bg4 = game.add.sprite(0, this.bg3.y - this.bg3.height, "sense_4");
        this.bg5 = game.add.sprite(0, this.bg4.y - this.bg4.height, "sense_5");
        this.bg1.anchor.set(0, 1);
        this.bg2.anchor.set(0, 1);
        this.bg3.anchor.set(0, 1);
        this.bg4.anchor.set(0, 1);
        this.bg5.anchor.set(0, 1);

        mileSprite = game.add.text(game.width * 1 / 2, game.height / 4, 0, {
                font: '68px Arial', fill: '#ffffff'
            }
        );
        mileSprite.anchor.set(0.5, 0.5);
        mileSprite.visible = false;

        car = game.add.sprite(game.width / 2, 400, "car");
        car.anchor.setTo(0.5, 0.5);
        lineGroup = game.add.group();
        lineGroup.create(game.width / 2 + 10, 460, "line_left").anchor.setTo(1, 0);
        lineGroup.create(game.width / 2, 460, "line_right").anchor.setTo(0, 0);

        plane = game.add.sprite(game.width / 2, 690, "airplane");
        plane.anchor.setTo(0.5, 0.5);
        game.add.sprite(game.width / 2, 710, "airplane_shade").anchor.setTo(0.5, 0.5);
        planeAirSprite = game.add.sprite(game.width / 2, 675, "airplane_cloud");
        planeAirSprite.anchor.setTo(0.5, 0);
        planeAirSprite.visible = false;

        rotateSprite = game.add.sprite(game.width * 3 / 4, 120, "rotate").anchor.setTo(0.5, 0.5);
        speedUnitSprite = game.add.text(game.width * 3 / 4, 150, "km/h", {
                font: '26px Arial', fill: '#ffffff'
            }
        ).anchor.setTo(0.5, 0.5);

        btnTap = game.add.button(game.width / 2, game.height - 95, "tapbutton", null);
        btnTap.anchor.setTo(0.5, 0.5);
        speedSprite = game.add.text(game.width * 3 / 4, 120, 0, {
                font: '34px Arial', fill: '#ffffff'
            }
        );
        speedSprite.anchor.setTo(0.5, 0.5);
        timer = game.time.create(false);
        timer.loop(1000, readyGo, this);
        timer.start();
    }

    this.update = function () {
        if (isAutoSpeedUp)
            speed += 0.04;
        else if (isSpeedCut) {
            if (speed > 0) {
                speed -= 0.05
                mileSprite.visible = true;
                score = Math.ceil(this.bg1.y - game.height);
                mileSprite.setText(score + "米");
                planeAirSprite.visible = true;
            } else {
                game.state.start("end");
            }

            if (!isFly) {
                isFly = true;
                game.add.tween(plane.position).to({y: plane.y - 50}, 1000, null, true, 0, 0, false);
                game.add.tween(planeAirSprite.position).to({y: planeAirSprite.y - 50}, 1000, null, true, 0, 0, false);
            }
        }

        if (isTiled) {
            this.roadLine.tilePosition.y += speed;
            this.roadBg.tilePosition.y += speed;
        } else {
            this.roadLine.y += speed;
            this.roadBg.y += speed;
            if (this.roadLine.y > game.height + this.roadLine.height)
                isSpeedCut = true;
            roadend.y = this.roadBg.y - this.roadBg.height;
            this.bg1.y = this.roadBg.y - this.roadBg.height - 55;
            this.bg2.y = this.bg1.y - this.bg1.height;
            this.bg3.y = this.bg2.y - this.bg2.height;
            this.bg4.y = this.bg3.y - this.bg3.height;
            this.bg5.y = this.bg4.y - this.bg4.height;
        }

        var y = Math.ceil(5 * speed);
        speedSprite.setText(y);
    }


    function readyGo() {
        if (countDownSpirt != null)
            countDownSpirt.destroy();
        switch (countDown) {
            case 7:
                countDownSpirt = game.add.image(game.width / 4, 120, "ready");
                countDownSpirt.anchor.setTo(0.5, 0.5);
                countDownSpirt.scale.setTo(2.0, 2.0);
                countDownSpirt.scale.setTo(1, 1);
                game.add.tween(countDownSpirt.scale).to({x: 1.5, y: 1.5}, 200, null, true, 0, 0, true);
                break;
            case 6 :
                countDownSpirt = game.add.image(game.width / 4, 120, "go");
                countDownSpirt.anchor.setTo(0.5, 0.5);
                game.add.tween(countDownSpirt.scale).to({x: 1.5, y: 1.5}, 200, null, true, 0, 0, true);
                break;
            case 5 :
                this.roadBg.loadTexture("moveback");
                btnTap.onInputDown.add(tapBtn, this);
                isAutoSpeedUp = true;
            case 4:
            case 3:
            case 2:
            case 1:
                countDownSpirt = game.add.text(game.width / 4, 120, countDown, {
                        font: '58px Arial', fill: '#ffffff'
                    }
                );
                countDownSpirt.anchor.setTo(0.5, 0.5);
                break;
            case 0:
                isAutoSpeedUp = false;
                btnTap.destroy();
                timer.destroy();
                timer = game.time.create();
                timer.add(1000, loseHaul);
                timer.start()
                break;
        }
        countDown--;
    }


    /**
     * 失去汽车牵引
     */
    function loseHaul() {
        isTiled = false;
        lineGroup.destroy(true, false);
        var dur = Math.ceil(roadBagHeight * 14 / speed);
        game.add.tween(car).to({angle: -120}, dur, Phaser.Easing.Quadratic.Out, true, 0, 0, false);
        game.add.tween(car.position).to({y: game.height + 50}, dur, Phaser.Easing.Linear.In, true, 0, 0, false);
    }
}
game.States.end = function () {
    this.create = function () {
        game.add.sprite(0, 0, "gameoverback");
        var bmd = game.add.bitmapData(game.width, game.height);
        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, game.width, game.height);
        bmd.ctx.fillStyle = '#000000';
        bmd.ctx.fill();
        game.add.sprite(0, 0, bmd).alpha = 0.6;

        game.add.text(game.width / 2, game.height / 4, score + "米", {
                font: '58px Arial', fill: '#ffffff', align: "center"
            }
        ).anchor.setTo(0.5, 0);

        var str = "本轮成绩\n\n您已超过" + Math.ceil(score * 100 / 9000) + "%的玩家，地球人已无法阻止你了。\n"
            + "分享游戏结果到微信朋友圈\n" + "即可获得Lamando凌度组装部件，\n"
            + "快去炫耀你的无敌战绩吧！";

        game.add.text(game.width / 2, game.height / 2 + 50, str, {
                font: '28px Arial', fill: '#ffffff', align: "center"
            }
        ).anchor.setTo(0.5, 1);

        game.add.button(game.width / 2 - 100, game.height * 2 / 3, "again", function () {
            game.state.start("prePlay");
        }).anchor.setTo(0.5, 0.5);
        game.add.button(game.width / 2 + 100, game.height * 2 / 3, "share", function () {
            var arrow = game.add.sprite(game.width - 40, 50, "arrow");
            arrow.anchor.setTo(0.5, 0);
            game.add.tween(arrow.position).to({y: 0}, 500, null, true, 0, -1, true);
        }).anchor.setTo(0.5, 0.5);

        game.add.button(game.width / 2, game.height * 2 / 3 + 120, "bestscoreback").anchor.setTo(0.5, 0.5);
        game.add.button(game.width - 40, game.height - 30, "gamelogo").anchor.setTo(1, 1);
    }
}
game.state.add('boot', game.States.boot);
game.state.add('loading', game.States.loading);
game.state.add('home', game.States.home);
game.state.add('prePlay', game.States.prePlay);
game.state.add('play', game.States.play);
game.state.add('end', game.States.end);
game.state.start('boot');
